// JavaScript Document

$(document).ready(function(){
	
	
		   //$('.btn2').trigger('click'); 	
		

//<--- fancybox jquery -->
$(".fancybox").fancybox({
    // solves some issues with streamed media
    iframe: {
        preload: false
    },
    // Increase left/right margin for iframe content
    margin: [20, 60, 20, 60]
});

$(".fancybox").fancybox({
 tpl: {
  closeBtn: '<div title="Close" id="myCloseID"></div>'
 }
});
//<!--- fancybox jquery -->

$('.bxslider').bxSlider({
   speed:1500,
   pause: 8000,   
   auto: true,
   //preventDefaultSwipeY: false,
   swipeThreshold: 100  
});	

$(".navbar-nav .notification-alert").hide();

setTimeout(function(){ 	$(".messages").hide('slow');}, 5000);

var getWidth = $(window).width();

if(getWidth >= 768){
	//<--- BX Slider -->
	$('.bxslider1').bxSlider({
	  minSlides: 1,
	  maxSlides: 1,
	  slideWidth: 275,
	  auto:true,
	  pager:false,
	  speed:200,
	  slideMargin: 0,
	  onSliderLoad: function (currentSlide) {          
		   $('.device-text h2').html($('.bxslider1 li img:last').attr('title')); 
		},
	  onSlideAfter: function (currentSlide) {
			  $('.device-text h2').html(currentSlide.find('img').attr('title'));
		  }	
	});
	//<!--- BX Slider -->
}

if(getWidth <= 767){

	//<--- for login popup to check IOS devices keyboard close-->	
	document.addEventListener('focusout', function(e) {
		$(".my_modal.popup1").css("top","30%");	
	});
	//<!--- for login popup to check IOS devices keyboard close-->	

	//<--- to fix the body -->
	$( "#consent-form,#report-Beat" ).on('shown.bs.modal', function(e){ 
		$("body").addClass("fixed-body");
	});	

	$( "#consent-form,#report-Beat" ).on('hidden.bs.modal', function(e){
	   $("body").removeClass("fixed-body");
	});		
	//<!--- to fix the body -->
	
	//<--- BX Slider -->
	$('.bxslider1').bxSlider({
	  minSlides: 1,
	  maxSlides: 1,
	  slideWidth: 180,
	  auto:true,
	  pager:false,
	  speed:200,
	  slideMargin: 0,
	  onSliderLoad: function (currentSlide) {          
		   $('.device-text h2').html($('.bxslider1 li img:last').attr('title')); 
		},
	  onSlideAfter: function (currentSlide) {
			  $('.device-text h2').html(currentSlide.find('img').attr('title'));
		  }	
	});
	//<!--- BX Slider -->
}

if( (  getWidth>= 480 ) && (getWidth <=767) ){
	$( "#consent-form,#report-Beat" ).on('shown.bs.modal', function(e){ 
		$("#consent-form .modal-dialog,#report-Beat .modal-dialog").css("top","0");		
	});		
}

// $(window).on("resize",function(){
// 	jQuery('.articleContainer').isotope('reLayout');
// }); 

$(".portfolioFilter a").click(function(){
	var htmlString = $( this ).html();  
});	
	
/*owl*/

////------- Custom Carousel
if($("#owl-demo").length){
	var owl = $("#owl-demo");
 
  owl.owlCarousel({
      items : 6, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      //itemsDesktopSmall : [900,4], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
	  pagination : false,
	  autoPlay : true,
	  navigation : true,
	  
	  afterAction: function(){
      if ( this.itemsAmount > this.visibleItems.length ) {
        $('.owl-next').show();
        $('.owl-prev').show();

        $('.owl-next').removeClass('disabled');
        $('.owl-prev').removeClass('disabled');
        if ( this.currentItem == 0 ) {
          $('.owl-prev').addClass('disabled');
        }
        if ( this.currentItem == this.maximumItem ) {
          $('.owl-next').addClass('disabled');
        }

      } else {
        $('.owl-next').hide();
        $('.owl-prev').hide();
      }
    }	  	  
  });
} 
  
 
  // Custom Navigation Events
  $(".next").click(function(){
    owl.trigger('owl.next');
  })
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  })
  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })
  $(".stop").click(function(){
    owl.trigger('owl.stop');
  })
	
//<--- for search -->
$('#search-label').click(function () {  
	var $e = $('#search-terms');
	var search = $('#search-terms').val();
	if(search == ""){
		 $e.animate({
		'left': $e.css('left') === "0px" ? '-1299px' : "0px"
		})
	}else{
		 $e.animate({
		'left': "0px"
		})
		$("#search-form").submit();
	}   
}); 

var tab_id = $(".portfolioFilter ul li a.current").attr("id");
if(tab_id == "top"){
	action = "/search/2";
}else{
	action = "/search/1";
}
$("#search-form").attr("action",action);

//<--- for search -->
		
if($(".select").length){
	$('.select').customSelect();
 }

/*pop-up*/
$('.my_modal_open').click(function(){
	$('.popup1, .overlayBg').fadeIn('slow');
});
$('.my_modal_close').click(function(){
	$('.popup1, .overlayBg').fadeOut('slow');	
}); 

$('.open-forgot-popup').click(function(){
	$('.popup2, .overlayBg').fadeIn('slow');
	$('.popup1').fadeOut('slow');
});
$('.my_modal_close').click(function(){
	$('.popup2, .overlayBg').fadeOut('slow');	
});	

$('a.page-scroll').bind('click', function(event) {
	var $anchor = $(this);
	$('html, body').stop().animate({
		scrollTop: $($anchor.attr('href')).offset().top+20
	}, 1500, 'easeInOutExpo');
	event.preventDefault();
});
	
/*calendar*/
if($("#dob").length){
 $( "#dob" ).datepicker({
		//dateFormat: 'yy-mm-dd',
		dateFormat: 'dd-M-yy',
		maxDate: '0',
		//maxDate: '-17Y',
		changeMonth: true,
		changeYear: true,
		yearRange: "-100:+0"
	 });	
}

$("#upload-link").on('click', function(e){
	e.preventDefault();
	$("#upload-doc:hidden").trigger('click');
});

$(document).on('click', '.report-article', function(){ 
	var beat_id = $(this).attr("beat_id");
	$("#article_id").val(beat_id);
});
 
//<--- Signup GMC file extension check -->
$('#form-file-type input[type="submit"]').click(function(e){
	e.preventDefault();
	var form = $('#form-file-type');
	var file = $('input[type="file"]', form).val();
	var exts = ['jpg','jpeg','gif','png'];
	var msg = $('.error-msg', form);
	msg.hide();
	// first check if file field has any value
	if ( file ) {
		// split file name at dot
		var get_ext = file.split('.');
		// reverse name to check extension
		get_ext = get_ext.reverse();

		// check file type is valid as given in 'exts' array
		if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
			msg.show().html( '<strong style="color:#40b02c">Allowed extension!</strong>' );
		} else {
			msg.show().html( '<strong style="color:#f00">Invalid file!</strong>' );
		}
	}else{
		msg.show().html( '<strong style="color:#f00">Please choose a file!</strong>' );
	}
});
//<!--- Signup GMC file extension check -->

//<--- show password -->
$("#showPwd").click(function () {
	if ($(".password").attr("type")=="password") {
		$(".password").attr("type", "text");
	}else{
		$(".password").attr("type", "password");
	}
});		
//<!--- show password -->
			
function dateToYMD(date) { 
	var d = date.getDate(); 
	var m = date.getMonth() + 1; 
	var y = date.getFullYear(); 
	return ''+ (m<=9 ? '0' + m : m) + '/' + (d <= 9 ? '0' + d : d) + '/' + y ; 
}

//<--- Add methods for validate plugin -->
$.validator.addMethod("regex", function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please enter a valid Phone number." );

$.validator.addMethod( "compareDate", function(value, element) {
			var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];						
			var now = new Date();
			now.setFullYear(now.getFullYear() - 17);			
			max_date = dateToYMD(now);			
			max_date = max_date.split('/');
			var new_max_date = new Date(max_date[2],months.indexOf(max_date[0])+6,max_date[1]);	
			
			select_date = value;			
			select_date = select_date.split('-');
			var new_select_date = new Date(select_date[2],months.indexOf(select_date[1]),select_date[0]);	
			
			if(new_max_date >= new_select_date) {
				return true;
			}else{
				return false;
			}			
        },
        "Age should be minimum 17 years." );
        
$.validator.addMethod( "validvideo", function(value, element, validvideo) {
			if(value != ""){			
				var n = value.indexOf(validvideo); 
				if(n > 0){
					return true;
				}else{
					return false;
				}
			}else{
					return true;
			}
        },
        "Please enter a required video url.");

$.validator.addMethod("uploadFile", function (val, element,limit) {
			if(val != ''){
				var size = element.files[0].size;
				if (size > (limit*1000000)) {
					return false
				} else {
					return true
				}
			}else{
				return true
			}
		}, 
		"Invalid file size");

$.validator.addMethod("noHTML", function(value, element) {
			if(element.id == "search-terms"){
				return true;
			}else{
				return this.optional(element) || !/<(\w+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/.test(value);   
			}    
		}, 
		"No HTML tags are allowed!");

$.validator.addMethod("reLayout", function(value, element) {
			//jQuery('.articleContainer').isotope('reLayout');
			return true;
		}, 
		"check");

$.validator.addMethod("emailExist", function(value, element) {
			var returned = true;
			$.ajax({
				url: "/frontusers/emailExist",
				type: "post",
				async: false,
				data: {'email':value},
				success: function(d) {
					if (d == 0){	
						returned = false;		
					}else{
						returned = true;	
					}	
				}
			});
			return returned;
		}, 
		"Email already exists, register with some other email.");	

$.validator.addMethod("VerifyAliasName", function(value, element) {
			var returned = false;
			$.ajax({
				url: "/frontusers/VerifyAliasName",
				type: "post",
				async: false,
				//data: {'email':value},
				success: function(d) {
					if (d == 0){
						//alert('Please save alias name first.');	
						returned = false;		
					}else{
						returned = true;	
					}	
				}
			});
			return returned;
		}, 
		"Please enter alias name in your profile to post beat as anonymous." );	

$.validator.addMethod("VerifyUniqueAliasName", function(value, element) {
			var returned = false;
			var attrib = $("#"+element.id).attr('readonly');
			if(attrib == "readonly"){
				returned = true;
			}else{
				$.ajax({
					url: "/frontusers/VerifyUniqueAliasName",
					type: "post",
					async: false,
					data: {'alias_name':value},
					success: function(d) {
						if (d == 0){
							//alert('Please save alias name first.');	
							returned = false;		
						}else{
							returned = true;	
						}	
					}
				});
			}
			return returned;
		},
		"Alias Name already exist." );	

// Add images Validation
$("#beatimagesform").validate({
	focusInvalid: false,
	errorPlacement: function(error, element) {		
					error.insertAfter(element.parent());
            },
	rules: {
		"beatimage-filename": {
				required: true,
				accept: "jpg|jpeg|png|gif",
				uploadFile: 4,				
			},
	},
	messages: {
		"beatimage-filename": {
				//required: "Please attach your profile pic",
				accept: "Selected file is not valid."
			},
	}
});		

/*Check image width*/
$("div").delegate('#file-upload-button-select','change',function(self) {	
	if ($('#beatimagesform').valid()){		
		var validImage = true;
		var file = $(this).get(0).files[0];
		var self = $(this);
		var imgWidth = '';
		var imgHeight = '';
		var oFReader = new FileReader();
		oFReader.readAsDataURL(file);
		oFReader.onload = function (oFREvent) {
			var image = new Image();
			image.src     = oFREvent.target.result;
			image.onload = function () {
				imgWidth  = image.width;
				imgHeight = image.height;
				var thumbvalid    = (imgWidth >= 350);
				if (!thumbvalid) {
					$('label.filerr').remove();
					$("#beatimagesform #file-upload-button-select").addClass("error");
					if($('#beatimagesform label.error').length){
						$('#beatimagesform label.error').text('Image width should be greater than 350.');
					}else{
						$("#file-upload-button-select").parent().after('<label for="file-upload-button-select" generated="true" class="error">Image size should be greater than dimension 350.</label>');						
					}						
				} else {
					$('#beatimagesform label.error').remove();
					$("#file-upload-button-select").removeClass("error");
				}
			}
		}
	}
	return false;
});	
/* end Check image width*/


$("div").delegate('#attach-upload-button-select','change',function(self) {	
	$('#beatfilesform').valid();
	return false;
});	

// ! -- Add images Validation

/*Validation For all forms*/
$('form').each(function(){
  $(this).validate({
			errorPlacement: function(error, element) {		 
				if(element.attr('id') == "report"){
				 error.insertAfter('#custom-dropdown');
				}else if(element.attr('id') == "loginemail" || element.attr('id') == "loginpassword"){
					error.appendTo('.login-error');
				}else if(element.attr('id') == "forgetemail"){
					error.appendTo('.forgot-error');	
				}else if(element.attr('id') == "add-beat-title" || element.attr('id') == "add-beat-description" || element.attr('id') == "is_Anonymous"){
					error.appendTo('.add-beat-error');				
				}else if(element.attr('id') == "update-beat-title" || element.attr('id') == "update-beat-category" ||  element.attr('id') == "update-beat-description"){
					error.appendTo('.add-beat-error');				
				}else{
					error.insertAfter(element.parent());
				}						
            },

			rules: {
					"data[contact][name]": {
						required: true,
						regex: "^[a-zA-Z ]+$",		 
						minlength: 2,
						maxlength: 30
					},
					"data[contact][email]": {
						required: true,
						email: true
					},
					"data[contact][captcha]": {
						required: true,
						
						
					},

					"data[contact][message]": {
						required: true,
						maxlength: 300
					},
			        "data[signup][name]":  {
						required: true,
						regex: "^[a-zA-Z ]+$",		 
					},
			        "data[signup][last_name]":  {
						required: true,
						regex: "^[a-zA-Z ]+$",		 
					},
			        "data[signup][email]": {
						required: true,
						email: true,
						emailExist: true						
					},					
			        "data[signup][password]": {
						required: true,
						regex: "^(?=.*[0-9])(?=.*[a-zA-Z!_@#\$%\^\&*\])([a-zA-Z!_@#\$%\^\&*\0-9]+)$",
						minlength: 4,
						maxlength: 15
					},
					"data[signup][confirm_password]": {
						required: true,
						regex: "^(?=.*[0-9])(?=.*[a-zA-Z!_@#\$%\^\&*\])([a-zA-Z!_@#\$%\^\&*\0-9]+)$",
						minlength: 4,
						maxlength: 15,
						equalTo: "#password"
					},
					"data[signup][dob]": {
						required: true,
						compareDate: true
					},
					"data[signup][profession_id]": "required",
					"data[signup][county]": {
						required: true
					},
					"data[signup][country_id]": "required",
					"data[signup][terms]": "required",
					"data[settings][old_password]": {
						required: true,
						regex: "^(?=.*[0-9])(?=.*[a-zA-Z!_@#\$%\^\&*\])([a-zA-Z!_@#\$%\^\&*\0-9]+)$",
						minlength: 4,
						maxlength: 15
					},
					"data[settings][password]": {
						required: true,
						regex: "^(?=.*[0-9])(?=.*[a-zA-Z!_@#\$%\^\&*\])([a-zA-Z!_@#\$%\^\&*\0-9]+)$",
						minlength: 4,
						maxlength: 15
					},
					"data[settings][confirm_password]": {
						required: true,
						regex: "^(?=.*[0-9])(?=.*[a-zA-Z!_@#\$%\^\&*\])([a-zA-Z!_@#\$%\^\&*\0-9]+)$",
						minlength: 4,
						maxlength: 15,
						equalTo: "#password"
					},	
					"data[login][email]": {
						required: true,
						email: true
					},			
					"data[login][password]": {
						required: true,
						regex: "^(?=.*[0-9])(?=.*[a-zA-Z!_@#\$%\^\&*\])([a-zA-Z!_@#\$%\^\&*\0-9]+)$",
						minlength: 4,
						maxlength: 15,
					},			
					"data[profile][name]":  {
						required: true,
						regex: "^[a-zA-Z ]+$",		 
						minlength: 2,
						maxlength: 30
					},
					"data[profile][last_name]":  {
						required: true,
						regex: "^[a-zA-Z ]+$",		 
						minlength: 2,
						maxlength: 30
					},
					"data[profile][alias_name]":{
						minlength: 3,
						VerifyUniqueAliasName: true,
						maxlength: 30						
					},
					"data[profile][dob]": {
						required: true,
						compareDate: true
					},
					//"data[profile][profession_id]": "required",
					"data[profile][county]": "required",
					"data[profile][country_id]": "required",					
					"data[profile][address]":{
						//required: true,
						minlength: 5
					},
					"data[profile][phone_number]":{
						//required: true,
						regex: "^[0-9-+]+$",
						minlength: 10,
						maxlength: 14
					},
					"data[forgotPassword][forgetemail]": {
						required: true,
						email: true
					},
					password: {
						required: true,
						regex: "^(?=.*[0-9])(?=.*[a-zA-Z!_@#\$%\^\&*\])([a-zA-Z!_@#\$%\^\&*\0-9]+)$",
						minlength: 4,
						maxlength: 15,
					},
					confirm_password: {
						required: true,
						regex: "^(?=.*[0-9])(?=.*[a-zA-Z!_@#\$%\^\&*\])([a-zA-Z!_@#\$%\^\&*\0-9]+)$",
						minlength: 4,
						maxlength: 15,
						equalTo: "#password"
					},	        
					name:"required",
					message:{
							required: true,
							minlength: 3,
							maxlength: 300
					},	
					firstname: "required",
					lastname: "required",
					aliasname: {
						required: true,
						minlength: 3
					},	
					patientName: {
						required: true,
						minlength: 3
					},
					email: {
						required: true,
						email: true
					},
					dob: "required",
					desc: {
						required: true,
						minlength: 20
					},
					report: "required",
					topic: {
						required: "#newsletter:checked",
						minlength: 2
					},
					agree: "required",					
					filename: {
						accept: "jpg|jpeg|png|gif",
					},
					"data[comment][comment]":"required"	,
					"data[beat][title]": {
						required: true,
						//reLayout: true
					},										
					"data[beat][description]":{
						required: true,
						noHTML: true,
						reLayout: true
					},
					"data[beat][is_Anonymous]":{
						VerifyAliasName: {
								depends: function() {
									return $('input[id=is_Anonymous]').is(':checked');
								}
						}
					},		
					"data[beat][is_anonymous]":{
						VerifyAliasName: {
								depends: function() {
									return $('input[id=is_Anonymous]').is(':checked');
								}
						}
					},							
					"data[beatvideosform][video][]":"required",
					videoName1:{
							required: {
								depends: function() {
									return $('input[name=video]:checked').val() == 'youtube';
								}
							},
							url: true,
							validvideo: "youtube.com/",
					},
					videoName2:{
							required: {
								depends: function() {
									return $('input[name=video]:checked').val() == 'vimeo';
								}
							},
							url: true,
							validvideo: "vimeo.com/",
					},
					"data[consent-form][patientName]":{
						required: true,
						regex: "^[a-zA-Z ]+$",		 
					},
					"data[consent-form][agree]":"required",
					"data[consent-form][videoName]": {
							required: {
								depends: function() {
									return $('input[name=video]:checked').val() == 'youtube';
								}
							},
							url: true,
							validvideo: "youtube.com/",
						},
					"data[consent-form][videoName1]": {
							required: {
								depends: function() {
									return $('input[name=video]:checked').val() == 'vimeo';
								}
							},
							url: true,
							validvideo: "vimeo.com/",
						},
					"data[consent-form][email]":{
						//required: true,
						email: true
					},
					"data[consent-form][filename]":{
						uploadFile: 4,
						accept: "jpg|jpeg|png|gif"
					},
					"data[consent-form][signature]":{
						uploadFile: 4,
						accept: "jpg|jpeg|png|gif"
					},
					attach:{
						required: true,
						uploadFile: 4,
						accept: "xls|xlsx|ppt|pptx|docx|doc|pdf"
					},
					"data[reportBeat][type]":{
						required: true
					},
					"data[reportBeat][reason]":{
						required: true,
						maxlength: 100
					},
					search: "required",
					
			},
			messages: {
					search: "",
					videoName1: {
							required: "Youtube Url is not valid.",
							url: "Youtube Url is not valid.",
							validvideo: "Youtube Url is not valid.",
					},					
					videoName2:{
							required: "Vimeo Url is not valid.",
							url: "Vimeo Url is not valid.",
							validvideo: "Vimeo Url is not valid.",
					},												
					"data[contact][name]": {
						required: "Name is required.",
						regex: "Invalid Name",						 
						minlength: "Name should be 2 to 30 characters long.",
						maxlength: "Name should be 2 to 30 characters long."
					},
					"data[contact][email]": {
						required: "Email is required",
					},"data[contact][message]": {
						required: "Message is required",
					},	

					"data[contact][captcha]": {
						required: "Captcha field is required.",
					}
					,				
					"data[forgotPassword][forgetemail]": {
						required: "Email is required",
						email: "Email is not valid."						
					},
			        "data[signup][name]": {
						required: "First Name is required.",
						regex: "First Name is not valid.",		 
					},
			        "data[signup][last_name]": {
						required:  "Last Name is required.",
						regex: "Last Name is not valid",		 
					},
			        "data[signup][email]": {
						required: "Email is required.",
					},					
			       		    
					"data[signup][password]": {
						required: "Password should be 4 to 15 characters long.",
						regex: "Password must be alphanumeric.",
						minlength: "Password should be 4 to 15 characters long.",
						maxlength: "Password should be 4 to 15 characters long."

					},
					"data[signup][confirm_password]": {
						required: "Confirm Password is too short or empty.",
						regex: "Confirm Password must be alphanumeric.",
						minlength: "Confirm Password should be 4 to 15 characters long.",
						maxlength: "Confirm Password should be 4 to 15 characters long.",
						equalTo: "Password and confirm password doesn't match."
					},
					"data[signup][terms]": "Please accept Terms and Policies.",
					"data[signup][dob]": {
						required:"Date of birth is required."
					},
					"data[signup][profession_id]": "Profession/Occupation is required.",
					"data[signup][county]": {
						required: "County/State is required."
					},
					"data[signup][country_id]": "Country is required.",


					"data[settings][old_password]": {
						required: "Old Password should be 4 to 15 characters long.",
						regex: "Old Password must be alphanumeric.",
						minlength: "Old Password should be 4 to 15 characters long.",
						maxlength: "Old Password should be 4 to 15 characters long."
					},
					"data[settings][password]": {
						required: "Password is too short or empty.",
						regex: "Password must be alphanumeric.",
						minlength: "Password should be 4 to 15 characters long.",
						maxlength: "Password should be 4 to 15 characters long."
					},
					"data[settings][confirm_password]": {
						required: "Confirm Password is too short or empty.",
						regex: "Confirm Password must be alphanumeric.",
						minlength: "Confirm Password should be 4 to 15 characters long.",
						maxlength: "Confirm Password should be 4 to 15 characters long.",
						equalTo: "Password and confirm password doesn't match."
					},	
															
					"data[login][email]":{
						required: "Email is required.",
						email: "Email is not valid."
					},			
					"data[login][password]": {
						required: "Password is required.",
						regex: "Password must be alphanumeric.",
						minlength: "Password should be 4 to 15 characters long.",
						maxlength: "Password should be 4 to 15 characters long."
					},
					
					"data[profile][name]":{
						required: "First Name is required.",
						regex: "First Name is not valid.",	
						maxlength: "You cannot enter more than 30 Characters"	 
					},
					"data[profile][last_name]": {
						required: "Last Name is required.",
						regex: "Last Name is not valid.",	
						maxlength: "You cannot enter more than 30 Characters"		 
					},
					"data[profile][alias_name]": {
						maxlength: "You cannot enter more than 30 Characters"		 
					},
					"data[profile][dob]": {
						required: "Date of Birth is required."
					},
					"data[profile][county]": {
						required: "County/State is required.",
					},
					"data[profile][country_id]": "Country is required.",					
					"data[profile][address]":{
						//required: "Address is required.",
					},
					"data[profile][phone_number]":{
						//required: "Phone Number is required.",
						regex: "Phone Number is not valid",
						maxlength: "You cannot enter more than 14 Characters"	
					},
					
					"data[consent-form][patientName]":{
						required: "Patient Name is required.",
						regex: "Patient Name is not valid.",		 
					},
					"data[consent-form][agree]":"Agree on Consent Form.",
					"data[consent-form][videoName]": {
							required: "Youtube Url is not valid.",
							url: "Youtube Url is not valid.",
							validvideo: "Youtube Url is not valid.",
					},					
					"data[consent-form][videoName1]": {
							required: "Vimeo Url is not valid.",
							url: "Vimeo Url is not valid.",
							validvideo: "Vimeo Url is not valid.",
					},					
					"data[consent-form][email]":{
						required: "Email is required.",
						email: "Email is not valid."
					},
					"data[consent-form][signature]":{
						required: "Signature is required",
					},						
					name: "This field is required.",
			        message:{
						required: "This field is required.",
						minlength: "Message must consist of at least 3 characters",
						maxlength: "Message must consist of less than 300 characters"
					},
					password: {
							required: "Old Password should be 4 to 15 characters long.",
							regex: "^[a-z0-9\-\s]+$",
							minlength: "Old Password should be 4 to 15 characters long.",
							maxlength: "Old Password should be 4 to 15 characters long."
					},
					confirm_password: {
							required: "Password is too short or empty.",
							regex: "Password must be alphanumeric.",
							minlength: "Old Password should be 4 to 15 characters long.",
							maxlength: "Old Password should be 4 to 15 characters long.",
							equalTo: "Password and confirm password doesn't match."
					},				
					firstname: "This field is required.",
					lastname: "This field is required.",
					aliasname: {
						required: "This field is required.",
						minlength: "Your Alias Name must consist of at least 3 characters"
					},
					patientName: {
						required: "This field is required.",
						minlength: "Your Name must consist of at least 3 characters"
					},					
					filename: {
						required: "Please attach your profile pic",
						accept: "Images with jpg,jpeg,png,gif extensions are allowed."
					},
					report: "Please select atleast one option",
					desc: {
						required: "This field is required.",
						minlength: "You must enter atleast 20 characters"
					}, 					
					agree: "Please accept our policy",	
					"data[comment][comment]":"Please enter comment.",	
					"data[beat][title]": {
						required: "Beat Title is required.",		
					},				
					"data[beat][category]":  "Category is required.",					
					"data[beat][description]":{
						required: "Description is required.",			
					},	
					attach:	{
						accept: "Selected file is not valid."
					},	
					"data[reportBeat][report]":{
						required: "Type is required",		
					},
					"data[reportBeat][desc]":{
						required: "Description is required",		
						maxlength: "Description must be less than equal 100 characters"
					},
					"data[reportBeat][reason]":{		
						maxlength: "You cannot enter more than 100 Characters"
					}
			} 
  });
// <--apply No html rule to all text fields-->
$(this).find('input[type="text"]').each(function(){
	$(this).rules('add', {
		noHTML: true,
		messages: {
			noHTML: "No HTML tags are allowed!"
		}
	});
});
// <!-- apply No html rule to all text fields-->

});	
/*//valid*/
  
$('#addbeat .edit-article-form fieldset .form-control').click(function(e) {
  e.stopPropagation();
});

$('[data-toggle="tooltip"]').tooltip();

/*search*/
$('.search').focus(function() {
     $('.search').css('width', '450px');
     $('#id_to_animate').animate({width: '200px'});
});
$('.search').blur(function() {
     $('.search').css('width', '150px');
     $('#id_to_animate').animate({width: '100px'});
});


//<-- for multi select plugin-->
if($("#addbeat #ms,#editbeat #ms").length){
	$('#addbeat #ms,#editbeat #ms').change(function() {
        }).multipleSelect({
            width: '100%',
			placeholder: "Choose Category"
   });
}

/*multiselect dropdown*/
if($("#ms").length){
$('#ms').change(function() {
        }).multipleSelect({
            width: '100%',
			placeholder: "Speciality"
   });
}

//<!-- for multi select plugin-->

$('#upload').change(function() {
    var filename = $(this).val();
    var lastIndex = filename.lastIndexOf("\\");
    if (lastIndex >= 0) {
        filename = filename.substring(lastIndex + 1);
    }
    $('#filename').val(filename);
});

/*nav*/
$('.dropdown-toggle').click(function(){
 if($('.user-icon').hasClass("active")){
   $('.user-icon').removeClass("active");
   $(".user-nav").animate({
     right: "-550"
	 },400, function() {
	 
	 });
	 $("body").animate({
	   right: "0"
	  }, 400, function(){
    });	  
 
 }

});


if ($(window).width() >= 768) {	
	$('.user-icon').click(function(){
			if($(this).hasClass("active")){
				//jQuery('body').removeClass('fixed-body');			
				$(this).removeClass("active");
					$( ".user-nav" ).animate({
						right: "-550"
						}, 400, function() {
						// Animation complete.
					});
					$( "body" ).animate({
						right: "0"
						}, 400, function() {
						// Animation complete.
					});
			}else{
				//jQuery('body').addClass('fixed-body');
				$(this).addClass("active");
					$( ".user-nav" ).animate({
					right: "0"
					}, 400, function() {
					// Animation complete.
					});
					
					$( "body" ).animate({
					right: "550"
					}, 400, function() {
					// Animation complete.
					});
					
			}
		});
	}

       else {
	   
	   $('.user-icon').click(function(){
	     $('header').append('<div class="overlay-bg"></div>');
		 //$('.navbar-fixed-top').css('z-index','1032');
	   
		if($(this).hasClass("active")){
			//jQuery('body').removeClass('fixed-body');			
			$(this).removeClass("active");
				$( ".user-nav" ).animate({
					right: "-260"
					}, 500, function() {
					// Animation complete.
				});
				$( "body" ).animate({
					right: "0"
					}, 500, function() {
					// Animation complete.
				});
		}else{
			//jQuery('body').addClass('fixed-body');           
			$(this).addClass("active");
				$( ".user-nav" ).animate({
				right: "0"
				}, 500, function() {
				// Animation complete.
				});
				$( "body" ).animate({
				right: "260"
				}, 500, function() {
				// Animation complete.
				});
				
		}
	});	
  
     }

    $('.close-menu').click(function(){
	   $('.overlay-bg').remove();
	   //$('.navbar-fixed-top').css('z-index','1030');
		$('.user-icon').removeClass("active");		 
			//jQuery('body').removeClass('fixed-body');
				$( ".user-nav" ).animate({
					right: "-260"
					}, 500, function() {
					// Animation complete.
				});
				$( "body" ).animate({
					right: "0"
					}, 500, function() {
					// Animation complete.
				});
		
	});

/*animation */
if($(".animate-block").length > 0){
	// This example adds a duration to the tweens so they are synced to the scroll position
	var controller = $.superscrollorama();
	// amount of scrolling over which the tween takes place (in pixels)
	var scrollDuration = 2; 

	// individual element tween examples
	controller.addTween('.animate-block', TweenMax.from( $('.animate-block'), .5, {css:{opacity: 0}}), scrollDuration);
}

//////////////////////////////////////// custom for layouts///////////////////////////////////
//<--for image previews	-->
$("#upload-doc").change(function (e) {
	var allowedExtension = ['jpeg', 'jpg', 'png', 'gif'];
	var file, img;
	var msg = $('.error-msg');
	if (/^image/.test(this.files[0].type)){ 
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {
		};
			msg.show().html( '<strong style="color:#40b02c">Allowed extension!</strong>' );
			img.src = URL.createObjectURL(file);
			$('#submit').removeAttr('disabled');
			
			if ($("#gmc-img").length > 0){
				$('#gmc-img').attr('src', img.src);
			}else{			
				$('.upload-box').append('<img src="'+img.src+'" id="gmc-img">');
			}
		}
	}else{
		if ($("#gmc-img").length > 0){
			$('#gmc-img').remove();
		}
		msg.show().html( '<strong style="color:#f00">Invalid file!</strong>' );
		
	}
});
$("input#file-upload-button-select").change(function (e) {
	var allowedExtension = ['jpeg', 'jpg', 'png', 'gif'];
	var file, img;
	var msg = $('.error-msg');
	if (/^image/.test(this.files[0].type)){ 
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {
			};
			img.src = URL.createObjectURL(file);
			$('#beat-image-preview').attr('src', img.src);			
		}
	}else{
		$("#beat-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
	}
});	
$("input#file-upload-button").change(function (e) {
	var allowedExtension = ['jpeg', 'jpg', 'png', 'gif'];
	var file, img;
	var msg = $('.error-msg');
	if (/^image/.test(this.files[0].type)){ 
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {
			};
			img.src = URL.createObjectURL(file);
			$('#consent-image-preview').attr('src', img.src);			
		}
	}else{
		$("#consent-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');	
	}
	var y = $(window).scrollTop();
	$(window).scrollTop(y+10);
});	
$("input#signature-upload-button").change(function (e) {
	var allowedExtension = ['jpeg', 'jpg', 'png', 'gif'];
	var file, img;
	var msg = $('.error-msg');
	if (/^image/.test(this.files[0].type)){ 
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function () {
			};
			img.src = URL.createObjectURL(file);
			$('#signature-image-preview').attr('src', img.src);			
		}
	}else{
		$("#signature-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');	
	}
	var x = $(window).scrollTop();
	$(window).scrollTop(x+10);
});	
//<!--for image previews	-->	

$('input#file-upload-button,input#signature-upload-button').on('click',(function(e) {
	var z = $(window).scrollTop();
	$(window).scrollTop(z+10);
}));
	
$(".who-is-it .submenu").hide();
$( "a.who-is-it-for" ).on( "click", function() {
	$(".who-is-it .submenu").toggle('slow');
});
	
	//Save consent in database
	$('#consent-frm').on('submit',(function(e) {
		e.preventDefault();
		if ($('#consent-frm').valid()){
			
			var formData = new FormData(this);
			$('#consent-form .btn.btn-blue').attr('disabled',true);
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				cache:false,
				contentType: false,
				dataType:"json",
				processData: false,
				success:function(data){
					liclassname = data.name.replace(' ', '_');
					var li = '<li class="'+liclassname.toLowerCase() +'"><div class="gry-lbl"> </div><input type="checkbox" name="consent_ids" value="'+data.id+'" id="checkbox'+data.id+'"><label for="checkbox'+data.id+'"></label>'+data.name+'<span>'+data.created+'</span> </li>'                                 ;
					if ($('.consent-select-box').length <= 0) {
						var divul = '<div class="consent-select-box"><ul></ul></div>'                                 ;
						$( divul ).insertBefore( "div.pull-right" );					
					}
					$('.consent-select-box ul').append(li);     
					$('#consent-frm')[0].reset();
					
					$('#signature-upload-filename').html('no file selected');
					$("#signature-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
					$('#file-upload-filename').html('no file selected');
					$("#consent-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
					$('.consent-mess').css("color","green");
					$('.consent-mess').text("Consent form added successfully");
					$('#consent-frm .select-video input[type="radio"]').removeClass('clicked');		
					$("#videoYT").prop('disabled', true);
					$("#videoVm").prop('disabled', true);
					$('#consent-form .btn.btn-blue').attr('disabled',false);
					setTimeout(function(){ $(".consent-mess").text(''); }, 3000);
				},
				error: function(data){
					console.log("error");
					console.log(data);
				}
			});
		}
	}));
	$("#consent-frm #cancel,#consent-form .consent-module button.close").on( "click", function() {
		$('#consent-frm')[0].reset();
		$('label.error').text("");
		$('#signature-upload-filename').html('no file selected');
		$("#signature-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
		$('#file-upload-filename').html('no file selected');
		$("#consent-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
		$('#consent-frm .select-video input[type="radio"]').removeClass('clicked');		
		$("#videoYT").prop('disabled', true);
		$("#videoVm").prop('disabled', true);
	});
		
	$( ".search-consent .search-bar" ).keyup(function() {
			var searchValue = $( ".search-consent .search-bar" ).val();
			searchValue = searchValue.replace(' ', '_');
			searchValue = searchValue.toLowerCase();
			if(searchValue != ""){
				$(".consent-select-box li").hide();
				$(".consent-select-box li[class*="+searchValue+"]").show();
				console.log(searchValue)
			}else{
				$(".consent-select-box li").show();
			}
	});
	//<!--Save consent in database
		
	$('#reportBeat').on('submit',(function(e) {
		e.preventDefault();
		if ($('#reportBeat').valid()){		
			var formData = new FormData(this);
			var beat_listing= parseInt($("#beat_listing").text());
			$(".loader-model").show()
			$(".loaderOverlayBg").show()
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				cache:false,
				contentType: false,
				dataType:"json",
				processData: false,
				success:function(data){				
					$('#reportBeat')[0].reset();
					if(beat_listing == 1){
						window.location = window.location.href;
					}else{
						if(data.error_code == 0 || data.error_code == 1){
							window.location = "/beats";
						}else{
							window.location = window.location.href;
						}
					}
				},
				error: function(data){
					console.log("error");
					console.log(data);
				}
			});
		}
	}));

	$("#report-Beat button.close").on( "click", function() {
		 $('#reportBeat')[0].reset();		
	});
	//Add/Edit beat validation messages

	$(document).on('change','#addbeat #ms',function(e) {
		cat = $("#ms").val();
		desc_valid_flag= 0;
		if(cat == null){
			desc_valid_flag= 0;
			if ($("#descr-err").length > 0){
				
			}else{
				$('.add-beat-error > :nth-child(1)').after('<label for="ms" id="descr-err" class="error-custom">Category is required</label>');
			}
		}else{
			$("#descr-err").remove();
			desc_valid_flag= 1;
		}	
		//jQuery('.articleContainer').isotope('reLayout');
	});

	$(document).on('change','#editbeat #ms',function(e) {
		cat = $("#ms").val();
		desc_valid_flag= 0;
		if(cat == null){
			desc_valid_flag= 0;
			if ($("#descr-err").length > 0){
				
			}else{
				$('.add-beat-error > :nth-child(1)').after('<label for="ms" id="descr-err" class="error-custom">Category is required</label>');
			}
		}else{
			$("#descr-err").remove();
			desc_valid_flag= 1;
		}	
	});

	//<!-- Add/Edit beat validation messages -->
	
	// <--ajax to update seen notifications.-->
	$(document).on('click','.notify .item-text a',function(e) {
		var notify_id = $(this).attr("notify_id");
		$.ajax({
			url: "/frontusers/update_notification",
			type: "post",
			dataType: "json",
			data: {'notification_id':notify_id},
			async: false,
			success: function(d) {
				console.log(d)
			}
		});
	});
	// <!-- ajax to update seen notifications.-->
		
	//Like beat comment ajax
	var processing = true;
	$(document).on('click','.article-detail-comment h6 a img',function(e) {
		var comment_id = $(this).attr("comment_id");
		if(processing){
			processing = false;		
		$.ajax({
			url: "/frontbeats/like_comment",
			type: "post",
			dataType: "json",
			data: {'comment_id':comment_id},
			async: false,
			success: function(d) {
				previous_count =  $("#comment-id-"+comment_id+" > h6 span").text();
				if(d.error_code == 1){
					$("#comment-id-"+comment_id+" > h6 span").text(parseInt(previous_count)+1);
					$("img[comment_id="+comment_id+"]").attr('src','/latest_theme_assets/images/likes.png');
					$("img[comment_id="+comment_id+"]").parent().attr('title','Unlike');
				}else if(d.error_code == 2){
					$("#comment-id-"+comment_id+" > h6 span").text(parseInt(previous_count)-1);
					$("img[comment_id="+comment_id+"]").attr('src','/latest_theme_assets/images/beat_like.png');
					$("img[comment_id="+comment_id+"]").parent().attr('title','Like');
				}else{
					window.location = "/";
				}
				processing = true;			
			}
		});
		}
	});
	//<!--Like beat comment ajax
	
	//<!--Follow/unfollow User ajax
	$(document).on('click','.follow-list .item-follow-btn a',function(e) {
		var followed_id = $(this).attr("followed_id");
			$.ajax({
				url:  "/frontusers/follow",
				type: "post",
				//async: false,
				data: {'followed_id':followed_id},
				success: function(d) {
					following_count = $(".following_count_conatainer #following_count").text();	
					if(d == 1){
										
						$("a#a-"+followed_id).text("Following");						
						$("a#a-"+followed_id).addClass("following-btn");	
						$("a#a-"+followed_id).attr("title","Unfollow");	
						$(".following_count_conatainer #following_count").text(parseInt(following_count)+1);						
					}else if(d == 4){
						$("a#a-"+followed_id).text("Follow");
						$("a#a-"+followed_id).removeClass("following-btn");
						$("a#a-"+followed_id).attr("title","Follow");	
						$(".following_count_conatainer #following_count").text(parseInt(following_count)-1);	
					}					
				}
			});
		return true;
	});
	//<!--Follow/unfollow User ajax
	
	//<!--Add Beat ajax
	$('#addbeat').on('submit',(function(e) {
		e.preventDefault();
		cat = $("#ms").val();
		var desc_valid_flag= 0;	
		if ($('#addbeat').valid()){
			if(cat == null){
				desc_valid_flag= 0;
				if ($("#descr-err").length > 0){
				}else{
					$('.add-beat-error > :nth-child(1)').after('<label for="ms" id="descr-err" class="error-custom">Category is required</label>');
				}
			}else{
				$("#descr-err").remove();
				desc_valid_flag= 1;
			}		
			if(desc_valid_flag == 1){
				$("#addbeat #share").attr("disabled",true);
				$.ajax({
					type:'POST',
					url: $(this).attr('action'),
					data: $('#addbeat').serialize(),
					success:function(data){
						$('#addbeat')[0].reset();
						$('#beatvideossform')[0].reset();
						$('#beatimagesform')[0].reset();
						$('#addbeat .ms-choice > span').text('Choose Category');					
						$('.upload-preview-box .upload-preview').html('');
						$('.upload-img-box .upload-thumb > img#beat-image-preview').attr('src','/latest_theme_assets/images/upload-1.jpg');
						$('.upload-preview-box .upload-preview-files').html('');
						$('#attach-upload-filename').html('no file selected');	
						$('input[name=consent_ids]').attr('checked', false);	
						$(".beat-mess-box span").text("Beat added successfully. Redirecting...");
						$(".beat-mess-box span").css("color","green");
						$(".beat-mess-box").show();
						setTimeout(function(){ $(".beat-mess-box").hide('slow');
							window.location = window.location.href;
							 }, 5000);
					}
				});
			}	
		}else{
			if(cat == null){
				desc_valid_flag= 0;
				if ($("#descr-err").length > 0){
					
				}else{
					$('.add-beat-error > :nth-child(1)').after('<label for="ms" id="descr-err" class="error-custom">Category is required</label>');
				}
			}else{
				$("#descr-err").remove();
				desc_valid_flag= 1;
			}			
		}
	}));
	//<!-- Add Beat ajax
});	

$(window).load(function(){
	var $container = jQuery('.articleContainer');
	$container.imagesLoaded( function(){
	  $container.isotope({
			masonry: {
			//columnWidth: 240
			},
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
	  });
	});

	jQuery(".galleryWrapper").css("width","100%");
	
	//<-- Get notifications ajax	

	
	if(CURRENT_CONTROLLER != "Staticservices" ){
		if ((CURRENT_ACTION == "login") || (CURRENT_ACTION == "signup" )) {
		} else {
			$.ajax({
				url: "/frontusers/isActive",
				type: "post",
				async: false,
				data: {'current':CURRENT_CONTROLLER},
				success: function(d) {
					if (d == 0){	
						window.location = "/";	
					}	
				}
			});
		}
	}
    
    $.ajax({
		url: "/frontusers/get_notification_count",
		type: "post",
		dataType: "json",
		async: false,
		success: function(d) {
			if (d.error_code == 1){	
				$(".navbar-nav .notification-alert").show();
				$(".navbar-nav .notification-alert").text(d.data);						
			}else{
				$(".navbar-nav .notification-alert").hide();		
			}	
		}
	});	    
	setInterval(function(){ 
		if(CURRENT_CONTROLLER != "Staticservices"){
			if ((CURRENT_ACTION == "login") || (CURRENT_ACTION == "signup" )) {
			} else {
				$.ajax({
					url: "/frontusers/isActive",
					type: "post",
					async: false,
					data: {'current':CURRENT_CONTROLLER},
					success: function(d) {
						if (d == 0){	
							window.location = "/";	
						}	
					}
				});
			}
		}
        
        
		$.ajax({
			url: "/frontusers/get_notification_count",
			type: "post",
			dataType: "json",
			async: false,
			success: function(d) {
				if (d.error_code == 1){	
					$(".navbar-nav .notification-alert").show();
					$(".navbar-nav .notification-alert").text(d.data);						
				}	
			}
		});
		
		
	}, 20000);
	//<!-- Get notifications ajax	
});

$(document).ready(function (e) {

//<!-- image name show/hide on select-->
$("#attach-upload-button-select").on('change',function(){
	 var fileName = $("#attach-upload-button-select").val();
	 $("#attach-upload-filename").html('<span>'+this.files[0].name +  '</span><a href="javascript:void(0);" id="remove-attach" style="color:red">    X </a></div>');
});

$( ".upload-txt" ).on( "click", '#remove-attach' ,function() {
	 $("#attach-upload-button-select").val('');
	$("#attach-upload-filename").text("no file selected");
});


$("#file-upload-button").on('change',function(){
	 var fileName = $("#file-upload-button").val();
	 $("#file-upload-filename").html('<span>'+this.files[0].name +  '</span><a href="javascript:void(0);" id="remove-consent-attach" style="color:red">    X </a></div>');
});

$( ".file-upload-wrap" ).on( "click", '#remove-consent-attach' ,function() {
	 $("#file-upload-button").val('');
	 $("#consent-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
	$("#file-upload-filename").text("no file selected");
});

///////// for consent signature //////////
$("#signature-upload-button").on('change',function(){
	 var fileName = $("#signature-upload-button").val();
	 $("#signature-upload-filename").html('<span>'+this.files[0].name +  '</span><a href="javascript:void(0);" id="remove-consent-sign" style="color:red">    X </a></div>');
});

$( ".file-upload-wrap" ).on( "click", '#remove-consent-sign' ,function() {
	 $("#signature-upload-button").val('');
	 $("#signature-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
	$("#signature-upload-filename").text("no file selected");
});
///////// end for consent signature //////////

$("#file-upload-button-select").on('change',function(){
	 $("#file-upload-image-filename").html('<span>'+this.files[0].name +  '</span><a href="javascript:void(0);" class="remove-beatimage" id="remove-image" style="color:red">    X </a></div>');
});

$( ".file-upload-wrap" ).on( "click", '#remove-image' ,function() {
	 $("#file-upload-button-select").val('');
	 $("#beat-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
	$("#file-upload-image-filename").text("no file selected");
});

//<!--// image name show/hide on select-->


//<!-- upload/unlink beat attachment-->
$('#beatfilesform').on('submit',(function(e) {
	e.preventDefault();
	var formData = new FormData(this);
	var fileName = $("#attach-upload-button-select").val();
	if ($('#beatfilesform').valid()){
		if(fileName) { 
			$(".loader-model").show()
			$(".loaderOverlayBg").show()	
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				cache:false,
				contentType: false,
				processData: false,
				dataType:"json",
				success:function(data){
					console.log(data);
					value = data.value;
					x = value.substr(0, value.lastIndexOf('.'));	
	$('.upload-preview-files').append('<li id="li'+ x +'"><a href="'+BUCKET_URL+'/img/articleuploads/'+data.value+'" >'+data.display_name+'</a><a class="remove-beat" title="Remove" id="'+data.value + '" href="javascript:void(0);">x</a></li>');
					$('#beatattachmentdb').val(data.value+'-sep-'+data.display_name);
					$('.attach-btn').hide();
					$("#attach-upload-button-select").val('');
					$("#attach-upload-filename").text("no file selected");	
                    $("#beatfilesform .upload-img-box").hide();
                    $(".loader-model").hide()
					$(".loaderOverlayBg").hide()	
				},
				error: function(data){
					console.log("error");
					console.log(data);
					$(".loader-model").hide()
					$(".loaderOverlayBg").hide()	
				}
			});
		} else { 
			$("#attach-upload-filename").text("no file selected");
		}
	}
}));


$( document ).on( "click", '.upload-preview-files .remove-beat' ,function() {
	if (confirm("Are you sure you want to remove this attachment/file?") == true) {	
		var values = this.id;		
		$.ajax({
			url: "/Frontbeats/unlink_attachment/",
			type: "post",
			async: false,
			data: {'files':values},
			success: function(d) {
					$(d).remove();
					$("#beatattachmentdb").val("");
					$('.attach-btn').show();
					$("#beatfilesform .upload-img-box").show();
			}
		});
	}
});


//".owl-wrapper-outer"
$( document ).on( "click", '.owl-item a.delete' ,function() {
		if (confirm("Are you sure you want to remove this attachment/file?") == true) {	
			if($(this).hasClass("remove-beat")){
				
				var values = this.id;		
				var li_id = $(this).attr("delete_id");	
				console.log(li_id)
				$.ajax({
					url: "/Frontbeats/unlink_files/",
					type: "post",
					async: false,
					data: {'files':values},
					success: function(d) {
							$('#li'+d).remove("");
							var text = $("#beatimagesdb").val();
							text = text.replace(values, "");
							$("#beatimagesdb").val(text);
							filter = "owl-item-"+d;
							console.log(filter);
							var player  = $('.item'),
							current = player.filter('#'+filter),
							owlNumber = current.index('.item');					
							 var owl = $("#owl-demo"),
							i = 0,
							textholder,
							booleanValue = false;
							owl.data('owlCarousel').removeItem(owlNumber);
					}
				});
				$(".upload-preview > li#"+li_id).remove();
			}else{
				var uploadedid = this.id;
						
					if ($( "#div-"+uploadedid).hasClass( "attachment-cat" )){
						$(".attachment-view").attr('data-target','#beat-files');
					}	
					if ($( "#div-"+uploadedid).hasClass( "videos-cat" )){
						$(".video-view").attr('data-target','#beat-videos');
					}	
					$('#div-'+uploadedid).remove();
					var $tobedeleted = $('#tobedeleted');	
					$tobedeleted.val($tobedeleted.val() + '-sep-' + uploadedid);			
			}	
		}
});


$( document ).on( "click", '#editbeat a.attachment-view' ,function() {
	if($(".attachment-view").attr('data-target') == "#"){
	alert("File already exists.");
	}
});

$( document ).on( "click", '#editbeat a.video-view' ,function() {
	if($(".video-view").attr('data-target') == "#"){
		alert("Video already exists.");
	}
});

//<!-- // upload/unlink beat attacgment-->

//<!-- save/update and valiadate beat video url-->

$("#videoYT1").prop('disabled', true);
$("#videoVm1").prop('disabled', true);
$(".upload-url").hide();

$( '#beatvideossform input[type="radio"]' ).on( "click",function() {
	$('#beatvideossform input[type="text"].video_name').val('');
	$('#beatvideossform input[type="text"].video_name').prop('disabled', true);
	$(this).next().next().prop('disabled', false);
	$(".upload-url").show();
});



$('#beatvideossform').on('submit',(function(e) {
	e.preventDefault();
	if ($('#beatvideossform').valid()){
		$("#beat-video-error").text("");		
		$("#beat-video-error").show();
		if($('input[type=radio]:checked').size() > 0){
			value = $("input[type=radio]:checked").val();
			if(value == "youtube"){						
				videovalue = $("#videoYT1").val();
				var n = videovalue.indexOf("youtube.com/"); 
			}else{
				videovalue = $("#videoVm1").val();
				var n = videovalue.indexOf("vimeo.com/"); 
			}
			if(n > 0){		
				$('#beatvideossform .select-video input[type="radio"]').attr("disabled",true);				
				$('#beatvideossform .select-video input[type="text"]').prop('readonly', true);			
				$('#beatvideossform #btn-upload').hide();				
				$("#beatvideodb").val(videovalue);				
				$("#beat-video-error").css("color","green");		
				$("#beat-video-error").text("Video URL added successfully.");					
				$("#remove-video").show();			
				setTimeout(function(){ $("#beat-video-error").hide('slow'); }, 3000);
			}else{
				$("#beat-video-error").css("color","red");
				$("#beat-video-error").text("Enter valid video url");	
				setTimeout(function(){ $("#beat-video-error").hide('slow'); }, 3000);			
				return false;
			}
			
		}	
	}
	
}));
beatvideodb = $('#beatvideodb').val();
if(beatvideodb != ""){
	$("#remove-video").show();	
}
 $('#remove-video').click(function(){
	$('#beatvideodb').val("");
	$('#beatvideossform')[0].reset();
	$("#beat-video-error").css("color","red");		
	$("#beat-video-error").text("Video URL has removed.");	
	$("#beat-video-error").show();
	
	$('#beatvideossform .select-video input[type="radio"]').attr("disabled",false);				
	$('#beatvideossform .select-video input[type="radio"]').removeClass("clicked");	
				
	$('#beatvideossform .select-video input[type="text"]').prop('readonly', false);			
	$('#beatvideossform .select-video input[type="text"]').attr("disabled",true);	
							
	$('#beatvideossform #btn-upload').show();	
	
	$('#remove-video').hide();	
	setTimeout(function(){ $("#beat-video-error").hide('slow'); }, 3000);
});
//<!-- // save/update beat video url-->

//	<!-- link consents -->
 $('.pull-right #btn-signup').click(function(){
	var checkValues = $('input[name=consent_ids]:checked').map(function()
	{
		return $(this).val();
	}).get();
	$("#consent_forms").val(checkValues);
	if($("#consent_forms").val() == ""){
		color = "red";
		mess = "You did not select any consent.";
	}else{
		color = "green";
		mess = "Consent Link updated.";		
	}	
	$('.consent-mess').css("color", color);
	$('.consent-mess').text(mess);
	setTimeout(function(){ $(".consent-mess").text(''); }, 3000);
});
 //<!-- // link consents-->
$(".urltolinks").not('.art-description .urltolinks').remove();
$('#beat-images .close').click(function(event){
	event.preventDefault();
	if($("#beatimagesform").length > 0) {
		$("#beatimagesform")[0].reset();	
		$("#beat-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
		$("#beatimagesform .error").text("");
		$("#file-upload-image-filename").text("no file selected");	
	}

});

$('#beat-files .close').click(function(event){
event.preventDefault();
	$("#beatfilesform")[0].reset();	
	$("#beatfilesform .error").text("");
	$("#attach-upload-filename").text("no file selected");	
});
$('#consent-form .close').click(function(event){
event.preventDefault();
	$("#consent-frm")[0].reset();	
	$("#consent-frm .error").text("");
	$("#file-upload-filename").text("no file selected");
	$("#consent-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');
	$("#signature-upload-filename").text("no file selected");
	$("#signature-image-preview").attr('src','/latest_theme_assets/images/upload-1.jpg');	
});

$('li  a .comment').click(function(event){
        $(".add-comment textarea").focus();
});

$('#youtube1,#vimeo1').click(function(){
	if($(this).hasClass('clicked')){		
		$(this).prop('checked',false);
		$("#beatvideossform input[type=radio]").removeClass('clicked');		
		$(this).removeClass('clicked');	
		$("#videoYT1").prop('disabled', true);
		$("#videoVm1").prop('disabled', true);
	}else{
		$('#beatvideossform').valid();
		$("#beatvideossform input[type=radio]").removeClass('clicked');		
		$(this).addClass('clicked');		
	}				
});

$('#youtube,#vimeo').click(function(){
	if($(this).hasClass('clicked')){		
		$(this).prop('checked',false);
		$("#consent-frm input[type=radio]").removeClass('clicked');		
		$(this).removeClass('clicked');	
		$('#consent-frm input[type="text"].video_name').val('');
		$("#videoYT").prop('disabled', true);
		$("#videoVm").prop('disabled', true);
	}else{
		$("#videoYT").prop('disabled', true);
		$("#videoVm").prop('disabled', true);
		$("#consent-frm input[type=radio]").removeClass('clicked');		
		$(this).addClass('clicked');
		$('#consent-frm input[type="text"].video_name').val('');
		$(this).next().next().prop('disabled', false);		
	}				
});

$("#videoYT").prop('disabled', true);
$("#videoVm").prop('disabled', true);

$( document ).on( "click", 'a.delete-article' ,function() {
	if (confirm("Are you sure you want to delete this beat ?")) { 
		return true;
	} 
	return false;
});



$( "body" ).on( "click", '#beats-listing .article-listing .multiple-view li a' ,function(e) {
	e.preventDefault();
	var clicked_class = $(this).attr('class');
	if(!$(this).hasClass('fancybox')){	
		var parent_class = $(this).parent().attr('class');	
		var parent_data = parent_class.split('-');
		if(clicked_class == "img-view"){
			heading = "Uploaded Photos";
			filter = 1;
		}else if(clicked_class == "video-view"){
			heading = "Video Url";
			filter = 2;
		}else{
			heading = "Attachment";	
			filter = 3;	
		}
		$(".consent-beat h2").text(heading);		
		$(".loader-model").show();
		$(".loaderOverlayBg").show();
		$.ajax({
			url: "/Frontbeats/get_uploads/",
			type: "post",
			//async: false,
			data: {'article_id':parent_data[1],'filter':filter},
			success: function(d) {	
				$( '#beat-images-preview' ).css('z-index','999999');			
				$(".loader-model").hide();
				$(".loaderOverlayBg").hide();
				if(clicked_class == "video-view"){
					if (!d.trim()) {			
						alert("Data not found");
					}else{
						$("li."+parent_class+" a.video-view").addClass('fancybox');
						$("li."+parent_class+" a.video-view").attr('href', d);
					}
				}else{					
					if (!d.trim()) {			
						alert("Data not found");
						return false;
					}else{						
						$("#beat-images-preview .upload-preview-box").html(d);											
						if(clicked_class == "img-view"){
							$( 'body' ).addClass('modal-open');	
							$( '.loaderOverlayBg' ).show();
							$( '#beat-images-preview' ).show();
							$( '#beat-images-preview' ).addClass("in");
						}else if(clicked_class == "video-view"){							
						}else{
							$( 'body' ).addClass('modal-open');
							$( '.loaderOverlayBg' ).show();
							$( '#beat-images-preview' ).show();
							$( '#beat-images-preview' ).addClass("in");
						}
						$(".video-msg p").text("");
					}
				}				
			}
		});
		return false;
	}	
});

$( "body" ).on( "click", '#beat-images-preview .close' ,function(e) {
	$( 'body' ).removeClass('modal-open');
	$( '.loaderOverlayBg' ).hide();
	$( '#beat-images-preview' ).hide();
	$( '#beat-images-preview' ).removeClass( "in");
});

$( "body" ).on( "click", '.article-rating .plus' ,function(e) {
	e.preventDefault();
	$(".article-rating .plus, .article-rating .minus").prop("disabled",true);
	article = $(this).attr('beat-id');
	console.log("+ " + article)
	$.ajax({
		url: "/Frontbeats/like_beat/",
		type: "post",
		data: {'article':article},
		success: function(d) {
			var mess = "";
			if(d == 1){
				likecount = $('.like'+article).text();
				$('.like'+article).text(parseInt(likecount)+1);
				$('#al'+article).css('background','url(/latest_theme_assets/images/beat-rating-blue-left.jpg)  no-repeat');
			}else if(d == 2){
				likecount = $('.like'+article).text();
				dislikecount = $('.dislike'+article).text();
				$('.like'+article).text(parseInt(likecount)+1);
				$('.dislike'+article).text(parseInt(dislikecount)-1);
				$('#al'+article).css('background','url(/latest_theme_assets/images/beat-rating-blue-left.jpg)  no-repeat');
				$('#ad'+article).css('background','url(/latest_theme_assets/images/beat-rating-grey-left.jpg)  no-repeat');
			}else if(d == 3){
				likecount = $('.like'+article).text();
				$('.like'+article).text(parseInt(likecount)-1);
				$('#al'+article).css('background','url(/latest_theme_assets/images/beat-rating-grey-left.jpg)  no-repeat');
			}
			$(".article-rating .plus, .article-rating .minus").prop("disabled",false);
		}
	});
});

$( "body" ).on( "click", '.article-rating .minus' ,function(e) {
	e.preventDefault();
	$(".article-rating .plus, .article-rating .minus").prop("disabled",true);
	article = $(this).attr('beat-id');
	console.log("- " + article)
	$.ajax({
		url: "/Frontbeats/dislike_beat/",
		type: "post",
		data: {'article':article},
		success: function(d) {
			var mess = "";
			if(d == 1){
				dislikecount = $('.dislike'+article).text();
				$('.dislike'+article).text(parseInt(dislikecount)+1);
				$('#ad'+article).css('background','url(/latest_theme_assets/images/beat-rating-red-left.png)  no-repeat');
			}else if(d == 2){
				likecount = $('.like'+article).text();
				dislikecount = $('.dislike'+article).text();
				$('.like'+article).text(parseInt(likecount)-1);
				$('.dislike'+article).text(parseInt(dislikecount)+1);
				$('#ad'+article).css('background','url(/latest_theme_assets/images/beat-rating-red-left.png)  no-repeat');
				$('#al'+article).css('background','url(/latest_theme_assets/images/beat-rating-grey-left.jpg)  no-repeat');
			}else if(d == 3){
				dislikecount = $('.dislike'+article).text();
				$('.dislike'+article).text(parseInt(dislikecount)-1);
				$('#ad'+article).css('background','url(/latest_theme_assets/images/beat-rating-grey-left.jpg)  no-repeat');
			}
			$(".article-rating .plus, .article-rating .minus").prop("disabled",false);
		}
	});
});

});

function validatePhone(phone) {	
	var filter = /^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}$/;
	if (filter.test(phone)) {
		return true;
	}
	else {
		return false;
	}
}
