<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Stock
 */
?>

	</div><!-- #content -->

	<footer><!--footer-->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="footer-left">
          <ul>
            <li><a title="About Us" href="/about">About Us</a></li>
            <li><a title="Terms and Conditions" href="/terms-and-conditions">Terms and Conditions</a></li>
            <li><a title="Join the Team" href="/join-the-team">Join the Team</a></li>
          </ul>
          <ul>
            <li><a title="Privacy Policy" href="/privacy-policy">Privacy Policy</a></li>
            <li><a title="Frequently Asked Questions" href="/faq">FAQ's</a></li>
            <li><a title="Blog" href="/blog/">Blog</a></li>
			 
          </ul>
          <ul>
           <li><a title="Contact Us"  href="/contact" >Contact Us</a></li>
            <li><a title="Network Guidelines" href="/network-guidelines">Network Guidelines</a></li>
            <li><a title="Help and Support" href="mailto:support@theoncallroom.com">Help and Support</a></li>
          </ul>

        </div>
        <div class="footer-right">
        <div class="social-links">
					<ul>
						<li><a href="https://www.linkedin.com/company/4842839?trk=tyah&amp;trkInfo=idx%3A1-1-1%2CtarId%3A1425467003079%2Ctas%3Athe+on+call+room" title="LinkedIn" target="_blank"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/linked-icon.png" alt="LinkedIn"></a></li>
						<li><a href="https://www.facebook.com/TheOnCallRoom?ref=br_rs" title="Facebook" target="_blank"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/fb-icon.png" alt="Facebook"></a></li>
						<li><a href="https://twitter.com/theoncallroom" title="Twitter" target="_blank"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/tweets.png" alt="Twitter"></a></li>
					</ul>
				</div>
          <p>Copyright &copy; <a href="//mediccreations.com/" target="_blank"><img width="14" height="14" alt="Logo" src="<?php echo get_template_directory_uri(); ?>/images/medic-logo-small.png"> Medic Creations</a> All Rights Reserved. </p>
        </div>
      </div>
    </div>
  </div>
  <!--//footer--></footer>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
