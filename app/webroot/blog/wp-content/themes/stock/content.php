<?php
/**
 * @package Stock
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
		<div class="entry-footer">
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php stock_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
		<?php stock_entry_footer(); ?>
	</div><!-- .entry-footer -->
		
	</div><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( ' %s <span class="meta-nav"></span>', 'stock' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'stock' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	
	
</article><!-- #post-## -->
