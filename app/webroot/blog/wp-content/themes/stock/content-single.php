<?php
/**
 * @package Stock
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="<?php esc_attr_e( has_post_thumbnail() ? 'entry-header has-thumbnail' : 'entry-header' ) ?>">

		<?php the_title( '<h1 class="entry-title"><span>', '</span></h1>' ); ?>
		<div class="entry-meta">
			<?php stock_posted_on(); ?>
		</div><!-- .entry-meta -->
	<div class="entry-footer">
		<?php stock_entry_footer(); ?>
	</div><!-- .entry-footer -->
		<?php
		if ( has_post_thumbnail() ) {
			$image   = get_the_post_thumbnail();
			$pattern = '/src="([^"]*)"/';

			preg_match( $pattern, $image, $matches );

			printf( '<div class="entry-thumbnail" style="background-image: url(%s);"></div>', esc_attr( $matches[1] ) ); //xss ok
		}
		?>

		

	</div><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'stock' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
