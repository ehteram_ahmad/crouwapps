<?php

App::uses('Model', 'Model');

/**
 * Represents model UserDndStatus
 */
class UserBatonRole extends Model
{

	/*
	-----------------------------------------------------------
	On: 06-03-2019
	I/P: $userId
	O/P: array
	Desc: fetch user all baton roles
	-----------------------------------------------------------
	*/
	public function getUserBatonRoleDetail($userId = NULL){
		$returnData = array();
		$conditions = array("UserBatonRole.from_user"=> $userId , "UserBatonRole.is_active"=> 1);
		$returnData = $this->find('all',
							array(
								'joins'=>
								array(
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfile',
										'type' => 'left',
										'conditions'=> array('UserBatonRole.from_user = UserProfile.user_id')
									),
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfiles',
										'type' => 'left',
										'conditions'=> array('UserBatonRole.to_user = UserProfiles.user_id')
									),
									array(
										'table' => 'user_duty_logs',
										'alias' => 'UserDutyLog',
										'type' => 'left',
										'conditions'=> array('UserProfiles.user_id = UserDutyLog.user_id')
									),
									array(
										'table' => 'departments_baton_roles',
										'alias' => 'DepartmentsBatonRole',
										'type' => 'left',
										'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
									),
									array(
										'table' => 'baton_roles',
										'alias' => 'BatonRole',
										'type' => 'left',
										'conditions'=> array('DepartmentsBatonRole.role_id = BatonRole.id')
									)
								),
								'fields'=> array("UserBatonRole.role_id", "UserBatonRole.from_user","UserBatonRole.to_user","UserBatonRole.role_id","UserBatonRole.status","UserBatonRole.notes","UserBatonRole.updated_at", "UserProfile.first_name","UserProfile.last_name","UserProfile.profile_img","UserProfile.thumbnail_img","UserProfiles.user_id","UserProfiles.first_name","UserProfiles.last_name","UserProfiles.profile_img","UserProfiles.thumbnail_img","BatonRole.id", "BatonRole.baton_roles","DepartmentsBatonRole.timeout","DepartmentsBatonRole.on_call_value","UserDutyLog.status","UserDutyLog.atwork_status"),
								'conditions' => $conditions,
								'group' => array('UserBatonRole.role_id'),
								'order'=> array('UserBatonRole.updated_at DESC')
							)
						);
		return $returnData;

    }


    /*
	-----------------------------------------------------------
	On: 06-03-2019
	I/P: $userId
	O/P: array
	Desc: fetch user all baton roles
	-----------------------------------------------------------
	*/
	public function getUserBatonRoleDetailOne($userId = NULL){
		$returnData = array();
		$conditions = array("UserBatonRole.from_user"=> $userId , "UserBatonRole.is_active"=> 1);
		$returnData = $this->find('all',
							array(
								'joins'=>
								array(
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfile',
										'type' => 'left',
										'conditions'=> array('UserBatonRole.from_user = UserProfile.user_id')
									),
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfiles',
										'type' => 'left',
										'conditions'=> array('UserBatonRole.to_user = UserProfiles.user_id')
									),
									array(
										'table' => 'user_duty_logs',
										'alias' => 'UserDutyLog',
										'type' => 'left',
										'conditions'=> array('UserProfiles.user_id = UserDutyLog.user_id')
									),
									array(
										'table' => 'departments_baton_roles',
										'alias' => 'DepartmentsBatonRole',
										'type' => 'left',
										'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
									),
									array(
										'table' => 'baton_roles',
										'alias' => 'BatonRole',
										'type' => 'left',
										'conditions'=> array('DepartmentsBatonRole.role_id = BatonRole.id')
									)
								),
								'fields'=> array("UserBatonRole.role_id", "UserBatonRole.from_user","UserBatonRole.to_user","UserBatonRole.role_id","UserBatonRole.status","UserBatonRole.notes","UserBatonRole.updated_at", "UserProfile.first_name","UserProfile.last_name","UserProfile.profile_img","UserProfile.thumbnail_img","UserProfiles.user_id","UserProfiles.first_name","UserProfiles.last_name","UserProfiles.profile_img","UserProfiles.thumbnail_img","BatonRole.id", "BatonRole.baton_roles","DepartmentsBatonRole.timeout","DepartmentsBatonRole.on_call_value", "UserDutyLog.status","UserDutyLog.atwork_status"),
								'conditions' => $conditions,
								'group' => array('UserBatonRole.role_id'),
								'order'=> array('UserBatonRole.updated_at DESC')
							)
						);
		return $returnData;

    }

    /*
    --------------------------------------------------------------------------
    On: 12-03-2019
    I/P:
    O/P: array
    Desc: fetch all users whose request or transfer interval is expired
    --------------------------------------------------------------------------
     */
    public function getUserForExpiration()
    {
        $returnData = array();
        $conditions = array(
            "UserBatonRole.is_active" => 1,
            "OR" => array(
                array("UserBatonRole.status" => 5),
                array("UserBatonRole.status" => 3)
            ),
            "UserBatonRole.updated_at + INTERVAL 1 HOUR < now()"
        );
        $returnData = $this->find('all',
            array(
                'fields' => array("UserBatonRole.id", "UserBatonRole.role_id", "UserBatonRole.from_user", "UserBatonRole.to_user", "UserBatonRole.status", "UserBatonRole.created_at"),
                'conditions' => $conditions,
            )
        );
        return $returnData;

    }

}
