<?php 
/*
Represent model for notification visited by user (last date).
*/
App::uses('AppModel', 'Model');

class NotificationLastVisit extends AppModel {
	
	/*
	----------------------------------------------------------------------------------------
	On: 04-04-2016
	I/P: 
	O/P: 
	Desc: Unread notification count after notification last visited date
	----------------------------------------------------------------------------------------
	*/
	public function unreadNotificationCountAfterLastVisited( $userId = NULL ){
		$unreadNotifications = 0;
		if(!empty($userId)){
			$lastVisitedDateData = $this->find("first", array("conditions"=> array("NotificationLastVisit.user_id"=> $userId)));
			//** Get total notification after last visited
			if(!empty($lastVisitedDateData)){
				$quer = "SELECT count(*) AS totUnreadNotification FROM `notification_last_visits` AS nlv LEFT JOIN notification_logs AS nl 
				ON nl.to_user_id = nlv.user_id WHERE nl.to_user_id = $userId AND nl.read_status = 0 AND  nl.created > nlv.last_visited_date";
			}else{
				$quer = "SELECT count(*) AS totUnreadNotification FROM `notification_logs` AS nl WHERE nl.read_status = 0 AND nl.to_user_id = $userId ";
			}
			$unreadNotifications = $this->query($quer);
		}
		return $unreadNotifications;
	}
}
?>