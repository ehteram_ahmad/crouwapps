<?php
/*
 * Colleague controller.
 *
 * This file will render views from views/Colleaguewebservices/
 *
 
 */

App::uses('AppController', 'Controller');


class Mbapiv2colleaguewebservicesController extends AppController {
	public $uses = array('Mbapiv2.User', 'Mbapiv2.UserProfile', 'Mbapiv2.UserColleague', 'Mbapiv2.NotificationUser', 'Mbapiv2.UserNotificationSetting', 'Mbapiv2.NotificationLog', 'Mbapiv2.UserFollow', 'Mbapiv2.NotificationLastVisit', 'UserQbDetail', 'Country', 'Profession', 'Mbapiv2.UserEmployment', 'Mbapiv2.CompanyName','Mbapiv2.UserDutyLog', "Mbapiv2.ApiRequestResponseTrack", 'Mbapiv2.EnterpriseUserList', "Mbapiv2.StaticRoleTag", "Mbapiv2.Specilities", "Mbapiv2.RoleTag", "Mbapiv2.PatientHcpAssociation","Mbapiv2.UserQbDetail");
	public $components = array('Common','Quickblox');
	
	/*
	------------------------------------------------------------------------------------------------
	On: 29-06-2016
	I/P: JSON (logggedin user id and userid of the user invited as a colleague)
	O/P: JSON (message of success or Fail)
	Desc: Invite as colleague by any user.
	------------------------------------------------------------------------------------------------
	*/
	public function inviteColleague(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true); 
			$checkColleagueStatus = $this->User->find("count", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'], "status"=> 1)));
			$checkUserStatus = $this->User->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "status"=> 1)));
			$loggedinUserId = $dataInput['user_id'];
			$colleagueUserId = $dataInput['colleague_user_id'];
			if($checkUserStatus > 0)
			{
				if($checkColleagueStatus > 0)
				{
					if( $this->tokenValidate() && $this->accesskeyCheck() ){
						$userDetails = $this->User->findById( $dataInput['user_id']); 
						if( !empty($userDetails)){ //** Check logged in user
							$colleagueDetails = $this->User->findById( $dataInput['colleague_user_id'] );
							if( !empty($colleagueDetails)){ //** Check colleague user
								if(!empty($dataInput['app_version'])){
									if($userDetails['UserProfile']['country_id'] == $colleagueDetails['UserProfile']['country_id'] ){
								//** Send colleague request
								$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'])));
								$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id'])));
									$colleagueData = array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id']);
									try{
										if( $myColleague == 0 && $meColleague == 0 ){ //** Check if already colleague
											$inviteColleague = 1;
											$inviteColleague = $this->UserColleague->save( $colleagueData );
											$sendCollegueNotifyUserId = $dataInput['colleague_user_id'];
										}else{
											$inviteColleague = 0;
											if( $meColleague > 0){
												//$colleagueData = array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id'] );
												$checkColleagueStatus = $this->UserColleague->find("first", array("conditions"=> $colleagueData)); 
												if( $checkColleagueStatus['UserColleague']['status'] == 1){
													$responseData = array('method_name'=> 'inviteColleague', 'status'=>"0", 'response_code'=> "626", 'message'=> ERROR_626);
												}elseif( $checkColleagueStatus['UserColleague']['status'] == 2 ){
													$responseData = array('method_name'=> 'inviteColleague', 'status'=>"0", 'response_code'=> "630", 'message'=> ERROR_630);
												}elseif( ($checkColleagueStatus['UserColleague']['status'] == 3) || ($checkColleagueStatus['UserColleague']['status'] == 0) ){
													//$inviteColleague = $this->UserColleague->updateAll( array("status"=> 2), $colleagueData );
													$inviteColleague = $this->UserColleague->updateAll(array("status"=> 2), $colleagueData );
												}
												$sendCollegueNotifyUserId = $dataInput['colleague_user_id'];
											}else{
												$colleagueData = array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id'] );
												$checkColleagueStatus = $this->UserColleague->find("first", array("conditions"=> $colleagueData, "fields"=> array("UserColleague.id", "UserColleague.user_id", "UserColleague.colleague_user_id", "UserColleague.status", "UserColleague.created")));
												
												if( $checkColleagueStatus['UserColleague']['status'] == 1){
													$responseData = array('method_name'=> 'inviteColleague', 'status'=>"0", 'response_code'=> "626", 'message'=> ERROR_626);
												}elseif( $checkColleagueStatus['UserColleague']['status'] == 2 ){
													$responseData = array('method_name'=> 'inviteColleague', 'status'=>"0", 'response_code'=> "630", 'message'=> ERROR_630);
												}elseif( ($checkColleagueStatus['UserColleague']['status'] == 3) || ($checkColleagueStatus['UserColleague']['status'] == 0) ){
													//$inviteColleague = $this->UserColleague->updateAll( array("status"=> 2), $colleagueData );
													$inviteColleague = $this->UserColleague->updateAll( array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'],"status"=> 2), array("id"=> $checkColleagueStatus['UserColleague']['id']) );
													
												}
												$sendCollegueNotifyUserId = $dataInput['colleague_user_id'];
											}
										}
										if( $inviteColleague ){
											$responseData = array('method_name'=> 'inviteColleague', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
											
										}
										/*else{
											$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
										}*/
									}catch( Exception $e ){
										$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
									}
									//** If any user send colleague request also add user follow
									try{
										$conditions = array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id']);
										$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
										if($userFollowCheck == 0 ){
											$this->UserFollow->save( array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id'], "follow_type"=> 1, "status"=> 1) );
										}else{
											$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
										}
									}catch( Exception $e ){}
									
								/*}else{
									$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=> "626", 'message'=> ERROR_626);
								}*/
				    		 }else{
				    			$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'642', 'message'=> ERROR_642);
				    		}
				    		}else{
				    			$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'640', 'message'=> ERROR_640);
				    		}
				    		}else{
				    			$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'625', 'message'=> ERROR_625);
				    		}

				    	}else{
				    		$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
				    	}
				    }else{
			         $responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
		        }else{
			         $getUserStatusResponse = $this->getUserStatus($loggedinUserId, $loggedinUserId);
		        $responseData = array('method_name'=> 'colleagueCheck','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
		         }
		    }	
		    else
		    {
		    	$getUserStatusResponse = $this->getUserStatus($loggedinUserId, $loggedinUserId);
		        $responseData = array('method_name'=> 'colleagueCheck','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
	        }
	       }else{
			$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 30-06-2016
	I/P: JSON Data ( user id, colleague id with status )
	O/P: JSON data ( Success or Fail message )
	Desc: Change status of colleague like accept, deny etc.
	------------------------------------------------------------------------------------------------
	*/
	public function colleagueStatusChange(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					$userDetails = $this->User->findById( $dataInput['user_id'] );
					if(!empty($userDetails)){ //** Check logged in user
						$checkColleagueStatus = $this->User->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "status"=> 1)));
						if($checkColleagueStatus > 0)
						{
							$colleagueDetails = $this->User->findById( $dataInput['colleague_user_id'] );
							if(!empty($colleagueDetails)){ //** Check colleague user
								if(!empty($dataInput['app_version'])){
									if($userDetails['UserProfile']['country_id'] == $colleagueDetails['UserProfile']['country_id'] ){
								//$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'])));
								$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'])));
									if( $meColleague > 0){
										$colleagueCondition = array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id']);
									}else{
										$colleagueCondition = array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id']);
									}
									try{
										$updateColleague = $this->UserColleague->updateAll( array("status"=> (int) $dataInput['colleagueStatus']), $colleagueCondition );
										if( $updateColleague ){
											$responseData = array('method_name'=> 'colleagueStatusChange', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
											//** If any user accept colleague request auto follow[START]
												try{
													if($dataInput['colleagueStatus'] == 1){ //** Follow only in case of accept(not reject or delete)
														$conditions = array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id']);
														$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
															if($userFollowCheck == 0 ){
															$this->UserFollow->save( array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id'], "follow_type"=> 1, "status"=> 1) );
															}else{
																$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
															}
													}
												}catch( Exception $e ){}
											//** If any user accept colleague request auto follow[END]
										}else{
											$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
										}
									}catch( Exception $e ){
										$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
									}
				    			}else{
				    				$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'642', 'message'=> ERROR_642);
				    			}
							 }else{
				    			$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'640', 'message'=> ERROR_640);
				    		}
				    		}else{
				    			$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'625', 'message'=> ERROR_625);
				    		}
						}
						else
						{
							$getUserStatusResponse = $this->getUserStatus($dataInput['user_id'], $dataInput['user_id']);
		        			$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
						}

			    	}else{
			    		$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 28-06-2016
	I/P: JSON Data
	O/P: JSON data as colleague List
	Desc: Display all colleague list for any particular user.
	------------------------------------------------------------------------------------------------
	*/
	public function colleagueLists(){
		$responseData = array();
		$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if( $this->User->findById( $dataInput['user_id'] ) ){ //** Check logged in user
						//** Code goes here
						$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
						$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
						$params['user_id'] = isset($dataInput['user_id']) ? $dataInput['user_id'] : DEFAULT_PAGE_SIZE;
						$params['colleagueStatus'] = isset($dataInput['colleagueStatus']) ? implode(",", $dataInput['colleagueStatus']) : 1;
						$colleagueRequestBy = isset($dataInput['request_by']) ? $dataInput['request_by'] : "all";
						$colleagueList = $this->colleagueData( $params, $colleagueRequestBy );
						if( !empty($colleagueList) ){
							$colleagueListData = $this->colleagueFields( $colleagueList );
							//** Total records and per page size
							$totColleagueCount = $this->colleagueDataCount($params, $colleagueRequestBy);
							$colleagueList = array('Colleagues'=> $colleagueListData, 'total_records'=> $totColleagueCount, 'size'=> $params['size']);
							$responseData = array('method_name'=> 'colleagueLists','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $colleagueList);
			    		}else{
			    			$responseData = array('method_name'=> 'colleagueLists','status'=>'0','response_code'=>'613', 'message'=> ERROR_613);
			    		}
			    	}else{
			    		$responseData = array('method_name'=> 'colleagueLists','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'colleagueLists','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'colleagueLists','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
	    //echo json_encode($responseData);
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		//** Track API Request, Response[START]
		$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$headerVals = $this->getHeaderValues();
		$trackData = array(
							"user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> ($endTime - $startTime),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		//** Track API Request, Response[END]
    	exit;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 28-06-2016
	I/P: array()
	O/P: array() of colleague list
	Desc: Formatting colleague list Data.
	---------------------------------------------------------------------------------------
	*/
	public function colleagueFields( $colleagueList = array() ){
		$colleagueListData = array();
		foreach( $colleagueList as $colD ){ 
			//** Get QB details [START]
			$qbInfo = array(); $qbDetails = array();
			$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $colD['UserColleague']['colleague_user_id'])));
			$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
			$qbInfo = array("id"=> $qbId);
			//** Get QB details [END]
			if(!empty($qbDetails)){ //** If user has not QB details don't include in list
				$colleagueData = array(); $colleagueProfile = array();
				//** Colleague info
				$colleagueData['colleague_user_id'] = !empty($colD['UserColleague']['colleague_user_id']) ? $colD['UserColleague']['colleague_user_id'] : '';
				$colleagueData['status'] = !empty($colD['UserColleague']['status']) ? $colD['UserColleague']['status'] : '';
				//** Colleague profile Info[START]
				$colleagueProfile['user_id'] = !empty($colD['UserProfile']['user_id']) ? $colD['UserProfile']['user_id'] : '';
				$colleagueProfile['email'] = !empty($colD['User']['email']) ? $colD['User']['email'] : '';
				$colleagueProfile['first_name'] = !empty($colD[0]['first_name']) ? $colD[0]['first_name'] : '';
				$colleagueProfile['last_name'] = !empty($colD['UserProfile']['last_name']) ? $colD['UserProfile']['last_name'] : '';
				$colleagueProfile['role_status'] = !empty($colD['UserProfile']['role_status']) ? $colD['UserProfile']['role_status'] : '';
				$colleagueProfile['profile_img'] = !empty($colD['UserProfile']['profile_img']) ? AMAZON_PATH . $colD['UserProfile']['user_id']. '/profile/' . $colD['UserProfile']['profile_img'] : '';
				$colleagueProfile['country'] = !empty($colD['Country']['country_name']) ? $colD['Country']['country_name'] : '';
				$colleagueProfile['country_code'] = !empty($colD['Country']['country_code']) ? $colD['Country']['country_code'] : '';
				$colleagueProfile['profession'] = !empty($colD['Profession']['profession_type']) ? $colD['Profession']['profession_type'] : '';
				$colleagueProfile['at_work'] = $this->UserDutyLog->getStatusInfo($colleagueProfile['user_id']); 
				//** Colleague profile Info[END]
				
				//** Colleague info array
				$colleagueListData[] = array('Colleague'=> $colleagueData, 'UserProfile'=> $colleagueProfile, 'qb_details'=> $qbInfo);
			}
		}
		return $colleagueListData;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 28-06-2016
	I/P: array() as parameter
	O/P: array() of colleague data fetched from query
	Desc: Fetching all colleagues data from DB 
	---------------------------------------------------------------------------------------	
	*/
		public function colleagueData( $params = array(), $colleagueRequestBy = 'all' ){ 
			$colleagueList = array();
			if( !empty($params) ){
				App::import('model','UserColleague');
				$UserColleague = new UserColleague();
				$offsetVal = ( $params['page_number'] - 1 ) * $params['size'];
				if( !empty($params['name']) ){
						$conditions = ' AND User.status =  1 AND  User.approved = 1 AND ( LOWER(UserProfile.first_name) LIKE LOWER("%'.$params['name'].'%") ';
						//$conditions .= ' OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['name'].'%") ) LIMIT ' . $offsetVal . ',' . $params['size'];
						$conditions .= ' OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['name'].'%") ) ORDER BY UserProfile.first_name ASC ';	
				}else{
						//$conditions = " LIMIT " . $offsetVal . "," . $params['size'];
					$conditions = ' AND User.status =  1 AND  User.approved = 1 ORDER BY UserProfile.first_name ASC ';
				}
				if( $colleagueRequestBy == "from_me" ){
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT User.id, User.email, UserProfile.user_id, concat( upper(substring(UserProfile.first_name,1,1)),lower(substring(UserProfile.first_name,2)) ) AS  first_name, UserProfile.last_name , 
					UserProfile.profile_img, UserProfile.role_status, Country.country_name, Country.country_code ,Profession.profession_type,  UserColleague.colleague_user_id  , UserColleague.status 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					$colleagueList = $myColleague;
				}elseif( $colleagueRequestBy == "to_me" ){
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT User.id, User.email, UserProfile.user_id, concat( upper(substring(UserProfile.first_name,1,1)),lower(substring(UserProfile.first_name,2)) ) AS  first_name, UserProfile.last_name , 
					UserProfile.profile_img, UserProfile.role_status, Country.country_name, Country.country_code, Profession.profession_type, UserColleague.user_id colleague_user_id , UserColleague.status  
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueList = $meColleague;
				}else{
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT User.id, User.email, UserProfile.user_id, concat( upper(substring(UserProfile.first_name,1,1)),lower(substring(UserProfile.first_name,2)) ) AS  first_name, UserProfile.last_name , 
					UserProfile.profile_img, UserProfile.role_status, Country.country_name, Country.country_code, Profession.profession_type,  UserColleague.colleague_user_id  , UserColleague.status 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT User.id,User.email, UserProfile.user_id, concat( upper(substring(UserProfile.first_name,1,1)),lower(substring(UserProfile.first_name,2)) ) AS  first_name, UserProfile.last_name , 
					UserProfile.profile_img, UserProfile.role_status, Country.country_name, Country.country_code, Profession.profession_type, UserColleague.user_id colleague_user_id , UserColleague.status  
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueList = array_merge( $myColleague , $meColleague); 
				}
			}
			$slicedArr = $this->arraySort($colleagueList, 'first_name', SORT_ASC); //** Sort By First name
			$slicedArr = array_slice($slicedArr, $offsetVal, $params['size']); //** Fetch array size
			
			//echo "<pre>";print_r($slicedArr);die;
			return $slicedArr;
		}

	/*
	------------------------------------------------------------------------------------------------
	On: 28-06-2016
	I/P: JSON Data
	O/P: JSON data as colleague List
	Desc: Display all colleague list search by first_name, last_name of any particular user.
	------------------------------------------------------------------------------------------------
	*/
	public function colleagueSearch(){ 
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){

					if( $this->User->findById( $dataInput['user_id'] ) ){ //** Check logged in user
						//** Search colleague list
						$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
						$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
						$params['user_id'] = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
						$params['colleagueStatus'] = 1;
						$params['name'] = isset($dataInput['name']) ? $dataInput['name'] : '';
						$colleagueRequestBy = isset($dataInput['request_by']) ? $dataInput['request_by'] : 'all';
						$colleagueList = $this->colleagueData( $params, $colleagueRequestBy );
						$colleagueListData = $this->colleagueFields( $colleagueList );
						//** Total records and per page size
						$totColleagueCount = $this->colleagueDataCount($params, $colleagueRequestBy);
						$colleagueList = array('Colleagues'=> $colleagueListData, 'total_records'=> $totColleagueCount, 'size'=> $params['size']);
						$responseData = array('method_name'=> 'colleagueSearch','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $colleagueList);
			    	}else{
			    		$responseData = array('method_name'=> 'colleagueSearch','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'colleagueSearch','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'colleagueSearch','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
	    $encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}
	/*
	----------------------------------------------
	On: 
	I/P: 
	O/P:
	Desc: 
	----------------------------------------------
	*/
	public function arraySort($array, $on, $order=SORT_ASC)
	{
		$new_array = array();
    	$sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	            	foreach ($v as $k2 => $v2) {
	            		if (is_array($v2)) {
	                foreach ($v2 as $k3 => $v3) {
	                    if ($k3 == trim($on)) {
	                        $sortable_array[$k] = $v3;
	                    }
	                }
	              } else {
	                $sortable_array[$k2] = $v2;
	            	} 
	            } 
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        	
	        }
	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    	
	    }
	    return $new_array;
	}

	/*
	----------------------------------------------------------------------
	On: 22-04-2016
	I/P: 
	O/P: 
	Desc: Fetch total colleague count
	----------------------------------------------------------------------
	*/
	public function colleagueDataCount( $params = array(), $colleagueRequestBy = 'all' ){ 
			$colleagueListCount = 0;
			if( !empty($params) ){
				App::import('model','UserColleague');
				$UserColleague = new UserColleague();
				$conditions = "";
				if( !empty($params['name']) ){
						$conditions = ' AND User.status =  1 AND  User.approved = 1 AND ( LOWER(UserProfile.first_name) LIKE LOWER("%'.$params['name'].'%") ';
						$conditions .= ' OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['name'].'%") ) ORDER BY UserProfile.first_name ASC ';	
				}else{
					$conditions = ' AND User.status =  1 AND  User.approved = 1 ORDER BY UserProfile.first_name ASC ';
				}
				if( $colleagueRequestBy == "from_me" ){
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT count(*) AS totCnt 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					$colleagueListCount = $myColleague[0][0]['totCnt'];
				}elseif( $colleagueRequestBy == "to_me" ){
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT count(*)  AS totCnt  
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueListCount = $meColleague[0][0]['totCnt'];
				}else{
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT count(*)  AS totCnt 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT count(*)  AS totCnt 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueListCount = $myColleague[0][0]['totCnt'] + $meColleague[0][0]['totCnt']; 
				}
			}
			return $colleagueListCount;
		}

	/*
	------------------------------------------------------------------------------------------------
	On: 07-07-2016
	I/P: JSON Data ( loggedin user id, colleague id  )
	O/P: JSON data
	Desc: check if any loggedin user's collegue
	------------------------------------------------------------------------------------------------
	*/
	public function colleagueCheck(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			$checkColleagueStatus = $this->User->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "status"=> 1)));
			$loggedinUserId = $dataInput['user_id'];
			$colleagueUserId = $dataInput['colleague_user_id'];
			if($checkColleagueStatus > 0)
			{
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if( $this->User->findById( $dataInput['user_id'] ) ){ //** Check logged in user
						if( $this->User->findById( $dataInput['colleague_user_id'] ) ){ //** Check colleague user
							$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id'], "status"=> 1)));
							$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'], "status"=> 1)));
							//** check colleague
							if( ($myColleague > 0) || ($meColleague > 0) ){ //** Check if already colleague
								$colleagueData["is_colleague"] = 1;
							}else{
								$colleagueData["is_colleague"] = 0;
							}
							//** Check user's Country
							$myProfile = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
							$colleagueProfile = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'])));
							if($myProfile['UserProfile']['country_id'] == $colleagueProfile['UserProfile']['country_id']){
								$colleagueData["is_same_country"] = 1;
							}else{
								$colleagueData["is_same_country"] = 0;
							}
							$responseData = array('method_name'=> 'colleagueCheck','status'=>'1','response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Colleague"=> $colleagueData));
			    		}else{
			    			$responseData = array('method_name'=> 'colleagueCheck','status'=>'0','response_code'=>'625', 'message'=> ERROR_625);
			    		}

			    	}else{
			    		$responseData = array('method_name'=> 'colleagueCheck','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'colleagueCheck','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
			}	
		    else
		    {
		    	$getUserStatusResponse = $this->getUserStatus($loggedinUserId, $loggedinUserId);
		        $responseData = array('method_name'=> 'colleagueCheck','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
	        }
	       }else{
			$responseData = array('method_name'=> 'colleagueCheck','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 01-08-2016
	I/P: JSON Data ( loggedin user id, contacts like mobile,email  )
	O/P: JSON data
	Desc: check suggested colleagues according to contact of user's email/mobile
	------------------------------------------------------------------------------------------------
	*/
	public function suggestedColleague(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$loggedinUserId = "";$loggedinUserDetails = array();
				$loggedinUserId = $dataInput['loggedin_user_id'];
				$loggedinUserDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId), "fields"=> array("User.id", "UserProfile.country_id")));
				$loggedinUserCountryId = $loggedinUserDetails['UserProfile']['country_id'];
				//** suggested colleague data with phone number
				$suggestedColleaguesPhone = array(); $suggestedColleaguesIdByPhone = array();
				foreach($dataInput['contacts'] as $contacts){
					if(!empty($contacts['phoneNumber'])){
						$suggestedColleaguesIdPhoneMatch = $this->getSuggestedColleaguesByContactNumber($contacts['phoneNumber'], $loggedinUserId, $loggedinUserCountryId);
						if(!empty($suggestedColleaguesIdPhoneMatch)){
							$suggestedColleaguesIdByPhone[] = $suggestedColleaguesIdPhoneMatch;
						}
					}
				}
				//** suggested colleague data with Email
				$suggestedColleaguesEmail = array(); $suggestedColleaguesIdByEmail = array();
				foreach($dataInput['contacts'] as $contacts){
					if(!empty($contacts['emailAddress'])){
						$emailId = implode(",",$contacts['emailAddress']);
						$suggestedColleaguesIdEmailMatch = $this->getSuggestedColleaguesByEmail( strtolower($emailId), $loggedinUserId, $loggedinUserCountryId);
						if(!empty($suggestedColleaguesIdEmailMatch)){
							$suggestedColleaguesIdByEmail[] = $suggestedColleaguesIdEmailMatch;
						}
					}
				}
				//** Consolidated suggested colleagues
				$suggestedUserDetails = array();
				$consolidatedUserIds = array_unique(array_merge( $suggestedColleaguesIdByPhone, $suggestedColleaguesIdByEmail) );
				//** Remove self user
				foreach($consolidatedUserIds as $suggestedColleagueId){
					if(!empty($suggestedColleagueId)){
						$suggestedUserDetails[] = $this->User->userDetailsByUserId($suggestedColleagueId);
					}
				}
				$suggestedColleagueData = $this->userFieldsFormat($suggestedUserDetails, $loggedinUserId);
				//echo "<pre>"; print_r($suggestedColleagueData);die;
				$userData['SuggestedColleague'] = $suggestedColleagueData;
				if(!empty($suggestedColleagueData)){
					$responseData = array('method_name'=> 'suggestedColleague', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $userData);
				}else{
					$responseData = array('method_name'=> 'suggestedColleague', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'suggestedColleague', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'suggestedColleague', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 01-08-2016
	I/P: $contactNumbers
	O/P: $matchingColleagues
	Desc: Find all matching colleagues by phone number
	------------------------------------------------------------------------------------------------
	*/
	public function getSuggestedColleaguesByContactNumber($phoneNumber = NULL, $loggedinUserId = NULL, $loggedinUserCountryId = NULL){
		$suggestedColleagues = "";
		//foreach($contactNumbers as $phoneNumber){
			if(!empty($phoneNumber)){
				//** Suggested colleague according to contact number
				$suggestedUserDetails = $this->User->find("first", array("conditions"=> array("UserProfile.contact_no"=> $phoneNumber), "fields"=> array("User.id", "UserProfile.country_id")));
				if(!empty($suggestedUserDetails)){
					$suggestedUserId = $suggestedUserDetails['User']['id'];
					$suggestedUserCountryId = $suggestedUserDetails['UserProfile']['country_id'];
					//** find colleague status of both user
					$checkColleague = $this->UserColleague->isColleagueCheck($loggedinUserId, $suggestedUserId);
					if( empty($checkColleague) && ($loggedinUserCountryId == $suggestedUserCountryId) && ( $loggedinUserId !=$suggestedUserId) ){
						$suggestedColleagues = $suggestedUserId;
					}
				}
			}
		//}
		return $suggestedColleagues;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 01-08-2016
	I/P: $contactNumbers
	O/P: $matchingColleagues
	Desc: Find all matching colleagues by email
	------------------------------------------------------------------------------------------------
	*/
	public function getSuggestedColleaguesByEmail($email = NULL, $loggedinUserId = NULL, $loggedinUserCountryId = NULL){
		$suggestedColleagues = "";
		//foreach($emails as $em){
			if(!empty($email)){
				//** Suggested colleague according to contact number
				$suggestedUserDetails = $this->User->find("first", array("conditions"=> array("User.email"=> $email), "fields"=> array("User.id", "UserProfile.country_id")));
				if(!empty($suggestedUserDetails)){
					$suggestedUserId = $suggestedUserDetails['User']['id'];
					$suggestedUserCountryId = $suggestedUserDetails['UserProfile']['country_id'];
					//** find colleague status of both user
					$checkColleague = $this->UserColleague->isColleagueCheck($loggedinUserId, $suggestedUserId);
					if( empty($checkColleague) && ($loggedinUserCountryId == $suggestedUserCountryId) && ( $loggedinUserId !=$suggestedUserId) ){
						$suggestedColleagues = $suggestedUserId;
					}
				}
			}
		//}
		return $suggestedColleagues;
	}

	/*
	--------------------------------------------------------------------------
	On: 01-08-2016
	I/P: $userData = array(), $loggedinUserId = NULL
	O/P: $userDataList
	Desc: Fetching user list
	--------------------------------------------------------------------------
	*/
	public function userFieldsFormat( $userData = array(), $loggedinUserId = NULL ){
		$userDataList = array();
		if( !empty($userData) ){
		//** User Data	
			foreach( $userData as $ud ){
				if(!empty($ud)){
				//** Find user colleague with logged in user
				$colleagueStatus = 0; $requestType = '';
				$myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'])));
				$meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId)));
				if( !empty($myColleague) ){
					$colleagueStatus = $myColleague['UserColleague']['status'];
					$requestType = "from_me";
				}elseif( !empty($meColleague) ){
					$colleagueStatus = $meColleague['UserColleague']['status'];
					$requestType = "to_me";
				}
				//** Check loggedin user follows or not
				$isFollow = 0;
				$conditions = array("followed_by"=> $loggedinUserId, "followed_to"=> $ud['User']['id'], "follow_type"=> 1, "status"=> 1);
				$followCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
				if( $followCheck > 0) { $isFollow = 1; }
				//** Get Country name
				$countryDetails = $this->Country->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['country_id'])));
				//** Get Profession
				$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['profession_id'])));
				//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
				$qbInfo = array("id"=> $qbId);
				//** Get QB details [END]
				//** Get role status [START]
				$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				//** Get role status [END]
				//** Get Current Employment[START]
				$userCurrentCompanyName = "";
				$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "is_current"=> 1), "order"=> array("id DESC")));
				if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
					$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
					if(!empty($userCompanyDetails)){
						$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
					}
				}
				//** Get Current Employment[END]
				if(!empty($qbId)){
				$userDataList[] = array(
								'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
								'email'=>	!empty($ud['User']['email']) ? $ud['User']['email']:'',
								'last_loggedin_date'=> !empty($ud['User']['last_loggedin_date']) ? $ud['User']['last_loggedin_date']:'',
								'status' => !empty($ud['User']['status']) ? (string) $ud['User']['status']:'',
								'registration_date'=> !empty($ud['User']['registration_date']) ? (string) $ud['User']['registration_date']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? $ud['UserProfile']['first_name']:'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? $ud['UserProfile']['last_name']:'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'country'=> !empty($countryDetails['Country']['country_name']) ? $countryDetails['Country']['country_name']:'',
								'country_code'=> !empty($countryDetails['Country']['country_code']) ? $countryDetails['Country']['country_code']:'',
								'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
								'city'=> !empty($ud['UserProfile']['city']) ? $ud['UserProfile']['city']:'',
								'profession'=> !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type']:'',
								'gmcnumber'=> !empty($ud['UserProfile']['gmcnumber']) ? $ud['UserProfile']['gmcnumber']:'',
								'contact_no'=> !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'',
								'address'=> !empty($ud['UserProfile']['address']) ? $ud['UserProfile']['address']:'',
								'dob'=> (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'',
								'year_of_graduation'=> !empty($ud['UserProfile']['year_of_graduation']) ? $ud['UserProfile']['year_of_graduation']:'',
								'gender'=> !empty($ud['UserProfile']['gender']) ? $ud['UserProfile']['gender']:'',
								//'current_employment'=> !empty($ud['UserProfile']['current_employment']) ? $ud['UserProfile']['current_employment']:'',
								'current_employment'=> $userCurrentCompanyName,
								'institution_name'=> !empty($ud['UserProfile']['institution_name']) ? $ud['UserProfile']['institution_name']:'',
								'Colleague'=> array("status"=> $colleagueStatus, "request_by"=> $requestType), 
								'is_follow'=> $isFollow,
								'qb_details'=> $qbInfo,
								//'role_status'=> !empty($ud['UserProfile']['role_status']) ? $ud['UserProfile']['role_status']:'',
								"role_status"=> !empty($userRoleStatusData['UserProfile']['role_status']) ? $userRoleStatusData['UserProfile']['role_status']:'',
								);
					}
				}
			}
		}
		return $userDataList;
	}

	/*
	----------------------------------------------
	On: 
	I/P: 
	O/P:
	Desc: 
	----------------------------------------------
	*/
	public function arraySortSuggestedColleague($array, $on, $order=SORT_ASC)
	{
		$new_array = array();
    	$sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            $sortable_array[$k] = $v;
	        }
	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    	
	    }
	    return $new_array;
	}

	public function getOnCallUserByRole(){
		 $responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			$userId =  $dataInput['user_id'];
			$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
			$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
			$params['user_id'] = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
			$params['colleagueStatus'] = 1;
			$params['role_status'] = isset($dataInput['role_status']) ? $dataInput['role_status'] : '';
			$colleagueRequestBy = isset($dataInput['request_by']) ? $dataInput['request_by'] : 'all';
			$colleagueList = $this->colleagueDataNew( $params, $colleagueRequestBy );
			$responseData = array('method_name'=> 'colleagueSearch','status'=>'1','response_code'=>'200','message'=> ERROR_200, 'data' => $colleagueList);
		}else{
			$responseData = array('method_name'=> 'colleagueSearch','status'=>'0','response_code'=>'611','message'=> ERROR_611);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	public function colleagueDataNew( $params = array(), $colleagueRequestBy = 'all' ){ 
			$colleagueList = array();
			if( !empty($params) ){
				App::import('model','UserColleague');
				$UserColleague = new UserColleague();
				$offsetVal = ( $params['page_number'] - 1 ) * $params['size'];
				if( !empty($params['role_status']) ){
						$conditions = ' AND User.status =  1 AND  User.approved = 1 AND ( LOWER(UserProfile.first_name) LIKE LOWER("%'.$params['role_status'].'%") ';
						$conditions .= ' OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['name'].'%") ) LIMIT ' . $offsetVal . ',' . $params['size'];
						// $conditions .= ' OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['role_status'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['role_status'].'%") ) ORDER BY UserProfile.first_name ASC ';	
				}else{
						//$conditions = " LIMIT " . $offsetVal . "," . $params['size'];
					// $conditions = ' AND User.status =  1 AND  User.approved = 1 ORDER BY UserProfile.first_name ASC ';
				}
				if( $colleagueRequestBy == "from_me" ){
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT User.id, User.email, UserProfile.user_id, concat( upper(substring(UserProfile.first_name,1,1)),lower(substring(UserProfile.first_name,2)) ) AS  first_name, UserProfile.last_name , 
					UserProfile.profile_img, UserProfile.role_status, Country.country_name, Country.country_code ,Profession.profession_type,  UserColleague.colleague_user_id  , UserColleague.status 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					$colleagueList = $myColleague;
				}elseif( $colleagueRequestBy == "to_me" ){
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT User.id, User.email, UserProfile.user_id, concat( upper(substring(UserProfile.first_name,1,1)),lower(substring(UserProfile.first_name,2)) ) AS  first_name, UserProfile.last_name , 
					UserProfile.profile_img, UserProfile.role_status, Country.country_name, Country.country_code, Profession.profession_type, UserColleague.user_id colleague_user_id , UserColleague.status  
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueList = $meColleague;
				}else{
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT User.id, User.email, UserProfile.user_id, concat( upper(substring(UserProfile.first_name,1,1)),lower(substring(UserProfile.first_name,2)) ) AS  first_name, UserProfile.last_name , UserQbDetail.qb_id,
					UserProfile.profile_img, UserProfile.role_status, Country.country_name, Country.country_code, Profession.profession_type,  UserColleague.colleague_user_id  , UserColleague.status 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					LEFT JOIN user_duty_logs UserDutyLog ON ( User.id = UserDutyLog.user_id ) 
					LEFT JOIN user_qb_details UserQbDetail ON ( User.id = UserQbDetail.user_id ) 
					WHERE UserDutyLog.status = 1 AND UserDutyLog.hospital_id = 86 AND LOWER(UserProfile.role_status) LIKE '%".strtolower($params['role_status'])."%' AND 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'];
					$myColleague = $UserColleague->query( $myColleagueQuer );
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT User.id,User.email, UserProfile.user_id, concat( upper(substring(UserProfile.first_name,1,1)),lower(substring(UserProfile.first_name,2)) ) AS  first_name, UserProfile.last_name ,  UserQbDetail.qb_id,
					UserProfile.profile_img, UserProfile.role_status, Country.country_name, Country.country_code, Profession.profession_type, UserColleague.user_id colleague_user_id , UserColleague.status  
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					LEFT JOIN user_duty_logs UserDutyLog ON ( User.id = UserDutyLog.user_id ) 
					LEFT JOIN user_qb_details UserQbDetail ON ( User.id = UserQbDetail.user_id ) 
					WHERE UserDutyLog.status = 1 AND UserDutyLog.hospital_id = 86 AND LOWER(UserProfile.role_status) LIKE '%".strtolower($params['role_status'])."%' AND 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'];
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueList = array_merge( $myColleague , $meColleague); 
				}
			}
			$slicedArr = $this->arraySort($colleagueList, 'first_name', SORT_ASC); //** Sort By First name
			$slicedArr = array_slice($slicedArr, $offsetVal, $params['size']); //** Fetch array size
			
			//echo "<pre>";print_r($slicedArr);die;
			return $slicedArr;
		}

	/*
	------------------------------------------------------------------------------------------------
	On: 04-12-2017
	I/P: JSON (logggedin user id and institution_id)
	O/P: JSON (List of users who has sent contact request to loggedIn use)
	Desc: This API will be used to display a list of users who has sent contact request to loggedIn user(User who will call this API from mobiles/web app).
		1>User in the response must belong to same Institution and Country.
		2>User in the response must be active and approved at the time of calling this API.
		3>User must be subscribed for the institution.

	------------------------------------------------------------------------------------------------
	*/

	public function  getContactRequestsSentToMe()
	{
		$responseData = array();
		$contactUserList = array();
		$qbDetails = array();
		$enterpriseCheck = 0;
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$params['loggedinUserId'] = $dataInput['user_id'];
			// $params['loggedinUserInstitute'] = $dataInput['institution_id'];
			// $dataParams['company_id'] = $dataInput['institution_id'];
			$loggedinUserStatus = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));

			$loggedinUserId = $dataInput['user_id'];
			$getUserInstitute = $this->UserEmployment->getUserCurrentInstitution($loggedinUserId);
			//echo "<pre>";print_r($getUserCountry);exit();
			$logggedinUserInstitute = (isset($getUserInstitute['UserEmployment']['company_id']) ? (int) $getUserInstitute['UserEmployment']['company_id'] : (int) 0);

			$dataParams['company_id'] = $logggedinUserInstitute;

			$params['loggedinUserCountry'] = $loggedinUserStatus['UserProfile']['country_id'];
			$params['loggedinUserInstitute'] =  $logggedinUserInstitute;
			$params['loggedinUserProffesion'] = $loggedinUserStatus['UserProfile']['profession_id'];
			//echo "<pre>";print_r($params);exit();
			$isCompanyPaid = $this->CompanyName->getComapanySubscriptionDetails($dataParams);
			$params['status'] = ($isCompanyPaid > 0 ? 1 : 0);
			if($params['status'] == 1)
			{
				$loggedinUserEnterPriseCheck = $this->EnterpriseUserList->find("first", array("conditions"=> array("email"=> $loggedinUserStatus['User']['email'], "company_id"=> $dataParams['company_id'])));
				if($loggedinUserEnterPriseCheck['EnterpriseUserList']['status'] == 1)
				{
					$enterpriseCheck = 1;
				}
				else
				{
					$enterpriseCheck = 0;
				}
			}
			else
			{
				$enterpriseCheck = 1;
			}
			// echo "<pre>";print_r($enterpriseCheck);exit();
			if($enterpriseCheck == 1)
			{
				if($loggedinUserStatus['User']['status']==1 && $loggedinUserStatus['User']['approved']==1)
				{
					// if( $this->validateToken() && $this->validateAccessKey() ){
						if(!empty($params['loggedinUserId']))
						{
							$getContactRequestsDataValue = $this->UserEmployment->getContactRequestsData($params);
							if(! empty($getContactRequestsDataValue) && ($dataParams['company_id'] != -1) )
							{
								foreach ($getContactRequestsDataValue as $getContactRequestsData) {

								//** Get user role tags[START] //Added On 23 July 2018

								$roleTagsArr = array(); $roleTagsVals = array();
								$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $getContactRequestsData['UserProfile']['profession_id'])));
								if(!empty($roleTags['Profession']['tags'])){
									$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
								}
								if(!empty($roleTagsVals)){
									foreach($roleTagsVals as $rt){
										$values = array(); $roleTagId = 0;
										$paramtrs['user_id'] = $getContactRequestsData['UserEmployment']['user_id'];
										$paramtrs['key'] = $rt;
										$roleTagUser = $this->RoleTag->userRoleTags($paramtrs);
										if(!empty($roleTagUser)){
											$roleTagId = $roleTagUser[0]['rt']['id'];
											$values = array($roleTagUser[0]['rt']['value']);
										}
										$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
									}
								}

								//** Get user role tags[END] //Added On 23 July 2018



								$qbDetails['id'] = $getContactRequestsData['UserQbDetails']['qb_id'];
								$contactUserList[] = array("user_id"=> $getContactRequestsData['UserEmployment']['user_id'], "first_name"=> $getContactRequestsData['UserProfile']['first_name'], "last_name"=> $getContactRequestsData['UserProfile']['last_name'],"profile_img"=>(!empty($getContactRequestsData['UserProfile']['profile_img']) ? AMAZON_PATH . $getContactRequestsData['UserEmployment']['user_id']. '/profile/' . $getContactRequestsData['UserProfile']['profile_img']:''), "profession"=>$getContactRequestsData['Professions']['profession_type'], "role_status"=>!empty($getContactRequestsData['UserProfile']['role_status']) ? $getContactRequestsData['UserProfile']['role_status'] : '' , "role_tags"=>$roleTagsArr, "qb_details"=>array("id"=>$qbDetails['id']),"at_work"=>(int) $getContactRequestsData['UserDutyLog']['atwork_status'],"on_call"=>(int) $getContactRequestsData['UserDutyLog']['status'],"contact_custom_msg"=> isset($getContactRequestsData['UserColleagues']['message']) ? $getContactRequestsData['UserColleagues']['message'] : '', "sent_at"=> strtotime($getContactRequestsData['UserColleagues']['updated']));
								}


								$userList['User'] = $contactUserList;
								$responseData = array('method_name'=> 'getContactRequestsSentToMe', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $userList);
							}
							else
							{
								$responseData = array('method_name'=> 'getContactRequestsSentToMe', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
							}
						}
						else
						{
							$responseData = array('method_name'=> 'getContactRequestsSentToMe', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
						}
					// }
					// else{
					// 	$responseData = array('method_name'=> 'getContactRequestsSentToMe', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
					// }
				}
				else
				{
					$isCurrentUser = 1;
					$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
					$responseData = array('method_name'=> 'getContactRequestsSentToMe','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
				}
			}
			else
			{
				$responseData = array('method_name'=> 'getContactRequestsSentToMe', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
			}
		}
		else{
			$responseData = array('method_name'=> 'getContactRequestsSentToMe', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 04-12-2017
	I/P: JSON (user_id, logggedin_user_id and institution_id)
	O/P: JSON 
	Desc: This API will be used to accept a contact request sent to the logged in user and both the users will become friends and will be able send messages to each other in Medic Bleep.

	------------------------------------------------------------------------------------------------
	*/

	public function acceptContactRequest()
	{
		$responseData = array();
		$userCompanyCheck = 0;
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			$loggedinUserId = $dataInput['user_id'];
			$colleagueUserId = $dataInput['colleague_user_id'];
			$instituteId = $dataInput['institution_id'];
			$dataParams['company_id'] = $dataInput['institution_id'];
			$loggedinUserStatus = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$checkColleagueStatus = $this->User->find("first", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'])));
			$userCurrentCompany = $this->UserEmployment->find("count", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
			$colleagueCurrentCompany = $this->UserEmployment->find("count", array("conditions"=> array("user_id"=> $colleagueUserId, "is_current"=> 1), "order"=> array("id DESC")));
			if(($userCurrentCompany > 0 && $colleagueCurrentCompany > 0) )
			{
				$userCompanyCheck = 1;
			}
			$isCompanyPaid = $this->CompanyName->getComapanySubscriptionDetails($dataParams);
			$params['status'] = ($isCompanyPaid > 0 ? 1 : 0);
			if($isCompanyPaid > 0)
			{
				$checkColleagueUserIsAssigned = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $checkColleagueStatus['User']['email'], "company_id"=>$instituteId, "status"=>$params['status'])));
			}
			else
			{
				$checkColleagueUserIsAssigned = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $checkColleagueStatus['User']['email'])));
			}
			if($loggedinUserStatus['User']['status']==1 && $loggedinUserStatus['User']['approved']==1)
			{
				if( ($checkColleagueUserIsAssigned > 0 && $userCompanyCheck == 1) )
				{
					if($checkColleagueStatus['User']['status']==1 && $checkColleagueStatus['User']['approved']==1)
					{
						// if( $this->tokenValidate() && $this->accesskeyCheck() ){
							if(!empty($loggedinUserId))
							{
								$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $colleagueUserId, "colleague_user_id"=> $loggedinUserId)));
								if($myColleague > 0)
								{
									try
									{
										$updateColleague = $this->UserColleague->updateAll( array("status"=> 1), array("user_id"=> $colleagueUserId, "colleague_user_id" =>$loggedinUserId));
										if( $updateColleague ){
											$responseData = array('method_name'=> 'acceptContactRequest','status'=>'1','response_code'=> "200", 'message'=> ERROR_200);
											try{
												//if($dataInput['colleagueStatus'] == 1){ //** Follow only in case of accept(not reject or delete)
													$conditions = array("followed_by"=> $loggedinUserId, "followed_to"=> $colleagueUserId);
													$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
														if($userFollowCheck == 0 ){
														$this->UserFollow->save( array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id'], "follow_type"=> 1, "status"=> 1) );
														}else{
															$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
														}
												//}
											}catch( Exception $e ){}
										}else{
											$responseData = array('method_name'=> 'acceptContactRequest','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
										}
									}
									catch( Exception $e ){
										$responseData = array('method_name'=> 'acceptContactRequest','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
									}

								}
							}else
							{
								$responseData = array('method_name'=> 'acceptContactRequest', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
							}
						// }else{
						// 	$responseData = array('method_name'=> 'acceptContactRequest', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
						// }
							
					}else{
						$isCurrentUser = 0;
						$statusResponse = $this->getUserProfileStatuscheck($colleagueUserId,$isCurrentUser);
						$responseData = array('method_name'=> 'acceptContactRequest','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
					}
				}else{
					$responseData = array('method_name'=> 'acceptContactRequest','status'=>'0','response_code'=>"656", 'message'=> ERROR_656);
				}
			}else{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
				$responseData = array('method_name'=> 'acceptContactRequest','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'acceptContactRequest', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 04-12-2017
	I/P: (user_id, logggedin_user_id and institution_id)
	O/P: JSON 
	Desc: This API will be used to reject a contact request sent to the logged in user.

	------------------------------------------------------------------------------------------------
	*/

	public function rejectContactRequest()
	{
		$responseData = array();
		$userCompanyCheck = 0;
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			$loggedinUserId = $dataInput['user_id'];
			$colleagueUserId = $dataInput['colleague_user_id'];
			$instituteId = $dataInput['institution_id'];
			$dataParams['company_id'] = $dataInput['institution_id'];
			$loggedinUserStatus = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$checkColleagueStatus = $this->User->find("first", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'])));
			$userCurrentCompany = $this->UserEmployment->find("count", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
			$colleagueCurrentCompany = $this->UserEmployment->find("count", array("conditions"=> array("user_id"=> $colleagueUserId, "is_current"=> 1), "order"=> array("id DESC")));
			if(($userCurrentCompany > 0 && $colleagueCurrentCompany > 0) )
			{
				$userCompanyCheck = 1;
			}
			$isCompanyPaid = $this->CompanyName->getComapanySubscriptionDetails($dataParams);
			$params['status'] = ($isCompanyPaid > 0 ? 1 : 0);
			if($isCompanyPaid > 0)
			{
				$checkColleagueUserIsAssigned = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $checkColleagueStatus['User']['email'], "company_id"=>$instituteId, "status"=>$params['status'])));
			}
			else
			{
				$checkColleagueUserIsAssigned = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $checkColleagueStatus['User']['email'])));
			}
			if($loggedinUserStatus['User']['status']==1 && $loggedinUserStatus['User']['approved']==1)
			{
				if( ($checkColleagueUserIsAssigned > 0 && $userCompanyCheck == 1) )
				{
					if($checkColleagueStatus['User']['status']==1 && $checkColleagueStatus['User']['approved']==1)
					{
						// if( $this->tokenValidate() && $this->accesskeyCheck() ){
							if(!empty($loggedinUserId))
							{
								$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $colleagueUserId, "colleague_user_id"=> $loggedinUserId)));
								if($myColleague > 0)
								{
									try
									{
										$updateColleague = $this->UserColleague->updateAll( array("status"=> 3), array("user_id"=> $colleagueUserId, "colleague_user_id" =>$loggedinUserId));
										if( $updateColleague ){
											$responseData = array('method_name'=> 'rejectContactRequest','status'=>'1','response_code'=> "200", 'message'=> ERROR_200);
										}else{
											$responseData = array('method_name'=> 'rejectContactRequest','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
										}
									}
									catch( Exception $e ){
										$responseData = array('method_name'=> 'rejectContactRequest','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
									}

								}
							}else
							{
								$responseData = array('method_name'=> 'rejectContactRequest', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
							}
						// }else{
						// 	$responseData = array('method_name'=> 'rejectContactRequest', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
						// }
							
					}else{
						$isCurrentUser = 0;
						$statusResponse = $this->getUserProfileStatuscheck($colleagueUserId,$isCurrentUser);
						$responseData = array('method_name'=> 'rejectContactRequest','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
					}
				}else{
					$responseData = array('method_name'=> 'rejectContactRequest','status'=>'0','response_code'=>"656", 'message'=> ERROR_656);
				}
			}else{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
				$responseData = array('method_name'=> 'rejectContactRequest','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'rejectContactRequest', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 05-12-2017
	I/P: JSON (logggedin user id and institution_id)
	O/P: JSON (Count users who has sent contact request to loggedIn use)
	Desc: This API will be used to display count of users who has sent contact request to loggedIn user.

	------------------------------------------------------------------------------------------------
	*/

	public function  getPendingContactRequestCount()
	{
		$responseData = array();
		$contactUserList = array();
		$qbDetails = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$params['loggedinUserId'] = $dataInput['user_id'];
			// $params['loggedinUserInstitute'] = $dataInput['institution_id'];
			$loggedinUserStatus = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));

			$loggedinUserId = $dataInput['user_id'];
			$getUserInstitute = $this->UserEmployment->getUserCurrentInstitution($loggedinUserId);
			$logggedinUserInstitute = (isset($getUserInstitute['UserEmployment']['company_id']) ? (int) $getUserInstitute['UserEmployment']['company_id'] : (int) 0);


			$params['loggedinUserCountry'] = $loggedinUserStatus['UserProfile']['country_id'];
			$dataParams['company_id'] = $logggedinUserInstitute;
			$params['loggedinUserInstitute'] = $logggedinUserInstitute;
			$isCompanyPaid = $this->CompanyName->getComapanySubscriptionDetails($dataParams);
			$params['status'] = ($isCompanyPaid > 0 ? 1 : 0);
			$params['status'] = ($isCompanyPaid > 0 ? 1 : 0);
			if($params['status'] == 1)
			{
				$loggedinUserEnterPriseCheck = $this->EnterpriseUserList->find("first", array("conditions"=> array("email"=> $loggedinUserStatus['User']['email'], "company_id"=> $dataParams['company_id'])));
				if($loggedinUserEnterPriseCheck['EnterpriseUserList']['status'] == 1)
				{
					$enterpriseCheck = 1;
				}
				else
				{
					$enterpriseCheck = 0;
				}
			}
			else
			{
				$enterpriseCheck = 1;
			}
			if($enterpriseCheck == 1)
			{
				//echo "<pre>";print_r($params);exit();
				if($loggedinUserStatus['User']['status']==1 && $loggedinUserStatus['User']['approved']==1)
				{
					if( $this->validateToken() && $this->validateAccessKey() ){
						if(!empty($params['loggedinUserId']))
						{
							if($dataParams['company_id'] != -1)
							{
								$getContactRequestsDataValue = $this->UserEmployment->getPendingRequestData($params);
								$pendingUserCount['pending_users'] = (! empty($getContactRequestsDataValue) ? (int) count($getContactRequestsDataValue): (int) 0);
								$responseData = array('method_name'=> 'getPendingContactRequestCount', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $pendingUserCount) ;
							}
							else
							{
								$pendingUserCount['pending_users'] = 0;
								$responseData = array('method_name'=> 'getPendingContactRequestCount', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $pendingUserCount) ;
							}
							
						}
						else
						{
							$responseData = array('method_name'=> 'getPendingContactRequestCount', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
						}
					}
					else{
						$responseData = array('method_name'=> 'getPendingContactRequestCount', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
					}
				}
				else
				{
					$isCurrentUser = 1;
					$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
					$responseData = array('method_name'=> 'getPendingContactRequestCount','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
				}
			}
			else
			{
				$responseData = array('method_name'=> 'getContactRequestsSentToMe', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
			}
		}
		else{
			$responseData = array('method_name'=> 'getPendingContactRequestCount', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 05-12-2017
	I/P: JSON (logggedin user id and institution_id)
	O/P: JSON (StaticTags, Professions, Specialities, Grade, Wards, Bands)
	Desc: This API will be used to display count of users who has sent contact request to loggedIn user.

	------------------------------------------------------------------------------------------------
	*/

	public function getSearchTagsForAnInstitute()
	{
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$loggedinUserId = $dataInput['user_id'];
			$countryCode = $dataInput['country_code'];
			$getUserInstitute = $this->UserEmployment->getUserCurrentInstitution($loggedinUserId);
			$getUserCountry = $this->UserProfile->getUserCountry($loggedinUserId);
			//echo "<pre>";print_r($getUserCountry);exit();
			$logggedinUserInstitute = (isset($getUserInstitute['UserEmployment']['company_id']) ? (int) $getUserInstitute['UserEmployment']['company_id'] : (int) 0);
			$loggedinUserStatus = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId)));
			$userCountryId = (isset($getUserCountry['UserProfile']['country_id']) ? (int) $getUserCountry['UserProfile']['country_id'] : (int) 0);
			if($loggedinUserStatus['User']['status']==1 && $loggedinUserStatus['User']['approved']==1)
			{
				// if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if(! empty($loggedinUserId))
					{
						//******** Static Role Tags ********//
						$getStaticRoleTags = $this->StaticRoleTag->find('all', array('conditions'=> array('trust_id'=>array(0, $logggedinUserInstitute))));
						foreach ($getStaticRoleTags AS  $staticRoleTags) {
							$staticRoleTagList[] = array("type"=> $staticRoleTags['StaticRoleTag']['key'], "value"=> $staticRoleTags['StaticRoleTag']['value']);
						}
						//********  ********//

						//******** Professions ********//
						$getProfessions = $this->Profession->find('all', array('conditions'=> array('country_id'=>$userCountryId ,'status'=> 1), 'order'=> array('profession_type')));
						foreach ($getProfessions as  $professions) {
							$professionsList[] = array("type"=> "Profession", "value"=> $professions['Profession']['profession_type']);
						}
						//********  ********//

						//******** Role Tags ********//
						$params['tagKey'] = "Speciality";
						$params['institution_id'] = $logggedinUserInstitute;
						$getSpeciality = $this->RoleTag->roleTagsInstituteWise($params);
						foreach ($getSpeciality as  $speciality) {
							$specialityList[] = array("type"=> "Speciality", "value"=> $speciality['RoleTag']['value']);
						}
						//********  ********//

						//******** Grade ********//
						$params['tagKey'] = "Grade";
						$params['institution_id'] = $logggedinUserInstitute;
						$getGradeValues = $this->RoleTag->roleTagsInstituteWise($params);
						foreach ($getGradeValues as  $gradeValues) {
							$gradeList[] = array("type"=> "Grade", "value"=> $gradeValues['RoleTag']['value']);
						}
						//********  ********// 

						//******** Ward ********//
						$params['tagKey'] = "Base Location";
						$params['institution_id'] = $logggedinUserInstitute;
						$getWardValues = $this->RoleTag->roleTagsInstituteWise($params);
						foreach ($getWardValues as  $wardValues) {
							$wardList[] = array("type"=> "Base Location", "value"=> $wardValues['RoleTag']['value']);
						}
						//********  ********//

						//******** Bands ********//
						$params['tagKey'] = "Band";
						$params['institution_id'] = $logggedinUserInstitute;
						$getBandValues = $this->RoleTag->roleTagsInstituteWise($params);
						foreach ($getBandValues as  $bandValues) {
							$bandList[] = array("type"=> "Band", "value"=> $bandValues['RoleTag']['value']);
						}
						//********  ********//

						$otherTagArr = array($professionsList,$specialityList,$gradeList,$wardList,$bandList);
						foreach ($otherTagArr as $keyOne => $valOne) {
							foreach ($valOne as $keyTwo => $valTwo) {
								$otherTagsArray[] = array("type"=> $valTwo['type'], "value"=> $valTwo['value']);
							}
						}

						$data = array('StaticTags'=> $staticRoleTagList, 'Other_Tags'=> $otherTagsArray);
						$responseData = array('method_name'=> 'getSearchTagsForAnInstitute', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $data);

					}else{
						$responseData = array('method_name'=> 'getSearchTagsForAnInstitute', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
					}
				// }else{
				// 	$responseData = array('method_name'=> 'getSearchTagsForAnInstitute', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
				// }
			
			}else
			{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($loggedinUserId,$isCurrentUser);
				$responseData = array('method_name'=> 'getSearchTagsForAnInstitute','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'getSearchTagsForAnInstitute', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 06-12-2017
	I/P: (user_id, logggedin_user_id and institution_id, status)
	O/P: JSON 
	Desc: This API will update the current contact request status by desired status.

	------------------------------------------------------------------------------------------------
	*/

	public function updateContactRequestStatus()
	{
		$responseData = array();
		$sendStatus = 0;
		$loggedInUserenterpriseCheck = 0;
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$params['user_id'] = $dataInput['user_id'];
			$params['colleague_user_id'] = $dataInput['colleague_user_id'];
			$params['request_type'] = $dataInput['request_type'];
			$params['contact_custom_msg'] = isset($dataInput['contact_custom_msg']) ? $dataInput['contact_custom_msg'] : '';
			$params['dialog_id'] = isset($dataInput['dialog_id']) ? $dataInput['dialog_id'] : '';
			//echo "pre>";print_r($params);exit();
			$loggedinUserStatus = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$checkColleagueStatus = $this->User->find("first", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'])));
			if($loggedinUserStatus['User']['status']==1 && $loggedinUserStatus['User']['approved']==1)
			{
				if($checkColleagueStatus['User']['status']==1 && $checkColleagueStatus['User']['approved']==1)
				{
					// if( $this->validateToken() && $this->validateAccessKey() )
					// {
						if(!empty($params['user_id']))
						{
							$getUserInstitution = $this->UserEmployment->getUserInstitution($params);
							if(($params['request_type'] == "REMOVE"))
							{
								$params['colleague_status'] = 0;
								$updateUserCollegue = $this->updateUserCollegue($params);
								if($updateUserCollegue)
								{
									$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
								}
							}
							else
							{
								if(!empty($getUserInstitution))
								{
									//check logged in user is assigned or not
									$isloggedInUserAssigned = $this->checkUserIsAssigned($loggedinUserStatus['User']['email'], $getUserInstitution[0]['ue']['company_id']);
									//check colleague is assigned or not
									$isCollegueUserAssigned = $this->checkUserIsAssigned($checkColleagueStatus['User']['email'], $getUserInstitution[0]['ues']['company_id']);
									if($isloggedInUserAssigned == 1)
									{
										if($isCollegueUserAssigned == 1)
										{
											//check company is not none
											$getloggedInUserInstitute = $this->UserEmployment->getUserCurrentInstitution($params['user_id']);
											$getCollegueUserInstitute = $this->UserEmployment->getUserCurrentInstitution($params['colleague_user_id']);
											if($getloggedInUserInstitute['UserEmployment']['company_id'] != -1)
											{
												if($getCollegueUserInstitute['UserEmployment']['company_id'] != -1)
												{
													if($params['request_type'] == "SEND")
													{
														$params['colleague_status'] = 2;
														$sendStatus = 1;
														$params['send_status'] = $sendStatus;
													}
													else if($params['request_type'] == "CANCEL")
													{
														$params['colleague_status'] = 0;
													}
													else if($params['request_type'] == "ACCEPT")
													{
														$params['colleague_status'] = 1;
													}
													else if($params['request_type'] == "REJECT")
													{
														$params['colleague_status'] = 3;
													}
													//echo "<pre>";print_r($params);exit();
													$updateUserCollegue = $this->updateUserCollegue($params);
													if($updateUserCollegue == 1)
													{
														$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
															if(! empty($params['dialog_id']))
															{
																try{
																$updateAnaliticsData = $this->updateColleagueAnalyticsData($params);
																}
																catch( Exception $e){

																}
															}
													}
													else
													{
														$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
													}
												}
												else
												{
													$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "656", 'message'=> ERROR_656);
												}
											}
											else
											{
												$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "656", 'message'=> ERROR_656);
											}
										}
										else
										{
											$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "656", 'message'=> ERROR_656);
										}
									}
									else
									{
										$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "656", 'message'=> ERROR_656);
									}
								}
								else
								{
									$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "656", 'message'=> ERROR_656);
								}
							}
						}else
						{
							$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
						}
					// }else{
					// 	$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
					// }
					
				}else{
					$isCurrentUser = 0;
					$statusResponse = $this->getUserProfileStatuscheck($colleagueUserId,$isCurrentUser);
					$responseData = array('method_name'=> 'updateContactRequestStatus','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
					}
			}else{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
				$responseData = array('method_name'=> 'updateContactRequestStatus','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}

		}else{
			$responseData = array('method_name'=> 'updateContactRequestStatus', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	public function updateUserCollegue($params=array())
	{
		$updateStatus = 0;
		$sendRequestDate = date('Y-m-d H:i:s');
		$myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $params['colleague_user_id'], "colleague_user_id"=> $params['user_id'])));
		$meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "colleague_user_id"=> $params['colleague_user_id'])));
		if(!empty($myColleague))
		{
			$conditions = array("user_id"=> $params['colleague_user_id'], "colleague_user_id"=> $params['user_id']);
		}
		else if(!empty($meColleague))
		{
			$conditions = array("user_id"=> $params['user_id'], "colleague_user_id"=> $params['colleague_user_id']);
		}
		if( (!empty($myColleague)) || (!empty($meColleague)) ){ //** Check if already colleague
			try{
				$updateColleague = $this->UserColleague->updateAll( array("status"=> $params['colleague_status'], "updated" => "'".$sendRequestDate."'"), $conditions);
				$updateStatus = 1;

			}catch( Exception $e ){
				$updateStatus = 0;
			}
		}
		else
		{
			try{
				$saveCollegue = $this->UserColleague->save( array("user_id"=> $params['user_id'], "colleague_user_id"=> $params['colleague_user_id'], "status"=> $params['colleague_status'], 'message' =>  $params['contact_custom_msg'], "updated" => $sendRequestDate) );
				if($saveCollegue)
				{
					$updateStatus = 1;
				}
				else
				{
					$updateStatus = 0;
				}
				
			}catch( Exception $e ){
				$updateStatus = 0;
			}
		}
		if($params['send_status'] == 1)
		{
			$conditions = array("id"=> array($myColleague['UserColleague']['id'], $meColleague['UserColleague']['id']));
			// $updateColleague = $this->UserColleague->updateAll( array("user_id"=> $params['user_id'], "colleague_user_id"=>$params['colleague_user_id'],"status"=>2), $conditions);
			$updateColleague = $this->UserColleague->updateAll( array("user_id"=> $params['user_id'], "colleague_user_id"=>$params['colleague_user_id'],"status"=>2, 'message' =>  "'".$params['contact_custom_msg']."'", "updated" => "'".$sendRequestDate."'"), $conditions);
		}

		
		//************************** This section of code is commented after discussing ocr follow/following
		//will independent from mediccreations contact request/friend request[START]**********************//

		// if($params['colleague_status'] == 1){ //** Follow only in case of accept(not reject or delete)
		// $followConditions = array("followed_by"=> $params['user_id'], "followed_to"=> $params['colleague_user_id']);
		// $userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $followConditions));
		// 	if($userFollowCheck == 0 ){
		// 	$this->UserFollow->save( array("followed_by"=> $params['user_id'], "followed_to"=> $params['colleague_user_id'], "follow_type"=> 1, "status"=> 1) );
		// 	}else{
		// 		$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $followConditions );
		// 	}
		// }

		//************************** This section of code is commented after discussing ocr follow/following
		//will independent from mediccreations contact request/friend request[END]**********************//

		return $updateStatus;
	}

	public function checkUserIsAssigned($email, $companyId)
	{
		$isAssigned = 0;
		$dataParams['company_id'] = $companyId;
		$dataParams['email'] = $email;
		$checkCompanySubscription = $this->CompanyName->getComapanySubscriptionDetails($dataParams);
		//return $checkCompanySubscription;
		if($checkCompanySubscription > 0)
		{
			$checkCompanySubscription = $this->EnterpriseUserList->getEnterpriseUserStatus($dataParams);
			if($checkCompanySubscription['EnterpriseUserList']['status'] == 1)
			{
				$isAssigned = 1;
			}
			else
			{
				$isAssigned = 0;
			}
		}
		else
		{
			$isAssigned = 1;
		}

		return $isAssigned;
	}

	// public function updateColleagueAnalyticsData($params=array())
	// {
	// 	ini_set('display_errors', 1);
	// 	ini_set('display_startup_errors', 1);
	// 	error_reporting(E_ALL);
	// 	$dbhost = '35.177.136.62:3306';
	// 	$dbuser = 'mb_analitics';
	// 	$dbpass = 'Team@Medic@';
	// 	$database = 'mb_analytics';
	// 	$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
	// 	if(! $conn )
	// 	{
	// 	die('Could not connect to instance: ' . mysqli_error($conn));
	// 	}

	// 	// echo "Connected to MySQL Successfully! <br />";
	// 	$sql = 'SELECT * FROM user_colleagues_transactions';

	// 	$db = mysqli_select_db($conn,"mb_analytics");
	// 	if(! $db )
	// 	{
	// 	die('Could not Select Database: ' . mysqli_error($conn));
	// 	}
	// 	// echo "Database Selected Successfully!";
	// 	// $retval = mysqli_query($conn,"SELECT * FROM user_colleagues_transactions");
	// 	// $senderDetail = $this->UserProfile->getUserDetail($params['user_id']);
	// 	$senderuserid = $params['colleague_user_id'];
	// 	$senderDetail = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $senderuserid)));
	// 	$senderFirstName = $senderDetail['UserProfile']['first_name'];
	// 	$senderLastName = $senderDetail['UserProfile']['last_name'];
	// 	$senderFullName = $senderFirstName. " ".$senderLastName; 
	// 	// $reciverDetail = $this->UserProfile->getUserDetail($params['colleague_user_id']);
	// 	$recieveruserid = $params['user_id'];
	// 	$reciverDetail = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $recieveruserid)));
	// 	$reciverFirstName = $reciverDetail['UserProfile']['first_name'];
	// 	$reciverLastName = $reciverDetail['UserProfile']['last_name'];
	// 	$reciverFullName = $reciverFirstName. " ".$reciverLastName; 
	// 	if($params['request_type'] == "ACCEPT")
	// 	{
	// 		$insertData = mysqli_query($conn,"INSERT INTO user_colleagues_transactions (sender_id,sender_name,reciever_id, reciever_name, dialogue_id,status) 
	// 		VALUES ('".$params['colleague_user_id']."','".$senderFullName."','".$params['user_id']."','".$reciverFullName."','".$params['dialog_id']."', 1)");
	// 	}
	// 	$istrue = 1;

	// 	return $istrue;

	// }


	/*
	------------------------------------------------------------------------------------------------
	On: 23-10-18
	I/P: 
	O/P:  
	Desc: This Api only run for one time to update analitics database to store sender_id, reciever_id, dialog_id etc. 

	------------------------------------------------------------------------------------------------
	*/

	// public function updateUserColleaguesData()
	// {

	// 	ini_set('display_errors', 1);
	// 	ini_set('display_startup_errors', 1);
	// 	error_reporting(E_ALL);
	// 	$dbhost = '35.177.136.62:3306';
	// 	$dbuser = 'mb_analitics';
	// 	$dbpass = 'Team@Medic@';
	// 	$database = 'mb_analytics';
	// 	$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
	// 	if(! $conn )
	// 	{
	// 	die('Could not connect to instance: ' . mysqli_error($conn));
	// 	}

	// 	// echo "Connected to MySQL Successfully! <br />";
	// 	$sql = 'SELECT * FROM user_colleagues_transactions';

	// 	$db = mysqli_select_db($conn,"mb_analytics");
	// 	if(! $db )
	// 	{
	// 	die('Could not Select Database: ' . mysqli_error($conn));
	// 	}
	// 	$PatientHcpAssociationData = $this->PatientHcpAssociation->find("all", array("conditions"=> array("dialogue_type"=> "private")));
	// 	foreach ($PatientHcpAssociationData as  $value) {
	// 		$explodedvalue = explode(",", $value['PatientHcpAssociation']['participant_ids']);
	// 		$sender_id = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $explodedvalue[0])));
	// 		$senderDetail = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $sender_id['UserQbDetail']['user_id'])));
	// 		// echo "<pre>";print_r($senderDetail);exit();
	// 		$senderFirstName = $senderDetail['UserProfile']['first_name'];
	// 		$senderLastName = $senderDetail['UserProfile']['last_name'];
	// 		$senderFullName = $senderFirstName. " ".$senderLastName; 

	// 		$reciever_id = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $explodedvalue[1])));
	// 		$reciverDetail = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $reciever_id['UserQbDetail']['user_id'])));
	// 		$reciverFirstName = $reciverDetail['UserProfile']['first_name'];
	// 		$reciverLastName = $reciverDetail['UserProfile']['last_name'];
	// 		$reciverFullName = $reciverFirstName. " ".$reciverLastName;

	// 		$insertData = mysqli_query($conn,"INSERT INTO user_colleagues_transactions (sender_id,sender_name,reciever_id, reciever_name, dialogue_id,status,created) 
	// 		VALUES ('".$sender_id['UserQbDetail']['user_id']."','".$senderFullName."','".$reciever_id['UserQbDetail']['user_id']."','".$reciverFullName."','".$value['PatientHcpAssociation']['dialogue_id']."', 1,  '".$value['PatientHcpAssociation']['created_date']."')");

	// 	}
	// 	echo "Done";exit;
	// }

}
