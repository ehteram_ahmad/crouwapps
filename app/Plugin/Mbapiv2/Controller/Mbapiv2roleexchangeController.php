<?php
/*
 * DndStatus controller.
 * Created By Ehteram Ahmad
 * On 12 FEB 2019
 */
App::uses('AppController', 'Controller');


class Mbapiv2roleexchangeController extends AppController {
	public $uses = array('Mbapiv2.User','Mbapiv2.UserDndStatus','Mbapiv2.DndStatusOption', 'Mbapiv2.VoipPushNotification', 'UserQbDetail','Mbapiv2.UserOneTimeToken','Mbapiv2.UserBatonRole','Mbapiv2.BatonRole','Mbapiv2.CacheLastModifiedUser','Mbapiv2.UserBatonRoleTransaction', 'Mbapiv2.UserDutyLog','Mbapiv2.UserOneTimeToken','Mbapiv2.DepartmentsBatonRole','Mbapiv2.UserPermanentRole','Mbapiv2.UserPermanentRolesTransaction');
	public $components = array('Common','Cache','Quickblox');


	/*
	-------------------------------------------------------------------------
	ON: 06-03-2019
	I/P: JSON (user_id)
	O/P: JSON
	Desc: Will return list of all baton roles values related to the user
	-------------------------------------------------------------------------
	*/

	public function userBatonRolesDetail()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$user_id = $dataInput['user_id'];
			// if( $this->validateToken() && $this->validateAccessKey() ){
				if(!empty($user_id)){
					try{

						$getUserBatonRoleDetail = $this->UserBatonRole->getUserBatonRoleDetail($user_id);
						if(!empty($getUserBatonRoleDetail))
						{
							$userData['UserBatonRoles'] = $this->batonRoleDataFormat($getUserBatonRoleDetail);
						}
						else
						{
							$userData['UserBatonRoles'] = array();
						}
						$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $userData);
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{

					$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "657", 'message'=> ERROR_657);
				}

			// }else{
			// 	$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	--------------------------------------------------------------------------
	On: 13-11-2017
	I/P: JSON
	O/P: JSON, user lists
	Desc: Formatting static directory data
	--------------------------------------------------------------------------
	*/
	public function batonRoleDataFormat( $batonRoleDetails = array())
	{
		$batonRoleDataList = array();
		if(!empty($batonRoleDetails))
		{
			foreach ($batonRoleDetails as  $batonRoleDetail) {
				$otherUserFullname = $batonRoleDetail['UserProfiles']['first_name']." ".$batonRoleDetail['UserProfiles']['last_name'];

				$updated_at = $batonRoleDetail['UserBatonRole']['updated_at'];
				$createdTimeStamp = strtotime($updated_at);
				$timeoutInSecond = $batonRoleDetail['DepartmentsBatonRole']['timeout']*60;
				$duration = $createdTimeStamp+$timeoutInSecond;
				$batonRoleDataList[] = array(
						'other_user_id'=> isset($batonRoleDetail['UserProfiles']['user_id']) ? $batonRoleDetail['UserProfiles']['user_id'] : 0,
						'profile_img'=> !empty($batonRoleDetail['UserProfiles']['profile_img']) ? AMAZON_PATH . $batonRoleDetail['UserProfiles']['user_id']. '/profile/' . $batonRoleDetail['UserProfiles']['profile_img']:'',
						'thumbnail_img'=> !empty($batonRoleDetail['UserProfiles']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $batonRoleDetail['UserProfiles']['user_id']. '/profile/' . $batonRoleDetail['UserProfiles']['thumbnail_img']:'',
						'other_user_name'=> $otherUserFullname,
						'role_name'=> isset($batonRoleDetail['BatonRole']['baton_roles']) ? $batonRoleDetail['BatonRole']['baton_roles'] : "",
						'role_id'=> isset($batonRoleDetail['UserBatonRole']['role_id']) ? $batonRoleDetail['UserBatonRole']['role_id'] : 0,
						'duration'=> isset($duration) ? $duration : 0,
						'type'=>isset($batonRoleDetail['UserBatonRole']['status']) ? $batonRoleDetail['UserBatonRole']['status'] : 0,
						'on_call_value'=>isset($batonRoleDetail['DepartmentsBatonRole']['on_call_value']) ? $batonRoleDetail['DepartmentsBatonRole']['on_call_value'] : 0,
						'note'=>isset($batonRoleDetail['UserBatonRole']['notes']) ? $batonRoleDetail['UserBatonRole']['notes'] : "",
						'other_user_on_call'=>isset($batonRoleDetail['UserDutyLog']['status']) ? $batonRoleDetail['UserDutyLog']['status'] : "",
						'other_user_available'=>isset($batonRoleDetail['UserDutyLog']['atwork_status']) ? $batonRoleDetail['UserDutyLog']['atwork_status'] : "",
						);

			}
			return $batonRoleDataList;
		}
	}

	/*
	-------------------------------------------------------------------------
	ON: 07-03-2019
	I/P: $params
	O/P: array
	Desc: This function will update the user role status
	-------------------------------------------------------------------------
	*/

	public function updateUsersBatonRole($params = array())
	{
		$currentDateFormate = date('Y-m-d H:i:s');
		$vExecuteUpdate = "yes";
		// $note = isset($params['note']) ? $params['note'] : "";
		$note = isset($params['note']) ? $params['note'] : "";
		$result['msg'] = "";
		if(!empty($params))
		{
			$getFromUserData = $this->UserBatonRole->find("first", array("conditions"=> array("from_user"=> $params['from_user'],'to_user'=>$params['to_user'], "role_id"=>$params['role_id'], "is_active"=>"1")));
			$result['current_status'] = $getFromUserData['UserBatonRole']['status'];
			$result['current_active_state'] = $getFromUserData['UserBatonRole']['is_active'];
			switch ($params['request_type']) {
			    case "transfer":
			        $data = array('from_user_status'=>5, "to_user_status"=>4,"from_user_is_active"=>1,"to_user_is_active"=>1);
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];

			        $this->UserBatonRole->updateAll(array("is_active"=>0, "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['from_user'],"role_id"=>$params['role_id'],'is_active'=>1,'status'=>1));

			        if($params['to_user'] == 0){
						$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$params['role_id']));

						$data = array('from_user_status'=>0, "to_user_status"=>1,"from_user_is_active"=>0,"to_user_is_active"=>1);
						$result['msg'] = ROLEEX_018;
						$result['isCacheModified'] = 1;
					}

			        break;
			    case "transfer_accept":
			        $data = array('from_user_status'=>1, "to_user_status"=>0,"from_user_is_active"=>1,"to_user_is_active"=>0);
			        $this->UserBatonRole->updateAll(array("status"=>-1,"updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['to_user'],"role_id"=>$params['role_id'],'is_active'=>0,'status'=>1));
			        $result['isCacheModified'] = 1;
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];
			        $result['oncall_user_id'] = $params['from_user'];
			        if($result['current_status'] != 2 && $result['current_status'] != 3 && $result['current_status'] != 4 && $result['current_status'] != 5  ){
			        	$vExecuteUpdate = "no";
			        }
			        break;
			    case "transfer_rejected":
			        $data = array('from_user_status'=>0, "to_user_status"=>7,"from_user_is_active"=>0,"to_user_is_active"=>0);
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];

			        $this->UserBatonRole->updateAll(array("is_active"=>1, "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['to_user'],"role_id"=>$params['role_id'],'is_active'=>0,'status'=>1));
			        if($params['to_user'] == 0){
						// $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$params['role_id'],'DepartmentsBatonRole.institute_id'=>$params['institute_id']));
						$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$params['role_id']));
					}
			        $result['isCacheModified'] = 1;
			        if($result['current_status'] != 2 && $result['current_status'] != 3 && $result['current_status'] != 4 && $result['current_status'] != 5  ){
			        	$vExecuteUpdate = "no";
			        }
			        break;
			    case "request":
			        $data = array('from_user_status'=>3, "to_user_status"=>2,"from_user_is_active"=>1,"to_user_is_active"=>1);
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];
			        $this->UserBatonRole->updateAll(array("is_active"=>0, "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['to_user'],"role_id"=>$params['role_id'],'is_active'=>1,'status'=>1));
							if($params['to_user'] == 0){
								// $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>1),array('DepartmentsBatonRole.role_id'=>$params['role_id'],'DepartmentsBatonRole.institute_id'=>$params['institute_id']));
								$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>1),array('DepartmentsBatonRole.role_id'=>$params['role_id']));
								$data = array('from_user_status'=>1, "to_user_status"=>0,"from_user_is_active"=>1,"to_user_is_active"=>0);
								$result['push_on'] = 1;
								$result['isCacheModified'] = 1;
								$result['oncall_user_id'] = $params['from_user'];
							}
			        break;
			    case "request_accept":
			        $data = array('from_user_status'=>0, "to_user_status"=>1,"from_user_is_active"=>0,"to_user_is_active"=>1);
			        $this->UserBatonRole->updateAll(array("status"=>-1, "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['from_user'],"role_id"=>$params['role_id'],'is_active'=>0,'status'=>1));
			        $result['isCacheModified'] = 1;
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];
			        $result['oncall_user_id'] = $params['to_user'];
			        $result['push_on'] = 1;
			        if($result['current_status'] != 2 && $result['current_status'] != 3 && $result['current_status'] != 4 && $result['current_status'] != 5  ){
			        	$vExecuteUpdate = "no";
			        }
			        break;
			    case "request_reject":
			        $data = array('from_user_status'=>6, "to_user_status"=>0,"from_user_is_active"=>0,"to_user_is_active"=>0);
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];

			        $this->UserBatonRole->updateAll(array("is_active"=>1, "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['from_user'],"role_id"=>$params['role_id'],'is_active'=>0,'status'=>1));
			        $result['isCacheModified'] = 1;
			        if($result['current_status'] != 2 && $result['current_status'] != 3 && $result['current_status'] != 4 && $result['current_status'] != 5  ){
			        	$vExecuteUpdate = "no";
			        }
			        break;
			    case "revoke_request":

			        $data = array('from_user_status'=>0, "to_user_status"=>1,"from_user_is_active"=>0,"to_user_is_active"=>1);
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];
			        if($params['to_user'] == 0){
						// $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$params['role_id'],'DepartmentsBatonRole.institute_id'=>$params['institute_id']));
			        	if($result['current_status'] != 1){
							$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$params['role_id']));
						}
					}
			        if($result['current_status'] != 2 && $result['current_status'] != 3 && $result['current_status'] != 4 && $result['current_status'] != 5  ){
			        	$vExecuteUpdate = "no";
			        }
			        break;
			    case "revoke_transfer":
			        $data = array('from_user_status'=>1, "to_user_status"=>0,"from_user_is_active"=>1,"to_user_is_active"=>0);
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];
			        $result['current_status'] = $getFromUserData['UserBatonRole']['status'];
			        $result['current_active_state'] = $getFromUserData['UserBatonRole']['is_active'];
			        if($result['current_status'] != 2 && $result['current_status'] != 3 && $result['current_status'] != 4 && $result['current_status'] != 5  ){
			        	$vExecuteUpdate = "no";
			        }
			        $result['check'] = $result['current_status'];
			        $result['vExecuteUpdate'] = $vExecuteUpdate;
			        break;
			    case "qr_transfer":
			        $data = array('from_user_status'=>1, "to_user_status"=>0,"from_user_is_active"=>1,"to_user_is_active"=>0);
			        $result['user_id'] = $params['to_user'];
			        $result['from_user_id'] = $params['from_user'];
			        $result['oncall_user_id'] = $params['from_user'];
							$result['isCacheModified'] = 1;
							break;
			}

			try{

				if($params['request_type'] == "qr_transfer")
				{
					$getFromUserData = $this->UserBatonRole->find("first", array("conditions"=> array("from_user"=> $params['from_user'], "role_id"=>$params['role_id'])));
					if(!empty($getFromUserData))
					{
						$this->UserBatonRole->updateAll(array("status"=> $data['from_user_status'], "is_active"=>$data['from_user_is_active'], "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['from_user'],"role_id"=>$params['role_id']));
					}
					else
					{
						$saveFromUserBatonRole = array("from_user"=>$params['from_user'], "to_user"=>$params['to_user'], "role_id"=> $params['role_id'], "status"=>$data['from_user_status'],"is_active"=>$data['from_user_is_active'], "notes"=>$note, "created_at"=> date("Y-m-d H:i:s"),  "updated_at"=> date("Y-m-d H:i:s"),  "deleted_at"=> date("Y-m-d H:i:s"));
							$this->UserBatonRole->saveAll($saveFromUserBatonRole);
					}
					$getToUserData = $this->UserBatonRole->find("first", array("conditions"=> array("from_user"=> $params['to_user'], "role_id"=>$params['role_id'])));
					if(!empty($getToUserData))
					{
						$this->UserBatonRole->updateAll(array("status"=> $data['to_user_status'], "is_active"=>$data['to_user_is_active'], "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['to_user'],"role_id"=>$params['role_id']));
					}
					else
					{
							$saveFromUserBatonRole = array("from_user"=>$params['to_user'], "to_user"=>$params['from_user'], "role_id"=> $params['role_id'], "status"=>$data['to_user_status'],"is_active"=>$data['to_user_is_active'], "notes"=>$note, "created_at"=> date("Y-m-d H:i:s"),  "updated_at"=> date("Y-m-d H:i:s"),  "deleted_at"=> date("Y-m-d H:i:s"));
							$this->UserBatonRole->saveAll($saveFromUserBatonRole);
					}
					$result['success'] = 1;

				}
				else
				{
					$getFromUserData = $this->UserBatonRole->find("first", array("conditions"=> array("from_user"=> $params['from_user'],'to_user'=>$params['to_user'], "role_id"=>$params['role_id'],'status !='=>1)));
					if($vExecuteUpdate == "yes") {
						if(!empty($getFromUserData))
						{
							$this->UserBatonRole->updateAll(array("status"=> $data['from_user_status'], "is_active"=>$data['from_user_is_active'], "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['from_user'],"to_user"=>$params['to_user'],"role_id"=>$params['role_id'],'status !='=>1));
						}
						else
						{
							$saveFromUserBatonRole = array("from_user"=>$params['from_user'], "to_user"=>$params['to_user'], "role_id"=> $params['role_id'], "status"=>$data['from_user_status'],"is_active"=>$data['from_user_is_active'], "notes"=>$note, "created_at"=> date("Y-m-d H:i:s"),  "updated_at"=> date("Y-m-d H:i:s"),  "deleted_at"=> date("Y-m-d H:i:s"));
							$this->UserBatonRole->saveAll($saveFromUserBatonRole);
						}

						$getToUserData = $this->UserBatonRole->find("first", array("conditions"=> array("from_user"=> $params['to_user'],'to_user'=>$params['from_user'], "role_id"=>$params['role_id'],'status !='=>1)));
						if(!empty($getToUserData))
						{
							$this->UserBatonRole->updateAll(array("status"=> $data['to_user_status'], "is_active"=>$data['to_user_is_active'], "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['to_user'],"to_user"=>$params['from_user'],"role_id"=>$params['role_id'],'status !='=>1));
						}
						else
						{
							$saveToUserBatonRole = array("from_user"=>$params['to_user'], "to_user"=>$params['from_user'], "role_id"=> $params['role_id'], "status"=>$data['to_user_status'],"is_active"=>$data['to_user_is_active'], "notes"=>$note, "created_at"=> date("Y-m-d H:i:s"),  "updated_at"=> date("Y-m-d H:i:s"),  "deleted_at"=> date("Y-m-d H:i:s"));
							$this->UserBatonRole->saveAll($saveToUserBatonRole);
						}
						$result['success'] = 1;

					} else {
						$result['success'] = 0;
					}
				}
			}catch( Exception $e ){
				$result['error'] = $e;
			}
		}

		return $result;
	}

	/*
	--------------------------------------------------------------------------
	On: 07-03-2019
	I/P: JSON
	O/P: JSON, user lists
	Desc: Formatting static directory data
	--------------------------------------------------------------------------
	*/
	public function batonRoleListDataFormat( $batonRoleData = array())
	{
		$batonRoleDataList = array();
		if(!empty($batonRoleData))
		{
			foreach ($batonRoleData as  $batonRoleDetail) {
				$batonRoleDataList[] = array(
						'role_id'=>	isset($batonRoleDetail['BatonRole']['id']) ? $batonRoleDetail['BatonRole']['id'] : "0",
						'role_name'=> isset($batonRoleDetail['BatonRole']['baton_roles']) ? $batonRoleDetail['BatonRole']['baton_roles'] : "",
						'on_call_value'=> isset($batonRoleDetail['DepartmentsBatonRole']['on_call_value']) ? $batonRoleDetail['DepartmentsBatonRole']['on_call_value'] : "0",
						);

			}
			return $batonRoleDataList;
		}
	}

	/*
	--------------------------------------------------------------------------
	On: 07-03-2019
	I/P: JSON
	O/P: JSON, user lists
	Desc: Formatting static directory data
	--------------------------------------------------------------------------
	*/


	public function getUserActiveBatonRoles()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			// if( $this->validateToken() && $this->validateAccessKey() ){
				if(!empty($dataInput)){
					try{
						if(isset($dataInput['user_id']) && isset($dataInput['user_id'])){
							$userData = $this->getActiveBatonRoles($dataInput['user_id']);
							echo "<pre>";print_r($userData);exit();
						}else{
							$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "611", 'message'=> ERROR_611);
						}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{

					$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "657", 'message'=> ERROR_657);
				}

			// }else{
			// 	$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	public function getActiveBatonRoles($userId)
	{
		if(!empty($userId))
		{
			$counter = 0;
			$userBatonRoleDataList = array();
			$getUserBatonRoleData = $this->UserBatonRole->find("all", array("conditions"=> array("from_user"=> $userId, "status"=>"1","is_active"=>"1")));
			if(!empty($getUserBatonRoleData))
			{
				foreach ($getUserBatonRoleData as $value) {
					if($value['UserBatonRole']['status'] == 1)
					{
						$counter++;
					}

					$batonRoleData = $this->BatonRole->find("first", array("conditions"=> array("id"=> $value['UserBatonRole']['role_id'])));

					$userBatonRoleDataList[] = array(
					'id'=> isset($batonRoleData['BatonRole']['id']) ? $batonRoleData['BatonRole']['baton_roles'] : 0,
					'role_name'=>	isset($batonRoleData['BatonRole']['baton_roles']) ? $batonRoleData['BatonRole']['baton_roles'] : 0,
					'status'=> isset($value['UserBatonRole']['status']) ? $value['UserBatonRole']['status'] : 0,
					);
				}
				return $counter;
			}
		}
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 12-03-2019
	I/P: JSON
	O/P: JSON
	Desc: Update at work status for user on qb
	----------------------------------------------------------------------------------------------
	*/

	public function updateOnCallStatusRoleExchange($userId)
	{
		$response = array();
		$customData = array();
		$is_import = "true";
		$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $userId['user_id'])));
		$userEmail = $userData['User']['email'];

		//** One time token to validate for QB[START]
		$oneTimeTokenForOnCall = $this->genrateRandomToken($userId['user_id']);
		$oneTimeTokenDataOnCall = array("user_id"=> $userId['user_id'], "token"=> $oneTimeTokenForOnCall);
		$this->UserOneTimeToken->saveAll($oneTimeTokenDataOnCall);
		$password = $oneTimeTokenForOnCall;
		//** One time token to validate for QB[END]
		$dutyVal  = $userId['user_duty_val'];
		if($userId['on_call_value'] == 0)
		{
			$duty = ($dutyVal == 1) ? "1" : "0";
			$at_work = "1";
		}
		else
		{
			$duty = "1";
			$at_work = "1";
		}
		$tokenDetails = $this->Quickblox->quickLogin($userEmail, $password);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
		$custom_data_from_api = json_decode($user_details->user->custom_data);
		$customData['userRoleStatus'] = $custom_data_from_api->status;
		$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
		$customData['isImport'] = $is_import;
		$customData['at_work'] = $at_work;
		$customData['on_call'] = $duty;
		$customData['dnd_status'] = $custom_data_from_api->dnd_status;
		if(!empty($customData['dnd_status']))
		{
			$customData['is_dnd_active'] = 1;
		}
		if(!empty($token)){
			$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
			$res = json_decode(json_encode($qbResponce));
			if(!empty($res['errors'])){
				$response['qberror'] = $qbResponce;
			}
			else
			{
				$response['qbsuccess'] = 1;
			}
		}
		else{
			$response['qberror'] = $tokenDetails;
		}
		return $response;
	}

	public function updateUserBatonRoleStatus()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			// if( $this->validateToken() && $this->validateAccessKey() ){
				if(!empty($dataInput)){
					try{
						if(isset($dataInput['from_user']) && isset($dataInput['to_user'])){
							$header = getallheaders();
							$params['device_id'] = $dataInput['device_id'];
							$params['from_user'] = $dataInput['from_user'];
							$params['to_user'] = $dataInput['to_user'];
							$params['role_id'] = $dataInput['role_id'];
							$params['type'] = $dataInput['type'];
							$params['request_type'] = $dataInput['request_type'];
							$params['institute_id'] = $dataInput['institute_id'];
							$params['note'] = $dataInput['note'];
							$params['on_call_value'] = $dataInput['on_call_value'];
							$userData = $this->updateUsersBatonRole($params);
							if($userData['success'] == 1)
							{
								$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_200, 'custom_msg'=>$userData['msg']);
								if($userData['isCacheModified'] == 1)
								{
									//Update directory dynamic data for current user
									$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $params['from_user'], "company_id"=> $params['institute_id'], "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

									if(!empty($isDynamicDataExist))
									{
										$this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($params['from_user'], $params['institute_id'], 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
									}
									else
									{
										$saveCacheLastModifiedData = array("user_id"=> $params['from_user'], "company_id"=> $params['institute_id'], "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>date('Y-m-d H:i:s'));
										$this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
									}
									//** Get last modified date dynamic directory[START]
									$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($params['institute_id']);
									if(!empty($getDynamicDirectoryLastModified)){
										$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
										$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$params['institute_id'];
										$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
									}


									//Update directory dynamic data for other user

									$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $params['to_user'], "company_id"=> $params['institute_id'], "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

									if(!empty($isDynamicDataExist))
									{
										$this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($params['to_user'], $params['institute_id'], 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
									}
									else
									{
										$saveCacheLastModifiedData = array("user_id"=> $params['to_user'], "company_id"=> $params['institute_id'], "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>date('Y-m-d H:i:s'));
										$this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
									}
									//** Get last modified date dynamic directory[START]
									$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($params['institute_id']);
									if(!empty($getDynamicDirectoryLastModified)){
										$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
										$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$params['institute_id'];
										$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
									}
								}
								//Send Voip Push notification
								$params['user_id'] = $userData['user_id'];
								$params['from_user_id'] = $userData['from_user_id'];
								$params['push_on'] = $userData['push_on'];
								$params['is_qr_request'] = $dataInput['is_qr_request'];
								$params['role_id'] = $dataInput['role_id'];
								$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

								//Save transactions
								$saveRoleExchangeTransactions = $this->UserBatonRoleTransaction->saveRoleExchangeTransactions($params);
								//update Custom Baton Role
								$cusParams['to_user'] = $dataInput['to_user'];
								$cusParams['from_user'] = $dataInput['from_user'];
								$updateCustomBatonRole = $this->getUserBatonRolesData($cusParams);

								//Update user On call status
								if(!empty($userData['oncall_user_id']))
								{
									$paramsVal['user_id'] =  $userData['oncall_user_id'];
									$paramsVal['company_id'] =  $params['institute_id'];
									$paramsVal['on_call_value'] =  $params['on_call_value'];
									$getUserOncallStatus = $this->UserDutyLog->userDutyStatus($paramsVal);
									$paramsVal['user_duty_val'] =  $getUserOncallStatus['UserDutyLog']['status'];
									$updateOncallOnQb = $this->updateOnCallStatusRoleExchange($paramsVal);
									if($updateOncallOnQb['qbsuccess'] == 1)
									{
										$dutyStatus = $getUserOncallStatus['UserDutyLog']['status'];
										if($params['on_call_value'] == 0)
										{
											$duty = ($dutyStatus == 1) ? 1 : 0;
											$at_work = 1;
										}
										else
										{
											$duty = 1;
											$at_work = 1;
										}
										$this->UserDutyLog->updateAll(array("status"=> $duty, "atwork_status"=>$at_work), array("user_id"=>$paramsVal['user_id'],"hospital_id"=>$paramsVal['company_id']));
									}
								}
								$deviceType =  $this->getDeviceType();
								if($deviceType == "MBWEB" || $deviceType == "mbweb")
								{
									$getUserDndStatus = $this->UserDndStatus->getUserDndStatus($dataInput['from_user']);
									$params['user_id'] = $dataInput['from_user'];
									$params['at_work_status'] = $at_work;

									if(empty($getUserDndStatus) || (!empty($getUserDndStatus) && $getUserDndStatus['UserDndStatus']['is_active'] == 0) )
									{
										if($params['request_type'] == 'request_accept' || $params['request_type'] == 'transfer_accept')
										{
											$params['push_on'] = 1;
											$sendAtWorkPushNotification = $this->sendAtWorkPushNotification($params);
										}
									}
								}

							}
							else
							{
								$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "615", 'message'=> "", 'error'=>$userData['error']);
							}
						}else{
							$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "611", 'message'=> ERROR_611);
						}

					}catch( Exception $e ){
						$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $userData['error']);
					}
				}else{

					$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "657", 'message'=> ERROR_657);
				}

			// }else{
			// 	$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'updateUserBatonRoleStatus', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------
	ON: 24-04-2019
	I/P: $params
	O/P: array
	Desc: This function will return baton roles institute wise
	-------------------------------------------------------------------------
	*/


	public function getBatonRoleList()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$params['institute_id'] = $dataInput['institute_id'];
			// if( $this->validateToken() && $this->validateAccessKey() ){
				try{

					$getUserBatonRoleDetail = $this->BatonRole->batonRoleList_v1($params);
					$listData = $this->batonRoleListDataFormat($getUserBatonRoleDetail);
					$listData = (!empty($listData))  ? $listData : "";
					$responseData = array('method_name'=> 'getBatonRoleList', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $listData);
				}catch( Exception $e ){
					$responseData = array('method_name'=> 'getBatonRoleList', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
				}

			// }else{
			// 	$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'getBatonRoleList', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------
	ON: 11-04-2019
	I/P: $params
	O/P: array
	Desc: This function use to save parmanent role
	-------------------------------------------------------------------------
	*/

	public function addPermanentRole()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			try{
				$saveUserPermanentRole = array("user_id"=>$dataInput['user_id'], "other_user_id"=>"0", "institute_id"=> $dataInput['institute_id'], "user_role_id"=>"0","role"=>json_encode($dataInput['permanent_role']), "status"=>"1", "active"=> "1",  "created_at"=> date("Y-m-d H:i:s"),  "updated_at"=> date("Y-m-d H:i:s"));
						$this->UserPermanentRole->saveAll($saveUserPermanentRole);

				$role_id = $this->UserPermanentRole->getLastInsertId();
				$this->UserPermanentRole->updateAll(array("user_role_id"=> $role_id), array("id"=>$role_id));

				//Update User Pofile
				$cusParams['user_id'] = $dataInput['user_id'];
				$cusParams['other_user_id'] = 0;
				$cusParams['institute_id'] = $dataInput['institute_id'];
				$updateCustomPermanentRole = $this->getUserPermanentRolesData($cusParams);

				if($dataInput['user_id'] != 0){
					//Update directory dynamic data for current user
					$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'], "company_id"=> $dataInput['institute_id'], "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

					if(!empty($isDynamicDataExist))
					{
						$this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($dataInput['user_id'], $dataInput['institute_id'], 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
					}
					else
					{
						$saveCacheLastModifiedData = array("user_id"=>  $dataInput['user_id'], "company_id"=> $dataInput['institute_id'], "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>date('Y-m-d H:i:s'));
						$this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
					}
					//** Get last modified date dynamic directory[START]
					$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($dataInput['institute_id']);
					if(!empty($getDynamicDirectoryLastModified)){
						$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
						$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$dataInput['institute_id'];
						$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
					}
				}

				$responseData = array('method_name'=> 'addPermanentRole', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
				//Store permanent role transaction
				$transactionParams['user_id'] = $dataInput['user_id'];
				$transactionParams['other_user_id'] = "0";
				$transactionParams['institute_id'] = $dataInput['institute_id'];
				$transactionParams['user_role_id'] = $role_id;
				$transactionParams['role'] = json_encode($dataInput['permanent_role']);
				$transactionParams['status'] = "1";
				$transactionParams['active'] = "1";
				$transactionParams['action'] = "Added";
				$transactionParams['note'] = isset($dataInput['note']) ? $dataInput['note'] : "";
				$saveAvailableTransaction = $this->savePermanentRoleTransation($transactionParams);
			}catch( Exception $e ){
				$responseData = array('method_name'=> 'addPermanentRole', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
			}

		}else{
			$responseData = array('method_name'=> 'addPermanentRole', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------
	ON: 11-06-2019
	I/P: $params
	O/P: array
	Desc: Use to get user permanent Role
	-------------------------------------------------------------------------
	*/

	public function userPermanentRoleDetail()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$user_id = $dataInput['user_id'];
			$institute_id = $dataInput['institute_id'];
			// if( $this->validateToken() && $this->validateAccessKey() ){
				if(!empty($user_id) && !empty($institute_id)){
					try{

						$getUserPermanentRoleDetail = $this->UserPermanentRole->getUserPermanentRoleDetail($user_id,$institute_id);
						if(!empty($getUserPermanentRoleDetail))
						{
							$userData['UserPermanentRoles'] = $this->permanentRoleDataFormat($getUserPermanentRoleDetail);
						}
						else
						{
							$userData['UserPermanentRoles'] = array();
						}
						$custom_dimension_data = $this->getRoleTagForCustomDimension($user_id,$institute_id);
						$userData['custom_analytic_data'] = json_decode($custom_dimension_data, true);
						$responseData = array('method_name'=> 'userPermanentRoleDetail', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $userData);
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'userPermanentRoleDetail', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{

					$responseData = array('method_name'=> 'userPermanentRoleDetail', 'status'=>"0", 'response_code'=> "657", 'message'=> ERROR_657);
				}

			// }else{
			// 	$responseData = array('method_name'=> 'userPermanentRoleDetail', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'userPermanentRoleDetail', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	--------------------------------------------------------------------------
	On: 11-06-2019
	I/P: JSON
	O/P: JSON, user lists
	Desc: Formatting permanent Data
	--------------------------------------------------------------------------
	*/

	public function permanentRoleDataFormat( $permanentRoleDetails = array())
	{
		$permanentRoleDataList = array();
		if(!empty($permanentRoleDetails))
		{
			foreach ($permanentRoleDetails as  $permanentRoleDetail) {
				$otherUserFullname = $permanentRoleDetail['UserProfiles']['first_name']." ".$permanentRoleDetail['UserProfiles']['last_name'];

				$updated_at = $permanentRoleDetail['UserPermanentRole']['updated_at'];
				$createdTimeStamp = strtotime($updated_at);
				$timeoutInSecond = 60*60;
				$duration = $createdTimeStamp+$timeoutInSecond;
				if(!empty($permanentRoleDetail['UserPermanentRole']['role']))
				{
					$roleName = json_decode($permanentRoleDetail['UserPermanentRole']['role'], true);
				}
				else
				{
					$roleName = array();
				}
				$roleName = json_decode($permanentRoleDetail['UserPermanentRole']['role']);
				$permanentRoleDataList[] = array(
						'other_user_id'=> isset($permanentRoleDetail['UserProfiles']['user_id']) ? $permanentRoleDetail['UserProfiles']['user_id'] : 0,
						'profile_img'=> !empty($permanentRoleDetail['UserProfiles']['profile_img']) ? AMAZON_PATH . $permanentRoleDetail['UserProfiles']['user_id']. '/profile/' . $permanentRoleDetail['UserProfiles']['profile_img']:'',
						'thumbnail_img'=> !empty($permanentRoleDetail['UserProfiles']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $permanentRoleDetail['UserProfiles']['user_id']. '/profile/' . $permanentRoleDetail['UserProfiles']['thumbnail_img']:'',
						'other_user_name'=> $otherUserFullname,
						'role_name'=> $roleName,
						// 'role_id'=> isset($permanentRoleDetail['UserPermanentRole']['user_role_id']) ? $permanentRoleDetail['UserPermanentRole']['user_role_id'] : 0,
						'role_id'=> isset($permanentRoleDetail['UserPermanentRole']['id']) ? $permanentRoleDetail['UserPermanentRole']['id'] : 0,
						'duration'=> isset($duration) ? $duration : 0,
						'type'=>isset($permanentRoleDetail['UserPermanentRole']['status']) ? $permanentRoleDetail['UserPermanentRole']['status'] : 0,
						'note'=>isset($permanentRoleDetail['UserPermanentRole']['notes']) ? $permanentRoleDetail['UserPermanentRole']['notes'] : "",
						'other_user_on_call'=>isset($permanentRoleDetail['UserDutyLog']['status']) ? $permanentRoleDetail['UserDutyLog']['status'] : "",
						'other_user_available'=>isset($permanentRoleDetail['UserDutyLog']['atwork_status']) ? $permanentRoleDetail['UserDutyLog']['atwork_status'] : "",
						'updated_at'=> isset($permanentRoleDetail['UserPermanentRole']['updated_at']) ? $permanentRoleDetail['UserPermanentRole']['updated_at'] : "",
						);

			}
			return $permanentRoleDataList;
		}
	}


	/*
	--------------------------------------------------------------------------
	On: 11-06-2019
	I/P: JSON
	O/P: JSON, user lists
	Desc: Update User permanent Role
	--------------------------------------------------------------------------
	*/

	public function updateUserPermanentRoleStatus()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			// if( $this->validateToken() && $this->validateAccessKey() ){
				if(!empty($dataInput)){
					try{
						if(isset($dataInput['user_id']) && isset($dataInput['user_id'])){
							$params['user_id'] = $dataInput['user_id'];
							$params['other_user_id'] = $dataInput['other_user_id'];
							$params['role_id'] = $dataInput['role_id'];
							$params['request_type'] = $dataInput['request_type'];
							$params['institute_id'] = $dataInput['institute_id'];
							$params['note'] = $dataInput['note'];
							$params['permanent_role'] = $dataInput['permanent_role'];
							$userData = $this->updateUsersPermanentRole($params);
							if($userData['success'] == 1)
							{
								$responseData = array('method_name'=> 'updateUserPermanentRoleStatus', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_200);


								///////////////1111111111///////////////

								if($userData['isCacheModified'] == 1)
								{
									if($params['user_id'] != 0){
										//Update directory dynamic data for current user
										$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "company_id"=> $params['institute_id'], "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

										if(!empty($isDynamicDataExist))
										{
											$this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($params['user_id'], $params['institute_id'], 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
										}
										else
										{
											$saveCacheLastModifiedData = array("user_id"=> $params['user_id'], "company_id"=> $params['institute_id'], "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>date('Y-m-d H:i:s'));
											$this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
										}
										//** Get last modified date dynamic directory[START]
										$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($params['institute_id']);
										if(!empty($getDynamicDirectoryLastModified)){
											$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
											$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$params['institute_id'];
											$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
										}
									}
									if($params['other_user_id'] != 0){
										//Update directory dynamic data for other user

										$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $params['other_user_id'], "company_id"=> $params['institute_id'], "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

										if(!empty($isDynamicDataExist))
										{
											$this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($params['other_user_id'], $params['institute_id'], 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
										}
										else
										{
											$saveCacheLastModifiedData = array("user_id"=> $params['other_user_id'], "company_id"=> $params['institute_id'], "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>date('Y-m-d H:i:s'));
											$this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
										}
										//** Get last modified date dynamic directory[START]
										$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($params['institute_id']);
										if(!empty($getDynamicDirectoryLastModified)){
											$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
											$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$params['institute_id'];
											$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
										}
									}
								}


								//Send Voip Push notification

								$params['user_id'] = $userData['user_id'];
								$params['other_user_id'] = $userData['other_user_id'];
								$sendRoleExchangePushNotification = $this->sendPermanentRoleExchangePushNotification($params);

								//Save transactions
								// $saveRoleExchangeTransactions = $this->UserBatonRoleTransaction->saveRoleExchangeTransactions($params);


								// update Custom Permanent Role
								$cusParams['user_id'] = $userData['user_id'];
								$cusParams['other_user_id'] = $userData['other_user_id'];
								$cusParams['institute_id'] = $dataInput['institute_id'];
								$updateCustomPermanentRole = $this->getUserPermanentRolesData($cusParams);

								$transactionParams['user_id'] = $dataInput['user_id'];
								$transactionParams['other_user_id'] = $dataInput['other_user_id'];;
								$transactionParams['institute_id'] = $dataInput['institute_id'];
								$transactionParams['user_role_id'] = $dataInput['role_id'];
								$transactionParams['role'] = json_encode($dataInput['permanent_role']);
								$transactionParams['status'] = "1";
								$transactionParams['active'] = "1";
								$transactionParams['action'] = $params['request_type'];
								$transactionParams['note'] = isset($dataInput['note']) ? $dataInput['note'] : "";
								$saveAvailableTransaction = $this->savePermanentRoleTransation($transactionParams);

							}
							else
							{
								$responseData = array('method_name'=> 'updateUserPermanentRoleStatus', 'status'=>"0", 'response_code'=> "615", "message"=>ERROR_615, "error_data"=>$userData['error']);
							}
						}else{
							$responseData = array('method_name'=> 'updateUserPermanentRoleStatus', 'status'=>"0", 'response_code'=> "611", 'message'=> ERROR_611);
						}

					}catch( Exception $e ){
						$responseData = array('method_name'=> 'updateUserPermanentRoleStatus', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{

					$responseData = array('method_name'=> 'updateUserPermanentRoleStatus', 'status'=>"0", 'response_code'=> "657", 'message'=> ERROR_657);
				}

			// }else{
			// 	$responseData = array('method_name'=> 'updateUserPermanentRoleStatus', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'updateUserPermanentRoleStatus', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------
	ON: 07-03-2019
	I/P: $params
	O/P: array
	Desc: This function will update the user role status
	Role Status value:
	0=>inactive
	1=>active
	2=>transfer awaiting
	3=>incoming transfer request
	4=>transfer accepted
	5=>transfer rejected
	-------------------------------------------------------------------------
	*/

	public function updateUsersPermanentRole($params = array())
	{
		$responseData = array();
		$currentDateFormate = date('Y-m-d H:i:s');
		$note = isset($params['note']) ? $params['note'] : "";
		if(!empty($params)){
			if($params['request_type'] == "transfer"){
				$getFromUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "id"=>$params['role_id'])));
				if(!empty($getFromUserData)){
					$this->UserPermanentRole->updateAll(
						array("other_user_id"=>$params['other_user_id'], "status"=> "2", "active"=>"1", "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"),
						array("user_id"=>$params['user_id'],"id"=>$params['role_id'])
					);

					$saveToUserPermanentRole = array("user_id"=>$params['other_user_id'],
																						"other_user_id"=>$params['user_id'],
																						"institute_id"=> $params['institute_id'],
																						"user_role_id"=>$params['role_id'],
																						"role"=>json_encode($params['permanent_role']),
																						"status"=>"3",
																						"active"=>"1",
																						"notes"=>$note,
																						"created_at"=> date("Y-m-d H:i:s"),
																						"updated_at"=> date("Y-m-d H:i:s")
																					);
					$this->UserPermanentRole->saveAll($saveToUserPermanentRole);
				}
				$responseData['success'] = 1;
				$responseData['user_id'] = $params['user_id'];
				$responseData['other_user_id'] = $params['other_user_id'];

			}elseif ($params['request_type'] == "transfer_accept") {
				$getotherUserData = array();
				$getFromUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "id"=>$params['role_id'])));
				if(!empty($getFromUserData)){
					$this->UserPermanentRole->updateAll(array("status"=> "1", "active"=>"1", "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("user_id"=>$params['user_id'],"id"=>$params['role_id']));

					$getotherUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['other_user_id'], "id"=>$getFromUserData['UserPermanentRole']['user_role_id'])));
				}
				if(!empty($getotherUserData))
				{
					$this->UserPermanentRole->updateAll(array("status"=> "4", "active"=>"1", "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("user_id"=>$params['other_user_id'],"id"=>$getFromUserData['UserPermanentRole']['user_role_id']));
				}
				$responseData['success'] = 1;
				$responseData['user_id'] = $params['user_id'];
				$responseData['other_user_id'] = $params['other_user_id'];
				$responseData['isCacheModified'] = 1;


			}elseif ($params['request_type'] == "transfer_rejected") {
				$getotherUserData = array();
				$getFromUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "id"=>$params['role_id'])));
				if(!empty($getFromUserData)){
					$getotherUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['other_user_id'], "id"=>$getFromUserData['UserPermanentRole']['user_role_id'])));

					$deleteRecords = $this->UserPermanentRole->deleteAll(array("user_id"=>$params['user_id'], "id"=>$params['role_id']));
				}
				if(!empty($getotherUserData))
				{
					$this->UserPermanentRole->updateAll(array("status"=> "1", "active"=>"1", "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("user_id"=>$params['other_user_id'],"id"=>$getFromUserData['UserPermanentRole']['user_role_id']));
				}
				$responseData['success'] = 1;
				$responseData['user_id'] = $params['user_id'];
				$responseData['other_user_id'] = $params['other_user_id'];

			}elseif ($params['request_type'] == "revoke_transfer") {
				$getotherUserData = array();

				$getFromUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "id"=>$params['role_id'])));
				if(!empty($getFromUserData)){
					$this->UserPermanentRole->updateAll(array("status"=> "1", "active"=>"1", "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("user_id"=>$params['user_id'],"id"=>$params['role_id']));
				}
				$getotherUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['other_user_id'], "user_role_id"=>$params['role_id'])));
				if(!empty($getotherUserData))
				{
					$deleteRecords = $this->UserPermanentRole->deleteAll(array("user_id"=>$params['other_user_id'], "user_role_id"=>$params['role_id'], "status"=>3));
				}
				$responseData['success'] = 1;
				$responseData['user_id'] = $params['user_id'];
				$responseData['other_user_id'] = $params['other_user_id'];

			}elseif ($params['request_type'] == "delete") {
				$getFromUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "id"=>$params['role_id'])));
				if(!empty($getFromUserData))
				{
					$deleteRecords = $this->UserPermanentRole->deleteAll(array("user_id"=>$params['user_id'], "id"=>$params['role_id']));
				}
				$responseData['user_id'] = $params['user_id'];
				$responseData['success'] = 1;
				$responseData['isCacheModified'] = 1;
				$responseData['other_user_id'] = 0;
			}

			elseif ($params['request_type'] == "inactive") {
				$getFromUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "id"=>$params['role_id'])));
				if(!empty($getFromUserData))
				{
					$this->UserPermanentRole->updateAll(array("status"=> "0", "active"=>"1", "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("user_id"=>$params['user_id'],"id"=>$params['role_id']));
				}
				$responseData['user_id'] = $params['user_id'];
				$responseData['success'] = 1;
				$responseData['isCacheModified'] = 1;
				$responseData['other_user_id'] = 0;
			}

			elseif ($params['request_type'] == "active") {
				$getFromUserData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "id"=>$params['role_id'])));
				if(!empty($getFromUserData))
				{
					$this->UserPermanentRole->updateAll(array("status"=> "1", "active"=>"1", "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("user_id"=>$params['user_id'],"id"=>$params['role_id']));
				}
				$responseData['user_id'] = $params['user_id'];
				$responseData['success'] = 1;
				$responseData['isCacheModified'] = 1;
				$responseData['other_user_id'] = 0;
			}

		}else{
			$responseData['error'] = 0;
		}
		return $responseData;
	}

	public function getUserPermanentRoleData()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$user_id = $dataInput['user_id'];
			// if( $this->validateToken() && $this->validateAccessKey() ){
				if(!empty($user_id)){
					try{
						// $getFromUserData = $this->UserPermanentRole->find("all", array("conditions"=> array("user_id"=> $user_id)));
						$userpermanentRolestatus = array(1,2);
						$permanentRoleDetail = $this->UserPermanentRole->find("all", array("conditions"=> array("user_id"=> $user_id, "status"=>$userpermanentRolestatus,"active"=>1)));
						echo "<pre>";print_r($permanentRoleDetail);exit();
						foreach($getFromUserData as $getFromUser)
						{
							$rows[] =  json_decode($getFromUser['UserPermanentRole']['role']);
						}
						$responseData = array('method_name'=> 'userl', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $rows);
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'use', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{

					$responseData = array('method_name'=> 'getUse', 'status'=>"0", 'response_code'=> "657", 'message'=> ERROR_657);
				}

			// }else{
			// 	$responseData = array('method_name'=> 'userPermanentRoleDetail', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'getUserPe', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	------------------------------------------------------------------------------
	ON: 09-09-2019
	I/P: $params
	O/P: JSON
	Desc: Save Permanent Role Transactions
	------------------------------------------------------------------------------
	*/

	public function savePermanentRoleTransation($params = array())
	{
		$response = 0;
		if( !empty($params) )
		{
			$saveTransactionData = array("user_id"=> $params['user_id'], "other_user_id"=> $params['other_user_id'], "institute_id"=>$params['institute_id'], "user_role_id"=>$params['user_role_id'], "role"=>$params['role'], "status"=>$params['status'], "active"=>$params['active'], "note"=>$params['note'], "action"=>$params['action'],'created_at'=> date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s'));
	        $this->UserPermanentRolesTransaction->saveAll($saveTransactionData);
	        $response = 1;
		}
		return $response;
	}

}
