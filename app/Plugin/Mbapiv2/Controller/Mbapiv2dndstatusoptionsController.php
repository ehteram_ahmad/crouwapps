<?php
/*
 * DndStatus controller.
 * Created By Ehteram Ahmad
 * On 12 FEB 2019
 */
App::uses('AppController', 'Controller');


class Mbapiv2dndstatusoptionsController extends AppController {
	public $uses = array('Mbapiv2.User','Mbapiv2.UserDndStatus','Mbapiv2.DndStatusOption', 'Mbapiv2.VoipPushNotification', 'UserQbDetail','Mbapiv2.UserOneTimeToken','Mbapiv2.DndTransaction');
	public $components = array('Common','Cache','Quickblox');


	/*
	-------------------------------------------------------------------------
	ON: 12-02-2019
	I/P: JSON (institute_id)
	O/P: JSON (success/fail)
	Desc: Will return list of all active Do not disturb detail.
	-------------------------------------------------------------------------
	*/

	public function getDndStatusOption(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$instituteId = $dataInput['institute_id'];
			if( $this->validateToken() && $this->validateAccessKey() ){
				if(!empty($instituteId)){
					try{
						$cacObj = $this->Cache->redisConn();
						if(($cacObj['connection']) && empty($cacObj['errors'])){
							if($cacObj['robj']->exists('institutionDndStatusOptions:'.$instituteId)){
								$dataJson = $cacObj['robj']->get('institutionDndStatusOptions:'.$instituteId);
								$dndDetailData = json_decode($dataJson, true);
								$responseData = array('method_name'=> 'getDndStatusOption', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_200, 'data'=>$dndDetailData);
							}
							else
							{
								$dndStatusOptionList = $this->DndStatusOption->getDndStatusOptionModel($instituteId);
								$dndDetailData = $this->getDndStatusOptionDataFormat($dndStatusOptionList);
								$key = 'institutionDndStatusOptions:'.$instituteId;
								$cacObj['robj']->set($key,json_encode($dndDetailData, true));
								$responseData = array('method_name'=> 'getDndStatusOption', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_200, 'data'=>$dndDetailData);
							}
						}
						else
						{
							$responseData = array('method_name'=> 'getDndStatusOption', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
						}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'getDndStatusOption', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{

					$responseData = array('method_name'=> 'getDndStatusOption', 'status'=>"0", 'response_code'=> "657", 'message'=> ERROR_657);
				}

			}else{
				$responseData = array('method_name'=> 'getDndStatusOption', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'getDndStatusOption', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	--------------------------------------------------------------------------
	ON: 12-02-2019
	I/P: data array $dndStatusOptionList
	O/P: array
	Desc: Will return list of all active Do not disturb detail.
	--------------------------------------------------------------------------
	*/
	public function getDndStatusOptionDataFormat( $dndStatusOptionList = array()){
		$dndList = array();
		if( !empty($dndStatusOptionList) ){
			foreach( $dndStatusOptionList as $dndStatusOption ){
				$dndList[] = array(
					'dnd_status_id'=> $dndStatusOption['DndStatusOption']['id'],
					'dnd_name'=>	$dndStatusOption['DndStatusOption']['dnd_name'],
					'icon_selected'=> $dndStatusOption['DndStatusOption']['icon_selected'],
					'icon_unselected'=> $dndStatusOption['DndStatusOption']['icon_unselected'],
					'duration'=>unserialize($dndStatusOption['DndStatusOption']['duration'])
					);
				}
			}
		return $dndList;
	}

	/*
	--------------------------------------------------------------------------
	ON: 12-02-2019
	I/P:
	O/P: True or False
	Desc: This function will send a voip/fcm push notification to dissmiss
		  the Dnd status options.
	--------------------------------------------------------------------------
	*/

	public function dismissDndStatusOptions()
	{
		$currentTime = time();
		$message = 'Dissmiss Dnd';
		$conditions = array(
						'UserDndStatus.is_active' => 1
					);
		$joins = array(
				array(
					'table' => 'voip_push_notifications',
					'alias' => 'VoipPushNotification',
					'type' => 'left',
					'conditions'=> array('UserDndStatus.user_id = VoipPushNotification.user_id')
				)
			);
		$options = array(
				'conditions'=>$conditions,
				'joins' => $joins,
				'fields' => array('UserDndStatus.user_id','UserDndStatus.institute_id','UserDndStatus.dnd_status_id','UserDndStatus.start_time','UserDndStatus.duration', 'UserDndStatus.end_time','VoipPushNotification.device_token','VoipPushNotification.fcm_voip_token','VoipPushNotification.device_type'),
			);
		$getUserDndStatus = $this->UserDndStatus->find('all',$options);

		foreach ($getUserDndStatus as $key => $value) {
			$val = $value['UserDndStatus']['end_time'];
			$userId = $value['UserDndStatus']['user_id'];
			$diff = $val-$currentTime;
			if($diff > 0)
			{
				// echo "Hi";exit();
				// No need to send voip/fcm push notification
			}
			else
			{
				// echo "Bye";exit();
				if(empty($value['VoipPushNotification']['fcm_voip_token']))
				{
					// echo "Hi";exit();
					// No need to send voip/fcm push notification
				}
				else
				{
					// echo "Bye";exit();
					$params['device_token'] = $value['VoipPushNotification']['device_token'];
					$params['fcm_voip_token'] = $value['VoipPushNotification']['fcm_voip_token'];
				    $params['notificationMessage'] = $message;
					if($value['VoipPushNotification']['device_type'] == 'ios' || $value['VoipPushNotification']['device_type'] == 'iOS')
					{
						$sendNotification = $this->sendDndPushNotificationOnIos($params);
						if($sendNotification == 1)
						{
							$params['user_id'] = $userId;
							$oneTimeTokenForDnd = $this->dissmissDndOnOcrAndQb($params);
							// $this->UserDndStatus->updateAll(array("is_active"=> 0, "duration"=> 0), array('user_id'=>$userId));
						}
					}
					else
					{
						$sendNotification = $this->sendDndPushNotificationAndroid($params);
						if($sendNotification == 1)
						{
							$params['user_id'] = $userId;
							$oneTimeTokenForDnd = $this->dissmissDndOnOcrAndQb($params);
							// $this->UserDndStatus->updateAll(array("is_active"=> 0, "duration"=> 0), array('user_id'=>$userId));
						}
					}
				}
				$this->DndTransaction->saveAll(array('user_id'=>$value['UserDndStatus']['user_id'],'institute_id'=>$value['UserDndStatus']['user_id'],"dnd_status_id"=>$value['UserDndStatus']['dnd_status_id'],"start_time"=>$value['UserDndStatus']['start_time'],"end_time"=>$value['UserDndStatus']['end_time'],"duration"=>$value['UserDndStatus']['duration'],"is_active"=>"0",'custom_data'=>'Dnd Expire','created'=>date("Y-m-d H:i:s")));
			}

		}
		echo "Done";exit();

	}

	public function sendDndPushNotificationOnIos($params = array())
	{
		if(!empty($params))
		{

			$deviceToken = $params['device_token'];
			$fcmVoipToken = $params['fcm_voip_token'];
			$passphrase = 'password';
			$message = 'Dissmiss Dnd';
			$pushOn = 1;
			$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "ios_voip"=>1,
                        "VOIPCall"=>1,
                        "VOIPDndStatus"=>0,
                        "push_on"=>$pushOn
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}


	public function sendDndPushNotificationAndroid($params = array()){
		if(!empty($params))
		{
			$responseData = $this->sendDndPushNotificationOnAndroid($params);
			return $responseData;
		}
	}

	public function sendDndPushNotificationOnAndroid($params = array())
	{
		if(!empty($params['fcm_voip_token'])){
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$pushOn = 1;
			$tokenRegistrationIds = array($dataUserDevice);
			$voipPushMessage = array
					(
						'VOIPDndStatus'=> 0,
						'VOIPCall'=> 1,
						'push_on'=>$pushOn
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $var = 1;
		}
		else
		{
			return $var = 0;
		}
	}
}
