<?php
/*
 * Post controller.
 *
 * This file will render views from views/postwebservices/
 *
 
 */

App::uses('AppController', 'Controller');


class Mbapiv2pagewebservicesController extends AppController {
	public $uses = array('Content');
	public $components = array('Common');
	/*
	On:
	I/P:
	O/P:
	Desc: Page path as a JSON response.
	*/
	public function pageContent(){
		$this->autoRender = false;
		$responseData = array();
		if($this->validateAccessKey()){
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$dataInput['requestFrom'] = 'mobile';
			$requestFrom = isset($dataInput['requestFrom']) ? $dataInput['requestFrom'] : '';
			if( $dataInput['page'] == 'tnc' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/tnc/' . $requestFrom );
				$pageContent = array('PageContent'=> 'https://medicbleep.com/termsMobile.html');
			}elseif( $dataInput['page'] == 'faq' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/faq/' . $requestFrom);
				// $pageContent = array('PageContent'=> BASE_URL . 'Index/faqMobile');
				$pageContent = array('PageContent'=> 'https://medicbleep.com/Index/faqMobile');
			}elseif( $dataInput['page'] == 'privacy' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/privacyPolicy/' . $requestFrom);
				$pageContent = array('PageContent'=> 'https://medicbleep.com/privacyPolicyMobile.html');
			}elseif( $dataInput['page'] == 'about' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/about/' . $requestFrom);
				// $pageContent = array('PageContent'=> BASE_URL . 'Index/aboutMobile');
				$pageContent = array('PageContent'=> 'https://medicbleep.com/Index/aboutMobile');
			}elseif( $dataInput['page'] == 'consentTermConditions' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/consentTermConditions/' . $requestFrom);
				// $pageContent = array('PageContent'=> BASE_URL . 'Index/consentTermConditionsMobile');
				$pageContent = array('PageContent'=> 'https://medicbleep.com/Index/consentTermConditionsMobile');
			}
			$responseData = array('method_name'=> 'pageContent', 'status'=>"1", 'response_code'=> '200', 'message'=> ERROR_200, 'data'=> $pageContent);	
		}else{
			$responseData = array('method_name'=> 'pageContent', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);	
		}
		echo json_encode($responseData);
		exit;
	}
	
}
