<?php
/*
Desc: API data get/post to Webservices.
*/

//App::uses('AppController', 'Controller');

class Mbapiv2geofenceController extends AppController {

	public $uses = array('Mbapiv2.User', 'Mbapiv2.UserProfile', 'Mbapiv2.CountryGroup', 'Mbapiv2.Country');
	public $components = array('Common');

	/*
	-------------------------------------------------------------------------------------------------------
	On: 13-09-2017
	I/P: JSON
	O/P: JSOn
	Desc: Get group country list for any user
	-------------------------------------------------------------------------------------------------------
	*/
	public function getGroupCountry(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() && $this->validateAccessKey() ){
					//** User Group country list[START]
					$countryGroups = array();
					$userData = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
					$userCountry = $this->Country->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['country_id'])));
					$countryGroupData = $this->CountryGroup->find("first", array("conditions"=> array('FIND_IN_SET(\''. $userCountry['Country']['country_code'] .'\',country_code_list)')));
					$countryGroupDataVal =  explode(",", $countryGroupData['CountryGroup']['country_code_list']);
					if(!empty($countryGroupData)){
						$countryGroups = array("GroupCountries"=> $countryGroupDataVal);
					}else{
						$countryGroups = array("GroupCountries"=> array($userCountry['Country']['country_code']));
					}
					$responseData = array('method_name'=> 'getGroupCountry', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $countryGroups); 
			    }else{
			         $responseData = array('method_name'=> 'getGroupCountry','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'getGroupCountry','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
    	exit;
	}

}// End Class
