<?php
/*
Desc: Track controller .
*/

//App::uses('AppController', 'Controller');

class Mbapiv2trackwebservicesController extends AppController {

	public $uses = array("Mbapiv2.UserDeviceInfo",'Mbapiv2.UserLoginTransaction','Mbapiv2.UserIpSetting','Mbapiv2.SessionTimeIp','Mbapiv2.SessionTimeIpRange','Mbapiv2.OverrideSilentMode','Mbapiv2.CalculatorVariable','Mbapiv2.InAppAlertTransaction');
	public $components = array('Common');

	/*
	-------------------------------------------------------------------------------------------------------
	On: 13-09-2017
	I/P: JSON
	O/P: JSOn
	Desc: Get group country list for any user
	-------------------------------------------------------------------------------------------------------
	*/
	public function setDeviceInfo(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() ){
					//** Store Track record[START]
					try{
						$deviceData = array(
											"user_id"=> $dataInput['device_info']['user_id'],
											"device_info"=> json_encode($dataInput['device_info']),
											"local_server_info"=> $this->Common->getFrontServerInfo()
											);
						$addData = $this->UserDeviceInfo->save($deviceData );
						$responseData = array('method_name'=> 'setDeviceInfo', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
						// try {
							$header = getallheaders();
							$token = $header['token'];

							$this->UserLoginTransaction->updateAll(array("UserLoginTransaction.device_info"=> "'".json_encode($dataInput['device_info'])."'"), array('UserLoginTransaction.token'=>$token,'UserLoginTransaction.login_status'=>'1','UserLoginTransaction.user_id'=>$dataInput['device_info']['user_id']));
						// } catch (Exception $e) {
						// 	$responseData = array('method_name'=> 'setDeviceInfo', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "message"=> $e->getMessage());
						// }
					}catch(Exception $e){
						$responseData = array('method_name'=> 'setDeviceInfo', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "message"=> $e->getMessage());
					}
					//** Store Track record[END]
			    }else{
			         $responseData = array('method_name'=> 'setDeviceInfo','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'setDeviceInfo','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
    	exit;
	}

	/*
	-------------------------------------------------------------------------------------------------------
	On: 13-12-2018
	I/P: JSON
	O/P: JSOn
	Desc: Get Local ip address user wise
	-------------------------------------------------------------------------------------------------------
	*/

	public function getDeviceInfo(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() ){
					$dataInput = $this->request->input ( 'json_decode', true);
					if( $this->User->find( "count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0 ){
						try {
							// $device_info = $this->UserIpSetting->find('all',array('conditions'=>array('user_id'=>$dataInput['user_id'],'company_id'=>$dataInput['institution_id'])));

							$userIp = $dataInput['local_ip'];
							$ip_time = 10;
							$companyId = $dataInput['institution_id'];
		          $normal_ips = $this->SessionTimeIp->find('all',array('conditions'=>array('SessionTimeIp.company_id'=> $companyId)));
		          $ip_range = $this->SessionTimeIpRange->find('all',array('conditions'=>array('SessionTimeIpRange.company_id'=> $companyId)));

							if(!empty($normal_ips)){
								foreach ($normal_ips as $value) {
									if((string)$userIp == (string)$value['SessionTimeIp']['ip']){
										$ip_time = $value['SessionTimeIp']['time_span'];
									}
								}
							}

		          if(!empty($ip_range)){
		            foreach ($ip_range as $value) {
									$from_ip = explode(".",$value['SessionTimeIpRange']['ip_from']);
									$to_ip = explode(".",$value['SessionTimeIpRange']['ip_to']);
									$user_ip = explode(".",$userIp);
									if($user_ip[0] == $from_ip[0] && $user_ip[1] == $from_ip[1] && $user_ip[2] == $from_ip[2]){
										if(($user_ip[3] <= $from_ip[3] && $user_ip[3] >= $to_ip[3]) || ($user_ip[3] <= $to_ip[3] && $user_ip[3] >= $from_ip[3])){
											$ip_time = $value['SessionTimeIpRange']['time_span'];
										}
									}
		            }
		          }

							$ip_data = array(
								'time_span' => $ip_time
							);
							$responseData = array('method_name'=> 'getDeviceInfo', 'status'=>"1", 'data'=> $ip_data, 'response_code'=> "200", 'message'=> ERROR_200);
						} catch (Exception $e) {
							$responseData = array('method_name'=> 'getDeviceInfo', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "message"=> $e->getMessage());
						}
					}else{
						$responseData = array('method_name'=> 'getDeviceInfo', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
					}
			    }else{
			        $responseData = array('method_name'=> 'getDeviceInfo','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		        }
	       }else{
			$responseData = array('method_name'=> 'getDeviceInfo','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
    	exit;
	}

	/*
	-------------------------------------------------------------------------------------------------------
	On: 22-04-2019
	I/P: JSON
	O/P: JSOn
	Desc: Override silent mode autid traila\s
	-------------------------------------------------------------------------------------------------------
	*/

	public function overrideSilentMode()
	{
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() && $this->validateAccessKey() ){
					try{
						$deviceData = array(
											"user_id"=> $dataInput['user_id'],
											"institute_id"=>$dataInput['institute_id'],
											"mode_status"=>$dataInput['mode_status'],
											"in_app_alert"=>$dataInput['in_app_alert'],
											"device_info"=> json_encode($dataInput['device_info'])
											);
						$addData = $this->OverrideSilentMode->save($deviceData );
						$responseData = array('method_name'=> 'overrideSilentMode', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
					}catch(Exception $e){
						$responseData = array('method_name'=> 'overrideSilentMode', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "message"=> $e->getMessage());
					}
				}else{
			         $responseData = array('method_name'=> 'overrideSilentMode','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		        }
			}else{
			$responseData = array('method_name'=> 'overrideSilentMode','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
    	exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 03-04-2017
	I/P: $activationToken,$userId
	O/P: Update Password
	Desc: Update Password on MB
	-------------------------------------------------------------------------------------
	*/
	public function calculator( $totalPopulation = NULL){
		$totalPopulation = isset($_REQUEST['totalPopulation']) ? $_REQUEST['totalPopulation'] : "";
		$this->layout = '';
		$responseData = array();
		if( !empty( $totalPopulation ) ){
			$calculatorData = $this->CalculatorVariable->find("first");
			if( !empty($calculatorData) ){
				try{
					//FTE Ward Nurses in Wales
					$fte_ward_nurses_in_wales = $calculatorData['CalculatorVariable']['fte_ward_nurses_in_wales'];
					//Population of wales
					$populationOfWalse = $calculatorData['CalculatorVariable']['population_of_walse'];
					//FTE Ward Doctor in Wales
					$fteJuniorDoctorsInWales = $calculatorData['CalculatorVariable']['fte_junior_doctors_in_wales'];
					$jnl = ($totalPopulation*$fte_ward_nurses_in_wales)/$populationOfWalse;
					$jdl = ($fteJuniorDoctorsInWales*$totalPopulation)/$populationOfWalse;


					//FTE Ward Senior Nurses in Wales
					$fte_ward_senior_nurses_in_wales = $calculatorData['CalculatorVariable']['senior_nurses_in_nhs_wales'];
					//FTE Ward Senior Doctor in Wales
					$fteSeniorDoctorsInWales = $calculatorData['CalculatorVariable']['senior_doctors_in_nhs_wales'];
					$snl = ($fte_ward_senior_nurses_in_wales*$totalPopulation)/$populationOfWalse;
					$sdl = ($fteSeniorDoctorsInWales*$totalPopulation)/$populationOfWalse;



					$fixed_input1 = "316881";
					$fixed_input2 = "0.7646";
					$populationOfWalse = "54000000";
					$jndsw = "3";
					$jndwy = "47";
					$wjd = "48609";
					$jndu = "50%";
					$jnts = "21";
					$jdts = "48";
					$jnch = "37";
					$jdch = "39.45";


					$pjnts = "50";
					$sndu = "50";
					$wsn = "14118";
					$snch = "54";
					$snts = "10.5";
					$pjdts = "50";
					$wsd = "54916";
					$sdsw = "5";
					$sdwy = "47";
					$sdch = "104.67";
					$sdts = "24";

					$nsy = ($jnl*$jndsw*$jndwy);

					///// Junior Nurses
					$ftejNusrse1 = $jnts*$nsy;
					$ftejNusrse2 = ($ftejNusrse1*$jndu)/100;
					$ftejNusrse = $ftejNusrse2/60;
					// echo "Time savings for Junior Nurses in a year in hours Recent-------------------------------------". ceil($ftejNusrse). "<br />";

					$fteJunior1 =  $ftejNusrse/$jndsw;
					$fteJunior2 =  $fteJunior1/$jndwy;
					$fteJunior =  $fteJunior2/10;
					// echo "FTE Junior Nurses Recent-------------------------------------". ceil($fteJunior). "<br />";

					////


					///// Junior Doctors
					$dsy = $jdl*$jndsw*$jndwy;
					$ftejDoc1 = ($jndu*$jdts)/100;
					$ftejDoc = ($ftejDoc1*$dsy)/60;
					// echo "Time savings for Junior Doc in a year in hours Recent-------------------------------------". ceil($ftejDoc). "<br />";

					$fteJuniorDoc1 =  $ftejDoc/$jndsw;
					$fteJuniorDoc2 =  $fteJuniorDoc1/$jndwy;
					$fteJuniorDoc =  $fteJuniorDoc2/10;
					// echo "FTE Junior Doc Recent-------------------------------------". ceil($fteJuniorDoc). "<br />";

					////



					///// Senior Nurses
					$snsy = $snl*$jndsw*$jndwy;
					$ftesNusrse1 = $snts*$snsy;
					$ftesNusrse2 = ($ftesNusrse1*$pjdts)/100;
					$ftesNusrse = $ftesNusrse2/60;
					// echo "Time savings for Senior Nurses in a year in hours Recent-------------------------------------". ceil($ftesNusrse). "<br />";

					$fteSenior1 =  $ftesNusrse/$jndsw;
					$fteSenior2 =  $fteSenior1/$jndwy;
					$fteSenior =  $fteSenior2/10;
					// echo "FTE Senior Nurses Recent-------------------------------------". ceil($fteSenior). "<br />";

					////


					///// Senior Doc
					$sdsy = $sdl*$sdsw*$sdwy;
					$ftesDoc1 = ($sdts*$sndu)/100;
					$ftesDoc2 = $ftesDoc1*$sdsy;
					$ftesDoc = $ftesDoc2/60;
					// echo "Time savings for Senior Nurses in a year in hours Recent-------------------------------------". ceil($ftesDoc). "<br />";

					$fteSeniorDoc1 =  $ftesDoc/$sdwy;
					$fteSeniorDoc2 =  $fteSeniorDoc1/$sdsw;
					$fteSeniorDoc =  $fteSeniorDoc2/10;
					// echo "FTE Senior Nurses Recent-------------------------------------". ceil($fteSeniorDoc). "<br />";

					////

					$responseData['FTE Junior Nurses'] = $fteJunior;
					$responseData['FTE Senior Nurses'] = $fteSenior;

					$responseData['FTE Junior Doc'] = $fteJuniorDoc;
					$responseData['FTE Senior Doc'] = $fteSeniorDoc;

					// $totalNurses = $responseData['fte_ward_junior_nurses_in_locality']+$responseData['fte_ward_senior_nurses_in_locality'];

					// $totalDoctors = $responseData['fte_ward_junior_doctor_in_locality']+$responseData['fte_ward_senior_doctor_in_locality'];

					$responseData['nurse_sum'] =  $fteJunior + $fteSenior;
					$responseData['nurse_sum'] =  ceil($responseData['nurse_sum']);
					$responseData['doctor_sum'] = $fteJuniorDoc + $fteSeniorDoc;
					$responseData['doctor_sum'] = ceil($responseData['doctor_sum']);
					$responseData['success'] = "success";

				}catch(Exception $e){
					$responseData['error'] = $e->getMessage();
				}
			}else{
				$responseData['error'] = "Something wrong! Try Againd.";
			}
		}else{
			$responseData['error'] = "Something wrong! Try Againd.";
		}
		echo json_encode($responseData);
		die;
	}

	/*
	-------------------------------------------------------------------------------------------------------
	On: 14-09-2019
	I/P: JSON
	O/P: JSOn
	Desc: In App Alert autid trails
	-------------------------------------------------------------------------------------------------------
	*/

	public function inAppAlert()
	{
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() && $this->validateAccessKey() ){
					try{
						$deviceData = array(
											"user_id"=> $dataInput['user_id'],
											"institute_id"=>$dataInput['institute_id'],
											"in_app_alert"=>$dataInput['in_app_alert'],
											"override_silent_mode"=>$dataInput['override_silent_mode'],
											"device_info"=> json_encode($dataInput['device_info'])
											);
						$addData = $this->InAppAlertTransaction->save($deviceData );
						$responseData = array('method_name'=> 'inAppAlert', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
					}catch(Exception $e){
						$responseData = array('method_name'=> 'inAppAlert', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "message"=> $e->getMessage());
					}
				}else{
			         $responseData = array('method_name'=> 'inAppAlert','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		        }
			}else{
			$responseData = array('method_name'=> 'inAppAlert','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
    	exit;
	}
}
