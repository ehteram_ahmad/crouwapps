<?php
/*
-----------------------------------------------------------------------------------------------
	Desc: Controller used to do MB Analytics.
-----------------------------------------------------------------------------------------------
*/

App::uses('AppController', 'Controller');

class Mbapiv2analyticwebservicesController extends AppController {
	public $uses = array('Mbapiv2.User', 'Mbapiv2.MbChatAnalytic', 'Mbapiv2.PatientHcpAssociation', 'Mbapiv2.ChatMessageAnalytic',
		'Mbapiv2.InstituteName','Mbapiv2.NewTableTest','Mbapiv2.UserDutyLog');
	
	/*
	--------------------------------------------------------------------
	On: 25-07-2016
	I/P: JSON
	O/P: JSON
	Desc: Captures chat button pressed by user for analytic purpose.
	--------------------------------------------------------------------
	*/
	public function addMbAnalytic(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if( $this->validateToken() && $this->validateAccessKey() ){
				if( $this->User->findById( $dataInput['user_id'] ) ){
					//** Add Chat analytics  of any user
					try{
						$analyticData = array(
										"user_id"=> $dataInput['user_id'],
										"device_type"=> $dataInput['device_type'],
										"content_type"=> $dataInput['content_type'],
									);
						$addAnalytics = $this->MbChatAnalytic->save($analyticData);
						if($addAnalytics){
							$responseData = array('method_name'=> 'addMbAnalytic','status'=>'1','response_code'=>'200', 'message'=> ERROR_200);
						}else{
							$responseData = array('method_name'=> 'addMbAnalytic','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
						}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'addMbAnalytic','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
			    		$responseData = array('method_name'=> 'addMbAnalytic','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			}else{
				$responseData = array('method_name'=> 'addMbAnalytic', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addMbAnalytic', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	--------------------------------------------------------------------
	On: 03-05-2017
	I/P: JSON
	O/P: JSON
	Desc: Capture chat messages.
	--------------------------------------------------------------------
	*/
	public function chatMessageAnalytic(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->findById( $dataInput['user_id'] ) ){
					//** Add Chat analytics  of any user
					try{
						$analyticData = array(
										"user_id"=> $dataInput['user_id'],
										//"device_type"=> $dataInput['device_type'],
										//"content_type"=> $dataInput['content_type'],
										"dialog_id"=> $dataInput['dialog_id'],
									);
						$addAnalytics = $this->MbChatAnalytic->save($analyticData);
						if($addAnalytics){
							$responseData = array('method_name'=> 'chatMessageAnalytic','status'=>'1','response_code'=>'200', 'message'=> ERROR_200);
						}else{
							$responseData = array('method_name'=> 'chatMessageAnalytic','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
						}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'chatMessageAnalytic','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
			    		$responseData = array('method_name'=> 'chatMessageAnalytic','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			}else{
				$responseData = array('method_name'=> 'chatMessageAnalytic', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'chatMessageAnalytic', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	--------------------------------------------------------------------
	On: 04-05-2017
	I/P: JSON
	O/P: JSON
	Desc: Use to add Patient Hcp Association
	--------------------------------------------------------------------
	*/

	public function patientHcpAssociationAnalytics(){
			$this->autoRender = false;
			$responseData = array();
			if($this->request->is('post')) {
				$dataInput = $this->request->input ( 'json_decode', true) ;
				// $userStatusDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['hcp_patient_associations']['user_id'])));
				// if($userStatusDetails['User']['status']==1 && $userStatusDetails['User']['approved']==1)
				// {
					if( $this->validateToken() && $this->validateAccessKey() ){
						//if( $this->User->findById( $dataInput['user_id'] ) ) {
							//** Add patient hcp analytics  of any user
							try{
								$applicationType = $this->getAppType();
								$device_type = $this->getDeviceType();
								foreach($dataInput['hcp_patient_associations'] as $hcpPatientAssociations){
									$userId = $hcpPatientAssociations['user_id'];
									$participant_ids = implode(",", $hcpPatientAssociations['participant_ids']);
									$dialogueId = $hcpPatientAssociations['dialogue_id'];
									$dialogueType = $hcpPatientAssociations['dialogue_type'];
									$action = $hcpPatientAssociations['action'];
									$timeStamp = $hcpPatientAssociations['timestamp'];
									$dialogueName = $hcpPatientAssociations['dialogue_name'];
									
									$hcpAssociationData = array(
													"user_id"=> $userId,
													"participant_ids"=> $participant_ids,
													"dialogue_id"=> $dialogueId,
													"dialogue_type"=> $dialogueType,
													"action"=> $action,
													"application_type"=> $applicationType,
													"device_type"=> $device_type,
													"timestamp"=> $timeStamp,
													"dialogue_name"=> $dialogueName,
												);
									$addhcpAssociation = $this->PatientHcpAssociation->saveAll($hcpAssociationData);
								}
								if($addhcpAssociation){
									$responseData = array('method_name'=> 'patientHcpAssociationAnalytics','status'=>'1','response_code'=>'200', 'message'=> ERROR_200);
								}else{
									$responseData = array('method_name'=> 'patientHcpAssociationAnalytics','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
								}
							}catch( Exception $e ){
								$responseData = array('method_name'=> 'patientHcpAssociationAnalytics','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
							}
						// }else{
					 //    		$responseData = array('method_name'=> 'patientHcpAssociationAnalytics','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
					 //    	}
					}else{
						$responseData = array('method_name'=> 'patientHcpAssociationAnalytics', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
					}
				// }
				// else
				// {
				// 	$statusResponse = $this->getUserStatus($dataInput['hcp_patient_associations']['user_id'],$dataInput['hcp_patient_associations']['user_id']);
				// 	$responseData = array('method_name'=> 'patientHcpAssociationAnalytics','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
				// }
			}else{
				$responseData = array('method_name'=> 'patientHcpAssociationAnalytics', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
			}
			echo json_encode($responseData);
			exit;
		}

		/*
		--------------------------------------------------------------------
		On: 05-05-2017
		I/P: JSON
		O/P: JSON
		Desc: Use to get Chat Data
		--------------------------------------------------------------------
		*/

		public function chatAnalytics(){
			$this->autoRender = false;
			$responseData = array();
			if($this->request->is('post')) {
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck()){
					//if( $this->User->findById( $dataInput['user_id'] ) ) {
						//** Add patient hcp analytics  of any user
						try{

							foreach($dataInput['message'] as $chatData){
								$userId = $chatData['user_id'];
								$dialogueId = $chatData['dialogue_id'];
								$messageType = $chatData['message_type'];
								$timeStamp = $chatData['timestamp'];
								
								$chatMessageData = array(
												"user_id"=> $userId,
												"dialogue_id"=> $dialogueId,
												"message_type"=> $messageType,
												"timestamp"=> $timeStamp,
											);
								$addChatMessageData = $this->ChatMessageAnalytic->saveAll($chatMessageData);
							}
							if($addChatMessageData){
								$responseData = array('method_name'=> 'chatAnalytics','status'=>'1','response_code'=>'200', 'message'=> ERROR_200);
							}else{
								$responseData = array('method_name'=> 'chatAnalytics','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
							}
						}catch( Exception $e ){
							$responseData = array('method_name'=> 'chatAnalytics','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
						}
					// }else{
				 //    		$responseData = array('method_name'=> 'chatAnalytics','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
				 //    	}
				}else{
					$responseData = array('method_name'=> 'chatAnalytics', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
				}
			}else{
				$responseData = array('method_name'=> 'chatAnalytics', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
			}
			$encryptedData = $this->Common->encryptData(json_encode($responseData));
			echo json_encode(array("values"=> $encryptedData));
			exit;
		}

}
