<?php


App::uses('AppController', 'Controller');

class Mbapiv2appdetailsController extends AppController {
	public $uses = array('MbAppVersion', 'Mbapiv2.AppDownloadLog', "Mbapiv2.ApiRequestResponseTrack");
	
	/*
	-------------------------------------
	On: 
	I/P:
	O/P:
	Desc: fetches app version details for Android, ios
	-------------------------------------
	*/
	public function appVersionDetails(){


		
		$this->autoRender = false;
		$responseData = array();
		$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			//if( $this->tokenValidate() ){
				//** Fetch app version details
			   //echo '<pre>';print_r($dataInput);exit();
				$forceUpdate = 0;
				$conditionsForUser = array("version"=> $dataInput["version"],"device_type"=> $dataInput["device_type"], "status"=> 1);
				//$appVersionDetails = $this->MbAppVersion->find("first", array("conditions"=> $conditions));
				$userCurrentAppVersionDetails = $this->MbAppVersion->find("first", array("conditions"=> $conditionsForUser));
				
				$startId = $userCurrentAppVersionDetails['MbAppVersion']['id'];
				$conditionsForUpdate = array("device_type"=> $dataInput["device_type"], "status"=> 1);
				$latestUpdate = $this->MbAppVersion->find("first",array("conditions"=>$conditionsForUpdate,"order"=>array("created DESC") ) );
				$endId = $latestUpdate['MbAppVersion']['id'];
				if($startId == $endId){
					$versionDetails = array("version"=> $userCurrentAppVersionDetails['MbAppVersion']['version'], "force_update"=> $userCurrentAppVersionDetails['MbAppVersion']['force_update'], "update_frequency"=> $userCurrentAppVersionDetails['MbAppVersion']['update_frequency'] );
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
				
				}else{
					$returnVal = 0;
					for($i=$startId+1;$i<=$endId;$i++){
					$conditionsForUpdate = array("id"=>$i,"device_type"=> $dataInput["device_type"],"force_update"=>1, "status"=> 1);
					$checkForForceUpdate = $this->MbAppVersion->find('all',array('conditions'=>$conditionsForUpdate));
					if(!empty($checkForForceUpdate)){
						$returnVal = $returnVal + 1;
						
					}

				   }
				   if($returnVal > 0){
				   	$versionDetails = array("version"=> $latestUpdate['MbAppVersion']['version'], "force_update"=> 1, "update_frequency"=> $latestUpdate['MbAppVersion']['update_frequency'] );
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
				
				   }else{
				   	$versionDetails = array("version"=> $latestUpdate['MbAppVersion']['version'], "force_update"=> 0, "update_frequency"=> $latestUpdate['MbAppVersion']['update_frequency'] );
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
				  
				   }
				}// End if else


				
				/*
				if($appVersionDetails['MbAppVersion']['force_update']){
					$forceUpdate = 1;
				}
				
				if(!empty($appVersionDetails)){
					$versionDetails = array("version"=> $appVersionDetails['MbAppVersion']['version'], "force_update"=> $forceUpdate, "update_frequency"=> $appVersionDetails['MbAppVersion']['update_frequency'] );
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
				}else{
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
				*/
			
		}else{
			$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		//** Track API Request, Response[START]
		$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$headerVals = $this->getHeaderValues();
		$trackData = array(
							"user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> ($endTime - $startTime),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		//** Track API Request, Response[END]
		exit;
		
	}

	/*
	-------------------------------------
	On: 19-10-2016
	I/P: JSON DATA
	O/P: JSON DATA as response
	Desc: Inserts App install by user with version
	-------------------------------------
	*/
	public function appInstallLogAdd(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if( $this->validateToken() ){
				//** Add App install log

				$logData = array(
						"user_id"=> $dataInput['user_id'],
						"version"=> $dataInput['version'],
						"device_id"=> $dataInput['device_id'],
						"device_type"=> $dataInput['device_type'],
						"application_type"=> "MB"
					);
				try{
					$addLogData = $this->AppDownloadLog->save($logData);
					$responseData = array('method_name'=> 'appInstallLogAdd', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
				}catch(Exception $e){
					$responseData = array('method_name'=> 'appInstallLogAdd', 'status'=>"0", 'response_code'=> '615', 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
				}
			}else{
				$responseData = array('method_name'=> 'appInstallLogAdd', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'appInstallLogAdd', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;	
	}
}
