<?php

App::uses('AppController', 'Controller');

class Mbapiv3sessionexpiryController extends AppController {
    public $uses = array();
    public $components = array('Cache');

    public function index() {
        $header = getallheaders();
        $token = null;

        if (array_key_exists('token', $header)) {
            $token = $header['token'];
        } else if (array_key_exists('Token', $header)) {
            $token = $header['Token'];
        }

        if (empty($token)) {
            http_response_code(401);
            exit(null);
        }

        $hasSession = false;

        try {
            $hasSession = $this->Cache->hasSession($token);
        } catch (Exception $e) {
            http_response_code(500);
            exit("Error retrieving session from Redis:\n\n" . $e);
        }

        if (isset($token) && $hasSession) {
            http_response_code(202);
            exit(null);
        }

        http_response_code(410);
        exit(null);
    }
}
