<?php

/*
 * Email Component
 * 
 * Contains all related function for email sending
 *
 */

App::uses('CakeEmail', 'Network/Email');

class MbemailComponent extends Component{ 
	private $mandrillKey = 'QcLlXKhzVK6oIr0o4U5gkw';
	/*
	On: 17-05-2016
	I/P: $params = array()
	O/P: 
	Desc: Sending email to user with template.
	*/	
	public function verifyEmail( $params = array()){ 
		// if( !empty($params) ){
		// 	$Email = new CakeEmail();
			//$Email->config('medicbleepsmtp');
			// $Email->config('mailchimp');
			// $params['subject'] = 'Please verify your email';
			// $Email->template('medicbleep_verify_email', '');
			// $Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			// $replacearray = array();
			// $replacearray['NAME'] = $params['name'];
			// $replacearray['ACTIVATION_KEY'] = $params['activationLink'];
			//$replacearray['BASEURL'] = BASE_URL;
			/*===========End values to be replaced in the template==========*/
		//   	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
		// 	$Email->viewVars(array('msg' => $message));
		// 	//$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
		// 	$Email->from(array("medicbleep@theoncallroom.com"=> "Medic Bleep"));
		// 	$Email->to( $params['toMail'] );
		// 	$Email->subject( $params['subject'] );

		// 	try{
		// 		if($Email->send()){
		// 			$msg = 'mail sent';
		// 		}else{
		// 			$msg = 'mail not sent';
		// 		}
		// 	}catch( Exception $e ){
		// 		$msg = $e->getMessage();
		// 	}
		// 	return $msg;
		// }


		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		//$template = "VERIFY_LINK";
		$template = "MB_your_email_address";
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'Please verify your email'
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
			array(
				'name' => 'NAME',
				'content' => $params['name']
			),
			array(
				'name' => 'VERIFY_LINK',
				'content' => $params['activationLink']
			),
			)
		);	
		$message = array(
		'html' => 'Please verify your email',
		'text' => 'Please verify your email',
		'subject' => 'Please verify your email',
		//'from_email' => 'medicbleep@theoncallroom.com',
		'from_email' => 'support@medicbleep.com',
		'from_name' => 'Medic Bleep',
		'to' => $to,
		"merge_vars" => $var
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	}

	/*
	--------------------------------------------------------------------------------
	On: 17-05-2016
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to user with template for forgot password.
	--------------------------------------------------------------------------------
	*/	
	public function forgotPassword( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('medicbleepsmtp');
			$Email->config('mailchimp');
			$params['subject'] = 'Your new password';
			$Email->template('medicbleep_forgot_password', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['PASS'] = $params['password'];
			//$replacearray['SITE_NAME'] = 'OCR Team';
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array("medicbleep@theoncallroom.com"=> "Medic Bleep"));
			$Email->from(array("support@medicbleep.com"=> "Medic Bleep"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				return $msg = $e->getMessage();
			}
		}
	}

	/*
	On: 17-05-2016
	I/P: $params = array()
	O/P: 
	Desc: Sending signup welcome email to user with template.
	*/	
	public function signupWelcomeEmail( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('medicbleepsmtp');
			$Email->config('mailchimp');
			$params['subject'] = 'Welcome';
			$Email->template('medicbleep_signup_welcome', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['REGARDS'] = $params['regards'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array("support@medicbleep.com"=> "Medic Bleep"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Welcome mail sent';
				}else{
					$msg = 'Welcome mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	--------------------------------------------------------------------
	On: 17-05-2016
	I/P:
	O/P:
	Desc: Replace variable and put values from email template.
	--------------------------------------------------------------------
	*/
	function strreplacetextreplace(&$t, $d) {
		preg_match_all ( '/{\%(\w*)\%\}/' , $t , $matches );
		foreach($matches[1] as $m){
						$pattern = "/{\%".$m."\%\}/";
						$t = preg_replace( $pattern, $d[$m], $t);
		}
		return $t;
    }

	/*
	------------------------------------------------------------------------
	On: 17-05-2016
	I/P: 
	O/P: 
	Desc: Sending mail user when admin approves the user.
	------------------------------------------------------------------------
	*/	
	public function userApproveByAdmin( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('medicbleepsmtp');
			$Email->config('mailchimp');
			$params['subject'] = 'Welcome to MedicBleep';
			$Email->template('medicbleep_user_approve_by_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			//$replacearray['SITE_NAME'] = "OCR Team";
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array("support@medicbleep.com"=> "Medic Bleep"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Admin Approve User Mail sent';
				}else{
					$msg = 'Admin Approve User Mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	-----------------------------------------------------------------------------
	On: 17-05-2016
	I/P: 
	O/P: 
	Desc: Sending promt mail to admin when user clicks on verify link.
	-----------------------------------------------------------------------------
	*/
	public function userActivationPromtToAdmin($params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('medicbleepsmtp');
			$Email->config('mailchimp');
			$params['subject'] = 'New User Activated Account';
			$Email->template('user_activation_promt_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name']; 
			$replacearray['EMAIL'] = $params['email']; 
			$replacearray['COUNTRY'] = $params['country']; 
			$replacearray['SITE_URL'] = $params['siteUrl']; 
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array("support@medicbleep.com"=> "Medic Bleep")); 
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'New User Active Promt to Admin Sent';
				}else{
					$msg = 'New User Active Promt to Admin Not Sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg; 
		}
	}

	/*
	-----------------------------------------------------------------------------
	On: 20-12-2016
	I/P: 
	O/P: 
	Desc: Sending Mail to users who didn't completed signup
	-----------------------------------------------------------------------------
	*/
	public function sendMailPartialSignupUser($params){
		// if( !empty($params) ){
		// 	$Email = new CakeEmail();
			//$Email->config('medicbleepsmtp');
			// $Email->config('mailchimp');
			// $params['subject'] = 'We got your back!';
			// $Email->template('medicbleep_complete_partial_signup', '');
			// $Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			// $replacearray = array();
			// $replacearray['PARTIALSIGNUPURL'] = $params['partialSignupUrl'];
			/*===========End values to be replaced in the template==========*/
		 //  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			// $Email->viewVars(array('msg' => $message));
			// $Email->from(array("medicbleep@theoncallroom.com"=> "Medic Bleep")); 
			// $Email->to( $params['toMail'] );
			// $Email->subject( $params['subject'] );

		// 	try{
		// 		if($Email->send()){
		// 			$msg = 'Medicbleep Complete Partial Signup Mail Sent';
		// 		}else{
		// 			$msg = 'Medicbleep Complete Partial Signup Mail Not Sent';
		// 		}
		// 	}catch( Exception $e ){
		// 		$msg = $e->getMessage();
		// 	}
		// 	return $msg; 
		// }
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
			$mandrill = new Mandrill($this->mandrillKey);
			//$template = "Partial SignUp";
			$template = "MB_Partial_SignUp";
			//$template_name = $template;
			$template_content = array(
			array(
			'name' => $template,
			'content' => "You're so close! Complete Medic Bleep registration."
			)
			);

			$to[]=array('email'=> $params['toMail'],
			'name' => $params['name'],
			'type' => 'to');


			$var[]=array(
				'rcpt' => $params['toMail'],
				'vars' => array(
				array(
					'name' => 'PARTIALSIGNUPURL',
					'content' => $params['partialSignupUrl']
				),
				)
			);	
			$message = array(
			'html' => "You're so close! Complete Medic Bleep registration.",
			'text' => "You're so close! Complete Medic Bleep registration.",
			'subject' => "You're so close! Complete Medic Bleep registration.",
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
			"subaccount" => MANDRILL_SUBACCOUNT,
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = date('d-m-Y');
			$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
		 	return json_encode($result); 
	}

	/*
	-----------------------------------------------------------------------------
	On: 20-12-2016
	I/P: 
	O/P: 
	Desc: Sending Mail to first time logged in user
	-----------------------------------------------------------------------------
	*/
	public function sendMailToFirstTimeLoggIn($params){
		// if( !empty($params) ){
		// 	$Email = new CakeEmail();
			//$Email->config('medicbleepsmtp');
			// $Email->config('mailchimp');
			// $params['subject'] = 'What if your Friend lose their Medical Licence tomorrow?';
			// $Email->template('medicbleep_first_time_login', '');
			// $Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			// $replacearray = array();
			// $replacearray['COLLEAGUEURL'] = $params['colleagueUrl']; 
			// if(!empty($params['iosHitUrl'])){
			// 	$replacearray['IOSHITURL'] = $params['iosHitUrl'];
			// }else{
			// 	$replacearray['IOSHITURL'] = "";
			// }
			/*===========End values to be replaced in the template==========*/
		 //  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			// $Email->viewVars(array('msg' => $message));
			// $Email->from(array("medicbleep@theoncallroom.com"=> "Medic Bleep")); 
			// $Email->to( $params['toMail'] );
			// $Email->subject( $params['subject'] );

		// 	try{
		// 		if($Email->send()){
		// 			$msg = 'Medicbleep First Time Login Mail Sent';
		// 		}else{
		// 			$msg = 'Medicbleep First Time Login Mail Not Sent';
		// 		}
		// 	}catch( Exception $e ){
		// 		$msg = $e->getMessage();
		// 	}
		// 	return $msg; 
		// }

			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		$template = "Invite medicbleep  after first login";
		//$template_name = $template;
		if(!empty($params['iosHitUrl'])){
			$replacearray['IOSHITURL'] = $params['iosHitUrl'];
		}else{
			$replacearray['IOSHITURL'] = "";
		}
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'What if your Friend lose their Medical Licence tomorrow?'
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
			array(
				'name' => 'COLLEAGUEURL',
				'content' => $params['colleagueUrl']
			),
			array(
				'name' => 'IOSHITURL',
				'content' => $params['iosHitUrl']
			)
			)
		);	
		$message = array(
		'html' => 'What if your Friend lose their Medical Licence tomorrow?',
		'text' => 'What if your Friend lose their Medical Licence tomorrow?',
		'subject' => 'What if your Friend lose their Medical Licence tomorrow',
		'from_email' => 'support@medicbleep.com',
		'from_name' => 'Medic Bleep',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	}

	/*
	-----------------------------------------------------------------------------
	On: 20-02-2017
	I/P: 
	O/P: 
	Desc: Sending Mail to user within 24 hour who didn't verified mail(After Registration)
	-----------------------------------------------------------------------------
	*/
	public function sendMailToEmailNotVerifiedWithin24Hour($params){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$params['subject'] = 'Your email verification is still pending.';
			$Email->template('medicbleep_mailnotverifiedwithin24hour', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			/*$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['ACTIVATION_KEY'] = $params['activationLink'];*/ 
			/*===========End values to be replaced in the template==========*/
		  	//$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('NAME' => $params['name'], 'ACTIVATION_KEY'=> $params['activationLink']));
			$Email->from(array("support@medicbleep.com"=> "Medic Bleep")); 
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Medicbleep Not Verified Mail Within 24 Hour Sent';
				}else{
					$msg = 'Medicbleep Not Verified Mail Within 24 Hour No tSent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg; 
		}
	}


	/*
	-----------------------------------------------------------------------------------------
	On: 29-03-2017
	I/P: 
	O/P: 
	Desc: Sending Mail to user after reset password
	-----------------------------------------------------------------------------------------
	*/
	public function resetPassword($params = array()){
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		//$template = "Password_Change_Confirmation_Email_Template_MB";
		$template = "MB_Password_Change_Confirmation";
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'Password Change Confirmation'
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
			array(
				'name' => 'NAME',
				'content' => $params['name']
			),
			)
		);	
		$message = array(
		'html' => 'Password Change Confirmation',
		'text' => 'Password Change Confirmation',
		'subject' => 'Password Change Confirmation',
		'from_email' => 'support@medicbleep.com',
		'from_name' => 'Medic Bleep Team',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	 }  

	 /*
	-----------------------------------------------------------------------------------------
	On: 30-03-2017
	I/P: 
	O/P: 
	Desc: Sending Mail to user after reset password
	-----------------------------------------------------------------------------------------
	*/
	 public function forgotPasswordMb($params=array()){
	 	App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		//$template = "Forgotten_Password_Email_Template_MB";
		$template = "MB_Forgot_Password";
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'Forgot Password'
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
			array(
				'name' => 'NAME',
				'content' => $params['name']
			),
			array(
				'name' => 'FORGOTTEN_PASSWORD_LINK',
				'content' => $params['forgotPasswordLink']
			),
			)
		);	
		$message = array(
		'html' => 'Forgot Password',
		'text' => 'Forgot Password',
		'subject' => 'Forgot Password',
		'from_email' => 'support@medicbleep.com',
		'from_name' => 'Medic Bleep Team',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	 }

	/*
	------------------------------------------------------------------------------------------
	On: 17-04-2017
	I/P: 
	O/P: 
	Desc: Sending Chat History to mail
	------------------------------------------------------------------------------------------
	*/
	public function sendChatHistory($params=array()){
	 	App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		//$template = "mb_chat_history";
		$template = "MB_Exported_PDF";
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'Chat History'
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
			array(
				'name' => 'NAME',
				'content' => $params['name']
			),
			array(
				'name' => 'CONVERSION_NAME',
				'content' => $params['conversationName']
			),
			array(
				'name' => 'FROM_DATE',
				'content' => $params['fromDate']
			),
			array(
				'name' => 'TO_DATE',
				'content' => $params['toDate']
			),
			)
		);	
		$attachments[]=
                array(
                	"type" => "application/pdf",
                	"name"=> $params['chatHistoryFileName'],
                	"content"=> $params['chatHistoryFilePath']
                
            );
            
    
		$message = array(
		'html' => 'Chat History',
		'text' => 'Chat History',
		'subject' => $params['emailSubject'],
		'from_email' => 'support@medicbleep.com',//'medicbleep@theoncallroom.com',
		'from_name' => 'Medic Bleep',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		"attachments"=> $attachments
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	 }

	 /*
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	 */
	public function sendMailToSalesFreeEnterpriseUser($params = array()){
		$params['toMail'] = 'sales@medicbleep.com';
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		$template = "mb_freeTrialmail_to_salesPerson";
		//$template_name = $template;
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'Free trial user information mail to sales'
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
				array(
					'name' => 'NAME',
					'content' => $params['name']
				),
				array(
				'name' => 'PROFESSION',
				'content' => $params['profession']
				),
				array(
				'name' => 'INSTITUTION_NAME',
				'content' => $params['institution_name']
				),
				array(
				'name' => 'USER_EMAIL',
				'content' => $params['user_email']
				),
				array(
				'name' => 'USER_PHONE_NUMBER',
				'content' => $params['user_phone_number']
				),
				array(
				'name' => 'REGISTRATION_DATE',
				'content' => $params['registration_date']
				),
			)
		);	        
    
		$message = array(
		'html' => 'Free Trial Member Joined',
		'text' => 'Free Trial Member Joined',
		'subject' => "Free Trial Member Joined",
		'from_email' => 'support@medicbleep.com',//'medicbleep@theoncallroom.com',
		'from_name' => 'Medic Bleep',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	}

	/*
	------------------------------------------------------------------------------------------
	On: 11-08-2017
	I/P: 
	O/P: 
	Desc: Sending Onboarding emails
	------------------------------------------------------------------------------------------
	*/
	public function onboardingEmail($params = array()){
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		$template = $params['templateName'];
		
		$template_content = array(
		array(
		'name' => $template,
		'content' => $params['subject']
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
				array(
					'name' => 'FNAME',
					'content' => $params['name']
				),
			)
		);	        
    
		$message = array(
		//'html' => 'Test Mail',
		//'text' => 'Test Mail',
		'subject' => $params['subject'],
		'from_email' => 'support@medicbleep.com',
		'from_name' => 'Medic Bleep',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => 'MB_MARKETING',
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = $params['sendAt'];
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	}

	/*
	------------------------------------------------------------------------------------------
	On: 14-09-2017
	I/P: 
	O/P: 
	Desc: Sending Free subscription reminder mail to the user
	------------------------------------------------------------------------------------------
	*/

	public function sendFreeSubscriptionMail( $subscriptionPeriod, $userEmail, $userName){ 

		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		//$template = "VERIFY_LINK";
		if($subscriptionPeriod == 0)
		{
			$template = "MB_Subscription_Expired";
		}
		else
		{
			$template = "MB_Subscription_Notification";
		}
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'Free Trial Subscription Mail'
		)
		);

		$to[]=array('email'=> $userEmail,
		'name' => $userEmail,
		'type' => 'to');


		$var[]=array(
			'rcpt' => $userEmail,
			'vars' => array(
			array(
				'name' => 'NAME',
				'content' => $userName
			),
			array(
				'name' => 'COUNTER_DAYS',
				'content' => $subscriptionPeriod
			),
			)
		);	
		$message = array(
		'html' => 'Free Trial Subscription Mail',
		'text' => 'Free Trial Subscription Mail',
		'subject' => 'Free Trial Subscription Mail',
		//'from_email' => 'medicbleep@theoncallroom.com',
		'from_email' => 'support@medicbleep.com',
		'from_name' => 'Medic Bleep',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	}

	// public function sendFeedBackEmail( $params = array()){
	// 	$params['toMail'] = 'support@medicbleep.zendesk.com';
	// 	App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
	// 	$mandrill = new Mandrill($this->mandrillKey);
	// 	//$template = "VERIFY_LINK";
	// 	$template = "MB_your_email_address";
	// 	$template_content = array(
	// 	array(
	// 	'name' => $template,
	// 	'content' => 'Please verify your email'
	// 	)
	// 	);

	// 	$to[]=array('email'=> $params['toMail'],
	// 	'name' => $params['name'],
	// 	'type' => 'to');


	// 	$var[]=array(
	// 		'rcpt' => $params['toMail'],
	// 		'vars' => array(
	// 		array(
	// 			'name' => 'NAME',
	// 			'content' => $params['name']
	// 		),
	// 		)
	// 	);	
	// 	$message = array(
	// 	'html' => 'Please verify your email',
	// 	'text' => 'Please verify your email',
	// 	'subject' => 'Please verify your email',
	// 	//'from_email' => 'medicbleep@theoncallroom.com',
	// 	'from_email' => $params['fromMail'],
	// 	'from_name' => $params['name'],
	// 	'to' => $to,
	// 	"merge_vars" => $var,
	// 	"subaccount" => MANDRILL_SUBACCOUNT,
	// 	);
	// 	$async = false;
	// 	$ip_pool = 'Main Pool';
	// 	$send_at = date('d-m-Y');
	// 	$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	//  	return json_encode($result);
	// }

	/*
	------------------------------------------------------------------------------------------
	On: 18-01-2019
	I/P: 
	O/P: 
	Desc: Send an email to zenddesk support level one if user choose supprt from app.
	------------------------------------------------------------------------------------------
	*/

	public function sendFeedBackEmail($params=array()){
	 	App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		$template = "MB_feedback";
		$nameTxt = 'Role Removed';
		$params['fromMail'] = $params['fromMail'];
		$params['feedback'] = $params['feedback']."\n\nFROM \n ".$params['name']."\n".$params['fromMail'];
		$template_content = array(
			array(
				'name' => $template,
				'content' => $params['feedback']
			)
		);
		// $params['toMail'] = "ehteram@mediccreations.com";
		$params['toMail'] = "support@medicbleep.zendesk.com";
		$to[]=array('email'=> $params['toMail'],
			'name' => $params['f_name'],
			'type' => 'to'
		);

		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
				array(
					'name' => 'FEEDBACK',
					'content' => $params['feedback']
				),
				array(
					'name' => 'NAME',
					'content' => $params['name']
				),
				array(
					'name' => 'EMAIL',
					'content' => $params['toMail']
				),
			)
		);


		$message = array(
			'html' => $params['feedback'],
			'text' => $params['feedback'],
			'subject' => $params['subject'],
			'bcc_address' => 'anil@mediccreations.com',
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
		$msg = $result;
		return $msg;
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 26-02-2019
	I/P: 
	O/P: 
	Desc: Sending Mail to admin after contact us form submition
	-----------------------------------------------------------------------------------------
	*/
	public function contactUs($params = array()){
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		$template = "Mb_contactus";
		$template_content = array(
			array(
			'name' => $template,
			'content' => 'Contact Us'
			)
		);

		$to[]= array(
			'email'=> $params['to_mail'],
			'name' => $params['user_name'],
			'type' => 'to'
		);


		$var[]=array(
			'rcpt' => $params['to_mail'],
			'vars' => array(
				array(
					'name' => 'SUBJECT',
					'content' => $params['mailSubject']
				),
				array(
					'name' => 'NAME',
					'content' => $params['user_name']
				),
				array(
					'name' => 'EMAIL',
					'content' => $params['user_email']
				),
				array(
					'name' => 'MESSAGE',
					'content' => $params['user_message']
				),
			)
		);	
		$message = array(
		'html' => 'Contact Us',
		'text' => 'Contact Us',
		'subject' => 'Contact Us',
		'bcc_address' => 'anil@mediccreations.com',
		'from_email' => $params['fromMail'],
		'from_name' => $params['fromName'],
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	 } 

	/*
	On: 03-08-2015
	I/P: $params = array()
	O/P: 
	Desc: Sending email to user with template.
	*/	
	public function verifyEmailTest( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			$params['toMail'] = "support@medicbleep.zendesk.com";
			// $params['toMail'] = "ehteram@mediccreations.com";
			$params['fromEmail'] = $params['from'];
			// $params['fromEmail'] = "ehteram333@gmail.com";
			//$Email->config('smtp');
			$Email->config('smtp_custom');
			$params['subject'] = '';
			$Email->template('zendesk_email', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			//$replacearray['SITE_NAME'] = 'The On Call Room Team!';
			/*===========End values to be replaced in the template==========*/
		  	$message =  $params['user_subject']." : ".$params['feedback']." <br />"." From "."<br />".$params['name']."<br />".$params['fromEmail'];
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array($params['fromEmail'] => $params['name']));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'mail sent';
				}else{
					$msg = 'mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}
}
?>
