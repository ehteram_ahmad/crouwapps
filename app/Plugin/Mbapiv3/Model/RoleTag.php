<?php


App::uses('Model', 'Model');

/**
 * Represents model role_tags
 */
class RoleTag extends Model {

/*
-----------------------------------------------------
On: 26-10-2017
I/P:
O/P:
Desc:
-----------------------------------------------------
*/
public function roleTagVals($params=array()){
	$roleTags = array();
	if(!empty($params)){
		$roleTags = $this->query("SELECT `key`, GROUP_CONCAT( `value` ) AS `value` FROM `role_tags` AS `RoleTag`   WHERE FIND_IN_SET(`key`,"."'".$params['roleTags']."'".") AND country_id = ".$params['countryId']."  GROUP BY `key`");
	}
	return $roleTags;
}

/*
-----------------------------------------------------
On: 31-10-2017
I/P:
O/P:
Desc:
-----------------------------------------------------
*/
public function userRoleTags($params=array()){
	$roleTag = array();
	if(!empty($params)){
		$userId = $params['user_id'];
		$quer = "SELECT rt.id,urt.id,rt.`key`,rt.value FROM `role_tags` rt LEFT JOIN user_role_tags urt ON rt.id=urt.role_tag_id WHERE urt.user_id =$userId AND rt.key='".$params['key']."'";
		$roleTag = $this->query($quer);
	}
	return $roleTag;
}

/*
-----------------------------------------------------
On: 31-10-2017
I/P:
O/P:
Desc:
-----------------------------------------------------
*/
public function roleTagValsLite($params=array()){
	$roleTags = array();
	if(!empty($params)){
		/*$roleTags = $this->query("SELECT `key`, GROUP_CONCAT( `value` ) AS `value` FROM `role_tags` AS `RoleTag`   WHERE FIND_IN_SET(`key`,"."'".$params['tagKey']."'".") GROUP BY `key`");*/
		// $roleTags = $this->query("SELECT `key`, `value`  AS `value` FROM `role_tags` AS `RoleTag`   WHERE `key`="."'".$params['tagKey']."'"."  AND `status` = 1 ORDER BY `value`");
		// $roleTags = $this->query("SELECT `key`, `value`  AS `value` FROM `role_tags` AS `RoleTag`   WHERE `key`="."'".$params['tagKey']."'"."  ORDER BY `value`");
		$roleTags = $this->query("SELECT `key`, `value`  AS `value` FROM `role_tags` AS `RoleTag`   WHERE `key`="."'".$params['tagKey']."'"."  AND `trust_id`=336  AND `status` = 1 ORDER BY `value`");
	}
	return $roleTags;
}

public function roleTagValsLitev2($params=array()){
	$roleTags = array();
	if(!empty($params)){
		$profession_id = ($params['tagKey'] ==  "Speciality" ) ? $params['profession_id'] : 0;
		/*$roleTags = $this->query("SELECT `key`, GROUP_CONCAT( `value` ) AS `value` FROM `role_tags` AS `RoleTag`   WHERE FIND_IN_SET(`key`,"."'".$params['tagKey']."'".") GROUP BY `key`");*/
		$roleTags = $this->query("SELECT `key`, `value`  AS `value` FROM `role_tags` AS `RoleTag`   WHERE `key`="."'".$params['tagKey']."'"."  AND `trust_id`="."'".$params['institute_id']."'"."  AND `profession_id`="."'".$profession_id."'"." AND `status` = 1 ORDER BY `value`");
	}
	return $roleTags;
}

public function roleTagValsLiteNew($params=array()){
	$roleTags = array();
	if(!empty($params)){
		$profession_id = ($params['tagKey'] ==  "Speciality" ) ? $params['profession_id'] : 0;
		/*$roleTags = $this->query("SELECT `key`, GROUP_CONCAT( `value` ) AS `value` FROM `role_tags` AS `RoleTag`   WHERE FIND_IN_SET(`key`,"."'".$params['tagKey']."'".") GROUP BY `key`");*/
		$roleTags = $this->query("SELECT `key`, `value`  AS `value` FROM `role_tags` AS `RoleTag`   WHERE `key`="."'".$params['tagKey']."'"."  AND `trust_id`="."'".$params['institute_id']."'"."  AND `profession_id`="."'".$profession_id."'"." AND `status` = 1 ORDER BY `value`");
	}
	return $roleTags;
}

/*
-----------------------------------------------------
On: 20-12-2017
I/P: $params
O/P: Role Tags Value
Desc:
-----------------------------------------------------
*/
public function roleTagsInstituteWise($params=array()){
	$roleTags = array();
	if(!empty($params)){
		$roleTags = $this->find('all', array('conditions'=> array('key'=> $params['tagKey'],"trust_id"=>array(0, $params['institution_id'])), 'order'=> array('value')));
	}
	return $roleTags;
}

public function getUserRoleTagValue($params){
	$data = array();
	if(!empty($params['user_id']))
	{
		foreach ($params['user_id'] as $value) {
			$userDetails = $this->find("first", array("conditions"=> array("id"=> $value['role_tag_id'])));
			if(!empty($userDetails))
			{
				$data[] = array('key'=>$userDetails['RoleTag']['key'],'value'=>$userDetails['RoleTag']['value']);
			}
		}
	}
	return $data;
}

}
