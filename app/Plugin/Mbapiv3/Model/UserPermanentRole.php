<?php

App::uses('Model', 'Model');

/**
 * Represents model UserPermanentRole
 */
class UserPermanentRole extends Model
{
	/*
	-----------------------------------------------------------
	On: 06-03-2019
	I/P: $userId
	O/P: array
	Desc: fetch user all permanent roles
	-----------------------------------------------------------
	*/
	public function getUserPermanentRoleDetail($userId = NULL, $instituteId = NULL){
		$returnData = array();
		$conditions = array("UserPermanentRole.user_id"=> $userId , "UserPermanentRole.active"=> 1, "UserPermanentRole.institute_id"=> $instituteId);
		$returnData = $this->find('all',
							array(
								'joins'=>
								array(
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfile',
										'type' => 'left',
										'conditions'=> array('UserPermanentRole.user_id = UserProfile.user_id')
									),
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfiles',
										'type' => 'left',
										'conditions'=> array('UserPermanentRole.other_user_id = UserProfiles.user_id')
									),
									array(
										'table' => 'user_duty_logs',
										'alias' => 'UserDutyLog',
										'type' => 'left',
										'conditions'=> array('UserProfiles.user_id = UserDutyLog.user_id')
									)
								),
								'fields'=> array("UserPermanentRole.id", "UserPermanentRole.user_id","UserPermanentRole.other_user_id","UserPermanentRole.user_role_id","UserPermanentRole.role","UserPermanentRole.status","UserPermanentRole.notes", "UserPermanentRole.created_at","UserPermanentRole.updated_at","UserProfile.first_name","UserProfile.last_name","UserProfile.profile_img","UserProfile.thumbnail_img", "UserProfiles.user_id", "UserProfiles.first_name","UserProfiles.last_name","UserProfiles.profile_img","UserProfiles.thumbnail_img", "UserDutyLog.status","UserDutyLog.atwork_status"),
								'conditions' => $conditions,
								'group' => array('UserPermanentRole.id'),
								'order'=> array('UserPermanentRole.status DESC, UserPermanentRole.updated_at DESC')
							)
						);
		return $returnData;

    }

    /*
    --------------------------------------------------------------------------
    On: 02-07-2019
    I/P:
    O/P: array
    Desc: fetch all users whose transfer interval is expired
    --------------------------------------------------------------------------
     */
    public function getUserForExpiration()
    {
        $returnData = array();
        $conditions = array(
            "UserPermanentRole.active" => 1,
            "OR" => array(
                array("UserPermanentRole.status" => 2)
            ),
            "UserPermanentRole.updated_at + INTERVAL 1 HOUR < now()"
        );
        $returnData = $this->find('all',
            array(
                'fields' => array("UserPermanentRole.id", "UserPermanentRole.user_id", "UserPermanentRole.other_user_id", "UserPermanentRole.user_role_id", "UserPermanentRole.status", "UserPermanentRole.created_at"),
                'conditions' => $conditions,
            )
        );
        return $returnData;

    }
}
