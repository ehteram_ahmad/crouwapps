<?php 
/*
  Represents Institution Information model
*/
App::uses('AppModel', 'Model');

class InstitutionInformation extends AppModel {
	 public $name = 'InstitutionInformation';
	
	/*
	---------------------------------------------------------------------------------------------
	On: 28-04-2016
	I/P: 
	O/P: 
	Desc: Fetches institution info
	---------------------------------------------------------------------------------------------
	*/
	public function institutionInfo($userId = NULL){
		$institutionDetails = array();
		$conditions = " InstitutionInformation.user_id = " . $userId;
		//** Fetch Fields
		$fields = array("InstitutionInformation.id, InstitutionInformation.institution_type, 
			InstitutionInformation.institution_description, InstitutionInformation.institution_phone,
			InstitutionInformation.institution_sort_name,InstitutionInformation.institution_full_name, 
			InstitutionInformation.status, InstitutionInformation.user_id, 
			InstitutionInformation.institution_contact_person_email,  
			InstitutionType.institution_type, InstitutionType.status");
			
		if(!empty($userId)){
			$institutionDetails = $this->find('first', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'institution_types',
										'alias' => 'InstitutionType',
										'type' => 'left',
										'conditions'=> array('InstitutionInformation.institution_type = InstitutionType.id')
									),
									
								),
								'fields'=> $fields,
								'conditions' => $conditions,
							)
						);
		}
		return $institutionDetails;
	}
}
?>