<?php


App::uses('Model', 'Model');

/**
 * Represents model user_role_tags
 */
class UserRoleTag extends Model {
	public function getUserRoleTagData($userId, $userRoleTag)
	{
		$userRoleTagData = $this->query("SELECT *
                                                FROM user_role_tags AS UserRoleTag
                                                LEFT JOIN role_tags AS RoleTag
                                                ON UserRoleTag.role_tag_id = RoleTag.id
                                                where UserRoleTag.user_id=$userId AND RoleTag.key='".$userRoleTag."'
                                                LIMIT 1");
		return $userRoleTagData;
	}

	/*
	------------------------------------------------------------------------------
	On: 26-10-2017
	I/P:
	O/P:
	Desc: Get user role tag
	------------------------------------------------------------------------------
	*/
	public function getUserRoleTag($params = array()){
		$userRoleTagData = array();
		if(!empty($params)){
		$userRoleTagData = $this->query("SELECT *
                                                FROM user_role_tags AS UserRoleTag
                                                LEFT JOIN role_tags AS RoleTag
                                                ON UserRoleTag.role_tag_id = RoleTag.id
                                                where UserRoleTag.user_id='".$params['user_id']."'");
		}
		return $userRoleTagData;
	}

	public function getUserRoleTagLite($userIds){
		$userRoleTagData = array();
		if(isset($$userIds)){
		$userRoleTagData = $this->query("SELECT *
                                                FROM user_role_tags AS UserRoleTag
                                                LEFT JOIN role_tags AS RoleTag
                                                ON UserRoleTag.role_tag_id = RoleTag.id
                                                where UserRoleTag.user_id='".$$userIds."'");
		}
		return $userRoleTagData;
	}
	
}
