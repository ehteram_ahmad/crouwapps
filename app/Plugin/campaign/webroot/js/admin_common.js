"<?php  define('ImageSizeToUpload','10485760'); define('DocSizeToUpload','10485760');?>"
$(window).load(function() {
    $(".loader").fadeOut("fast");
})
function rsh(url,src){ 
    $fId=$("div#main div.active").attr('id');
    $('#txt'+$fId).val(url);
    $("div#"+$fId+",div.blurr,button#shtBtn").addClass("hidden");
    $('img#'+$fId).attr('src',src);
    $('body').removeClass('stop-scrolling');
    $('#'+$fId).removeClass('active');
    $('#main'+$fId+' + div').removeClass('hidden');
}
function sh(val){
      $('div#editimg'+val+',div.blurr,button#shtBtn').fadeToggle().removeClass("hidden");
      $('body').addClass('stop-scrolling');
      
    var container = document.getElementById('editimg'+val)
    var editor = new PhotoEditorSDK.UI.ReactUI({
      container: container,
      assets: {
        baseUrl: BASE_URL+'admin/assets' // <-- This should be the absolute path to your `assets` directory
      }
    })
    $('div#editimg'+val).addClass('active');
  }
function rmvEditor(){
    $fId=$("div#main div.active").attr('id');
    $('div#'+$fId+',div.blurr,button#shtBtn').addClass("hidden");
    $('body').removeClass('stop-scrolling');
    $('#'+$fId).removeClass('active');
}
function hashCount(){
    $(".loader").fadeIn("fast");
    var describTxt=document.getElementById("textDescrip").value;
    if(describTxt.trim() == ""){
        $(".loader").fadeOut("fast");
        alert("Please Enter the Description.");
        $("#textDescrip").focus();
        return false;
    }
    var descripHash=describTxt.match(/#\w+/g);
    var titleHash=document.getElementById("textTitle").value.match(/#\w+/g);
    $('#textHash').val(titleHash+','+descripHash);
}
function chckDoc(input,val,id){
    $max_size='<?php echo DocSizeToUpload; ?>';
        var ext = val.split('.').pop().toLowerCase();
        var myarray=["doc","docx","xls","xlsx","pdf","ppt","pptx","rtf"];
        if($.inArray(ext, myarray) !== -1){
            if(input.files[0].size <= $max_size){
                switch (ext) {
                case 'doc':case 'docx':case 'rtf':
                    var fileExt="doc";
                break;
                case 'xls':case 'xlsx':
                    var fileExt="excel";
                break;
                case 'pdf':
                    var fileExt="pdf";
                break;
                case 'ppt':case 'pptx':
                    var fileExt="ppt";
                break; }
                $('img#doc'+id).attr('src',"<?php echo BASE_URL.'admin/img/';?>"+fileExt+"-icon.png")
                var no=id;
                no++;
                $('div#doc'+no).removeClass('hidden');
            }else{
            alert("Please upload less than 10MB File");
        }
    }
        else{
            $('div#doc'+id).empty().append('<span class="btn btn-lg success btn-file"><span class="fileinput-exists"><img id="doc'+id+'" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>"></span><input type="hidden" value="" name=""><input type="file" name="data[addBeat][docs][]" id="doc'+id+'" class="form-control" onchange="chckDoc(this,value,'+id+');"></span><br><a onclick="rmvDoc('+id+');" id="doc'+id+'" class="btn default"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>');
            alert("Allowed extention are doc,docx,ppt,pptx,xls,xlsx,pdf");
        }
}
// Reset Image Inputs
function resetImgAll(){
    $('div#imageContent').empty().append('<div class="col-md-12"><span>Add Images: </span> / <a onclick="resetImgAll();">Reset</a><br><div class="row"><div id="maineditimg0" class="col-md-2"><span class="btn btn-lg success btn-file" onclick="sh(0);"><span class="fileinput-exists"><img id="editimg0" src="http://localhost/projects/ocr/ocrpoc/admin/img/plus.gif"></span><input type="hidden" value="" name=""></span><br><a onclick="rmvImg(0);" id="img0" class="btn default"><i class="fa fa-times" aria-hidden="true"></i>Remove</a></div><div id="maineditimg1" class="col-md-2 hidden"><span class="btn btn-lg success btn-file" onclick="sh(1);"><span class="fileinput-exists"><img id="editimg1" src="http://localhost/projects/ocr/ocrpoc/admin/img/plus.gif"></span><input type="hidden" value="" name=""></span><br><a data-dismiss="fileinput" onclick="rmvImg(1);" id="img1" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a></div><div id="maineditimg2" class="col-md-2 hidden"><span class="btn btn-lg success btn-file" onclick="sh(2);"><span class="fileinput-exists"><img id="editimg2" src="http://localhost/projects/ocr/ocrpoc/admin/img/plus.gif"></span><input type="hidden" value="" name=""></span><br><a data-dismiss="fileinput" onclick="rmvImg(2);" id="img2" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a></div><div id="maineditimg3" class="col-md-2 hidden"><span class="btn btn-lg success btn-file" onclick="sh(3);"><span class="fileinput-exists"><img id="editimg3" src="http://localhost/projects/ocr/ocrpoc/admin/img/plus.gif"></span><input type="hidden" value="" name=""></span><br><a data-dismiss="fileinput" onclick="rmvImg(3);" id="img3" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a></div><div id="maineditimg4" class="col-md-2 hidden"><span class="btn btn-lg success btn-file" onclick="sh(4);"><span class="fileinput-exists"><img id="editimg4" src="http://localhost/projects/ocr/ocrpoc/admin/img/plus.gif"></span><input type="hidden" value="" name=""></span><br><a data-dismiss="fileinput" onclick="rmvImg(4);" id="img4" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a></div><div id="maineditimg5" class="col-md-2 hidden"><span class="btn btn-lg success btn-file" onclick="sh(5);"><span class="fileinput-exists"><img id="editimg5" src="http://localhost/projects/ocr/ocrpoc/admin/img/plus.gif"></span><input type="hidden" value="" name=""></span><br><a data-dismiss="fileinput" onclick="rmvImg(4);" id="img5" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a></div></div></div>');
    $('div#main').empty().append('<div id="main" class="col-md-12"><div id="editimg0" class="hidden" style="width: 640px; height: 480px;"></div><div id="editimg1" class="hidden" style="width: 640px; height: 480px;"></div><div id="editimg2" class="hidden" style="width: 640px; height: 480px;"></div><div id="editimg3" class="hidden" style="width: 640px; height: 480px;"></div><div id="editimg4" class="hidden" style="width: 640px; height: 480px;"></div><div id="editimg5" class="hidden" style="width: 640px; height: 480px;"></div></div>');
}
// Remove Image Inputs
function rmvImg(id){
    $('div#maineditimg'+id).empty().append('<span class="btn btn-lg success btn-file" onclick="sh('+id+');"><span class="fileinput-exists"><img id="editimg'+id+'" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>"></span></span><br><a onclick="rmvImg('+id+');" id="img'+id+'" class="btn default"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>');
        $('div#editimg'+id).empty();
}
//----------------------------------
// Reset Docs Inputs
function rstDoc(){
    $('#docContent').empty().append('<div class="col-md-12"><span>Add Files: (supported formats: doc, xls, ppt, pdf, rtf)</span> / <a onclick="rstDoc();">Reset</a><br><div class="row"><div id="doc0" class="col-md-2"><span class="btn btn-lg success btn-file"><span class="fileinput-exists"><img id="doc0" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>"></span><input type="hidden" value="" name=""><input type="file" name="data[addBeat][docs][]" id="doc0" class="form-control" onchange="chckDoc(this,value,0);"></span><br><a onclick="rmvDoc(0);" id="doc0" class="btn default"><i class="fa fa-times" aria-hidden="true"></i>Remove</a></div><div id="doc1" class="col-md-2 hidden"><span class="btn btn-lg success btn-file"><span class="fileinput-exists"><img id="doc1" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>"></span><input type="hidden" value="" name=""><input type="file" name="data[addBeat][docs][]" id="doc1" class="form-control" onchange="chckDoc(this,value,1);"></span> <br><a data-dismiss="fileinput" onclick="rmvDoc(1);" id="doc1" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a> </div> <div id="doc2" class="col-md-2 hidden"> <span class="btn btn-lg success btn-file"><span class="fileinput-exists"><img id="doc2" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>"></span><input type="hidden" value="" name=""><input type="file" name="data[addBeat][docs][]" id="doc2" class="form-control" onchange="chckDoc(this,value,2);"></span> <br><a data-dismiss="fileinput" onclick="rmvDoc(2);" id="doc2" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a> </div> <div id="doc3" class="col-md-2 hidden"> <span class="btn btn-lg success btn-file"><span class="fileinput-exists"><img id="doc3" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>"></span><input type="hidden" value="" name=""><input type="file" name="data[addBeat][docs][]" id="doc3" class="form-control" onchange="chckDoc(this,value,3);"></span> <br><a data-dismiss="fileinput" onclick="rmvDoc(3);" id="doc3" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a></div></div></div>');
}
// Remove Docs Inputs
function rmvDoc(id){
    $('div#doc'+id).empty().append('<span class="btn btn-lg success btn-file"><span class="fileinput-exists"><img id="doc'+id+'" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>"></span><input type="hidden" value="" name=""><input type="file" name="data[addBeat][docs][]" id="doc'+id+'" class="form-control" onchange="chckDoc(this,value,'+id+');"></span><br><a onclick="rmvDoc('+id+');" id="doc'+id+'" class="btn default"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>');
}
//Speciality Group Action ----START----
function specialityGroupAction(){
    
    if($('tbody tr').find(':input[type="checkbox"]').is(':checked') === true){
    var inputElems = document.getElementsByTagName("input");
    length=inputElems.length;
    switch ($('#selectSpecialityAction').val()){

    case '1': //Activate
    var cnf=confirm("You are going to Activate multiple Specialities.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateSpecilityGroup(id,'Inactive');
            }
        }
        alert("Speciality's succesfully Activated");
        location.reload(true);
        }else{
        $('#selectSpecialityAction').val("");
    } 
        break;

    case '2': //Inactivate
    var cnf=confirm("You are going to Inactivate multiple Specialities.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateSpecilityGroup(id,'Active');
            }
        }
        alert("Speciality's succesfully Inactivated");
        location.reload(true);
        }else{
        $('#selectSpecialityAction').val("");
    } 
        break;
    }
    }
    else{
        $('#selectSpecialityAction').val("");
        alert("No Speciality's is selected.");
    }   
}
function updateSpecilityGroup( val,status ){
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/specility/changeSpecilityStatus",
            type: 'POST',
            data: {"specId": val, "statusVal": status },
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    var statusButtonString = '';
                if(data[1] == 'Active'){
                    statusButtonString = '<button type="button" onclick="updateSpecilityStatus('+val+','+ '\'Active\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                }else{
                    statusButtonString = '<button type="button" onclick="updateSpecilityStatus('+val+','+ '\'Inactive\''+');" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                }
               $('#statusButton'+val).html( statusButtonString  ); 
            }
            else{
                alert( result );
            }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
//Speciality Group Action ----END----

//Content Group Action ----START----
function contentGroupAction(){
    
    if($('tbody tr').find(':input[type="checkbox"]').is(':checked') === true){
    var inputElems = document.getElementsByTagName("input");
    length=inputElems.length;
    switch ($('#selectContentAction').val()){

    case '1': //Activate
    var cnf=confirm("You are going to Activate multiple Contents.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateContentGroup(id,'Inactive');
            }
        }
        alert("Content's succesfully Activated");
        location.reload(true);
        }else{
        $('#selectContentAction').val("");
    } 
        break;

    case '2': //Inactivate
    var cnf=confirm("You are going to Inactivate multiple Contents.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateContentGroup(id,'Active');
            }
        }
        alert("Content's succesfully Inactivated");
        location.reload(true);
        }else{
        $('#selectContentAction').val("");
    } 
        break;

        case '3': //Delete
            var cnf=confirm("You are going to Delete multiple Contents. Do you want to proceed?");
            if(cnf==true){
            for (var i=0; i<length; i++) {
                if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
                var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
                  $.ajax({
                     url: '<?php echo BASE_URL; ?>admin/Content/deleteContent',
                     type: "POST",
                     data: {'id': id},
                     success: function(data) { 
                    }
                });
                }
            }
            alert("Content's succesfully Deleted");
            location.reload(true);
            }
            else{
            $('#selectContentAction').val("");
            } 
        break;
    }
    }
    else{
        $('#selectContentAction').val("");
        alert("No Content's is selected.");
    }   
}
function updateContentGroup( val,status ){
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/Content/changeContentStatus",
            type: 'POST',
            data: {"contentId": val, "status": status },
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    var statusButtonString = '';
                if(data[1] == 'Active'){
                    statusButtonString = '<button type="button" onclick="changeContentStatus('+val+','+ '\'Active\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                }else{
                    statusButtonString = '<button type="button" onclick="changeContentStatus('+val+','+ '\'Inactive\''+');" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                }
               $('#statusButton'+val).html( statusButtonString  ); 
            }
            else{
                alert( result );
            }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
function changeContentStatus( val,status){ 
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/Content/changeContentStatus",
            type: 'POST',
            //data: {"user_id": val, "statusVal": $('#'+val).html()},
            data: {"contentId": val, "status": status},
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    alert("Status Changed."); 
                    //$('#'+val).html( data[1] ); 
                    var statusButtonString = '';
                    if(data[1] == 'Active'){
                        statusButtonString = '<button type="button" onclick="changeContentStatus('+val+','+ '\'Inactive\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                    }else{
                        statusButtonString = '<button onclick="changeContentStatus('+val+','+ '\'Active\''+');" type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                    }
                   $('#statusButton'+val).html( statusButtonString  ); 
                   location.reload(true);
                }
                else{
                    alert( result );
                }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
//Content Group Action ----END----
//Email Group Action ----START----
function emailGroupAction(){
    
    if($('tbody tr').find(':input[type="checkbox"]').is(':checked') === true){
    var inputElems = document.getElementsByTagName("input");
    length=inputElems.length;
    switch ($('#selectEmailAction').val()){

    case '1': //Activate
    var cnf=confirm("You are going to Activate multiple Emails. Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateEmailGroup(id,'Inactive');
            }
        }
        alert("Email's succesfully Activated");
        location.reload(true);
        }else{
        $('#selectEmailAction').val("");
    } 
        break;

    case '2': //Inactivate
    var cnf=confirm("You are going to Inactivate multiple Emails. Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateEmailGroup(id,'Active');
            }
        }
        alert("Email's succesfully Inactivated");
        location.reload(true);
        }else{
        $('#selectEmailAction').val("");
    } 
        break;

    case '3': //Delete
    var cnf=confirm("You are going to Delete multiple Emails. Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
              $.ajax({
                       url: '<?php echo BASE_URL; ?>admin/emails/deleteEmail',
                       type: "POST",
                       data: {'Id': id},
                       success: function(data) { 
                          // $('#mail' + Id).hide();
                           location.reload();
                      }
                  });
            }
        }
        alert("Email's succesfully Deleted");
        location.reload(true);
        }else{
        $('#selectEmailAction').val("");
    } 
        break;
    }
    }
    else{
        $('#selectEmailAction').val("");
        alert("No Email's is selected.");
    }   
}
function updateEmailGroup( val,status ){
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/emails/changeEmailStatus",
            type: 'POST',
            data: {"id": val, "statusVal": status },
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    var statusButtonString = '';
                if(data[1] == 'Active'){
                    statusButtonString = '<button type="button" onclick="updateEmailStatus('+val+','+ '\'Active\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                }else{
                    statusButtonString = '<button type="button" onclick="updateEmailStatus('+val+','+ '\'Inactive\''+');" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                }
               $('#statusButton'+val).html( statusButtonString  ); 
            }
            else{
                alert( result );
            }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
//Email Group Action ----END----

// group actions function ---START----
function groupAction(cn){
    if($('tbody tr').find(':input[type="checkbox"]').is(':checked') === true){
    var inputElems = document.getElementsByTagName("input");
    length=inputElems.length;

    switch ($('#selectAction').val()){

    case '1': //Active
    var cnf=confirm("You are going to Activate multiple Beats.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updatePostGroup(id,'Inactive');
            }
        }
        alert("Beat's succesfully Activated");
        location.reload(true);
        }else{
        $('#selectAction').val('0');
    } 
        break;

    case '2': //Inactive
    var cnf=confirm("You are going to Inactivate multiple Beats.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updatePostGroup(id,'Active');
            }
        }
        alert("Beat's succesfully Inactivated");
        location.reload(true);
        }else{
        $('#selectAction').val('0');
    } 
        break;

    case '3': //Flagged
    var cnf=confirm("You are going to Mark Flagg multiple Beats.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            flagGroup(id);
            }
        }
        alert("Beat's succesfully Flagged");
        location.reload(true);
        }else{
        $('#selectAction').val('0');
    } 
        break;

    case '4': //Featured
    if(cn < '10'){
        count=0;
    var cnf=confirm("You are going to Mark Feature multiple Beats.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var flagCon=$('tbody tr').find(':input[type="checkbox"]').eq(i).val();
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            if(flagCon==1){
                count++;
            }
            }
        }
        if(count>0){
            alert("One or more Beat(s) are Flagged in selected Beats. Please unselect them first and continue.");
        }
        else{
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            featureGroup(id);
            }
        }
        alert("Beat's succesfully Featured");
        location.reload(true);
        }
        }
        else{
        $('#selectAction').val('0');
    } 
    }
    else{
        alert("Maximum limit reached.");
    }
        break;

    case '5': //Delete
    var cnf=confirm("You are going to Delete multiple Beats.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            $.ajax({
                   url: '<?php echo BASE_URL; ?>admin/UserPost/deleteBeat',
                   type: "POST",
                   data: {'beatId': id},
                   success: function(data) { 
                      //alert(data);
                      $('#beat' + beatId).hide();
                        location.reload();
                  }
              });
            }
        }
        alert("Beat's succesfully Deleted");
        location.reload(true);
        }
        else{
        $('#selectAction').val('0');
    } 
        break;

    }
    }
    else{
        $('#selectAction').val('0');
        alert("No Beat's is selected.");
    }   
}
function updatePostGroup(val,statusVal){
    $.ajax({
        url: "<?php echo BASE_URL; ?>admin/UserPost/changeUserStatus",
        type: 'POST',
        // data: {"specId": val, "statusVal": $('#'+val).html()},
        data: {"user_id": val, "statusVal": statusVal},
        success: function( result ){
            var data = result.split("~");
            if( data[0] == "OK" ){  
                //$('#'+val).html( data[1] ); 
                var statusButtonString = '';
                if(data[1] == 'Active'){
                    statusButtonString = '<button type="button" onclick="updatePostStatus('+val+','+ '\'Active\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                }else{
                    statusButtonString = '<button type="button" onclick="updatePostStatus('+val+','+ '\'Inactive\''+');" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                }
               $('#statusButton'+val).html( statusButtonString  ); 
            }
            else{
                alert( result );
            }              
        }
    });
}
function flagGroup(val){
    $.ajax({
        url: "<?php echo BASE_URL; ?>admin/UserPost/markFlag",
        type: 'POST',
        data: {"post_id": val},
        success: function( result ){     
        }
    }); 
}
function featureGroup(val){
    $.ajax({
        url: "<?php echo BASE_URL; ?>admin/UserPost/markFeature",
        type: 'POST',
        data: {"post_id": val},
        success: function( result ){       
        }
    });
}
// group actions function ---END----
// group actions function for Users---START----
function userGroupAction(){
    if($('tbody tr').find(':input[type="checkbox"]').is(':checked') === true){
    var inputElems = document.getElementsByTagName("input");
    length=inputElems.length;
    switch ($('#selectUserAction').val()){
    case '1': //Activate
    var cnf=confirm("You are going to Activate multiple Users.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateUserGroup(id,'Inactive');
            }
        }
        alert("User's succesfully Activated");
        location.reload(true);
        }else{
        $('#selectUserAction').val('0');
    } 
        break;

    case '2': //Deactivate
    var cnf=confirm("You are going to Deactivate multiple Users.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateUserGroup(id,'Active');
            }
        }
        alert("User's succesfully Deactivated");
        location.reload(true);
        }else{
        $('#selectUserAction').val('0');
    } 
        break;

    case '3': //Approve
    var cnf=confirm("You are going to Approve multiple Users.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateUserApproveStatus(id,'Unapproved');
            }
        }
        alert("User's succesfully Approved");
        location.reload(true);
        }else{
        $('#selectUserAction').val('0');
    } 
        break;

    case '4': //Unapprove
    var cnf=confirm("You are going to Unapprove multiple Users.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            updateUserApproveStatus(id,'Approved');
            }
        }
        alert("User's succesfully Unapproved");
        location.reload(true);
        }else{
        $('#selectUserAction').val('0');
    } 
        break;

    case '5': //Delete
    var cnf=confirm("You are going to Delete multiple Beats.Do you want to proceed?");
        if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/deleteUser',
                 type: "POST",
                 data: {'userId': id},
                 success: function(data) { 
                    // alert(data);
                    $('#user' + id).hide();
                }
            });
            }
        }
        alert("User's succesfully Deleted");
        location.reload(true);
        }else{
        $('#selectUserAction').val('0');
    } 
        break;

    }
    }
    else{
        $('#selectUserAction').val('0');
        alert("No User's is selected.");
    }   
}
function updateUserGroup(val,statusVal){ 
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/user/changeUserStatus",
            type: 'POST',
            //data: {"user_id": val, "statusVal": $('#'+val).html()},
            data: {"user_id": val, "statusVal": statusVal},
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    // alert("Status Changed."); 
                    //$('#'+val).html( data[1] ); 
                    var statusButtonString = '';
                    if(data[1] == 'Active'){
                        statusButtonString = '<button type="button" onclick="updateStatus('+val+','+ '\'Inactive\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                    }else{
                        statusButtonString = '<button onclick="updateStatus('+val+','+ '\'Active\''+');" type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                    }
                   $('#statusButton'+val).html( statusButtonString  ); 
                   location.reload(true);
                }
                else{
                    alert( result );
                }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
function updateUserApproveStatus(val,approveVal){ 
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/user/changeUserApproveStatus",
            type: 'POST',
            //data: {"user_id": val, "statusVal": $('#approve'+val).html()},
            data: {"user_id": val, "statusVal": approveVal},
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    // alert("Status Changed."); 
                    //$('#approve'+val).html( data[1] ); 
                    var statusButtonString = '';
                    if(data[1] == 'Approved'){
                        statusButtonString = '<button type="button"  onclick="updateApproveStatus('+val+','+ '\'Unapproved\''+');" data-original-title="Approved" class="btn btn-circle btn-icon-only btn-info3 tooltips" aria-describedby="tooltip852273"><i class="fa fa-thumbs-up"></i></button>';
                    }else{
                        statusButtonString = '<button type="button" onclick="updateApproveStatus('+val+','+ '\'Approved\''+');" data-original-title="Unapproved" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip62607"><i class="fa fa-thumbs-o-down"></i></button>';
                    }
                   $('#approveButton'+val).html( statusButtonString  ); 
                }
                else{
                    alert( result );
                }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}

// group actions function for Users---END----
$(document).ready(function(){
    // Post the path to youtube.
 $('.youtubePath').on('click',function(){
        var id = $('#youtubePath').val();
        if(id==''){
            $('#loader').html("Please enter the search keyword.");
            return false;
        }
           $.ajax({
            type: 'POST',
            url: BASE_URL+'admin/UserPost/youtubepaths',
            data:{key: id},
            beforeSend:function(data) {
              $('#loader').html("Please wait for some time..");
            },

            success: function(data) {
              $('#showData').html(data);
            },
            error:function(err){
              alert("error"+JSON.stringify(err));
            },
            complete:function(data) {
              $('#loader').hide();
             
            },
            
          }); // Ajax implementation end

    });
});

// Group Delete FeedBack--[START]
function deleteFeedbackGroup(){
    if($('tbody tr').find(':input[type="checkbox"]').is(':checked') === true){
    var inputElems = document.getElementsByTagName("input");
    length=inputElems.length;
    var cnf=confirm("You are going to Delete multiple FeedBacks.Do you want to proceed?");
    if(cnf==true){
        for (var i=0; i<length; i++) {
            if ($('tbody tr').find(':input[type="checkbox"]').eq(i).is(':checked') === true){
            var id=$('tbody tr').find(':input[type="checkbox"]').eq(i).attr('id');
            $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/feedback/deleteFeedback',
                 type: "POST",
                 data: {'Id': id},
                 success: function(data) {
                 $('#feedBack' + id).hide(); 
                }
            });
            }
        }
        alert("FeedBacks's succesfully Deleted");
        }
    }
    else{
        alert("No FeedBack's selected.");
    } 
}
// Group Delete FeedBack--[END]

/*
On: 10 Aug;2016
I/P:value of all the input type
O/P: send data to controller function for saving the data
Desc: It will take the data from all the edited inputs to update the details.
*/
    function editBeat(){
        $(".loader").fadeIn("fast");
        var descrpTxt=$("#comments2").html();
        if(descrpTxt.trim()=="Empty"){
            $(".loader").fadeOut("fast");
            alert("Please enter the Description.");
            $("#comments2").click();
            return false;
        }
        var titleTags = $("#comments").html().match(/#\w+/g);
        var descTags = descrpTxt.match(/#\w+/g);
        // var descTags = CKEDITOR.instances.comments2.getData().match(/#\w+/g);

        $('#title').val($('#comments').html());
        $('#interests').val($('#tags').html());
        $('#descriptions').val($("#comments2").html());
        $('#hashtags').val($('#tagTxt').html());
        $('#newHash').val(titleTags+','+descTags);
        $('#video').val($('#link1 span').html());
        $('#docTxt1').val($('#doc1').html());
        $('#docTxt2').val($('#doc2').html());
        $('#docTxt3').val($('#doc3').html());
        $('#docTxt4').val($('#doc4').html());
        $('#status1').val($('#statusA').html());
        $('#status2').val($('#statusF').html());
        $('#imgTxt0').val($('img#editimg0').attr('src'));
        $('#imgTxt1').val($('img#editimg1').attr('src'));
        $('#imgTxt2').val($('img#editimg2').attr('src'));
        $('#imgTxt3').val($('img#editimg3').attr('src'));
        $('#imgTxt4').val($('img#editimg4').attr('src'));
        $('#imgTxt5').val($('img#editimg5').attr('src'));
           
    }
    /*
    On: 8 Aug;2016
    I/P: input type file(image)
    O/P: Preview of uploaded image
    Desc: showing the preiview of selected image in cache without saving. Its replacing the src of img tag as a preview image with a encrypted src.
    */

    function readURL(input,id) {
        var ext = input.files[0].name.split('.').pop().toLowerCase();
        var myarray=["jpg","jpeg","png"];
        if($.inArray(ext, myarray) !== -1){
    $max_size='<?php echo ImageSizeToUpload; ?>';
    if(input.files[0].size <= $max_size){
            if (input.files && input.files[0]) {
                var file    = document.querySelector('input[type=file]').files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                $('img#'+id)
                .attr('src', e.target.result)
                .width(120)
                .height(120);
                $('#'+id+' a').attr('href',e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        else{
            alert("Please upload less than 10MB image");
            $('#'+id).empty();
            $('#'+id).html('<img id="icon" src="'+BASE_URL+'admin/img/plus.gif"><input type="file" name="data[addSpeciality][icon]" id="spclIcon" class="form-control" onchange="readURL(this,&quot;icon&quot;);">');
        }}
        else{
            alert("Only JPG, JPEG & PNG extensions are allwoed.");
            $('#'+id).empty();
            $('#'+id).html('<img id="icon" src="'+BASE_URL+'admin/img/plus.gif"><input type="file" name="data[addSpeciality][icon]" id="spclIcon" class="form-control" onchange="readURL(this,&quot;icon&quot;);">');
        }
    }
    /*
    On: 7 Aug;2016
    I/P: input type file(document)
    O/P: changing the icon and html
    Desc: when selecting a file,it will provide an icon for the file type selected.
    */
    function upDoc(input,id){
        $max_size='<?php echo DocSizeToUpload; ?>';
            var file=$("#file"+id).val();
            var ext = file.split('.').pop().toLowerCase();
            var myarray=["doc","docx","xls","xlsx","pdf","ppt","pptx","rtf"];
            if($.inArray(ext, myarray) !== -1){
        if(input.files[0].size <= $max_size){
        if (input.files && input.files[0]) {
            switch (ext) {
            case 'doc':case 'docx':case 'rtf':
                var fileExt="word";
            break;
            case 'xls':case 'xlsx':
                var fileExt="excel";
            break;
            case 'pdf':
                var fileExt="pdf";
            break;
            case 'ppt':case 'pptx':
                var fileExt="powerpoint";
            break; }
            $('span#'+id).empty().prepend('<a class="fontSize20"> <i class="fa fa-file-'+fileExt+'-o" aria-hidden="true"></i></a> &nbsp;<a data-dismiss="fileinput" class="btn btn-xs btn default fileinput-exists" onclick="del('+id+');" href="javascript:;"> <i class="fa fa-times" aria-hidden="true"></i><span hidden id="doc'+id+'">'+file+'</a>');
            $('#up'+id).addClass("hidden");
        }}
        else{
            alert("Please upload less than 10MB File");
        }}
        else{
            alert("Allowed extention are doc,docx,ppt,pptx,xls,xlsx,pdf");
        }
    }
    /*
    On: 8 aug;2016
    I/P: id of the container of document icon
    O/P: blank file icon with an upload button
    Desc: It will delete the html of existing file and replaces with the upload icon and a blank file icon.
    */
    function del(id){
        $('span#'+id).empty().prepend('<a class="fontSize20" id="link5" data-type="text"><i class="fa fa-file-o tooltips" data-original-title="No file" aria-hidden="true"></i></a>');
        $('#up'+id).removeClass("hidden");
    }
    /*
    On: 8 AUg;2016
    I/P: 
    O/P: replace the existing image with a "noimage" image.
    Desc: It will replace the existing image with a blank one and provide an upload button for new image.
    */
    $(document).ready(function(){
        $('.imageinput-exists').click(function(){
            var imgId=$(this).attr('id');
           $('#'+imgId+' a').attr("href","<?php echo BASE_URL.'admin/img/noimage.jpg'; ?>");
           $('#'+imgId+' div.cbp-caption-defaultWrap img').attr("src","<?php echo BASE_URL.'admin/img/noimage.jpg'; ?>");
        });
    });
function searchData(){
    $( "#formSearch" ).prop( "disabled", true );

    $.ajax({
            url: "<?php echo BASE_URL; ?>admin/user/userListsSearch",
            type: 'POST',
            data: $('#formSearch').serialize(), 
            success: function( result ){
                $('#userList').html( result );                
            }
        });
    return false;
}

//** Change user status [START]
function updateStatus( val ,statusVal){ 
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/user/changeUserStatus",
            type: 'POST',
            //data: {"user_id": val, "statusVal": $('#'+val).html()},
            data: {"user_id": val, "statusVal": statusVal},
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    alert("Status Changed."); 
                    //$('#'+val).html( data[1] ); 
                    var statusButtonString = '';
                    if(data[1] == 'Active'){
                        statusButtonString = '<button type="button" onclick="updateStatus('+val+','+ '\'Inactive\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                    }else{
                        statusButtonString = '<button onclick="updateStatus('+val+','+ '\'Active\''+');" type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                    }
                   $('#statusButton'+val).html( statusButtonString  );
                   location.reload(true); 
                }
                else{
                    alert( result );
                }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
//** Change user status [END]

//** Get Modal Box[START]
function getModalBox( val ){
   $("#modalBox").html( 'Loading......' ); 
   $.ajax({
            url: "<?php echo BASE_URL; ?>admin/user/getModalBox",
            type: 'POST',
            data: {"user_id": val},
            success: function( result ){
                $("#modalBox").html( result ); 
                $("#myModal").modal('show');           
            }
        }); 
}
//** Get Modal Box[END]

function updatePostStatus(val,statusVal ){  
    if( val !='' ){ 

        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/UserPost/changeUserStatus",
            type: 'POST',
            // data: {"specId": val, "statusVal": $('#'+val).html()},
            data: {"user_id": val, "statusVal": statusVal},
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    alert("Status Changed."); 
                    //$('#'+val).html( data[1] ); 
                    var statusButtonString = '';
                    if(data[1] == 'Active'){
                        statusButtonString = '<button type="button" onclick="updatePostStatus('+val+','+ '\'Active\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                    }else{
                        statusButtonString = '<button type="button" onclick="updatePostStatus('+val+','+ '\'Inactive\''+');" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                    }
                   $('#statusButton'+val).html( statusButtonString  ); 
                   location.reload(true);
                }
                else{
                    alert( result );
                }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
//** Change user post status [END]
//** Change email status [Start]
function updateEmailStatus(val,statusVal ){  
    if( val !='' ){ 

        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/emails/changeEmailStatus",
            type: 'POST',
            // data: {"specId": val, "statusVal": $('#'+val).html()},
            data: {"id": val, "statusVal": statusVal},
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    alert("Status Changed."); 
                    //$('#'+val).html( data[1] ); 
                    var statusButtonString = '';
                    if(data[1] == 'Active'){
                        statusButtonString = '<button type="button" onclick="updateEmailStatus('+val+','+ '\'Active\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                    }else{
                        statusButtonString = '<button type="button" onclick="updateEmailStatus('+val+','+ '\'Inactive\''+');" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                    }
                   $('#statusButton'+val).html( statusButtonString  ); 
                   location.reload(true);
                }
                else{
                    alert( result );
                }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
//** Change email status [END]

//** Get Post Attributes[START]
function viewPostAttributes( val ){
   $.ajax({
            url: "<?php echo BASE_URL; ?>admin/UserPost/viewPostAttributes",
            type: 'POST',
            data: {"post_id": val},
            success: function( result ){
                $("#modalBox").html( result ); 
                $("#myModal").modal('show');           
            }
        }); 
}
//** Get Post Attributes[END]

//** Get Post Details[START]
function userPostDetails(val){
    alert();
   $.ajax({
            url: "<?php echo BASE_URL; ?>admin/UserPost/userPostDetails",
            type: 'POST',
            data: {"post_id": val},
            success: function( result ){
            		$("#modalBox").html('');
                $("#modalBox").html( result ); 
                $("#myModal").modal('show');           
            }
        }); 
}
//** Get Post Details[END]

//** Get Post Comments[START]
function viewPostComments( val ){
   $.ajax({
            url: "<?php echo BASE_URL; ?>admin/UserPost/viewPostComments",
            type: 'POST',
            data: {"post_id": val},
            success: function( result ){
                $("#modalBox").html( result ); 
                $("#myModal").modal('show');           
            }
        }); 
}
//** Get Post Comments[END]

//** Change user status [START]
function updateSpecilityStatus( val,status ){
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/specility/changeSpecilityStatus",
            type: 'POST',
            data: {"specId": val, "statusVal": status },
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    alert("Status Changed."); 
                    var statusButtonString = '';
                if(data[1] == 'Active'){
                    statusButtonString = '<button type="button" onclick="updateSpecilityStatus('+val+','+ '\'Active\''+');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>';
                }else{
                    statusButtonString = '<button type="button" onclick="updateSpecilityStatus('+val+','+ '\'Inactive\''+');" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                }
               $('#statusButton'+val).html( statusButtonString  );
               location.reload(true); 
            }
            else{
                alert( result );
            }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
//** Change user status [END]

function closeModal(){
    $("#modalBox").html(); 
    $(".modal").hide(); 
}

//** Change user status [START]
function updateContentStatus( val ){
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/Content/changeContentStatus",
            type: 'POST',
            data: {"contentId": val },
            dataType: "json",
            success: function( result ){
                if(result.status == 'Success'){
                    var status = "";
                    if(result.update_status == 1){
                        status = '<button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Active" type="button"><i class="fa fa-check"></i></button>';
                    }else{
                        status = '<button type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>';
                    }
                    alert("Status Changed."); 
                    $('#'+val).html(status); 
                    location.reload(true);
                }else{
                    alert(result.update_status);
                }       
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
$('.emailDetail').on('click',function(){
        var id = $(this).data('id');
        $('.modal-body').html('Loading');

           $.ajax({
            type: 'POST',
            url: BASE_URL+'admin/emails/emaildetail',
            data:{id: id},
            success: function(data) {
              $('.modal-body').html(data);
            },
            error:function(err){
              alert("error"+JSON.stringify(err));
            }
          }); // Ajax implementation end

    });
//** Change user status [END]

//** Change admin approve user status [START]
function updateApproveStatus( val ,approveVal){ 
    if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/user/changeUserApproveStatus",
            type: 'POST',
            //data: {"user_id": val, "statusVal": $('#approve'+val).html()},
            data: {"user_id": val, "statusVal": approveVal},
            success: function( result ){
                var data = result.split("~");
                if( data[0] == "OK" ){  
                    alert("Status Changed."); 
                    //$('#approve'+val).html( data[1] ); 
                    var statusButtonString = '';
                    if(data[1] == 'Approved'){
                        statusButtonString = '<button type="button"  onclick="updateApproveStatus('+val+','+ '\'Unapproved\''+');" data-original-title="Approved" class="btn btn-circle btn-icon-only btn-info3 tooltips" aria-describedby="tooltip852273"><i class="fa fa-thumbs-up"></i></button>';
                    }else{
                        statusButtonString = '<button type="button" onclick="updateApproveStatus('+val+','+ '\'Approved\''+');" data-original-title="Unapproved" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip62607"><i class="fa fa-thumbs-o-down"></i></button>';
                    }
                   $('#approveButton'+val).html( statusButtonString  );
                   location.reload(true); 
                }
                else{
                    alert( result );
                }              
            }
        });
    }else{
        alert("id is blank");
        return false;
    }
}
//** Change admin approve user status [END]

//** Edit user Profile [START]
function getEditUserProfile( userId ){
    if( userId !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/user/editUserProfileView",
            type: 'POST',
            data: {"user_id": userId},
            success: function( result ){ 
                $("#updateProfileModalBox").html( result );
                $(".updateProfile").modal('show');            
            }
        });
    }else{
        alert("UserId is blank");
        return false;
    }
}
//** Change admin approve user status [END]
//** Top not top update for posts [START]
function toggleTop(val){
	if( val !='' ){ 
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/UserPost/toggleTopPost",
            type: 'POST',
            data: {"post_id": val},
            success: function( result ){
            		var data = result.split("~");
                if( data[0] == "OK" ){ 
                		if (data[1] == "Top Beat"){
                			alert("Beat marked as top beat"); 
                		}else{
                			alert("Beat unmarked as top beat"); 
                		}
                    $('#mark_'+val).html( data[1] ); 
                }else if(data[0] == "NOTOK"){
                		alert(data[1]); 
                }
                else{
                    alert( result );
                }          
            }
        });
    }else{
        alert("Post id is blank");
        return false;
    }
}
//** Top not top update for posts [END]
//** Flag  posts [START]
	function flagUnflag(val){
		if( val !='' ){ 
			$.ajax({
	            url: "<?php echo BASE_URL; ?>admin/UserPost/flagPostView",
	            type: 'POST',
	            data: {"post_id": val},
	            success: function( result ){
	            		$("#modalBox").html('');
	                $("#modalBox").html( result ); 
	                $("#myModal").modal('show');       
	            }
	        });
	    }else{
	        alert("Post id is blank");
	        return false;
	    }
	}
//** Flag  posts [END]
		//** Flag  posts [START]
		function Unflag(val){
			if( val !='' ){ 
				$.ajax({
		            url: "<?php echo BASE_URL; ?>admin/UserPost/unFlagbeat",
		            type: 'POST',
		            data: {"post_id": val},
		            success: function( result ){
		            		alert('Beat unflagged successfully');
                        location.reload(true);       
		            }
		        });
		    }else{
		        alert("Post id is blank");
		        return false;
		    }	
				//** Flag  posts [END]
		}

        // function flag(val){
        //     if( val !='' ){
        //         $.ajax({
        //             url: "<?php echo BASE_URL; ?>admin/UserPost/markFlag",
        //             type: 'POST',
        //             data: {"post_id": val},
        //             success: function( result ){
        //                     alert('Beat flagged successfully');
        //                 location.reload(true);       
        //             }
        //         });
        //     }else{
        //         alert("Post id is blank");
        //         return false;
        //     }   
        //         //** Flag  posts [END]
        // }
function flag(){
    var post_id = $("span#flagId").html(); 
    var comment = $("#txt_comment").val();
    var spam_type = $("input[name='spam_type']:checked").val();
    var spam_content = ""
    if(spam_type){
      spam_content = $("input[name='spam_type']:checked").val();
    }
    if(spam_content == ""){
    alert('Please select spam type');
    return false;
    }
    if(comment == ""){
    alert('Please add your comment');
    $("#txt_comment").focus();
    return false;
    }else{
      // $("#btn_markspam").hide();
      // $("#show_progress").html('Please wait ...');
      $.ajax({
          url: "<?php echo BASE_URL; ?>admin/UserPost/markFlag",
          type: 'POST',
          data: {"post_id": post_id, "spam_type": spam_type, "comment" : comment},
          success: function( result ){
              if(result == "success"){
                  alert('Beat flagged successfully');
                  // closeModal();
                  location.reload(); 
              }          
          }
      });
    }
}


//** Feature Posts [START]
function feature(val,count){
    if(count < '10'){
    if( val !='' ){
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/UserPost/markFeature",
            type: 'POST',
            data: {"post_id": val},
            success: function( result ){
                    alert('Beat featured successfully');
                location.reload(true);       
            }
        });
    }else{
        alert("Post id is blank");
        return false;
    }   }
    else{
        alert("Maximum limit reached");
    }
        
}
//** Feature Posts [END]
//** Feature Posts [START]
function unfeature(val){
    if( val !='' ){
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/UserPost/unMarkFeature",
            type: 'POST',
            data: {"post_id": val},
            success: function( result ){
                    alert('Beat unfeatured successfully');
                location.reload(true);       
            }
        });
    }else{
        alert("Post id is blank");
        return false;
    }   
        
}
//** Feature Posts [START]
//** Get user view[START]
function userView( val ){
   
   window.location.href =  "<?php echo BASE_URL; ?>admin/user/userView/" + val;
}
//** Get user view[END]

 function error(){
    alert("Maximum limit reached");
 }
 //reset form values

 function resetVal(){
     $('.select2-selection__rendered li:not(:last)').remove();
     $('.select2-search__field').attr("placeholder", "Choose Interests...").css("width", "766px");
 }
