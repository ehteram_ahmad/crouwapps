BEGIN VIEW MAIL POP-UP --> 
  <div class="modal-dialog">
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <!-- <h4 class="modal-title">VIEW MAIL</h4> -->
      </div>
      <div class="modal-body">
        <div class="row">
            <!--/span-->
            <div class="col-md-12">
              <div class="form-group">
                  <label class="control-label col-md-3 bold">To:</label>
                      <div class="col-md-9">
                          <p class="form-control-static bold"> example@gmail.com </p>
                        </div>
                 </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
              <div class="form-group">
                  <label class="control-label col-md-3 bold">Subject:</label>
                      <div class="col-md-9">
                          <p id="viewSub" class="form-control-static"><?php echo $subject; ?></p>
                        </div>
                 </div>
            </div>
            <!--/span-->
        </div>
        <div class="form-group">
          <label class="control-label col-md-12 bold">Description:</label>
            <div class="clearfix"></div>
             <?php echo $content; ?><br>
        </div>
      </div>
      <div class="modal-footer textCenter">
        <button type="button" data-dismiss="modal" class="btn default">Close</button>
      </div>
  </div>
 </div>
<!-- END VIEW MAIL POP-UP