<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div id="menu" class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start">
                <a href="<?php echo BASE_URL . 'campaign/CampaignHome/index';?>" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                    <!--<span class="arrow open"></span>-->
                </a>
            </li>
            <!--<li class="heading">
                <h3 class="uppercase">Inner Pages</h3>
            </li>-->
            <!-- <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Users</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL . 'campaign/CampaignHome/userList';?>" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">All</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL . 'campaign/user/approvedUserLists';?>" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Approved</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL . 'campaign/user/pendingUserLists';?>" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Pending</span>
                        </a>
                    </li>
                </ul>
            </li> -->
            <li class="nav-item start">
                <a href="<?php echo BASE_URL . 'campaign/CampaignHome/templateList';?>" class="nav-link nav-toggle">
                    <i class="fa fa-envelope-o"></i>
                    <span class="title">Templates</span>
                    <span class="selected"></span>
                    <!--<span class="arrow open"></span>-->
                </a>
            </li>
            <!-- <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-desktop"></i>
                    <span class="title">Posts</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/UserPost/userPostLists" class="nav-link ">
                            <i class="fa fa-desktop"></i>
                            <span class="title">All</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/UserPost/featuredPostLists" class="nav-link ">
                            <i class="fa fa-desktop"></i>
                            <span class="title">Featured</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/UserPost/flaggedPostLists" class="nav-link ">
                            <i class="fa fa-desktop"></i>
                            <span class="title">Flagged</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">Specialties</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/Specility/allSpecilities" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Specialties Listings</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-clone"></i>
                    <span class="title">Manage Content</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/Content/listContents" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Content Listings</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-envelope-o"></i>
                    <span class="title">Email Templates</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/emails/index" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Template Listing</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-commenting-o"></i>
                    <span class="title">Manage Story Points</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/StoryPoint/manageStoryPoint" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Manage Story Point</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-comments-o"></i>
                    <span class="title">User Feedbacks</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/feedback/feedbackList" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">User Feedback Lists</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-mobile"></i>
                    <span class="title">Manage App Versions</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="<?php echo BASE_URL;?>admin/AppVersion/manageAppVersion" class="nav-link ">
                            <i class="fa fa-mobile"></i>
                            <span class="title">Update App Version</span>
                        </a>
                    </li>
                </ul>
            </li> -->
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>