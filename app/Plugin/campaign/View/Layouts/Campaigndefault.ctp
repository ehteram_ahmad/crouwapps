<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>The OCR | Campaign</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL The OCR STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <?php
        //** Admin CSS files
        echo $this->Html->css(array(
                     'Campaign.font-awesome',
                     'Campaign.simple-line-icons',
                     'Campaign.bootstrap',
                     'Campaign.uniform.default',
                     'Campaign.bootstrap-toggle',
                     'Campaign.bootstrap-switch.min',
                     'Campaign.daterangepicker.min',
                     'Campaign.morris',
                     'Campaign.fullcalendar.min',
                     'Campaign.jqvmap',
                     'Campaign.components',
                     'Campaign.plugins',
                     'Campaign.layout',
                     'Campaign.light',
                     'Campaign.custom.min',
                     'Campaign.profile',
                     'Campaign.select2.min',
                     'Campaign.select2-bootstrap.min',
                     'Campaign.custom',
                     'Campaign.datetimepicker',
                     'Campaign.bootstrap-editable',
                     'Campaign.bootstrap-fileinput',
                     'Campaign.bootstrap-wysihtml5',
                     'Campaign.bootstrap-markdown.min',
                     'Campaign.typeahead.js-bootstrap',
                     'Campaign.address',
                     'Campaign.cubeportfolio',
                     'Campaign.google-api-font'
            ));
        //** Admin JS files
        echo $this->Html->script(array('Campaign.jquery.min', 'Campaign.admin_common'));

        ?>
        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>favicon.ico" /> 
        <script language="Javascript">
        BASE_URL = '<?php echo BASE_URL; ?>'; 
        </script>
     </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo BASE_URL . 'campaign/CampaignHome'; ?>">
                        <?php echo $this->Html->image('Campaign.logo.png', array("alt"=> "logo", "class"=>"logo-default")); ?>
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="separator hide"> </li>
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            
                           
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?php
                            $nameUser = $this->Session->read('userName');
                             echo ( null !== $nameUser && !empty($nameUser) ? $this->Session->read('userName'):'' );?> </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <?php echo $this->Html->image('Campaign.admin.jpg', array("alt"=>"", "class"=> "img-circle")); ?>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!--<li>
                                        <a href="page_user_profile_1.html">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="app_todo_2.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success2"> 7 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>-->
                                    <li>
                                        <a href="<?php echo BASE_URL . 'campaign/CampaignHome/logout'?>">
                                            <i class="fa fa-power-off"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                           <!-- <li class="dropdown dropdown-extended quick-sidebar-toggler">
                                <span class="sr-only">Toggle Quick Sidebar</span>
                                <i class="icon-logout"></i>
                            </li>-->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
        
            <!-- BEGIN SIDEBAR -->
            <?php echo $this->element('left_menu'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE BREADCRUMB -->
        <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN CONTENT -->
            <?php echo $this->fetch('content'); ?>

            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; 
                <a style="color:#000; text-decoration:underline;" target="_blank" href="http://mediccreations.com/">
                    <?php echo $this->Html->image('Campaign.medic-logo-small.png', array("style"=> "margin-right: 4px;" , "alt"=> "Logo")); ?>
                    Medic Creations</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<script src="js/excanvas.min.js"></script> 
<![endif]-->
       <?php 
        echo $this->Html->script(array(
                'Campaign.bootstrap.min', 
                'Campaign.js.cookie.min', 
                'Campaign.bootstrap-hover-dropdown.min', 
                'Campaign.jquery.slimscroll.min', 
                'Campaign.jquery.blockui.min', 
                'Campaign.jquery.uniform.min', 
                'Campaign.bootstrap-switch.min', 
                'Campaign.moment.min', 
                'Campaign.daterangepicker.min',
                'Campaign.morris.min',
                'Campaign.raphael-min', 
                'Campaign.jquery.waypoints.min', 
                'Campaign.jquery.counterup.min', 
                'Campaign.amcharts', 
                'Campaign.serial',
                'Campaign.pie', 
                'Campaign.radar', 
                'Campaign.light', 
                'Campaign.patterns',
                'Campaign.chalk', 
                'Campaign.ammap', 
                'Campaign.worldLow', 
                'Campaign.amstock', 
                'Campaign.fullcalendar.min', 
                'Campaign.jquery.flot.min', 
                'Campaign.jquery.flot.resize.min', 
                'Campaign.jquery.flot.categories.min', 
                'Campaign.jquery.easypiechart.min', 
                'Campaign.jquery.sparkline.min', 
                'Campaign.jquery.vmap', 
                'Campaign.jquery.vmap.russia', 
                'Campaign.jquery.vmap.world', 
                'Campaign.jquery.vmap.europe', 
                'Campaign.jquery.vmap.germany', 
                'Campaign.jquery.vmap.usa', 
                'Campaign.jquery.vmap.sampledata', 
                'Campaign.wysihtml5-0.3.0', 
                'Campaign.bootstrap-wysihtml5', 
                'Campaign.markdown', 
                'Campaign.bootstrap-markdown', 
                'Campaign.summernote.min', 
                'Campaign.app.min', 
                'Campaign.dashboard', 
                'Campaign.layout.min', 
                'Campaign.demo.min', 
                'Campaign.bootstrap-toggle',
                'Campaign.test-pie', 
                'Campaign.select2.full.min',
                'Campaign.components-select3',
                'Campaign.bootstrap-datepicker.min',
                'Campaign.components-date-time-pickers.min',
                'Campaign.jquery.mockjax',
                'Campaign.bootstrap-datetimepicker',
                'Campaign.bootstrap-editable',
                'Campaign.typeahead',
                'Campaign.typeaheadjs',
                'Campaign.address',
                'Campaign.jquery.cubeportfolio',
                'Campaign.portfolio-4',
                'Campaign.demo-mock',
                'Campaign.form-demo',
                'Campaign.layout.min',
                'Campaign.loader'
            ));
       ?>
        <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
        <?php //echo $this->element('sql_dump'); ?>
    </body>

</html>