<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>The OCR | Campaign Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL The OCR STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>favicon.ico" />
        <?php
        echo $this->Html->css(array(
            'Campaign.font-awesome',
            // 'Campaign.simple-line-icons.min',
            'Campaign.bootstrap.min',
            // 'Campaign.uniform.default',
            // 'Campaign.bootstrap-switch.min',
            // 'Campaign.select2.min',
            // 'Campaign.select2-bootstrap.min',
            'Campaign.components',
            // 'Campaign.plugins.min',
            'Campaign.login-5',
            'Campaign.google-api-font'
            ));

        ?>
    </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN : LOGIN PAGE 5-2 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 login-container bs-reset">
                    <?php echo $this->Html->image('Campaign.logo.png', array("class"=> "login-logo login-6" )); ?>
                    <div class="login-content">
                        <h1>The OCR Campaign Login</h1>
                        <p>Share, Learn &amp; Collaborate with Medical Professionals Around the World to Help Improve Patient Care.</p>
                        <div class="login-form">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Enter any username and password. </span>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Username" name="textUserName" id="textUserName" required/> </div>
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="textPassword" id="textPassword" required/> </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                        <span id="loginMsg" class="text-lg" style="color: red;"></span>
                                    <!-- <div class="rem-password">
                                        <p>Remember Me
                                            <input type="checkbox" class="rem-checkbox" />
                                        </p>
                                    </div> -->
                                </div>
                                <div class="col-sm-8 text-right">
                                    <div class="forgot-password">
                                        <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
                                    </div>
                                    <button onclick="login();" class="btn blue" type="submit" >Sign In</button>
                                </div>
                            </div>
                        </div>
                        <!-- BEGIN FORGOT PASSWORD FORM -->
                        <!-- <form class="forget-form" method="post"> -->
                        <div class="forget-form">
                            <h3 class="font-green">Forgot Password ?</h3>
                            <p> Enter your e-mail address below to reset your password. </p>
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" id="textEmail" /> </div>
                            <div class="form-actions">
                                <button type="button" id="back-btn" class="btn grey btn-default">Back</button>
                                <button onclick="forgotPwd();" class="btn blue btn-success2 uppercase pull-right">Submit</button>
                            </div>
                            </div>
                        <!-- </form> -->
                        <!-- END FORGOT PASSWORD FORM -->
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-5 bs-reset">
                                <ul class="login-social">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-7 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>2016 &copy; 
                <a style="color:#000; text-decoration:underline;" target="_blank" href="http://mediccreations.com/">
                    <?php echo $this->Html->image('Campaign.medic-logo-small.png', array("style"=> "margin-right: 4px;", "alt"=> "Logo" )); ?>
                    Medic Creations</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bs-reset">
                    <div class="login-bg"> </div>
                </div>
            </div>
        </div>
        <!-- END : LOGIN PAGE 5-2 -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <?php 
        echo $this->Html->script(array(
            'Campaign.jquery.min',
            // 'Campaign.bootstrap.min',
            // 'Campaign.js.cookie.min',
            // 'Campaign.bootstrap-hover-dropdown.min',
            // 'Campaign.jquery.slimscroll.min',
            // 'Campaign.jquery.blockui.min',
            // 'Campaign.jquery.uniform.min',
            // 'Campaign.bootstrap-switch.min',
            'Campaign.jquery.validate.min',
            // 'Campaign.additional-methods.min',
            // 'Campaign.select2.full.min',
            'Campaign.jquery.backstretch.min',
            // 'Campaign.app.min',
            'Campaign.login-5'
            ));
        ?>
    </body>

</html>
<script>
    function login(){
        var userName = $("#textUserName").val();
        var password = $("#textPassword").val(); 
        $.ajax({ 
            url: "<?php echo BASE_URL; ?>campaign/CampaignHome/login",
            type: 'POST',
            async: false,
            data: { userName: userName, password: password }, 
            success: function( result ){
                if( result == 'success' ){ 
                    location.href = "<?php echo BASE_URL; ?>campaign/CampaignHome/index";
                }else{  
                    alert(result);
                    $("#textUserName").css('border-bottom-color','red');
                    $("#textPassword").css('border-bottom-color','red');
                    $('#loginMsg').html('* '+result+'.' );
                }  
            },
            error: function( result ){
                $('#loginMsg').html( result );

            }
        });
    }
    function forgotPwd(){
        var email = $('#textEmail').val();
        if(email==""){
            alert("Please Enter Your Email");
            $('#textEmail').focus();
            $("#textEmail").css('border-color','red');
            window.stop();
            die();
            return false;
        }
            else{
        $.ajax({ 
            url: "<?php echo BASE_URL; ?>campaign/CampaignHome/forgotPwd",
            type: 'POST',
            async: false,
            data: { email: email}, 
            success: function( result ){
                if( result == 'success' ){ 
                    alert(" Your new password sent successfully on your registered mail id. ");
                    location.reload(true);
                }else{ 
                    alert(result);
                    $("#textEmail").css('border-color','red');
                    
                }  
            },
            error: function( result ){
                

            }
        });
    }
    }
</script>