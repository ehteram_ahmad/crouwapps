<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
    }
    .modal-dialog{
        width:52%;
    }
    .labelRight10 label {
    margin-right: 25px;
}
.labelRight10 label input {
    margin-right: 5px;
}
div.radio input{
    opacity: 1!important;
    margin-top: -6px;
}
div.checker input{
    opacity: 1!important;
}

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <!-- <h1>User Search</h1> -->
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <!-- <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Users</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">User List</span>
        </li>
    </ul> -->
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box blue2">
                <div class="portlet-title">
                    <div class="caption">Email Template List </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <div class="clearfix"></div>
                    <table class="table table-bordered table-striped table-condensed flip-content tableButtonNew">
                        <thead class="flip-content">
                            <tr>
                                <th>&nbsp;  </th>
                                <th> Name </th>
                                <th> Subject </th>
                                <th> From Email </th>
                                <th> From Name </th>
                                <th> Created Date </th>
                                <!-- <th> Labels </th> -->
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                        for ($i=0; $i < count($list) ; $i++) { 
                            // pr($value); 
                            // exit();
                            ?>
                        
                            <tr>
                                <td> <?php echo $i+1;?> </td>
                                <td id="title<?php echo $i+1;?>"> <?php echo $list[$i]['name'];?> </td>
                                <td> <?php echo $list[$i]['subject'];?> </td>
                                <td> <?php echo $list[$i]['from_email'];?> </td>
                                <td> <?php echo $list[$i]['from_name'];?> </td>
                                <td> <?php echo $list[$i]['created_at'];?> </td>
                                <td class="text-center">
                                    <a class="btn btn-circle btn-icon-only btn-info2 tooltips " data-original-title="View" id="<?php echo $i+1;?>" onclick="showContent(<?php echo $i+1;?>);"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                           <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>
<div id="viewMail" class="modal fade" tabindex="-1" data-replace="true">
<div id="cn-overlay" class="cn-overlay"></div>
<!-- END CONTENT -->
<script type="text/javascript">
function showContent(val){
    var title=$('#title'+val).html();
$(".loader").fadeIn("fast");
    $.ajax({
             url: '<?php echo BASE_URL; ?>campaign/CampaignHome/previewMail',
             type: "POST",
             data: {'content':title},
             success: function(data) { 
                $(".loader").fadeOut("fast");
                $('#viewMail').html(data);
                $("#viewMail").modal('show');
            }
        });
}
    $('.circular-menu .circle').click(function() {
        $(this).parent('.circular-menu').removeClass('open');
        $('#cn-overlay').removeClass('on-overlay');
    });
    $("#menu>ul>li").eq(1).addClass('active open');
</script>