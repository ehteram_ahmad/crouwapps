<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
    }
    .modal-dialog{
        width:52%;
    }
    .labelRight10 label {
    margin-right: 25px;
}
.labelRight10 label input {
    margin-right: 5px;
}
div.radio input{
    opacity: 1!important;
    margin-top: -3px;
}
div.checker input{
    opacity: 1!important;
}

</style>
<div id="content" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1><!-- Add Beat --></h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/CampaignHome/index';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <!-- <li>
                <a href="<?php //echo BASE_URL . 'admin/UserPost/userPostLists';?>">All Posts</a>
                <i class="fa fa-circle"></i>
            </li> -->
            
            <li>
                <span class="active">Send Mail</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            Send Mail </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('mail',array('class'=>'form-horizontal','type'=>'file','onsubmit'=>'return send();')); ?>
                        <!-- <form class="form-horizontal" role="form" onsubmit="return searchBeat();" id="formSearch"> -->
                        <div class="form-body width100Auto">
                            <div class="form-group">
                                <div class="col-md-12 labelRight10">
                                <?php echo $this->Form->label('Users : '); ?>
                                    <label><input class="users" type="radio" value="All" name="user" checked="checked"> All</label>
                                    <label><input class="users" type="radio" value="Approved" name="user"> Approved</label>
                                    <label><input class="users" type="radio" value="Pending" name="user"> Pending</label>
                                    <label><input class="users" type="radio" value="Custom" name="user"> Custom</label>
                                </div>
                                <div class="col-md-6 labelRight10">
                                <?php echo $this->Form->label('Mail From : '); ?>
                                    <label><input class="from" type="radio" value="MB" name="from"> MB</label>
                                    <label><input class="from" type="radio" value="OCR" name="from" checked="checked"> OCR</label>
                                </div>
                                <div class="col-md-6 labelRight10">
                                <?php echo $this->Form->label('Users Of : '); ?>
                                    <label><input class="of" type="checkbox" value="MB" name="of[Mb]" checked="checked"> MB</label>
                                    <label><input class="of" type="checkbox" value="OCR" name="of[OCR]" checked="checked"> OCR</label>
                                </div>
                            </div>
                            <div id="customForm" class="form-group hidden">
                            <div class="col-md-12">
                                <?php echo $this->Form->text('emails',array('class'=>'form-control input-sm','placeholder'=>'Enter Emails','id'=>'textEmailsOnly')); ?>
                                </div>
                            </div>
                            <div id="fullForm">
                                
                                <div class="form-group">
                                    
                                    <div class="col-md-6">
                                        <?php echo $this->Form->text('firstName',array('class'=>'form-control input-sm','placeholder'=>'First Name','id'=>'textFirstName')); ?>
                                        </div>
                                        <div class="col-md-6">
                                        <?php echo $this->Form->text('lastName',array('class'=>'form-control input-sm','placeholder'=>'Last Name','id'=>'textLastName')); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="input-group select2-bootstrap-prepend">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="multi-prepend"> Specialities </button>
                                            </span>
                                            <?php 
                                                    foreach($speciality as $interest){
                                                        $options[$interest['specilities']['id']]=$interest['specilities']['name'];
                                                    }
                                                ?>
                                            <?php echo $this->Form->select('interest',$options,array('id'=>'multi-prepend','class'=>'form-control select2 spec hidden','multiple'=>true)); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <div class="col-md-6">
                                        <?php echo $this->Form->text('from',array('class'=>'form-control datePick','id'=>'dateFrom','placeholder'=>'User Registered From')); ?>
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $this->Form->text('to',array('class'=>'form-control datePick','id'=>'dateTo','placeholder'=>'User Registered To')); ?>
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php 
                                                foreach ($country as  $value) {
                                                $countries[$value['countries']['id']]=$value['countries']['country_name'];
                                                }
                                        echo $this->Form->select('country',$countries,array('class'=>'form-control','empty'=>'Choose Country','id'=>'countrySearch')); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php 
                                                foreach ($professions as  $value) {
                                                $profession[$value['professions']['id']]=$value['professions']['profession_type'];
                                                }
                                        echo $this->Form->select('profession',$profession,array('class'=>'form-control','empty'=>'Choose Profession','id'=>'profession')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <?php 
                                    foreach ($list as $value) {
                                        $template[$value['name']]=$value['name'];
                                    }
                                    echo $this->Form->select('template',$template,array('class'=>'form-control','empty'=>'Choose Template','id'=>'template'));
                                     ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center-block txt-center">
                            <button class="btn green2">Send</button>
                            <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                            <a onclick="preview();" class="btn blue pull-right">Preview Mail</a>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<div id="viewMail" class="modal fade" tabindex="-1" data-replace="true">
</div>
<script type="text/javascript">
$('input[type="radio"].users').click(function(){
    $('input[type="radio"].users:checked').each(function() {
        if(this.value=="Custom"){
            $('#customForm').removeClass('hidden');
            $('#fullForm').addClass('hidden');
            $('#textEmailsOnly,#textFirstName,#textLastName,#dateFrom,#dateTo,#template,#countrySearch,#profession').val('');
        }
        else{
            $('#customForm').addClass('hidden');
            $('#fullForm').removeClass('hidden');
            $('#textEmailsOnly,#textFirstName,#textLastName,#dateFrom,#dateTo,#template,#countrySearch,#profession').val('');
        }
    });
});
$(document).ready(function() {
    $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' }); 
});
function preview(){
    val=$('#template').val();
    if(val ==""){
        alert("Please select the template.");
        return false;
    }
    $('input[type="radio"].from:checked').each(function() {
        type=this.value;
    });
    $(".loader").fadeIn("fast");
    $.ajax({
             url: '<?php echo BASE_URL; ?>campaign/CampaignHome/previewMail',
             type: "POST",
             data: {'content':val},
             success: function(data) { 
                $(".loader").fadeOut("fast");
                $('#viewMail').html(data);
                $("#viewMail").modal('show');
            }
        });
}
function send(){
    val=$('#template').val();
    if(val ==""){
        alert("Please select the template.");
        return false;
    }
    if($('input[type="checkbox"].of:checked').value=="undefined"){
        alert("Please Select Options for 'Users Of'.");
        return false;
    }
}
    $("#menu>ul>li").eq(0).addClass('active open');
</script>