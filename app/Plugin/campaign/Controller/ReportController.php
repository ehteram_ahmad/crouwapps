<?php
/*

Methods To write data in google spead sheet

*/
//echo APP;
App::uses('AppController', 'Controller');
App::import('Vendor','SPREADSHEET',array('file' => 'googleapi/autoload.php'));
define('APPLICATION_NAME', 'Web client 1');
define('CLIENT_SECRET_PATH', APP.'/Plugin/campaign/Controller/Component/client_secret.json');
// define('SCOPES', implode(' ', array(
//   Google_Service_Sheets::SPREADSHEETS)
// ));
define('SCOPES', implode(' ', array(
  Google_Service_Drive::DRIVE)
));

class ReportController extends CampaignAppController {
	public $uses = array('Campaign.PatientHcpAssociation','Campaign.User','Campaign.UserColleague',
		'Campaign.InstituteName','Campaign.UserProfile','Campaign.UserQbDetail','Mbapi.Profession','Mbapi.RoleTag','Mbapi.UserLoginTransaction');
	//public $components = array('Campaign.Quickblox');

	/*
	-----------------------------------------------------------------------------------------------
	On: 17-05-2017
	I/P:
	O/P:
	Desc: Store Patient Hcp Association data on google spreadsheet
	-----------------------------------------------------------------------------------------------
	*/

	public function index()
	{
		$this->loadModel('PatientHcpAssociation');
		$response = array();

		$filename='lastInsertedId.txt';
		$fh = fopen(BASE_URL .'/googlesheet/credentialData/'.$filename,'r');
		$line = fgets($fh);
		if($line == '' || $line == null)
		{
			$patientAssociationsData = $this->PatientHcpAssociation->find('all', array('conditions' => array('PatientHcpAssociation.id >' => 0)));
		}
		else
		{
			$patientAssociationsData = $this->PatientHcpAssociation->find('all', array('conditions' => array('PatientHcpAssociation.id >' => $line)));
			$lastInsertedId = $line;
		}
		fclose($fh);
		$client = $this->getClient();
		$service = new Google_Service_Sheets($client);
		$spreadsheetId = '1mMgEeKZUM8OjYMKepqVz3GfE1I6_n34zrCjSHQl1j3E';
		$range = 'Sheet1!A:G';
		if(!empty($patientAssociationsData))
		{
			foreach($patientAssociationsData AS $result) {
            $lastInsertedId = $result['PatientHcpAssociation']['id'];   
				$values = array(
				    array(
				        $result['PatientHcpAssociation']['id'],$result['PatientHcpAssociation']['user_id'],$result['PatientHcpAssociation']['participant_ids'],$result['PatientHcpAssociation']['dialogue_id'],$result['PatientHcpAssociation']['dialogue_type'],$result['PatientHcpAssociation']['action'],$result['PatientHcpAssociation']['created_date']
				    )
				);
				$body = new Google_Service_Sheets_ValueRange(array(
				  'values' => $values
				));
				$params = array(
				  'valueInputOption' => 'RAW'
				);
				$result = $service->spreadsheets_values->append($spreadsheetId, $range,
				    $body, $params);
			}
			$response['success'] = "Updated Successfuly";
		}
		else
		{
			$response['success'] = "Already Up-to-date";
		}
		
		$filename='lastInsertedId.txt';
        $textFile = fopen(BASE_URL .'/googlesheet/credentialData/'.$filename, 'w+');
        $txt = $lastInsertedId;
        fwrite($textFile, $txt);
        fclose($textFile);
        print_r($response);
        exit();
    }

	/*
	-----------------------------------------------------------------------------------------------
	On: 17-05-2017
	I/P:
	O/P:
	Desc: Method for google authorization
	-----------------------------------------------------------------------------------------------
	*/

	public function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');
        //$credentialsPath = WWW_ROOT . DS .'googlesheet/credentialData/CREDENTIALS_PATH.json';
        $credentialsPath = APP . '/webroot/googlesheet/credentialData/CREDENTIALS_PATH.json';
        print_r($credentialsPath);
        $accessToken = json_decode(file_get_contents($credentialsPath), true);

        /*if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
        }
        else
        {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        // Store the credentials to disk.
        if(!file_exists(dirname($credentialsPath))) {
        mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
        }*/
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }


	/*
	-----------------------------------------------------------------------------------------------
	On: 17-05-2017
	I/P:
	O/P:
	Desc: Method to get creadential file from directory
	-----------------------------------------------------------------------------------------------
	*/

	public function expandHomeDirectory($path) 
	{
	  $homeDirectory = getenv('HOME');
		  if (empty($homeDirectory)) {
		    $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
		  }
	  return str_replace('~', realpath($homeDirectory), $path);
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 24-05-2017
	I/P:
	O/P:
	Desc: Institute Wise On Call User count
	-----------------------------------------------------------------------------------------------
	*/

	public function institutionWiseOnCallUser()
	{
		$response = array();
		$filename='institutionWiseOnCallUser.txt';
		$fh = fopen(BASE_URL .'/googlesheet/credentialData/'.$filename,'r');
		$line = fgets($fh);
		if($line == '' || $line == null)
		{
			$textKey = 0;
			$institutionalOnCallData = $this->InstituteName->instituteWiseOnCallUser($textKey);
		}
		else
		{
			$textKey = $line;
			$institutionalOnCallData = $this->InstituteName->instituteWiseOnCallUser($textKey);
			$lastInsertedId = $line;
		}
		//$colleagueInfo = $this->UserColleague->colleagueCount();
		//echo "<pre>"; print_r($institutionalOnCallData);die;
		fclose($fh);
		$client = $this->getClient();
		$service = new Google_Service_Sheets($client);
		$spreadsheetId = '1Jqb1hGA-XdhSbBS_8dUF34hXlIWLBLL6Cdll6YAiUZs';
		$range = 'Sheet1!A:B';
		if(!empty($institutionalOnCallData))
		{
			foreach($institutionalOnCallData AS $result) {
            	$lastInsertedId = $result['InstituteName']['id'];
            	$updatedDate = date('d-m-Y H:i');
				$values = array(
				    array(
				        $result['InstituteName']['institute_name'],$result[0]['onCallStatus'],$updatedDate
				    )
				);
				$body = new Google_Service_Sheets_ValueRange(array(
				  'values' => $values
				));
				$params = array(
				  'valueInputOption' => 'RAW'
				);
				$result = $service->spreadsheets_values->append($spreadsheetId, $range,
				    $body, $params);
			}
			$response['success'] = "Updated Successfuly";
		}
		else
		{
			$response['success'] = "Already Up-to-date";
		}
		
		$filename='institutionWiseOnCallUser.txt';
        $textFile = fopen(BASE_URL.'/googlesheet/credentialData/'.$filename, 'w+');
        $txt = $lastInsertedId;
		fwrite($textFile, $txt);
		fclose($textFile);
		print_r($response);
		exit();
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 22-06-2017
	I/P:
	O/P:
	Desc: Upload Csv To Google Drive for patient dialogue
	-----------------------------------------------------------------------------------------------
	*/

	public function petientDialogueCheck()
	{
		//echo "HI";exit();
		$response = array();
		$dateofSheetUpdation = date('d-m-Y H:i');
		// $patientAssociationsData = $this->PatientHcpAssociation->find('all', array('conditions' => array('PatientHcpAssociation.id >' => 0,"PatientHcpAssociation.dialogue_type"=>"patient","PatientHcpAssociation.dialogue_name IS NOT NULL","PatientHcpAssociation.action"=>"create"),'order'=>array('PatientHcpAssociation.id ASC')));
		// echo "<pre>";print_r($patientAssociationsData);exit();
		$patientAssociationsData = $this->PatientHcpAssociation->query("SELECT * FROM patient_hcp_associations where id >0 AND dialogue_name IS NOT NULL AND dialogue_type='patient' AND id in  (SELECT MAX(id) FROM patient_hcp_associations GROUP BY dialogue_id)  ");
		//echo "<pre>";print_r($patientAssociationsData);exit();
		if(!empty($patientAssociationsData))
		{
			$filename='patientDialogue.csv';
			$csv_file = fopen(APP .'/webroot/googlesheet/credentialData/'.$filename, 'w+');
 		
            $header_row = array("dialogue_id", "Patient's Name", "created_date","user  id","user name","Trust","proffesion");
            fputcsv($csv_file, $header_row);
			foreach($patientAssociationsData AS $result)
			{
				//echo "<pre>";print_r($result);exit();
				$dialogueId = !empty($result['patient_hcp_associations']['dialogue_id']) ? $result['patient_hcp_associations']['dialogue_id'] :'' ;
				$dialogueName = !empty($result['patient_hcp_associations']['dialogue_name']) ? $result['patient_hcp_associations']['dialogue_name']:'';
            	$participantIdsArr = array();
	            $participantIdsArr = explode(',', $result['patient_hcp_associations']['participant_ids']);
	            $uniqueParticipantIds = array_unique($participantIdsArr);
	            for($i=0;$i<count($uniqueParticipantIds);$i++)
	            {
	            	$userId= 0;
	            	$userDetails = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=>$uniqueParticipantIds[$i])));
	            	if(!empty($userDetails))
            		{ 
            			$userId= $userDetails['UserQbDetail']['user_id']; 
            		}
            		$userInfo = $this->UserProfile->getUserInfo($userId);
            		$UserName = !empty($userInfo[0][0]['UserName']) ? $userInfo[0][0]['UserName'] :'NA' ;
            		$companyName = !empty($userInfo[0]['companyNames']['company_name']) ? $userInfo[0]['companyNames']['company_name'] :'NA' ;
            		$professionType = !empty($userInfo[0]['userProfessions']['profession_type']) ? $userInfo[0]['userProfessions']['profession_type'] :'NA' ;
            		$rows = array(
		                $dialogueId,
		                $dialogueName,
		                $dateofSheetUpdation,
		                $userId,
		                $UserName,
		                $companyName,
		                $professionType
	                );
	                fputcsv($csv_file,$rows,',','"');
	            }
			}
			fclose($csv_file);
			// $client = $this->getClient();
			// $service = new Google_Service_Drive($client);
			// $fileMetadata = new Google_Service_Drive_DriveFile(array(
			//   'name' => 'Sheet1',
			//   'mimeType' => 'application/vnd.google-apps.spreadsheet'));
			// $content = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/app/webroot/googlesheet/credentialData/patientDialogue.csv');
			// $file = $service->files->create($fileMetadata, array(
			//   'data' => $content,
			//   'mimeType' => 'text/csv',
			//   'uploadType' => 'multipart',
			//   'fields' => 'id'));
			// printf("File ID: %s\n", $file->id);

			$client = $this->getClient();
			$driveService = new Google_Service_Drive($client);
			$fileId = "1JIfJxvb9n8dw56aXFOOwZoPdSUHSCieTyLBbChujxkA";

			    // File's new metadata.
			//$newTitle = "My New Report";
			$fileMetadata = new Google_Service_Drive_DriveFile(array(
			  'name' => 'Patient Dialogue Server Data',
			  'mimeType' => 'application/vnd.google-apps.spreadsheet'));

			    // File's new content.
			$data = file_get_contents(APP .'/webroot/googlesheet/credentialData/patientDialogue.csv');

			$additionalParams = array(
			    'data' => $data,
			    'mimeType' => 'text/csv'
			);

			// Send the request to the API.
			$updatedFile = $driveService->files->update($fileId, $fileMetadata, $additionalParams);
			$range = 'Patient Dialogue Server Data!A:G';
			$sheetServices = new Google_Service_Sheets($client);
			$values = array(
			            array(
			                '','','','','','',''
			            )
			        );
			$body = new Google_Service_Sheets_ValueRange(array(
			  'values' => $values
			));
			$params = array(
			  'valueInputOption' => 'RAW'
			);
			$result = $sheetServices->spreadsheets_values->append($fileId, $range,
			    $body, $params);
			$response['success'] = "Updated Successfuly And File Id".$file->id;
		}
		else
		{
			$response['error'] = "No data found";
		}
		print_r($response);exit();
		exit();
	}

	public function userinformation()
	{
		ini_set('max_execution_time', 4*300);
    	ini_set('max_input_time', 4*300);
		echo BASE_URL;
		//echo "HI";exit();
		$response = array();
		$dateofSheetUpdation = date('d-m-Y H:i');
		$userLoginStatus = 0;


		// $conditions = "User.id > 0 AND User.status != 2";
		$type = array(0,1);
		$conditions = array(
				'UserEmployment.is_current' => 1,
				'UserEmployment.status' => 1,
				'User.status' => 1,
				'User.approved' => 1,
				'EnterpriseUserList.status' => 1,
			);
		$userInformation = $this->User->find('all', 
						array(
							'joins'=>
							array(
								array(
									'table' => 'user_employments',
									'alias' => 'UserEmployment',
									'type' => 'left',
									'conditions'=> array('UserEmployment.user_id = User.id')
								),
								array(
									'table' => 'user_profiles',
									'alias' => 'userProfiles',
									'type' => 'left',
									'conditions'=> array('User.id = userProfiles.user_id')
								),
								array(
									'table'=> 'professions',
									'alias' => 'userProfessions',
									'type' => 'left',
									'conditions'=> array('userProfiles.profession_id = userProfessions.id')
								),
								array(
									'table'=> 'user_duty_logs',
									'alias' => 'userDutyLogs',
									'type' => 'left',
									'conditions'=> array('User.id = userDutyLogs.user_id')
								),
								array(
									'table' => 'user_subscription_logs',
									'alias' => 'UserSubscriptionLog',
									'type' => 'left',
									'conditions'=> array('User.id = UserSubscriptionLog.user_id')
								),
								array(
									'table'=> 'company_names',
									'alias' => 'companyNames',
									'type' => 'left',
									'conditions'=> array('userDutyLogs.hospital_id = companyNames.id')
								),
								array(
									'table' => 'enterprise_user_lists',
									'alias' => 'EnterpriseUserList',
									'type' => 'left',
									'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
								),
								
							),
							'conditions' => $conditions,
							'fields' => array('User.id', 'User.email', "CONCAT(userProfiles.first_name, ' ' , userProfiles.last_name) AS UserName", 'userDutyLogs.status AS onCall', 'companyNames.company_name', 'userProfessions.profession_type','userProfessions.id'),
							'group'=> array('User.id'),
							'order'=> 'User.id ASC'
						)
					);

		// echo "<pre>";print_r($userInformation);exit();



		// $userInformation = $this->User->userStatusDetailByLimit();
    	if(!empty($userInformation))
		{
			$filename='userInformation.csv';
			$csv_file = fopen(APP .'/webroot/googlesheet/credentialData/'.$filename, 'w+');
 		

            $header_row = array("date of sheet updation", "user  id", "user name","OnCall (1/0)","Trust Name","Signed In","proffesion", "Speciality","Grade");
            fputcsv($csv_file, $header_row);
			foreach($userInformation AS $result)
			{
				$userLoginStatus = 0;
				$signedInStatusOnWeb = $this->UserLoginTransaction->find("first", array("conditions"=> array("user_id"=> $result['User']['id'], "application_type"=> "MB-WEB"), "order"=> array("id DESC")));

				$signedInStatusAndroid = $this->UserLoginTransaction->find("first", array("conditions"=> array("user_id"=> $result['User']['id'], "application_type"=> "MB"), "order"=> array("id DESC")));
				// echo "<pre>";print_r($signedInStatusOnWeb);
				// echo "<pre>";print_r($signedInStatusAndroid);exit();


				if($signedInStatusOnWeb['UserLoginTransaction']['login_status'] == 1 || $signedInStatusAndroid['UserLoginTransaction']['login_status'] == 1)
				{
					$userLoginStatus = 1;
				}
				else
				{
					$userLoginStatus = 0;
				}
				// echo $userLoginStatus;
				// exit();
				$roleTagsArr = array(); $roleTagsVals = array();
				$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $result['userProfessions']['id'])));
				if(!empty($roleTags['Profession']['tags'])){
					$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
				}
				if(!empty($roleTagsVals)){
					foreach($roleTagsVals as $rt){
						$values = array(); $roleTagId = 0;
						$params['user_id'] = $result['User']['id'];
						$params['key'] = $rt;
						$roleTagUser = $this->RoleTag->userRoleTags($params);
						if(!empty($roleTagUser)){
							$roleTagId = $roleTagUser[0]['rt']['id'];
							$values = array($roleTagUser[0]['rt']['value']);
						}
						$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
					}
				}
				// echo "<pre>";print_r($roleTagsArr);exit();


				$userSpeciality = !empty($roleTagsArr[0]['value'][0]) ? $roleTagsArr[0]['value'][0] :'' ;
				$userRoleTag = !empty($roleTagsArr[1]['value'][0]) ? $roleTagsArr[1]['value'][0] :'' ;
				$userId = !empty($result['User']['id']) ? $result['User']['id'] :'' ;
				$userName = !empty($result[0]['UserName']) ? $result[0]['UserName']:'';
        		$onCallStatus = !empty($result['userDutyLogs']['onCall']) ? 1 :0 ;
        		$trustName = !empty($result['companyNames']['company_name']) ? $result['companyNames']['company_name'] :'NA' ;
        		// $signedIn = !empty($result['userDevices']['signedIn']) ? 1 :0 ;
        		$signedIn =$userLoginStatus;
        		$proffesion = !empty($result['userProfessions']['profession_type']) ? $result['userProfessions']['profession_type'] :'NA' ;
            		$rows = array(
		                $dateofSheetUpdation,
		                $userId,
		                $userName,
		                $onCallStatus,
		                $trustName,
		                $signedIn,
		                $proffesion,
		                $userSpeciality,
		                $userRoleTag
	                );
	                fputcsv($csv_file,$rows,',','"');
			}
			fclose($csv_file);
			// $client = $this->getClient();
			// $service = new Google_Service_Drive($client);
			// $fileMetadata = new Google_Service_Drive_DriveFile(array(
			//   'name' => 'Sheet1',
			//   'mimeType' => 'application/vnd.google-apps.spreadsheet'));
			// $content = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/app/webroot/googlesheet/credentialData/patientDialogue.csv');
			// $file = $service->files->create($fileMetadata, array(
			//   'data' => $content,
			//   'mimeType' => 'text/csv',
			//   'uploadType' => 'multipart',
			//   'fields' => 'id'));
			// printf("File ID: %s\n", $file->id);

			$client = $this->getClient();
			$driveService = new Google_Service_Drive($client);
			$fileId = "1Iqrvn9DS0DyYm6KRpn3MeSIVSF04TSTs0Z8D4eJnH3g";

			    // File's new metadata.
			//$newTitle = "My New Report";
			$fileMetadata = new Google_Service_Drive_DriveFile(array(
			  'name' => 'User Information from Server',
			  'mimeType' => 'application/vnd.google-apps.spreadsheet'));

			    // File's new content.
			$data = file_get_contents(APP .'/webroot/googlesheet/credentialData/userInformation.csv');

			$additionalParams = array(
			    'data' => $data,
			    'mimeType' => 'text/csv'
			);

			// Send the request to the API.
			$updatedFile = $driveService->files->update($fileId, $fileMetadata, $additionalParams);
			$range = 'User Information from Server!A:I';
			$sheetServices = new Google_Service_Sheets($client);
			$values = array(
			            array(
			                '','','','','','','','',''
			            )
			        );
			$body = new Google_Service_Sheets_ValueRange(array(
			  'values' => $values
			));
			$params = array(
			  'valueInputOption' => 'RAW'
			);
			$result = $sheetServices->spreadsheets_values->append($fileId, $range,
			    $body, $params);
			$response['success'] = "Updated Successfuly And File Id";
		}
		else
		{
			$response['error'] = "No data found";
		}
		print_r($response);exit();
	}

	public function justCheck()
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		$dbhost = '35.177.136.62:3306';
		$dbuser = 'mb_analitics';
		$dbpass = 'Team@Medic@';
		$database = 'mb_analytics';
		$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
		if(! $conn )
		{
		die('Could not connect to instance: ' . mysqli_error($conn));
		}

		// echo "Connected to MySQL Successfully! <br />";
		// $sql = 'SELECT * FROM user_colleagues_transactions';

		$db = mysqli_select_db($conn,"mb_analytics");
		if(! $db )
		{
		die('Could not Select Database: ' . mysqli_error($conn));
		}
		// $client = $this->getClient();
		// $driveService = new Google_Service_Drive($client);
		// $fileId = "1Iqrvn9DS0DyYm6KRpn3MeSIVSF04TSTs0Z8D4eJnH3g";

		//     // File's new metadata.
		// //$newTitle = "My New Report";
		// $fileMetadata = new Google_Service_Drive_DriveFile(array(
		//   'name' => 'User Information from Server',
		//   'mimeType' => 'application/vnd.google-apps.spreadsheet'));
		// $range = 'User Information from Server!A:I';
		// $sheetServices = new Google_Service_Sheets($client);

		// $result = $sheetServices->spreadsheets_values->get($fileId, $range, ['majorDimension' => 'ROWS']);
		// $row = 1;
		// foreach($result['values'] AS $sheetData){
		// 	$row++;
		// 	if($row == 2) continue;
		// 	// echo "<pre>";print_r($sheetData[8]);exit();
		// 	$date_of_sheet_updattion = $sheetData[0];
		// 	$user_id = $sheetData[1];
		// 	$user_name = $sheetData[2];
		// 	$onCall = $sheetData[3];
		// 	$trust_name = $sheetData[4];
		// 	$signed_id = $sheetData[5];
		// 	$profession = $sheetData[6];
			// foreach ($sheetData AS $shData) {
				// echo "<pre>";print_r($shData);exit();
			// $insertData = mysqli_query($conn,"INSERT INTO user_informastions (date_of_sheet_updation,user_id,user_name, on_call, trust_name,signed_in,proffesion,speciality,grade) 
			// VALUES ('".$date_of_sheet_updattion."','".$user_id."','".$user_name."','".$onCall."','".$trust_name."', '".$signed_id."' , '".$profession."')");

			$insertData = mysqli_query($conn,"INSERT INTO test_info (sender_id,sender_name) 
			VALUES ('22','ehteram')");
			// }
		echo "<pre>";print_r($result);exit();
	}

	public function readUserDataFromGoogleSheet()
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		$dbhost = '35.177.136.62:3306';
		$dbuser = 'mb_analitics';
		$dbpass = 'Team@Medic@';
		$database = 'mb_analytics';
		$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
		if(! $conn )
		{
		die('Could not connect to instance: ' . mysqli_error($conn));
		}

		// echo "Connected to MySQL Successfully! <br />";
		// $sql = 'SELECT * FROM user_colleagues_transactions';

		$db = mysqli_select_db($conn,"mb_analytics");
		if(! $db )
		{
		die('Could not Select Database: ' . mysqli_error($conn));
		}


		$truncateBeforeUpdate = mysqli_query($conn,'TRUNCATE TABLE user_informastions');
		// echo "<pre>";print_r("Done");exit();
		$client = $this->getClient();
		$driveService = new Google_Service_Drive($client);
		$fileId = "1Iqrvn9DS0DyYm6KRpn3MeSIVSF04TSTs0Z8D4eJnH3g";

		    // File's new metadata.
		//$newTitle = "My New Report";
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
		  'name' => 'User Information from Server',
		  'mimeType' => 'application/vnd.google-apps.spreadsheet'));
		$range = 'User Information from Server!A:I';
		$sheetServices = new Google_Service_Sheets($client);

		$result = $sheetServices->spreadsheets_values->get($fileId, $range, ['majorDimension' => 'ROWS']);
		// array_shift($result['values']);
		$row = 1;
		foreach($result['values'] AS $sheetData){
			$row++;
			if($row == 2) continue;
			// echo "<pre>";print_r($sheetData[8]);exit();
			$date_of_sheet_updattion = $sheetData[0];
			$user_id = $sheetData[1];
			$user_name = $sheetData[2];
			$onCall = $sheetData[3];
			$trust_name = $sheetData[4];
			$signed_id = $sheetData[5];
			$profession = $sheetData[6];
			$baselocation = $sheetData[7];
			$speciality = $sheetData[8];
			$grade = $sheetData[9];
			$userBand = $sheetData[10];
			// foreach ($sheetData AS $shData) {
				// echo "<pre>";print_r($shData);exit();
			$insertData = mysqli_query($conn,"INSERT INTO user_informastions (user_id,user_name,on_call, trust_name,signed_in,proffesion,base_location,speciality,grade,band) 
			VALUES ('".$user_id."','".$user_name."','".$onCall."','".$trust_name."', '".$signed_id."' , '".$profession."', '".$baselocation."', '".$speciality."', '".$grade."', '".$userBand."')");

			// $insertData = mysqli_query($conn,"INSERT INTO user_informastions (date_of_sheet_updation,user_id,user_name, on_call, trust_name,signed_in,proffesion) 
			// VALUES ('2018-10-12','22','ehteram','1','Medic', '1' , 'Doc')");
			}
		
			echo "<pre>";print_r($result);exit();
	}

	public function readPatientDataFromGoogleSheet()
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		$dbhost = '35.177.136.62:3306';
		$dbuser = 'mb_analitics';
		$dbpass = 'Team@Medic@';
		$database = 'mb_analytics';
		$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
		if(! $conn )
		{
		die('Could not connect to instance: ' . mysqli_error($conn));
		}
		// echo "Connected to MySQL Successfully! <br />";
		// $sql = 'SELECT * FROM user_colleagues_transactions';

		$db = mysqli_select_db($conn,"mb_analytics");
		if(! $db )
		{
		die('Could not Select Database: ' . mysqli_error($conn));
		}

		$truncateBeforeUpdate = mysqli_query($conn,'TRUNCATE TABLE patient_group_infos');
		$client = $this->getClient();
		$driveService = new Google_Service_Drive($client);
		$fileId = "1JIfJxvb9n8dw56aXFOOwZoPdSUHSCieTyLBbChujxkA";

		    // File's new metadata.
		//$newTitle = "My New Report";
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
		  'name' => 'Patient Dialogue Server Data',
		  'mimeType' => 'application/vnd.google-apps.spreadsheet'));
		$range = 'Patient Dialogue Server Data!A:G';
		$sheetServices = new Google_Service_Sheets($client);

		$result = $sheetServices->spreadsheets_values->get($fileId, $range, ['majorDimension' => 'ROWS']);
		// array_shift($result['values']);
		$row = 1;
		foreach($result['values'] AS $sheetData){
			$row++;
			if($row == 2) continue;
			// echo "<pre>";print_r($sheetData[8]);exit();
			$dialogue_id = $sheetData[0];
			$patient_name = $sheetData[1];
			$created_date = $sheetData[2];
			$user_id = $sheetData[3];
			$user_name = $sheetData[4];
			$trust_name = $sheetData[5];
			$profession = $sheetData[6];
			// foreach ($sheetData AS $shData) {
				// echo "<pre>";print_r($shData);exit();
			$insertData = mysqli_query($conn,"INSERT INTO patient_group_infos (dialogue_id,patients_name,user_id, user_name,proffesion,trust) 
			VALUES ('".$dialogue_id."','".$patient_name."','".$user_id."','".$user_name."', '".$profession."' ,'".$trust_name."')");

			}
			exit();
			echo "<pre>";print_r("Done");exit();

		exit();
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 22-01-2019
	I/P:
	O/P:
	Desc: Upload Csv To Google Drive User Informations
	-----------------------------------------------------------------------------------------------
	*/

	public function allPetientDialogueData()
	{
		//echo "HI";exit();
		$response = array();
		$dateofSheetUpdation = date('d-m-Y H:i');
		$counter1 = 0;
		$counter2 = 0;
		$patientAssociationsData = $this->PatientHcpAssociation->query("SELECT * FROM patient_hcp_associations where id >0 AND dialogue_name IS NOT NULL AND dialogue_type !='private' AND id in  (SELECT MAX(id) FROM patient_hcp_associations GROUP BY dialogue_id)  ");

		// echo "<pre>";print_r($patientAssociationsData);exit();
		if(!empty($patientAssociationsData))
		{
			$filename='allPatientDialogueData.csv';
			$csv_file = fopen(APP .'/webroot/googlesheet/credentialData/'.$filename, 'w+');
 		
            $header_row = array("dialogue_id", "Patient's Name", "created_date","user  id","user name","Trust","proffesion", "group type");
            fputcsv($csv_file, $header_row);
			foreach($patientAssociationsData AS $result)
			{
				//echo "<pre>";print_r($result);exit();
				$counter++;
				$dialogueId = !empty($result['patient_hcp_associations']['dialogue_id']) ? $result['patient_hcp_associations']['dialogue_id'] :'' ;
				$dialogueType = !empty($result['patient_hcp_associations']['dialogue_type']) ? $result['patient_hcp_associations']['dialogue_type'] :'' ;
				$dialogueName = !empty($result['patient_hcp_associations']['dialogue_name']) ? $result['patient_hcp_associations']['dialogue_name']:'';
            	$participantIdsArr = array();
	            $participantIdsArr = explode(',', $result['patient_hcp_associations']['participant_ids']);
	            $uniqueParticipantIds = array_unique($participantIdsArr);
	            for($i=0;$i<count($uniqueParticipantIds);$i++)
	            {
	            	$userId= 0;
	            	$userDetails = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=>$uniqueParticipantIds[$i])));
	            	if(!empty($userDetails))
            		{ 
            			$userId= $userDetails['UserQbDetail']['user_id']; 
            		}
            		$userInfo = $this->UserProfile->getUserInfo($userId);
            		$UserName = !empty($userInfo[0][0]['UserName']) ? $userInfo[0][0]['UserName'] :'NA' ;
            		$companyName = !empty($userInfo[0]['companyNames']['company_name']) ? $userInfo[0]['companyNames']['company_name'] :'NA' ;
            		$professionType = !empty($userInfo[0]['userProfessions']['profession_type']) ? $userInfo[0]['userProfessions']['profession_type'] :'NA' ;
            		$rows = array(
		                $dialogueId,
		                $dialogueName,
		                $dateofSheetUpdation,
		                $userId,
		                $UserName,
		                $companyName,
		                $professionType,
		                $dialogueType
	                );
	                fputcsv($csv_file,$rows,',','"');
		            if($counter1 == 1000)
		            {
		            	sleep(60);
		            }
	            }
	            if($counter2 == 1000)
	            {
	            	sleep(60);
	            }
			}
			fclose($csv_file);
			$client = $this->getClient();
			$driveService = new Google_Service_Drive($client);
			$fileId = "1Zl6QANnapkwMCqCLb4V7vgY8Pzyi6QoMfhwEAGRaH_8";

			    // File's new metadata.
			//$newTitle = "My New Report";
			$fileMetadata = new Google_Service_Drive_DriveFile(array(
			  'name' => 'Patient Dialogue All data',
			  'mimeType' => 'application/vnd.google-apps.spreadsheet'));

			    // File's new content.
			$data = file_get_contents(APP .'/webroot/googlesheet/credentialData/allPatientDialogueData.csv');

			$additionalParams = array(
			    'data' => $data,
			    'mimeType' => 'text/csv'
			);

			// Send the request to the API.
			$updatedFile = $driveService->files->update($fileId, $fileMetadata, $additionalParams);
			$range = 'Patient Dialogue Server Data!A:H';
			$sheetServices = new Google_Service_Sheets($client);
			$values = array(
			            array(
			                '','','','','','','',''
			            )
			        );
			$body = new Google_Service_Sheets_ValueRange(array(
			  'values' => $values
			));
			$params = array(
			  'valueInputOption' => 'RAW'
			);
			$result = $sheetServices->spreadsheets_values->append($fileId, $range,
			    $body, $params);
			$response['success'] = "Updated Successfuly And File Id".$file->id;
		}
		else
		{
			$response['error'] = "No data found";
		}
		print_r($response);exit();
		exit();
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 22-01-2019
	I/P:
	O/P:
	Desc: Upload Data to analytics DataBase
	-----------------------------------------------------------------------------------------------
	*/

	public function readAllPatientDataFromGoogleSheet()
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		$dbhost = '35.177.136.62:3306';
		$dbuser = 'mb_analitics';
		$dbpass = 'Team@Medic@';
		$database = 'mb_analytics';
		$counter = 0;
		$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
		if(! $conn )
		{
		die('Could not connect to instance: ' . mysqli_error($conn));
		}
		// echo "Connected to MySQL Successfully! <br />";
		// $sql = 'SELECT * FROM user_colleagues_transactions';

		$db = mysqli_select_db($conn,"mb_analytics");
		if(! $db )
		{
		die('Could not Select Database: ' . mysqli_error($conn));
		}

		$truncateBeforeUpdate = mysqli_query($conn,'TRUNCATE TABLE patient_group_all_infos');
		$client = $this->getClient();
		$driveService = new Google_Service_Drive($client);
		$fileId = "1Zl6QANnapkwMCqCLb4V7vgY8Pzyi6QoMfhwEAGRaH_8";

		    // File's new metadata.
		//$newTitle = "My New Report";
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
		  'name' => 'Patient Dialogue All data',
		  'mimeType' => 'application/vnd.google-apps.spreadsheet'));
		$range = 'Patient Dialogue All data!A:H';
		$sheetServices = new Google_Service_Sheets($client);

		$result = $sheetServices->spreadsheets_values->get($fileId, $range, ['majorDimension' => 'ROWS']);
		// array_shift($result['values']);
		$row = 1;
		// echo "<pre>";print_r($result['values']);exit();
		foreach($result['values'] AS $sheetData){
			$counter++;
			$row++;
			if($row == 2) continue;
			// echo "<pre>";print_r($sheetData[8]);exit();
			$dialogue_id = $sheetData[0];
			$patient_name = $sheetData[1];
			$created_date = $sheetData[2];
			$user_id = $sheetData[3];
			$user_name = $sheetData[4];
			$trust_name = $sheetData[5];
			$profession = $sheetData[6];
			$group_type = $sheetData[7];
			// foreach ($sheetData AS $shData) {
				// echo "<pre>";print_r($shData);exit();
			$insertData = mysqli_query($conn,"INSERT INTO patient_group_all_infos (dialogue_id,patients_name,user_id, user_name,proffesion,trust,group_type) 
			VALUES ('".$dialogue_id."','".$patient_name."','".$user_id."','".$user_name."', '".$profession."' ,'".$trust_name."','".$group_type."')");
			if($counter == 500)
			{
				sleep(20);
			}
			}
			exit();
			echo "<pre>";print_r("Done");exit();

		exit();
	}

	/*
    -----------------------------------------------------------------------------------------------
    On: 13-03-2019
    I/P:
    O/P:
    Desc: fetch participant relation based on patient group
    -----------------------------------------------------------------------------------------------
     */

    public function fetchPtGroupPrtsSubset()
    {
        $forEachCounter = 0;
        $aGroupList = array();
        $aMemberList = array();
        $nLength = 0;
        $aResult = array();
        $nCount = 0;
        $aRows = array();
        $aPatientAssociationsData = $this->PatientHcpAssociation->query("SELECT * FROM patient_hcp_associations where id >0 AND dialogue_name IS NOT NULL AND dialogue_type='patient' AND id in  (SELECT MAX(id) FROM patient_hcp_associations GROUP BY dialogue_id)  ");
        if (!empty($aPatientAssociationsData)) {
            foreach ($aPatientAssociationsData as $aPartRecord) {
                $aData = array();
                $aMemberList = explode(',', $aPartRecord['patient_hcp_associations']['participant_ids']);
                $sDialogName = $aPartRecord['patient_hcp_associations']['dialogue_name'];
                if (!empty($aMemberList)) {
                    if (!empty($aTotalMembers)) {
                        $aTotalMembers = array_merge($aTotalMembers, $aMemberList);
                    } else {
                        $aTotalMembers = $aMemberList;
                    }
                    $nLength = count($aMemberList);
                    $aResult[$nCount]["value"] = $this->getParticipantRelation($aMemberList, $nLength, 2, 0, $aData, 0, $aData);
                    $aResult[$nCount]["name"] = $sDialogName;
                    $nCount++;
                    $this->memberList = $aData;
                }
            }
        }

        $aTotalMembers = implode(",", array_unique($aTotalMembers));
        $aTotalMembers = "'" . $aTotalMembers . "'";
        $aUsersData = $this->getUserDetails($aTotalMembers);
        $aUserDtsData = array();
        for ($i = 0; $i < count($aUsersData); $i++) {
            $vKey = $aUsersData[$i]["UserQbDetail"]["qb_id"];
            $aUserDtsData[$vKey] = $aUsersData[$i];
        }
        if (!empty($aResult)) {
            $vFilename = 'patientGroupDialogue.csv';
            $oCsvFile = fopen(APP . '/webroot/googlesheet/credentialData/' . $vFilename, 'w+');

            $aHeaderRow = array("dialogue_name", "user1", "user2", "user1 name", "user2 name", "user1 trustname", "user2 trustname", "user1 profession", "user2 profession", "user1 baselocation", "user2 baselocation", "user1 speciality", "user2 speciality", "user1 grade", "user2 grade");
            //$aHeaderRow = array("dialogue_name", "user1", "user2", "user1 name", "user2 name", "user1 trustname", "user2 trustname");
            fputcsv($oCsvFile, $aHeaderRow);
            foreach ($aResult as $aRes) {
                foreach ($aRes['value'] as $aValue) {
                    $forEachCounter++;
                    if($forEachCounter == 500)
                    {
                        sleep(30);
                    }
                    $vDialogueName = $aRes['name'];
                    if (isset($aUserDtsData[$aValue[0]]) && isset($aUserDtsData[$aValue[1]])) {
                        $aUser1Detail = $aUserDtsData[$aValue[0]];
                        //print_r($aUser1Detail);die;
                        $aUser2Detail = $aUserDtsData[$aValue[1]];
                        $roleTagsArr = array();
                        $roleTagsVals = array();
                        $roleTagsArr1 = array();
                        $roleTagsVals1 = array();

                        $roleTags1 = $aUser1Detail["userProfessions"]["tags"];
                        $roleTags2 = $aUser2Detail["userProfessions"]["tags"];
                        if (!empty($roleTags1)) {
                            $roleTagsVals = explode(",", $roleTags1);
                        }
                        if (!empty($roleTags2)) {
                            $roleTagsVals1 = explode(",", $roleTags2);
                        }
                        if (!empty($roleTagsVals)) {
                            foreach ($roleTagsVals as $rt1) {
                                $values = array();
                                $roleTagId = 0;
                                $params['user_id'] = $aUser1Detail['UserQbDetail']['user_id'];
                                $params['key'] = $rt1;
                                $roleTagUser = $this->RoleTag->userRoleTags($params);
                                if (!empty($roleTagUser)) {
                                    $roleTagId = $roleTagUser[0]['rt']['id'];
                                    $values = array($roleTagUser[0]['rt']['value']);
                                }
                                $roleTagsArr[] = array("id" => $roleTagId, "key" => $rt1, "value" => $values);
                            }
                        }
                        if (!empty($roleTagsVals1)) {
                            foreach ($roleTagsVals1 as $rt) {
                                $values = array();
                                $roleTagId = 0;
                                $params['user_id'] = $aUser2Detail['UserQbDetail']['user_id'];
                                $params['key'] = $rt;
                                $roleTagUser = $this->RoleTag->userRoleTags($params);
                                if (!empty($roleTagUser)) {
                                    $roleTagId = $roleTagUser[0]['rt']['id'];
                                    $values = array($roleTagUser[0]['rt']['value']);
                                }
                                $roleTagsArr1[] = array("id" => $roleTagId, "key" => $rt, "value" => $values);
                            }
												}
                        $userBaseLoc = !empty($roleTagsArr[0]['value'][0]) ? $roleTagsArr[0]['value'][0] : '';
                        $userBaseLoc1 = !empty($roleTagsArr1[0]['value'][0]) ? $roleTagsArr1[0]['value'][0] : '';
                        $userSpeciality = !empty($roleTagsArr[1]['value'][0]) ? $roleTagsArr[1]['value'][0] : '';
                        $userSpeciality1 = !empty($roleTagsArr1[1]['value'][0]) ? $roleTagsArr1[1]['value'][0] : '';
                        $userRoleTag = !empty($roleTagsArr[2]['value'][0]) ? $roleTagsArr[2]['value'][0] : '';
                        $userRoleTag1 = !empty($roleTagsArr1[2]['value'][0]) ? $roleTagsArr1[2]['value'][0] : '';

                        if (!empty($aUser1Detail) && !empty($aUser2Detail)) {
                            $aRows = array(
                                $vDialogueName,
                                $aUser1Detail['UserQbDetail']['user_id'],
                                $aUser2Detail['UserQbDetail']['user_id'],
                                $aUser1Detail['0']['UserName'],
                                $aUser2Detail['0']['UserName'],
                                $aUser1Detail['companyNames']['company_name'],
                                $aUser2Detail['companyNames']['company_name'],
                                $aUser2Detail['userProfessions']['profession_type'],
																$aUser2Detail['userProfessions']['profession_type'],
																$userBaseLoc,
																$userBaseLoc1,
                                $userSpeciality,
                                $userSpeciality1,
                                $userRoleTag,
                                $userRoleTag1,
                            );
												}
												fputcsv($oCsvFile, $aRows, ',', '"');

                    }
                }
            }
            fclose($oCsvFile);
            $oClient = $this->getClient();
            $oDriveService = new Google_Service_Drive($oClient);
            $vFileId = "1Oze4CabItjFxhR2zhfPH33sjbj5N3nws1QC6KWrn-EY";

            $oFileMetadata = new Google_Service_Drive_DriveFile(array(
                'name' => 'PatientGroupUsers',
                'mimeType' => 'application/vnd.google-apps.spreadsheet'));

            // File's new content.
            $oData = file_get_contents(APP . '/webroot/googlesheet/credentialData/patientGroupDialogue.csv');

            $aAdditionalParams = array(
                'data' => $oData,
                'mimeType' => 'text/csv',
            );

            // Send the request to the API.
            $oUpdatedFile = $oDriveService->files->update($vFileId, $oFileMetadata, $aAdditionalParams);
            $vRange = 'PatientGroupUsers!A:C';
            $oSheetServices = new Google_Service_Sheets($oClient);
            $aValues = array(
                array(
                    '', '', '',
                ),
            );
            $oBody = new Google_Service_Sheets_ValueRange(array(
                'values' => $aValues,
            ));
            $aParams = array(
                'valueInputOption' => 'RAW',
            );
            $oResult = $oSheetServices->spreadsheets_values->append($vFileId, $vRange,
                $oBody, $aParams);
            $aResponse['success'] = "Updated Successfuly And File Id" . $file->id;
        } else {
            $aResponse['error'] = "No data found";
        }
        print_r($aResponse);exit();
        exit();

    }

    public function getParticipantRelation($aMembers, $nArrLength, $nSubsetSize, $nIndex, $aData, $nPtr, $aResponse)
    {
        if ($nIndex == $nSubsetSize) {
            $aSubSet = array();
            for ($j = 0; $j < $nSubsetSize; $j++) {
                $aSubSet[$j] = $aData[$j];
            }
            return $aSubSet;
        }

        if ($nPtr >= $nArrLength) {
            return;
        }

        $aData[$nIndex] = $aMembers[$nPtr];
        $aRes = $this->getParticipantRelation($aMembers, $nArrLength, $nSubsetSize, $nIndex + 1,
            $aData, $nPtr + 1, $aResponse);
        if (!empty($aRes)) {
            if (count($aRes) == count($aRes, COUNT_RECURSIVE)) {
                array_push($this->memberList, $aRes);
            }
        }

        $this->getParticipantRelation($aMembers, $nArrLength, $nSubsetSize, $nIndex,
            $aData, $nPtr + 1, $aResponse);
        return $this->memberList;
    }

    public function getUserDetails($nUserId)
    {

        // $conditions = "User.id > 0 AND User.status != 2";
        $aConditions = array(
            'UserEmployment.is_current' => 1,
            'UserEmployment.status' => 1,
            'User.status' => 1,
            'User.approved' => 1,
            "FIND_IN_SET(`qb_id`,$nUserId)",
        );
        $aUserInformation = $this->User->find('all',
            array(
                'joins' => array(
                    array(
                        'table' => 'user_qb_details',
                        'alias' => 'UserQbDetail',
                        'type' => 'left',
                        'conditions' => array('User.id = UserQbDetail.user_id'),
                    ),
                    array(
                        'table' => 'user_employments',
                        'alias' => 'UserEmployment',
                        'type' => 'left',
                        'conditions' => array('UserEmployment.user_id = User.id'),
                    ),
                    array(
                        'table' => 'user_profiles',
                        'alias' => 'userProfiles',
                        'type' => 'left',
                        'conditions' => array('User.id = userProfiles.user_id'),
                    ),
                    array(
                        'table' => 'professions',
                        'alias' => 'userProfessions',
                        'type' => 'left',
                        'conditions' => array('userProfiles.profession_id = userProfessions.id'),
                    ),
                    array(
                        'table' => 'user_duty_logs',
                        'alias' => 'userDutyLogs',
                        'type' => 'left',
                        'conditions' => array('User.id = userDutyLogs.user_id'),
                    ),
                    array(
                        'table' => 'user_subscription_logs',
                        'alias' => 'UserSubscriptionLog',
                        'type' => 'left',
                        'conditions' => array('User.id = UserSubscriptionLog.user_id'),
                    ),
                    array(
                        'table' => 'company_names',
                        'alias' => 'companyNames',
                        'type' => 'left',
                        'conditions' => array('userDutyLogs.hospital_id = companyNames.id'),
                    ),
                    array(
                        'table' => 'user_devices',
                        'alias' => 'userDevices',
                        'type' => 'left',
                        'conditions' => array('User.id = userDevices.user_id'),
                    ),
                    array(
                        'table' => 'enterprise_user_lists',
                        'alias' => 'EnterpriseUserList',
                        'type' => 'left',
                        'conditions' => array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id'),
                    ),

                ),
                'conditions' => $aConditions,
                'fields' => array('UserQbDetail.qb_id', 'UserQbDetail.user_id', "CONCAT(userProfiles.first_name, ' ' , userProfiles.last_name) AS UserName", 'userDutyLogs.status AS onCall', 'companyNames.company_name', 'userDevices.status AS signedIn', 'userProfessions.profession_type', 'userProfessions.tags'),
            )
        );
        // $log = $this->User->getDataSource()->getLog(false, false);
        // print_r($log);die;
        return $aUserInformation;
    }

    public function readNetworkGroupFromGoogleSheet()
    {
        $counter = 0;
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $dbhost = '35.177.136.62:3306';
        $dbuser = 'mb_analitics';
        $dbpass = 'Team@Medic@';
        $database = 'mb_analytics';
        $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
        if (!$conn) {
            die('Could not connect to instance: ' . mysqli_error($conn));
        }
        // echo "Connected to MySQL Successfully! <br />";
        // $sql = 'SELECT * FROM user_colleagues_transactions';

        $db = mysqli_select_db($conn, "mb_analytics");
        if (!$db) {
            die('Could not Select Database: ' . mysqli_error($conn));
        }

        $truncateBeforeUpdate = mysqli_query($conn, 'TRUNCATE TABLE network_group');
        $client = $this->getClient();
        $driveService = new Google_Service_Drive($client);
        $fileId = "1Oze4CabItjFxhR2zhfPH33sjbj5N3nws1QC6KWrn-EY";

        // File's new metadata.
        //$newTitle = "My New Report";
        $fileMetadata = new Google_Service_Drive_DriveFile(array(
            'name' => 'PatientGroupUsers',
            'mimeType' => 'application/vnd.google-apps.spreadsheet'));
        $range = 'PatientGroupUsers!A:M';
        $sheetServices = new Google_Service_Sheets($client);

        $result = $sheetServices->spreadsheets_values->get($fileId, $range, ['majorDimension' => 'ROWS']);
        // array_shift($result['values']);
        $row = 1;
        foreach ($result['values'] as $sheetData) {
            $row++;
            if ($row == 2) {
                continue;
            }

            // echo "<pre>";print_r($sheetData[8]);exit();
            $dialogue_name = $sheetData[0];
            $userid_one = $sheetData[1];
            $userid_two = $sheetData[2];
            $username_one = $sheetData[3];
            $username_two = $sheetData[4];
            $userone_trust = $sheetData[5];
            $usertwo_trust = $sheetData[6];
            $userone_profession = $sheetData[7];
            $usertwo_profession = $sheetData[8];
            $insertData = mysqli_query($conn, "INSERT INTO network_group (dialogue_name,userid_one,userid_two, username_one,username_two,userinstitute_one,userinstitute_two,userprofession_one, userprofession_two)
            VALUES ('" . $dialogue_name . "','" . $userid_one . "','" . $userid_two . "','" . $username_one . "', '" . $username_two . "' ,'" . $userone_trust . "','" . $usertwo_trust . "','" . $userone_profession . "','" . $usertwo_profession . "')");
            $counter++;
            if($counter == 500)
            {
                sleep(30);
            }

        }
        exit();
        echo "<pre>";
        print_r("Done");exit();

        exit();
    }


    public function userinformationCheck()
	{
		ini_set('max_execution_time', 4*300);
    	ini_set('max_input_time', 4*300);
		echo BASE_URL;
		//echo "HI";exit();
		$response = array();
		$dateofSheetUpdation = date('d-m-Y H:i');
		$userLoginStatus = 0;


		// $conditions = "User.id > 0 AND User.status != 2";
		$type = array(0,1);
		$conditions = array(
				'UserEmployment.is_current' => 1,
				'UserEmployment.status' => 1,
				'User.status' => 1,
				'User.approved' => 1,
				'EnterpriseUserList.status' => 1,
			);
		$userInformation = $this->User->find('all', 
						array(
							'joins'=>
							array(
								array(
									'table' => 'user_employments',
									'alias' => 'UserEmployment',
									'type' => 'left',
									'conditions'=> array('UserEmployment.user_id = User.id')
								),
								array(
									'table' => 'user_profiles',
									'alias' => 'userProfiles',
									'type' => 'left',
									'conditions'=> array('User.id = userProfiles.user_id')
								),
								array(
									'table'=> 'professions',
									'alias' => 'userProfessions',
									'type' => 'left',
									'conditions'=> array('userProfiles.profession_id = userProfessions.id')
								),
								array(
									'table'=> 'user_duty_logs',
									'alias' => 'userDutyLogs',
									'type' => 'left',
									'conditions'=> array('User.id = userDutyLogs.user_id')
								),
								array(
									'table' => 'user_subscription_logs',
									'alias' => 'UserSubscriptionLog',
									'type' => 'left',
									'conditions'=> array('User.id = UserSubscriptionLog.user_id')
								),
								array(
									'table'=> 'company_names',
									'alias' => 'companyNames',
									'type' => 'left',
									'conditions'=> array('userDutyLogs.hospital_id = companyNames.id')
								),
								array(
									'table' => 'enterprise_user_lists',
									'alias' => 'EnterpriseUserList',
									'type' => 'left',
									'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
								),
								
							),
							'conditions' => $conditions,
							'fields' => array('User.id', 'User.email', "CONCAT(userProfiles.first_name, ' ' , userProfiles.last_name) AS UserName", 'userDutyLogs.status AS onCall', 'companyNames.company_name', 'userProfessions.profession_type','userProfessions.id'),
							'group'=> array('User.id'),
							'order'=> 'User.id ASC'
						)
					);

		// echo "<pre>";print_r($userInformation);exit();



		// $userInformation = $this->User->userStatusDetailByLimit();
    	if(!empty($userInformation))
		{
			$userBand = '';
			$filename='userInformation.csv';
			$csv_file = fopen(APP .'/webroot/googlesheet/credentialData/'.$filename, 'w+');
 		

            $header_row = array("date of sheet updation", "user  id", "user name","OnCall (1/0)","Trust Name","Signed In","proffesion", "Base Location", "Speciality","Grade", "Band");
            fputcsv($csv_file, $header_row);
			foreach($userInformation AS $result)
			{
				$userLoginStatus = 0;
				$signedInStatusOnWeb = $this->UserLoginTransaction->find("first", array("conditions"=> array("user_id"=> $result['User']['id'], "application_type"=> "MB-WEB"), "order"=> array("id DESC")));

				$signedInStatusAndroid = $this->UserLoginTransaction->find("first", array("conditions"=> array("user_id"=> $result['User']['id'], "application_type"=> "MB"), "order"=> array("id DESC")));
				// echo "<pre>";print_r($signedInStatusOnWeb);
				// echo "<pre>";print_r($signedInStatusAndroid);exit();


				if($signedInStatusOnWeb['UserLoginTransaction']['login_status'] == 1 || $signedInStatusAndroid['UserLoginTransaction']['login_status'] == 1)
				{
					$userLoginStatus = 1;
				}
				else
				{
					$userLoginStatus = 0;
				}
				// echo $userLoginStatus;
				// exit();
				$roleTagsArr = array(); $roleTagsVals = array();
				$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $result['userProfessions']['id'])));
				if(!empty($roleTags['Profession']['tags'])){
					$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
				}
				if(!empty($roleTagsVals)){
					foreach($roleTagsVals as $rt){
						$values = array(); $roleTagId = 0;
						$params['user_id'] = $result['User']['id'];
						$params['key'] = $rt;
						$roleTagUser = $this->RoleTag->userRoleTags($params);
						if(!empty($roleTagUser)){
							$roleTagId = $roleTagUser[0]['rt']['id'];
							$values = array($roleTagUser[0]['rt']['value']);
						}
						$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
					}
				}
				// echo "<pre>";print_r($roleTagsArr);exit();

				$arrayCount = count($roleTagsArr);
				if($arrayCount == 3)
				{
					$userBaseLocation = !empty($roleTagsArr[0]['value'][0]) ? $roleTagsArr[0]['value'][0] :'' ;
					$userSpeciality = !empty($roleTagsArr[1]['value'][0]) ? $roleTagsArr[1]['value'][0] :'' ;
					$userRoleTag = !empty($roleTagsArr[2]['value'][0]) ? $roleTagsArr[2]['value'][0] :'' ;
				}
				if($arrayCount == 2)
				{
					$userBaseLocation = !empty($roleTagsArr[0]['value'][0]) ? $roleTagsArr[0]['value'][0] :'' ;
					$userBand = !empty($roleTagsArr[1]['value'][0]) ? $roleTagsArr[1]['value'][0] :'' ;
				}
				$userId = !empty($result['User']['id']) ? $result['User']['id'] :'' ;
				$userName = !empty($result[0]['UserName']) ? $result[0]['UserName']:'';
        		$onCallStatus = !empty($result['userDutyLogs']['onCall']) ? 1 :0 ;
        		$trustName = !empty($result['companyNames']['company_name']) ? $result['companyNames']['company_name'] :'NA' ;
        		// $signedIn = !empty($result['userDevices']['signedIn']) ? 1 :0 ;
        		$signedIn =$userLoginStatus;
        		$proffesion = !empty($result['userProfessions']['profession_type']) ? $result['userProfessions']['profession_type'] :'NA' ;
            		$rows = array(
		                $dateofSheetUpdation,
		                $userId,
		                $userName,
		                $onCallStatus,
		                $trustName,
		                $signedIn,
		                $proffesion,
		                $userBaseLocation,
		                $userSpeciality,
		                $userRoleTag,
		                $userBand
	                );
	                fputcsv($csv_file,$rows,',','"');
			}
			fclose($csv_file);
			// $client = $this->getClient();
			// $service = new Google_Service_Drive($client);
			// $fileMetadata = new Google_Service_Drive_DriveFile(array(
			//   'name' => 'Sheet1',
			//   'mimeType' => 'application/vnd.google-apps.spreadsheet'));
			// $content = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/app/webroot/googlesheet/credentialData/patientDialogue.csv');
			// $file = $service->files->create($fileMetadata, array(
			//   'data' => $content,
			//   'mimeType' => 'text/csv',
			//   'uploadType' => 'multipart',
			//   'fields' => 'id'));
			// printf("File ID: %s\n", $file->id);

			$client = $this->getClient();
			$driveService = new Google_Service_Drive($client);
			$fileId = "1Iqrvn9DS0DyYm6KRpn3MeSIVSF04TSTs0Z8D4eJnH3g";

			    // File's new metadata.
			//$newTitle = "My New Report";
			$fileMetadata = new Google_Service_Drive_DriveFile(array(
			  'name' => 'User Information from Server',
			  'mimeType' => 'application/vnd.google-apps.spreadsheet'));

			    // File's new content.
			$data = file_get_contents(APP .'/webroot/googlesheet/credentialData/userInformation.csv');

			$additionalParams = array(
			    'data' => $data,
			    'mimeType' => 'text/csv'
			);

			// Send the request to the API.
			$updatedFile = $driveService->files->update($fileId, $fileMetadata, $additionalParams);
			$range = 'User Information from Server!A:I';
			$sheetServices = new Google_Service_Sheets($client);
			$values = array(
			            array(
			                '','','','','','','','','','',''
			            )
			        );
			$body = new Google_Service_Sheets_ValueRange(array(
			  'values' => $values
			));
			$params = array(
			  'valueInputOption' => 'RAW'
			);
			$result = $sheetServices->spreadsheets_values->append($fileId, $range,
			    $body, $params);
			$response['success'] = "Updated Successfuly And File Id";
		}
		else
		{
			$response['error'] = "No data found";
		}
		print_r($response);exit();
	}

}
?>