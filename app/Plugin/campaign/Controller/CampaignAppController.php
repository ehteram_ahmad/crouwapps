<?php
class CampaignAppController extends AppController {
	public $components = array('Session');
	public function beforeFilter(){
		if( null !== $this->Session->read('userName') && $this->Session->read('userName') !='' ){
			if(in_array($this->Session->read('userName'), array('subadmin1','subadmin2'))){
              $this->redirect(BASE_URL . 'subadmin/SubadminHome/dashboard');
              exit;}else{
			$this->layout = 'Campaigndefault';
		}}else{
			$this->layout = 'Campaignlogin2';
		}
	}
}