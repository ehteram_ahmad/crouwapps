<?php
class CampaignHomeController extends CampaignAppController {
	public $uses = array('User','UserSpecilitie','AdminUserSentMail','AdminUser','EmailTemplate');
	public $components = array('RequestHandler','Paginator','Session', 'Common', 'Campaign.Email', 'Image');
	public $helpers = array('Js','Html', 'Paginator');
	public function mail_conf() {
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
	    $mandrill = new Mandrill('Mb39AZ8BVLBYO2N4SLmUjA');
	}

    public function login(){
    	if( $this->request->is('ajax') ) {
    		$this->autoRender = false;
    		if( !empty($this->request->data['userName']) && !empty($this->request->data['password']) ){
        		$this->request->data['UserAdmin']['username'] = $this->request->data['userName'];
    			$this->request->data['UserAdmin']['password'] = $this->request->data['password'];
    			$userData = $this->AdminUser->find('count', 
    						array('conditions'=> 
    							array(
    								'username'=> $this->request->data['userName'],
    								'password'=> md5( $this->request->data['password'] ),
    								'status'=> 1
    								)
    						));
    			if( $userData > 0 ){
    				$this->Session->write('userName', $this->request->data['userName']);
    				//$this->redirect('/admin/CampaignHome/dashboard');
    				echo 'success';
    			}else{
    				echo 'Invalid Login Credentials';
    			}
				/*if ( $this->Auth->login() ) {
					$this->redirect('/admin/index/dashboard');
				}*/
    		}else{
    			echo ERROR_603;
    		}
		}
    	exit;
    }


	public function logout(){
		$this->Session->destroy('userName');
		$this->redirect('/campaign');
	}

	public function forgotPwd(){
	    if( $this->request->is('ajax') ) {
	        $this->autoRender = false;
	        if( !empty($this->request->data['email']) ){
	            $userData = $this->AdminUser->find('first',array('conditions'=>array('email'=> $this->request->data['email'], 'status'=> 1)));
	            if( count($userData) > 0 ){
	                $randompass = $this->randomPassword();
	                $params['name'] = $userData['AdminUser']['username'];
	                $params['toMail'] = $this->data['email'];
	                $params['from'] = NO_REPLY_EMAIL;
	                $params['password'] = $randompass;
	                $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Forgot password')));
	                $params['temp'] = $templateContent['EmailTemplate']['content'];
	                 try{
	                 // Sending Email
	                 if($this->Email->forgotPassword( $params )){
	                     //** Update new password to DB
	                     $this->AdminUser->updateAll(array('AdminUser.password'=>'"'.md5($randompass).'"'),array('AdminUser.email'=>$this->data['email']));
	                 }else{
	                 }
	                }catch(Exception $e){
	                } 

	                echo 'success';
	            }else{
	                echo 'Invalid Login Credentials';
	            }
	        }else{
	            echo ERROR_603;
	        }
	    }
	    exit;
	}
	/*
	________________________________________________
	Method Name:Main=>randomPassword
	Purpose:
	Detail: 
	created by :Developer
	Params:NA
	_________________________________________________
	*/

	    public function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    
	    // Small,capital,number minimum 6 char
	    $smallAlphabet = "abcdefghijklmnopqrstuwxyz";
	    $capitalAlphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
	    $number = "0123456789";

	    $string = $capitalAlphabet[rand(0, strlen($capitalAlphabet)-1 )].$smallAlphabet[rand(0, strlen($smallAlphabet)-1)].$number[rand(0, strlen($number)-1 )].$capitalAlphabet[rand(0, strlen($capitalAlphabet)-1 )].$number[rand(0, strlen($number)-1 )].$smallAlphabet[rand(0, strlen($smallAlphabet)-1 )];
	    
	    return $string; //turn the array into a string
	    }

	


	/*
	On: 18-10-2016
	I/P: 
	O/P: Provides the list of users.
	Desc: Showing user list.
	*/	

	public function index(){
    	App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
        $mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');

		if(isset($this->request->data['mail']['template'])){
			$data=$this->data;
			$userType=$data['user'];
			$templateType=$data['from'];
			$customMail=trim($data['mail']['emails'],',');
			$firstName=$data['mail']['firstName'];
			$lastName=$data['mail']['lastName'];
			$speciality=$data['mail']['interest'];
			$regFrom=$data['mail']['from'];
			$regTo=$data['mail']['to'];
			$country=$data['mail']['country'];
			$profession=$data['mail']['profession'];
			$template=$data['mail']['template'];

			foreach ($data['of'] as $value) {
				$source[]=$value;
			}
				$userSource=array('User.registration_source'=>$source);
			if($userType =="All"){
				$status=array('User.status'=>array(0,1),'User.approved'=>array(0,1));
			}elseif ($userType =="Approved") {
				$status=array('User.status'=>array(1),'User.approved'=>array(1));
			}elseif ($userType =="Pending") {
				$status=array('OR'=>array('User.status'=>array(0),'User.approved'=>array(0)),'AND'=>array('User.status'=>array(0,1)));
			}else{
				$status=array();
			}
			if(!isset($regTo) || $regTo==""){
			  $endDate = date("Y-m-d");
			}else{
			  $endDate = date('Y-m-d', strtotime($regTo));
			}
			if(!isset($regFrom) || $regFrom==""){
			  $date = array();
			}else{
			  $startDate = date('Y-m-d', strtotime($regFrom));
			  $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
			}
			if(!isset($firstName) || $firstName==""){
			    $firstName=array();
			}else{
			    $firstName=trim($firstName);
			    $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($firstName.'%'));
			}
			if(!isset($lastName) || $lastName==""){
			    $lastName=array();
			}else{
			    $lastName=trim($lastName);
			    $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower($lastName.'%'));
			}
			if(!empty($country)){
			    $country=array('UserProfile.country_id'=>$country);
			}else{
			   $country=array();
			}
			if(!empty($profession)){
			    $profession=array('UserProfile.profession_id'=>$profession);
			}else{
			   $profession=array();
			}
			if(!empty($customMail)){
				$myArray = explode(',',$customMail);
				foreach ($myArray as $value) {
					$condition=array('User.email'=>$value);
					$result=$this->getUserList($condition);
					if($result){
						$mailName[]=$result[0]['UserProfile']['first_name'];
					}else{
						$mailName[]='Sir';
					}
					$mailTo[]=$value;
				}
					$this->mandrillMailSend($mailTo,$mailName,$template);
			}else{
				$conditions=array_merge($firstName,$lastName,$status,$date,$country,$profession,$userSource);
			$result=$this->getUserList($conditions);
// ------------------------ with specialities ----------------------
			if(!empty($speciality)){
				$interest=array('UserSpecility.specilities_id'=>$speciality);

				asort($speciality);
				$filterCount=count($speciality);
				foreach ($result as $value) {
					if(count($value['UserSpecilities']) == $filterCount){
					$specialityFinal[]=$value['UserSpecilities'];
					}
				}
				foreach ($specialityFinal as $specValue) {
					usort($specValue, function($a, $b) {
					    return $a['specilities_id'] - $b['specilities_id'];
					});
					$finalSpec[]=$specValue;
				}
				for ($k=0; $k <count($finalSpec) ; $k++) { 
					$count=0;
					for ($c=0; $c < $filterCount ; $c++) { 
						if($finalSpec[$k][$c]['specilities_id'] == $speciality[$c] ){
						$count++;
					}
						if($count==$filterCount){
							$finalUsersSpec[]=$finalSpec[$k];
							$specUsers[]=$this->User->find('first',array('conditions'=>array('User.id'=>$finalSpec[$k][0]['user_id'])));
						}
					}
				}
				if(isset($specUsers)){
					foreach ($specUsers as $value) {
						$mailTo[]=$value['User']['email'];
						$mailName[]=$value['UserProfile']['first_name'];
					}
				$this->mandrillMailSend($mailTo,$mailName,$template);
				}else{
					echo "<script>alert('No User Found.');</script>";
				}
			}else{
				$interest=array();
				foreach ($result as $value) {
					$mailTo[]=$value['User']['email'];
					$mailName[]=$value['UserProfile']['first_name'];
				}
				$this->mandrillMailSend($mailTo,$mailName,$template);
				echo "<script>alert('Mail Sent.');</script>";
			}
		}
			$this->redirect(BASE_URL . 'campaign/CampaignHome/index');
			exit();
		}
		$interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
		$profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1   ORDER BY profession_type ASC');
        $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        $result = $mandrill->templates->getList();
		$this->set(array('speciality'=>$interestlists,'country'=>$country,'professions'=>$profession,'list'=>$result));
	}

	/*
	On: 18-10-2016
	I/P: 
	O/P: Provides the preview of mail.
	Desc: 
	*/

	public function previewMail(){
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
        $mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');

		$name=$this->data['content'];
		$result = $mandrill->templates->info($name);
		$content=$result['code'];
		pr($content);
		exit();
		// $type=$this->data['type'];
		$this->set(array('content'=>$content,'subject'=>$name));
		$this->render('/Elements/CampaignHome/template_preview');
	}


	/*
	On: 18-10-2016
	I/P: 
	O/P: Provides the list of users.
	Desc: 
	*/	
	public function getUserList($conditions=array()){
        // $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        // $profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1   ORDER BY profession_type ASC');
        // $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        // $interestLists['interestlist'] = $interestlists; 
        // $this->set(array('interestLists'=>$interestLists,'country'=>$country,'professions'=>$profession));
        //** Get User Data
        $fields = array("User.id, User.email,User.registration_source, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                ),
                'fields'=> $fields,
                'conditions'=>$conditions,
                'group'=> array('User.id'),
                'order'=> 'User.registration_date DESC', 
                // 'limit'=>20,
                );
                $users=$this->User->find('all',$options);
        if(isset($users) && !empty($users)){
            foreach($users as $user){
                $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                $userS[]=$user;
            }
        }
        else{
          $userS=array();
        }
        return $userS;
	}

	public function userList(){
		$conditions = array('User.status'=>array(0,1));
		$userS=$this->getUserList($conditions);
		$limit=20;
		$this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit));
	}

	public function get_admin_user_mail($id){
	    $mail=array();
	    $conditions = array('AdminUserSentMail.user_id'=> $id); 
	    $mail=$this->AdminUserSentMail->find('all',array('conditions'=>$conditions));
	    return $mail;
	}

	/*
	On: 18-10-2016
	I/P: 
	O/P: Provides the list of filtered users.
	Desc: Take the i/p from form search and providing the filtered out user list.
	*/

	public function filter() {
		$users=$this->User->find('all');
		foreach ($users as $value) {
			$speciality=$this->UserSpecilitie->find('all',array('conditions'=>array('user_id'=>$value['User']['id'],'status'=>1)));
			if(count($speciality) == 1){
				$specData[]=$speciality;
			}
		}
		$this->set(array('users'=>$users,'spec'=>$specData));
		}


	/*
	On: 18-10-2016
	I/P: 
	O/P: Provides the list of email templates.
	Desc: 
	*/

	public function templateList(){
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
	    $mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
	    $result = $mandrill->templates->getList();
	    $this->set('list',$result);
	}

	/*
	On: 18-10-2016
	I/P: take the list of users and other variables like('name','global variable values')
	O/P: Send mail with maildrill
	Desc: With the list of users,it sends the mail in a bunch.
	*/

	public function mandrillMailSend($mailTo,$mailName,$template){
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));

			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');

			$template_name = $template;
		    $template_content = array(
		        array(
		            'name' => $template,
		            'content' => 'example content'
		        )
		    );
		    for ($i=0; $i <count($mailName) ; $i++) { 
		    	$to[$i]=array('email'=>$mailTo[$i],
		        		'name' =>$mailName[$i],
		        		'type' => 'to');
		    }
		    for ($j=0; $j <count($mailName) ; $j++) { 
			    $var[$j]=array(
			        'rcpt' => $mailTo[$j],
			        'vars' => array(
			            array(
			                'name' => 'name',
			                'content' => $mailName[$j]
			            ),
	            		array(
	                	'name' => 'UPDATE_PROFILE',
	                	'content' => 'http://mediccreations.us12.list-manage1.com/profile?u=4f8f2caf54859b1e05155fb68&id=d83d76884e&e=9d2e0a3d4d" style="color:#c52e26;font-weight:bold;text-decoration:none" target="_blank'
	            		),
	            		array(
	                	'name' => 'UNSUB',
	                	'content' => 'http://mediccreations.us12.list-manage2.com/unsubscribe" style="color:#c52e26;font-weight:bold;text-decoration:none" target="_blank'
	            		),
	            		array(
	                	'name' => 'LIST_ADDRESS_HTML',
	                	'content' => '<div class="m_2554631907282546865vcard"><span class="m_2554631907282546865org m_2554631907282546865fn">Medic Creations</span><div class="m_2554631907282546865adr"><div class="m_2554631907282546865street-address">Oakdale</div><div class="m_2554631907282546865extended-address">Royal Oak Hill</div><span class="m_2554631907282546865locality">Newport</span>, <span class="m_2554631907282546865region">Gwent</span>  <span class="m_2554631907282546865postal-code">NP18 1JF</span> <div class="m_2554631907282546865country-name">United Kingdom</div></div><br><a href="http://mediccreations.us12.list-manage2.com/vcard?u=3D4f8f2caf54859b1e05155fb68&id=3Dd83d76884e" class="m_2554631907282546865hcard-download" target="_blank">Add us to your address book</a></div>'
	            		)
			        )
			    );
			}
	    $message = array(
		        'html' => '<p>Example HTML content</p>',
		        'text' => 'Example text content',
		        // 'subject' => $template,
		        'subject' => 'Server Maintenance & Security Update',
		        // 'from_email' => 'sandeep@mediccreations.com',
		        'from_email' => 'medicbleep@theoncallroom.com',
		        // 'from_name' => 'Sandeep Bansal',
		        'from_name' => 'Medic Bleep Team',
		        'to' => $to,
		        "merge_vars" => $var,
	        );
	    $async = false;
	    $ip_pool = 'Main Pool';
	    $send_at = '07-09-2016';
	    // pr($message);
	        $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
	    
	}


	/*
	On: 18-10-2016
	I/P: 
	O/P: Add a email template on the maildrill
	Desc: 
	*/

	public function abcd(){
		try {
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		    $mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
		    $name = 'Demo2';
		    $from_email = 'from_email@example.com';
		    $from_name = 'Example Name';
		    $subject = 'example subject';
		    $code = '<div>My Code</div>';
		    $text = 'Example text content';
		    $publish = true;
		    $labels = array('example-label');
		    $result = $mandrill->templates->add($name, $from_email, $from_name, $subject, $code, $text, $publish, $labels);
		    pr($result);
		    exit();
		} catch(Mandrill_Error $e) {
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
		    throw $e;
		}
	}
}