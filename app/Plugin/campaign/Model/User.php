<?php 
/*
User model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');

class User extends AppModel {

	var $hasOne = array(
        'UserProfile' => array(
            'className' => 'UserProfile',
            'foreignKey' => 'user_id',
        ),
        'InstitutionInformation' => array(
            'className' => 'InstitutionInformation',
            'foreignKey' => 'user_id',
        )
    );

	public function userDetailsByInstitute($textKey){
		$userLists = array();
		//$userLists = $this->find('all');
		//echo "<pre>"; print_r($userLists);die;
		//$conditions = "userEmployments.is_current = 1 AND User.id > $textKey";
		$conditions = "User.id > $textKey";
		$userLists = $this->find('all', 
						array(
							'joins'=>
							array(
								array(
									'table' => 'user_profiles',
									'alias' => 'userProfiles',
									'type' => 'left',
									'conditions'=> array('User.id = userProfiles.user_id')
								),
								array(
									'table'=> 'user_employments',
									'alias' => 'userEmployments',
									'type' => 'left',
									'conditions'=> array('User.id = userEmployments.user_id')
								),
								array(
									'table'=> 'institute_names',
									'alias' => 'instituteNames',
									'type' => 'left',
									'conditions'=> array('userEmployments.company_id = instituteNames.id')
								),
								
							),
							'conditions' => $conditions,
							'fields' => array('User.id', 'User.email', "CONCAT(userProfiles.first_name, ' ' , userProfiles.last_name) AS UserName", 'instituteNames.institute_name', 'instituteNames.status'),
							'group'=> array('User.id'),
							'order'=> 'User.id DESC',
							'limit'=> 200
						)
					);
	//echo "<pre>"; print_r($userLists);die;
		return $userLists;

	}


	public function userStatusDetail(){
		$userLists = array();
		//$userLists = $this->find('all');
		//echo "<pre>"; print_r($userLists);die;
		//$conditions = "userEmployments.is_current = 1 AND User.id > $textKey";
		$conditions = "User.id > 0";
		$userLists = $this->find('all', 
						array(
							'joins'=>
							array(
								array(
									'table' => 'user_profiles',
									'alias' => 'userProfiles',
									'type' => 'left',
									'conditions'=> array('User.id = userProfiles.user_id')
								),
								array(
									'table'=> 'professions',
									'alias' => 'userProfessions',
									'type' => 'left',
									'conditions'=> array('userProfiles.profession_id = userProfessions.id')
								),
								array(
									'table'=> 'user_duty_logs',
									'alias' => 'userDutyLogs',
									'type' => 'left',
									'conditions'=> array('User.id = userDutyLogs.user_id')
								),
								array(
									'table'=> 'company_names',
									'alias' => 'companyNames',
									'type' => 'left',
									'conditions'=> array('userDutyLogs.hospital_id = companyNames.id')
								),
								array(
									'table'=> 'user_devices',
									'alias' => 'userDevices',
									'type' => 'left',
									'conditions'=> array('User.id = userDevices.user_id')
								),
								
							),
							'conditions' => $conditions,
							'fields' => array('User.id', 'User.email', "CONCAT(userProfiles.first_name, ' ' , userProfiles.last_name) AS UserName", 'userDutyLogs.status AS onCall', 'companyNames.company_name', 'userDevices.status AS signedIn', 'userProfessions.profession_type'),
							'group'=> array('User.id'),
							'order'=> 'User.id ASC'
						)
					);
	//echo "<pre>"; print_r($userLists);die;
		return sizeof($userLists);

	}

	public function userStatusDetailByLimit(){
		$userLists = array();
		//$userLists = $this->find('all');
		//echo "<pre>"; print_r($userLists);die;
		//$conditions = "userEmployments.is_current = 1 AND User.id > $textKey";
		$conditions = "User.status = 1 AND User.approved = 1";
		$userLists = $this->find('all', 
						array(
							'joins'=>
							array(
								array(
									'table' => 'user_profiles',
									'alias' => 'userProfiles',
									'type' => 'left',
									'conditions'=> array('User.id = userProfiles.user_id')
								),
								array(
									'table'=> 'professions',
									'alias' => 'userProfessions',
									'type' => 'left',
									'conditions'=> array('userProfiles.profession_id = userProfessions.id')
								),
								array(
									'table'=> 'user_duty_logs',
									'alias' => 'userDutyLogs',
									'type' => 'left',
									'conditions'=> array('User.id = userDutyLogs.user_id')
								),
								array(
									'table'=> 'company_names',
									'alias' => 'companyNames',
									'type' => 'left',
									'conditions'=> array('userDutyLogs.hospital_id = companyNames.id')
								),
								array(
									'table'=> 'user_devices',
									'alias' => 'userDevices',
									'type' => 'left',
									'conditions'=> array('User.id = userDevices.user_id')
								),
								
							),
							'conditions' => $conditions,
							'fields' => array('User.id', 'User.email', "CONCAT(userProfiles.first_name, ' ' , userProfiles.last_name) AS UserName", 'userDutyLogs.status AS onCall', 'companyNames.company_name', 'userDevices.status AS signedIn', 'userProfessions.profession_type'),
							'group'=> array('User.id'),
							'order'=> 'User.id ASC'
						)
					);
	//echo "<pre>"; print_r($userLists);die;
		return $userLists;

	}

}
?>