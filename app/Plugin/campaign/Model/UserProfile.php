<?php 
/*
Add, update, Validate user profile.
*/
App::uses('AppModel', 'Model');

class UserProfile extends AppModel {


    public $validate = array(		
            'first_name' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message'  => 'First name is Required.'
                ),
    			'characters' => array(
    				'rule' => array('custom', '/^[a-z0-9. ]*$/i'),
    				'message'  => 'Alphanumeric characters with spaces only'
    			)
            ),
            'last_name' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message'  => 'Last name is Required.'
                ),
    			'characters' => array(
    				'rule' => array('custom', '/^[a-z0-9 ]*$/i'),
    				'message'  => 'Alphanumeric characters with spaces only'
    			)
            ),
        );

    /*
    On: 06-08-2015
    I/P: $fields = array(), $conditions = array()
    O/P: true/false
    Desc: 
    */
    public function updateFields( $fields = array(), $conditions = array() ){
        $return = 0;
        if( !empty($fields) && !empty($conditions) ){
            $this->updateAll( $fields, $conditions );

            if( $this->getAffectedRows() ){
                $return = 1;
            }
        }
        return $return;
    }

    /*
    -----------------------------------------------------------------------------------------------
    On: 30-05-2017
    I/P:
    O/P:
    Desc: Get user information from user id
    -----------------------------------------------------------------------------------------------
    */

    public function getUserInfo($userId){
        $userLists = array();
        //$userLists = $this->find('all');
        //echo "<pre>"; print_r($userLists);die;
        //$conditions = "userEmployments.is_current = 1 AND User.id > $textKey";
        $conditions = "UserProfile.user_id = $userId";
        $userLists = $this->find('all', 
                        array(
                            'joins'=>
                            array(
                                array(
                                    'table'=> 'professions',
                                    'alias' => 'userProfessions',
                                    'type' => 'left',
                                    'conditions'=> array('UserProfile.profession_id = userProfessions.id')
                                ),
                                array(
                                    'table'=> 'user_employments',
                                    'alias' => 'userEmployments',
                                    'type' => 'left',
                                    'conditions'=> array('UserProfile.user_id = userEmployments.user_id AND userEmployments.is_current = "1" ')
                                ),
                                array(
                                    'table'=> 'company_names',
                                    'alias' => 'companyNames',
                                    'type' => 'left',
                                    'conditions'=> array('userEmployments.company_id = companyNames.id')
                                ),
                            ),
                            'conditions' => $conditions,
                            'fields' => array("CONCAT(UserProfile.first_name, ' ' , UserProfile.last_name) AS UserName", 'userProfessions.profession_type',
                             'companyNames.company_name')
                        )
                    );
    //echo "<pre>"; print_r($userLists);die;
        return $userLists; 

    }			
	
}
?>