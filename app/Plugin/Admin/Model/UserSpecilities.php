<?php 
/*
User Specilities.
*/
App::uses('AppModel', 'Model');

class UserSpecilities extends AppModel {            
    public $name = 'UserSpecilities';

    var $belongsTo = array(
        'Specilities' => array(
            'className' => 'Specilities',
            'conditions'=> 'Specilities.id = UserSpecilities.specilities_id'
            )
    );

    function specName($id){
        // return "Hello";
        $specialId=null;
        $conditions = array("user_id"=>$id, "UserSpecilities.status"=> 1);
        $options =  array(
                            'joins'=> array(
                                array(
                                'table' => 'specilities',
                                'alias' => 'spec',
                                'type' => 'inner',
                                'conditions'=> array('spec.id = UserSpecilities.specilities_id')
                                ),
                            ),
                            'conditions'=> $conditions,
                            'fields'=> '`UserSpecilities`.`user_id`, `UserSpecilities`.`specilities_id`,  spec.id, spec.name, spec.img_name',
                            'order'=> array('spec.name'),
                            // 'limit'=> $pageSize,
                            // 'offset'=> $offsetVal
                        );
        $specId=$this->find("all", $options);
        // return $specId;

        if(count($specId)>0){
            foreach ($specId as $value) {
                $specialId[]=$value['spec']['name'];
            }
        }
        else{
            $specialId=null;
        }
        return $specialId;
    }
}
?>