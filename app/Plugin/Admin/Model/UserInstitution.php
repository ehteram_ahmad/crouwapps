<?php 
/*
Model used to stores user's education details
*/
App::uses('AppModel', 'Model');

class UserInstitution extends AppModel {
	
	/*
	---------------------------------------------------------------------------------------
	On: 05-01-2015
	I/P: 
	O/P: 
	Desc: Fetches all user institutions
	---------------------------------------------------------------------------------------
	*/
	public function getUserInstitutions( $userId = NULL , $params = array()){
		$userInstitutionList = array();
		if( !empty($userId) ){
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			$conditions = array("UserInstitution.user_id"=> $userId, "UserInstitution.status"=> 1);
			$userInstitutionList = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'institute_names',
										'alias' => 'InstituteName',
										'type' => 'left',
										'conditions'=> array('UserInstitution.institution_id = InstituteName.id')
									),
									array(
										'table' => 'education_degrees',
										'alias' => 'EducationDegree',
										'type' => 'left',
										'conditions'=> array('UserInstitution.education_degree_id = EducationDegree.id')
									),
								),
								'conditions' => $conditions,
								'fields'=> array('UserInstitution.id, UserInstitution.user_id, UserInstitution.institution_id, UserInstitution.education_degree_id,UserInstitution.description, UserInstitution.status, UserInstitution.from_date, UserInstitution.to_date, InstituteName.id, InstituteName.institute_name, EducationDegree.id, EducationDegree.degree_name,EducationDegree.short_name'),
								'limit'=> $pageSize,
								'offset'=> $offsetVal,
								'order'=> 'UserInstitution.id'
							)
						);
		}
		return $userInstitutionList;
	}
}
?>