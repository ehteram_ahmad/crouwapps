<?php 
/*
Add, update, List user employments.
*/
App::uses('AppModel', 'Model');

class UserEmployment extends AppModel {
	
	/*
	--------------------------------------------------------------
	On: 05-01-2015
	I/P: 
	O/P: 
	Desc: Fetches all user employments.
	--------------------------------------------------------------
	*/
	public function getUserEmployment( $userId = NULL, $params = array() ){
		$userEmploymentList = array();
		if( !empty($userId) ){
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			// $pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			// $offsetVal = ( $pageNum - 1 ) * $pageSize;
			$conditions = array("UserEmployment.user_id"=> $userId);
			$userEmploymentList = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'company_names',
										'alias' => 'Company',
										'type' => 'left',
										'conditions'=> array('UserEmployment.company_id = Company.id')
									),
									array(
										'table' => 'designations',
										'alias' => 'Designation',
										'type' => 'left',
										'conditions'=> array('UserEmployment.designation_id	 = Designation.id')
									),
								),
								'conditions' => $conditions,
								'fields'=> array('UserEmployment.id, UserEmployment.company_id, UserEmployment.from_date, UserEmployment.to_date, UserEmployment.location, UserEmployment.description, UserEmployment.is_current, Company.id, Company.company_name, Designation.id, Designation.designation_name'),
								// 'limit'=> $pageSize,
								// 'offset'=> $offsetVal,
								'order'=> 'UserEmployment.id'
							)
						);
		}
		return $userEmploymentList;
	}
}
?>