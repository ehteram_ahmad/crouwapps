<?php


App::uses('Model', 'Model');

/**
 * Represents model EnterpriseUserList
 */
class EnterpriseUserList extends Model {

	/*
	--------------------------------------------------------------------------------
	ON: 29-05-2017
	I/P:
	O/P:
	Desc: 
	--------------------------------------------------------------------------------
	*/
	public function getUserSubscriptionDetails($dataParams=array()){
		$returnData = array();
		//if(!empty($dataParams['email'])){
		$companyId = $dataParams['company_id'];
			$conditions = array("EnterpriseUserList.email"=> $dataParams['email'],"EnterpriseUserList.company_id"=> $companyId, "EnterpriseUserList.status"=> 1,"CompanyName.is_subscribed"=>1, "CompanyName.subscription_expiry_date >= "=> date('Y-m-d 23:59:59'));
			$returnData = $this->find('first', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'company_names',
										'alias' => 'CompanyName',
										'type' => 'left',
										'conditions'=> array('CompanyName.id = EnterpriseUserList.company_id')
									)
								),
								'fields'=> array("CompanyName.id","CompanyName.subscription_date","CompanyName.subscription_expiry_date"),
								'conditions' => $conditions,
							)
						);
		return $returnData;
	}
	
}