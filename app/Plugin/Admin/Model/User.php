<?php
/*
User model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');

class User extends AppModel {
	var $hasOne = array(
        'UserProfile' => array(
            'className' => 'UserProfile',
            'foreignKey' => 'user_id',
        ),
        'InstitutionInformation' => array(
            'className' => 'InstitutionInformation',
            'foreignKey' => 'user_id',
        )
    );

    public $validate = array(
		'email' => array(
              'rule' => 'email',
              'message' => 'Please provide a valid email address.'
         ),
        'password' => array(
            'rule' => array('minLength', '6'),
            'message' => 'Minimum 6 characters long'
        ),

    );

		// public $hasMany = array(
    //     'UserRoleTag' => array(
    //         'className' => 'UserRoleTag',
    //         'foreignKey' => 'user_id',
    //     ),
		//
    // );
public function beatViewDetails($id){
        $beatViewUser=array();
        $conditions=array('UserProfile.user_id'=>$id);
        $beatViewUser['BeatViewUser']=$this->find("first",array('conditions'=>$conditions));
        return $beatViewUser;
    }
  /*
  On: 27-08-2015
  I/P: $params = array()
  O/P: $params = array()
  Desc: return as array details of user depend upon search criteria.
  */
	public function userDetails( $params = array() ){
    $userData = array();
    $conditions = "";
    $limit = isset($params['size']) ? $params['size'] : ADMIN_PAGINATION;
    $conditions = "";
		if( !empty($params) ){
        if( isset($params['textFirstName']) && $params['textFirstName'] !=''){
            if(strlen($conditions) == 0){
              $conditions .= '  User.status IN (0,1) AND LOWER(UserProfile.first_name) LIKE LOWER("'.$params['textFirstName'].'%") ';
            }else{
              $conditions .= ' AND User.status IN (0,1) AND LOWER(UserProfile.first_name) LIKE LOWER("'.$params['textFirstName'].'%") ';
            }
        }
        if( isset($params['textLastName']) && $params['textLastName'] !=''){
            if(strlen($conditions) == 0){
              $conditions .= '  User.status IN (0,1) AND LOWER(UserProfile.last_name) LIKE LOWER("'.$params['textLastName'].'%") ';
            }else{
              $conditions .= ' AND User.status IN (0,1) AND LOWER(UserProfile.last_name) LIKE LOWER("'.$params['textLastName'].'%") ';
            }
        }
        if( isset($params['textEmail']) && $params['textEmail'] !=''){
            if(strlen($conditions) == 0){
              $conditions .= '  User.status IN (0,1) AND LOWER(User.email) LIKE LOWER("'.$params['textEmail'].'%") ';
            }else{
              $conditions .= ' AND User.status IN (0,1) AND LOWER(User.email) LIKE LOWER("'.$params['textEmail'].'%") ';
            }
        }
        if( isset($params['specilities']) && !empty($params['specilities'])){
            if(strlen($conditions) == 0){
              $conditions .= '  User.status IN (0,1) AND UserSpecility.specilities_id IN ("'.$params['specilities'].'")';
            }else{
              $conditions .= '  AND User.status IN (0,1) AND UserSpecility.specilities_id IN ("'.$params['specilities'].'")';
            }
        }
        if( isset($params['userApproved']) && $params['userApproved'] !=''){
            if(strlen($conditions) == 0){
              $conditions .= '  User.approved IN ('.$params['userApproved'].')';
            }else{
              $conditions .= '  AND User.approved IN ('.$params['userApproved'].')';
            }
        }

    }else{
      $conditions .=  " User.status IN (0,1)";
    }
    //** Get Fields
    $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date,
                      User.login_device, User.registration_date, User.status, User.approved,
                      UserProfile.first_name, UserProfile.last_name, UserProfile.country_id,
                      UserProfile.county, UserProfile.county, UserProfile.city, UserProfile.gmcnumber,
                      UserProfile.contact_no, UserProfile.address, UserProfile.dob, UserProfile.alias_name,
                      Country.country_name, Profession.profession_type");
    $userData = $this->find('all',
              array(
                'joins'=> array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                ),
                'fields'=> $fields,
                'conditions' => $conditions,
                'order'=> 'User.registration_date DESC',
                'group'=> array('User.id'),
                'limit'=> $limit
                )
            );
    return $userData;
	}

  /*
  On: 24-02-2016
  I/P: $params = array()
  O/P: $params = array()
  Desc: return as array details of user depend upon search criteria.
  */
  public function userDownload( $params = array() ){
    $userData = array();
    $conditions = "";
    if( !empty($params) ){
        if(isset($params['status']) && $params['status'] == 0){
          if(strlen($conditions) == 0){
              $conditions .= '  User.status = 0 ';
            }else{
              $conditions .= '  AND User.status = 0 ';
            }
        }
        if(isset($params['status']) && $params['status'] == 1){
          if(strlen($conditions) == 0){
              $conditions .= '  User.status = 0 ';
            }else{
              $conditions .= '  AND User.status = 0 ';
            }
        }
        if(isset($params['approved']) && $params['approved'] == 0){
          if(strlen($conditions) == 0){
              $conditions .= '  User.approved = 0 ';
            }else{
              $conditions .= '  AND User.approved = 0 ';
            }
        }
        if(isset($params['approved']) && $params['approved'] == 1){
          if(strlen($conditions) == 0){
              $conditions .= '  User.approved = 1 ';
            }else{
              $conditions .= '  AND User.approved = 1 ';
            }
        }
        if( isset($params['specilities']) && !empty($params['specilities'])){
            if(strlen($conditions) == 0){
              $conditions .= '  UserSpecility.specilities_id IN ('. implode(',', $params['specilities']) .')';
            }else{
              $conditions .= '  AND UserSpecility.specilities_id IN ('. implode(',', $params['specilities']) .')';
            }
        }

    }else{
      $conditions .=  " User.status IN (0,1)";
    }
    //** Get Fields
    $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date,
                      User.login_device, User.registration_date, User.status, User.approved,
                      UserProfile.first_name, UserProfile.last_name, UserProfile.country_id,
                      UserProfile.county, UserProfile.county, UserProfile.city, UserProfile.gmcnumber,
                      UserProfile.contact_no, UserProfile.address, UserProfile.dob, UserProfile.alias_name,
                      Country.country_name, Profession.profession_type");
    $userData = $this->find('all',
              array(
                'joins'=> array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                ),
                'fields'=> $fields,
                'conditions' => $conditions,
                'order'=> array('User.id'),
                'group'=> array('User.id'),
                //'limit'=> $limit
                )
            );
    return $userData;
  }

  /*
  On: 27-08-2015
  I/P: $params = array()
  O/P: $params = array()
  Desc: return as array details of user depend upon search criteria.
  */
  /*public function createQuery(){
    $session_data = array();
    $session_data = CakeSession::read('searchdata');


    $select = array();
    $select['users'] = array('distinct(users.id) as id','users.email','users.registration_date','users.status','users.approved');
    $select['user_profiles'] = array('user_profiles.first_name','user_profiles.last_name', 'user_profiles.country_id', 'user_profiles.county','user_profiles.city','user_profiles.contact_no','user_profiles.address','user_profiles.gender','user_profiles.alias_name', 'user_profiles.dob');
    $where = 'where users.id = user_profiles.user_id ';
    $order_by = " order by users.id desc";

    if(count($session_data) > 0){
        if( isset($session_data['firstname']) && $session_data['firstname'] !=''){
                $where .= ' AND LOWER(user_profiles.first_name) LIKE "%'.strtolower($session_data['firstname']).'%" ';
        }
        if( isset($session_data['lastname']) && $session_data['lastname'] !=''){
                $where .= ' AND LOWER(user_profiles.last_name) LIKE "%'.strtolower($session_data['lastname']).'%" ';
        }
        if( isset($session_data['email']) && $session_data['email'] !=''){
                $where .= ' AND LOWER(users.email) LIKE "%'.strtolower($session_data['email']).'%" ';
        }
        if( isset($session_data['interestlist']) && count($session_data['interestlist']) > 0){
               $select['user_interests'] = array();
               $where .= ' AND  users.id  = user_interests.user_id AND user_interests.status = 1  AND user_interests.interest_id IN ('.implode(",",$session_data['interestlist']).') ';

        }
        if( isset($session_data['status']) && $session_data['status'] !=''){
                if($session_data['status'] == 1){
                    $where .= ' AND  users.status ='.$session_data['status'].' AND users.approved = 1';
                }
                if($session_data['status'] == 0){
                    $where .= ' AND  users.status ='.$session_data['status'];
                }
                if($session_data['status'] == 4 || $session_data['status'] == 5){

                    $select['user_post_beats'] = array('count(user_post_beats.id) as beatcount');
                    $where .= ' AND  users.id  = user_post_beats.user_id group by users.id AND users.status = 1 ';

                    if($session_data['status'] == 4){
                      $order_by = " order by beatcount desc";
                    }else{
                      $order_by = " order by beatcount asc";
                    }
                }
        }
    }
    $fieldlist = "";
    $tables = "";
    foreach($select as $table => $fields){
       if($tables == ""){
          $tables .=  $table;
       }else{
          $tables .=  ','.$table;
       }
       foreach($fields as $field){
            if($fieldlist == ""){
                $fieldlist .= $field;
            }else{
                $fieldlist .= ','.$field;
            }
       }
    }
    return 'SELECT '.$fieldlist.' FROM '.$tables.' '.$where.' '.$order_by;

    //return $select.' '.$where.' '.$order_by;
  }
  public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $sql =  $this->createQuery();

        //echo $sql;

        $recursive = -1;
        // Mandatory to have
        $this->useTable = false;
        // Adding LIMIT Clause
        $sql .= ' LIMIT '.(($page - 1) * $limit) . ', ' . $limit;

        //echo $sql;
        $results = $this->query($sql);
        return $results;
    }
public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {

        $sql = '';

        $sql .= $this->createQuery();

        $this->recursive = $recursive;

        $results = $this->query($sql);

        return count($results);
    }*/

public function getProfessionCount($sortPost = array()){
  if(!isset($sortPost['to']) || $sortPost['to']==""){
    $endDate = date("Y-m-d");
  }else{
    $endDate = date('Y-m-d', strtotime($sortPost['to']));
  }

  if(!isset($sortPost['from']) || $sortPost['from']==""){
    $date = array();
  }else{
    $startDate = date('Y-m-d', strtotime($sortPost['from']));
    $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
  }

  $conditions = array('User.status' => 1,'User.approved' => 1);
  $company=array('UserEmployment.company_id' => $sortPost['company'],'UserEmployment.is_current' => 1);

  $conditions=array_merge($conditions,$date,$company);


  $fields = array("User.email,User.registration_date,Profession.profession_type,Profession.id,UserEmployment.company_id");
  $options  =
      array(
      'joins'=>array(
          array(
              'table' => 'professions',
              'alias' => 'Profession',
              'type' => 'left',
              'conditions'=> array('UserProfile.profession_id = Profession.id')
          ),
          array(
              'table' => 'user_specilities',
              'alias' => 'UserSpecility',
              'type' => 'left',
              'conditions'=> array('User.id = UserSpecility.user_id')
          ),
          array(
              'table' => 'user_employments',
              'alias' => 'UserEmployment',
              'type' => 'left',
              'conditions'=> array('User.id = UserEmployment.user_id')
          ),
          array(
              'table' => 'enterprise_user_lists',
              'alias' => 'EnterpriseUserList',
              'type' => 'left',
              'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
          ),
      ),
      'fields'=> $fields,
      'conditions'=> $conditions,
      'group'=> array('User.id'),
      'limit'=>'1000000000',
  );
}

}
?>
