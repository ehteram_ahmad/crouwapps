<?php
/*
Add, update, Validate user posts.
*/
App::uses('AppModel', 'Model');

class UserPost extends AppModel {
	public $hasOne = array(
        'UserProfile' => array(
            'className'     => 'UserProfile',
            'foreignKey'    => false,
            'conditions' => array('UserProfile.user_id = UserPost.user_id')
    ));
	public $hasMany = array(
        'UserPostAttribute' => array(
            'className' => 'UserPostAttribute',
            'conditions' => array('UserPostAttribute.status' => 1),
        ),
        'UserPostSpecilities' => array(
            'className' => 'UserPostSpecilities',
        ),
        'UserPostComment' => array(
            'className' => 'UserPostComment',
        		'conditions' => array('UserPostComment.status' => 1),
        ),
        'BeatView' => array(
            'className' => 'BeatView',
            'foreignKey' => 'post_id',
            'conditions' => array('BeatView.status' => '1'),
            // 'order' => 'BeatView.created DESC',
            // 'limit' => '5',
            'dependent' => true
        ),
        'UserPostSpam' => array(
            'className' => 'UserPostSpam',
            'foreignKey' => 'user_post_id',
            'conditions' => array('UserPostSpam.spam_confirmed'=>'1'),
            // 'order' => 'BeatView.created DESC',
            // 'limit' => '5',
            'dependent' => true
        ),
        'PostUserTag' => array(
            'className' => 'PostUserTag',
            'foreignKey' => 'post_id',
                'conditions' => array('PostUserTag.status' => 1),
        ),
        'BeatHashMapping' => array(
            'className' => 'BeatHashMapping',
            'foreignKey' => 'post_id',
                'conditions' => array('BeatHashMapping.status' => 1),
        ),

    );

    /*
    On:
    I/P:
    O/P:
    Desc:
    */
	public function userPostLists( $postId = NULL ){
        $userPostData = array();
        if( !empty($postId) ){
            $conditions = array('UserPost.id'=> $postId);
            $userPostData = $this->find("all", array('conditions'=> $conditions));
        }else{
            $userPostData = $this->find("all");
        }
        return $userPostData;
    }
    // public function beatViewDetails($id){
    //     $beatViewUser=array();
    //     $conditions=array('UserPost.id'=>2,'UserProfile.user_id'=>123);
    //     $beatViewUser=$this->find("all",array('conditions'=>$conditions));
    //     pr($id);
    //     pr($beatViewUser);
    //     exit();
    //     return $beatViewUser;
    //     // pr($id);
    //     // exit();

    // }


}
?>
