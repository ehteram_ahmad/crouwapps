<?php
//** AJAX Pagination
$this->Js->JqueryEngine->jQueryObject = 'jQuery';
// Paginator options
$this->Paginator->options(array(
    'update' => '#content',
    'evalScripts' => true,
    'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
    'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
    )
);
    
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
div.radio input{
    opacity: 1!important;
}
    img#icon,img#icon1,img#icon2{
        border:1px solid black;
        width:100px!important;
        height:100px!important;
    }
</style>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Specialties Search</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Specialties</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Specialties List</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>--> Specialties Search </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="" class="remove"> </a>
                            <a href="" class="reload"> </a>-->
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal" role="form" onsubmit="return search();" id="formSearch">
                            <div class="form-body width90Auto">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control input-sm" placeholder="Speciality Name" id="txtName">
                                    </div>
                                    <!-- <div class="col-md-4">
                                        <select class="form-control" name="options2" id="multi-prepend">
                                            <option value="" disabled="" selected=""> --Select Status-- </option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div> -->
                                </div>
                                
                                
                                
                            </div>
                            <div class="form-actions center-block txt-center">
                                <button type="submit" class="btn green2">Search</button>
                                <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>-->Specialties List </div>
                        <div class="tools">
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                    <div class="mainAction marBottom10">
                    <div class="leftFloat">
                    <div class="actions">
                            <div class="btn-group">
                                <?php 
                                $filterOptions=array(
                                    "1"=>"Active",
                                    "0"=>"Inactive"
                                    );
                                echo $this->Form->select('selectStatus',$filterOptions,array('type'=>'select','id'=>'selectStatus','class'=>'btn btn-md green2','onchange'=>'return search();','aria-expanded'=>false,'empty' => ' --Filter-- ')); ?>
                            </div>
                        </div>
                        </div>
                        <div class="rightFloat">
                        <div class="actions marRight10">
                            <div class="btn-group">
                                <a class="btn btn-sm green2 dropdown-toggle" href="#todo-members-modal" data-toggle="modal"> <i class="fa fa-plus"></i> Add New</a>
                            </div>
                        </div>
                        <!-- <div class="actions">
                            <div class="btn-group">
                                <a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-external-link"></i> Export </a>
                            </div>
                        </div> -->
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <span id="content">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <!-- <th class="width40px">&nbsp;  </th> -->
                                        <!-- <th class="width60px"> Sr. No. </th> -->
                                        <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> Speciality Name </a></th>
                                        <th class="txtCenter smallDeviceIconHeight"> Speciality Icon </th>
                                        <th class="txtCenter width90px"> Status </th>
                                        <th class="txtCenter width90px"> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                // pr($specilities);
                                    if(count($specilities) > 0){
                                    for ($i=0; $i < count($specilities) ; $i++) { 
                                 ?>
                                    <tr>
                                       <!-- <td><?php //echo $i+1; ?> </td>-->
                                        <td><?php echo $specilities[$i]['Specilities']['name']; ?></td>
                                        <td class="txtCenter">
                                        <span onclick="viewImg('<?php echo BASE_URL . 'img/specilities/'.$specilities[$i]['Specilities']['img_name']; ?>');">
                                            <img id="icon1" src="<?php echo BASE_URL . 'img/specilities/'.$specilities[$i]['Specilities']['img_name']; ?>" alt="No Image To Display" />
                                        </span>
                                        </td>
                                        <td class="txtCenter">
                                                <?php 
                                                 if($specilities[$i]['Specilities']['status'] == 1){
                                                ?>
                                                    <button type="button" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                                <?php }else{ ?>
                                                
                                                    <button type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                                                <?php } ?>
                                        </td>
                                        <td class="txtCenter">
                                        <span id="statusButton<?php echo $specilities[$i]['Specilities']['id']; ?>">
                                               <?php 
                                                if($specilities[$i]['Specilities']['status'] == 1){
                                               ?>
                                               
                                                   <button type="button" onclick="updateSpecilityStatus(<?php echo $specilities[$i]['Specilities']['id']; ?>, 'Active');" data-original-title="Inactivate" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                               <?php }else{ ?>
                                               
                                                   <button onclick="updateSpecilityStatus(<?php echo $specilities[$i]['Specilities']['id']; ?>, 'Inactive');" type="button" data-original-title="Activate" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                                               <?php } ?>
                                           </span>
                                            <a class="btn btn-circle btn-icon-only btn-info2 tooltips editModal" href="#todo-members-modal1" data-original-title="Edit" id="<?php echo $specilities[$i]['Specilities']['id']; ?>" data-toggle="modal"><i class="fa fa-pencil-square-o"></i></a>
                                            <button onclick="deleteSpec(<?php echo $specilities[$i]['Specilities']['id'].','."'".$specilities[$i]['Specilities']['name']."'"; ?>);" type="button" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    <?php }}else{ ?>
                                        <tr>
                                            <td colspan="4" align="center"><font color="red">No Record(s) Found!</font></td>
                                          </tr> 
                                      <?php  } ?>
                                </tbody>
                            </table>
                        <div class="row">
                        
                            <div class="col-md-12 col-sm-12">
                                <?php echo $this->element('pagination'); ?>
                            </div>
                        </div>
                        </span>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <div id="todo-members-modal" class="modal fade" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="    true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Add Speciality</h4>
                    </div>
                        <?php echo $this->Form->create('addSpeciality',array('class'=>'form-horizontal','type'=>'file')); ?>
                    <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">Speciality Name</label>
                                <div class="col-md-8">
                                    <div class="col-md-9">
                                        <?php echo $this->Form->text('name',array('class'=>'form-control','placeholder'=>'Enter Speciality Name','id'=>'spclName')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Speciality Icon</label>
                                <div class="col-md-8">
                                    <span id="icon" class="btn btn-lg success btn-file">
                                        <img id="icon" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                        <?php echo $this->Form->file('icon',array('id'=>'spclIcon','class'=>'form-control','onchange'=>'readURL(this,"icon");')); ?>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="col-md-9">
                                        <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="data[addSpeciality][status]" id="optionsRadios4" value="1" checked> Active </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="data[addSpeciality][status]" id="optionsRadios5" value="0"> Inactive </label>
                                    </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn green2" data-dismiss="modal" onclick="addSpeciality();">Add</button> -->
                        <button class="btn green2" onclick="validate();" type="submit">Add</button>
                        <button class="btn default" data-dismiss="modal" aria-hidden="true" onclick="rstSpeciality();">Cancel</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>
        <div id="todo-members-modal1" class="modal fade" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
            <div class="modal-dialog" id="editSpecialityCon">
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<a id="dd" class="btn btn-primary hidden" data-toggle="modal" href='#modal-id'>Trigger modal</a>
<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content text-center">
                    <img id="demo" src="" alt=" No Image To Display " style="width:100%">
        </div>
    </div>
</div>
<div id="mainView">
</div>
<script type="text/javascript">
function validate(){
    $(".loader").fadeIn("fast");
    name=$('#spclName').val();
    img=$('#spclIcon').val();
    if(name ==""){
        $(".loader").fadeOut("fast");
        alert("Please Enter Full details.");
       $('#spclName').focus(); 
        return false;
    }
    if(img == ""){
        $(".loader").fadeOut("fast");
         alert("Please Enter Full details.");
        $('#spclName').focus(); 
         return false;
    }
    $(".loader").fadeOut("fast");
}
function deleteSpec(specId,title){
    var conf=confirm("Do you want to delete Speciality : "+title);
    if(conf==true){
        $(".loader").fadeIn("fast");
    $.ajax({
             url: '<?php echo BASE_URL; ?>admin/Specility/deleteSpec',
             type: "POST",
             data: {'specId': specId},
             success: function(data) { 
                alert(data);
                location.reload();
            }
        });
  return false; 
}
else{}
}
function viewImg(val){
    $('#demo').attr('src',val);
    $('#dd').click();
}
$(document).on("click", ".editModal", function () {
    $(".loader").fadeIn("fast");
     var id = $(this).attr('id');
     $.ajax({
             url: '<?php echo BASE_URL; ?>admin/Specility/allSpecilities',
             type: "POST",
             data: {'spclId':id},
             success: function(data) { 
                $('#editSpecialityCon').html(data);
                $(".loader").fadeOut("fast");
            }
        });
  return false; 
});
function rstSpeciality(){
$('#spclName').val("");
$('#spclIcon').val("");
// $('#uniform-optionsRadios4 span').addClass('checked');
// $('#uniform-optionsRadios5 span').removeClass('checked');
// alert();
}
function sort(val,limit){
    $(".loader").fadeIn("fast");
    var a=$('#'+val).attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='DE';
    }
    else{
        var order='A';
    }
$.ajax({
             url: '<?php echo BASE_URL; ?>admin/Specility/specialityFilterData',
             type: "POST",
             data: {'sort':val,'order':order,'textName': $('#txtName').val(), 'status': $('#selectStatus').val(),'limit':limit},
             success: function(data) { 
                // alert(data);
                $('#content').html(data);
                $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                if(a==b){
                $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                }
                else{
                $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                }
                $(".loader").fadeOut("fast");
            }
        });
  return false; 
}
function search(){
    $(".loader").fadeIn("fast");
 $.ajax({
         url: '<?php echo BASE_URL; ?>admin/Specility/specialityFilterData',
         type: "POST",
         data: {'textName': $('#txtName').val(), 'status': $('#selectStatus').val()},
         success: function(data) { 
            // alert(data);
            $('#content').html(data);
            $(".loader").fadeOut("fast");
        }
    });
return false;  
}
function chngPage(val,limit) {
    $(".loader").fadeIn("fast");
var a=$('tr th.active').attr('id');
var b=$('tr th.ASC').attr('id');
if(a==b){
    var order='A';
}
else{
    var order='DE';
}
var goVal=$("#chngPage").val();
if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
$.ajax({
          // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
          url:'<?php echo BASE_URL; ?>admin/Specility/specialityFilterData',
          type: "POST",
          data: {'textName': $('#txtName').val(), 'status': $('#selectStatus').val(),'j':goVal,'limit':limit,'sort':a,'order':order},
          success: function(data) { 
              $('#content').html(data);
                $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                if(a==b){
                $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                }
                else{
                $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                }
                $(".loader").fadeOut("fast");
          }
      });
}}

function chngCount(val){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/Specility/specialityFilterData',
              type: "POST",
              data: {'textName': $('#txtName').val(), 'status': $('#selectStatus').val(),'limit':val,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

function abc(val,limit){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/Specility/specialityFilterData',
           type: "POST",
           data: {'textName': $('#txtName').val(), 'status': $('#selectStatus').val(),'j':val,'limit':limit,'sort':a,'order':order},
           success: function(data) { 
                $('#content').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
       });
}
$("#menu>ul>li").eq(3).addClass('active open');
$(".sub-menu>li").eq(7).addClass('active open');
</script>