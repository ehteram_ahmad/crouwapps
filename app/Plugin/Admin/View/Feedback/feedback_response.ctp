<?php
// pr($feedbackLists);
// exit();
    echo $this->Html->script(array('Admin.ckeditor/ckeditor.js'));   
?>
<!-- BEGIN CONTENT -->
<div id="content" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Feedback Response</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/feedback/feedbackList';?>">User Feedback List</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Feedback Response</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            Feedback Response </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('sendmail',array('class'=>'form-horizontal','onsubmit'=>'return validatefeed();')); ?>
                        <div class="form-body width100Auto">
                            <div class="form-group">
                                <label class="control-label col-md-2">TO : </label>
                                <div class="col-md-9">
                                  <?php echo $this->Form->text('to',array('class'=>'form-control','id'=>'mailto','value'=>$feedbackLists['User']['email'],'disabled')); ?>
                                  <?php echo $this->Form->text('to',array('class'=>'form-control hidden','id'=>'mailto','value'=>$feedbackLists['User']['email'],)); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Subject : </label>
                                <div class="col-md-9">
                                    <?php echo $this->Form->text('subject',array('class'=>'form-control','id'=>'mailSub')); ?>
                                    <?php echo $this->Form->text('source',array('class'=>'hidden','value'=>$feedbackLists['UserFeedback']['feedback_source'])); ?>
                                    <?php echo $this->Form->text('id',array('class'=>'hidden','value'=>$feedbackLists['UserFeedback']['id'])); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Content : </label>
                                <div class="col-md-9 disableFullScreen">
                                     <?php echo $this->Form->textarea('content', array('class'=>'form-control ckeditor','rows'=>'10','id'=>'textDescrip')); ?>
                                </div>
                            </div>
                            <?php if ($feedbackLists['UserFeedback']['feedback_source']=="MB") {
                                    $type="medicbleep_";
                                    }
                                    else{
                                        $type="";
                                    } ?>
                        </div>
                        <div class="form-actions center-block txt-center">
                            <button type="submit" id="sendBtn" class="btn green2"> Send </button>
                            <a class="btn btn-info" href="#todo-members-modal" data-toggle="modal" onclick="preview();"> Preview </a>
                            <a class="btn btn-default pull-right" href="<?php echo BASE_URL . 'admin/feedback/feedbackList';?>"> Cancel </a>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<div id="todo-members-modal" class="modal fade bs-modal-lg" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 id="toTxt" class="text-center">To:</h4>
                        <b><h3 id="titleTxt" class="text-center"></h3></b>
                        <div class="col-md-12">
                                <?php echo $this->element($type.'email_template_header'); ?>
                            </div>
                    </div>
                    <div class="modal-body">
                        <div id="con"></div>
                    </div>
                    <div class="modal-footer">
                    <div class="col-md-12">
                                <?php echo $this->element($type.'email_template_footer'); ?>
                            </div>
                        <button onclick="sendPrv();" class="btn green2 pull-right" data-dismiss="modal"> Send </button>
                        <button class="btn default" data-dismiss="modal" aria-hidden="true"> <span> << </span> Cancel </button>
                    </div>
                </div>
            </div>
        </div>
<!-- END CONTENT -->
<!-- Form validation[START] -->
<script type="text/javascript">
function sendPrv(){
    $('#sendBtn').click();
}
function preview(){
$('#toTxt').html('To: '+$('#mailto').val());
$('#titleTxt').html($('#mainTitle').html());
$('#titleTxt').html($('#mailSub').val());
$('#con').html(CKEDITOR.instances.textDescrip.getData());
}
 function validatefeed(){
    if($('#mailSub').val() == ''){
        alert("Please enter the subject.");
        $('#mailSub').focus();
        return false;
    }
    if(CKEDITOR.instances.textDescrip.getData() == ''){
        alert("Please enter the content.");
        CKEDITOR.instances.textDescrip.focus();
        return false;
    }
    return true;
 }
 
 $("#menu>ul>li").eq(7).addClass('active open');
</script>
<!-- Form validation[END] -->
