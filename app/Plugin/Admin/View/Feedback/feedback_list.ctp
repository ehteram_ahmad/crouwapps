<?php
// pr(count($feedResp[1]));
// exit();
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
  #subCon{
    width:20%;
    cursor:pointer;"
  }
  #subCon:hover{
    background: whitesmoke;
    text-decoration: underline;
    background-color:#0d93c2;
    color: white;
  }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>User Feedback List</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
             <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/feedback/feedbackList';?>">User Feedback</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User Feedback List</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>-->User Feedback List </div>
                        <div class="tools">
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a onclick="location.reload();" href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div id="content" class="portlet-body flip-scroll">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content">
                                <tr>
                                    <!-- <th class="width40px">&nbsp;  </th> -->
                                    <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> User Name </a></th>
                                    <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email ID </a></th>
                                    <th id="Subject" class="DESC" onclick="sort('Subject',<?php echo $limit; ?>);"><a> Subject </a></th>
                                    <th id="Source" class="DESC width90px" onclick="sort('Source',<?php echo $limit; ?>);"><a> Source </a></th>
                                    <th id="Date" class="DESC width140px" onclick="sort('Date',<?php echo $limit; ?>);"><a> Date </a></th>
                                    <th id="Status" class="DESC width140px"> Status </th>
                                    <th id="Action" class="DESC width140px"> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if(count($feedbackLists) > 0){
                                for ($i=0; $i < count($feedbackLists); $i++) { 
                            ?>
                                <tr id="feedBack<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>">
                                    <td style="width:20%" > <?php echo $feedbackLists[$i]['UserProfile']['first_name']." ".$feedbackLists[$i]['UserProfile']['last_name'];?> </td>
                                    <td id="to<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>" style="width:30%"> <?php echo $feedbackLists[$i]['User']['email'];?> </td>
                                    <td  id="subCon" onclick="showFeed(<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>);" data-toggle="modal" href='#modal-id' > <?php echo $feedbackLists[$i]['UserFeedback']['subject'];?> </td>
                                    <td id="con<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>" class="txtCenter hidden"> <?php echo $feedbackLists[$i]['UserFeedback']['feedback'];?> </td>
                                    <td style="width:10%"  class="txtCenter"> <?php echo $feedbackLists[$i]['UserFeedback']['feedback_source'];?> </td>
                                    <td style="width:10%" class="beatBtnBottom4 txtCenter">
                                      <?php 
                                       $datetime = explode(" ",$feedbackLists[$i]['UserFeedback']['created']);
                                       $date = $datetime[0];
                                       $time = $datetime[1];
                                       echo date('d-m-Y',strtotime($date))."  ".date ('g:i:s A',strtotime($time));
                                       ?>
                                     </td>
                                    <td style="width:5%" class="beatBtnBottom4 txtCenter">
                                      <?php 
                                          if(count($feedResp[$i]) > 0){
                                       ?>
                                      <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
                                      <?php }else{ ?>
                                      <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="No Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
                                      <?php } ?>
                                    </td>
                                    <td style="width:5%" class="beatBtnBottom4 txtCenter">
                                      <button onclick="javascript:window.location.href='feedbackResponse/<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>'" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Send Mail" type="button"><i class="fa fa-envelope-o"></i></button>
                                    </td>
                                </tr>
                                 <?php } }else{?> 

                                  <tr>
                                        <td colspan="7"><h1 align="center">No Record found</h1></td>
                                  </tr> 
                                  <?php } ?>   
                                
                            </tbody>
                        </table>
                        <div class="actions marBottom10">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                        <?php echo $this->element('pagination'); ?>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<div class="modal fade" id="modal-id">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title text-center">Feedback Content</h4>
      </div>
      <div class="modal-body"><p id="feedCon">
        
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function sort(val,limit){
  $(".loader").fadeIn("fast");
    var a=$('#'+val).attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='DE';
    }
    else{
        var order='A';
    }
$.ajax({
             url: '<?php echo BASE_URL; ?>admin/feedback/feedbackFilter',
             type: "POST",
             data: {'sort':val,'order':order,'limit':limit},
             success: function(data) { 
                $('#content').html(data);
                $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                if(a==b){
                $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                }
                else{
                $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                }
                $(".loader").fadeOut("fast");
            }
        });
  return false; 
}
 function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $(".loader").fadeIn("fast");
    $.ajax({
              url:'<?php echo BASE_URL; ?>admin/feedback/feedbackFilter',
              type: "POST",
              data: {'j':goVal,'limit':limit,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
              }
          });
  }
  else{
  }
  
 }
 function chngCount(val){
  $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              url:'<?php echo BASE_URL; ?>admin/feedback/feedbackFilter',
              type: "POST",
              data: {'limit':val,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

function abc(val,limit){
  $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/feedback/feedbackFilter',
           type: "POST",
           data: {'j':val,'limit':limit,'sort':a,'order':order},
           success: function(data) { 
                $('#content').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
       });
}
function showFeed(val){
$('#feedCon').html($('td#con'+val).html());
}
function mailSnd(val){
$('#mailto').val($('td#to'+val).html());
}
$("#menu>ul>li").eq(7).addClass('active open');
$(".sub-menu>li").eq(11).addClass('active open');

</script>