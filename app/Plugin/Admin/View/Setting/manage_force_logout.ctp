<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Force Logout Settings</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/user/userLists';?>">Users</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Force Logout Settings</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>-->Force Logout Settings </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        <!-- </div> -->
                        <div class="clearfix"></div>
                        <div id="content" class="paddingSide15">
                                <table class="table table-bordered table-striped table-condensed flip-content">
                                    <thead class="flip-content">
                                        <tr>
                                            <th id="first_name">Force Logout Status </a></th>
                                            <th> Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                <tr class="even pointer" >
                                    <td><?php echo ($forceLogoutVal['ForceLogoutSetting']['value']==1) ? 'ON':'OFF'; ?></td>
                                    <td><span id="forceLogoutVal"><?php echo ($forceLogoutVal['ForceLogoutSetting']['value']==1) ? 'OFF':'ON'; ?></td>
                                 </tr>   
                                    </tbody>
                                </table>


                                <div class="actions marBottom10 paddingSide15">
                                
                            </div>
                     </div>   
                </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
    </div>
    </div>
    </div>
<script>
$("#forceLogoutVal").click(function(){
    $.ajax({
          url:'<?php echo BASE_URL; ?>admin/setting/manageForceLogout',
          type: "POST",
          data: {'forceLogoutVal': $("#forceLogoutVal").html()},
          success: function(data) { 
              alert('Value Updated');
              location.reload(true);
          }
    });
});
</script>