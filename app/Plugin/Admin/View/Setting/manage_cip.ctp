<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>CIP Settings</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/user/userLists';?>">Users</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">CIP Settings</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>-->CIP Settings </div>
                        <div class="tools">
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="remove"> </a>
                            <a href="javascript:;" class="reload"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        <!-- </div> -->
                        <div class="clearfix"></div>
                        <div id="content" class="paddingSide15">
                                <table class="table table-bordered table-striped table-condensed flip-content">
                                    <thead class="flip-content">
                                        <tr>
                                            <th id="first_name">CIP ON/OFF Status </a></th>
                                            <th> Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                <tr class="even pointer" >
                                    <td><?php echo ($cipOnOffval['CipOnOff']['value']==1) ? 'ON':'OFF'; ?></td>
                                    <td><span id="cipOnOffVal"><?php echo ($cipOnOffval['CipOnOff']['value']==1) ? 'OFF':'ON'; ?></td>
                                 </tr>   
                                    </tbody>
                                </table>


                                <div class="actions marBottom10 paddingSide15">
                                
                            </div>
                     </div>   
                </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
    </div>
    </div>
    </div>
<script>
$("#cipOnOffVal").click(function(){
    $.ajax({
          url:'<?php echo BASE_URL; ?>admin/setting/manageCip',
          type: "POST",
          data: {'onOffVal': $("#cipOnOffVal").html()},
          success: function(data) { 
              alert('Value Updated');
              location.reload(true);
          }
    });
});
</script>