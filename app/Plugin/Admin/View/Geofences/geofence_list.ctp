<?php
//** AJAX Pagination
$this->Js->JqueryEngine->jQueryObject = 'jQuery';
// Paginator options
$this->Paginator->options(array(
    'update' => '#content',
    'evalScripts' => true,
    'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
    'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
    )
);

?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Geofence List</h1>
                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li><a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Geofence List</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>--> Search </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="" class="remove"> </a>
                            <a href="" class="reload"> </a>-->
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal" role="form" onsubmit="return search();" id="formSearch">
                            <div class="form-body width90Auto">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control input-sm" placeholder="Company Name" id="txtName">
                                    </div>
                                    <!-- <div class="col-md-4">
                                        <select class="form-control" name="options2" id="multi-prepend">
                                            <option value="" disabled="" selected=""> --Select Status-- </option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div> -->
                                </div>
                                
                                
                                
                            </div>
                            <div class="form-actions center-block txt-center">
                                <button type="submit" class="btn green2">Search</button>
                                <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="portlet box blue2">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="fa fa-cogs"></i>-->Geofence List </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                        <div class="mainAction marBottom10">
                        <div class="leftFloat">
                         <div class="actions">
                                
                         </div>
                            </div>
                            <div class="rightFloat">
                            <div class="actions marRight10">
                            <div class="btn-group">
                                    <a class="btn btn-sm green2 dropdown-toggle" href="<?php echo BASE_URL . 'admin/geofences/addGeofenceData/'; ?>"  aria-expanded="false"> <i class="fa fa-plus"></i> Add Geofence</a>
                                </div>
                            </div>
                            <div class="actions">
                                <div class="btn-group">
                                </div>
                            </div>
                            </div>
                            </div>
                            
                            <!-- </div> -->
                            <div class="clearfix"></div>
                            <div id="content" class="paddingSide15">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <!-- <th><input type="checkbox" id="selectAll"></th> -->
                                                <th id="first_name" class="DESC" ><a> Company Name </a></th>
                                                <th id="last_name" class="DESC" o><a> Company Address </a></th>
                                                <th id="email" class="DESC" ><a> Latitude </a></th>
                                                <th id="country" class="DESC" ><a> Longitude </a></th>
                                                <th id="profession" class="DESC"><a> Radius </a></th>
                                                <th id="joining_date" class="DESC"><a> Status </a></th>
                                                <th id="joining_date" class="DESC"><a> Created </a></th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            // echo "<pre>"; print_r($userData);die;
                                    if(count($geofenceListData) > 0){
                                    foreach( $geofenceListData as $geoData){ 
                                    ?>
                                    <tr class="even pointer">
                                        <td style="width:15%;" class=" "><?php echo $geoData['CompanyName']['company_name'];?></td>
                                        <td style="width:15%;" class=" "><?php echo $geoData['CompanyGeofenceParameter']['company_address'];?> </td>
                                        <td style="width:25%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['latitude'];?> </td>
                                        <td style="width:14%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['longitude'];?> </td>
                                        <td style="width:15%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['radius'];?> </td>
                                        <td style="width:10%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['status'];?></td>
                                        <td style="width:10%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['created'];?></td>
                                        <td style="width:1%;" class="last"> 
                                          <button data-toggle="modal"  onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/geofences/editGeofenceData/' . $geoData['CompanyGeofenceParameter']['company_id'] ; ?>'; return false;"  type="button" data-original-title="Edit" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-pencil-square-o"></i></button>
                                        </td>
                                        
                                        </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                      </tr> 
                                      <?php } ?> 
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <?php echo $this->element('pagination'); ?>
                                        </div>
                                    </div>
                         </div>   
                    </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </div>
</div>
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?> 
<!-- BEGIN DELETE POP-UP --> 
 <div id="deleteModal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
   <div class="modal-dialog">
     <div class="modal-content"> 
       <div class="modal-header"> 
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Reason of Deletion</h4>
       </div>
       <div class="modal-body">
         <div class="form-group">
             <label class="control-label">Enter Reason below</label>
             <textarea id="deleteComment" name="deleteComment" class="form-control" rows="3" placeholder="comments"></textarea>
         </div>
       </div>
       <div class="modal-footer textCenter">
         <button id="delUser" value="" onclick="deleteUser();" type="button" data-dismiss="modal" class="btn green2"><i class="fa fa-times" aria-hidden="true"></i>  Delete </button>
         <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
       </div>
   </div>
  </div>
 </div>
<!-- END DELETE POP-UP --> 
<div id="cn-overlay" class="cn-overlay"></div>
<script>
$(document).ready(function() {
    // $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".datePick").datepicker({
    dateFormat: 'yy-mm-dd'
});
 
});

function search(){
    $(".loader").fadeIn("fast");
 $.ajax({
         url: '<?php echo BASE_URL; ?>admin/geofences/geofenceFilter',
         type: "POST",
         data: {'companyAddress': $('#txtName').val()},
         success: function(data) { 
            // alert(data);
            $('#content').html(data);
            $(".loader").fadeOut("fast");
        }
    });
return false;  
}

function sort(val,limit){
    $(".loader").fadeIn("fast");
        var a=$('#'+val).attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='DE';
        }
        else{
            var order='A';
        }
    $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/allFilter',
                 type: "POST",
                 data: {'sort':val,'order':order,'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'limit':limit,'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'country':$('#countrySearch').val(),'profession':$('#profession').val()},
                 success: function(data) { 
                    // alert(data);
                    $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    else{
                    $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
      return false; 
    }
function searchUser(){
    $(".loader").fadeIn("fast");
     $.ajax({
             url: '<?php echo BASE_URL; ?>admin/user/allFilter',
             type: "POST",
             data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'country':$('#countrySearch').val(),'profession':$('#profession').val()},
             success: function(data) { 
                $('#content').html(data);
                $(".loader").fadeOut("fast");
            }
        });
  return false;  
}

//** Give  Access
function addLiveStreamAccess(){
    userId=$('#accessLiveStream').val();
    if(confirm("Are you sure you want to remove access?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/addLiveStreamAccess',
                 type: "POST",
                 data: {'userId': userId},
                 success: function(data) { 
                    alert(data);
                    location.reload(true);
                }
            });
    }
    
  return false; 
}

//** Remove Access
function removeLiveStreamAccess(){
    userId=$('#accessRemoveLiveStream').val();
    if(confirm("Are you sure you want to add access?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/removeLiveStreamAccess',
                 type: "POST",
                 data: {'userId': userId},
                 success: function(data) { 
                    alert(data);
                    location.reload(true);
                }
            });
    }
    
  return false; 
}
function deleteUser(){
    userId=$('#delUser').val();
    if($.trim($('#deleteComment').val())==""){
        alert("Please Eneter The Reason.");
        $('#deleteComment').focus();
        return false;
    }
    if(confirm("Are you sure you want to delete this User?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/deleteUser',
                 type: "POST",
                 data: {'userId': userId,'reason':$('#deleteComment').val()},
                 success: function(data) { 
                    alert(data);
                    location.reload(true);
                }
            });
    }
    $('#deleteComment').val('');
  return false; 
}
function xyz(){
var FirstName=$('#textFirstName').val();
var LastName=$('#textLastName').val();
var Email=$('#textEmail').val();
var specilities=$('#multi-prepend').val();
var country=$('#countrySearch').val();
var profession=$('#profession').val();
var statusVal=$('#selectStatus').val();
var from = $('#dateFrom').val();
var to = $('#dateTo').val();
if(specilities==null)
{
    specilities="";
}
// alert(specilities);
window.open('<?php echo BASE_URL; ?>admin/user/userCsvDownload/?FirstName='+FirstName+'&LastName='+LastName+'&specilities='+specilities+'&statusVal='+statusVal+'&Email='+Email+'&from='+from+'&to='+to+'&country='+country+'&profession='+profession, '_blank');
}

 function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $(".loader").fadeIn("fast");
    $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/geofences/geofenceFilter',
              type: "POST",
              data: {'companyAddress': $('#txtName').val(), 'j':goVal,'limit':limit,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 function chngCount(val){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              //"<?php echo BASE_URL;?>admin/geofences/geofenceList";
              // url:'<?php //echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/geofences/geofenceFilter',
              type: "POST",
              data: {'companyAddress': $('#company_address').val(),'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

function abc(val,limit){
  alert("HI");
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
    var goVal=val;
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/geofences/geofenceFilter',
           type: "POST",
           data: {'companyAddress': $('#company_address').val(), 'j':goVal,'limit':limit,'sort':a,'order':order},
           success: function(data) { 
                $('#content').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
       });
}
$("#menu>ul>li").eq(1).addClass('active open');
$(".sub-menu>li").eq(0).addClass('active open');
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script> 


