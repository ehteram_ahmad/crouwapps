<?php
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Edit Geofence</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Edit Geofence</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            Edit Geofencing Data </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo $this->Form->create('geofence',array('action' => 'updateGeofenceData', 'class'=>'form-horizontal','onsubmit'=>'return editGeofence();')); ?>
                            <div class="form-body width85Auto">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Company Name</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                       <?php  echo $this->Form->hidden('company_id', ['value'=>$geofenceEditData[0]['CompanyGeofenceParameter']['company_id']]); ?>
                                            <?php echo $this->Form->text('company_name',array('id'=>'company_id', 'class'=>'form-control','value'=>$geofenceEditData[0]['CompanyName']['company_name'])); ?>
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Company Address</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php echo $this->Form->text('company_address',array('id'=>'comp_address','class'=>'form-control','value'=>$geofenceEditData[0]['CompanyGeofenceParameter']['company_address'])); ?>
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Latitude</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php echo $this->Form->text('latitude',array('id'=>'comp_latitude','class'=>'form-control','value'=>$geofenceEditData[0]['CompanyGeofenceParameter']['latitude'])); ?>
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Longitude</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php echo $this->Form->text('longitude',array('id'=>'comp_longitude','class'=>'form-control','value'=>$geofenceEditData[0]['CompanyGeofenceParameter']['longitude'])); ?>
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Radius</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php echo $this->Form->text('radius',array('id'=>'comp_radius','class'=>'form-control','value'=>$geofenceEditData[0]['CompanyGeofenceParameter']['radius'])); ?>
                                        </div>
                                   </div>
                                </div>
                            </div>
                            <div class="form-actions center-block txt-center">
                                <?php echo $this->Form->submit('Update',array('class'=>'btn green2')); ?>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    $("#menu>ul>li").eq(6).addClass('active open');
    $(".sub-menu>li").eq(10).addClass('active open');
    function editGeofence(){
    var companyVal=$("#company_id").val();
    var companyaddress=$("#comp_address").val();
    var companylat=$("#comp_latitude").val();
    var companylong=$("#comp_longitude").val();
    var companyradius=$("#comp_radius").val();
    if(companyVal == ''){
       alert("Please select company.");
       return false; 
    }
    if(companyaddress == ''){
       alert("Please Enter company address");
       return false; 
    }
    if(companylat == ''){
       alert("Please Enter latitude");
       return false; 
    }
    if(companylong == ''){
       alert("Please Enter longitude");
       return false; 
    }
    if(companyradius == ''){
       alert("Please Enter radius");
       return false; 
    }
    else{

    return true;
       }
    }
</script>