<?php
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
  .menuButton3 .circular-menu.open .circle{
    left: -115px;
  }
  .menu-button {
    left: calc(50% - -3px);
  }
  @media only screen and (device-width: 768px){
.radialMenu.menuButton3 .circular-menu.open .circle {
    width: 34px;
    left: 20px;
    top: -153px;
}
.menuButton3 .circular-menu.open .menu-button {
    left: 20px;
}
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Email Templates List</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/emails/index';?>">Email Templates</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Email Templates List</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>-->Email Templates List </div>
                        <div class="tools">
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <!-- <a href="javascript:;" class="reload"> </a> -->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                    <div class="mainAction marBottom10">
                    <div class="leftFloat">
                    <div class="actions">
                            <div class="btn-group">
                                <select class="btn btn-md green2" aria-expanded="false" name="selectStatus" id="selectStatus" onchange="return searchEmail();">
                                        <option value=""> --Filter-- </option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                            </div>
                        </div>
                        </div>
                        <div class="rightFloat">
                        <div class="actions marRight10">
                            <div class="btn-group">
                                <a class="btn btn-sm green2 dropdown-toggle" href="<?php echo BASE_URL . 'admin/emails/add';?>" data-toggle="modal"> <i class="fa fa-plus"></i> Add New</a>
                            </div>
                        </div>
                        
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <span id="content">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> Template Name </a></th>
                                        <th id="Created" class="DESC txtCenter" onclick="sort('Created',<?php echo $limit; ?>);"><a> Created Date </a></th>
                                        <th id="Modified" class="DESC txtCenter" onclick="sort('Modified',<?php echo $limit; ?>);"><a> Modified Date </a></th>
                                        <th class="txtCenter width90px"> Status </th>
                                        <th class="txtCenter width140px"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(count($data) > 0){
                                foreach ($data as $value) {
                                ?>
                                    <tr>
                                        <td style="width: 40%;"> <?php echo $value['Emailtemplate']['name']; ?> </td>
                                        <td style="width: 25%;" class="txtCenter"> <?php echo $value['Emailtemplate']['created'];  ?> </td>
                                        <td style="width: 25%;" class="txtCenter"> <?php echo $value['Emailtemplate']['modified']; ?> </td>
                                        <td style="width: 5%;" class="txtCenter">
                                            <?php 
                                             if($value['Emailtemplate']['status'] == 1){
                                            ?>
                                                <button type="button" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                            <?php }else{ ?>
                                                <button type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                                            <?php } ?>
                                        </td>
                                        <td style="width: 5%;" class="beatBtnBottom4 txtCenter">
                                          <div class="radialMenu menuButton3">
                                          <nav class="circular-menu">
                                            <div class="circle">
                                                <span id="statusButton<?php echo $value['Emailtemplate']['id']; ?>">
                                                  <?php 
                                                   if($value['Emailtemplate']['status'] == 1){
                                                  ?>
                                                      <button type="button" onclick="updateEmailStatus(<?php echo $value['Emailtemplate']['id']; ?>, 'Active');" data-original-title="Inactivate" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                                  </span>
                                                  <?php }else{ ?>
                                                      <button onclick="updateEmailStatus(<?php echo $value['Emailtemplate']['id']; ?>, 'Inactive');" type="button" data-original-title="Activate" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i style="color: black" class="fa fa-check"></i></button>
                                                     </span>
                                                  <?php } ?>
                                              <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="View" onclick="javascript:window.location.href='emaildetail/<?php echo $value['Emailtemplate']['id'];?>'"><i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Edit" onclick="javascript:window.location.href='emaileditdetail/<?php echo $value['Emailtemplate']['id'];?>'"><i class="fa fa-pencil-square-o"></i></button>
                                            <button onclick="deleteEmail(<?php echo $value['Emailtemplate']['id'].','."'".$value['Emailtemplate']['name']."'"; ?>);" type="button" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                          </div>
                                            <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
                                          </nav>
                                          </div>
                                        </td>
                                    </tr> 
                                    <?php }}else{ ?>
                                      <tr>
                                            <td colspan="5" align="center"><font color="red">No Record(s) Found!</font></td>
                                      </tr> 
                                    <?php  } ?>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                <?php echo $this->element('pagination'); ?>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <div id="todo-members-modal" class="modal fade bs-modal-lg" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Add Email Template</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="control-label col-md-2">Template Title</label>
                                <div class="col-md-10">
                                        <input type="text" class="form-control">
                                </div>
                            </div>
                            <?php echo $this->Html->script('Admin.ckeditor/ckeditor.js'); ?>
                            <div class="form-group">
                                <label class="control-label col-md-2">Template Editor</label>
                                <div class="col-md-9 disableFullScreen">
                                     <?php echo $this->Form->textarea('Emailtemplate.content', array('class'=>'ckeditor','value'=>'','rows'=>'100')); ?>
                                </div>
                                <div class="col-md-10">
                                        <div name="summernote" id="summernote_1"> </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Status</label>
                                <div class="col-md-10">
                                        <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked> Active </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="optionsRadios" id="optionsRadios5" value="option2"> Inactive </label>
                                    </div>
                               </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn green2" data-dismiss="modal">Add</button>
                        <button class="btn default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<div id="cn-overlay" class="cn-overlay"></div>
<!-- END CONTENT -->
<script type="text/javascript">
function sort(val,limit){
  $(".loader").fadeIn("fast");
    var a=$('#'+val).attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='DE';
    }
    else{
        var order='A';
    }
$.ajax({
             url: '<?php echo BASE_URL; ?>admin/emails/emailFilter',
             type: "POST",
             data: {'sort':val,'order':order,'statusVal': $('#selectStatus').val(),'limit':limit},
             success: function(data) { 
                $('#content').html(data);
                $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                if(a==b){
                $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                }
                else{
                $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                }
                $(".loader").fadeOut("fast");
            }
        });
  return false; 
}
 function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $(".loader").fadeIn("fast");
    $.ajax({
              url:'<?php echo BASE_URL; ?>admin/emails/emailFilter',
              type: "POST",
              data: {'statusVal': $('#selectStatus').val(),'j':goVal,'limit':limit,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
  }
 }
 function chngCount(val){
  $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              url:'<?php echo BASE_URL; ?>admin/emails/emailFilter',
              type: "POST",
              data: {'statusVal': $('#selectStatus').val(),'limit':val,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

function abc(val,limit){
  $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/emails/emailFilter',
           type: "POST",
           data: {'statusVal': $('#selectStatus').val(),'j':val,'limit':limit,'sort':a,'order':order},
           success: function(data) { 
               $('#content').html(data);
              $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
              if(a==b){
              $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
              }
              else{
              $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
              }
              $(".loader").fadeOut("fast");
           }
       });
}
function searchEmail(){
  $(".loader").fadeIn("fast");
     $.ajax({
             url: '<?php echo BASE_URL; ?>admin/emails/emailFilter',
             type: "POST",
             data: {'statusVal': $('#selectStatus').val()},
             success: function(data) { 
                $('#content').html(data);
                $(".loader").fadeOut("fast");
            }
        });
  return false;  
}
function deleteEmail(Id,title){
    var conf=confirm("Do you want to delete Email Template : "+title);
    if(conf==true){
      $(".loader").fadeIn("fast");
    $.ajax({
             url: '<?php echo BASE_URL; ?>admin/emails/deleteEmail',
             type: "POST",
             data: {'Id': Id},
             success: function(data) {
                alert(data);
                location.reload();
            }
        });
  return false; 
}
else{}
}
$("#menu>ul>li").eq(5).addClass('active open');
$(".sub-menu>li").eq(9).addClass('active open');

// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});

</script> 
