<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
</style>
<!-- BEGIN CONTENT -->
<div id="content" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Template Detail</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/emails/index';?>">Email Templates List</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span class="active">Template Detail</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">Template Detail </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                    	<?php
                        if (stripos($data[0]['Emailtemplate']['name'], "MedicBleep") !== false) {
                            $type="medicbleep_";
                        }
                        else{
                            $type="";
                        }
                    	echo '<h2 class="text-center"><b>'.$data[0]['Emailtemplate']['name']."</b></h2>";
                        // pr($data);
                        if($data[0]['Emailtemplate']['name']=="MedicBleep User Approve By Admin"){}else{
                                echo $this->element($type.'email_template_header');}
                    	echo '<br>'; ?>
                        <div id="con" align="left">
                    	<?php echo $data[0]['Emailtemplate']['content'];
                        if($data[0]['Emailtemplate']['name']=="MedicBleep User Approve By Admin"){}else{
                    echo $this->element($type.'email_template_footer');}
                    	//echo $this->Ck->input('test');
                    	?></div>
                    </div>
                    <br>
                    <div class="form-actions">
                                <div class="row">
                                            <div class="txtCenter">
                                                <a type="submit" class="btn green2" href="<?php echo BASE_URL . 'admin/emails/emaileditdetail/'.$data[0]['Emailtemplate']['id'];?>">
                                                    <i class="fa fa-pencil"></i> Edit</a>
                                                <button type="button" class="btn default" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/emails/index';?>'">Back</button>
                                            </div>
                                        </div>
                            </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script type="text/javascript">
	$("#menu>ul>li").eq(5).addClass('active open');
</script>