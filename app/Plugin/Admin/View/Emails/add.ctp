<?php
    echo $this->Html->script(array('Admin.ckeditor/ckeditor.js'));   
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
</style>
<!-- BEGIN CONTENT -->
<div id="content" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Template</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/emails/index';?>">Email Templates List</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span class="active">Add Template</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            Add Template </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('addEmail',array('class'=>'form-horizontal','type'=>'file','onsubmit'=>'return validateTemplateAdd();')); ?>
                        <!-- <form class="form-horizontal" role="form" onsubmit="return searchBeat();" id="formSearch"> -->
                        <div class="form-body width100Auto">
                            <div class="form-group">
                                <div class="col-xs-1">
                                    <?php echo $this->Form->label('Title:'); ?>
                                </div>
                                <div class="col-md-11">
                                <?php echo $this->Form->text('title',array('class'=>'form-control input-sm','placeholder'=>'Enter Title','id'=>'textTitle')); ?>
                                    <!-- <input type="text" class="form-control input-sm" placeholder="Beat Title" name="textBeatPostedBy" id="textBeatPostedBy"> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-1">
                                    <?php echo $this->Form->label('Content:'); ?>
                                </div>
                                <div class="col-md-11">
                                    <?php echo $this->Form->textarea('content',array('class'=>'ckeditor','placeholder'=>'Enter Content','id'=>'textDescrip')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center-block txt-center">
                            <button type="submit" class="btn green2"> Save </button>
                            <a class="btn btn-info blue2" href="#todo-members-modal" data-toggle="modal" onclick="preview();"> Preview </a>
                            <button type="button" class="btn default pull-right" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/emails/index';?>'"> Cancel </button>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<div id="todo-members-modal" class="modal fade bs-modal-lg" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-info blue2 pull-right" data-dismiss="modal" aria-hidden="true">X</button>
                <h2 class="modal-title text-center"><b> Preview </b></h2>
            </div>
            <div class="modal-body">
                <h3 id="titleTxt" class="text-center"></h3>
                <div id="con" align="left"></div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn green2" data-dismiss="modal">Add</button> -->
                <button class="btn default" data-dismiss="modal" aria-hidden="true">Back</button>
            </div>
        </div>
    </div>
</div>
<!-- Form validation[START] -->
<script type="text/javascript">
function preview(){
$('#titleTxt').html($('#textTitle').val());
$('#con').html(CKEDITOR.instances.textDescrip.getData());
}
 function validateTemplateAdd(){
    if($('#textTitle').val() == ''){
        alert("Template Title is Blank.");
        $('#textTitle').focus();
        return false;
    }
    else if(CKEDITOR.instances.textDescrip.getData() == '' ){
        alert("Template Content is Blank.");
        CKEDITOR.instances.textDescrip.focus();
        return false;
    }
    return true;
 }
 $("#menu>ul>li").eq(5).addClass('active open');
 $("#menu>ul>li").eq(5).addClass('active open');
</script>
<!-- Form validation[END] -->
