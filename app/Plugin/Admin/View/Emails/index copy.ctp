
<?php

?>
<div class="">
            <div class="page-title">
                        <div class="title_left">
                            <h3>
                    Email Templates
                    
                   </h3>
                        </div>

<div class="title_right">
<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
 

                                
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
<div class="row">

<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">

        <div class="x_title">
        <div class="leftClass">
            <h2>Filter Templates </h2>

<?php                                   
echo $this->Form->create('Emailtemplate');
echo $this->Form->input('searchWord', array(
        'label' => false,
        'type' => 'text',
        'class'=>'',
        'div'=>false,
        ));
echo $this->Form->submit('Search');
      ?>

      </div>

                                          <div style="text-align:right;padding-right:10px !important" >


<?php 

/*echo $this->Form->input('sortType', array(
        'label' => false,
        'options'=>array(1=>"Sort By",2=>"Show all active",3=>"Show all deactive"),
        'type' => 'select',
        'default'=>1,
        'onchange'=>"this.form.submit()",
        ));
        */
echo $this->Form->end();?>

                                           
                                            </div>
            <div class="clearfix"></div>
        </div>
<div class="x_content">
<?php echo $this->Session->flash(); ?>
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                
                                                <th>Id</th>
                                                <th>Name </th>
                                                <th>Ceated Date </th>
                                                <th>Modifiel Date </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                                <th>
                                                   <!-- <input type="checkbox" class="tableflat">-->
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        <?php 
                                        if(count($data)>0){ 
                                        for($i=0;$i < count($data); $i++){
                                            # code...
                                        ?>
                                            <tr class="even pointer">
                                                <td class=" "><?php  echo (!empty($data[$i]['Emailtemplate']['id']))?$data[$i]['Emailtemplate']['id']:'N/A';?></td>
                                                <td class=" "><?php  echo (!empty($data[$i]['Emailtemplate']['name']))?$data[$i]['Emailtemplate']['name']:'N/A';?></td>
                                                
                                                <td class=" "><?php echo (!empty($data[$i]['Emailtemplate']['created']))?$data[$i]['Emailtemplate']['created']:'N/A';?></td>
                                                <td class=" "><?php echo (!empty($data[$i]['Emailtemplate']['modified']))?$data[$i]['Emailtemplate']['modified']:'N/A';?></td>
                                                <td class=" last">
                                                <a  class="emailDetail" data-id="<?php echo $data[$i]['Emailtemplate']['id']; ?>" data-toggle="modal" href="#myModal">View</a>/
                                                <?php echo $this->Html->link('Edit',array('plugin'=>'admin','controller'=>'emails','action'=>'emaileditdetail/'.$data[$i]['Emailtemplate']['id'])); ?>

                                               
                                                </td>
                                                
                                            </tr>
                                            <?php 
                                              }// for loop
                                            }else{

                                             ?>
                                            <tr class="even pointer">
                                                <td colspan="10" class="a-center ">
                                                <h3>No Record Found</h3>
                                                <?php } ?>
                                                </td>
                                            </tr>
                                            
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />
                    </div>
<?php echo $this->Form->end();?>


                   
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Email Templates </h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div  class="paging btn-group page-buttons">
    <?php
    echo $this->Paginator->prev('< ' . __d('users', 'previous'), array('class' => 'btn btn-default prev', 'tag' => 'button'), null, array('class' => 'btn btn-default prev disabled', 'tag' => 'button'));
    echo $this->Paginator->numbers(array('separator' => '', 'class' => 'btn btn-default', 'currentClass' => 'disabled', 'tag' => 'button'));
    echo $this->Paginator->next(__d('users', 'next') . ' >', array('class' => 'btn btn-default next', 'tag' => 'button'), null, array('class' => 'btn btn-default next disabled', 'tag' => 'button'));
    ?>
</div>


