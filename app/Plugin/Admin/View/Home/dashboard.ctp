<?php
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#dashboardRefresh',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
    
?>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
</style>
<div class="loader"></div>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Dashboard
                                <small>dashboard & statistics</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>&nbsp;
                                <span class="thin uppercase hidden-xs"></span>&nbsp;
                                <i class="fa fa-angle-down"></i>
                            </div>
                            
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <?php echo $this->element('admin_breadcrumb'); ?>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN DASHBOARD STATS 1-->
                    <div id="dashboardRefresh">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue2">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($totalUsers) ? $totalUsers : 0; ?>">0</span>
                                    </div>
                                    <div class="desc"> Total Users </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'admin/user/userLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($pendingUsers) ? $pendingUsers : 0; ?>">0</span>
                                    </div>
                                    <div class="desc"> Pending Approval </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'admin/user/pendingUserLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($registeredUsers) ? $registeredUsers : 0; ?>">0</span> </div>
                                    <div class="desc"> Approved Users </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'admin/user/approvedUserLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($incompleteUsers) ? $incompleteUsers : 0; ?>">0</span> </div>
                                    <div class="desc"> Incomplete Users </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'admin/user/incompleteUserLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green2">
                                <div class="visual">
                                    <i class="fa fa-heartbeat"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($totalBeats) ? $totalBeats : 0; ?>">0</span>
                                    </div>
                                    <div class="desc"> Total Beats </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'admin/UserPost/userPostLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat purple">
                                <div class="visual">
                                    <i class="fa fa-heartbeat"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($reportedBeats) ? $reportedBeats : 0; ?>"></span> </div>
                                    <div class="desc"> Reported Beats </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'admin/UserPost/flaggedPostLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                        
                        <?php /* ?><div class="col-md-6 col-sm-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-green"></i>
                                        <span class="caption-subject font-green bold uppercase">interest-wise posted beats</span>
                                    </div>
                                    <div class="actions">
                                        <!--<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>-->
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <!--<div id="echarts_pie" style="height:320px;"></div>-->
                                    <div class="actions marBottom5">
                                                            <div class="btn-group">
                                                                <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a href="javascript:;">
                                                                            <i class="i"></i>last week</a>
                                                                    </li>
                                                                    <li class="divider"> </li>
                                                                    <li>
                                                                        <a href="javascript:;">this week</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;">last month</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;">this month</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div id="dashboard-report-range2" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>&nbsp;
                                <span class="thin uppercase hidden-xs"></span>&nbsp;
                                <i class="fa fa-angle-down"></i>
                            </div>
                                                        </div>
                                    <div id="piechart" style="width: 100%; height: 300px;"></div>

                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div><?php */ ?>
                        <?php /* ?><div class="col-md-6 col-sm-6">
                            <!-- BEGIN REGIONAL STATS PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-red-sunglo"></i>
                                        <span class="caption-subject font-red-sunglo bold uppercase">region-wise posted beats</span>
                                    </div>
                                    <div class="actions">
                                        <!--<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>-->
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="region_statistics_loading">
                                        <?php echo $this->Html->image('Admin.loading.gif', array("alt"=>"loading")); ?>
                                    </div>
                                    <div id="region_statistics_content" class="display-none">
                                        <div class="btn-toolbar margin-bottom-20">
                                            <div class="actions">
                                                            <div class="btn-group">
                                                                <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a href="javascript:;">
                                                                            <i class="i"></i>last week</a>
                                                                    </li>
                                                                    <li class="divider"> </li>
                                                                    <li>
                                                                        <a href="javascript:;">this week</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;">last month</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;">this month</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                            <div id="dashboard-report-range3" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>&nbsp;
                                <span class="thin uppercase hidden-xs"></span>&nbsp;
                                <i class="fa fa-angle-down"></i>
                            </div>
                                        </div>
                                        <div id="vmap_world" class="vmaps display-none"> </div>
                                        <div id="vmap_usa" class="vmaps display-none"> </div>
                                        <div id="vmap_europe" class="vmaps display-none"> </div>
                                        <div id="vmap_russia" class="vmaps display-none"> </div>
                                        <div id="vmap_germany" class="vmaps display-none"> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END REGIONAL STATS PORTLET-->
                        </div><?php */ ?>
                    </div>
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                    
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>          
<script>
$(document).on('click', '.applyBtn', function(){ 
    $(".loader").fadeIn("fast");
    $.ajax({
            url: '<?php echo BASE_URL; ?>admin/Home/dashboardFilterData',
            type: 'POST',
            data: {'start':$('.left .input-mini').val()+' 00:00:00','end':$('.right .input-mini').val()+' 23:59:59'},
            success: function( data ){
                     $("#dashboardRefresh").html(data) ;
                     $(".loader").fadeOut("fast");           
                }
        });
});
$("#menu>ul>li").eq(0).addClass('active open');
</script>            