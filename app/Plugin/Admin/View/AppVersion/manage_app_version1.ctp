<?php 
    $androidAppVersion = ''; $iosAppVersion = ''; 
    if(isset($appVersions) && !empty($appVersions)){
        foreach($appVersions as $appVer){
            //** Android App Version
            if($appVer['AppVersion']['device_type'] == 'Android'){
                $androidAppVersion = $appVer['AppVersion']['version'];
            }
            //** ios App Version
            if($appVer['AppVersion']['device_type'] == 'ios'){
                $iosAppVersion = $appVer['AppVersion']['version'];
            }
        }
    }
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Manage App Versions</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Manage App Versions</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Update App Version</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            Manage App Versions </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo $this->Form->create('appVersion',array('class'=>'form-horizontal','onsubmit'=>'return validateForm()')); ?>
                            <div class="form-body width85Auto">
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Android App Version</label>
                                    <div class="col-md-9">
                                        <div class="col-md-7">
                                            <?php echo $this->Form->text('android',array('id'=>'textAndroidAppVersion','class'=>'form-control','value'=>$androidAppVersion)); ?>
                                        </div>
                                        <div class="col-md-2"><span class="lineHeight30"> (e.g. 2.0.0) </span></div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Ios App Version</label>
                                    <div class="col-md-9">
                                        <div class="col-md-7">
                                            <?php echo $this->Form->text('ios',array('id'=>'textIosAppVersion','class'=>'form-control','value'=>$iosAppVersion)); ?>
                                        </div>
                                        <div class="col-md-2"><span class="lineHeight30"> (e.g. 2.0.0) </span></div>
                                   </div>
                                </div>
                            </div>
                            <div class="form-actions center-block txt-center">
                                <?php echo $this->Form->submit('Save App Version',array('class'=>'btn green2')); ?>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script>
function validateForm(){  
    var androidString = $("#textAndroidAppVersion").val();
    var arrayAndroid = androidString.split(".");
    var lengthAndroid = arrayAndroid.length;

    var iosString = $("#textIosAppVersion").val();
    var arrayIos = iosString.split(".");
    var lengthIos = arrayIos.length; 
    if(androidString == '')
       {
            alert("Enter Android Version.");
            $('#textAndroidAppVersion').focus();
            return false;
       }
    else if(lengthAndroid < 3 || lengthAndroid > 3)
       {
            alert("Version format is not proper (e.g. 2.0.0).");
            $('#textAndroidAppVersion').focus();
            return false;
       }
    if(iosString == '')
       {
            alert("Enter Ios Version.");
            $('#textIosAppVersion').focus();
            return false;
       }
    else if(lengthIos < 3 || lengthIos > 3)
       {
            alert("Version format is not proper (e.g. 2.0.0).");
            $('#textIosAppVersion').focus();
            return false;
       }
       else{

    return true;
       }
    }
$("#menu>ul>li").eq(8).addClass('active open');
$(".sub-menu>li").eq(12).addClass('active open');
</script>
  

