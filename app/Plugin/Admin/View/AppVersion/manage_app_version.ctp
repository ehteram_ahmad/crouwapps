<?php 
    $androidAppVersion = ''; $iosAppVersion = ''; 
    if(isset($appVersions) && !empty($appVersions)){
        foreach($appVersions as $appVer){
            if($appVer['AppVersion']['application_type'] == 'OCR'){
                
                //** Android App Version
                if($appVer['AppVersion']['device_type'] == 'Android'){
                    $androidAppVersionOCR = $appVer['AppVersion']['version'];
                }
                //** ios App Version
                if($appVer['AppVersion']['device_type'] == 'ios'){
                    $iosAppVersionOCR = $appVer['AppVersion']['version'];
                }
                //** Web App Version
                if($appVer['AppVersion']['device_type'] == 'web'){
                    $webAppVersionOCR = $appVer['AppVersion']['version'];
                }
                
            }
            if($appVer['AppVersion']['application_type'] == 'MB'){
                //** Android App Version
                if($appVer['AppVersion']['device_type'] == 'Android'){
                    $androidAppVersionMB = $appVer['AppVersion']['version'];
                }
                //** ios App Version
                if($appVer['AppVersion']['device_type'] == 'ios'){
                    $iosAppVersionMB = $appVer['AppVersion']['version'];
                }
                //** Web App Version
                if($appVer['AppVersion']['device_type'] == 'web'){
                    $webAppVersionMB = $appVer['AppVersion']['version'];
                }

            }
        }
    }
?>
<style type="text/css">
    div.radio input{
    opacity: 1!important;
}
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Manage App Versions</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Manage App Versions</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Update App Version</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            Manage App Versions </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo $this->Form->create('appVersion',array('class'=>'form-horizontal','onsubmit'=>'return validateForm()')); ?>
                            <table class="col-md-12 table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <th class="text-center"> &nbsp; </th>
                                        <th class="text-center">The OCR</th>
                                        <th class="text-center">MB</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">Android</td>
                                        <td>
                                            <?php echo $this->Form->text('androidOcr',array('id'=>'textAndroidAppVersionOCR','class'=>'form-control text-center','value'=>$androidAppVersionOCR)); ?>
                                        </td>
                                        <td>
                                            <?php echo $this->Form->text('androidMb',array('id'=>'textAndroidAppVersionMB','class'=>'form-control text-center','value'=>$androidAppVersionMB)); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">Ios</td>
                                        <td>
                                            <?php echo $this->Form->text('iosOcr',array('id'=>'textIosAppVersionOCR','class'=>'form-control text-center','value'=>$iosAppVersionOCR)); ?> 
                                        </td>
                                        <td>
                                            <?php echo $this->Form->text('iosMb',array('id'=>'textIosAppVersionMB','class'=>'form-control text-center','value'=>$iosAppVersionMB)); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">WEB</td>
                                        <td>
                                            <?php echo $this->Form->text('webOcr',array('id'=>'textWebAppVersionOCR','class'=>'form-control text-center','value'=>$webAppVersionOCR)); ?>
                                        </td>
                                        <td>
                                            <?php echo $this->Form->text('webMb',array('id'=>'textWebAppVersionMB','class'=>'form-control text-center','value'=>$webAppVersionMB)); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-actions center-block txt-center">
                                <?php echo $this->Form->submit('Save App Version',array('class'=>'btn green2')); ?>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script>
function validateForm(){  
    var androidStringOCR = $("#textAndroidAppVersionOCR").val();
    var arrayAndroidOCR = androidStringOCR.split(".");
    var lengthAndroidOCR = arrayAndroidOCR.length;

    var iosStringOCR = $("#textIosAppVersionOCR").val();
    var arrayIosOCR = iosStringOCR.split(".");
    var lengthIosOCR = arrayIosOCR.length; 

    var webStringOCR = $("#textWebAppVersionOCR").val();
    var arrayWebOCR = webStringOCR.split(".");
    var lengthWebOCR = arrayWebOCR.length; 
    
    var androidStringMB = $("#textAndroidAppVersionMB").val();
    var arrayAndroidMB = androidStringMB.split(".");
    var lengthAndroidMB = arrayAndroidMB.length;

    var iosStringMB = $("#textIosAppVersionMB").val();
    var arrayIosMB = iosStringMB.split(".");
    var lengthIosMB = arrayIosMB.length; 

    var webStringMB = $("#textWebAppVersionMB").val();
    var arrayWebMB = webStringMB.split(".");
    var lengthWebMB = arrayWebMB.length; 

    if(androidStringOCR == '')
       {
            alert("Enter OCR Android Version.");
            $('#textAndroidAppVersionOCR').focus();
            return false;
       }
    else if(lengthAndroidOCR < 3 || lengthAndroidOCR > 3)
       {
            alert("Version format is not proper (e.g. 2.0.0).");
            $('#textAndroidAppVersionOCR').focus();
            return false;
       }
    if(androidStringMB == '')
       {
            alert("Enter MB Android Version.");
            $('#textAndroidAppVersionMB').focus();
            return false;
       }
    else if(lengthAndroidMB < 3 || lengthAndroidMB > 3)
       {
            alert("Version format is not proper (e.g. 2.0.0).");
            $('#textAndroidAppVersionMB').focus();
            return false;
       }
    if(iosStringOCR == '')
       {
            alert("Enter OCR Ios Version.");
            $('#textIosAppVersionOCR').focus();
            return false;
       }
    else if(lengthIosOCR < 3 || lengthIosOCR > 3)
       {
            alert("Version format is not proper (e.g. 2.0.0).");
            $('#textIosAppVersionOCR').focus();
            return false;
       }
    if(iosStringMB == '')
       {
            alert("Enter MB Ios Version.");
            $('#textIosAppVersionMB').focus();
            return false;
       }
    else if(lengthIosMB < 3 || lengthIosMB > 3)
       {
            alert("Version format is not proper (e.g. 2.0.0).");
            $('#textIosAppVersionMB').focus();
            return false;
       }
    if(webStringOCR == '')
     {
          alert("Enter OCR Web Version.");
          $('#textWebAppVersionOCR').focus();
          return false;
     }
    else if(lengthWebOCR < 3 || lengthWebOCR > 3)
     {
          alert("Version format is not proper (e.g. 2.0.0).");
          $('#textWebAppVersionOCR').focus();
          return false;
     }
    if(webStringMB == '')
     {
          alert("Enter MB Web Version.");
          $('#textWebAppVersionMB').focus();
          return false;
     }
    else if(lengthWebMB < 3 || lengthWebMB > 3)
     {
          alert("Version format is not proper (e.g. 2.0.0).");
          $('#textWebAppVersionMB').focus();
          return false;
     }
    else{
        return true;
       }
    }
$("#menu>ul>li").eq(8).addClass('active open');
$(".sub-menu>li").eq(12).addClass('active open');
</script>
  

