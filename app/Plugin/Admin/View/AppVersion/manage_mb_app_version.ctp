<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>App Version For MB</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/user/userLists';?>">Users</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">App Version list</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue2 ">
            <div class="portlet-title">
                <div class="caption">
                    <!--<i class="fa fa-cogs"></i>--> Add MB App Version </div>
                <div class="tools">
                    
                    <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                    <a href="" class="remove"> </a>
                    <a href="" class="reload"> </a>-->
                    <a href="" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo $this->Form->create('add',array('class'=>'form-horizontal form-label-left','onsubmit'=>'return save();')); ?>
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-6">
                                <?php 
                                $device=array(
                                    "0"=>"Android",
                                    "1"=>"ios",
                                    );
                                echo $this->Form->select('device',$device,array('class'=>'form-control','default'=>'0','empty'=>'Select Device Type','id'=>'selectDevice')); ?>
                            </div>
                            <div class="col-md-6">
                                <?php 
                                $update=array(
                                    "0"=>"No",
                                    "1"=>"Yes",
                                    );
                                echo $this->Form->select('update',$update,array('class'=>'form-control','default'=>'0','empty'=>'Select Force Update','id'=>'selectUpdate')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                            <?php echo $this->Form->text('version',array('class'=>'form-control input-sm','placeholder'=>'Enter Version','id'=>'textVersion')); ?>
                                <!-- <input type="text" class="form-control input-sm" placeholder="Enter Version" name="textVersion" id="textVersion"> -->
                            </div>
                            <div class="col-md-6">
                            <?php echo $this->Form->text('frequency',array('class'=>'form-control input-sm','placeholder'=>"Enter Update Frequency (in Hour)",'id'=>'textFrequency')); ?>
                                <!-- <input type="text" class="form-control input-sm" placeholder="Enter Update Frequency" name="textFrequency" id="textFrequency"> -->
                            </div>
                        </div>
                 </div>
                    <div class="form-actions center-block txt-center">
                        <button type="submit" class="btn green2" >Add</button>
                        <button type="reset" class="btn default">Reset</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>-->Versions List </div>
                        <div class="tools">
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="remove"> </a>
                            <a href="javascript:;" class="reload"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        <!-- </div> -->
                        <div class="clearfix"></div>
                        <div id="content" class="paddingSide15">
                                <table class="table table-bordered table-striped table-condensed flip-content">
                                    <thead class="flip-content">
                                        <tr>
                                            <!-- <th><input type="checkbox" id="selectAll"></th> -->
                                            <th id="first_name" class="DESC"> <a> Device Type </a></th>
                                            <th id="last_name" class="DESC"> <a> Versions </a></th>
                                            <th id="email" class="DESC"> <a> Force Update </a></th>
                                            <th id="country" class="DESC"> <a> Update Frequency </a></th>
                                            <th id="joining_date" class="DESC"> <a> Created Date </a></th>
                                            <th> Status </th>
                                            <!-- <th> Action </th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        //echo "<pre>"; print_r($appVersions);die;
                                if(count($appVersions) > 0){
                                foreach( $appVersions as $ud){ 
                                    // pr($ud);
                                ?>
                                <tr class="even pointer" >
                                    <td><?php echo $ud['MbAppVersion']['device_type']; ?></td>
                                    <td><?php echo $ud['MbAppVersion']['version']; ?></td>
                                    <td>
                                        <?php if($ud['MbAppVersion']['force_update'] == "1"){
                                            echo "Yes";
                                            }elseif ($ud['MbAppVersion']['force_update'] == "0") {
                                            echo "No";
                                            } ?>
                                    </td>
                                    <td><?php echo $ud['MbAppVersion']['update_frequency']." Hour"; ?></td>
                                    <td><?php echo $ud['MbAppVersion']['created']; ?></td>
                                    <td><?php echo $ud['MbAppVersion']['status']; ?></td>
                                    </tr>
                                 <?php } }else{?> 

                                  <tr>
                                        <td colspan="8" align="center"><font color="red">Record(s) Not  Found!</font></td>
                                  </tr> 
                                  <?php } ?> 
                                    </tbody>
                                </table>


                                <div class="actions marBottom10 paddingSide15">
                                
                            </div>
                     </div>   
                </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
    </div>
    </div>
    </div>
<script type="text/javascript">
function save(){  
    var androidString = $.trim($("#textVersion").val());
    var arrayAndroid = androidString.split(".");
    var lengthAndroid = arrayAndroid.length;
    var device=$("#selectDevice").val();
    var update=$("#selectUpdate").val();
    var frequency = $.trim($("#textFrequency").val());
    if(device == ''){
       alert("Select Device Type.");
       return false; 
    }
    if(update == ''){
       alert("Select Force Update.");
       return false; 
    }
    if(androidString == '')
       {
            alert("Enter Version.");
            $('#textVersion').focus();
            return false;
       }
    else if(lengthAndroid < 3 || lengthAndroid > 3)
       {
            alert("Version format is not proper (e.g. 2.0.0).");
            $('#textVersion').focus();
            return false;
       }
    if(frequency == '')
       {
            alert("Enter Update Frequency.");
            $('#textFrequency').focus();
            return false;
       }
       else{

    return true;
       }
    }
    $("#menu>ul>li").eq(8).addClass('active open');
    $(".sub-menu>li").eq(13).addClass('active open');
</script>