<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>OCR Admin</title>
	<?php
		echo $this->Html->css(array('Admin.bootstrap.min', 'Admin.font/font-awesome.min', 'Admin.animate.min', 'Admin.custom', 'Admin.icheck/flat/green'));

		echo $this->Html->script(array('Admin.jquery.min'));
        

	?>
<!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">
    
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form action="<?php echo BASE_URL;?>admin/index/dashboard" method="post">
                        <h1>Login</h1>
                        <div>
                            <p id="loginMsg"></p>
                        </div>
                        <div>
                            <input type="text" class="form-control" name="textUserName" id="textUserName" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="textPassword" id="textPassword" placeholder="Password" required="" />
                        </div>
                        <div>
                            <a class="btn btn-default submit" onclick="login();">Log in</a>
                            <a class="reset_pass" href="#">Lost your password?</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <!--<p class="change_link">New to site?
                                <a href="#toregister" class="to_register"> Create Account </a>
                            </p>-->
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                

                                <p>OCR</p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="register" class="animate form">
                <section class="login_content">
                    <form>
                        <h1>Create Account</h1>
                        <div>
                            <input type="text" class="form-control" name="textUserName" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="email" class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <a class="btn btn-default submit" href="">Submit</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Already a member ?
                                <a href="#tologin" class="to_register"> Log in </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> OCR</h1>

                                <p>©2015 OCR</p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

</body>

</html>
<script>
    function login(){
        var userName = $("#textUserName").val();
        var password = $("#textPassword").val();
        $.ajax({
            url: "<?php echo BASE_URL; ?>admin/index/login",
            type: 'POST',
            data: { userName: userName, password: password }, 
            success: function( result ){
                if( result.trim() == 'success' ){
                    location.href = "<?php echo BASE_URL; ?>admin/home/dashboard";
                }else{
                    $('#loginMsg').html( result );
                }
                
            }
        });
    }
</script>