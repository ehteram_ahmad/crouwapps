<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>OCR ADMIN </title>
    <?php
        echo $this->Html->css(array('Admin.bootstrap.min', 'Admin.font/font-awesome.min', 'Admin.animate.min', 'Admin.custom', 'Admin.maps/jquery-jvectormap-2.0.1', 'Admin.icheck/flat/green', 'Admin.floatexamples', 'Admin.floatexamples','Admin.chosen'));

        //echo $this->Html->css(array('Admin.chosen')); 

        echo $this->Html->script(array('Admin.jquery.min.js'));

    ?>
    <script language="Javascript">
         BASE_URL = '<?php echo BASE_URL; ?>'; 
   </script>
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <?php /* ?><a href="<?php echo BASE_URL;?>/admin/home/dashboard" class="site_title"> 
                            <?php echo $this->Html->image('Admin.ocr-white.png'); ?>
                        </a><?php */ ?>
                    </div>
                    <div class="clearfix"></div>


                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <?php //echo $this->Html->image('Admin.admin.jpg', array('alt'=> '', 'class'=> 'img-circle profile_img')); ?>
                            </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><?php
                            $nameUser = $this->Session->read('userName');
                             echo ( null !== $nameUser && !empty($nameUser) ? $this->Session->read('userName'):'' );?></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <?php //echo $this->element('Admin.left_menu'); ?>

                    <?php //echo $this->element('Admin.footer_buttons'); ?>                    
                </div>
            </div>
            <?php echo $this->element('Admin.top_navigation'); ?> 
            <?php   echo $this->fetch('content'); ?>
            
        </div>


    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <?php
        echo $this->Html->script(array('Admin.bootstrap.min.js', 'Admin.custom.js'));

        echo $this->Html->script(array('Admin.admin_common.js'));

        echo $this->Html->script(array('Admin.chosen.jquery.js'));

        
    ?>
    
</body>

<script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>

</html>