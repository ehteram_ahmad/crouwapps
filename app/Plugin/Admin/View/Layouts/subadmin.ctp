<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>The OCR | Admin</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL The OCR STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <?php
        //** Admin CSS files
        echo $this->Html->css(array(
                     'Admin.font-awesome',
                     'Admin.simple-line-icons',
                     'Admin.bootstrap',
                     'Admin.uniform.default',
                     'Admin.bootstrap-toggle',
                     'Admin.bootstrap-switch.min',
                     'Admin.daterangepicker.min',
                     'Admin.morris',
                     'Admin.fullcalendar.min',
                     'Admin.jqvmap',
                     'Admin.components',
                     'Admin.plugins',
                     'Admin.layout',
                     'Admin.light',
                     'Admin.custom.min',
                     'Admin.profile',
                     'Admin.select2.min',
                     // 'Admin.select2',
                     'Admin.select2-bootstrap.min',
                     'Admin.custom',
                     'Admin.datetimepicker',
                     'Admin.bootstrap-editable',
                     'Admin.bootstrap-fileinput',
                     'Admin.bootstrap-wysihtml5',
                     'Admin.bootstrap-markdown.min',
                     'Admin.summernote',
                     'Admin.typeahead.js-bootstrap',
                     'Admin.demo-bs3',
                     'Admin.address',
                     'Admin.cubeportfolio',
                     'Admin.PhotoEditorReactUI'
            ));
        //** Admin JS files
        echo $this->Html->script(array('Admin.jquery.min', 'Admin.admin_common'));

        ?>
        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>favicon.ico" /> 
        <script language="Javascript">
        BASE_URL = '<?php echo BASE_URL; ?>'; 
        </script>
     </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo BASE_URL . 'admin/user/userListsForsubadmin'; ?>">
                        <?php echo $this->Html->image('Admin.logo.png', array("alt"=> "logo", "class"=>"logo-default")); ?>
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?php
                            $nameUser = $this->Session->read('userName');
                             echo ( null !== $nameUser && !empty($nameUser) ? $this->Session->read('userName'):'' );?> </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <?php echo $this->Html->image('Admin.admin.jpg', array("alt"=>"", "class"=> "img-circle")); ?>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!--<li>
                                        <a href="page_user_profile_1.html">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="app_todo_2.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success2"> 7 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>-->
                                    <li>
                                        <a href="<?php echo BASE_URL . 'admin/index/logout'?>">
                                            <i class="fa fa-power-off"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                           <!-- <li class="dropdown dropdown-extended quick-sidebar-toggler">
                                <span class="sr-only">Toggle Quick Sidebar</span>
                                <i class="icon-logout"></i>
                            </li>-->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
        
            <!-- BEGIN SIDEBAR -->
            <?php //echo $this->element('left_menu'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE BREADCRUMB -->
        <!-- <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/UserPost/userPostLists';?>">All Posts</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
            
                <a href="<?php echo BASE_URL . 'admin/UserPost/PostLists';?>"></a> 
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Post Details</span>
            </li>
        </ul> -->
        <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN CONTENT -->
            <?php echo $this->fetch('content'); ?>

            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; 
                <a style="color:#000; text-decoration:underline;" target="_blank" href="http://mediccreations.com/">
                    <?php echo $this->Html->image('Admin.medic-logo-small.png', array("style"=> "margin-right: 4px;" , "alt"=> "Logo")); ?>
                    Medic Creations</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<script src="js/excanvas.min.js"></script> 
<![endif]-->
       <?php 
        echo $this->Html->script(array(
                'Admin.bootstrap.min', 
                'Admin.js.cookie.min', 
                'Admin.bootstrap-hover-dropdown.min', 
                'Admin.jquery.slimscroll.min', 
                'Admin.jquery.blockui.min', 
                'Admin.jquery.uniform.min', 
                'Admin.bootstrap-switch.min', 
                'Admin.moment.min', 
                'Admin.daterangepicker.min',
                'Admin.morris.min',
                'Admin.raphael-min', 
                'Admin.jquery.waypoints.min', 
                'Admin.jquery.counterup.min', 
                'Admin.amcharts', 
                'Admin.serial',
                'Admin.pie', 
                'Admin.radar', 
                'Admin.light', 
                'Admin.patterns',
                'Admin.chalk', 
                'Admin.ammap', 
                'Admin.worldLow', 
                'Admin.amstock', 
                'Admin.fullcalendar.min', 
                'Admin.jquery.flot.min', 
                'Admin.jquery.flot.resize.min', 
                'Admin.jquery.flot.categories.min', 
                'Admin.jquery.easypiechart.min', 
                'Admin.jquery.sparkline.min', 
                'Admin.jquery.vmap', 
                'Admin.jquery.vmap.russia', 
                'Admin.jquery.vmap.world', 
                'Admin.jquery.vmap.europe', 
                'Admin.jquery.vmap.germany', 
                'Admin.jquery.vmap.usa', 
                'Admin.jquery.vmap.sampledata', 
                //'Admin.echarts', 
                'Admin.wysihtml5-0.3.0', 
                'Admin.bootstrap-wysihtml5', 
                'Admin.markdown', 
                'Admin.bootstrap-markdown', 
                'Admin.summernote.min', 
                'Admin.app.min', 
                'Admin.dashboard', 
                //'Admin.charts-echarts', 
                //'Admin.components-editors.min', 
                'Admin.layout.min', 
                'Admin.demo.min', 
                'Admin.quick-sidebar.min', 
                'Admin.bootstrap-toggle',
                'Admin.test-pie', 
                'Admin.select2.full.min',
                'Admin.components-select3',
                'Admin.bootstrap-datepicker.min',
                'Admin.components-date-time-pickers.min',
                'Admin.jquery.mockjax',
                // 'Admin.select2',
                //'Admin.bootstrap',
                'Admin.bootstrap-datetimepicker',
                'Admin.bootstrap-editable',
                'Admin.typeahead',
                'Admin.typeaheadjs',
                'Admin.address',
                'Admin.jquery.cubeportfolio',
                'Admin.portfolio-4',
                'Admin.demo-mock',
                'Admin.form-demo',
                //'Admin.app2',
                'Admin.layout.min',
                'Admin.PhotoEditorSDK',
                'Admin.PhotoEditorReactUI',
            ));
       ?>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <?php //echo $this->element('sql_dump'); ?>
    </body>

</html>