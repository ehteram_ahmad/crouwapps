<?php
if( isset($storyPoints) && !empty($storyPoints) ){
    foreach( $storyPoints as $points ){
        if( $points['TopStoryPoint']['name'] == 'Top Shared' ){
            $topSharedPoint = $points['TopStoryPoint']['point'];
        }
        if( $points['TopStoryPoint']['name'] == 'Up Beat' ){
            $upBeatPoint = $points['TopStoryPoint']['point'];
        }
        if( $points['TopStoryPoint']['name'] == 'Top Comment' ){
            $topCommentPoint = $points['TopStoryPoint']['point'];
        }
        if( $points['TopStoryPoint']['name'] == 'Down Beat' ){
            $downBeatPoint = $points['TopStoryPoint']['point'];
        }
    }
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Manage Story Points</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Manage Story Points</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Manage Story Points</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            Manage Story Points </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo $this->Form->create('storyPoint',array('class'=>'form-horizontal')); ?>
                            <div class="form-body width85Auto">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Share</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php echo $this->Form->text('share',array('class'=>'form-control','value'=>$topSharedPoint)); ?>
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Upbeat</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php echo $this->Form->text('upbeat',array('class'=>'form-control','value'=>$upBeatPoint)); ?>
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Comment</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php echo $this->Form->text('comments',array('class'=>'form-control','value'=>$topCommentPoint)); ?>
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Downbeat</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php echo $this->Form->text('downbeat',array('class'=>'form-control','value'=>$downBeatPoint)); ?>
                                        </div>
                                   </div>
                                </div>
                            </div>
                            <div class="form-actions center-block txt-center">
                                <?php echo $this->Form->submit('Save Story point',array('class'=>'btn green2')); ?>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    $("#menu>ul>li").eq(6).addClass('active open');
    $(".sub-menu>li").eq(10).addClass('active open');
</script>