<!-- BEGIN CONTENT -->
<div id="content" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Content Detail</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/emails/index';?>">Content List</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span class="active">Content Detail</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">Template Detail </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form container-fluid" id="contentTxt">
                    	<?php
                    	// pr($data);
                    	echo '<h2 class="text-center"><b>'.$data[0]['Content']['content_title']."</b></h2>";
                    	echo '<br>';
                    	echo $data[0]['Content']['content_text'];

                    	//echo $this->Ck->input('test');
                    	?>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="txtCenter">
                                <a type="submit" class="btn green2" href="<?php echo BASE_URL . 'admin/Content/editContents/'.$data[0]['Content']['content_id'];?>">
                                    <i class="fa fa-pencil"></i> Edit</a>
                                <button type="button" class="btn default" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/Content/listContents';?>'">Back</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script type="text/javascript">
	$("#menu>ul>li").eq(4).addClass('active open');
</script>