<table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <!-- <th><input type="checkbox" id="selectAll"></th> -->
                                                <th id="first_name" class="DESC" ><a> Company Name </a></th>
                                                <th id="last_name" class="DESC" o><a> Company Address </a></th>
                                                <th id="email" class="DESC" ><a> Latitude </a></th>
                                                <th id="country" class="DESC" ><a> Longitude </a></th>
                                                <th id="profession" class="DESC"><a> Radius </a></th>
                                                <th id="joining_date" class="DESC"><a> Status </a></th>
                                                <th id="joining_date" class="DESC"><a> Created </a></th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            // echo "<pre>"; print_r($userData);die;
                                    if(count($geofenceListData) > 0){
                                    foreach( $geofenceListData as $geoData){ 
                                    ?>
                                    <tr class="even pointer">
                                        <td style="width:15%;" class=" "><?php echo $geoData['CompanyName']['company_name'];?></td>
                                        <td style="width:15%;" class=" "><?php echo $geoData['CompanyGeofenceParameter']['company_address'];?> </td>
                                        <td style="width:25%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['latitude'];?> </td>
                                        <td style="width:14%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['longitude'];?> </td>
                                        <td style="width:15%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['radius'];?> </td>
                                        <td style="width:10%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['status'];?></td>
                                        <td style="width:10%;" class=" "><?php echo $geoData ['CompanyGeofenceParameter']['created'];?></td>
                                        <td style="width:1%;" class="last"> 
                                          <button data-toggle="modal"  onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/geofences/editGeofenceData/' . $geoData['CompanyGeofenceParameter']['company_id'] ; ?>'; return false;"  type="button" data-original-title="Edit" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-pencil-square-o"></i></button>
                                        </td>
                                        
                                        </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                      </tr> 
                                      <?php } ?> 
                                        </tbody>
                                    </table>
                                    <div class="row">

    <div class="col-md-12 col-sm-12">
        <?php echo $this->element('pagination'); ?>
    </div>
</div>