<table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>
            <!-- <th class="width40px">&nbsp;  </th> -->
            <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> User Name </a></th>
            <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email ID </a></th>
            <th id="Subject" class="DESC" onclick="sort('Subject',<?php echo $limit; ?>);"><a> Subject </a></th>
            <th id="Source" class="DESC width90px" onclick="sort('Source',<?php echo $limit; ?>);"><a> Source </a></th>
            <th id="Date" class="DESC width140px" onclick="sort('Date',<?php echo $limit; ?>);"><a> Date </a></th>
            <th id="Status" class="DESC width140px"> Status </th>
            <th id="Action" class="DESC width140px"> Action </th>
        </tr>
    </thead>
    <tbody>
      <?php 
      if(count($feedbackLists) > 0){
          for ($i=0; $i < count($feedbackLists); $i++) { 
      ?>
          <tr id="feedBack<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>">
              <td style="width:20%" > <?php echo $feedbackLists[$i]['UserProfile']['first_name']." ".$feedbackLists[$i]['UserProfile']['last_name'];?> </td>
              <td id="to<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>" style="width:30%"> <?php echo $feedbackLists[$i]['User']['email'];?> </td>
              <td  id="subCon" onclick="showFeed(<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>);" data-toggle="modal" href='#modal-id' > <?php echo $feedbackLists[$i]['UserFeedback']['subject'];?> </td>
              <td id="con<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>" class="txtCenter hidden"> <?php echo $feedbackLists[$i]['UserFeedback']['feedback'];?> </td>
              <td style="width:10%"  class="txtCenter"> <?php echo $feedbackLists[$i]['UserFeedback']['feedback_source'];?> </td>
              <td style="width:10%" class="beatBtnBottom4 txtCenter"><?php 
                                       $datetime = explode(" ",$feedbackLists[$i]['UserFeedback']['created']);
                                       $date = $datetime[0];
                                       $time = $datetime[1];
                                       echo date('d-m-Y',strtotime($date))."  ".date ('g:i:s A',strtotime($time));
                                       ?></td>
              <td style="width:5%" class="beatBtnBottom4 txtCenter">
                <?php 
                    if(count($feedResp[$i]) > 0){
                 ?>
                <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
                <?php }else{ ?>
                <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="No Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
                <?php } ?>
              </td>
              <td style="width:5%" class="beatBtnBottom4 txtCenter">
                <button onclick="javascript:window.location.href='feedbackResponse/<?php echo $feedbackLists[$i]['UserFeedback']['id'];?>'" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Send Mail" type="button"><i class="fa fa-envelope-o"></i></button>
              </td>
          </tr>
           <?php } }else{?> 

            <tr>
                  <td colspan="7"><h1 align="center">No Record found</h1></td>
            </tr> 
            <?php } ?>   
          
      </tbody>
</table>
<div class="actions marBottom10">
    <div class="row">
        <div class="col-md-12 col-sm-12">
                <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>