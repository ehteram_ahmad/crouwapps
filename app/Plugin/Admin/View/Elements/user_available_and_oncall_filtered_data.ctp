<table class="table table-bordered table-striped table-condensed flip-content tableButtonNew">
                                <thead class="flip-content">
                                    <tr>
                                        <th> Email </th>
                                        <th> Institute Name </th>
                                        <th> Type </th>
                                        <th> On/Off </th>
                                        <th> PlatForm </th>
                                        <th> Date </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if(count($availableAndOncallTransactionData) > 0){
                                    foreach( $availableAndOncallTransactionData as $userData){
                                    ?>
                                    <tr>
                                        <td><?php echo $userData['User']['email'];?></td>
                                        <td> <?php echo $userData['CompanyName']['company_name']; ?> </td>
                                        <td> <?php echo $userData['AvailableAndOncallTransaction']['type']; ?> </td>
                                        <td><?php 
                                                if($userData['AvailableAndOncallTransaction']['status'] == 1)
                                                {
                                                    echo "ON";
                                                }
                                                else
                                                {
                                                    echo "OFF";
                                                }
                                            ?> 
                                        </td>
                                        <td> <?php echo $userData['AvailableAndOncallTransaction']['device_type']; ?> </td>
                                        <td> <?php echo $userData['AvailableAndOncallTransaction']['created']; ?> </td>
                                    </tr>
                                    <?php } }else{?> 
                                    <tr>
                                        <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                    </tr> 
                                    <?php } ?>
                                </tbody>
                            </table>
                            <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <?php echo $this->element('pagination'); ?>
                                </div>
                            </div>