<table class="table table-bordered table-striped table-condensed flip-content tableButtonNew">
                                <thead class="flip-content">
                                    <tr>
                                        <th> Email </th>
                                        <th> Device </th>
                                        <th> Product </th>
                                        <th> Manufacturer </th>
                                        <th> Device id </th>
                                        <th> Api level </th>
                                        <th> Model </th>
                                        <th> OS version </th>
                                        <th> Version </th>
                                        <th> Device type </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if(count($userDeviceInfoData) > 0){
                                    foreach( $userDeviceInfoData as $deviceData){ 
                                        $userDeviceArr = json_decode($deviceData['UserDeviceInfo']['device_info']);
                                    ?>
                                    <tr>
                                        <td><?php echo $deviceData['User']['email'];?></td>
                                        <td> <?php echo $userDeviceArr->device; ?> </td>
                                        <td> <?php echo $userDeviceArr->product; ?> </td>
                                        <td> <?php echo $userDeviceArr->manufacturer; ?> </td>
                                        <td> <?php echo $userDeviceArr->device_id; ?> </td>
                                        <td> <?php echo $userDeviceArr->api_level; ?> </td>
                                        <td> <?php echo $userDeviceArr->model; ?> </td>
                                        <td> <?php echo $userDeviceArr->os_version; ?> </td>
                                        <td> <?php echo $userDeviceArr->version; ?> </td>
                                        <td> <?php echo $userDeviceArr->device_type; ?> </td>
                                    </tr>
                                    <?php } }else{?> 
                                    <tr>
                                        <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                    </tr> 
                                    <?php } ?> 
                                </tbody>
                            </table>
                            <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <?php echo $this->element('pagination'); ?>
                                </div>
                            </div>