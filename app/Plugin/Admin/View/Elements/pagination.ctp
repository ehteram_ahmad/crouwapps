<style type="text/css">
   /* input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; */
}
</style>
<div class="row">
<?php 

$pageNo=$this->Paginator->current();
$totalPage=$this->Paginator->counter('{:pages}');
$perPage=$this->Paginator->params()['limit'];
$count=array("20"=>"20","50"=>"50","100"=>"100"); 
?>

    <div class="col-md-3 col-sm-3">
        <div class="dataTables_length paginationNew" id="sample_2_length">
        <?php echo $this->Form->input('Data_Count',array('label'=>'Data Count ','options'=>$count,'type'=>'select','div'=>false,'onchange' =>'chngCount(this.value)','default'=>$perPage,'class'=>'form-control input-sm input-xsmall input-inline'));  ?>
        </div>
        <!--<div class="dataTables_info" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>-->
    </div>
    <div class="col-md-9 col-sm-9">
        <div class="dataTables_paginate paging_bootstrap_full_number rightFloat paginationNew" id="sample_1_paginate">
            <ul class="pagination" style="visibility: visible;">
            <?php if($pageNo==1){
                    echo '<li class="prev disabled"><a title="First"><i class="fa fa-angle-double-left"></i></a></li>';
                    echo '<li class="prev disabled"><a title="Prev"><i class="fa fa-angle-left"></i></a></li>';
                 }
                    else{
                echo '<li class="prev"><a onclick="abc(1,'.$limit.')" title="First"><i class="fa fa-angle-double-left"></i></a></li>';
                echo '<li class="prev"><a onclick="abc('.($pageNo-1).','.$limit.')" title="Prev"><i class="fa fa-angle-left"></i></a></li>';
                } ?>
                <li class="active"><a><?php echo $this->Paginator->counter('{:page} / {:pages}'); ?></a></li>
                <?php 
                if($pageNo==$totalPage){
                echo '<li class="next disabled"><a title="Next"><i class="fa fa-angle-right"></i></a></li>';
                echo '<li class="next disabled"><a title="Last"><i class="fa fa-angle-double-right"></i></a></li>';
                }
                else{
                echo '<li class="next"><a onclick="abc('.($pageNo+1).','.$limit.')" title="Next"><i class="fa fa-angle-right"></i></a></li>';
                echo '<li class="next"><a onclick="abc('.$totalPage.','.$limit.')" title="Last"><i class="fa fa-angle-double-right"></i></a></li>';
                }
                 ?>
            </ul>
            <div class="pagination-panel"> 
            <label>Go To Page</label>
            <a onclick="dec();" class="btn btn-sm default prev"><i class="fa fa-angle-left"></i></a>
                <?php echo $this->Form->number('Go_to',array('value'=>$pageNo,'type'=>'number','div'=>false,'min'=>'01','max'=>$totalPage,'id'=>'chngPage','class'=>'pagination-panel-input form-control input-sm input-inline')); ?>
            <a onclick="inc();" class="btn btn-sm default next"><i class="fa fa-angle-right"></i></a>
                <?php
echo $this->Form->button('GO',array('onclick'=>'chngPage('.$totalPage.','.$limit.')','class'=>'btn default marLeft10px')); ?>
           </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    max='<?php echo $totalPage ?>';
    function inc(){
        val=$('#chngPage').val();
        if(parseInt(val) >= Math.ceil(max)){
        }
        else{
        val++; }
        $('#chngPage').val(val);
    }
    function dec(){
        val=$('#chngPage').val();
        if(parseInt(val) <= 1){}
        else{
        val--; }
        $('#chngPage').val(val);
    }
</script>