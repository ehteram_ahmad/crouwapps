<div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div id="menu" class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start">
                            <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                                <!--<span class="arrow open"></span>-->
                            </a>
                        </li>
                        <!--<li class="heading">
                            <h3 class="uppercase">Inner Pages</h3>
                        </li>-->
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Users</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL . 'admin/user/userLists';?>" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">All</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL . 'admin/user/approvedUserLists';?>" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Approved</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL . 'admin/user/pendingUserLists';?>" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Pending</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL . 'admin/user/incompleteUserLists';?>" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Incomplete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-desktop"></i>
                                <span class="title">Posts</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/UserPost/userPostLists" class="nav-link ">
                                        <i class="fa fa-desktop"></i>
                                        <span class="title">All</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/UserPost/featuredPostLists" class="nav-link ">
                                        <i class="fa fa-desktop"></i>
                                        <span class="title">Featured</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/UserPost/flaggedPostLists" class="nav-link ">
                                        <i class="fa fa-desktop"></i>
                                        <span class="title">Flagged</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Specialties</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/Specility/allSpecilities" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Specialties Listings</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-clone"></i>
                                <span class="title">Manage Content</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/Content/listContents" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Content Listings</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-envelope-o"></i>
                                <span class="title">Email Templates</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/emails/index" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Template Listing</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-commenting-o"></i>
                                <span class="title">Manage Story Points</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/StoryPoint/manageStoryPoint" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Manage Story Point</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-comments-o"></i>
                                <span class="title">User Feedbacks</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/feedback/feedbackList" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">User Feedback Lists</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-mobile"></i>
                                <span class="title">Manage App Versions</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/AppVersion/manageAppVersion" class="nav-link ">
                                        <i class="fa fa-mobile"></i>
                                        <span class="title">Manage TheOCR App Versions</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/AppVersion/manageMbAppVersion" class="nav-link ">
                                        <i class="fa fa-mobile"></i>
                                        <span class="title">Manage MB App Versions</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Settings</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/setting/manageCip" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">CIP Setting</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/setting/manageForceLogout" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Force Logout Setting</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/setting/dndStatusOptions" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Dnd Status Options</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Livestream Access</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/user/liveStreamAccessUsers" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Livestream Access Users</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="<?php echo BASE_URL;?>admin/geofences/geofenceList" class="nav-link ">
                                <i class="icon-user"></i>
                                <span class="title">Company Geofence</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo BASE_URL;?>admin/userdevice/userDeviceInfoList" class="nav-link ">
                                <i class="icon-user"></i>
                                <span class="title">User Device Info</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo BASE_URL;?>admin/user/userAvailableOncallData" class="nav-link ">
                                <i class="icon-user"></i>
                                <span class="title">User Available/Oncall Transaction</span>
                            </a>
                        </li>

                        <!--  <li class="nav-item">
                            <a href="<?php //echo BASE_URL;?>admin/QrCode/qrCodeIndex" class="nav-link ">
                                <i class="icon-user"></i>
                                <span class="title">Qr Code Managment</span>
                            </a>
                        </li> -->



                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Qr Code Managment</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/QrCode/requestQrCodeListing" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Qr Code Requests</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/QrCode/qrCodeIndex" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">History</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Transactions</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/Transaction/screenShotTransactionListing" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Screen Shot Transaction</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/Transaction/userLoginTransaction" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">User Login Transaction</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item  ">
                            <a href="<?php echo BASE_URL;?>admin/user/adminRoles" class="nav-link ">
                                <i class="icon-user"></i>
                                <span class="title">Administration</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Report</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>admin/user/userReport" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">User's Report</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>