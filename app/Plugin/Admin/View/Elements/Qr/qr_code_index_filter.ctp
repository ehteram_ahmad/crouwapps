<table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>
            <!-- <th><input type="checkbox" id="selectAll"></th> -->
            <th id="first_name" class="DESC"><a> Request Id </a></th>
            <th id="country" class="DESC"><a> Institute Name </a></th>
            <th id="first_name" class="DESC"><a> Batch Number </a></th>
            <th id="last_name" class="DESC"><a>QrCode's </a></th>
            <th id="email" class="DESC"><a> Validity </a></th>
            <th id="email" class="DESC"><a> Status </a></th>
            <th id="joining_date" class="DESC"><a> Created On </a></th>
            <th><a> Action </a></th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(count($qrCodeRequests) > 0){
            foreach( $qrCodeRequests as $request){ 
        ?>
    <tr class="even pointer" id="user<?php echo $request['QrCodeRequest']['id']; ?>">
        <td style="width:10%;" class=" "><?php echo $request['QrCodeRequest']['id'];?></td>
        <td style="width:35%;" class=" "><?php echo $request ['CompanyName']['company_name'];?> </td>
        <td style="width:15%;" class=" "><?php if(!empty($request['batch_number'])){
            echo $request['batch_number']['QrCodeDetail']['batch_number'];
        }else{
            echo "-- NA --";
        } ?></td>
        <td style="width:5%;" class=" "><?php echo $request['QrCodeRequest']['number_of_qr_codes'];?> </td>
        <td style="width:5%;" class=" "><?php echo $request ['QrCodeRequest']['validity'];?> </td>
        <?php if($request ['QrCodeRequest']['status'] == 1) {
            $text = "Code generated";
        }else if($request ['QrCodeRequest']['status'] == 3){
            $text = "Batch expired";
        }else{
            $text = "Request rejected";
        } ?>
        <td style="width:20%;" class=" "><?php echo $text;?> </td>
        <td style="width:25%;" class=" "><?php echo $request ['QrCodeRequest']['requested_at'];?> </td>
        <td style="width:1%;" class="last">
        <?php if(!empty($request['batch_number'])){ ?>
            <div class="radialMenu menuButton3">
        <nav class="circular-menu">
          <div class="circle leftNegative115px">

            <button data-toggle="modal" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/QrCode/qrCodeBatchView/' . $request['batch_number']['QrCodeDetail']['batch_number'] ; ?>'; return false;" type="button" data-original-title="view" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-eye"></i></button>
            <?php
            if($request ['QrCodeRequest']['status'] != 3){ ?>
                <button type="button" id="deleteModalBtn" data-original-title="Expire Batch Number" value="<?php echo $request['batch_number']['QrCodeDetail']['batch_number']; ?>" data-rid="<?php echo $request['QrCodeRequest']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips deleteModalBtn" data-toggle="modal" ><i class="fa fa-trash-o"></i></button>
           <?php }?>
          </div>
          <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
        </nav>
     </div>
       <?php }else{
            echo "";
        } ?>
    </td>
    </tr>
    <?php 
    }} else { 
    ?> 

  <tr>
        <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
  </tr> 
  <?php } ?> 
    </tbody>
</table>


<span class="pull-right">Total Count : <?php echo $tCount; ?></span>
<div class="actions marBottom10 paddingSide15">
<?php 
            if(count($qrCodeRequests) > 0){  
                echo $this->element('pagination');
            }
        ?>
</div>
<script>
$(document).ready(function() {
    // $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".datePick").datepicker({
        dateFormat: 'yy-mm-dd'
    });
});
//** Delete user
$(".deleteModalBtn").click(function(){
  $("#deleteModal").modal('show');
  $('#delUser').val($(this).val());
  $('#delUser').attr('data-rid',$(this).attr('data-rid'));
});
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script> 