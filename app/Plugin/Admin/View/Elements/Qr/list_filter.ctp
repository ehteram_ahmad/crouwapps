<table class="table table-bordered table-striped table-condensed flip-content">
        <thead class="flip-content">
            <tr>
                <!-- <th><input type="checkbox" id="selectAll"></th> -->
                <th id="first_name" class="DESC"><a> Request Id </a></th>
                <th id="first_name" class="DESC"><a> Institute Name </a></th>
                <th id="last_name" class="DESC"><a> Number Of Cards </a></th>
                <th id="email" class="DESC"><a> validity </a></th>
                <th id="profession" class="DESC"><a> Requested at </a></th>
                <th><a> Action </a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            // echo "<pre>"; print_r($userData);die;
    if(count($qrCodeData) > 0){
    foreach( $qrCodeData as $qrd){ 
    ?>
    <tr class="even pointer">
        <td style="width:15%;" class=" "><?php echo $qrd['QrCodeRequest']['id'];?></td>
        <td style="width:35%;" class=" "><?php echo $qrd['CompanyName']['company_name'];?></td>
        <td style="width:15%;" class=" "><?php echo $qrd['QrCodeRequest']['number_of_qr_codes'];?></td>
        <td style="width:10%;" class=" "><?php echo $qrd['QrCodeRequest']['validity'];?> </td>
        <td style="width:20%;" class=" "><?php echo $qrd ['QrCodeRequest']['requested_at'];?> </td>
       <td style="width:1%;" class="last">
            <div class="radialMenu menuButton3">
            <nav class="circular-menu">
              <div class="circle">

                <button type="button" onclick="genrateQrCode(<?php echo $qrd ['CompanyName']['id']; ?>, <?php echo $qrd['QrCodeRequest']['validity']; ?>, <?php echo $qrd['QrCodeRequest']['number_of_qr_codes']; ?>,<?php echo $qrd['QrCodeRequest']['id'];?>);" data-original-title="Genrate Qr Code" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-toggle="modal" ><i class="fa fa-eye"></i></button>

                <button type="button" id="deleteModalBtn" data-original-title="Reject Request" value="<?php echo $qrd['QrCodeRequest']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips deleteModalBtn" data-toggle="modal" ><i class="fa fa-trash-o"></i></button>
              </div>
              <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
            </nav>
         </div>
        </td>
     <?php } }else{?> 
      <tr>
            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
      </tr> 
      <?php } ?> 
        </tbody>
    </table>


    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
    <div class="actions marBottom10 paddingSide15">
    <?php 
                if(count($qrCodeData) > 0){  
                    echo $this->element('pagination');
                }
            ?>
</div>
<script>
$(document).ready(function() {
    // $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".datePick").datepicker({
    dateFormat: 'yy-mm-dd'
});
 
});
//** Delete user
$(".deleteModalBtn").click(function(){
  $("#deleteModal").modal('show');
  $('#requestId').val($(this).val());
});
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script> 

