
    <table class="table table-bordered table-striped table-condensed flip-content">
        <thead class="flip-content">
            <tr>
                <!-- <th><input type="checkbox" id="selectAll"></th> -->
                <th id="first_name" class="DESC" onclick="sort('first_name',<?php echo $limit; ?>);"><a> First Name </a></th>
                <th id="last_name" class="DESC" onclick="sort('last_name',<?php echo $limit; ?>);"><a> Last Name </a></th>
                <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
                <th id="country" class="DESC" onclick="sort('country',<?php echo $limit; ?>);"><a> Country </a></th>
                <th id="profession" class="DESC" onclick="sort('profession',<?php echo $limit; ?>);"><a> Profession </a></th>
                <th id="joining_date" class="DESC" onclick="sort('joining_date',<?php echo $limit; ?>);"><a> Joining Date </a></th>
                <th> Access </th>
                <th> Action </th>
            </tr>
        </thead>
        <tbody>
            <?php 
            // echo "<pre>"; print_r($userData);die;
    if(count($userData) > 0){
    foreach( $userData as $ud){ 
    ?>
    <tr class="even pointer" id="user<?php echo $ud['User']['id']; ?>">
        <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
        <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
        <td style="width:25%;" class=" "><?php echo $ud ['User']['email'];?> </td>
        <td style="width:14%;" class=" "><?php echo !empty($ud ['Country']['country_name']) ? $ud ['Country']['country_name'] : 'N/A';?> </td>
        <td style="width:15%;" class=" "><?php echo !empty($ud ['Profession']['profession_type']) ? $ud ['Profession']['profession_type'] : 'N/A';?> </td>
        <td style="width:10%;" class=" "><?php echo date('d-m-Y', strtotime($ud ['User']['registration_date']));?></td>
        <td style="width:5%;" class="text-center">
          <?php 
           if($ud['lbpau']['status']==1){
          ?>
              <button type="button" data-original-title="Access Given" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips accessLiveStreamModalBtn" data-toggle="modal"><i class="fa fa-check"></i></button>

          <?php }else{ ?>
              <button type="button" data-original-title="Access Not Given" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips accessLiveStreamModalBtn" data-toggle="modal"><i class="fa fa-check"></i></button>
             
          <?php 
              }
           ?>
        </td>
        <td style="width:1%;" class="last">
                
            <div class="radialMenu menuButton3">
            <nav class="circular-menu">
              <div class="circle">
              <button type="button" id="accessLiveStream" data-original-title="Give Access" value="<?php echo $ud ['User']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips accessLiveStreamModalBtn" data-toggle="modal" onclick="addLiveStreamAccess();" ><i class="fa fa-check"></i></button>
              <button type="button" id="accessRemoveLiveStream" data-original-title="Remove Access" value="<?php echo $ud ['User']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips accessRemoveLiveStreamModalBtn" data-toggle="modal" onclick="removeLiveStreamAccess();"><i class="fa fa-uncheck"></i></button>
              </div>
              <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
            </nav>
         </div>
                 
        </td>
        
        </tr>
     <?php } }else{?> 

      <tr>
            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
      </tr> 
      <?php } ?> 
        </tbody>
    </table>

    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
    <div class="actions marBottom10 paddingSide15">
    <?php 
                if(count($userData) > 0){  
                    echo $this->element('pagination');
                }
            ?>
</div>