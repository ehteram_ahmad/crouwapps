<table class="table table-striped responsive-utilities jambo_table bulk_action">
                                        <thead>
                                            <tr class="headings">
                                                <th id="first_name" class="DESC" onclick="sort('first_name',<?php echo $limit; ?>);"><a> First Name </a></th>
                                                <th id="last_name" class="DESC" onclick="sort('last_name',<?php echo $limit; ?>);"><a> Last Name </a></th>
                                                <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
                                                <th> Country </th>
                                                <th id="profession" class="DESC" onclick="sort('profession',<?php echo $limit; ?>);"><a> Profession </a></th>
                                                <th id="joining_date" class="DESC" onclick="sort('joining_date',<?php echo $limit; ?>);"><a> Joining Date </a></th>
                                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                                </th>   
                                            </tr>
                                            
                            </thead>

                            <tbody>
                                <?php //echo "<pre>"; print_r($userData);die;
                                if(count($userData) > 0){
                                    foreach( $userData as $ud){ 
                                ?>
                                    <tr class="even pointer" id="user<?php echo $ud ['User']['id']; ?>">
                                        <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
                                                <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
                                                <td style="width:25%;" class=" "><?php echo $ud ['User']['email'];?> </td>
                                                <td style="width:14%;" class=" "><?php echo !empty($ud ['Country']['country_name']) ? $ud ['Country']['country_name'] : 'N/A';?> </td>
                                                <td style="width:15%;" class=" "><?php echo !empty($ud ['Profession']['profession_type']) ? $ud ['Profession']['profession_type'] : 'N/A';?> </td>
                                                <td style="width:10%;" class=" "><?php echo date('d-m-Y', strtotime($ud ['User']['registration_date']));?></td>
                                        <td style="width:5%;" class=" ">
                                                    <span id="statusButton<?php echo $ud ['User']['id']; ?>">
                                                    <?php 
                                                     if($ud ['User']['status'] == 1){
                                                    ?>
                                                    
                                                        <button type="button" onclick="updateStatus(<?php echo $ud ['User']['id']; ?>, 'Active');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                                    </span>
                                                    <?php }else{ ?>
                                                    
                                                        <button onclick="updateStatus(<?php echo $ud ['User']['id']; ?>, 'Inactive');" type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                                                       
                                                    <?php } ?>
                                                    </span> 
                                                    <span class="text-center" id="approveButton<?php echo $ud ['User']['id']; ?>">
                                                    <?php 
                                                     if($ud ['User']['approved'] == 1){
                                                    ?>
                                                        <button type="button"  onclick="updateApproveStatus(<?php echo $ud ['User']['id']; ?>, 'Approved');" data-original-title="Approved" class="btn btn-circle btn-icon-only btn-info3 tooltips" aria-describedby="tooltip852273"><i class="fa fa-thumbs-up"></i></button>
                                                    <?php }else{ ?>
                                                        <button type="button" onclick="updateApproveStatus(<?php echo $ud ['User']['id']; ?>, 'Unapproved');" data-original-title="Unapproved" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip62607"><i class="fa fa-thumbs-o-down"></i></button>
                                                    <?php } ?>
                                                    </span>
                                                </td>
                                        <td style="width:1%;" class=" last">
                                           <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="getModalBox(<?php echo $ud ['User']['id']; ?>)"><i class="fa fa-eye"></i>View</button>
                                           
                                            </td>
                                        </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="8"><font color="red">Record(s) Not  Found!</font></td>
                                      </tr> 
                                      <?php } ?>     
                                            </tbody>

                                    </table><?php 
                            if(count($userData) > 0){  
                                echo $this->element('pagination');
                            }
                        ?>