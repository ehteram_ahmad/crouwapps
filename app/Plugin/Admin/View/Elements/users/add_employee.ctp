<?php
if(isset($userData['UserProfile']['userEmployments'])){
foreach ($userData['UserProfile']['userEmployments'] as $value) {
    if($value['UserEmployment']['is_current']==1){
      $to="Present";
    }else{
      $to=date("F-Y", strtotime($value['UserEmployment']['to_date']));
    }
?>
<div class="row">
  <div class="col-md-2 col-xs-12">
    <div class="profile-userpic leftFloat"> <img src="<?php echo BASE_URL.'admin/img/institute.png'; ?>" class="img-responsive" alt=""> </div>
  </div>
  <div class="col-md-10 col-xs-12">
    <div class="profile-usertitle-name2"> <?php echo $value['Company']['company_name']; ?> </div>
    <div class="profile-usertitle">
      <div class="profile-stat-text"> <?php echo $value['Designation']['designation_name']; ?> <span> <?php echo $value['UserEmployment']['location']; ?> </span></div>
    </div>
    <div class="profile-stat2 width100full">
      <div class="profile-stat-text"> <?php
        $from=date("F-Y", strtotime($value['UserEmployment']['from_date']));
         echo $from." - ".$to; ?> </div>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-xs-12">
    <p class="eduText"><?php echo $value['UserEmployment']['description']; ?></p>
  </div>
</div>
<?php }}
 ?>