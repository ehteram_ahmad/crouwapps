<table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>
            <?php
            $count = 1;
            foreach ($headers as $value) {
              if($count == 1){
                $width = '60%';
              }else{
                $width = '20%';
              }
              echo '<th style="width:'.$width.';padding-left: 15px;">'.$value.'</th>';
              $count++;
            }
             ?>
        </tr>
    </thead>
    <tbody>
      <?php
      if(!empty($data)){
        foreach ($data as $list):
      ?>
        <tr>
          <?php echo isset($list['profession'])? '<td style="padding-left: 15px;">'.$list['profession'].'</td>':''; ?>
          <?php echo isset($list['today'])? '<td style="padding-left: 15px;">'.$list['today'].'</td>':''; ?>
          <?php echo isset($list['total'])? '<td style="padding-left: 15px;">'.$list['total'].'</td>':''; ?>
        </tr>
      <?php endforeach;
    }else{ ?>
      <tr>
         <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
      </tr>
    <?php } ?>
    </tbody>
</table>
