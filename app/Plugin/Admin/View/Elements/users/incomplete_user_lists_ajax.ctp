<table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
                                                <th> Platform </th>
                                                <th > OS version </th>
                                                <th > Source </th>
                                                <th id="Date" class="DESC" onclick="sort('Date',<?php echo $limit; ?>);"><a> Date </a></th>
                                            </tr>
                                        </thead>
                                    <input type="text" class="hidden" value="<?php echo $limit; ?>" id="perPg" name="">
                                        <tbody>
                                            <?php 
                                        $count=count($userData);
                                        if( $count > 0){
                                        for ($i=0; $i<$count ; $i++){
                                        ?>
                                    <tr class="even pointer" id="user<?php echo $userData[$i]['TempRegistration']['id']; ?>">
                                        <td style="width:38%;"><?php echo $userData[$i]['TempRegistration']['email'];?></td>
                                        <td style="width:15%;" class="text"><?php echo $userData[$i]['TempRegistration']['device_type'];?> </td>
                                        <td style="width:15%;"><?php echo $userData[$i] ['TempRegistration']['version'];?> </td>
                                        <td style="width:10%;"><?php echo $userData[$i] ['TempRegistration']['registration_source'];?> </td>
                                        <td style="width:27%;">
                                        <?php 
                                         $datetime = explode(" ",$userData[$i]['TempRegistration']['created']);
                                         $date = $datetime[0];
                                         $time = $datetime[1];
                                         echo date('d-m-Y',strtotime($date))." / ".date ('h:i:s A',strtotime($time));
                                         ?> </td>
                                        
                                    </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="6" align="center"><font color="red">No Record(s) Found!</font></td>
                                      </tr> 
                                      <?php } ?> 
                                        </tbody>
                                    </table>
                                    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                                    <div class="actions marBottom10 paddingSide15">
                                    <?php 
                                                if($count > 0){
                                                     echo $this->element('pagination');
                                                }
                                            ?>
                                </div>