
    <table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>
            <!-- <th><input type="checkbox" id="selectAll"></th> -->
            <th id="first_name" class="DESC" onclick="sort('first_name',<?php echo $limit; ?>);"><a> First Name </a></th>
            <th id="last_name" class="DESC" onclick="sort('last_name',<?php echo $limit; ?>);"><a> Last Name </a></th>
            <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
            <th id="country" class="DESC" onclick="sort('country',<?php echo $limit; ?>);"><a> Admin Role </a></th>
            <th id="profession" class="DESC" onclick="sort('profession',<?php echo $limit; ?>);"><a> Status </a></th>
            <th id="joining_date" class="DESC" onclick="sort('joining_date',<?php echo $limit; ?>);"><a> Created Date </a></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        // echo "<pre>"; print_r($userData);die;
if(count($adminUser) > 0){
foreach( $adminUser as $ud){ 
?>
<tr class="even pointer" id="user<?php echo $ud['User']['id']; ?>">
    <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
    <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
    <td style="width:25%;" class=" "><?php echo $ud ['AdminUser']['email'];?> </td>
    <?php 
     if($ud['AdminUser']['status'] == 1){
    ?>
    <td style="width:35%;" class=" "><?php echo "Admin";?></td>
    <?php }else{ ?>
    <td style="width:35%;" class=" "><?php echo "Sub-Admin";?></td>
    <?php } ?>

     <?php 
     if($ud['AdminUser']['status'] == 1){
    ?>
    <td style="width:35%;" class=" "><?php echo "Active";?></td>
    <?php }else{ ?>
    <td style="width:35%;" class=" "><?php echo "Inactive";?></td>
    <?php } ?>

    <td style="width:25%;" class=" "><?php echo $ud ['AdminUser']['added_date'];?> </td>
    </tr>
 <?php } }else{?> 

  <tr>
        <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
  </tr> 
  <?php } ?> 
    </tbody>
</table>

    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
    <div class="actions marBottom10 paddingSide15">
    <?php 
                if(count($adminUser) > 0){  
                    echo $this->element('pagination');
                }
            ?>
</div>
<script type="text/javascript">
$(".deleteModalBtn").click(function(){
  $("#deleteModal").modal('show');
  $('#delUser').val($(this).val());
});
 $('.tooltips').tooltip();
     // Group Action Icons
     var items = $(this).find('.circle button');
     for(var i = 0, l = items.length; i < l; i++) {
         items[i].style.right = ((18)*i).toFixed(4) + "%";
         items[i].style.top = (50).toFixed(4) + "%";
     }
     $('.menu-button').click(function() {
         $(this).parent('.circular-menu').toggleClass('open');
         $('#cn-overlay').toggleClass('on-overlay');
         return false;
     });
     $('.circular-menu .circle').click(function() {
         $(this).parent('.circular-menu').removeClass('open');
         $('#cn-overlay').removeClass('on-overlay');
     });
</script>