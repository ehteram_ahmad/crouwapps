<table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <!--<th><input type="checkbox" id="selectAll"></th>-->
                                                <th> First Name </th>
                                                <th> Last Name </th>
                                                <th> Email </th>
                                                <th> Country </th>
                                                <th> Profession </th>
                                                <th> Joining Date </th>
                                                <th> Status </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php //echo "<pre>"; print_r($userData);die;
                                if(count($userData) > 0){
                                    foreach( $userData as $ud){ 
                                ?>
                                    <tr class="even pointer" id="user<?php echo $ud ['User']['id']; ?>">
                                        <!--<td> <input type="checkbox"> </td>-->
                                        <td class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
                                        <td class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
                                        <td class=" "><?php echo $ud ['User']['email'];?> </td>
                                        <td class=" "><?php echo !empty($ud ['Country']['country_name']) ? $ud ['Country']['country_name'] : 'N/A';?> </td>
                                        <td class=" "><?php echo !empty($ud ['Profession']['profession_type']) ? $ud ['Profession']['profession_type'] : 'N/A';?> </td>
                                        <td class=" "><?php echo date('d-m-Y', strtotime($ud ['User']['registration_date']));?></td>
                                        <td class=" ">
                                            <span id="statusButton<?php echo $ud ['User']['id']; ?>">
                                            <?php 
                                             if($ud ['User']['status'] == 1){
                                            ?>
                                            
                                                <button type="button" onclick="updateStatus(<?php echo $ud ['User']['id']; ?>, 'Active');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                            </span>
                                            <?php }else{ ?>
                                            
                                                <button onclick="updateStatus(<?php echo $ud ['User']['id']; ?>, 'Inactive');" type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                                               
                                            <?php } ?>
                                            </span> 
                                            <span id="approveButton<?php echo $ud ['User']['id']; ?>">
                                            <?php 
                                             if($ud ['User']['approved'] == 1){
                                            ?>
                                                <button type="button"  onclick="updateApproveStatus(<?php echo $ud ['User']['id']; ?>, 'Approved');" data-original-title="Approved" class="btn btn-circle btn-icon-only btn-info3 tooltips" aria-describedby="tooltip852273"><i class="fa fa-thumbs-up"></i></button>
                                            <?php }else{ ?>
                                                <button type="button" onclick="updateApproveStatus(<?php echo $ud ['User']['id']; ?>, 'Unapproved');" data-original-title="Unapproved" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip62607"><i class="fa fa-thumbs-o-down"></i></button>
                                            <?php } ?>
                                            </span>
                                        </td>
                                        <td class="last">
                                            <?php /* ?><button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalBox" onclick="getModalBox(<?php echo $ud ['User']['id']; ?>)"><i class="fa fa-eye"></i></button>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#updateProfile" onclick="getEditUserProfile(<?php echo $ud ['User']['id']; ?>);"><i class="fa fa-pencil-square-o"></i></button>
                                            <button type="button" class="btn btn-info delBt" data-toggle="modal" data-target="#myModal3" onclick="deleteUser(<?php echo $ud ['User']['id']; ?>);"><i class="fa fa-times"></i></button><?php */ ?>
                                        <button data-target="#modalBox" onclick="userView(<?php echo $ud ['User']['id']; ?>)" type="button" data-original-title="View" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-eye"></i></button>
                                        <!--<button data-toggle="modal" data-target="#updateProfile" onclick="getEditUserProfile(<?php echo $ud ['User']['id']; ?>);" type="button" data-original-title="Edit" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-pencil-square-o"></i></button>-->
                                        <button type="button" data-original-title="Delete" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-toggle="modal" data-target="#myModal3" onclick="deleteUser(<?php echo $ud ['User']['id']; ?>);"><i class="fa fa-times"></i></button>
                                        </td>
                                        
                                        </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="9" align="center"><font color="red">Record(s) Not  Found!</font></td>
                                      </tr> 
                                      <?php } ?> 
                                        </tbody>
                                    </table>
                                
                                
                                    <div class="actions marBottom10 paddingSide15">
                                        <div class="btn-group marBottom10">
                                            <!--<a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> Group Action
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu bottom-up2">
                                                <li>
                                                    <a href="javascript:;">Activate</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Deactivate</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Approve</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Unapprove</a>
                                                </li>
                                            </ul>-->
                                        </div>
                                        <?php 
                                                    if(count($userData) > 0){  
                                                        echo $this->element('pagination');
                                                    }
                                                ?>
                                    </div>