<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Speciality</h4>
    </div>
        <?php echo $this->Form->create('addSpeciality',array('class'=>'form-horizontal','type'=>'file')); ?>
        <?php echo $this->Form->text('type',array('class'=>'hidden','value'=>'edit')); ?>
        <?php echo $this->Form->text('id',array('class'=>'hidden','value'=>$specilities['Specilities']['id'])); ?>
    <div class="modal-body">
            <div class="form-group">
                <label class="control-label col-md-4">Speciality Name</label>
                <div class="col-md-8">
                    <div class="col-md-9">
                        <?php echo $this->Form->text('name',array('class'=>'form-control','value'=>$specilities['Specilities']['name'],'id'=>'spclName')); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                                <label class="control-label col-md-4">Speciality Icon</label>
                                <div class="col-md-8">
                                    <span id="icon2" class="btn btn-lg success btn-file" onclick="">
                                        <img id="icon2" src="<?php echo BASE_URL . 'img/specilities/'.$specilities['Specilities']['img_name']; ?>">
                                        <?php echo $this->Form->file('icon',array('id'=>'spclIcon','class'=>'form-control','onchange'=>'readURL(this,"icon2");')); ?>
                                    </span>
                                </div>
                            </div>
            <div class="form-group">
            <?php 
            if($specilities['Specilities']['status']==1){
                $chckd1="checked";
                $chckd2="";
            }
            else{
                $chckd1="";
                $chckd2="checked";
            }

             ?>
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="col-md-9">
                                        <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="data[addSpeciality][status]" id="optionsRadios4" value="1"  <?php echo $chckd1; ?>> Active </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="data[addSpeciality][status]" id="optionsRadios5" value="0"  <?php echo $chckd2; ?>> Inactive </label>
                                    </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn green2" data-dismiss="modal" onclick="addSpeciality();">Add</button> -->
                        <button class="btn green2" type="submit" >Save</button>
                        <button class="btn default" data-dismiss="modal" aria-hidden="true" onclick="rstSpeciality();">Cancel</button>
                    </div>
                        </form>
                </div>