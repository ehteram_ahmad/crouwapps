<table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>
            <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> Template Name </a></th>
            <th id="Created" class="DESC txtCenter" onclick="sort('Created',<?php echo $limit; ?>);"><a> Created Date </a></th>
            <th id="Modified" class="DESC txtCenter" onclick="sort('Modified',<?php echo $limit; ?>);"><a> Modified Date </a></th>
            <th class="txtCenter width90px"> Status </th>
            <th class="txtCenter width140px"> Action </th>
        </tr>
    </thead>
    <tbody>
    <?php 
    if(count($data) > 0){
    foreach ($data as $value) {
    ?>
        <tr>
            <td style="width: 40%;"> <?php echo $value['Emailtemplate']['name']; ?> </td>
            <td style="width: 25%;" class="txtCenter"> <?php echo $value['Emailtemplate']['created'];  ?> </td>
            <td style="width: 25%;" class="txtCenter"> <?php echo $value['Emailtemplate']['modified']; ?> </td>
            <td style="width: 5%;" class="txtCenter">
                <?php 
                 if($value['Emailtemplate']['status'] == 1){
                ?>
                    <button type="button" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                <?php }else{ ?>
                    <button type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                <?php } ?>
            </td>
            <td style="width: 5%;" class="beatBtnBottom4 txtCenter">
              <div class="radialMenu menuButton3">
              <nav class="circular-menu">
                <div class="circle">
                    <span id="statusButton<?php echo $value['Emailtemplate']['id']; ?>">
                      <?php 
                       if($value['Emailtemplate']['status'] == 1){
                      ?>
                          <button type="button" onclick="updateEmailStatus(<?php echo $value['Emailtemplate']['id']; ?>, 'Active');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                      </span>
                      <?php }else{ ?>
                          <button onclick="updateEmailStatus(<?php echo $value['Emailtemplate']['id']; ?>, 'Inactive');" type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i style="color: black" class="fa fa-check"></i></button>
                         </span>
                      <?php } ?>
                  <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="View" onclick="javascript:window.location.href='emaildetail/<?php echo $value['Emailtemplate']['id'];?>'"><i class="fa fa-eye"></i></button>
                <button type="button" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Edit" onclick="javascript:window.location.href='emaileditdetail/<?php echo $value['Emailtemplate']['id'];?>'"><i class="fa fa-pencil-square-o"></i></button>
                <button onclick="deleteEmail(<?php echo $value['Emailtemplate']['id'].','."'".$value['Emailtemplate']['name']."'"; ?>);" type="button" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
              </div>
                <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
              </nav>
              </div>
            </td>
        </tr> 
        <?php }}else{ ?>
        <tr>
              <td colspan="5" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr> 
        <?php  } ?>
    </tbody>
</table>
<div class="row">
    <div class="col-md-12 col-sm-12">
    <?php echo $this->element('pagination'); ?>
    </div>
</div>
<script>
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});

</script> 
