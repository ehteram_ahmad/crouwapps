<?php
/*foreach ($country as  $value) {
    // pr($value['countries']);
}*/
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>

<?php
  echo $this->Html->script(array('Admin.ckeditor/ckeditor.js'));   
?>
<style type="text/css">
      div.checker, div.checker span, div.checker input {
      width: 35px;
      height: 30px;

  }
  .toggle{
    border-radius: 20px!important;
    width: 9.5px;
    height: 0px;
    position: absolute;
    left: -25px;
    top: 0px;
  }
  #presentTxt label{
    position: relative;
    top:8px;
  }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper"> 
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content"> 
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head"> 
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>User Details</h1>
      </div>
      <!-- END PAGE TITLE --> 
    </div>
    <!-- END PAGE HEAD--> 
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
          <a href="<?php echo BASE_URL . 'admin/home/dashboard'; ?>">Home</a>
          <i class="fa fa-circle"></i>
      </li>
      <li>

          <a href="<?php echo BASE_URL . 'admin/QrCode/qrCodeIndex'; ?>">History</a>
          <i class="fa fa-circle"></i>
      </li>
      <li>
          <span class="active">Batch Details</span>
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB --> 
    <!-- BEGIN PAGE TOOLBAR -->
    <div class="page-toolbar buttonTop">
      <div class="">

        <button type="button" class="btn default marLeft5px" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/QrCode/qrCodeIndex'; ?>'; return false;">Go Back</button>
      </div>
    </div>
    <!-- END PAGE TOOLBAR --> 
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">`
      <div class="col-md-12"> 
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content"> 
          <!-- BEGIN PAGE BASE CONTENT -->
          <div class="row">
            <div class="col-md-12">
             
              <div class="portlet light bordered customePadding">
                <div class="portlet-title">
                  <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase"><?php echo $batchId; ?></span> </div>
                  <div class="tools"> <a href="javascript:;" class="collapse"> </a> </div>
                </div>
                <div class="clearfix"></div>
                <div id="content" class="paddingSide15">

                <table class="table table-bordered table-striped table-condensed flip-content">
                  <thead class="flip-content">
                      <tr>
                          <!-- <th><input type="checkbox" id="selectAll"></th> -->
                          <th><a> Qr Code Number </a></th>
                          <th><a> Institute Name </a></th>
                          <th><a> Status </a></th>
                          <th><a> Valid </a></th>
                          <th><a> Action </a></th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php foreach( $batchData as $batchDetail){ ?>
                    <tr class="even pointer">
                    <td style="width:10%;" class=" "><?php echo $batchDetail['QrCodeDetail']['qr_code_number'];?></td>
                    <td style="width:30%;" class=" "><?php echo $batchDetail['CompanyName']['company_name'];?></td>
                    <td style="width:10%;" class=" "><?php
                        if($batchDetail['QrCodeDetail']['is_valid'] == 0)
                        {
                          echo "Invalid";
                        }
                        else{
                          if($batchDetail['QrCodeDetail']['is_used'] == 0)
                          {
                            echo "Used";
                          }
                          else{
                            echo "Active";
                          }
                        }
                        ?>
                    </td>
                    <td style="width:10%;" class=" "><?php 
                      if($batchDetail['QrCodeDetail']['is_valid'] == 0)
                      {
                        echo "Invalid";
                      }
                      else{
                        echo "Valid";
                      }
                      ?>
                    </td>
                    <td style="width:1%;" class="last"><?php
                      if($batchDetail['QrCodeDetail']['is_valid'] == 1){ ?>
                        <button type="button" id="deleteModalBtn" data-original-title="Expire QR code" data-batch="<?php echo $batchId; ?>" value="<?php echo $batchDetail['QrCodeDetail']['qr_code_number']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips deleteModalBtn" data-toggle="modal" >
                          <i class="fa fa-trash-o"></i>
                        </button>
                      <?php } else{
                        echo "-- NA --";
                      }?>
                    </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                </div>
              </div>
          <!-- END PAGE BASE CONTENT --> 
        </div>
        <!-- END PROFILE CONTENT --> 
      </div>
    </div>
    <!-- END PAGE BASE CONTENT --> 
  </div>
  <!-- END CONTENT BODY --> 
</div>

<!-- BEGIN DELETE POP-UP --> 
 <div id="deleteModal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
   <div class="modal-dialog">
     <div class="modal-content"> 
       <div class="modal-header"> 
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Reason of Expiation</h4>
       </div>
       <div class="modal-body">
         <div class="form-group">
             <label class="control-label">Enter Reason below</label>
             <textarea id="deleteComment" name="deleteComment" class="form-control" rows="3" placeholder="comments"></textarea>
         </div>
       </div>
       <div class="modal-footer textCenter">
         <button id="delUser" value="" onclick="expireQRCode();" type="button" data-dismiss="modal" class="btn green2"><i class="fa fa-times" aria-hidden="true"></i>  Delete </button>
         <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
       </div>
   </div>
  </div>
 </div>
<!-- END DELETE POP-UP --> 
<!-- END CONTENT -->

<!-- Registration Image popup [START]-->
    
<!-- Registration Image popup [END]-->
                             
<!-- Profile Image popup [START]-->
    
<!-- Profile Image popup [END]-->

<!-- BEGIN DELETE POP-UP --> 
    
<!-- END DELETE POP-UP --> 

<!-- BEGIN ADD EDUCATION POP-UP --> 
    
<!-- END ADD EDUCATION POP-UP -->

<!-- BEGIN ADD Employment POP-UP --> 
<!-- END ADD Employment POP-UP --> 
<script>
  $(".deleteModalBtn").click(function(){
    $("#deleteModal").modal('show');
    $('#delUser').val($(this).val());
    $('#delUser').attr('data-batch',$(this).attr('data-batch'));
  });

$("#menu>ul>li").eq(14).addClass('active open');
$(".sub-menu>li").eq(18).addClass('active open');

function expireQRCode(){
    QRCodeNumber=$('#delUser').val();
    batchNumber = $('#delUser').attr('data-batch');
    if($.trim($('#deleteComment').val())==""){
        alert("Please Eneter The Reason.");
        $('#deleteComment').focus();
        return false;
    }
    if(confirm("Are you sure you want to delete this QR Code?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/QrCode/expireQRCodeNumber',
                 type: "POST",
                 data: {'batchNumber':batchNumber, 'QRCodeNumber': QRCodeNumber,'reason':$('#deleteComment').val()},
                 success: function(data) { 
                    console.log(data);
                    location.reload(true);
                }
            });
    }
    $('#deleteComment').val('');
  return false; 
}
</script>