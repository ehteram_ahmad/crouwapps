<?php
/*foreach ($country as  $value) {
    // pr($value['countries']);
}*/
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Request Search</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <!--<div class="page-toolbar">
                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                        <i class="icon-calendar"></i>&nbsp;
                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>-->
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo BASE_URL . 'admin/QrCode/requestQrCodeListing';?>">Qr code requset</a>
                    <i class="fa fa-circle"></i>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box blue2 ">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="fa fa-cogs"></i>--> Search </div>
                            <div class="tools">
                                
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="" class="remove"> </a>
                                <a href="" class="reload"> </a>-->
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return searchUser();">
                                <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php 
                                                foreach ($companyNames as  $company) {
                                                $companyList[$company['CompanyName']['id']]=$company['CompanyName']['company_name'];
                                                }
                                        echo $this->Form->select('company',$companyList,array('class'=>'form-control','empty'=>'Choose Institution','id'=>'company')); ?>
                                    </div>
                                </div>
                             </div>
                                <div class="form-actions center-block txt-center">
                                    <button type="submit" class="btn green2" name="btn_search" value="Search" >Search</button>
                                    <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    
                    <!-- END SAMPLE TABLE PORTLET-->
                
                    <div class="portlet box blue2">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="fa fa-cogs"></i>-->Qr Code Request List </div>
                            <div class="tools">
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="remove"> </a>
                                <a href="javascript:;" class="reload"> </a>-->
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                            
                            <!-- </div> -->
                            <div class="clearfix"></div>
                            <div id="content" class="paddingSide15">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <!-- <th><input type="checkbox" id="selectAll"></th> -->
                                                <th id="first_name" class="DESC"><a> Request Id </a></th>
                                                <th id="first_name" class="DESC"><a> Institute Name </a></th>
                                                <th id="last_name" class="DESC"><a> Number Of Cards </a></th>
                                                <th id="email" class="DESC"><a> validity </a></th>
                                                <th id="profession" class="DESC"><a> Requested at </a></th>
                                                <th><a> Action </a></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            // echo "<pre>"; print_r($userData);die;
                                    if(count($qrCodeData) > 0){
                                    foreach( $qrCodeData as $qrd){ 
                                    ?>
                                    <tr class="even pointer">
                                        <td style="width:15%;" class=" "><?php echo $qrd['QrCodeRequest']['id'];?></td>
                                        <td style="width:35%;" class=" "><?php echo $qrd['CompanyName']['company_name'];?></td>
                                        <td style="width:15%;" class=" "><?php echo $qrd['QrCodeRequest']['number_of_qr_codes'];?></td>
                                        <td style="width:10%;" class=" "><?php echo $qrd['QrCodeRequest']['validity'];?> </td>
                                        <td style="width:20%;" class=" "><?php echo $qrd ['QrCodeRequest']['requested_at'];?> </td>
                                       <td style="width:1%;" class="last">
                                            <div class="radialMenu menuButton3">
                                            <nav class="circular-menu">
                                              <div class="circle">

                                                <button type="button" onclick="genrateQrCode(<?php echo $qrd ['CompanyName']['id']; ?>, <?php echo $qrd['QrCodeRequest']['validity']; ?>, <?php echo $qrd['QrCodeRequest']['number_of_qr_codes']; ?>,<?php echo $qrd['QrCodeRequest']['id'];?>);" data-original-title="Genrate Qr Code" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-toggle="modal" ><i class="fa fa-eye"></i></button>

                                                <button type="button" id="deleteModalBtn" data-original-title="Reject Request" value="<?php echo $qrd['QrCodeRequest']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips deleteModalBtn" data-toggle="modal" ><i class="fa fa-trash-o"></i></button>
                                              </div>
                                              <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
                                            </nav>
                                         </div>
                                        </td>
                                     <?php } }else{?> 
                                      <tr>
                                            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                      </tr> 
                                      <?php } ?> 
                                        </tbody>
                                    </table>


                                    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                                    <div class="actions marBottom10 paddingSide15">
                                    <?php 
                                                if(count($qrCodeData) > 0){  
                                                    echo $this->element('pagination');
                                                }
                                            ?>
                                </div>
                         </div>   
                    </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </div>
</div>
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?> 
<!-- BEGIN DELETE POP-UP --> 
 <div id="deleteModal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
   <div class="modal-dialog">
     <div class="modal-content"> 
       <div class="modal-header"> 
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Reason of Deletion</h4>
       </div>
       <div class="modal-body">
         <div class="form-group">
             <label class="control-label">Enter Reason below</label>
             <textarea id="deleteComment" name="deleteComment" class="form-control" rows="3" placeholder="comments"></textarea>
         </div>
       </div>
       <div class="modal-footer textCenter">
         <button id="requestId" value="" onclick="deleteUser();" type="button" data-dismiss="modal" class="btn green2"><i class="fa fa-times" aria-hidden="true"></i>  Delete </button>
         <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
       </div>
   </div>
  </div>
 </div>
<!-- END DELETE POP-UP --> 
<div id="cn-overlay" class="cn-overlay"></div>
<script>
$(document).ready(function() {
    // $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".datePick").datepicker({
    dateFormat: 'yy-mm-dd'
});
 
});
function sort(val,limit){
    $(".loader").fadeIn("fast");
        var a=$('#'+val).attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='DE';
        }
        else{
            var order='A';
        }
    $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/allFilter',
                 type: "POST",
                 data: {'sort':val,'order':order,'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'limit':limit,'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'country':$('#countrySearch').val(),'profession':$('#profession').val()},
                 success: function(data) { 
                    // alert(data);
                    $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    else{
                    $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
      return false; 
    }


function genrateQrCode(instituteId, validity, numberOfQrCode, requestId){

  $(".loader").fadeIn("fast");
   $.ajax({
           url: '<?php echo BASE_URL; ?>admin/QrCode/genrateQrCode',
           type: "POST",
           data: {'institute_id': instituteId, 'validity':validity, 'number_of_qr_codes':numberOfQrCode, 'request_id':requestId},
           success: function(data) { 
              console.log(data);
              // $('#content').html('Qr Code genrate successfully');
              alert('Qr Code genrate successfully');
              $(".loader").fadeOut("fast");
              location.reload();
          }
      });
return false;  
}
function searchUser(){
    $(".loader").fadeIn("fast");
     $.ajax({
             url: '<?php echo BASE_URL; ?>admin/QrCode/listFilter',
             type: "POST",
             data: {'company':$('#company').val()},
             success: function(data) { 
                $('#content').html(data);
                $(".loader").fadeOut("fast");
            }
        });
  return false;  
}
//** Delete user
$(".deleteModalBtn").click(function(){
  $("#deleteModal").modal('show');
  $('#requestId').val($(this).val());
});

function deleteUser(){
    requestId=$('#requestId').val();
    if($.trim($('#deleteComment').val())==""){
        alert("Please Eneter The Reason.");
        $('#deleteComment').focus();
        return false;
    }
    if(confirm("Are you sure you want to delete this Request?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/QrCode/rejectQrCodeRequest',
                 type: "POST",
                 data: {'requestId': requestId,'reason':$('#deleteComment').val()},
                 success: function(data) { 
                  console.log(data);
                    alert(data);
                    location.reload(true);
                }
            });
    }
    $('#deleteComment').val('');
  return false; 
}
function xyz(){
var FirstName=$('#textFirstName').val();
var LastName=$('#textLastName').val();
var Email=$('#textEmail').val();
var specilities=$('#multi-prepend').val();
var country=$('#countrySearch').val();
var profession=$('#profession').val();
var statusVal=$('#selectStatus').val();
var from = $('#dateFrom').val();
var to = $('#dateTo').val();
if(specilities==null)
{
    specilities="";
}
// alert(specilities);
window.open('<?php echo BASE_URL; ?>admin/user/userCsvDownload/?FirstName='+FirstName+'&LastName='+LastName+'&specilities='+specilities+'&statusVal='+statusVal+'&Email='+Email+'&from='+from+'&to='+to+'&country='+country+'&profession='+profession, '_blank');
}

 function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $(".loader").fadeIn("fast");
    $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/QrCode/listFilter',
              type: "POST",
              data: {'company': $('#company').val(),'j':goVal,'limit':limit,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 function chngCount(val){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/QrCode/listFilter',
              type: "POST",
              data: {'company': $('#company').val(),'limit':val,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

function abc(val,limit){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/QrCode/listFilter',
           type: "POST",
           data: {'company': $('#company').val(),'j':val,'limit':limit,'sort':a,'order':order},
           success: function(data) { 
                $('#content').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
       });
}
$("#menu>ul>li").eq(14).addClass('active open');
$(".sub-menu>li").eq(17).addClass('active open');
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script>