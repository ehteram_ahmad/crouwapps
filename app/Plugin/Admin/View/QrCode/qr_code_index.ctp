<?php
/*foreach ($country as  $value) {
    // pr($value['countries']);
}*/
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Genrate Qr Code</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <!--<div class="page-toolbar">
                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                        <i class="icon-calendar"></i>&nbsp;
                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>-->
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo BASE_URL . 'admin/user/userLists';?>">Users</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">User List</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box blue2 ">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="fa fa-cogs"></i>--> Gerate Qr Code </div>
                            <div class="tools">
                                
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="" class="remove"> </a>
                                <a href="" class="reload"> </a>-->
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return genrateQrCode();">
                                <div class="form-body">
                                <div class="form-group">
                                        
                                        <div class="col-md-6">
                                            <input onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" type="text" class="form-control input-sm" placeholder="Number of Qr code" name="number_of_qrcode" id="number_of_qrcode">
                                        </div>
                                <!-- </div> -->
                                <!-- <div class="form-group"> -->
                                    <div class="col-md-6">
                                        <?php 
                                        $dayList = array(1=>'1 Day',7=>'1 Week',14=>'2 Week',30=>'1 Month',90=>'3 Month',180=>'6 Month',365=>'1 Year');
                                        echo $this->Form->select('validity',$dayList,array('class'=>'form-control','empty'=>'Choose Validity','id'=>'validity')); ?>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <?php 
                                                foreach ($companyNames as  $company) {
                                                $companyList[$company['CompanyName']['id']]=$company['CompanyName']['company_name'];
                                                }
                                        echo $this->Form->select('company',$companyList,array('class'=>'form-control','empty'=>'Choose Institution','id'=>'company')); ?>
                                    </div>
                                </div>
                                <div class="form-actions center-block txt-center">
                                    <button type="submit" class="btn green2" name="btn_search" value="Search" >Genrate Qr Code</button>
                                    <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                    <!-- END SAMPLE FORM PORTLET-->
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    
                    <!-- END SAMPLE TABLE PORTLET-->
                
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>-->Qr Code Details </div>
                        <div class="tools">
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="remove"> </a>
                            <a href="javascript:;" class="reload"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                    <div class="mainAction marBottom10">
                    <div class="leftFloat">
                     <div class="actions">
                            <div class="btn-group">
                                <!-- <a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> Filter
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <div class="col-md-2"> -->
                                <select class="btn btn-md green2" aria-expanded="false" id = "selectStatus" name="selectStatus" onchange="return searchUser();" >
                                        <!-- <option value="">Filter</option> -->
                                        <option value="0" <?php if($searchdata['status'] == 0 ){?> selected <?php } ?>> --Filter-- </option>
                                        <option value="1" <?php if($searchdata['status'] == 1 ){?> selected <?php } ?>>Email ID Verified &amp; Unapproved</option>
                                        <option value="2" <?php if($searchdata['status'] == 2){?> selected <?php } ?>>Email ID Unverified &amp; Approved</option>
                                        <option value="3" <?php if($searchdata['status'] == 3){?> selected <?php } ?>>Email ID Verified &amp; Approved (Active)</option>
                                        <option value="4" <?php if($searchdata['status'] == 4){?> selected <?php } ?>>Email ID Unverified &amp; Unapproved (Inactive)</option>
                                        <option value="5" <?php if($searchdata['status'] == 5){?> selected <?php } ?>>Deleted</option>
                                    </select>

                               </div>
                            </div>
                        </div>
                        <div class="rightFloat">
                        <div class="actions marRight10">
                            <!--<div class="btn-group">
                                <a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-plus"></i> Add New User
                                </a>
                            </div>-->
                        </div>
                        <div class="actions">
                            <div class="btn-group">
                                <button class="btn btn-sm green2 dropdown-toggle" onclick="xyz();" type="button" class="btn btn-primary"> <i class="fa fa-external-link"></i> Export </button>
                            </div>
                        </div>
                        </div>
                        </div>
                        
                        <!-- </div> -->
                        <div class="clearfix"></div>
                        <div id="content" class="paddingSide15">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                    <tr>
                                        <!-- <th><input type="checkbox" id="selectAll"></th> -->
                                        <th id="first_name" class="DESC"><a> Request Id </a></th>
                                        <th id="country" class="DESC"><a> Institute Name </a></th>
                                        <th id="first_name" class="DESC"><a> Batch Number </a></th>
                                        <th id="last_name" class="DESC"><a>QrCode's </a></th>
                                        <th id="email" class="DESC"><a> Validity </a></th>
                                        <th id="email" class="DESC"><a> Status </a></th>
                                        <th id="joining_date" class="DESC"><a> Created On </a></th>
                                        <th><a> Action </a></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    // echo "<pre>"; print_r($userData);die;
                            if(count($qrCodeRequests) > 0){
                            foreach( $qrCodeRequests as $request){ 
                            ?>
                            <tr class="even pointer" id="user<?php echo $request['QrCodeRequest']['id']; ?>">
                                <td style="width:10%;" class=" "><?php echo $request['QrCodeRequest']['id'];?></td>
                                <td style="width:35%;" class=" "><?php echo $request ['CompanyName']['company_name'];?> </td>
                                <td style="width:15%;" class=" "><?php if(!empty($request['batch_number'])){
                                    echo $request['batch_number']['QrCodeDetail']['batch_number'];
                                }else{
                                    echo "-- NA --";
                                } ?></td>
                                <td style="width:5%;" class=" "><?php echo $request['QrCodeRequest']['number_of_qr_codes'];?> </td>
                                <td style="width:5%;" class=" "><?php echo $request ['QrCodeRequest']['validity'];?> </td>
                                <?php if($request ['QrCodeRequest']['status'] == 1) {
                                    $text = "Code generated";
                                }else if($request ['QrCodeRequest']['status'] == 3){
                                    $text = "Batch expired";
                                }else{
                                    $text = "Request rejected";
                                } ?>
                                <td style="width:20%;" class=" "><?php echo $text;?> </td>
                                <td style="width:25%;" class=" "><?php echo $request ['QrCodeRequest']['requested_at'];?> </td>
                                <td style="width:1%;" class="last">
                                    <?php if(!empty($request['batch_number'])){ ?>
                                        <div class="radialMenu menuButton3">
                                    <nav class="circular-menu">
                                      <div class="circle leftNegative115px">

                                        <button data-toggle="modal" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/QrCode/qrCodeBatchView/' . $request['batch_number']['QrCodeDetail']['batch_number'] ; ?>'; return false;" type="button" data-original-title="view" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-eye"></i></button>
                                        <?php
                                        if($request ['QrCodeRequest']['status'] != 3){ ?>
                                            <button type="button" id="deleteModalBtn" data-original-title="Expire Batch Number" value="<?php echo $request['batch_number']['QrCodeDetail']['batch_number']; ?>" data-rid="<?php echo $request['QrCodeRequest']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips deleteModalBtn" data-toggle="modal" ><i class="fa fa-trash-o"></i></button>
                                       <?php }?>
                                      </div>
                                      <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
                                    </nav>
                                 </div>
                                   <?php }else{
                                        echo "";
                                    } ?>
                                </td>
                                </tr>
                             <?php }} else { ?> 

                              <tr>
                                    <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                              </tr> 
                              <?php } ?> 
                                </tbody>
                            </table>


                            <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                            <div class="actions marBottom10 paddingSide15">
                            <?php 
                                        if(count($qrCodeRequests) > 0){  
                                            echo $this->element('pagination');
                                        }
                                    ?>
                            </div>
                     </div>   
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </div>
</div>
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?> 
<!-- BEGIN DELETE POP-UP --> 
 <div id="deleteModal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
   <div class="modal-dialog">
     <div class="modal-content"> 
       <div class="modal-header"> 
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Reason of Expiation</h4>
       </div>
       <div class="modal-body">
         <div class="form-group">
             <label class="control-label">Enter Reason below</label>
             <textarea id="deleteComment" name="deleteComment" class="form-control" rows="3" placeholder="comments"></textarea>
         </div>
       </div>
       <div class="modal-footer textCenter">
         <button id="delUser" value="" onclick="expireBatchNumber();" type="button" data-dismiss="modal" class="btn green2"><i class="fa fa-times" aria-hidden="true"></i>  Delete </button>
         <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
       </div>
   </div>
  </div>
 </div>
<!-- END DELETE POP-UP --> 
<div id="cn-overlay" class="cn-overlay"></div>
<script>
$(document).ready(function() {
    // $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".datePick").datepicker({
    dateFormat: 'yy-mm-dd'
});

    function QrCodeBatchView( val ){
        window.location.href =  "<?php echo BASE_URL; ?>admin/QrCode/qrCodeBatchView/" + val;
    }

 
});

function genrateQrCode(){
    var numberQr = $('#number_of_qrcode').val();
    var companyId = $('#company').val();
    var validity = $('#validity').val();
    if(numberQr == '' || numberQr == 0 ){
        alert("Please enter a valid number of cards.");
        return false;  
    }
    if(validity == ''){
        alert("Please select Validity.");
        return false;  
    }
    if(companyId == ''){
        alert("Please select Institute.");
        return false;  
    }
    $(".loader").fadeIn("fast");
    $.ajax({
         url: '<?php echo BASE_URL; ?>admin/QrCode/genrateQrCodeByAdmin',
         type: "POST",
         data: {'number_of_qr_codes': $('#number_of_qrcode').val(),'validity':validity, 'institute_id':$('#company').val()},
         success: function(data) { 
            console.log(data);
            // $('#content').html('Qr Code genrate successfully');
            alert('Qr Code genrated successfully');
            $(".loader").fadeOut("fast");
            location.reload(true);
        }
    });
  return false;  
}
//** Delete user
$(".deleteModalBtn").click(function(){
  $("#deleteModal").modal('show');
  $('#delUser').val($(this).val());
  $('#delUser').attr('data-rid',$(this).attr('data-rid'));
});
function expireBatchNumber(){
    batchNumber=$('#delUser').val();
    requestId=$('#delUser').attr('data-rid');
    if($.trim($('#deleteComment').val())==""){
        alert("Please Eneter The Reason.");
        $('#deleteComment').focus();
        return false;
    }
    if(confirm("Are you sure you want to delete this Batch?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/QrCode/expireBatchNumber',
                 type: "POST",
                 data: {'requestId':requestId, 'batchNumber': batchNumber,'reason':$('#deleteComment').val()},
                 success: function(data) { 
                    console.log(data);
                    location.reload(true);
                }
            });
    }
    $('#deleteComment').val('');
  return false; 
}


 function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $(".loader").fadeIn("fast");
    $.ajax({
              url:'<?php echo BASE_URL; ?>admin/QrCode/qrCodeIndexFilter',
              type: "POST",
              data: {'j':goVal,'limit':limit,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 function chngCount(val){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/QrCode/qrCodeIndexFilter',
              type: "POST",
              data: {'limit':val,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

function abc(val,limit){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/QrCode/qrCodeIndexFilter',
           type: "POST",
           data: {'j':val,'limit':limit,'sort':a,'order':order},
           success: function(data) { 
                $('#content').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
       });
}
$("#menu>ul>li").eq(14).addClass('active open');
$(".sub-menu>li").eq(18).addClass('active open');
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script>