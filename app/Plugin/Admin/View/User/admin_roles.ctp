<?php
/*foreach ($country as  $value) {
    // pr($value['countries']);
}*/
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Admin Search</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <!--<div class="page-toolbar">
                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                        <i class="icon-calendar"></i>&nbsp;
                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>-->
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo BASE_URL . 'admin/user/userLists';?>">Admins</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Admin List</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box blue2 ">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="fa fa-cogs"></i>--> Admin Search </div>
                            <div class="tools">
                                
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="" class="remove"> </a>
                                <a href="" class="reload"> </a>-->
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return searchUser();">
                                <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php 
                                                foreach ($companyNames as  $company) {
                                                $companyList[$company['CompanyName']['id']]=$company['CompanyName']['company_name'];
                                                }
                                        echo $this->Form->select('company',$companyList,array('class'=>'form-control','empty'=>'Choose Institution','id'=>'company')); ?>
                                    </div>
                                </div>
                             </div>
                                <div class="form-actions center-block txt-center">
                                    <button type="submit" class="btn green2" name="btn_search" value="Search" >Search</button>
                                    <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    
                    <!-- END SAMPLE TABLE PORTLET-->
                
                    <div class="portlet box blue2">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="fa fa-cogs"></i>-->
                            Admin List </div>
                            <div class="tools">
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="remove"> </a>
                                <a href="javascript:;" class="reload"> </a>-->
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                        <div class="mainAction marBottom10">
                        <div class="leftFloat">
                         <div class="actions">  
                                </div>
                            </div>
                            <div class="rightFloat">
                            <div class="actions marRight10">
                                <!--<div class="btn-group">
                                    <a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-plus"></i> Add New User
                                    </a>
                                </div>-->
                            </div>
                            </div>
                            </div>
                            
                            <!-- </div> -->
                            <div class="clearfix"></div>
                            <div id="content" class="paddingSide15">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <!-- <th><input type="checkbox" id="selectAll"></th> -->
                                                <th id="first_name" class="DESC" onclick="sort('first_name',<?php echo $limit; ?>);"><a> First Name </a></th>
                                                <th id="last_name" class="DESC" onclick="sort('last_name',<?php echo $limit; ?>);"><a> Last Name </a></th>
                                                <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
                                                <th id="country" class="DESC" onclick="sort('country',<?php echo $limit; ?>);"><a> Admin Role </a></th>
                                                <th id="profession" class="DESC" onclick="sort('profession',<?php echo $limit; ?>);"><a> Status </a></th>
                                                <th id="joining_date" class="DESC" onclick="sort('joining_date',<?php echo $limit; ?>);"><a> Created Date </a></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            // echo "<pre>"; print_r($userData);die;
                                    if(count($adminUser) > 0){
                                    foreach( $adminUser as $ud){ 
                                    ?>
                                    <tr class="even pointer" id="user<?php echo $ud['User']['id']; ?>">
                                        <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
                                        <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
                                        <td style="width:25%;" class=" "><?php echo $ud ['AdminUser']['email'];?> </td>
                                        <?php 
                                         if($ud['AdminUser']['role_id'] == 0){
                                        ?>
                                        <td style="width:35%;" class=" "><?php echo "Admin";?></td>
                                        <?php }else{ ?>
                                        <td style="width:35%;" class=" "><?php echo "Sub-Admin";?></td>
                                        <?php } ?>

                                         <?php 
                                         if($ud['AdminUser']['status'] == 1){
                                        ?>
                                        <td style="width:35%;" class=" "><?php echo "Active";?></td>
                                        <?php }else{ ?>
                                        <td style="width:35%;" class=" "><?php echo "Inactive";?></td>
                                        <?php } ?>

                                        <td style="width:25%;" class=" "><?php echo $ud ['AdminUser']['added_date'];?> </td>
                                        </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                      </tr> 
                                      <?php } ?> 
                                        </tbody>
                                    </table>


                                    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                                    <div class="actions marBottom10 paddingSide15">
                                    <?php 
                                                if(count($adminUser) > 0){  
                                                    echo $this->element('pagination');
                                                }
                                            ?>
                                </div>
                         </div>   
                    </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </div>
</div>
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?>  
<div id="cn-overlay" class="cn-overlay"></div>
<script>
$(document).ready(function() {
    // $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".datePick").datepicker({
    dateFormat: 'yy-mm-dd'
});
 
});
$('#company').on('change',function(){
    institute_id = $(this).val();
    if(institute_id != ''){
        $('#selectSubscription').prop('disabled', false);
    }else{
        $('#selectSubscription').val(0).prop('disabled', 'disabled');
    }
});

function sort(val,limit){
    $(".loader").fadeIn("fast");
        var a=$('#'+val).attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='DE';
        }
        else{
            var order='A';
        }
    $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/adminFilters',
                 type: "POST",
                 data: {'sort':val,'order':order,'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'limit':limit,'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'country':$('#countrySearch').val(),'profession':$('#profession').val(), 'company':$('#company').val(), 'subscriptionVal': $('#selectSubscription').val()},
                 success: function(data) { 
                    // alert(data);
                    $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    else{
                    $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
      return false; 
    }
function searchUser(){
    // alert("Test");
    $(".loader").fadeIn("fast");
     $.ajax({
             url: '<?php echo BASE_URL; ?>admin/user/adminFilters',
             type: "POST",
             data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'country':$('#countrySearch').val(),'profession':$('#profession').val(), 'company':$('#company').val(), 'subscriptionVal': $('#selectSubscription').val()},
             success: function(data) { 
                $('#content').html(data);
                $(".loader").fadeOut("fast");
            }
        });
  return false;  
}
//** Delete user
$(".deleteModalBtn").click(function(){
  $("#deleteModal").modal('show');
  $('#delUser').val($(this).val());
});
function xyz(){
var FirstName=$('#textFirstName').val();
var LastName=$('#textLastName').val();
var Email=$('#textEmail').val();
var specilities=$('#multi-prepend').val();
var country=$('#countrySearch').val();
var profession=$('#profession').val();
var statusVal=$('#selectStatus').val();
var from = $('#dateFrom').val();
var to = $('#dateTo').val();
var company = $('#company').val();
var subscriptionVal = $('#selectSubscription').val();

if(specilities==null)
{
    specilities="";
}
// alert(specilities);
window.open('<?php echo BASE_URL; ?>admin/user/userCsvDownload/?FirstName='+FirstName+'&LastName='+LastName+'&specilities='+specilities+'&statusVal='+statusVal+'&Email='+Email+'&from='+from+'&to='+to+'&country='+country+'&profession='+profession+'&company='+company+'&subscriptionVal='+subscriptionVal, '_blank');
}

 function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $(".loader").fadeIn("fast");
    $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/user/allFilter',
              type: "POST",
              data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'j':goVal,'limit':limit,'sort':a,'order':order,'country':$('#countrySearch').val(),'profession':$('#profession').val(), 'company':$('#company').val(), 'subscriptionVal': $('#selectSubscription').val()},
              success: function(data) { 
                  $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 function chngCount(val){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/user/allFilter',
              type: "POST",
              data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'limit':val,'sort':a,'order':order,'country':$('#countrySearch').val(),'profession':$('#profession').val(), 'company':$('#company').val(), 'subscriptionVal': $('#selectSubscription').val()},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

function abc(val,limit){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/user/allFilter',
           type: "POST",
           data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'j':val,'limit':limit,'sort':a,'order':order,'country':$('#countrySearch').val(),'profession':$('#profession').val(), 'company':$('#company').val(), 'subscriptionVal': $('#selectSubscription').val()},
           success: function(data) { 
                $('#content').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
       });
}
$("#menu>ul>li").eq(1).addClass('active open');
$(".sub-menu>li").eq(0).addClass('active open');
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script> 


