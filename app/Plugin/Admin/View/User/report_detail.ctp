<?php
// print_r($companyDetail);
// exit();
/*foreach ($country as  $value) {
    // pr($value['countries']);
}*/
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<!-- <div class="loader"></div> -->
<div id="MainLoader" style="display:none;" ></div>
<style type="text/css">
.loader{
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
#professionLoader,#baselocationLoader,#specialityLoader,#MainLoader {
    position: absolute;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Daily Report</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <!--<div class="page-toolbar">
                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                        <i class="icon-calendar"></i>&nbsp;
                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>-->
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo BASE_URL . 'admin/user/userReport';?>">User Report</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Daily Report</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box blue2 ">
                        <div class="portlet-title">
                            <div class="caption"> Custom Search </div>
                            <div class="tools">
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return searchUser();">
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input readonly="true" placeholder="Registration - From Date" class="form-control datePick" id="dateFrom">
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                    <div class="col-md-6">
                                        <input readonly="true" placeholder="Registration - To Date" class="form-control datePick" id="dateTo">
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                </div>
                             </div>
                                <div class="form-actions center-block txt-center">
                                    <button type="submit" class="btn green2" name="btn_search" value="Search" >Search</button>
                                    <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                    <!-- BEGIN SAMPLE TABLE PORTLET-->

                    <!-- END SAMPLE TABLE PORTLET-->
                    <div class="actions">
                        <div class="btn-group" style="padding: 2px;">
                            <button class="btn btn-sm green2 dropdown-toggle" onclick="allExport();" type="button" class="btn btn-primary"> <i class="fa fa-external-link"></i> Export All</button>
                        </div>
                    </div>
                    <div class="portlet box blue2" style="position: relative;">
                      <div id="professionLoader" ></div>
                        <div class="portlet-title">
                            <div class="caption">Profession Wise</div>
                            <div class="tools">
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="remove"> </a>
                                <a href="javascript:;" class="reload"> </a>-->
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll" style="padding: 0px!important;">
                            <div class="mainAction marBottom10">
                                <div class="rightFloat">
                                    <div class="actions">
                                        <div class="btn-group" style="padding: 2px;">
                                            <button class="btn btn-sm green2 dropdown-toggle" onclick="professionExport();" type="button" class="btn btn-primary"> <i class="fa fa-external-link"></i> Export </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- </div> -->
                            <div class="clearfix"></div>
                            <div id="contentProfession" class="paddingSide15">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th> Profession </th>
                                                <th> Today Count </th>
                                                <th> Total Count (Till Today) </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                              <td colspan="8" align="center">
                                                  <font color="green">Please wait while we are loading the list...</font>
                                              </td>
                                          </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>

                    <div class="portlet box blue2" style="position: relative;">
                      <div id="baselocationLoader" ></div>
                        <div class="portlet-title">
                            <div class="caption">Base Location Wise</div>
                            <div class="tools">
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="remove"> </a>
                                <a href="javascript:;" class="reload"> </a>-->
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll" style="padding: 0px!important;">
                            <div class="mainAction marBottom10">
                                <div class="rightFloat">
                                    <div class="actions">
                                        <div class="btn-group" style="padding: 2px;">
                                            <button class="btn btn-sm green2 dropdown-toggle" onclick="baselocationExport();" type="button" class="btn btn-primary"> <i class="fa fa-external-link"></i> Export </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- </div> -->
                            <div class="clearfix"></div>
                            <div id="contentBaseLocation" class="paddingSide15">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th> Base Location </th>
                                                <th> Today Count </th>
                                                <th> Total Count (Till Today) </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                              <td colspan="8" align="center">
                                                  <font color="green">Please wait while we are loading the list...</font>
                                              </td>
                                          </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>

                    <div class="portlet box blue2" style="position: relative;">
                      <div id="specialityLoader" ></div>
                        <div class="portlet-title">
                            <div class="caption">Speciality Wise</div>
                            <div class="tools">
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="remove"> </a>
                                <a href="javascript:;" class="reload"> </a>-->
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll" style="padding: 0px!important;">
                            <div class="mainAction marBottom10">
                                <div class="rightFloat">
                                    <div class="actions">
                                        <div class="btn-group" style="padding: 2px;">
                                            <button class="btn btn-sm green2 dropdown-toggle" onclick="specialityExport();" type="button" class="btn btn-primary"> <i class="fa fa-external-link"></i> Export </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- </div> -->
                            <div class="clearfix"></div>
                            <div id="contentSpeciality" class="paddingSide15">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th> Speciality </th>
                                                <th> Today Count </th>
                                                <th> Total Count (Till Today) </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                              <td colspan="8" align="center">
                                                  <font color="green">Please wait while we are loading the list...</font>
                                              </td>
                                          </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
            <!-- END PAGE BASE CONTENT -->
                </div>
        <!-- END CONTENT BODY -->
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->
<?php
echo $this->Js->writeBuffer();
?>
<div id="cn-overlay" class="cn-overlay"></div>
<script>
$(document).ready(function() {
    $(".datePick").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#menu>ul>li").eq(17).addClass('active open');
    $(".sub-menu>li").eq(21).addClass('active open');
    var company = "<?php echo $companyDetail['CompanyName']['id']; ?>";
    getProfessionList('','');
    getBaselocationList('','');
    getspecialityList('','');
});

function getProfessionList(from,to){
  $("#professionLoader").fadeIn("fast");
  var company = "<?php echo $companyDetail['CompanyName']['id']; ?>";
    $.ajax({
        url: '<?php echo BASE_URL; ?>admin/user/getProfessionList',
        type: "POST",
        data: {'company':company,'from':from,'to':to},
        success: function(data) {
            $('#contentProfession').html(data);
            $("#professionLoader").fadeOut("fast");
        }
    });
}

function getBaselocationList(from,to){
  $("#baselocationLoader").fadeIn("fast");
  var company = "<?php echo $companyDetail['CompanyName']['id']; ?>";
    $.ajax({
        url: '<?php echo BASE_URL; ?>admin/user/getBaselocationList',
        type: "POST",
        data: {'company':company,'from':from,'to':to},
        success: function(data) {
            $('#contentBaseLocation').html(data);
            $("#baselocationLoader").fadeOut("fast");
        }
    });
}

function getspecialityList(from,to){
  $("#specialityLoader").fadeIn("fast");
  var company = "<?php echo $companyDetail['CompanyName']['id']; ?>";
    $.ajax({
        url: '<?php echo BASE_URL; ?>admin/user/getSpecialityList',
        type: "POST",
        data: {'company':company,'from':from,'to':to},
        success: function(data) {
            $('#contentSpeciality').html(data);
            $("#specialityLoader").fadeOut("fast");
        }
    });
}

function searchUser(){
    var from = $('#dateFrom').val();
    var to = $('#dateTo').val();
    getProfessionList(from,to);
    getBaselocationList(from,to);
    getspecialityList(from,to);
  return false;
}

function allExport(){
  var from = $('#dateFrom').val();
  var to = $('#dateTo').val();
  var type = 'All';
  var company = "<?php echo $companyDetail['CompanyName']['id']; ?>";
  $("#MainLoader").fadeIn("fast");
  $.ajax({
        url: '<?php echo BASE_URL; ?>admin/user/userReportDownload',
        type: "POST",
        data: {'company':company,'from':from,'to':to,'type':type},
        success: function(data) {
          window.open(data);
            $("#MainLoader").fadeOut("fast");
        }
    });
}

function professionExport(){
  var from = $('#dateFrom').val();
  var to = $('#dateTo').val();
  var type = 'Profession';
  var company = "<?php echo $companyDetail['CompanyName']['id']; ?>";
  $("#professionLoader").fadeIn("fast");
  $.ajax({
        url: '<?php echo BASE_URL; ?>admin/user/userReportDownload',
        type: "POST",
        data: {'company':company,'from':from,'to':to,'type':type},
        success: function(data) {
          window.open(data);
            $("#professionLoader").fadeOut("fast");
        }
    });
}
function baselocationExport(){
  var from = $('#dateFrom').val();
  var to = $('#dateTo').val();
  var type = 'Base Location';
  var company = "<?php echo $companyDetail['CompanyName']['id']; ?>";
  $("#baselocationLoader").fadeIn("fast");
  $.ajax({
        url: '<?php echo BASE_URL; ?>admin/user/userReportDownload',
        type: "POST",
        data: {'company':company,'from':from,'to':to,'type':type},
        success: function(data) {
          window.open(data);
            $("#baselocationLoader").fadeOut("fast");
        }
    });
}
function specialityExport(){
  var from = $('#dateFrom').val();
  var to = $('#dateTo').val();
  var type = 'Speciality';
  var company = "<?php echo $companyDetail['CompanyName']['id']; ?>";
  $("#specialityLoader").fadeIn("fast");
  $.ajax({
        url: '<?php echo BASE_URL; ?>admin/user/userReportDownload',
        type: "POST",
        data: {'company':company,'from':from,'to':to,'type':type},
        success: function(data) {
          window.open(data);
            $("#specialityLoader").fadeOut("fast");
        }
    });
}

function xyz(){
  var FirstName=$('#textFirstName').val();
  var LastName=$('#textLastName').val();
  var Email=$('#textEmail').val();
  var specilities=$('#multi-prepend').val();
  var country=$('#countrySearch').val();
  var profession=$('#profession').val();
  var statusVal=$('#selectStatus').val();
  var from = $('#dateFrom').val();
  var to = $('#dateTo').val();
  var company = $('#company').val();
  var subscriptionVal = $('#selectSubscription').val();

  if(specilities==null)
  {
      specilities="";
  }

  window.open('<?php echo BASE_URL; ?>admin/user/userCsvDownload/?FirstName='+FirstName+'&LastName='+LastName+'&specilities='+specilities+'&statusVal='+statusVal+'&Email='+Email+'&from='+from+'&to='+to+'&country='+country+'&profession='+profession+'&company='+company+'&subscriptionVal='+subscriptionVal, '_blank');
}
</script>
