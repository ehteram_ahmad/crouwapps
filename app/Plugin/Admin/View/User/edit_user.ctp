<?php 
  $userId = $userData['UserProfile']['userId'];
  $firstName = $userData['UserProfile']['first_name'];
  $lastName = $userData['UserProfile']['last_name'];
  $email = $userData['User']['email'];
  $profileImg = (@getimagesize(AMAZON_PATH . $userData['UserProfile']['user_id']. '/profile/' . $userData['UserProfile']['profile_img'])) ? AMAZON_PATH . $userData['UserProfile']['user_id']. '/profile/' . $userData['UserProfile']['profile_img'] : 'Admin.no-image.png';
  $gender = $userData['UserProfile']['gender'];
  $contactNo = $userData['UserProfile']['contact_no'];
  $dob = (isset($userData['UserProfile']['dob']) && !empty($userData['UserProfile']['dob'])) ? date('d-m-Y', strtotime($userData['UserProfile']['dob'])) : 'N/A';
  $registrationDate = (isset($userData['User']['registration_date']) && !empty($userData['User']['registration_date'])) ? date('d-m-Y', strtotime($userData['User']['registration_date'])) : 'N/A';
  $gmcNo = !empty($userData['UserProfile']['gmcnumber']) ? $userData['UserProfile']['gmcnumber'] : 'N/A';
  $userStatus =  $userData['User']['status'];
  $address = !empty($userData['UserProfile']['address']) ? $userData['UserProfile']['address'] : 'N/A' ;
  $countryId = $userData['UserProfile']['countryId'];
  $countryName = $userData['UserProfile']['countryName'];
  $city = $userData['UserProfile']['city'];
  $county = $userData['UserProfile']['county'];
  $education = !empty($userData['UserProfile']['userEducation']) ? implode(",", $userData['UserProfile']['userEducation']) : '';
  $professionId = $userData['UserProfile']['professionId'];
  $professionName = $userData['UserProfile']['professionName'];
  $interests = $userData['UserProfile']['interestlist']; 
  $specilities = $userData['UserProfile']['specilityLists'];
  $registrationImg = (@getimagesize(AMAZON_PATH . $userId .'/certificate/'. $userData['UserProfile']['registration_image'])) ? AMAZON_PATH . $userId .'/certificate/'. $userData['UserProfile']['registration_image'] : 'Admin.no-image.png';
  $postedBeats = $userData['UserProfile']['userBeatPosted'];
  $colleagues = $userData['UserProfile']['userColleagues'];
  $followers = $userData['UserProfile']['userFollowers'];
  $following = $userData['UserProfile']['userFollowings'];
  $countryList = $userData['UserProfile']['countryList'];
  $professionList = $userData['UserProfile']['professionList'];
  $interestMasterLists = $userData['UserProfile']['interestMasterList'];
?>
<!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>User Profile | Account
                                <small>user account page</small>
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo BASE_URL . 'admin/home/dashboard'; ?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL . 'admin/user/userLists'; ?>">Users</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">User Details</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <!-- PORTLET MAIN -->
                                <div class="portlet light profile-sidebar-portlet bordered">
                                    <!-- SIDEBAR USERPIC -->
                                    <div class="profile-userpic">
                                        <a data-toggle="modal" href="#profileImg">
                                        <?php echo $this->Html->image($profileImg, array("class"=> "img-responsive")); ?> 
                                        </a>
                                    </div>
                                    <!-- END SIDEBAR USERPIC -->
                                    <!-- SIDEBAR USER TITLE -->
                                    <div class="profile-usertitle">
                                        <div class="profile-usertitle-name"> <?php echo $firstName . ' ' . $lastName; ?></div>
                                        <div class="profile-usertitle-job"> <?php echo $professionName;?> / <?php echo $gender;?></div>
                                    </div>
                                    <!-- END SIDEBAR USER TITLE -->
                                    <!-- SIDEBAR BUTTONS -->
                                    <!--<div class="profile-userbuttons">
                                        <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                        <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                    </div>-->
                                    <!-- END SIDEBAR BUTTONS -->
                                    <!-- SIDEBAR MENU -->
                                    <!--<div class="profile-usermenu">
                                        <ul class="nav">
                                            <li>
                                                <a href="page_user_profile_1.html">
                                                    <i class="icon-home"></i> Overview </a>
                                            </li>
                                            <li class="active">
                                                <a href="page_user_profile_1_account.html">
                                                    <i class="icon-settings"></i> Account Settings </a>
                                            </li>
                                            <li>
                                                <a href="page_user_profile_1_help.html">
                                                    <i class="icon-info"></i> Help </a>
                                            </li>
                                        </ul>
                                    </div>-->
                                    <!-- END MENU -->
                                </div>
                                <!-- END PORTLET MAIN -->
                                <!-- PORTLET MAIN -->
                                <div class="portlet light bordered">
                                    <!-- STAT -->
                                    <div class="row list-separated profile-stat">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="uppercase profile-stat-title"> <?php echo $postedBeats; ?> </div>
                                            <div class="uppercase profile-stat-text"> Posted Beat </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="uppercase profile-stat-title"> <?php echo $colleagues; ?> </div>
                                            <div class="uppercase profile-stat-text"> Colleagues </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="uppercase profile-stat-title"> <?php echo $followers; ?> </div>
                                            <div class="uppercase profile-stat-text"> Followers </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="uppercase profile-stat-title"> <?php echo $following; ?> </div>
                                            <div class="uppercase profile-stat-text"> Followings </div>
                                        </div>
                                    </div>
                                    <!-- END STAT -->
                                    <div>
                                        <!--<h4 class="profile-desc-title">About Methew Taranoc</h4>
                                        <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>-->
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-envelope"></i>
                                            <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                        </div>
                                        <div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-globe"></i>
                                            <a><?php echo $countryName; ?></a>
                                        </div>
                                        <!--<div class="margin-top-20 profile-desc-link">
                                            <i class="fa fa-facebook"></i>
                                            <a href="http://www.facebook.com/keenthemes/">keenthemes</a>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="portlet light bordered">
                                <h4 class="profile-desc-title">Verification Proof</h4>
                                        
                                    <!-- STAT -->
                                    <div class="row list-separated">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                        <span>&nbsp;</span>
                                            <a data-toggle="modal" href="#long">
                                                <?php echo $this->Html->image($registrationImg, array("alt"=>"")); ?>
                                            </a>
                                        </div>
                                        </div></div>
                                <!-- END PORTLET MAIN -->
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                </div>
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                                    </li>
                                                    <!--<li>
                                                        <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                                    </li>-->
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <!-- PERSONAL INFO TAB -->
                                                    <div class="tab-pane active" id="tab_1_1">
                                                        <form role="form" action="#" method="post" id="generalInfo">
                                                            <div class="form-group">
                                                                <label class="control-label">First Name</label>
                                                                <input type="text" value="<?php echo $firstName; ?>" class="form-control" name="firstName" /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Last Name</label>
                                                                <input type="text" value="<?php echo $lastName; ?>" class="form-control" name="lastName"/> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Contact Number</label>
                                                                <input type="text" value="<?php echo $contactNo; ?>" class="form-control" name="contactNumber"/> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Date of Birth</label>
                                                                <input type="text" placeholder="DD-MM-YYYY" class="form-control form-control-inline input-medium date-picker"   value="<?php echo $dob; ?>" name="dob" data-date-format="dd-mm-yyyy"/> 
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Date of Joining</label>
                                                                <input type="text" placeholder="DD-MM-YYYY" class="form-control form-control-inline input-medium date-picker" value="<?php echo $registrationDate; ?>" name="joiningDate" data-date-format="dd-mm-yyyy"/> <i class="fa fa-calendar" aria-hidden="true"></i></div>
                                                            <div class="form-group">
                                                                <label class="control-label">Registration / GMC Number</label>
                                                                <input type="text" value="<?php echo $gmcNo;?>" class="form-control" name="gmcNumber" /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Status</label>
                                                                <select class="form-control"  id="userStatus" name="userStatus">
                                                                    <option <?php echo ($userStatus == 1 ) ? "Selected" : ''; ?>  value="1">Active</option>
                                                                    <option <?php echo ($userStatus == 0 ) ? "Selected" : ''; ?> value="0">Inactive</option>
                                                                </select>
                                                            </div>
                                                                <div class="form-group">
                                                                <label class="control-label">Address</label>
                                                                <textarea class="form-control" rows="3" placeholder="401, Sewa Park" name="address">
                                                                    <?php echo $address; ?>
                                                                </textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">City</label>
                                                                <input type="text" value="<?php echo $city; ?>" class="form-control" name="city" /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">State / County</label>
                                                                <input type="text" value="<?php echo $county; ?>" class="form-control" name="county" /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Country</label>
                                                                <select class="form-control"  id="countryList" name="countryList">
                                                                    <?php foreach($countryList as $countryL){  ?>
                                                                    <option <?php echo ($countryL['id'] == $countryId ) ? "Selected" : ''; ?> value="<?php echo $countryL['id']; ?>"><?php echo $countryL['name']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Education</label>
                                                                <input type="text" value="<?php echo $education; ?>" class="form-control" name="education" /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Profession</label>
                                                                <select id="userProfession" name="userProfession" class="form-control">
                                                                    <?php foreach($professionList as $professionL){ ?>
                                                                    <option <?php echo ($professionL['id'] == $professionId ) ? "Selected" : ''; ?> value="<?php echo $professionL['id']; ?>"><?php echo $professionL['name']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Interests</label><select name="interests[]"  multiple="" class="form-control select2 select2-hidden-accessible" id="multiInterest" tabindex="-1" aria-hidden="true">
                                                
                                                            <optgroup label="">
                                                                <?php 
                                                                    foreach($interestMasterLists as $interest){
                                                                        if(in_array($interest['id'],$interests)){
                                                                            echo '<option value="'.$interest['id'].'" selected > '.$interest['name'].'</option>';
                                                                        }else{
                                                                            echo '<option value="'.$interest['id'].'"> '.$interest['name'].'</option>';
                                                                        }
                                                                ?>
                                                                
                                                                <?php } ?>
                                                            </optgroup>
                                                        </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Specialties</label>
                                                                <select name="specilities[]"  multiple="" class="form-control select2 select2-hidden-accessible" id="multiSpecilities" tabindex="-1" aria-hidden="true">
                                                
                                                            <optgroup label="">
                                                                <?php 
                                                                    foreach($interestMasterLists as $interest){
                                                                        if(in_array($interest['id'],$specilities)){
                                                                            echo '<option value="'.$interest['id'].'" selected > '.$interest['name'].'</option>';
                                                                        }else{
                                                                            echo '<option value="'.$interest['id'].'"> '.$interest['name'].'</option>';
                                                                        }
                                                                ?>
                                                                
                                                                <?php } ?>
                                                            </optgroup>
                                                        </select>

                                            </div>
                                                            <div class="margiv-top-10">
                                                                <a href="javascript:;" class="btn green2" onclick="editUserProfile(<?php echo $userId; ?>); "> Save Changes </a>
                                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- END PERSONAL INFO TAB -->
                                                    <!-- CHANGE AVATAR TAB -->
                                                    <div class="tab-pane" id="tab_1_2">
                                                        <p>  </p>
                                                        <form action="<?php echo BASE_URL; ?>admin/user/updateUserProfileImage" role="form" id="formChangeAvatar" enctype="multipart/form-data" method="post">
                                                            <input type="hidden" name="hiddenUserId" value="<?php echo $userId; ?>">
                                                            <div class="form-group">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <?php echo $this->Html->image($profileImg, array("class"=> "img-responsive")); ?>
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select image </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" id="profileImg" name="profileImg"> </span>
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                <!--<div class="clearfix margin-top-10">
                                                                    <span class="label label-danger">NOTE! </span>
                                                                    <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                                </div>-->
                                                            </div>
                                                            <div class="margin-top-10">
                                                                <!--<a href="javascript:;" class="btn green2" > Submit </a>-->
                                                                <input type="submit" value="Submit" class="btn green2">
                                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- END CHANGE AVATAR TAB -->
                                                    <!-- CHANGE PASSWORD TAB -->
                                                    <div class="tab-pane" id="tab_1_3">
                                                        <form action="#" id="formResetPassword">
                                                            <div class="form-group">
                                                                <label class="control-label">Current Password</label>
                                                                <input type="password" class="form-control" name="currentPassword" id="currentPassword" required /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">New Password</label>
                                                                <input type="password" class="form-control" name="newPassword" id="newPassword" required  /> </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Re-type New Password</label>
                                                                <input type="password" class="form-control" name="confirmNewPassword" id="confirmNewPassword" required  /> </div>
                                                            <div class="margin-top-10">
                                                                <a href="javascript:;" class="btn green2" onclick="resetUserPassword(<?php echo $userId; ?>);"> Change Password </a>
                                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- END CHANGE PASSWORD TAB -->
                                                    <!-- PRIVACY SETTINGS TAB -->
                                                    <!--<div class="tab-pane" id="tab_1_4">
                                                        <form action="#">
                                                            <table class="table table-light table-hover">
                                                                <tr>
                                                                    <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                                                    <td>
                                                                        <label class="uniform-inline">
                                                                            <input type="radio" name="optionsRadios1" value="option1" /> Yes </label>
                                                                        <label class="uniform-inline">
                                                                            <input type="radio" name="optionsRadios1" value="option2" checked/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                    <td>
                                                                        <label class="uniform-inline">
                                                                            <input type="checkbox" value="" /> Yes </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                    <td>
                                                                        <label class="uniform-inline">
                                                                            <input type="checkbox" value="" /> Yes </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                    <td>
                                                                        <label class="uniform-inline">
                                                                            <input type="checkbox" value="" /> Yes </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            
                                                            <div class="margin-top-10">
                                                                <a href="javascript:;" class="btn red"> Save Changes </a>
                                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                                            </div>
                                                        </form>
                                                    </div>-->
                                                    <!-- END PRIVACY SETTINGS TAB -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

            <!-- Registration Image popup [START]-->
            <div id="long" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>-->
                                                    <h4 class="modal-title">Verification Proof</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <?php echo $this->Html->image($registrationImg, array("alt"=> "")); ?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                                                </div>
                                            </div>
                                        </div>
        </div>
        <!-- Registration Image popup [END]-->

        <!-- Profile Image popup [START]-->
            <div id="profileImg" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>-->
                                                    <h4 class="modal-title">Profile</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <?php echo $this->Html->image($profileImg, array("alt"=> "")); ?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                                                </div>
                                            </div>
                                        </div>
        </div>
        <!-- Profile Image popup [END]-->

<script type="text/javascript">
//** Edit profile
function editUserProfile(userId){ 
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/editUserProfileAccount',
                 type: "POST",
                 data: $('#generalInfo').serialize() + "&userId=" + userId,
                 success: function(data) { 
                    alert(data);
                    window.location = '<?php echo BASE_URL; ?>admin/user/editUser/' + userId;
                }
            });
}

//** Edit avatar
function changeUserAvatar(userId){
    $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/updateUserProfileImage',
                 type: "POST",
                 data: $('#formChangeAvatar').serialize() + "&userId=" + userId,
                 success: function(data) { 
                    alert(data);
                    
                }
            });
}

//** Reset User Password
function resetUserPassword(userId){
    if($("#currentPassword").val() == ''){
        alert("Enter Current Password");
        return false;
    }
    else if($("#newPassword").val() == ''){
        alert("Enter New Password");
        return false;
    }
    else if($("#confirmNewPassword").val() == ''){
        alert("Enter Confirm Password");
        return false;
    }
    else if($("#newPassword").val() != $("#confirmNewPassword").val()){
        alert("New Password and Confirm Password not match.");
        return false;
    }else{
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/resetUserPassword',
                 type: "POST",
                 data: $('#formResetPassword').serialize() + "&userId=" + userId,
                 success: function(data) { 
                    alert(data);
                    $('#formResetPassword')[0].reset();
                }
            });
    }
}


</script>