
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="closeModal();">&times;</button>
          <h4 class="modal-title">User Info</h4>
        </div>
        <div class="modal-body">
          <table style="width:100%">
            <tr>
              <td width="25%" align="center">
                <?php $profileImage =  !empty($userData['UserProfile']['profile_img']) ? AMAZON_PATH . $userData['User']['id']. '/profile/' . $userData['UserProfile']['profile_img']:'';
                if (@getimagesize($profileImage)  ){
                ?>
                  <img src="<?php echo $profileImage; ?>" width="100px;" height="100px;">
                <?php 
                  } else{
                    echo $this->Html->image('profilePicNotFound.png', array("width"=> "100px", "height"=>"100px"));
                  }

                ?>

              </td>
              <td width="50%">
            </tr>
          </table>
          <table border="0">
            <?php 
            if( !empty($userData) ){
            ?>
              <tr>
                <td width="50%"><strong>First Name</strong></td>
                <td width="5%">:</td>
                <td width="45%"><?php echo $userData['UserProfile']['first_name']; ?></td>
              </tr>
              <tr>
                <td width="50%"><strong>Last Name</strong></td>
                <td width="5%">:</td>
                <td width="45%"><?php echo $userData['UserProfile']['last_name']; ?></td>
              </tr>
              <?php if(!(in_array($this->Session->read('userName'), array('subadmin1')))){?>
              <tr>
                <td width="50%"><strong>Email</strong></td>
                <td width="5%">:</td>
                <td width="45%"><?php echo $userData['User']['email']; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td width="50%"><strong>Profession</strong></td>
                <td width="5%">:</td>
                <td width="45%"><?php echo !empty($userData['Profession']['profession_type']) ? $userData['Profession']['profession_type'] : 'N/A'; ?></td>
              </tr>
              <?php if(!(in_array($this->Session->read('userName'), array('subadmin1')))){?>
              <tr>
                <td width="50%"><strong>Contact No</strong></td>
                <td width="5%">:</td>
                <td width="45%"><?php echo !empty($userData['UserProfile']['contact_no'])?$userData['UserProfile']['contact_no']:'N/A'; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td width="50%"><strong>Registration Date</strong></td>
                <td width="5%">:</td>
                <td width="45%"><?php echo date('d-m-Y', strtotime($userData['User']['registration_date'])); ?></td>
              </tr>
              <tr>
                <td width="50%"><strong>Status</strong></td>
                <td width="5%">:</td>
                <td width="45%"><?php echo ($userData['User']['status'] == 1 ? "Verified":"Unverified"); ?></td>
              </tr>
              <?php 
                  if(count($userData['UserProfile']['interestlist']) > 0){
                      $str = "";
                      foreach($userData['UserProfile']['interestlist'] as $interests){
                          if($str == "") {
                              $str .= $interests['specilities']['name'];
                          }else{
                              $str .= ', '.$interests['specilities']['name'];
                          }
                      }
              ?>
              <tr>
                <td width="50%" valign="top"><strong>Interests</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php echo $str; ?></td>
              </tr>
              <?php
                  }
              ?>
              <tr>
                <td width="50%" valign="top"><strong>Country</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php  echo !empty($userData['UserProfile']['country']) ? $userData['UserProfile']['country']:'N/A'; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>County</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php  echo !empty($userData['UserProfile']['county']) ? $userData['UserProfile']['county']:'N/A'; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>City</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php  echo !empty($userData['UserProfile']['city']) ? $userData['UserProfile']['city']:'N/A'; ?></td>
              </tr>
              <?php if(!(in_array($this->Session->read('userName'), array('subadmin1')))){?>
              <tr>
                <td width="50%" valign="top"><strong>Contact No.</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php  echo !empty($userData['UserProfile']['contact_no']) ? $userData['UserProfile']['contact_no']:'N/A'; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td width="50%" valign="top"><strong>Address</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php  echo !empty($userData['UserProfile']['address']) ? $userData['UserProfile']['address']:'N/A'; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>Gender</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%">
                  <?php  
                    if($userData['UserProfile']['gender'] == 'M'){
                      echo "Male";
                    }elseif($userData['UserProfile']['gender'] == 'F'){
                      echo "Female";
                    }else{
                      echo "N/A";
                    } 
                    ?>
                  </td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>DOB</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php  echo (!empty($userData['UserProfile']['dob']) && $userData['UserProfile']['dob'] != '0000-00-00') ? $userData['UserProfile']['dob']:'N/A'; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>No. of Posted Beat</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php echo !empty($userData['UserProfile']['userBeatPosted']) ? $userData['UserProfile']['userBeatPosted'] : 0; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>No. of Colleagues</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php echo !empty($userData['UserProfile']['userColleagues']) ? $userData['UserProfile']['userColleagues'] : 0; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>No. of Followers</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php echo !empty($userData['UserProfile']['userFollowers']) ? $userData['UserProfile']['userFollowers'] : 0; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>No. of Followings</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php echo !empty($userData['UserProfile']['userFollowings']) ? $userData['UserProfile']['userFollowings'] : 0; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>Educational Details</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%">
                  <table border="0">
                  <?php 
                    if(!empty($userData['UserProfile']['userInstitutions'])){
                  ?>
                    <tr>
                      <th>InstituteName</th>
                      <th>EducationDegree</th>
                      <th>From Date</th>
                      <th>To Date</th>
                    </tr>
                  <?php 
                      foreach($userData['UserProfile']['userInstitutions'] as $userInstitutes){
                  ?>
                    <tr>
                      <td><?php echo !empty($userInstitutes['InstituteName']['institute_name']) ? $userInstitutes['InstituteName']['institute_name'] : 'N/A' ; ?></td>
                      <td><?php echo !empty($userInstitutes['EducationDegree']['degree_name']) ? $userInstitutes['EducationDegree']['degree_name'] : 'N/A' ; ?></td>
                      <td><?php echo !empty($userInstitutes['UserInstitution']['from_date']) ? date('d-m-Y', strtotime($userInstitutes['EducationDegree']['from_date'])) : 'N/A' ; ?></td>
                      <td><?php echo !empty($userInstitutes['UserInstitution']['to_date']) ? date('d-m-Y', strtotime($userInstitutes['EducationDegree']['to_date'])) : 'N/A' ; ?></td>
                    </tr>
                  <?php 

                      }

                  ?>
                  <?php 
                    }
                  ?>
                  <tr><td colspan="4">N/A</td></tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>Employment Details</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%">
                  <table border="0">
                  <?php 
                    if(!empty($userData['UserProfile']['userEmployments'])){
                  ?>
                    <tr>
                      <th>Company Name</th>
                      <th>Designation</th>
                      <th>From Date</th>
                      <th>To Date</th>
                      <th>Location</th>
                      <th>Description</th>
                      <th>Is Current</th>
                    </tr>
                  <?php 
                      foreach($userData['UserProfile']['userEmployments'] as $userEmployments){
                  ?>
                    <tr>
                      <td><?php echo !empty($userEmployments['Company']['company_name']) ? $userEmployments['Company']['company_name'] : 'N/A' ; ?></td>
                      <td><?php echo !empty($userEmployments['Designation']['designation_name']) ? $userEmployments['Designation']['designation_name'] : 'N/A' ; ?></td>
                      <td><?php echo !empty($userEmployments['UserEmployment']['from_date']) ? $userEmployments['UserEmployment']['from_date'] : 'N/A' ; ?></td>
                      <td><?php echo !empty($userEmployments['UserEmployment']['to_date']) ? $userEmployments['UserEmployment']['to_date'] : 'N/A' ; ?></td>
                      <td><?php echo !empty($userEmployments['UserEmployment']['location']) ? $userEmployments['UserEmployment']['location'] : 'N/A' ; ?></td>
                      <td><?php echo !empty($userEmployments['UserEmployment']['description']) ? $userEmployments['UserEmployment']['description'] : 'N/A' ; ?></td>
                      <td><?php echo ($userEmployments['UserEmployment']['is_current'] == 1) ? 'Yes': 'No' ; ?></td>
                    </tr>
                  <?php 

                      }

                  ?>
                
                  <?php 
                    }else{
                  ?>
                  <tr><td colspan="7">N/A</td></tr>
                  <?php } ?>
                  </table>
                </td>
              </tr>
              <?php /* ?><tr>
                <td width="50%" valign="top"><strong>Status</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%">
                  <?php 
                  echo ($userData['User']['status'] == 1) ? 'Verified' : 'Unverified';
                  echo ' | ';
                  echo ($userData['User']['approved'] == 1) ? 'Approved' : 'Unapproved';
                  ?>
                </td>
              </tr><?php */ ?>
              <tr>
                <td width="50%" valign="top"><strong>GMC Number</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%"><?php  echo !empty($userData['UserProfile']['gmcnumber']) ? $userData['UserProfile']['gmcnumber']:'N/A'; ?></td>
              </tr>
              <tr>
                <td width="50%" valign="top"><strong>Registration Image</strong></td>
                <td width="5%" valign="top">:</td>
                <td width="45%" align="center">
                  <?php $regImage = !empty($userData['UserProfile']['registration_image']) ? AMAZON_PATH . $userData['UserProfile']['user_id']. '/certificate/' . $userData['UserProfile']['registration_image']:''; 
                  if (@getimagesize($regImage)  ){
                ?>
                  <img src="<?php echo $regImage; ?>" width="200px;" height="200px;">
                <?php 
                  } else{
                    echo $this->Html->image('profilePicNotFound.png', array("width"=> "100px", "height"=>"100px"));
                  }

                ?>
                </td>
              </tr>
            <?php
            }else{ 
            ?> 
            <tr><td colspan="3">User Info Not Found</td></tr>
            <?php 
            }
            ?>
</table>
        </div>
      </div>
    </div>
  </div>
</div>

