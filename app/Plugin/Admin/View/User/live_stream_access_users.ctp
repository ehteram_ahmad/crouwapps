<?php
foreach ($country as  $value) {
    // pr($value['countries']);
}
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>User Search</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <!--<div class="page-toolbar">
                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                        <i class="icon-calendar"></i>&nbsp;
                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>-->
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>admin/user/liveStreamAccessUsers">Users</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">User List</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet box blue2 ">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="fa fa-cogs"></i>--> User Search </div>
                            <div class="tools">
                                
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="" class="remove"> </a>
                                <a href="" class="reload"> </a>-->
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return searchUser();">
                                <div class="form-body">
                                    
                                    
                                    <div class="form-group">
                                        
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" placeholder="First Name" name="textFirstName" id="textFirstName"> </div>
                                            <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" placeholder="Last Name" name="textLastName" id="textLastName"> </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="email" class="form-control input-sm" placeholder="Email" name="textEmail" id="textEmail"> </div>
                                        <div class="col-md-6">
                                            <select name="specilities[]"  multiple="" class="form-control select2 spec select2-hidden-accessible" id="multi-prepend" tabindex="-1" aria-hidden="true">
                                        
                                        <optgroup label="">
                                            <?php 
                                                foreach($interestLists['interestlist'] as $interest){
                                                    echo '<option value="'.$interest['specilities']['id'].'" > '.$interest['specilities']['name'].'</option>';}
                                            ?>
                                        </optgroup>
                                    </select>
                                        </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input placeholder="Registration - From Date" class="form-control datePick" id="dateFrom">
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                    <div class="col-md-6">
                                        <input placeholder="Registration - To Date" class="form-control datePick" id="dateTo">
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php 
                                                foreach ($country as  $value) {
                                                $countries[$value['countries']['id']]=$value['countries']['country_name'];
                                                }
                                        echo $this->Form->select('country',$countries,array('class'=>'form-control','empty'=>'Choose Country','id'=>'countrySearch')); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php 
                                                foreach ($professions as  $value) {
                                                $profession[$value['professions']['id']]=$value['professions']['profession_type'];
                                                }
                                        echo $this->Form->select('profession',$profession,array('class'=>'form-control','empty'=>'Choose Profession','id'=>'profession')); ?>
                                    </div>
                                </div>
                             </div>
                                <div class="form-actions center-block txt-center">
                                    <button type="submit" class="btn green2" name="btn_search" value="Search" >Search</button>
                                    <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    
                    <!-- END SAMPLE TABLE PORTLET-->
                
                    <div class="portlet box blue2">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--<i class="fa fa-cogs"></i>-->User List </div>
                            <div class="tools">
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="remove"> </a>
                                <a href="javascript:;" class="reload"> </a>-->
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                        <div class="mainAction marBottom10">
                        <div class="leftFloat">
                         <div class="actions">
                                <div class="btn-group">
                                    <!-- <a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> Filter
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="col-md-2"> -->
                                    <select class="btn btn-md green2" aria-expanded="false" id = "selectStatus" name="selectStatus" onchange="return searchUser();" >
                                            <!-- <option value="">Filter</option> -->
                                            <option value="0" <?php if($searchdata['status'] == 0 ){?> selected <?php } ?>> --Filter-- </option>
                                            <option value="1" <?php if($searchdata['status'] == 1 ){?> selected <?php } ?>>Email ID Verified &amp; Unapproved</option>
                                            <option value="2" <?php if($searchdata['status'] == 2){?> selected <?php } ?>>Email ID Unverified &amp; Approved</option>
                                            <option value="3" <?php if($searchdata['status'] == 3){?> selected <?php } ?>>Email ID Verified &amp; Approved (Active)</option>
                                            <option value="4" <?php if($searchdata['status'] == 4){?> selected <?php } ?>>Email ID Unverified &amp; Unapproved (Inactive)</option>
                                            <option value="5" <?php if($searchdata['status'] == 5){?> selected <?php } ?>>Deleted</option>
                                        </select>

                                   </div> 
                                </div>
                            </div>
                            <div class="rightFloat">
                            <div class="actions marRight10">
                                <!--<div class="btn-group">
                                    <a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-plus"></i> Add New User
                                    </a>
                                </div>-->
                            </div>
                            <div class="actions">
                                <div class="btn-group">
                                    <!--<button class="btn btn-sm green2 dropdown-toggle" onclick="xyz();" type="button" class="btn btn-primary"> <i class="fa fa-external-link"></i> Export </button>-->
                                </div>
                            </div>
                            </div>
                            </div>
                            
                            <!-- </div> -->
                            <div class="clearfix"></div>
                            <div id="content" class="paddingSide15">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <!-- <th><input type="checkbox" id="selectAll"></th> -->
                                                <th id="first_name" class="DESC" onclick="sort('first_name',<?php echo $limit; ?>);"><a> First Name </a></th>
                                                <th id="last_name" class="DESC" onclick="sort('last_name',<?php echo $limit; ?>);"><a> Last Name </a></th>
                                                <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
                                                <th id="country" class="DESC" onclick="sort('country',<?php echo $limit; ?>);"><a> Country </a></th>
                                                <th id="profession" class="DESC" onclick="sort('profession',<?php echo $limit; ?>);"><a> Profession </a></th>
                                                <th id="joining_date" class="DESC" onclick="sort('joining_date',<?php echo $limit; ?>);"><a> Joining Date </a></th>
                                                <th> Access </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            // echo "<pre>"; print_r($userData);die;
                                    if(count($userData) > 0){
                                    foreach( $userData as $ud){ 
                                    ?>
                                    <tr class="even pointer" id="user<?php echo $ud['User']['id']; ?>">
                                        <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
                                        <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
                                        <td style="width:25%;" class=" "><?php echo $ud ['User']['email'];?> </td>
                                        <td style="width:14%;" class=" "><?php echo !empty($ud ['Country']['country_name']) ? $ud ['Country']['country_name'] : 'N/A';?> </td>
                                        <td style="width:15%;" class=" "><?php echo !empty($ud ['Profession']['profession_type']) ? $ud ['Profession']['profession_type'] : 'N/A';?> </td>
                                        <td style="width:10%;" class=" "><?php echo date('d-m-Y', strtotime($ud ['User']['registration_date']));?></td>
                                        <td style="width:5%;" class="text-center">
                                            <?php 
                                             if($ud['lbpau']['status']==1){
                                            ?>
                                                <button type="button" data-original-title="Access Given" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips accessLiveStreamModalBtn" data-toggle="modal"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
                                           
                                            <?php }else{ ?>
                                                <button type="button" data-original-title="Access Not Given" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips accessLiveStreamModalBtn" data-toggle="modal"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></button>
                                               
                                            <?php 
                                                }
                                             ?>
                                            
                                        </td>
                                        <td style="width:1%;" class="last">
                                                <?php
                                                 if(($ud['User']['status'] !=2)){ ?>
                                            <div class="radialMenu menuButton3">
                                            <nav class="circular-menu">
                                              <div class="circle">
                                                <?php /* ?><button onclick="userView(<?php echo $ud ['User']['id']; ?>)" type="button" data-original-title="View" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-eye"></i></button>
                                                <button data-toggle="modal" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/user/editUser/' . $ud['User']['id'] ; ?>'; return false;" type="button" data-original-title="Edit" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-pencil-square-o"></i></button>
                                                <button type="button" id="deleteModalBtn" data-original-title="Delete" value="<?php echo $ud ['User']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips deleteModalBtn" data-toggle="modal" ><i class="fa fa-trash-o"></i></button><?php */ ?>
                                                <button type="button" id="accessLiveStream" data-original-title="Give Access" value="<?php echo $ud ['User']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips accessLiveStreamModalBtn" data-toggle="modal" onclick="addLiveStreamAccess();" ><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
                                                <button type="button" id="accessRemoveLiveStream" data-original-title="Remove Access" value="<?php echo $ud ['User']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips accessRemoveLiveStreamModalBtn" data-toggle="modal" onclick="removeLiveStreamAccess();"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></button>
                                              </div>
                                              <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
                                            </nav>
                                         </div>
                                                 <?php }else{ ?>
                                                 <button onclick="userView(<?php echo $ud['User']['id']; ?>)" type="button" data-original-title="View" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-eye"></i></button>
                                                 <?php } ?>
                                        </td>
                                        
                                        </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                      </tr> 
                                      <?php } ?> 
                                        </tbody>
                                    </table>


                                    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                                    <div class="actions marBottom10 paddingSide15">
                                    <?php 
                                                if(count($userData) > 0){  
                                                    echo $this->element('pagination');
                                                }
                                            ?>
                                </div>
                         </div>   
                    </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </div>
</div>
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?> 
<!-- BEGIN DELETE POP-UP --> 
 <div id="deleteModal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
   <div class="modal-dialog">
     <div class="modal-content"> 
       <div class="modal-header"> 
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Reason of Deletion</h4>
       </div>
       <div class="modal-body">
         <div class="form-group">
             <label class="control-label">Enter Reason below</label>
             <textarea id="deleteComment" name="deleteComment" class="form-control" rows="3" placeholder="comments"></textarea>
         </div>
       </div>
       <div class="modal-footer textCenter">
         <button id="delUser" value="" onclick="deleteUser();" type="button" data-dismiss="modal" class="btn green2"><i class="fa fa-times" aria-hidden="true"></i>  Delete </button>
         <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
       </div>
   </div>
  </div>
 </div>
<!-- END DELETE POP-UP --> 
<div id="cn-overlay" class="cn-overlay"></div>
<script>
$(document).ready(function() {
    // $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".datePick").datepicker({
    dateFormat: 'yy-mm-dd'
});
 
});
function sort(val,limit){
    $(".loader").fadeIn("fast");
        var a=$('#'+val).attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='DE';
        }
        else{
            var order='A';
        }
    $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/allFilter',
                 type: "POST",
                 data: {'sort':val,'order':order,'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'limit':limit,'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'country':$('#countrySearch').val(),'profession':$('#profession').val()},
                 success: function(data) { 
                    // alert(data);
                    $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    else{
                    $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
      return false; 
    }
function searchUser(){
    $(".loader").fadeIn("fast");
     $.ajax({
             url: '<?php echo BASE_URL; ?>admin/user/allFilter',
             type: "POST",
             data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'country':$('#countrySearch').val(),'profession':$('#profession').val()},
             success: function(data) { 
                $('#content').html(data);
                $(".loader").fadeOut("fast");
            }
        });
  return false;  
}

//** Give  Access
function addLiveStreamAccess(){
    userId=$('#accessLiveStream').val();
    if(confirm("Are you sure you want to remove access?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/addLiveStreamAccess',
                 type: "POST",
                 data: {'userId': userId},
                 success: function(data) { 
                    alert(data);
                    location.reload(true);
                }
            });
    }
    
  return false; 
}

//** Remove Access
function removeLiveStreamAccess(){
    userId=$('#accessRemoveLiveStream').val();
    if(confirm("Are you sure you want to add access?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/removeLiveStreamAccess',
                 type: "POST",
                 data: {'userId': userId},
                 success: function(data) { 
                    alert(data);
                    location.reload(true);
                }
            });
    }
    
  return false; 
}
function deleteUser(){
    userId=$('#delUser').val();
    if($.trim($('#deleteComment').val())==""){
        alert("Please Eneter The Reason.");
        $('#deleteComment').focus();
        return false;
    }
    if(confirm("Are you sure you want to delete this User?")){ 
        $(".loader").fadeIn("fast");
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/deleteUser',
                 type: "POST",
                 data: {'userId': userId,'reason':$('#deleteComment').val()},
                 success: function(data) { 
                    alert(data);
                    location.reload(true);
                }
            });
    }
    $('#deleteComment').val('');
  return false; 
}
function xyz(){
var FirstName=$('#textFirstName').val();
var LastName=$('#textLastName').val();
var Email=$('#textEmail').val();
var specilities=$('#multi-prepend').val();
var country=$('#countrySearch').val();
var profession=$('#profession').val();
var statusVal=$('#selectStatus').val();
var from = $('#dateFrom').val();
var to = $('#dateTo').val();
if(specilities==null)
{
    specilities="";
}
// alert(specilities);
window.open('<?php echo BASE_URL; ?>admin/user/userCsvDownload/?FirstName='+FirstName+'&LastName='+LastName+'&specilities='+specilities+'&statusVal='+statusVal+'&Email='+Email+'&from='+from+'&to='+to+'&country='+country+'&profession='+profession, '_blank');
}

 function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $(".loader").fadeIn("fast");
    $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/user/allFilter',
              type: "POST",
              data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'j':goVal,'limit':limit,'sort':a,'order':order,'country':$('#countrySearch').val(),'profession':$('#profession').val()},
              success: function(data) { 
                  $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 function chngCount(val){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/user/allFilter',
              type: "POST",
              data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'limit':val,'sort':a,'order':order,'country':$('#countrySearch').val(),'profession':$('#profession').val()},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

function abc(val,limit){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/user/allFilter',
           type: "POST",
           data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'j':val,'limit':limit,'sort':a,'order':order,'country':$('#countrySearch').val(),'profession':$('#profession').val()},
           success: function(data) { 
                $('#content').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
       });
}
$("#menu>ul>li").eq(1).addClass('active open');
$(".sub-menu>li").eq(0).addClass('active open');
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script> 


