<?php
//echo "<pre>";print_r($companyDetail);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Map User</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Map User</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            Map User </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo $this->Form->create('',array('url' => '/admin/user/addUserMapping', 'default' => 'false', 'class'=>'form-horizontal','onsubmit'=>'return add();')); ?>
                            <div class="form-body width85Auto">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Company Name</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php 
                                                    foreach ($companyDetail as  $data) {
                                                    $company[$data['CompanyName']['id']]=$data['CompanyName']['company_name'];
                                                    }
                                            echo $this->Form->select('company_id',$company,array('class'=>'form-control','empty'=>'Choose Company','id'=>'selectCompany')); ?>
                                        </div>
                                   </div>
                                </div>

                                <!-- <div class="form-group">
                                    <label class="control-label col-md-2">status</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                        <?php 
                                            // $statusVal=array(
                                            //     "0"=>"Inactive",
                                            //     "1"=>"Active",
                                            //     );
                                            // echo $this->Form->select('status',$statusVal,array('class'=>'form-control','default'=>'0','empty'=>'Select status','id'=>'selectStatus'));?>
                                            <?php //echo $this->Form->text('status',array('class'=>'form-control')); ?>
                                            <?php  //echo $this->Form->hidden('username', ['value'=>$userData['User']['email']]); ?>
                                            <?php  //echo $this->Form->hidden('email', ['value'=>$userData['User']['email']]); ?>
                                        </div>
                                   </div>
                                </div> -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">Privilege</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                        <?php 
                                        $statusVal=array(
                                                "0"=>"Admin",
                                                "1"=>"Subadmin",
                                                // "2"=>"None",
                                                );
                                            echo $this->Form->select('status',$statusVal,array('class'=>'form-control','empty'=>'Select Privilege','id'=>'selectAdmin','value'=>''));
                                            echo $this->Form->hidden('username', ['value'=>$userData['User']['email']]);
                                            echo $this->Form->hidden('email', ['value'=>$userData['User']['email']]);
                                        ?>
                                            <!-- <?php //echo $this->Form->text('status',array('class'=>'form-control','value'=>$adminUserData['AdminUser']['status'])); ?> -->
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Status : </label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <label style="padding-top: 7px;" class="col-md-12">No role is assigned to this user.</label>
                                        </div>
                                   </div>
                                </div>
                            </div>
                            <div class="form-actions center-block txt-center">
                                <?php echo $this->Form->submit('Map User',array('class'=>'btn green2')); ?>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    // $("#menu>ul>li").eq(6).addClass('active open');
    // $(".sub-menu>li").eq(10).addClass('active open');
    $('#selectAdmin').on('change',function(){
        if($(this).val() == ''){
            alert('Select a valid Privilege');
        }
    });
    function add(){
    var companyName=$("#selectCompany").val();
    // var statusVal=$("#selectStatus").val();
    var statusVal=$("#selectAdmin").val();
    if(companyName == ''){
       alert("Please select company.");
       return false; 
    }
    if(statusVal == ''){
       // alert("Please select status.");
       alert('Select a valid Privilege');
       return false; 
    }
    else{
        conf = confirm("Are you sure you want to change?");
        return conf;
    // return true;
       }
    }
</script>