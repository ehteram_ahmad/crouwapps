<?php
// print_r($devices);
// exit();
  echo $this->Html->script(array('Admin.ckeditor/ckeditor.js'));
  $firstName = !empty($userData['UserProfile']['first_name']) ? $userData['UserProfile']['first_name'] : 'Not Available';
  $lastName = !empty($userData['UserProfile']['last_name']) ? $userData['UserProfile']['last_name'] : 'Not Available';
  $email = !empty($userData['User']['email']) ? $userData['User']['email']:'Not Available';
  if(!empty($userData['UserProfile']['profile_img'])){
    $profileImg=AMAZON_PATH . $userData['UserProfile']['user_id']. '/profile/' . $userData['UserProfile']['profile_img'];
  }
  else{
    $profileImg='Admin.no-image.png';
  }
  $gender = !empty($userData['UserProfile']['gender']) ? $userData['UserProfile']['gender']: 'Not Available';
  $contactNo = !empty($userData['UserProfile']['contact_no'])? $userData['UserProfile']['contact_no'] : 'Not Available';
  if($userData['UserProfile']['dob']=="0000-00-00"){
    $dob="Not Available";
  }
  else{
    $dob=$userData['UserProfile']['dob'];
  }
  $registrationDate = (isset($userData['User']['registration_date']) && !empty($userData['User']['registration_date'])) ? date('d-m-Y', strtotime($userData['User']['registration_date'])) : 'N/A';
  $gmcNo = !empty($userData['UserProfile']['gmcnumber']) ? $userData['UserProfile']['gmcnumber'] : 'N/A';
  $userStatus =  ($userData['User']['status'] == 1) ? 'Active' : 'Inactive';
  $address = !empty($userData['UserProfile']['address']) ? $userData['UserProfile']['address'] : 'Not Available' ;
  $countryId = $userData['UserProfile']['countryId'];
  $countryName = !empty($userData['UserProfile']['countryName'])? $userData['UserProfile']['countryName']:'Not Available';
  $city = !empty($userData['UserProfile']['city'])? $userData['UserProfile']['city']:'Not Available';
  $county = !empty($userData['UserProfile']['county'])? $userData['UserProfile']['county']:'Not Available';
  $interests = !empty($userData['UserProfile']['interestlist'])? $userData['UserProfile']['interestlist']:'Not Available';
  $specilities = !empty($userData['UserProfile']['specilityLists'])? $userData['UserProfile']['specilityLists']:'Not Available';
  if(!empty($userData['UserProfile']['registration_image'])){
    $registrationImg=AMAZON_PATH . $userData['UserProfile']['user_id'] .'/certificate/'. $userData['UserProfile']['registration_image'];
  }
  else{
    $registrationImg='Admin.no-image.png';
  }
  $source=!empty($userData['User']['registration_source'])? $userData['User']['registration_source']:'Not Available';
  $postedBeats = $userData['UserProfile']['userBeatPosted'];
  $colleagues = $userData['UserProfile']['userColleagues'];
  $followers = $userData['UserProfile']['userFollowers'];
  $following = $userData['UserProfile']['userFollowings'];

if(!empty($userData['UserProfile']['userEducation'])){
  $countNo=count($userData['UserProfile']['userEducation']);
  $countNo=$countNo-1;
  $edStatus=$userData['UserProfile']['userEducation'][$countNo]['EducationDegree']['short_name']." - ".$userData['UserProfile']['userEducation'][$countNo]['UserInstitution']['to_date'];
}else{
  $edStatus='<a onclick="addEdu();">Add Education</a>';
}
$profession=!empty($userData['UserProfile']['professionName'])? $userData['UserProfile']['professionName']:'Not Available';
$role = !empty($userData['UserProfile']['role_status'])? $userData['UserProfile']['role_status']:'Not Available';
if(!empty($userData['UserProfile']['userEmployments'])){
  $countNo=count($userData['UserProfile']['userEmployments']);
  $countNo=$countNo-1;
  $emStatus=$userData['UserProfile']['userEmployments'][$countNo]['Designation']['designation_name']." at ".$userData['UserProfile']['userEmployments'][$countNo]['Company']['company_name'];
}else{
$emStatus=$profession;
}
if (stripos($source, "MB") !== false) {
    $type="medicbleep_";
}
else{
    $type="";
}
// echo "<pre>";print_r($userDeviceInfo);
// $userDeviceArr = json_decode($userDeviceInfo['UserDeviceInfo']['device_info']);
// echo "<pre>";print_r($userDeviceArr);
if(!empty($userDeviceInfo))
{
  $userDeviceArr = json_decode($userDeviceInfo['UserDeviceInfo']['device_info']);
  $device = isset($userDeviceArr->Device) ? $userDeviceArr->Device: 'Not Available';
  $manufacturer = isset($userDeviceArr->manufacturer) ? $userDeviceArr->manufacturer: 'Not Available';
  $osVersion = isset($userDeviceArr->os_version) ? $userDeviceArr->os_version: 'Not Available';
  $version = isset($userDeviceArr->version) ? $userDeviceArr->version: 'Not Available';
  $product = isset($userDeviceArr->Product) ? $userDeviceArr->Product: 'Not Available';
  $deviceId = isset($userDeviceArr->device_id) ? $userDeviceArr->device_id: 'Not Available';
  $model = isset($userDeviceArr->Model) ? $userDeviceArr->Model: 'Not Available';
  $deviceType = isset($userDeviceArr->device_type) ? $userDeviceArr->device_type: 'Not Available';
}
else
{
  $device = 'Not Available';
  $manufacturer = 'Not Available';
  $osVersion = 'Not Available';
  $version = 'Not Available';
  $product = 'Not Available';
  $deviceId = 'Not Available';
  $model = 'Not Available';
  $deviceType = 'Not Available';
}
?>
<style type="text/css">
      div.checker, div.checker span, div.checker input {
      width: 35px;
      height: 30px;

  }
  .toggle{
    border-radius: 20px!important;
    width: 9.5px;
    height: 0px;
    position: absolute;
    left: -25px;
    top: 0px;
  }
  #presentTxt label{
    position: relative;
    top:8px;
  }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>User Details</h1>
      </div>
      <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
      <li>
          <a href="<?php echo BASE_URL . 'admin/home/dashboard'; ?>">Home</a>
          <i class="fa fa-circle"></i>
      </li>
      <li>
          <a href="<?php echo BASE_URL . 'admin/user/userLists'; ?>">Users</a>
          <i class="fa fa-circle"></i>
      </li>
      <li>
          <span class="active">User Details</span>
      </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE TOOLBAR -->
    <div class="page-toolbar buttonTop">
      <div class="">
      <?php if($userData ['User']['status'] == 2){}
      else{ ?>
        <button type="submit" class="btn green2" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/user/editUser/' . $userData['User']['id']; ?>'; return false;"> <i class="fa fa-pencil"></i> Edit Details</button>
        <?php } ?>
        <button type="button" class="btn default marLeft5px" onclick="javascript:window.location.href='<?php echo BASE_URL . 'admin/user/userLists'; ?>'; return false;">Go Back</button>
      </div>
    </div>
    <!-- END PAGE TOOLBAR -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
          <!-- BEGIN PAGE BASE CONTENT -->
          <div class="row">
            <div class="col-md-12">
              <div class="portlet box light2">
                <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <form class="form-horizontal" role="form">
                    <div class="form-body">
                      <div class="row">
                        <!-- SIDEBAR USERPIC -->
                        <div class="col-md-2 col-xs-12">
                          <div class="profile-userpic leftFloat">
                            <a data-toggle="modal" href="#profileImg">
                                <?php echo $this->Html->image($profileImg, array("class"=> "img-responsive")); ?>
                            </a>
                          </div>
                        </div>
                        <div class="col-md-7 col-xs-12">
                          <div class="profile-usertitle">
                            <div class="profile-usertitle-name" style="word-break: break-all;"> <?php echo $firstName." ".$lastName; ?> </div>
                            <div class="profile-desc-link"> <i class="fa fa-map-marker" aria-hidden="true"></i> <a><?php echo $county; ?>, <?php echo $countryName; ?></a> </div>
                            <div class="profile-stat-text"> <?php echo $emStatus; ?> <span><?php echo $edStatus; ?></span></div><br><br>
                          </div>
                          <div class="profile-stat2">
                            <div class="leftFloat">
                              <div class="profile-stat-no"> <?php echo $colleagues; ?> </div>
                              <div class="profile-stat-text"> Colleagues </div>
                            </div>
                            <div class="leftFloat">
                              <div class="profile-stat-no"> <?php echo $following; ?> </div>
                              <div class="profile-stat-text"> Followings </div>
                            </div>
                            <div class="leftFloat">
                              <div class="profile-stat-no"> <?php echo $followers; ?> </div>
                              <div class="profile-stat-text"> Followers </div>
                            </div>
                            <div class="leftFloat">
                              <div class="profile-stat-no"> <?php echo $postedBeats; ?> </div>
                              <div class="profile-stat-text">Beat </div>
                            </div>
                          </div>
                        </div>

                        <!-- END SIDEBAR USERPIC -->
                        <!--</div>-->
                        <div class="col-md-3 col-xs-12">
                          <div class="row">
                            <div class="col-md-12 col-xs-5">
                              <div class="statusIcons">
                                <?php
                                if($userData ['User']['status'] !=2){
                                 if(($userData ['User']['status'] != 1)|($userData ['User']['approved'] != 1)){
                                ?>
                                <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="Inactive" type="button" data-placement="bottom"><i class="fa fa-check"></i></button>
                                <?php } ?>
                                <?php
                                 if(($userData ['User']['status'] == 1)&($userData ['User']['approved'] == 1)){
                                ?>
                                <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Active" type="button" data-placement="bottom"><i class="fa fa-check"></i></button>
                                <?php }
                                 if($userData ['User']['status'] == 1){
                                ?>
                                <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Email Is Verified" type="button" data-placement="bottom"><i class="fa fa-envelope-o"></i></button>
                                <?php }else{ ?>
                                <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="Email Is Unverified" type="button" data-placement="bottom"><i class="fa fa-envelope-o"></i></button>
                                <?php }
                                 if($userData ['User']['approved'] == 1){
                                ?>
                                <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Approved" type="button" data-placement="bottom"><i class="fa fa-thumbs-up"></i></button>
                                <?php }else{ ?>
                                <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="Unapproved" type="button" data-placement="bottom"><i class="fa fa-thumbs-up"></i></button>
                                <?php }}else{
                                    ?>
                                    <button class="btn btn-circle btn-icon-only red tooltips" data-original-title="Deleted" type="button" data-placement="bottom"><i class="fa fa-times"></i></button>
                                    <?php
                                    } ?>
                              </div>
                            </div>
                            <div class="col-md-12 col-xs-7">
                              <div class="appDevice">
                                <div class="appOCR"> <span>TheOCR:</span>
                                  <?php
                                   if($devices['iosOCR'] >'0'){ ?>
                                  <button class="btn btn-circle btn-icon-only green2 tooltips" data-original-title="IOS" type="button" data-placement="bottom"><i class="fa fa-apple" aria-hidden="true"></i></button>
                                   <?php }else{ ?>
                                  <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="IOS" type="button" data-placement="bottom"><i class="fa fa-apple" aria-hidden="true"></i></button>
                                   <?php } ?>
                                  <?php if($devices['webOCR'] >'0'){ ?>
                                  <button class="btn btn-circle btn-icon-only green2 tooltips" data-original-title="Web App" type="button" data-placement="bottom"><i class="fa fa-laptop" aria-hidden="true"></i></button>
                                   <?php }else{ ?>
                                  <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="Web App" type="button" data-placement="bottom"><i class="fa fa-laptop" aria-hidden="true"></i></button>
                                   <?php } ?>
                                  <?php if($devices['androidOCR'] >'0'){ ?>
                                  <button class="btn btn-circle btn-icon-only green2 tooltips" data-original-title="Android" type="button" data-placement="bottom"><i class="fa fa-android" aria-hidden="true"></i></button>
                                   <?php }else{ ?>
                                  <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="Android" type="button" data-placement="bottom"><i class="fa fa-android" aria-hidden="true"></i></button>
                                   <?php } ?>
                                </div>
                                <div class="appMedicBleep"> <span>MB:</span>
                                  <?php
                                  if($devices['iosMB'] >'0'){ ?>
                                  <button class="btn btn-circle btn-icon-only green2 tooltips" data-original-title="IOS" type="button" data-placement="bottom"><i class="fa fa-apple" aria-hidden="true"></i></button>
                                   <?php }else{ ?>
                                  <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="IOS" type="button" data-placement="bottom"><i class="fa fa-apple" aria-hidden="true"></i></button>
                                   <?php } ?>
                                  <?php if($devices['webMB'] >'0'){ ?>
                                  <button class="btn btn-circle btn-icon-only green2 tooltips" data-original-title="Web App" type="button" data-placement="bottom"><i class="fa fa-laptop" aria-hidden="true"></i></button>
                                   <?php }else{ ?>
                                  <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="Web App" type="button" data-placement="bottom"><i class="fa fa-laptop" aria-hidden="true"></i></button>
                                   <?php } ?>
                                  <?php if($devices['androidMB'] >'0'){ ?>
                                  <button class="btn btn-circle btn-icon-only green2 tooltips" data-original-title="Android" type="button" data-placement="bottom"><i class="fa fa-android" aria-hidden="true"></i></button>
                                   <?php }else{ ?>
                                  <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="Android" type="button" data-placement="bottom"><i class="fa fa-android" aria-hidden="true"></i></button>
                                   <?php } ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--/row-->
                    </div>
                  </form>
                  <!-- END FORM-->
                </div>
              </div>
              <div class="portlet light bordered customePadding">
                <div class="portlet-title">
                  <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase">Contact Details</span> </div>
                  <div class="tools"> <a href="javascript:;" class="collapse"> </a> </div>
                </div>
                <div class="portlet-body">
                  <div class="row">
                    <div class="col-md-12 col-xs-12 widthSub30">
                      <div class="profile-desc-link"> <i class="fa fa-phone" aria-hidden="true"></i> <a><?php echo $contactNo; ?></a>
                    <?php if($userData['UserProfile']['contact_no_is_verified']==1){ ?>
                        <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Verified" type="button"><i class="fa fa-check"></i></button>
                    <?php }else{
                        ?>
                        <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="Not Verified" type="button"><i style="color: black;" class="fa fa-check"></i></button>
                        <?php
                        } ?>
                      </div>
                      <div class="margin-top-10 profile-desc-link lightBordered"> <i class="fa fa-envelope"></i> <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                      <?php if($userData ['User']['status'] == 2){}
                      else{ ?>
                        <button id="sendEmailBtn" type="button" class="btn default rightFloat">Send Mail</button>
                    <?php } ?>
                      </div>
                    </div>
                  </div>
                  <!--/row-->
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Address:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"><?php echo $address; ?></p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">City:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"><?php echo $city; ?></p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                  </div>
                  <!--/row-->
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">State / County:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"> <?php echo $county; ?> </p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Country:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"><?php echo $countryName; ?></p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                  </div>
                  <!--/row-->
                </div>
              </div>
              <div class="portlet light bordered customePadding">
                <div class="portlet-title">
                  <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase">General Details</span> </div>
                  <div class="tools"> <a href="javascript:;" class="collapse"> </a> </div>
                </div>
                <div class="portlet-body">
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Joining Date:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"><?php echo $registrationDate; ?></p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Source:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"><?php echo $source; ?></p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Profession:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"><?php echo $profession; ?></p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Role:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"><?php echo $role; ?></p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                  </div>
                  <!--/row-->
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Professional Registration Number:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"> <?php echo $gmcNo;?> </p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Date of Birth:</label>
                        <div class="col-md-7 col-xs-7">
                          <p class="form-control-static"><?php echo $dob; ?></p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                  </div>
                  <!--/row-->
                  <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                      <label class="control-label col-md-5 col-xs-5 bold">Interest(s):</label>
                      <div class="col-md-7 col-xs-7">
                        <p style="width: 100%;word-wrap: break-word;" class="form-control-static"><?php echo $interests; ?></p>
                      </div>
                    </div>
                  </div>
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label class="control-label col-md-5 col-xs-5 bold">Speciality(s):</label>
                        <div class="col-md-7 col-xs-7">
                          <p style="width: 100%;word-wrap: break-word;" class="form-control-static"> <?php echo $specilities; ?> </p>
                        </div>
                      </div>
                    </div>
                    <!--/span-->
                  </div>
                  <!--/row-->
                </div>
              </div>
              <div class="portlet light bordered customePadding">
                  <div class="portlet-title">
                    <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase">Device Details</span> </div>
                    <div class="tools"> <a href="javascript:;" class="collapse"> </a> </div>
                  </div>
                  <div class="portlet-body">
                    <div class="row">
                      <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">Device:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"><?php echo $device; ?></p>
                          </div>
                        </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">Product:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"><?php echo $product; ?></p>
                          </div>
                        </div>
                      </div>
                      <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                      <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">Manufacturer:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"> <?php echo $manufacturer; ?> </p>
                          </div>
                        </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">Device id:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"><?php echo $deviceId; ?></p>
                          </div>
                        </div>
                      </div>
                      <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                      <!-- <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">Api level:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"> 0 </p>
                          </div>
                        </div>
                      </div> -->
                      <!--/span-->
                      <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">Model:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"><?php echo $model; ?></p>
                          </div>
                        </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">OS version:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"><?php echo $osVersion; ?></p>
                          </div>
                        </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-6 col-xs-12">
                        <!-- <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">User id:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static">1289</p>
                          </div>
                        </div> -->
                      </div>
                      <!--/span-->
                      <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">Version:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"><?php echo $version; ?></p>
                          </div>
                        </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-5 col-xs-5 bold">Device type:</label>
                          <div class="col-md-7 col-xs-7">
                            <p class="form-control-static"><?php echo $deviceType; ?></p>
                          </div>
                        </div>
                      </div>
                      <!--/span-->
                    </div>
                    <!--/row-->
                  </div>
                </div>
              <div class="portlet light bordered customePadding">
                <div class="portlet-title">
                  <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase">Verification Identity</span> </div>
                  <div class="tools"> <a href="javascript:;" class="collapse"> </a> </div>
                </div>
                <div class="portlet-body">
                  <div class="row">
                    <div class="col-md-6 col-xs-12">
                      <div class="row list-separated">
                        <div class="col-md-6 col-sm-6 col-xs-6 width140px"> <a data-toggle="modal" href="#normalModal"><?php echo $this->Html->image($registrationImg, array("alt"=>"")); ?></a> </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                      <div class="profile-desc-link margin-top-5 fixWidthBt">
                        <ul>
                        <?php if($userData ['User']['status'] != 2){ ?>
                          <li>
                            <?php
                             if($userData ['User']['status'] == 1){
                            ?>
                            <button id="varBtn1" onclick="updateStatus(<?php echo $userData ['User']['id']; ?>, 'Active');" type="button" class="btn">Unverify Email ID</button>
                            <?php }else{ ?>
                            <button id="varBtn1" onclick="updateStatus(<?php echo $userData ['User']['id']; ?>, 'Inactive');" type="button" class="btn blue2">Verify Email ID</button>
                            <?php } ?>
                          </li>
                          <li>
                            <?php
                             if($userData ['User']['approved'] == 1){
                            ?>
                            <button id="varBtn2" onclick="updateApproveStatus(<?php echo $userData ['User']['id']; ?>, 'Approved');" type="button" class="btn">Unapprove Account</button>
                            <?php }else{ ?>
                            <button id="varBtn2" onclick="updateApproveStatus(<?php echo $userData ['User']['id']; ?>, 'Unapproved');" type="button" class="btn blue2">Approve Account</button>
                            <?php } ?>
                          </li>
                          <li>
                            <?php
                             if(($userData ['User']['status'] == 1)&($userData ['User']['approved'] == 1)){
                            ?>
                            <button id="varBtn3" onclick="accountStatus(<?php echo $userData['User']['id']; ?>,'Activate');" type="button" class="btn blue2">Deactivate Account</button>
                            <?php }else{ ?>
                            <button id="varBtn3" onclick="accountStatus(<?php echo $userData ['User']['id']; ?>,'Deactivate');" type="button" class="btn green2">Activate Account</button>
                            <?php }?>
                          </li>
                          <?php }else{ ?>
                          <li>
                            <button type="button" class="btn">Inactive</button>
                          </li>
                          <li>
                            <?php
                             if($userData ['User']['approved'] == 1){
                            ?>
                            <button type="button" class="btn">Approved</button>
                            <?php }else{ ?>
                            <button type="button" class="btn">Unapproved</button>
                            <?php } ?>
                          </li>
                            <?php } ?>
                          <li>
                          <?php if($userData ['User']['status'] == 2){
                            ?>
                            <button type="button" class="btn">Deleted</button>
                            <?php }else{ ?>
                            <button type="button" class="btn red" id="deleteModalBtn">Delete</button>
                            <?php } ?>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!--/row-->
                </div>
              </div>
              <?php
              if(count($userData['UserProfile']['userEducation']) > 0){
                $classEd="collapse";
                $styleEd="";
              }else{
                $classEd="expand";
                $styleEd='style="display:none;"';
              }
              if(count($userData['UserProfile']['userEmployments']) > 0){
                $classEmp="collapse";
                $styleEmp="";
              }else{
                $classEmp="expand";
                $styleEmp='style="display:none;"';
              }
               ?>
              <div class="portlet light bordered customePadding">
                <div class="portlet-title">
                  <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase">Education Details</span> </div>
                  <div class="tools"> <a href="javascript:;" class="<?php echo $classEd; ?>"> </a> </div>
                    </div>
                <div class="portlet-body" <?php echo $styleEd; ?> >
                <div id="eduContent">
                  <!-- row -->
                  <?php
                  if(isset($userData['UserProfile']['userEducation'])){
                  foreach ($userData['UserProfile']['userEducation'] as $value) {
                  ?>
                  <div class="row">
                    <div class="col-md-2 col-xs-12">
                      <div class="profile-userpic leftFloat"> <img src="<?php echo BASE_URL.'admin/img/building.png'; ?>" class="img-responsive" alt=""> </div>
                    </div>
                    <div class="col-md-10 col-xs-12">
                      <div class="profile-usertitle">
                        <div class="profile-stat-text"> <?php echo $value['InstituteName']['institute_name']; ?> <span> <?php echo $value['EducationDegree']['degree_name']." ( ".$value['EducationDegree']['short_name']." ) "; ?> </span></div>
                      </div>
                      <div class="profile-stat2 width100full">
                        <div class="profile-stat-text"> <?php echo $value['UserInstitution']['from_date']." - ".$value['UserInstitution']['to_date']; ?> </div>
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                      <p class="eduText"><?php echo $value['UserInstitution']['description']; ?></p>
                      <p class="lightBordered width100full leftFloat"></p>
                    </div>
                  </div>
                  <?php }}?>
                  <!--/row-->
                  </div>
                  <?php if($userData ['User']['status'] == 2){}else{
                    ?>
                <button id="addEducationBtn" class="btn btn-circle btn-icon-only btn-info3 tooltips actionBtn" data-original-title="Add More" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                <?php } ?>
                </div>
              </div>
              <div class="portlet light bordered customePadding">
                <div class="portlet-title">
                  <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase">Employment Details</span> </div>
                  <div class="tools"> <a href="javascript:;" class="<?php echo $classEmp; ?>"> </a> </div>
                </div>
                <div class="portlet-body" <?php echo $styleEmp; ?> >
                <div id="empContent">
                  <!-- row -->
                  <?php
                  if(isset($userData['UserProfile']['userEmployments'])){
                  foreach ($userData['UserProfile']['userEmployments'] as $value) {
                      if($value['UserEmployment']['is_current']==1){
                        $to="Present";
                      }else{
                        $to=date("F-Y", strtotime($value['UserEmployment']['to_date']));
                      }
                  ?>
                  <div class="row">
                    <div class="col-md-2 col-xs-12">
                      <div class="profile-userpic leftFloat"> <img src="<?php echo BASE_URL.'admin/img/institute.png'; ?>" class="img-responsive" alt=""> </div>
                    </div>
                    <div class="col-md-10 col-xs-12">
                      <div class="profile-usertitle-name2"> <?php echo $value['Company']['company_name']; ?> </div>
                      <div class="profile-usertitle">
                        <div class="profile-stat-text"> <?php echo $value['Designation']['designation_name']; ?> <span> <?php echo $value['UserEmployment']['location']; ?> </span></div>
                      </div>
                      <div class="profile-stat2 width100full">
                        <div class="profile-stat-text"> <?php
                        $from=date("F-Y", strtotime($value['UserEmployment']['from_date']));
                         echo $from." - ".$to; ?></div>
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                      <p class="eduText"><?php echo $value['UserEmployment']['description']; ?></p>
                      <p class="lightBordered width100full leftFloat"></p>
                    </div>
                  </div>
                  <?php }}
                   ?>
                  <!--/row-->
                </div>
                <?php if($userData ['User']['status'] == 2){}else{
                  ?>
                <button id="addEmploymentBtn" class="btn btn-circle btn-icon-only btn-info3 tooltips actionBtn" data-original-title="Add More" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                <?php } ?>
                </div>
              </div>
              <div class="portlet light bordered customePadding">
                <div class="portlet-title">
                  <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase">Action History</span> </div>
                  <div class="tools"> <a href="javascript:;" class="expand"> </a> </div>
                </div>
                <div class="portlet-body flip-scroll" style="display: none;">
                  <div class="row">
                    <div class="col-md-12 col-xs-12">
                      <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content">
                          <tr>
                            <th class="width40px txtCenter">#</th>
                            <th class="txtCenter"> Type </th>
                            <th class="txtCenter"> Subject </th>
                            <th class="txtCenter"> Comments </th>
                            <th class="txtCenter"> Date </th>
                            <th class="txtCenter width60px">&nbsp;  </th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            if((count($activityLog) > 0)|(count($mailLog) > 0) | (count($feedResp) > 0)){
                        for ($i=0; $i < count($activityLog) ; $i++) { ?>
                          <tr>
                            <td class="txtCenter"><?php echo $i+1; ?></td>
                            <td class="txtCenter"> <?php echo $activityLog[$i]['AdminActivityLog']['activity_type'];  ?> </td>
                            <td class="txtCenter"> - </td>
                            <td class="txtCenter"> <?php echo $activityLog[$i]['AdminActivityLog']['message']; ?> </td>
                            <td class="txtCenter"><?php echo $activityLog[$i]['AdminActivityLog']['created_date']; ?></td>
                            <td class="txtCenter">
                            <!-- <button id="viewMailBtn" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="View"><i class="fa fa-eye"></i></button> -->
                            </td>
                          </tr>
                       <?php }
                       for ($j=0; $j < count($mailLog); $j++) { ?>
                       <tr>
                         <td class="txtCenter"><?php echo $i+$j+1; ?></td>
                         <td class="txtCenter"> User_Mailing </td>
                         <td class="txtCenter">
                             <?php echo $mailLog[$j]['AdminUserSentMail']['email_category_id'];  ?>
                         </td>
                         <td class="txtCenter"> <?php echo $mailLog[$j]['AdminUserSentMail']['comment']; ?> </td>
                         <td class="txtCenter"><?php echo $mailLog[$j]['AdminUserSentMail']['created_date']; ?></td>
                         <td class="txtCenter"><button id="<?php echo $mailLog[$j]['AdminUserSentMail']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 tooltips viewMailBtn" data-original-title="View"><i class="fa fa-eye"></i></button></td>
                       </tr>
                       <?php }
                       for ($k=0; $k < count($feedResp) ; $k++) { ?>
                         <tr>
                           <td class="txtCenter"><?php echo $i+$j+$k+1; ?></td>
                           <td class="txtCenter"> Feedback Response </td>
                           <td class="txtCenter">

                           </td>
                           <td class="txtCenter"> <?php echo $feedResp[$k]['AdminFeedbackResponse']['content']; ?> </td>
                           <td class="txtCenter"><?php echo $feedResp[$k]['AdminFeedbackResponse']['created_date']; ?></td>
                           <td class="txtCenter"><button id="<?php echo $feedResp[$k]['AdminFeedbackResponse']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 tooltips viewMailBtn" data-original-title="View"><i class="fa fa-eye"></i></button></td>
                         </tr>
                      <?php }}else{ ?>
                        <tr>
                            <td colspan="6" class="text-center"><h3> No Action Has Taken Yet. </h3></td>
                        </tr>
                       <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!--/row-->
                </div>
              </div>
            </div>
          </div>
          <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END PROFILE CONTENT -->
      </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
  </div>
  <!-- END CONTENT BODY -->
</div>
            <!-- END CONTENT -->

            <!-- Registration Image popup [START]-->
            <div id="normalModal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <!--<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Verification Proof</h4>
                  </div>-->
                  <div class="modal-body width100full">
                    <div class="closeBt">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    </div>
                    <?php echo $this->Html->image($registrationImg, array("alt"=>"")); ?> </div>
                  <!--<div class="modal-footer textCenter">
                    <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                  </div>-->
                </div>
              </div>
            </div>
        <!-- Registration Image popup [END]-->

        <!-- Profile Image popup [START]-->
            <div id="profileImg" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                      <!--<div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Verification Proof</h4>
                      </div>-->
                      <div class="modal-body width100full">
                        <div class="closeBt">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        </div>
                        <?php echo $this->Html->image($profileImg, array("alt"=> "")); ?> </div>
                      <!--<div class="modal-footer textCenter">
                        <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                      </div>-->
                    </div>
                </div>
            </div>
        <!-- Profile Image popup [END]-->

        <!-- BEGIN DELETE POP-UP -->
            <div id="deleteModal" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Reason of Deletion</h4>
                  </div>
                  <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Enter Reason below</label>
                        <textarea id="deleteComment" name="deleteComment" class="form-control" rows="3" placeholder="comments"></textarea>
                    </div>
                  </div>
                  <div class="modal-footer textCenter">
                    <button onclick="deleteUser(<?php echo $userData ['User']['id']; ?>);" type="button" data-dismiss="modal" class="btn green2"><i class="fa fa-times" aria-hidden="true"></i>  Delete </button>
                    <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                  </div>
              </div>
             </div>
            </div>
        <!-- END DELETE POP-UP -->

        <!-- BEGIN ADD EDUCATION POP-UP -->
            <div id="addEducation" class="modal fade" tabindex="-1" data-replace="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">ADD MORE EDUCATION DETAILS</h4>
                  </div>
                  <div class="modal-body">
                    <div class="form-group">
                    <?php
                    foreach ($institutes as $value) {
                      $institute[$value['InstituteName']['id']]=$value['InstituteName']['institute_name'];
                    }
                    echo $this->Form->select('institute',$institute,array('class'=>'form-control select4me','empty'=>'Select your College / University / Institute','id'=>'eduInstitute')); ?>
                    </div>
                    <div class="form-group">
                    <?php
                    foreach ($degree as $value) {
                      $degrees[$value['EducationDegree']['id']]=$value['EducationDegree']['degree_name']." (".$value['EducationDegree']['short_name'].")";
                    }
                    echo $this->Form->select('degree',$degrees,array('class'=>'form-control select4me','empty'=>'Select your Degree','id'=>'eduDegree')); ?>
                    </div>
                    <div class="form-group">
                    <?php echo $this->Form->textarea('educationDescription',array('class'=>'form-control','rows'=>'3','placeholder'=>'Description','id'=>'eduDescrip')); ?>
                    </div>
                    <!-- BEGIN PAGE TOOLBAR -->
                    <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                      <?php echo $this->Form->text('from',array('class'=>'form-control input-sm yrPick','id'=>'dateFromEdu','placeholder'=>'From :')); ?>
                      </div>
                      <div class="col-sm-6">
                      <?php echo $this->Form->text('to',array('class'=>'form-control input-sm yrPick','id'=>'dateToEdu','placeholder'=>'To :')); ?>
                      </div>
                    </div>
                    </div>
                   <!-- END PAGE TOOLBAR -->
                  </div>
                  <div class="modal-footer textCenter">
                  <?php echo $this->Form->button('Save',array('label'=>'Save','class'=>'btn green2','onclick'=>'saveEdu('.$userData ['User']['id'].');')); ?>
                  <?php echo $this->Form->button('Cancel',array('class'=>'btn default','data-dismiss'=>'modal','onclick'=>'clrModal();')); ?>
                  </div>
              </div>
             </div>
            </div>
        <!-- END ADD EDUCATION POP-UP -->

        <!-- BEGIN ADD Employment POP-UP -->
        <div id="addEmployment" class="modal fade" tabindex="-1" data-replace="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">ADD MORE EMPLOYMENT DETAILS</h4>
              </div>
              <div class="modal-body">
              <?php //echo $this->Form->create('employement',array('class'=>'form-group')); ?>
                <div class="form-group height30px">
                    <label class="control-label">I'm currently working here ...</label>
                    <div class="toggleBtn">
                        <div class="checkbox" id="currentWork">
                          <?php echo $this->Form->checkbox('currently',array('checked'=>true,'data-toggle'=>'toggle','onclick'=>'x();')); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                <?php
                foreach ($comapnies as $value) {
                  $orgainsations[$value['CompanyName']['id']]=$value['CompanyName']['company_name'];
                }
                echo $this->Form->select('organisation',$orgainsations,array('class'=>'form-control select4me','empty'=>'Select your Hospital / University / Organisation','id'=>'organiseSelect')); ?>
                </div>
                <div class="form-group">
                <?php
                foreach ($designation as $value) {
                  $designations[$value['Designation']['id']]=$value['Designation']['designation_name'];
                }
                echo $this->Form->select('designation',$designations,array('class'=>'form-control select4me','empty'=>'Select your Designation','id'=>'desigSelect')); ?>
                </div>
                <div class="form-group">
                <?php echo $this->Form->text('location',array('class'=>'form-control input-sm','placeholder'=>'Location','id'=>'empLocation')); ?>
                </div>
                <div class="form-group">
                <?php echo $this->Form->textarea('description',array('class'=>'form-control','rows'=>'3','placeholder'=>'Description','id'=>'empDescrip')); ?>
                </div>
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                    <?php echo $this->Form->text('from',array('class'=>'form-control input-sm datePick','id'=>'dateFrom','placeholder'=>'From :')); ?>
                    </div>
                    <div class="col-sm-6" id="presentTxt">
                    <?php echo $this->Form->label(' - Present'); ?>
                    </div>
                    <div class="col-sm-6 hidden" id="presentInput">
                    <?php echo $this->Form->text('to',array('class'=>'form-control input-sm datePick','id'=>'dateTo','placeholder'=>'To :')); ?>
                    </div>
                  </div>
                </div>
               <!-- END PAGE TOOLBAR -->
              </div>
              <div class="modal-footer textCenter">
              <?php echo $this->Form->button('Save',array('label'=>'Save','class'=>'btn green2','onclick'=>'saveEmp('.$userData ['User']['id'].')')); ?>
              <?php echo $this->Form->button('Cancel',array('class'=>'btn default','data-dismiss'=>'modal','onclick'=>'clrModal();','id'=>'cancel')); ?>
              </div>
          </div>
         </div>
        </div>
        <div id="viewMail" class="modal fade" tabindex="-1" data-replace="true">
        </div>
        <!-- END ADD Employment POP-UP -->
<script type="text/javascript">
$(".modal").on("hidden.bs.modal", function(){
    $('#dateFrom,#dateTo,#empDescrip,#empLocation,#organiseSelect,#desigSelect,#eduInstitute,#eduDegree,#eduDescrip,#dateFromEdu,#dateToEdu,#mailSub,#mailTemplate,#extraComment,#deleteComment').val('');
});

$('#currentWork').click(function() {
  show= $('[data-toggle="toggle"]').attr('class');
  show=show.search("off");
  if(show > 0){
   $to=1;
   $('#presentInput').addClass('hidden');
   $('#presentTxt').removeClass('hidden');
  }
  else{
    $('#presentTxt').addClass('hidden');
    $('#presentInput').removeClass('hidden');
  }
});
function saveEmp(val){
  if($('#organiseSelect').val()==""){
    alert("Please select your Hospital / University / Organisation");
    return false;
  }
  if($('#desigSelect').val()==""){
    alert("please select your Designation");
    return false;
  }
  if($('#empLocation').val()==""){
    alert("please enter your Location");
    $('#empLocation').focus();
    return false;
  }
  if($('#empDescrip').val()==""){
    alert("please enter your Description");
    $('#empDescrip').focus();
    return false;
  }
  if($('#dateFrom').val()==""){
    alert("please select Month");
    $('#dateFrom').focus();
    return false;
  }
  show= $('[data-toggle="toggle"]').attr('class');
  show=show.search("off");
  if(show > 0){
   if($('#dateTo').val()==""){
       alert("please select Month");
       $('#dateTo').focus();
       return false;
     }
     else{
      $to=$('#dateTo').val();
     }
  }
  else{
    $to="0000-00-00 00:00:00";
  }
  $.ajax({
           url: '<?php echo BASE_URL; ?>admin/user/addEmployement',
           type: "POST",
           data: {'userId': val,'orgainsation':$('#organiseSelect').val(),'designation':$('#desigSelect').val(),'location':$('#empLocation').val(),'description':$('#empDescrip').val(),'from':$('#dateFrom').val(),'to':$to},
           success: function(data) {
              $('#empContent').html(data);
              // $('#cancel').click();
              location.reload();
          }
      });
}
function saveEdu(val){
  if($('#eduInstitute').val()==""){
    alert("Please Select your College / University / Institute");
    $('#eduInstitute').focus();
    return false;
  }
  if($('#eduDegree').val()==""){
    alert("Please Select your Degree");
    $('#eduDegree').focus();
    return false;
  }
  if($('#eduDescrip').val()==""){
    alert("Please enter Description");
    $('#eduDescrip').focus();
    return false;
  }
  if($('#dateFromEdu').val()==""){
    alert("Please select Year");
    $('#dateFromEdu').focus();
    return false;
  }
  if($('#dateToEdu').val()==""){
    alert("Please select Year");
    $('#dateToEdu').focus();
    return false;
  }
  $.ajax({
           url: '<?php echo BASE_URL; ?>admin/user/addEducation',
           type: "POST",
           data: {'userId': val,'institution':$('#eduInstitute').val(),'degree':$('#eduDegree').val(),'description':$('#eduDescrip').val(),'from':$('#dateFromEdu').val(),'to':$('#dateToEdu').val()},
           success: function(data) {
              $('#eduContent').html(data);
              // $('#cancel').click();
              location.reload();
          }
      });
}
$(document).ready(function() {
    // $(".datePick").datepicker({ dateFormat: 'dd/mm/yy' });
    $(".datePick").datepicker({
    format: "M-yyyy",
        startView: "months",
        minViewMode: "months"
});
    $(".yrPick").datepicker({
    format: "yyyy",
        startView: "years",
        minViewMode: "years"
});


});
//** Delete user
function deleteUser(userId){
    if($.trim($('#deleteComment').val())==""){
        alert("Please Eneter The Reason.");
        $('#deleteComment').val('');
        return false;
    }
    if(confirm("Are you sure you want to delete this User?")){
        $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/deleteUser',
                 type: "POST",
                 data: {'userId': userId,'reason':$('#deleteComment').val()},
                 success: function(data) {
                    alert(data);
                    location.reload();
                }
            });
    }
    $('#deleteComment').val('');
  return false;
}
function accountStatus(userId,status){
      $.ajax({
               url: '<?php echo BASE_URL; ?>admin/user/changeAccountStatus',
               type: "POST",
               data: {'userId': userId,'status':status},
               success: function(data) {
                  alert(data);
                  location.reload();
              }
          });
}
$("#deleteModalBtn").click(function(){
  $("#deleteModal").modal('show');
});
// from Top head-- start
function addEdu(){
  $("#addEducation").modal('show');
}
function addEmp(){
  $("#addEmployment").modal('show');
}
// from Top head-- end
$("#addEducationBtn").click(function(){
  $("#addEducation").modal('show');
});
$("#addEmploymentBtn").click(function(){
  $("#addEmployment").modal('show');
});
$(".viewMailBtn").click(function(){
  id=$(this).attr('id');
  email="<?php echo $email; ?>";
  $.ajax({
           url: '<?php echo BASE_URL; ?>admin/user/templatePreview',
           type: "POST",
           data: {'id': id,'type':"view",'email':email},
           success: function(data) {
            $("#viewMail").html(data);
            $("#viewMail").modal('show');
          }
      });

});
$("#sendEmailBtn").click(function(){
  window.location.href= BASE_URL +'admin/user/sendMail/'+id;
});
var id='<?php echo $userData ['User']['id']; ?>';
    $("#menu>ul>li").eq(1).addClass('active open');
</script>
