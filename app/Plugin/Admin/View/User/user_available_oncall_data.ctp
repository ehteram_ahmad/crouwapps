<?php
//** AJAX Pagination
$this->Js->JqueryEngine->jQueryObject = 'jQuery';
// Paginator options
$this->Paginator->options(array(
    'update' => '#content',
    'evalScripts' => true,
    'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
    'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
    )
);

?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<!-- <?php //echo "<pre>";print_r($userDeviceInfoData); ?> -->
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Search User available and oncall</h1>
                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li><a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Search User available and oncall</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>--> Search User available and oncall</div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return searchAvailableAndOncall();">
                                <div class="form-body">
                                    
                                    
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="email" class="form-control input-sm" id="userEmail" placeholder="Email">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="institution_name" placeholder="Institute Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="type" placeholder="Type">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="Onoff" placeholder="ON/OFF">
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="api_level" placeholder="Api level">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="deviceModel" placeholder="Model">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="os_version" placeholder="OS version">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="product" placeholder="Product">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="version" placeholder="Version">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="device_type" placeholder="Device type">
                                        </div>
                                    </div> -->
                                    
                                    
                                </div>
                                <div class="form-actions center-block txt-center">
                                <button type="submit" class="btn green2" name="btn_search" value="Search" >Search</button>
                                    <!-- <button type="submit" class="btn green2">Search</button> -->
                                    <button type="button" class="btn default">Reset</button>
                                </div>
                            </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="portlet box blue2">
                        <div class="portlet-title">
                            <div class="caption">User available and oncall list</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="rightFloat">
                            <div class="actions">
                                <div class="btn-group">
                                    <button class="btn btn-sm green2 dropdown-toggle" onclick="exportAvailabeAndOnCallTransaction();" type="button" class="btn btn-primary"> <i class="fa fa-external-link"></i> Export </button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="portlet-body flip-scroll" id="content">
                            <table class="table table-bordered table-striped table-condensed flip-content tableButtonNew">
                                <thead class="flip-content">
                                    <tr>
                                        <!-- <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> First Name </a></th> -->
                                        <th> Name </th>
                                        <th> Email </th>
                                        <th> Institute Name </th>
                                        <th> Type </th>
                                        <th> On/Off </th>
                                        <th> PlatForm </th>
                                        <th> Date </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if(count($availableAndOncallTransactionData) > 0){
                                    foreach( $availableAndOncallTransactionData as $userData){
                                    ?>
                                    <tr>
                                        <td><?php 
                                                $fullname = $userData['UserProfile']['first_name']." ".$userData['UserProfile']['last_name'];
                                                echo $fullname;
                                            ?>
                                        </td>
                                        <td><?php echo $userData['User']['email'];?></td>
                                        <td> <?php echo $userData['CompanyName']['company_name']; ?> </td>
                                        <td> <?php echo $userData['AvailableAndOncallTransaction']['type']; ?> </td>
                                        <td><?php 
                                                if($userData['AvailableAndOncallTransaction']['status'] == 1)
                                                {
                                                    echo "ON";
                                                }
                                                else
                                                {
                                                    echo "OFF";
                                                }
                                            ?> 
                                        </td>
                                        <td> <?php echo $userData['AvailableAndOncallTransaction']['device_type']; ?> </td>
                                        <td> <?php echo $userData['AvailableAndOncallTransaction']['created']; ?> </td>
                                    </tr>
                                    <?php } }else{?> 
                                    <tr>
                                        <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                    </tr> 
                                    <?php } ?> 
                                </tbody>
                            </table>
                            <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <?php echo $this->element('pagination'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </div>
</div>
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?> 
<div id="cn-overlay" class="cn-overlay"></div>
<script>
function searchAvailableAndOncall(){
        $(".loader").fadeIn("fast");
         $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/availableAndOncallAllFilter',
                 type: "POST",
                 data: {'textEmail': $('#userEmail').val(), 'textInstitute': $('#institution_name').val(), 'textType': $('#type').val(), 'textOnOff': $('#Onoff').val()},
                 success: function(data) {
                    $('#content').html(data);
                    $(".loader").fadeOut("fast");
                }
            });
      return false;  
    }


function exportAvailabeAndOnCallTransaction(){
    $(".loader").fadeIn("fast");
    // window.open('<?php echo BASE_URL; ?>admin/User/exportAvailabeAndOnCallCsvDownload/' '_blank');

    var BeatTitle = 'aaa';
    var BeatPostedBy = 'aaa';
    var specilities = 'aaa';
    var statusVal= 'aaa';
    var from = 'aaa';
    var to = 'aaa';
    if(specilities==null)
    {
        specilities="";
    }
    window.open('<?php echo BASE_URL; ?>admin/User/exportAvailableAndOnCallCsvDownload/?Beat=all&BeatPostedBy='+BeatPostedBy+'&BeatTitle='+BeatTitle+'&specilities='+specilities+'&statusVal='+statusVal+'&from='+from+'&to='+to, '_blank');
    $(".loader").fadeOut("fast");
}

    function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
      var goVal=$("#chngPage").val();
      if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
        $(".loader").fadeIn("fast");
        $.ajax({
                  // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
                  url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                  type: "POST",
                  data: {'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val(), 'j':goVal, 'limit':limit, 'sort':a,'order':order},
                  success: function(data) { 
                      $('#content').html(data);
                        $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                        if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                        }
                        else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                        }
                        $(".loader").fadeOut("fast");
                  }
              });
      }
      else{
        // alert("more");
      }
      
     }


     function chngCount(val){
            $(".loader").fadeIn("fast");
            var a=$('tr th.active').attr('id');
            var b=$('tr th.ASC').attr('id');
            if(a==b){
                var order='A';
            }
            else{
                var order='DE';
            }
          $.ajax({
                      // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
                      url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                      type: "POST",
                      data: {'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val(), 'limit':val,'sort':a,'order':order},
                      success: function(data) { 
                          $('#content').html(data);
                          $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                          if(a==b){
                          $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                          }
                          else{
                          $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                          }
                          $(".loader").fadeOut("fast");
                      }
                  });
         }

        function abc(val,limit){
            $(".loader").fadeIn("fast");
            var a=$('tr th.active').attr('id');
            var b=$('tr th.ASC').attr('id');
            if(a==b){
                var order='A';
            }
            else{
                var order='DE';
            }
          $.ajax({
                   url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                   type: "POST",
                   data: {'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val(), 'j':val,'limit':limit,'sort':a,'order':order},
                   success: function(data) { 
                        $('#content').html(data);
                       $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                       if(a==b){
                       $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                       }
                       else{
                       $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                       }
                       $(".loader").fadeOut("fast");
                   }
               });
        }

        function sort(val,limit){
            $(".loader").fadeIn("fast");
                var a=$('#'+val).attr('id');
                var b=$('tr th.ASC').attr('id');
                if(a==b){
                    var order='DE';
                }
                else{
                    var order='A';
                }
            $.ajax({
                         url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                         type: "POST",
                         data: {'sort':val,'order':order,'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val(), 'limit':limit},
                         success: function(data) { 
                            // alert(data);
                            $('#content').html(data);
                            $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                            if(a==b){
                            $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                            }
                            else{
                            $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                            }
                            $(".loader").fadeOut("fast");
                        }
                    });
              return false; 
            }

    $(".delBt").click(function(){
        $("#deleteModal").modal('show');
    });
</script>