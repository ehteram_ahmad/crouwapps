<?php
//echo "<pre>";print_r($companyDetail);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>User Report</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">User Report</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            User Report </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo $this->Form->create('',array('url' => '/admin/user/reportDetail', 'default' => 'false', 'class'=>'form-horizontal','onsubmit'=>'return add();')); ?>
                            <div class="form-body width85Auto">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Institute Name</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php 
                                                    foreach ($companyDetail as  $data) {
                                                    $company[$data['CompanyName']['id']]=$data['CompanyName']['company_name'];
                                                    }
                                            echo $this->Form->select('company_id',$company,array('class'=>'form-control','empty'=>'Choose Institute','id'=>'selectCompany')); ?>
                                        </div>
                                   </div>
                                </div>
                            </div>
                            <small>* Select Institute name for User Report</small>
                            <div class="form-actions center-block txt-center">
                                <?php echo $this->Form->submit('Get Report',array('class'=>'btn green2')); ?>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    $("#menu>ul>li").eq(17).addClass('active open');
    $(".sub-menu>li").eq(21).addClass('active open');
    $('#selectAdmin').on('change',function(){
        if($(this).val() == ''){
            alert('Select a valid Privilege');
        }
    });
    function add(){
        var companyName=$("#selectCompany").val();
        if(companyName == ''){
           alert("Please select institute.");
           return false; 
        }
    }
</script>