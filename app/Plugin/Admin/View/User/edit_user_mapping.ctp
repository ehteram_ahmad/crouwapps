<?php
//echo $companyByuserId['CompanyName']['company_name'];
// print_r($companyByuserId);
if($adminUserData['AdminUser']['status'] == 0){
    $adminUserData['AdminUser']['role_id'] = 2;
}
// exit();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Edit User Mapping</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Edit User Mapping</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            Edit User Mapping </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php echo $this->Form->create('',array('url' => '/admin/user/updateUserMapping', 'default' => 'false', 'class'=>'form-horizontal','onsubmit'=>'return edit();')); ?>
                            <div class="form-body width85Auto">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Company Name</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <?php 
                                                    $companyVal = array();
                                                    foreach ($companyDetail as  $data) {
                                                    // $company[$data['CompanyName']['id']]=$data['CompanyName']['company_name'];
                                                    $companyVal[$data['CompanyName']['id']] = $data['CompanyName']['company_name'];
                                                    }
                                                    echo $this->Form->select('company_id',$companyVal,array('class'=>'form-control','default'=>$companyByuserId['CompanyName']['company_name'],'empty'=>'Select Company','id'=>'company_id','value'=>$companyByuserId['CompanyName']['id']));
                                             ?>
                                             <?php  echo $this->Form->hidden('email', ['value'=>$adminUserData['AdminUser']['email']]); ?>
                                        </div>
                                   </div>
                                </div>

                                <!-- <div class="form-group">
                                    <label class="control-label col-md-2">Status</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                        <?php 
                                        // $statusVal=array(
                                        //         "0"=>"Inactive",
                                        //         "1"=>"Active",
                                        //         );
                                            //echo $this->Form->select('status',$statusVal,array('class'=>'form-control','default'=>'0','empty'=>'Select status','id'=>'selectStatus','value'=>$adminUserData['AdminUser']['status']));
                                        ?>
                                        </div>
                                   </div>
                                </div> -->
                                <div class="form-group">
                                    <label class="control-label col-md-2">Privilege</label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                        <?php 
                                        $statusVal=array(
                                                "0"=>"Admin",
                                                "1"=>"Subadmin",
                                                "2"=>"None",
                                                );
                                            echo $this->Form->select('status',$statusVal,array('class'=>'form-control','empty'=>'Select Privilege','id'=>'selectAdmin','value'=>$adminUserData['AdminUser']['role_id']));
                                        ?>
                                            <!-- <?php //echo $this->Form->text('status',array('class'=>'form-control','value'=>$adminUserData['AdminUser']['status'])); ?> -->
                                        </div>
                                   </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Status : </label>
                                    <div class="col-md-10">
                                        <div class="col-md-9">
                                            <label style="padding-top: 7px;" class="col-md-12">This user is currently 
                                            <span id="statusUpdate"><?php
                                                if($adminUserData['AdminUser']['status'] == 1){
                                                    ?>
                                                    <?php echo ($adminUserData['AdminUser']['role_id']==0?'Admin':'Subadmin').' in '.$companyByuserId['CompanyName']['company_name']; ?>
                                                    <?php
                                                }else{
                                                    ?>
                                                    Inactive
                                                    <?php
                                                }
                                            ?>
                                            </span></label>
                                        </div>
                                   </div>
                                </div>
                            </div>
                            <div class="form-actions center-block txt-center">
                                <?php echo $this->Form->submit('Update',array('class'=>'btn green2')); ?>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<script type="text/javascript">
    // $("#menu>ul>li").eq(6).addClass('active open');
    // $(".sub-menu>li").eq(10).addClass('active open');
    $('#selectAdmin').on('change',function(){
        var company_name = $('#company_id :selected').text();
        var text = 'Inactive';
        if($(this).val() == ''){
            alert('Select a valid Privilege');
        }
        // else{
        //     if($(this).val() == '0'){
        //         text = 'Admin in '+company_name;
        //     }else if($(this).val() == '1'){
        //         text = 'Subadmin in '+company_name;
        //     }else{
        //         text = 'Inactive';
        //     }
        // }
        // $('#statusUpdate').html(text);
    });

    $('#company_id').on('change',function(){
        $('#selectAdmin').val('');
    });

    function edit(){
    var companyVal=$("#company_id").val();
    // var statusVal=$("#selectStatus").val();
    var statusVal=$("#selectAdmin").val();

    if(companyVal == ''){
       alert("Please select company.");
       return false; 
    }
    if(statusVal == ''){
       // alert("Please select status.");
       alert('Select a valid Privilege');
       return false; 
    }
    else{
        conf = confirm("Are you sure you want to change?");
        return conf;
    // return true;
       }
    }
</script>