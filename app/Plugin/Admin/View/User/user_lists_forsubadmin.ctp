<?php
    $searchdata =  $this->Session->read('searchdata');
    $firstname = "";
    $lastname = "";
    $email = "";
    if(isset($searchdata['firstname']) && $searchdata['firstname'] != ""){
            $firstname = $searchdata['firstname'];
    }
    if(isset($searchdata['lastname']) && $searchdata['lastname'] != ""){
            $lastname = $searchdata['lastname'];
    }
    if(isset($searchdata['email']) && $searchdata['email'] != ""){
            $email = $searchdata['email'];
    }
    if(isset($searchdata['interestlist']) && count($searchdata['interestlist']) > 0){
        $interest_array = $searchdata['interestlist'];
    }
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<style type="text/css">
    #content td{
        word-break: break-all;
    }
</style>
<div class="right_col" role="main">
<div class="x_panel">
                                
                                    <!-- <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li> -->
                                        
                                    <!-- </ul> -->
                                    <div class="clearfix"></div>
                                <!-- </div> -->
                                <div class="x_title">
                                    <h2 align="center">User Search</h2></div>
                                <hr><hr>
                                <div class="x_content">
                                    <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return searchUser();">
                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-12">First Name <span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="textFirstName" value="<?php echo $firstname ;?>" class="form-control col-md-7 col-xs-12" required="required" id="textFirstName" data-parsley-id="0573"><ul class="parsley-errors-list" id="parsley-id-0573"></ul>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="last-name" class="control-label col-md-3 col-sm-3 col-xs-12">Last Name <span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="textLastName" class="form-control col-md-7 col-xs-12" required="required" value="<?php echo $lastname ;?>" id="textLastName" data-parsley-id="0846"><ul class="parsley-errors-list" id="parsley-id-0846"></ul>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="last-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email<span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="textEmail" class="form-control col-md-7 col-xs-12" required="required"  id="textEmail" value="<?php echo $email;?>" data-parsley-id="0846"><ul class="parsley-errors-list" id="parsley-id-0846"></ul>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <input type="submit" name="btn_search" value="Search" class="btn btn-success" />
                                                &nbsp;&nbsp;&nbsp;
                                                <input type="reset" name="btn_search" value="Reset" class="btn btn-success"  />
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                
<div class="x_panel" id="userList">
                                <div class="x_title">
                                    <h2>User Lists </h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="selectStatus" id="selectStatus" onchange="return searchUser();">
                                                    <option value="">--Filter--</option>
                                                    <option value="1" <?php if($searchdata['status'] == 1 ){?> selected <?php } ?>>Active</option>
                                                    <option value="2" <?php if($searchdata['status'] == 2){?> selected <?php } ?>>Inactive</option>
                                                    <option value="3" <?php if($searchdata['status'] == 3){?> selected <?php } ?>>Approved</option>
                                                    <option value="4" <?php if($searchdata['status'] == 4){?> selected <?php } ?>>Unapproved</option>
                                                </select>
                                                <ul class="parsley-errors-list" id="parsley-id-9185"></ul>
                                            </div>
                                            
                                <div class="x_content">
                                    
                                    <span id="content">
                                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                        <thead>
                                            <tr class="headings">
                                                <th id="first_name" class="DESC" onclick="sort('first_name',<?php echo $limit; ?>);"><a> First Name </a></th>
                                                <th id="last_name" class="DESC" onclick="sort('last_name',<?php echo $limit; ?>);"><a> Last Name </a></th>
                                                <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
                                                <th> Country </th>
                                                <th id="profession" class="DESC" onclick="sort('profession',<?php echo $limit; ?>);"><a> Profession </a></th>
                                                <th id="joining_date" class="DESC" onclick="sort('joining_date',<?php echo $limit; ?>);"><a> Joining Date </a></th>
                                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                                </th>   
                                            </tr>
                                            
                            </thead>

                            <tbody>
                                <?php //echo "<pre>"; print_r($userData);die;
                                $nameUser = $this->Session->read('userName');
                                pr($nameUser);
                                if(count($userData) > 0){
                                    foreach( $userData as $ud){ 
                                ?>
                                    <tr class="even pointer" id="user<?php echo $ud ['User']['id']; ?>">
                                        <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
                                                <td style="width:15%;" class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
                                                <td style="width:25%;" class=" "><?php echo $ud ['User']['email'];?> </td>
                                                <td style="width:14%;" class=" "><?php echo !empty($ud ['Country']['country_name']) ? $ud ['Country']['country_name'] : 'N/A';?> </td>
                                                <td style="width:15%;" class=" "><?php echo !empty($ud ['Profession']['profession_type']) ? $ud ['Profession']['profession_type'] : 'N/A';?> </td>
                                                <td style="width:10%;" class=" "><?php echo date('d-m-Y', strtotime($ud ['User']['registration_date']));?></td>
                                        <td style="width:5%;" class=" ">
                                                    <span id="statusButton<?php echo $ud ['User']['id']; ?>">
                                                    <?php 
                                                     if($ud ['User']['status'] == 1){
                                                    ?>
                                                    
                                                        <button type="button" onclick="updateStatus(<?php echo $ud ['User']['id']; ?>, 'Active');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                                    </span>
                                                    <?php }else{ ?>
                                                    
                                                        <button onclick="updateStatus(<?php echo $ud ['User']['id']; ?>, 'Inactive');" type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                                                       
                                                    <?php } ?>
                                                    </span> 
                                                    <span class="text-center" id="approveButton<?php echo $ud ['User']['id']; ?>">
                                                    <?php 
                                                     if($ud ['User']['approved'] == 1){
                                                    ?>
                                                        <button type="button"  onclick="updateApproveStatus(<?php echo $ud ['User']['id']; ?>, 'Approved');" data-original-title="Approved" class="btn btn-circle btn-icon-only btn-info3 tooltips" aria-describedby="tooltip852273"><i class="fa fa-thumbs-up"></i></button>
                                                    <?php }else{ ?>
                                                        <button type="button" onclick="updateApproveStatus(<?php echo $ud ['User']['id']; ?>, 'Unapproved');" data-original-title="Unapproved" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip62607"><i class="fa fa-thumbs-o-down"></i></button>
                                                    <?php } ?>
                                                    </span>
                                                </td>
                                        <td style="width:1%;" class=" last">
                                           <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="getModalBox(<?php echo $ud ['User']['id']; ?>)"><i class="fa fa-eye"></i>View</button>
                                           
                                            </td>
                                        </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="8"><font color="red">Record(s) Not  Found!</font></td>
                                      </tr> 
                                      <?php } ?>     
                                            </tbody>

                                    </table>
                                    <div class="clearfix"></div>
                                    <?php 
                            if(count($userData) > 0){  
                                echo $this->element('pagination');
                            }
                        ?>
                                </span>
                            
                            </div> 
     </div> 
     <?php
        echo $this->Js->writeBuffer();
     ?>
     <!-- Modal Box [START] -->
        <div id="modalBox"></div>
     <!-- Modal Box [END] --> 
<script>
function searchUser(){
     $.ajax({
             url: '<?php echo BASE_URL; ?>admin/user/userListsForsubadmin',
             type: "POST",
             data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#selectSpecilities').val(), 'statusVal': $('#selectStatus').val()},
             success: function(data) { 
                $('#content').html(data);
            }
        });
  return false;  
}
function sort(val,limit){
        var a=$('#'+val).attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='DE';
        }
        else{
            var order='A';
        }
    $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/user/userListsForsubadmin',
                 type: "POST",
                 data: {'sort':val,'order':order,'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'limit':limit,'from':$('#dateFrom').val(),'to':$('#dateTo').val()},
                 success: function(data) { 
                    // alert(data);
                    $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    else{
                    $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                }
            });
      return false; 
    }
function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/user/userListsForsubadmin',
              type: "POST",
              data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'j':goVal,'limit':limit,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 function chngCount(val){
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/user/userListsForsubadmin',
              type: "POST",
              data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'limit':val,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
              }
          });
 }

function abc(val,limit){
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/user/userListsForsubadmin',
           type: "POST",
           data: {'textFirstName': $('#textFirstName').val(), 'textLastName': $('#textLastName').val(), 'textEmail': $('#textEmail').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'j':val,'limit':limit,'sort':a,'order':order},
           success: function(data) { 
                $('#content').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
           }
       });
}
</script> 


