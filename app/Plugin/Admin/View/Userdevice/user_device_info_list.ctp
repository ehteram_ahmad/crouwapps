<?php
//** AJAX Pagination
$this->Js->JqueryEngine->jQueryObject = 'jQuery';
// Paginator options
$this->Paginator->options(array(
    'update' => '#content',
    'evalScripts' => true,
    'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
    'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
    )
);

?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<!-- <?php //echo "<pre>";print_r($userDeviceInfoData); ?> -->
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Device Search</h1>
                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li><a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Device Search</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>--> Device Search </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return searchUserDevice();">
                                <div class="form-body">
                                    
                                    
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="email" class="form-control input-sm" id="userEmail" placeholder="Email">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="device" placeholder="Device">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="manufacturer" placeholder="Manufacturer">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="device_id" placeholder="Device id">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="api_level" placeholder="Api level">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="deviceModel" placeholder="Model">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="os_version" placeholder="OS version">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="product" placeholder="Product">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="version" placeholder="Version">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" id="device_type" placeholder="Device type">
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="form-actions center-block txt-center">
                                <button type="submit" class="btn green2" name="btn_search" value="Search" >Search</button>
                                    <!-- <button type="submit" class="btn green2">Search</button> -->
                                    <button type="button" class="btn default">Reset</button>
                                </div>
                            </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="portlet box blue2">
                        <div class="portlet-title">
                            <div class="caption">Device List </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="portlet-body flip-scroll" id="content">
                            <table class="table table-bordered table-striped table-condensed flip-content tableButtonNew">
                                <thead class="flip-content">
                                    <tr>
                                        <!-- <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> First Name </a></th> -->
                                        <th> Email </th>
                                        <th> Device </th>
                                        <th> Product </th>
                                        <th> Manufacturer </th>
                                        <th> Device id </th>
                                        <th> Api level </th>
                                        <th> Model </th>
                                        <th> OS version </th>
                                        <th> Version </th>
                                        <th> Device type </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if(count($userDeviceInfoData) > 0){
                                    foreach( $userDeviceInfoData as $deviceData){ 
                                        $userDeviceArr = json_decode($deviceData['UserDeviceInfo']['device_info']);
                                    ?>
                                    <tr>
                                        <td><?php echo $deviceData['User']['email'];?></td>
                                        <td> <?php echo $userDeviceArr->device; ?> </td>
                                        <td> <?php echo $userDeviceArr->product; ?> </td>
                                        <td> <?php echo $userDeviceArr->manufacturer; ?> </td>
                                        <td> <?php echo $userDeviceArr->device_id; ?> </td>
                                        <td> <?php echo $userDeviceArr->api_level; ?> </td>
                                        <td> <?php echo $userDeviceArr->model; ?> </td>
                                        <td> <?php echo $userDeviceArr->os_version; ?> </td>
                                        <td> <?php echo $userDeviceArr->version; ?> </td>
                                        <td> <?php echo $userDeviceArr->device_type; ?> </td>
                                    </tr>
                                    <?php } }else{?> 
                                    <tr>
                                        <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                    </tr> 
                                    <?php } ?> 
                                </tbody>
                            </table>
                            <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <?php echo $this->element('pagination'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </div>
</div>
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?> 
<div id="cn-overlay" class="cn-overlay"></div>
<script>
function searchUserDevice(){
        $(".loader").fadeIn("fast");
         $.ajax({
                 url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                 type: "POST",
                 data: {'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val()},
                 success: function(data) {
                    $('#content').html(data);
                    $(".loader").fadeOut("fast");
                }
            });
      return false;  
    }

    function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
      var goVal=$("#chngPage").val();
      if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
        $(".loader").fadeIn("fast");
        $.ajax({
                  // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
                  url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                  type: "POST",
                  data: {'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val(), 'j':goVal, 'limit':limit, 'sort':a,'order':order},
                  success: function(data) { 
                      $('#content').html(data);
                        $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                        if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                        }
                        else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                        }
                        $(".loader").fadeOut("fast");
                  }
              });
      }
      else{
        // alert("more");
      }
      
     }


     function chngCount(val){
            $(".loader").fadeIn("fast");
            var a=$('tr th.active').attr('id');
            var b=$('tr th.ASC').attr('id');
            if(a==b){
                var order='A';
            }
            else{
                var order='DE';
            }
          $.ajax({
                      // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
                      url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                      type: "POST",
                      data: {'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val(), 'limit':val,'sort':a,'order':order},
                      success: function(data) { 
                          $('#content').html(data);
                          $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                          if(a==b){
                          $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                          }
                          else{
                          $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                          }
                          $(".loader").fadeOut("fast");
                      }
                  });
         }

        function abc(val,limit){
            $(".loader").fadeIn("fast");
            var a=$('tr th.active').attr('id');
            var b=$('tr th.ASC').attr('id');
            if(a==b){
                var order='A';
            }
            else{
                var order='DE';
            }
          $.ajax({
                   url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                   type: "POST",
                   data: {'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val(), 'j':val,'limit':limit,'sort':a,'order':order},
                   success: function(data) { 
                        $('#content').html(data);
                       $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                       if(a==b){
                       $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                       }
                       else{
                       $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                       }
                       $(".loader").fadeOut("fast");
                   }
               });
        }

        function sort(val,limit){
            $(".loader").fadeIn("fast");
                var a=$('#'+val).attr('id');
                var b=$('tr th.ASC').attr('id');
                if(a==b){
                    var order='DE';
                }
                else{
                    var order='A';
                }
            $.ajax({
                         url: '<?php echo BASE_URL; ?>admin/userdevice/userDeviceAllFilter',
                         type: "POST",
                         data: {'sort':val,'order':order,'textEmail': $('#userEmail').val(), 'textDevice': $('#device').val(), 'textDeviceId': $('#device_id').val(), 'textDeviceModel': $('#deviceModel').val(), 'textManufacturer': $('#manufacturer').val(), 'textApiLevel': $('#api_level').val(), 'textOsVersion': $('#os_version').val(), 'textVersion': $('#version').val(),'textDeviceType':$('#device_type').val(),'textProduct':$('#product').val(), 'limit':limit},
                         success: function(data) { 
                            // alert(data);
                            $('#content').html(data);
                            $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                            if(a==b){
                            $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                            }
                            else{
                            $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                            }
                            $(".loader").fadeOut("fast");
                        }
                    });
              return false; 
            }

    $(".delBt").click(function(){
        $("#deleteModal").modal('show');
    });
</script>