
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="closeModal();">&times;</button>
          <h4 class="modal-title">Post Comments</h4>
        </div>
        <div class="modal-body">
          <table border="1" align="center" width="100%">
            <?php 
              if( !empty($userPostComments) ){
            ?>
            <tr class="headings">
              <th style="text-align:center">Comment</th>
              <th style="text-align:center">Status</th>
              <th style="text-align:center">Posted At</th>
          </tr>
            <?php
              foreach( $userPostComments as $comment ){
            ?>
              <tr>
                <td width="55%" align="center">
                  <?php  echo $comment['content'];  ?>
                </td>
                <td width="10%" align="center"><?php echo $comment['status']==1 ? 'Active':'Inactive'; ?></td>
                <td width="20%" align="center"><?php echo $comment['created']; ?></td>
              </tr>
              
            <?php
              }
            }else{ 
            ?>
            <tr><td colspan="4" align="center">Record(s) Not Found!</td></tr>
            <?php 
            }
            ?>
</table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="closeModal();">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>