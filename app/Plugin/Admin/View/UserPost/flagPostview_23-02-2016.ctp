<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="closeModal();">&times;</button>
          <h4 class="modal-title">Flag Beat</h4>
        </div>
        <div class="modal-body">
              <table border="0" align="center" width="100%">
                <tr>
                  <td width="30%"><strong>Spam Type</strong></td>
                  <td width="5%">:</td>
                  <td width="65%">
                    <?php 
                      if(count($userPostdata[spam_type]) > 0){
                          foreach($userPostdata[spam_type] as $spam_type){

                      ?>
                        <input type="radio" name="spam_type" id="spam_type" value="<?php echo $spam_type['id'];?>">&nbsp;<?php echo $spam_type['spam_name'];?><br />
                      <?php     
                          }
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td width="30%"><strong>Comment</strong></td>
                  <td width="5%">:</td>
                  <td width="65%">
                    <textarea style="margin: 0px; width: 525px; height: 191px;" id="txt_comment" name="txt_comment"></textarea>
                    <input type="hidden" name="hdn_postid" id="hdn_postid" value="<?php echo $userPostdata['post_id']?>"
                  </td>
                </tr>
                <tr>
                  <td width="100%" colspan="3" align="center">&nbsp;</td>
                </tr>
                <tr>
                  <td width="100%" colspan="3" align="center">
                      <input type="button" name="btn_markspam" id = "btn_markspam" value="Submit" class="btn btn-success" onclick="flagpost();"/>
                      <span id = "show_progress"></span>
                  </td>
                  <script type="text/javascript">
                  
                     function flagpost(){
                          var post_id = $("#hdn_postid").val();
                          var comment = $("#txt_comment").val();
                          var spam_type = $("input[name='spam_type']:checked").val();
                          var spam_content = ""
                          if(spam_type){
                              spam_content = $("input[name='spam_type']:checked").val();
                          }
                          if(spam_content == ""){
                            alert('Please select spam type');
                            return false;
                          }
                          if(comment == ""){
                            alert('Please add your comment');
                            $("#txt_comment").focus();
                            return false;
                          }else{
                              $("#btn_markspam").hide();
                              $("#show_progress").html('Please wait ...');
                              $.ajax({
                                  url: "<?php echo BASE_URL; ?>admin/UserPost/markFlag",
                                  type: 'POST',
                                  data: {"post_id": post_id, "spam_type": spam_type, "comment" : comment},
                                  success: function( result ){
                                      if(result == "success"){
                                          alert('Beat flagged successfully');
                                          closeModal();
                                          location.reload(true); 
                                      }          
                                  }
                              });
                          }
                     } 
                 
                  </script>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="closeModal();">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

