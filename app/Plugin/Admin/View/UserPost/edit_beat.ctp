<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
img#editimg0,img#editimg1,img#editimg2,img#editimg3,img#editimg4,img#editimg5,img#editimg6,img#doc0,img#doc1,img#doc2,img#doc3{
    border:1px solid black;
    width:100px!important;
    height:100px!important;
}
a#img0,a#img1,a#img2,a#img3,a#img4,a#img5,a#img6,a#doc0,a#doc1,a#doc2,a#doc3{
        position:relative;
        left:20%;
    }
#main{
  position: fixed;
  z-index: 9994;
}
div#editimg0,div#editimg1,div#editimg2,div#editimg3,div#editimg4,div#editimg5{
  position: relative;
  left:20%;
  top:10%;
}
.stop-scrolling {
    height: 100%;
    overflow: hidden;
}
.blurr{
        margin-top: -120px;
        margin-left: -80px;
        height: 120%;
        width:150%;
        position: fixed;
        background: grey;
        opacity: 0.8;
        z-index: 9993;
    }
#shtBtn{
    border-radius: 20px!important;
    float: right;
    margin-right: 26%;
    margin-top: -1%;
}
</style>
<div class="blurr hidden"></div>
<div id="main" class="col-md-12">
<button class="btn btn-danger hidden" id="shtBtn" onclick="rmvEditor();">X</button>
  <div id="editimg0" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg1" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg2" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg3" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg4" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg5" class="hidden" style="width: 640px; height: 480px;"></div>
</div>
<?php //pr($userPostdata);exit(); ?>
<!-- BEGIN CONTENT -->
<?php 
echo $this->Html->css(array('Admin.PhotoEditorReactUI'));
echo $this->Html->script(array(
// 'Admin.ckeditor/ckeditor.js',
'Admin.PhotoEditorSDK',
'Admin.PhotoEditorReactUI')); ?>

<div id="content" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Edit Beat</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/UserPost/userPostLists';?>">All Posts</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span class="active">Post Details</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            Beat Details </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('Update',array('id'=>'formEdit','onsubmit'=>'return editBeat();','type'=>'file')); ?>
                        <!-- <form action="<?php //echo BASE_URL.'admin/UserPost/updated'; ?>" class="form-horizontal" onsubmit="return editBeat();" id="formEdit" enctype="multipart/form-data" method="post" accept-charset="utf-8"> -->
                            <div class="form-body">
                                <!--<h2 class="margin-bottom-20"> View User Info - Bob Nilson </h2>
                                <h3 class="form-section">Person Info</h3>-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Posted On:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <?php
                                              $datetime = explode(" ",$userPostdata[0]['UserPost']['post_date']);
                                              $date = $datetime[0];
                                              $time = $datetime[1];
                                              echo date('d-m-Y',strtotime($date))."  ".date ('g:i:s a',strtotime($time));
                                              ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Posted By:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <?php 
                                                echo $userPostdata[0]['UserProfile']['first_name'].' '.$userPostdata[0]['UserProfile']['last_name'];
                                                 ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Title:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <a href="javascript:;" id="comments" data-type="textarea" data-pk="1" data-placeholder="Your Title here..." data-original-title="Enter Title"><?php 
                                                echo $userPostdata[0]['UserPost']['title'];
                                                 ?></a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Interests:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <a href="#" id="tags" data-type="select2" data-pk="1" data-title="Enter Interests"><?php
                                                if($userPostdata[0]['specialitystring']==""){
                                                   echo "Null";
                                                }
                                                else{ 
                                                echo $userPostdata[0]['specialitystring'];}
                                                 ?></a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                       
                                    </script>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Description:</label>
                                            <div class="col-md-9">
                                            <!-- <textarea id="comments2" class="ckeditor" style="visibility: hidden; display: none;"> -->
                                            <?php foreach ($userPostdata as $key => $value){
                                                if(isset($value['UserPostAttribute'])){
                                                    $descAtt=$value['UserPostAttribute'];
                                                }
                                                else{
                                                    $descrip="";
                                                }
                                                }
                                                foreach ($descAtt as $key => $value) {if($value['attribute_type']=='text'){
                                                    $descrip= $value['content'];
                                                    }
                                                    } 

                                            ?>
                                            <p class="form-control-static"><a href="javascript:;" id="comments2" data-type="textarea" data-pk="1" data-placeholder="Your Description here..." data-original-title="Enter Description"><?php echo $descrip; ?></a></p>
                                            </div>
                                        </div>
                                        </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Hashtags:</label>
                                            <div class="col-md-9">
                                                <p id="tagTxt" class="form-control-static"><?php 
                                                foreach ($userPostdata as $key => $value) {
                                                    if(isset($value['HashTag']))
                                                    {
                                                        $tag[]=$value['HashTag'];
                                                    }
                                                    else{
                                                        $tag=NULL;
                                                    }
                                                }
                                                $tag = array_map("unserialize", array_unique(array_map("serialize", $tag)));
                                                $tagCount=count($tag);
                                                if(isset($tag[0]['id'])){
                                                for ($i=0; $i<$tagCount ; $i++) { 
                                                   echo "#".$tag[$i]['name'].",";
                                                }}
                                                else{
                                                    echo "Null";
                                                }
                                                 ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->    
                                </div>
                                <!--/row-->
                                <div class="row">  
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Video:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"><?php 
                                                foreach ($userPostdata as $key => $value) {
                                                    if(isset($value['UserPostAttribute'])){
                                                    $att=$value['UserPostAttribute'];
                                                }}
                                                $attr=array_merge($att);
                                                foreach ($att as $key => $value) {
                                                    if($value['attribute_type']=='video'){
                                                        $vid=$value['content'];
                                                    }
                                                    }
                                                    if(!isset($vid)){
                                                        $vid="";
                                                        $vidMsg="No File";
                                                    }
                                                    else{
                                                        $vidMsg="";
                                                    }
                                                 ?>
                                                <a href="<?php echo $vid; ?>" target="_blank" id="link1" data-type="text" data-pk="1" data-title="VIDEO URL">
                                                <?php if(!$vid=="") { ?>
                                                <i class="fontSize20 fa fa-file-video-o" aria-hidden="true"></i><?php } ?><span id="vidEl" hidden><?php echo $vid; ?></span>
                                                
                                                </a>
                                                </p>
                                                <script type="text/javascript">
                                                    $(document).ready(function(){
                                                        $("#link1").click(function(){
                                                             if(!$('#link1').attr('href')==""){
                                                        $conf=confirm("Do you want to play video?");
                                                        if($conf==true){
                                                           
                                                            window.open($('#link1').attr('href'), '_blank');
                                                            }
                                                        }
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Documents:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                <?php 
                                                foreach ($userPostdata as $key => $value) {
                                                    if(isset($value['UserPostAttribute'])){
                                                    $attr=$value['UserPostAttribute'];
                                                }}
                                                $docCnt = 1;
                                                $attr=array_merge($attr);
                                                $attrCount=count($attr);
                                                for ($i=0; $i < $attrCount; $i++) { 
                                                if(in_array($userPostdata[0]['UserPostAttribute'][$i]['attribute_type'],array('doc','docx','DOC','DOCX','ppt','PPT','pptx','PPTX','xls','XLS','xlsx','XLSX','pdf','PDF','rtf','RTF'))){
                                                $doc[$docCnt]=$userPostdata[0]['UserPostAttribute'][$i]['content'];
                                                $type[$docCnt]=explode(".",$doc[$docCnt]);
                                                $type[$docCnt]=$type[$docCnt][1];
                                                $docCnt++;
                                                    }
                                                }
                                                for($j=1;$j<=4;$j++){
                                                    if(isset($type[$j])){
                                                   switch (strtolower($type[$j])) {
                                                        case 'doc':case 'docx':case 'rtf':
                                                            $file="word";
                                                        break;
                                                        case 'xls':case 'xlsx':
                                                            $file="excel";
                                                        break;
                                                        case 'pdf':
                                                            $file="pdf";
                                                        break;
                                                        case 'ppt':case 'pptx':
                                                            $file="powerpoint";
                                                        break;
                                                    }}
                                                    if(isset($doc[$j])){ ?>
                                                        <span id="<?php echo $j; ?>">
                                                          <a class="fontSize20" href="<?php echo AMAZON_PATH.'posts/'.$id.'/doc/'.$doc[$j];?>" value="<?php echo $doc[$j]; ?>" target="_blank"> <i class="fa fa-file-<?php echo $file; ?>-o" aria-hidden="true"></i><span hidden id="doc<?php echo $j;?>"><?php echo $doc[$j]; ?></span>
                                                          </a>
                                                          <a onclick="del('<?php echo $j; ?>');" data-dismiss="fileinput" class="btn btn-xs btn default fileinput-exists" href="javascript:;"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                                          </span>&nbsp;
                                                        <span id="up<?php echo $j; ?>" class="btn btn-xs default btn-file hidden"><span class="fileinput-exists"> <i class="fa fa-upload" aria-hidden="true"></i></span><input type="hidden" value="" name="">
                                                            <?php echo $this->Form->file('doc.',array('type'=>'file','onchange'=>'upDoc(this,'."'".$j."'".');','id'=>"file$j",'class'=>'')); ?>
                                                        </span>
                                                          <br>
                                                    <?php } 
                                                    else{
                                                        ?>
                                                        <span id="<?php echo $j; ?>">
                                                        <a class="fontSize20" id="link5" data-type="text"><i class="fa fa-file-o tooltips" data-original-title="No file" aria-hidden="true"></i></a></span>&nbsp;
                                                        <span id="up<?php echo $j; ?>" class="btn btn-xs default btn-file"><span class="fileinput-exists"> <i class="fa fa-upload" aria-hidden="true"></i></span><input type="hidden" value="" name="">
                                                            <?php echo $this->Form->file('doc.',array('type'=>'file','onchange'=>'upDoc(this,'."'".$j."'".');','id'=>"file$j",'class'=>'')); ?>
                                                        </span>
                                                    <br>
                                                        <?php
                                                    }
                                                }

                                                ?>
                                                </p>
                                            </div>
                                            <!-- <span>skdb</span> -->
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Consent Form:</label>
                                            <div class="col-md-9">
                                            <?php 
                                                if(!$userPostdata[0]['UserPost']['consent_id']>0){
                                                    $pname=Null;
                                                    $pmail=Null;
                                                }
                                                else{
                                                    $consentId=$userPostdata[0]['UserPost']['consent_id'];
                                                
                                                foreach ($userPostdata as $key => $value) {
                                                   if(isset($value['ConsentForm'])){
                                                    $ConsentForm[]=$value['ConsentForm'];
                                                   }
                                                }
                                                $ConsentForm = array_map("unserialize", array_unique(array_map("serialize", $ConsentForm)));
                                                // pr($ConsentForm);
                                                $consnetCount=count($ConsentForm);
                                                for($i=0;$i<$consnetCount;$i++){
                                                    $pname=$ConsentForm[$i]['patient_name'];
                                                    $pmail=$ConsentForm[$i]['patient_email'];
                                                }
                                                  }   ?>
                                                <p class="form-control-static"><?php echo $pname; ?><br /> <?php echo $pmail; ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Status:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"><?php if($userPostdata[0]['UserPost']['status']=="1")
                                                    {
                                                        echo "Active";
                                                    }
                                                    elseif ($userPostdata[0]['UserPost']['status']=="0") {
                                                        echo "Inactive";
                                                    }
                                                    else{
                                                        echo "Deleted";
                                                    } ?>
                                                 &nbsp;
                                                <?php
                                                if($userPostdata[0]['UserPost']['spam_confirmed']=="1"){
                                                        echo "/ Flagged";
                                                    }
                                                   else if($userPostdata[0]['UserPost']['top_beat']=="1"){
                                                        echo "/ Featured";
                                                    }
                                                    else{
                                                        echo "";
                                                    }
                                                    ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="form-section">Images</h3>
                                <div id="imageContent" class="form-group form-inline tile image cbp-item">
                                    <div class="col-md-12">
                                        <div class="row">
                                    <?php 
                                    if(!empty($userPostdata[0]['UserPostAttribute']) && isset($userPostdata[0]['UserPostAttribute'])){
                                        foreach ($userPostdata[0]['UserPostAttribute'] as $key => $value) {
                                           if($value['attribute_type']=="image"){
                                            $imgData[]=$value['content'];
                                           }
                                        }
                                        if(!isset($imgData)){
                                            $imgData=null;
                                        }
                                    }
                                    for($i=0;$i<6;$i++){
                                        if(isset($imgData[$i])){
                                        $img[$i]=AMAZON_PATH.'posts/'.$userPostdata[0]['UserPost']['id'].'/image/'.$imgData[$i];
                                    }
                                    else{
                                        $img[$i]= BASE_URL.'admin/img/plus.gif';
                                    }
                                    ?>
                                    
                                                <div id="maineditimg<?php echo $i; ?>" class="col-md-2">
                                                    <span class="btn btn-lg success btn-file" onclick="sh(<?php echo $i; ?>);">
                                                        <span class="fileinput-exists">
                                                            <img id="editimg<?php echo $i; ?>" src="<?php echo $img[$i]; ?>">
                                                        </span>
                                                    </span>
                                                    <br><a onclick="rmvImg('<?php echo $i; ?>');" id="img<?php echo $i; ?>" class="btn default"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                                </div>
                                    <?php
                                    }
                                    ?>
                                            </div>
                                        </div>
                                    </div>
                            <div class="form-actions">
                                <div class="row">
                                            <div class="txtCenter">
                                                <button id="ss" type="submit" class="btn green2">
                                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                                                <button type="button" class="btn default"><a href="<?php echo BASE_URL . 'admin/UserPost/userPostDetails/'.$userPostdata[0]['UserPost']['id']; ?>">Cancel</a></button>
                                            </div>
                                        </div>
<?php echo $this->Form->text('userId',array('class'=>'hidden','value'=>$userPostdata[0]['UserPost']['user_id'],'id'=>'userId'));?>
<?php echo $this->Form->text('title',array('class'=>'hidden','value'=>'','id'=>'title'));?>
<?php echo $this->Form->text('interests',array('class'=>'hidden','value'=>'','id'=>'interests'));?>
<?php echo $this->Form->text('descriptions',array('class'=>'hidden','value'=>'','id'=>'descriptions'));?>
<?php echo $this->Form->text('hashtags',array('class'=>'hidden','value'=>'','id'=>'hashtags'));?>
<?php echo $this->Form->text('newHash',array('class'=>'hidden','value'=>'','id'=>'newHash'));?>
<?php echo $this->Form->text('video',array('class'=>'hidden','value'=>'','id'=>'video'));?>
<?php echo $this->Form->text('doc1',array('class'=>'hidden','value'=>'','id'=>'docTxt1'));?>
<?php echo $this->Form->text('doc2',array('class'=>'hidden','value'=>'','id'=>'docTxt2'));?>
<?php echo $this->Form->text('doc3',array('class'=>'hidden','value'=>'','id'=>'docTxt3'));?>
<?php echo $this->Form->text('doc4',array('class'=>'hidden','value'=>'','id'=>'docTxt4'));?>
<?php echo $this->Form->text('status1',array('class'=>'hidden','value'=>'','id'=>'status1'));?>
<?php echo $this->Form->text('status2',array('class'=>'hidden','value'=>'','id'=>'status2'));?>
<?php echo $this->Form->text('img0',array('class'=>'hidden','value'=>'','id'=>'imgTxt0'));?>
<?php echo $this->Form->text('img1',array('class'=>'hidden','value'=>'','id'=>'imgTxt1'));?>
<?php echo $this->Form->text('img2',array('class'=>'hidden','value'=>'','id'=>'imgTxt2'));?>
<?php echo $this->Form->text('img3',array('class'=>'hidden','value'=>'','id'=>'imgTxt3'));?>
<?php echo $this->Form->text('img4',array('class'=>'hidden','value'=>'','id'=>'imgTxt4'));?>
<?php echo $this->Form->text('img5',array('class'=>'hidden','value'=>'','id'=>'imgTxt5'));?>
<?php echo $this->Form->textarea('editedImg0',array('class'=>'hidden','value'=>'','id'=>'txteditimg0'));?>
<?php echo $this->Form->textarea('editedImg1',array('class'=>'hidden','value'=>'','id'=>'txteditimg1'));?>
<?php echo $this->Form->textarea('editedImg2',array('class'=>'hidden','value'=>'','id'=>'txteditimg2'));?>
<?php echo $this->Form->textarea('editedImg3',array('class'=>'hidden','value'=>'','id'=>'txteditimg3'));?>
<?php echo $this->Form->textarea('editedImg4',array('class'=>'hidden','value'=>'','id'=>'txteditimg4'));?>
<?php echo $this->Form->textarea('editedImg5',array('class'=>'hidden','value'=>'','id'=>'txteditimg5'));?>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- <div id="long" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Video</h4>
        </div>
        <div class="modal-body">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/oVsP_qKnb84" frameborder="0" allowfullscreen></iframe>
            </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
        </div>
    </div>
</div>
</div> -->
<script type="text/javascript">
    $("#menu>ul>li").eq(2).addClass('active open');

</script>