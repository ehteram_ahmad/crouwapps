<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="closeModal();">&times;</button>
          <h4 class="modal-title">Post Attributes</h4>
        </div>
        <div class="modal-body">
          <table border="1" align="center" width="100%">
            <?php 
              if( !empty($userPostAttributes) ){
            ?>
            <tr class="headings">
              <th style="text-align:center">Attribute Type</th>
              <th style="text-align:center">Content</th>
              <th style="text-align:center">Status</th>
              <th style="text-align:center">Posted At</th>
          </tr>
            <?php
              foreach( $userPostAttributes as $uAttr ){
            ?>
              <tr>
                <td width="15%" align="center"><?php echo $uAttr['attribute_type']; ?></td>
                <td width="55%" align="center">
                  <?php if( $uAttr['attribute_type'] == "text" ){ echo $uAttr['content'];  ?>
                  <?php }else if( $uAttr['attribute_type']=="image" ){  ?>
                  <img src="<?php echo $uAttr['content']; ?>" width="100px" height="100px" />
                  <?php } ?>
                </td>
                <td width="10%" align="center"><?php echo $uAttr['status']==1 ? 'Active':'Inactive'; ?></td>
                <td width="20%" align="center"><?php echo date('d-m-Y', strtotime($uAttr['added_date'])); ?></td>
              </tr>
              
            <?php
              }
            }else{ 
            ?>
            <tr><td colspan="4" align="center">Record(s) Not Found!</td></tr>
            <?php 
            }
            ?>
</table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="closeModal();">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

