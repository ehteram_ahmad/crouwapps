<!-- BEGIN CONTENT -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Beat Details</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="postsList.html">Posts</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Post Details</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Beat Details </div>
                                    <div class="tools">
                                        
                                        <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                        <a href="javascript:;" class="remove"> </a>-->
                                        <a href="javascript:;" class="collapse"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal" role="form">
                                        <div class="form-body">
                                            <!--<h2 class="margin-bottom-20"> View User Info - Bob Nilson </h2>
                                            <h3 class="form-section">Person Info</h3>-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Posted On:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> 05-04-2016 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Posted By:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> Nilson </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Title:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> Why are you in the medical school? Understand the real purpose of you. </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Interests:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> Cardiology, Clinical Neurophysiology, Dermato-Venereology  </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Description:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> A 30-year-old Asian male recently diagnosed as a case of #tuberculosis presented with the following change in his urine.most probably he is receiving rifampicin. </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Hashtags:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> #dehydration, #laxatives,  #containing </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                            	
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Video:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static">
                                                            <a class="fontSize20" data-toggle="modal" href="#long"><i class="fa fa-file-video-o" aria-hidden="true"></i></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Documents:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static">
                                                            <a class="fontSize20" href="#"> <i class="fa fa-file-word-o" aria-hidden="true"></i></a>
                                                            <a class="fontSize20" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                                            <a class="fontSize20" href="#"> <i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                                                            <a class="fontSize20" href="#"><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i></a> 
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <!--<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Consent Form:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> </p>
                                                        </div>
                                                    </div>
                                                </div>-->
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Consent Form:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static">Gracy Miller <br /> gracy@yahoo.com </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 bold">Status:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> Active, Inactive, Flagged, Approved</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <h3 class="form-section">Images</h3>
                                            <div class="row">
                                                <div id="js-grid-full-width" class="tiles cbp">
                                                <div class="tile image cbp-item">
                                                    <div class="tile-body ">
                                                        <a href="images/teambg1.jpg" class="cbp-caption cbp-lightbox">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="images/teambg1.jpg" alt=""> </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tile image cbp-item">
                                                    <div class="tile-body ">
                                                        <a href="images/teambg2.jpg" class="cbp-caption cbp-lightbox">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="images/teambg2.jpg" alt=""> </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tile image cbp-item">
                                                    <div class="tile-body ">
                                                        <a href="images/teambg3.jpg" class="cbp-caption cbp-lightbox">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="images/teambg3.jpg" alt=""> </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tile image cbp-item">
                                                    <div class="tile-body ">
                                                        <a href="images/teambg4.jpg" class="cbp-caption cbp-lightbox">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="images/teambg4.jpg" alt=""> </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tile image cbp-item">
                                                    <div class="tile-body ">
                                                        <a href="images/teambg5.jpg" class="cbp-caption cbp-lightbox">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="images/teambg5.jpg" alt=""> </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tile image cbp-item">
                                                    <div class="tile-body ">
                                                        <a href="images/teambg6.jpg" class="cbp-caption cbp-lightbox">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="images/teambg6.jpg" alt=""> </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <!--<div class="tile image cbp-item">
                                                    <div class="tile-body ">
                                                        <a href="images/teambg7.jpg" class="cbp-caption cbp-lightbox">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="images/teambg7.jpg" alt=""> </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tile image cbp-item">
                                                    <div class="tile-body ">
                                                        <a href="images/teambg8.jpg" class="cbp-caption cbp-lightbox">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="images/teambg8.jpg" alt=""> </div>
                                                        </a>
                                                    </div>
                                                </div>-->
                                            </div>
                                         </div>
                                    <!--/row-->
                                </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                        <div class="txtCenter">
                                                            <button type="submit" class="btn green2" onclick="javascript:window.location.href='postEdit.html'; return false;">
                                                                <i class="fa fa-pencil"></i> Edit</button>
                                                            <button type="button" class="btn default">Cancel</button>
                                                        </div>
                                                    </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                <!-- END SAMPLE FORM PORTLET-->
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-green2-haze bold uppercase">STATISTICS</span>
                                    </div>
                                    <div class="tools">
                                        <a href="" class="collapse"> </a>
                                        <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                        <a href="" class="reload"> </a>
                                        <a href="" class="remove"> </a>-->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                     <!-- BEGIN WIDGET TAB -->
                                    <div class="widget-bg-color-white widget-tab">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_1" data-toggle="tab" class="iconCounts tooltips" data-original-title="Up Beats">
                                                    <i class="demo-icon icon2-up fontSize20"></i>
                                                    <span class="badge badge-success2"> 1 </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_2" data-toggle="tab" class="iconCounts tooltips" data-original-title="Down Beats">
                                                    <i class="demo-icon icon2-down fontSize20"></i>
                                                    <span class="badge badge-success2"> 2 </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_3" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Comments">
                                                    <i class="fa fa-comment-o fontSize20"></i>
                                                    <span class="badge badge-success2"> 4 </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_4" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Shares">
                                                    <i class="fa fa-share-alt fontSize20"></i>
                                                    <span class="badge badge-success2"> 4 </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_5" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Views">
                                                    <i class="fa fa-eye fontSize20"></i>
                                                    <span class="badge badge-success2"> 4 </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_6" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Tagged">
                                                    <i class="fa fa-tag fontSize20"></i>
                                                    <span class="badge badge-success2"> 44 </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_7" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Flaged">
                                                    <i class="fa fa-flag fontSize20"></i>
                                                    <span class="badge badge-success2"> 5 </span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content scroller2" style="height: 380px;" data-always-visible="1" data-handle-color="#D7DCE2">
                                            <div class="tab-pane fade active in" id="tab_1_1">
                                            	<div class="widget-news margin-bottom-20 bold">
                                                	<span class="widget-news-left-elem">S. NO.</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width87">
                                                        	User Name
                                                            <span class="widget-news-right-elem"> Time </span>
                                                            
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">1</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Gracy Miller
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">2</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Billy Sanders
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">3</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Chris Patterson
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">4</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Dave Pit
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">5</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Billy Sanders
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">6</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Deepak Chaula
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news">
                                                	<span class="widget-news-left-elem">7</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Gracy Miller
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row paginationTopBorder">
                                                	<div class="col-md-5 col-sm-5">
                                                        <div class="dataTables_info lineHeight60" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7">
                                                        <div class="dataTables_paginate paging_bootstrap_full_number rightFloat" id="sample_1_paginate">
                                                            <ul class="pagination green2" style="visibility: visible;">
                                                                <li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li>
                                                                <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                                                                <li class="active"><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li><li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li><li><a href="#">5</a></li>
                                                                <li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                                                                <li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_2">
                                            	<div class="widget-news margin-bottom-20 bold">
                                                	<span class="widget-news-left-elem">S. NO.</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width87">
                                                        	User Name
                                                            <span class="widget-news-right-elem"> Time </span>
                                                            
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">1</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Gracy Miller
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">2</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Billy Sanders
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">3</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Chris Patterson
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">4</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Dave Pit
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">5</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Billy Sanders
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">6</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Deepak Chaula
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news">
                                                	<span class="widget-news-left-elem">7</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Gracy Miller
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row paginationTopBorder">
                                                	<div class="col-md-5 col-sm-5">
                                                        <div class="dataTables_info lineHeight60" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7">
                                                        <div class="dataTables_paginate paging_bootstrap_full_number rightFloat" id="sample_1_paginate">
                                                            <ul class="pagination green2" style="visibility: visible;">
                                                                <li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li>
                                                                <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                                                                <li class="active"><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li><li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li><li><a href="#">5</a></li>
                                                                <li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                                                                <li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_3">
                                            	<div class="widget-news margin-bottom-20 bold">
                                                	<span class="widget-news-left-elem">S. NO.</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">User Name</span>
                                                            <span class="widget-news-left-elem left"> Time </span>
                                                            <span class="widget-news-right-elem">Comments</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">1</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Gracy Miller</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">2</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Billy Sanders</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">3</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Chris Patterson</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">4</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Dave Pit</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">5</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Billy Sanders</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">6</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Deepak Chaula</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news">
                                                	<span class="widget-news-left-elem">7</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Gracy Miller</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row paginationTopBorder">
                                                	<div class="col-md-5 col-sm-5">
                                                        <div class="dataTables_info lineHeight60" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7">
                                                        <div class="dataTables_paginate paging_bootstrap_full_number rightFloat" id="sample_1_paginate">
                                                            <ul class="pagination green2" style="visibility: visible;">
                                                                <li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li>
                                                                <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                                                                <li class="active"><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li><li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li><li><a href="#">5</a></li>
                                                                <li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                                                                <li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_4">
                                            	<div class="widget-news margin-bottom-20 bold">
                                                	<span class="widget-news-left-elem">S. NO.</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width87">
                                                        	User Name
                                                            <span class="widget-news-right-elem"> Time </span>
                                                            
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">1</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Gracy Miller
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">2</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Billy Sanders
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">3</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Chris Patterson
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">4</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Dave Pit
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">5</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Billy Sanders
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">6</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Deepak Chaula
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news">
                                                	<span class="widget-news-left-elem">7</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Gracy Miller
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row paginationTopBorder">
                                                	<div class="col-md-5 col-sm-5">
                                                        <div class="dataTables_info lineHeight60" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7">
                                                        <div class="dataTables_paginate paging_bootstrap_full_number rightFloat" id="sample_1_paginate">
                                                            <ul class="pagination green2" style="visibility: visible;">
                                                                <li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li>
                                                                <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                                                                <li class="active"><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li><li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li><li><a href="#">5</a></li>
                                                                <li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                                                                <li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_5">
                                            	<div class="widget-news margin-bottom-20 bold">
                                                	<span class="widget-news-left-elem">S. NO.</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">
                                                        	User Name
                                                            
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">1</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">Gracy Miller</p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">2</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">Billy Sanders</p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">3</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">Chris Patterson</p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">4</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">Dave Pit</p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">5</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">Billy Sanders</p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">6</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">Deepak Chaula</p>
                                                    </div>
                                                </div>
                                                <div class="widget-news">
                                                	<span class="widget-news-left-elem">7</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">Gracy Miller</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row paginationTopBorder">
                                                	<div class="col-md-5 col-sm-5">
                                                        <div class="dataTables_info lineHeight60" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7">
                                                        <div class="dataTables_paginate paging_bootstrap_full_number rightFloat" id="sample_1_paginate">
                                                            <ul class="pagination green2" style="visibility: visible;">
                                                                <li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li>
                                                                <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                                                                <li class="active"><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li><li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li><li><a href="#">5</a></li>
                                                                <li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                                                                <li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_6">
                                            	<div class="widget-news margin-bottom-20 bold">
                                                	<span class="widget-news-left-elem">S. NO.</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width87">
                                                        	User Name
                                                            <span class="widget-news-right-elem"> Time </span>
                                                            
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">1</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Gracy Miller
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">2</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Billy Sanders
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">3</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Chris Patterson
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">4</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Dave Pit
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">5</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Billy Sanders
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">6</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Deepak Chaula
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news">
                                                	<span class="widget-news-left-elem">7</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title width50">Gracy Miller
                                                            <span class="label label-default"> 11-04-2016 </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row paginationTopBorder">
                                                	<div class="col-md-5 col-sm-5">
                                                        <div class="dataTables_info lineHeight60" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7">
                                                        <div class="dataTables_paginate paging_bootstrap_full_number rightFloat" id="sample_1_paginate">
                                                            <ul class="pagination green2" style="visibility: visible;">
                                                                <li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li>
                                                                <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                                                                <li class="active"><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li><li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li><li><a href="#">5</a></li>
                                                                <li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                                                                <li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_7">
                                            	<div class="widget-news margin-bottom-20 bold">
                                                	<span class="widget-news-left-elem">S. NO.</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">User Name</span>
                                                            <span class="widget-news-left-elem left"> Time </span>
                                                            <span class="widget-news-right-elem">Reasons</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">1</span>
                                                    <div class="widget-news-right-body">
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Gracy Miller</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">2</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Billy Sanders</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">3</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Chris Patterson</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">4</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Dave Pit</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">5</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Billy Sanders</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news margin-bottom-20">
                                                	<span class="widget-news-left-elem">6</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Deepak Chaula</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="widget-news">
                                                	<span class="widget-news-left-elem">7</span>
                                                    <div class="widget-news-right-body">
                                                        
                                                        <p class="widget-news-right-body-title">
                                                        	<span class="widget-news-left-elem width20">Gracy Miller</span>
                                                            <span class="widget-news-left-elem label left"> 11-04-2016 </span>
                                                            <span class="widget-news-right-elem">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row paginationTopBorder">
                                                	<div class="col-md-5 col-sm-5">
                                                        <div class="dataTables_info lineHeight60" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7">
                                                        <div class="dataTables_paginate paging_bootstrap_full_number rightFloat" id="sample_1_paginate">
                                                            <ul class="pagination green2" style="visibility: visible;">
                                                                <li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li>
                                                                <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                                                                <li class="active"><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li><li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li><li><a href="#">5</a></li>
                                                                <li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                                                                <li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END WIDGET TAB -->
                                </div>
                            </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->