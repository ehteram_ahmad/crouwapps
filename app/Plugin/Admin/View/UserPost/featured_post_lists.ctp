<?php
 // pr($limit);
// pr($condition);
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
    
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .menuButton3 .circular-menu.open .circle{
    left: -195px;
  }
  .menu-button {
    left: calc(50% - -3px);
  }
  @media only screen and (device-width: 768px){
.radialMenu.menuButton3 .circular-menu.open .circle {
    width: 34px;
    left: 20px;
    top: -230px;
}
.menuButton3 .circular-menu.open .menu-button {
    left: 20px;
}
}
@media (max-width: 767px){
.menuButton3 .circle {
    width: 34px;
    position: relative;
    height: 34px;
}}
.calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
#content td{
        word-break: break-all;
    }
    div.radio input{
        opacity: 1!important;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Beat Search</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard'?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/UserPost/userPostLists'?>">Posts</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Featured Posts List</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2 ">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>--> Beat Search </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="" class="remove"> </a>
                            <a href="" class="reload"> </a>-->
                            <a href="" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal" role="form" onsubmit="return searchBeat();" id="formSearch">
                            <div class="form-body width90Auto">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control input-sm" placeholder="Beat Owner Name" name="textBeatPostedBy" id="textBeatPostedBy"> </div>
                                        <div class="col-md-6">
                                        <input type="text" class="form-control input-sm" placeholder="Beat Tittle" name="textBeatTitle" id="textBeatTitle"> </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="input-group select2-bootstrap-prepend">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="multi-prepend"> Interest </button>
                                            </span>
                                            <select id="multi-prepend" name="specilities[]"  class="form-control select2 hidden" multiple>
                                                <?php 
                                                        foreach($interestLists['interestlist'] as $interest){
                                                            echo '<option value="'.$interest['specilities']['id'].'" > '.$interest['specilities']['name'].'</option>';

                                                    } 
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                    <div class="col-md-6">
                                        <input placeholder="Beat posted - From Date" class="form-control datepicker" id="dateFrom">
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                    <div class="col-md-6">
                                        <input placeholder="Beat posted - To Date" class="form-control datepicker" id="dateTo">
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions center-block txt-center">
                                <button type="submit" class="btn green2">Search</button>
                                <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            <!--<i class="fa fa-cogs"></i>-->Posts List </div>
                        <div class="tools">
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                    <div class="mainAction marBottom10">
                    <div class="leftFloat">
                        <div class="actions">
                                <div class="btn-group">
                                        <select class="btn btn-md green2" aria-expanded="false" name="selectStatus" id="selectStatus" onchange="return searchBeat();">
                                            <option value=""> --Filter-- </option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                </div>
                        </div>
                    </div>
                        <div class="rightFloat">
                            <div class="actions marRight10">
                                <div class="btn-group">
                                    <a class="btn btn-sm green2 dropdown-toggle" href="<?php echo BASE_URL . 'admin/UserPost/addBeat';  ?>"  aria-expanded="false"> <i class="fa fa-plus"></i> Add Beat</a>
                                </div>
                            </div>
                            <div class="actions">
                                <div class="btn-group">
                                    <button class="btn btn-sm green2 dropdown-toggle" onclick="xyz();" type="button" class="btn btn-primary"> <i class="fa fa-external-link"></i> Export </button>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <span id="content">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content">
                                <tr>
                                    <!-- <th>&nbsp;  </th> -->
                                    <th id="title" class="DESC" onclick="sort('title',<?php echo $limit; ?>);"><a> Post Name </a></th>
                                    <th> Interests </th>
                                    <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> Posted By </a></th>
                                    <th id="date" class="DESC" onclick="sort('date',<?php echo $limit; ?>);"><a> Posted On </a></th>
                                    <th> Counts </th>
                                    <th> Status </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                        if(count($userPostData) > 0){
                            foreach ($userPostData as $key => $value) {
                                ?>
                             
                                <tr id="<?php echo "beat".$value ['UserPost']['id']; ?>">
                                    <!-- <td style="width:1%;"> <input type="checkbox" value="<?php echo $value['UserPost']['spam_confirmed']; ?>" id="<?php echo $value['UserPost']['id']; ?>" > </td> -->
                                    <td style="width:29%;"><?php echo $value['UserPost']['title']; ?></td>
                                    <td  style="width:29%;"><?php echo $value['speciality_list']; ?></td>
                                    <td style="width:12%;"><?php echo $value['UserProfile']['first_name']." ".$value['UserProfile']['last_name']; ?></td>
                                    <td style="width:15%;"><?php $datetime = explode(" ",$value['UserPost']['post_date']);
                                        $date = $datetime[0];
                                        $time = $datetime[1];
                                        echo date('d-m-Y',strtotime($date))."  ".date ('G:i:s',strtotime($time));
                                     ?></td>
                                    <td  style="width:5%;"class="txtCenter width80px"> 
                                    <a class="iconCounts tooltips" data-original-title="Up Beats">
                                        <i class="demo-icon icon2-up"></i>
                                        <span class="badge badge-success2"><?php echo count($value['beat_details']['upbeat']); ?></span>
                                    </a>
                                    
                                    <a class="iconCounts tooltips" data-original-title="Down Beats">
                                        <i class="demo-icon icon2-down"></i>
                                        <span class="badge badge-success2"><?php echo count($value['beat_details']['downbeat']); ?></span>
                                    </a> 
                                    
                                    <a class="iconCounts tooltips" data-original-title="Comments">
                                        <i class="fa fa-comment-o"></i>
                                        <span class="badge badge-success2"><?php echo count($value['UserPostComment']); ?></span>
                                    </a>
                                    
                                    <a class="iconCounts tooltips" data-original-title="Shares">
                                        <i class="fa fa-share-alt"></i>
                                        <span class="badge badge-success2"><?php
                                        if($value['share_details']=="0"){
                                            echo "0";
                                        }
                                            else{
                                         echo count($value['share_details']); }
                                         ?></span>
                                    </a>
                                    
                                    <a class="iconCounts tooltips" data-original-title="Views">
                                        <i class="fa fa-eye"></i>
                                        <span class="badge badge-success2"><?php echo count($value['BeatView']); ?></span>
                                    </a>
                                    </td>
                                    
                                    <td style="width:5%;" class="beatBtnBottom4">
                                            <?php 
                                             if($value ['UserPost']['status'] == 1){
                                            ?>
                                            <button type="button" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                            <?php }else{ ?>
                                            <button type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                                            <?php } ?>
                                            <span id="flagButton<?php echo $value ['UserPost']['id']; ?>">
                                                <?php 
                                                 if($value ['UserPost']['spam_confirmed'] == 1){
                                                ?>
                                                
                                                    <button data-original-title="Flaged" class="btn btn-circle btn-icon-only btn-info3 delBt tooltips"><i class="fa fa-flag"></i></button>
                                                </span>
                                                <?php }else{ ?>
                                                    <button type="button" data-original-title="Unflaged" class="btn btn-circle btn-icon-only btn-default2 tooltips"><i class="fa fa-flag"></i></button>
                                                    </span>
                                                   
                                                <?php } ?>
                                       
                                       <span id="featuredButton<?php echo $value ['UserPost']['id']; ?>">
                                                <?php 
                                                 if($value ['UserPost']['top_beat'] == 1){
                                                ?>
                                                
                                                    <button data-original-title="Featured" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-star"></i></button>
                                                </span>
                                                <?php }else{ ?>
                                                    <button type="button" data-original-title="Unfeatured" class="btn btn-circle btn-icon-only btn-default2 tooltips"><i class="fa fa-star"></i></button>
                                                    </span>
                                                   
                                                <?php }
                                                        if($value ['mail'] > 0)
                                                        {
                                                     ?>
                                                    <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
                                                    <?php }else{ ?>
                                                    <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="No Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
                                                    <?php } ?>
                                        
                                    </td>
                                    <td style="width:5%;" class="beatBtnBottom4">
                                    <?php if(in_array($value ['UserPost']['status'],array(0,1))){ ?>
                                        <div class="radialMenu menuButton3">
                                        <nav class="circular-menu">
                                          <div class="circle">
                                           <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Details" type="button" onclick="javascript:window.location.href='userPostDetails/<?php echo $value ['UserPost']['id'];?>'"><i class="fa fa-eye"></i></button>
                                           <button type="button" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Edit" type="button" onclick="javascript:window.location.href='editBeat/<?php echo $value ['UserPost']['id'];?>'"><i class="fa fa-pencil-square-o"></i></button>
                                           <span id="statusButton<?php echo $value ['UserPost']['id']; ?>">
                                               <?php 
                                                if($value ['UserPost']['status'] == 1){
                                               ?>
                                               <button type="button" onclick="updatePostStatus(<?php echo $value ['UserPost']['id']; ?>, 'Active');" data-original-title="Mark Inactive" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                               <?php }else{ ?>
                                               <button onclick="updatePostStatus(<?php echo $value ['UserPost']['id']; ?>, 'Inactive');" type="button" data-original-title="Mark Active" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i style="color: black;" class="fa fa-check"></i></button>
                                               <?php } ?>
                                           </span>
                                           <?php 
                                            if($value ['UserPost']['top_beat'] == 1){
                                           ?>
                                               <button onclick="unfeature(<?php echo $value ['UserPost']['id']; ?>); " class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Unfeature" type="button"><i class="fa fa-star"></i></button>
                                           </span>
                                           <?php }else {
                                               if($value ['UserPost']['status'] !=1 ){ ?>
                                               <button onclick="alert('Beat Is Inactive,Cannot Mark Feature.');" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Feature" type="button"><i class="fa fa-star"></i></button>
                                           <?php }elseif($value['UserPost']['spam_confirmed']==0){ ?>
                                               <button type="button" onclick="feature(<?php echo $value ['UserPost']['id'].','.$featuredCount[0]; ?>);" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Feature" type="button"><i class="fa fa-star"></i></button>
                                           <?php }else{ ?>
                                               <button type="button" onclick="alert('Beat Is Flagged,Cannot Mark Feature.');" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Feature" type="button"><i class="fa fa-star"></i></button>
                                               <?php }
                                           }
                                            if($value ['UserPost']['spam_confirmed'] == 1){
                                           ?>
                                           <button onclick="Unflag(<?php echo $value ['UserPost']['id'];?>);" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Unmark Flag" type="button"><i class="fa fa-flag"></i></button>
                                            <?php }elseif ($value ['UserPost']['spam_confirmed'] == 0) {
                                               if($value ['UserPost']['status'] !=1 ){ ?>
                                               <button onclick="alert('Beat Is Inactive,Cannot Mark Flagg.');" class="btn btn-circle btn-icon-only btn-info2 tooltips flagging" data-original-title="Mark Flag" type="button"><i class="fa fa-flag"></i></button>
                                           <?php }else{ ?>
                                           <button id="<?php echo $value ['UserPost']['id'];?>" class="btn btn-circle btn-icon-only btn-info2 tooltips flagging" data-toggle="modal" href='#modal-id' data-original-title="Mark Flag" type="button"><i class="fa fa-flag"></i></button>
                                           <?php }} ?>
                                           <button onclick="deleteBeat(<?php echo $value ['UserPost']['id'].','."'".$value ['UserPost']['title']."'"; ?>);" type="button" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                          </div>
                                          <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
                                        </nav>
                                        </div>
                                        <?php } ?>
                                </td>
                                </tr>
                            <?php }
                            }
                            else{
                            ?>
                            </div>
                            <tr>
                                    <td colspan="8" align="center"><font color="red">Record(s) Not  Found!</font></td>
                              </tr> 
                            <?php } ?>

                            </tbody>
                        </table>
                        <div class="row">
                        
                           <!--  <div class="col-md-5 col-sm-5">
                                <div class="dataTables_info" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 9 records</div>
                            </div> -->
                            <div class="col-md-7 col-sm-7">
                                <div class="dataTables_paginate paging_bootstrap_full_number rightFloat" id="sample_1_paginate">
                                
                                    <!-- <ul class="pagination" style="visibility: visible;">
                                        <li class="prev disabled"><a href="#" title="First"><i class="fa fa-angle-double-left"></i></a></li>
                                        <li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li><li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li><li><a href="#">5</a></li>
                                        <li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li>
                                        <li class="next"><a href="#" title="Last"><i class="fa fa-angle-double-right"></i></a></li>
                                    </ul> -->
                                </div>
                            </div>
                        </div>
                        <?php echo $this->element('pagination'); ?>
                        <span>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?> 
     <!-- Modal Box [START] -->
        <div id="modalBox"></div>
     <!-- Modal Box [END] -->
     <div class="modal fade" id="modal-id">
         <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title text-center text-danger"><b>Flag a Beat</b></h4>
                 </div>
                 <div class="modal-body">
                      <?php echo $this->Form->create('markFlag'); ?>
                     <div class="row">
                         <div class="form-group col-md-12">
                             <div class="col-md-3">
                                 <label>Reason : </label>
                             </div>
                             <div class="col-md-9">
                                 <input type="radio" name="spam_type" id="spam_type" value="1"> Contains Abusive/Offensive Content<br>
                                 <input type="radio" name="spam_type" id="spam_type" value="2"> Contains Personal Identifiable Information<br>
                                 <input type="radio" name="spam_type" id="spam_type" value="3"> Is Spam/Advert<br>
                             </div>
                         </div>
                         <div class="form-group col-md-12">
                             <div class="col-md-3">
                                 <label>Comment : </label>
                             </div>
                             <div class="col-md-9">
                                 <textarea style="margin: 0px; width: 100%; height: 191px;" id="txt_comment" name="txt_comment"></textarea>
                             </div>
                         </div>
                         <span id="flagId" class="hidden"></span>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-primary" onclick="flag();">Submit</button>
                     <button type="button" class="btn btn-default" onclick="closeModal();" data-dismiss="modal">Close</button>
                 </div>
                     <?php echo $this->Form->end(); ?>
             </div>
         </div>
     </div>
<div id="cn-overlay" class="cn-overlay"></div>
<script>
$(document).ready(function() {
    $(".datepicker").datepicker(); 
});

function sort(val,limit){
    $(".loader").fadeIn("fast");
    var a=$('#'+val).attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='DE';
    }
    else{
        var order='A';
    }
$.ajax({
             url: '<?php echo BASE_URL; ?>admin/UserPost/featuredFilter',
             type: "POST",
             data: {'sort':val,'order':order,'textBeatTitle': $('#textBeatTitle').val(), 'textBeatPostedBy': $('#textBeatPostedBy').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'limit':limit},
             success: function(data) { 
                // alert(data);
                $('#content').html(data);
                $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                if(a==b){
                $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                }
                else{
                $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                }
                $(".loader").fadeOut("fast");
            }
        });
  return false; 
}
function xyz(){
    $(".loader").fadeIn("fast");
var BeatTitle = $('#textBeatTitle').val();
var BeatPostedBy = $('#textBeatPostedBy').val();
var specilities = $('#multi-prepend').val();
var statusVal= $('#selectStatus').val();
var from = $('#dateFrom').val();
var to = $('#dateTo').val();
if(specilities==null)
{
    specilities="";
}
// alert(specilities);
window.open('<?php echo BASE_URL; ?>admin/UserPost/beatCsvDownload/?Beat=all&BeatPostedBy='+BeatPostedBy+'&BeatTitle='+BeatTitle+'&specilities='+specilities+'&statusVal='+statusVal+'&from='+from+'&to='+to, '_blank');
}        
function searchBeat(){
    $(".loader").fadeIn("fast");
 $.ajax({
         url: '<?php echo BASE_URL; ?>admin/UserPost/featuredFilter',
         type: "POST",
         data: {'textBeatTitle': $('#textBeatTitle').val(), 'textBeatPostedBy': $('#textBeatPostedBy').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val()},
 success: function(data) { 
    // alert(data);
    $('#content').html(data);
    $(".loader").fadeOut("fast");
}
    });
return false;  
}
 function chngPage(val,limit) {
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
    $(".loader").fadeIn("fast");
    $.ajax({
              url:'<?php echo BASE_URL; ?>admin/UserPost/featuredFilter',
              type: "POST",
              data: {'textBeatTitle': $('#textBeatTitle').val(), 'textBeatPostedBy': $('#textBeatPostedBy').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'j':goVal,'limit':limit,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 function chngCount(val){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
              // url:'<?php echo BASE_URL; ?>admin/UserPost/userPostLists',
              url:'<?php echo BASE_URL; ?>admin/UserPost/featuredFilter',
              type: "POST",
              data: {'textBeatTitle': $('#textBeatTitle').val(), 'textBeatPostedBy': $('#textBeatPostedBy').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'limit':val,'sort':a,'order':order},
              success: function(data) { 
                  $('#content').html(data);
                  $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                  if(a==b){
                  $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                  }
                  else{
                  $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                  }
                  $(".loader").fadeOut("fast");
              }
          });
 }

//** Delete beat
function deleteBeat(beatId,title){
var conf=confirm("Do you want to delete Post : "+title);
if(conf==true){
     $(".loader").fadeIn("fast");
$.ajax({
         url: '<?php echo BASE_URL; ?>admin/UserPost/deleteBeat',
         type: "POST",
         data: {'beatId': beatId},
         success: function(data) { 
            alert(data);
            location.reload();
        }
    });
return false; 
}
else{}
}
function abc(val,limit){
    $(".loader").fadeIn("fast");
    var a=$('tr th.active').attr('id');
    var b=$('tr th.ASC').attr('id');
    if(a==b){
        var order='A';
    }
    else{
        var order='DE';
    }
  $.ajax({
           url:'<?php echo BASE_URL; ?>admin/UserPost/featuredFilter',
           type: "POST",
           data: {'textBeatTitle': $('#textBeatTitle').val(), 'textBeatPostedBy': $('#textBeatPostedBy').val(), 'specilities': $('#multi-prepend').val(), 'statusVal': $('#selectStatus').val(),'from':$('#dateFrom').val(),'to':$('#dateTo').val(),'j':val,'limit':limit,'sort':a,'order':order},
           success: function(data) { 
               $('#content').html(data);
              $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
              if(a==b){
              $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
              }
              else{
              $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
              }
              $(".loader").fadeOut("fast");
           }
       });
}

$("#menu>ul>li").eq(2).addClass('active open');
$(".sub-menu>li").eq(5).addClass('active open');
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script> 