<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    img#editimg0,img#editimg1,img#editimg2,img#editimg3,img#editimg4,img#editimg5,img#editimg6,img#doc0,img#doc1,img#doc2,img#doc3{
        border:1px solid black;
        width:100px!important;
        height:100px!important;
    }
    a#img0,a#img1,a#img2,a#img3,a#img4,a#img5,a#img6,a#doc0,a#doc1,a#doc2,a#doc3{
        position:relative;
        left:20%;
    }
    #main{
      position: fixed;
      z-index: 9994;
    }
    div#editimg0,div#editimg1,div#editimg2,div#editimg3,div#editimg4,div#editimg5{
      position: relative;
      left:25%;
      top:10%;
    }
    .blurr{
        margin-top: -120px;
        margin-left: -80px;
        height: 120%;
        width:150%;
        position: fixed;
        background: grey;
        opacity: 0.8;
        z-index: 9993;
    }
    .stop-scrolling {
  height: 100%;
  overflow: hidden;
}
#shtBtn{
    border-radius: 20px!important;
    float: right;
    margin-right: 21%;
    margin-top: -1%;
}
</style>
<div class="blurr hidden"></div>
<div id="main" class="col-md-12">
<button class="btn btn-danger hidden" id="shtBtn" onclick="rmvEditor();">X</button>
  <div id="editimg0" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg1" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg2" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg3" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg4" class="hidden" style="width: 640px; height: 480px;"></div>
  <div id="editimg5" class="hidden" style="width: 640px; height: 480px;"></div>
</div>

<?php //pr($userPostdata);exit(); ?>
<!-- BEGIN CONTENT -->
<?php 
echo $this->Html->css(array('Admin.PhotoEditorReactUI'));
echo $this->Html->script(array(
// 'Admin.ckeditor/ckeditor.js',
'Admin.PhotoEditorSDK',
'Admin.PhotoEditorReactUI')); ?>

<div id="content" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Beat</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'admin/UserPost/userPostLists';?>">All Posts</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span class="active">Post Details</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            Add Beat </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('addBeat',array('class'=>'form-horizontal','type'=>'file','onsubmit'=>'return hashCount();')); ?>
                        <!-- <form class="form-horizontal" role="form" onsubmit="return searchBeat();" id="formSearch"> -->
                        <div class="form-body width90Auto">
                            <div class="form-group">
                                <div class="col-md-12">
                                <?php echo $this->Form->text('title',array('class'=>'form-control input-sm','placeholder'=>'Beat Title','id'=>'textTitle')); ?>
                                    <!-- <input type="text" class="form-control input-sm" placeholder="Beat Title" name="textBeatPostedBy" id="textBeatPostedBy"> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group select2-bootstrap-prepend">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="multi-prepend"> Interest </button>
                                        </span>
                                        <?php 
                                                foreach($interestLists['interestlist'] as $interest){
                                                    $options[$interest['specilities']['id']]=$interest['specilities']['name'];
                                                }
                                            ?>
                                        <?php echo $this->Form->select('interest',$options,array('id'=>'multi-prepend','class'=>'form-control select2 hidden','multiple'=>true)); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <?php echo $this->Form->textarea('description',array('class'=>'form-control input-sm','placeholder'=>'Beat Description','id'=>'textDescrip')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <span>Add Videos: (supported platform: YouTube )</span>
                                    <?php echo $this->Form->input('video_path',array('id'=>'videoPath','readonly'=>true,'class'=>'form-control','label'=>false,'placeholder'=>'Video Path','required'=>false));?>
                                    <a  class="patientUserDetail" data-id="1" data-toggle="modal" href="#myModal">
                                      Select the video path
                                    </a>
                                </div>
                            </div>
                            <div id="imageContent" class="form-group">
                                <div class="col-md-12">
                                    <span>Add Images: </span>
                                        / 
                                    <a onclick="resetImgAll();">Reset</a><br>
                                    <div class="row">
                                        <div id="maineditimg0" class="col-md-2">
                                        <span class="btn btn-lg success btn-file" onclick="sh(0);">
                                            <span class="fileinput-exists">
                                                <img id="editimg0" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                        </span>
                                        <br><a onclick="rmvImg('0');" id="img0" class="btn default"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                        <div id="maineditimg1" class="col-md-2 hidden">
                                        <span class="btn btn-lg success btn-file" onclick="sh(1);">
                                            <span class="fileinput-exists">
                                                <img id="editimg1" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                        </span>
                                        <br><a data-dismiss="fileinput" onclick="rmvImg('1');" id="img1" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                        <div id="maineditimg2" class="col-md-2 hidden">
                                        <span class="btn btn-lg success btn-file" onclick="sh(2);">
                                            <span class="fileinput-exists">
                                                <img id="editimg2" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                        </span>
                                        <br><a data-dismiss="fileinput" onclick="rmvImg('2');" id="img2" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                        <div id="maineditimg3" class="col-md-2 hidden">
                                        <span class="btn btn-lg success btn-file" onclick="sh(3);">
                                            <span class="fileinput-exists">
                                                <img id="editimg3" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                        </span>
                                        <br><a data-dismiss="fileinput" onclick="rmvImg('3');" id="img3" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                        <div id="maineditimg4" class="col-md-2 hidden">
                                        <span class="btn btn-lg success btn-file" onclick="sh(4);">
                                            <span class="fileinput-exists">
                                                <img id="editimg4" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                        </span>
                                        <br><a data-dismiss="fileinput" onclick="rmvImg('4');" id="img4" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                        <div id="maineditimg5" class="col-md-2 hidden">
                                        <span class="btn btn-lg success btn-file" onclick="sh(5);">
                                            <span class="fileinput-exists">
                                                <img id="editimg5" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                        </span>
                                        <br><a data-dismiss="fileinput" onclick="rmvImg('5');" id="img5" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                            <div id="docContent" class="form-group">
                                <div class="col-md-12">
                                <span>Add Files: (supported formats: doc, xls, ppt, pdf, rtf)</span>
                                        / 
                                    <a onclick="rstDoc();">Reset</a><br>
                                    <div class="row">
                                        <div id="doc0" class="col-md-2">
                                        <span class="btn btn-lg success btn-file">
                                            <span class="fileinput-exists">
                                                <img id="doc0" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                            <input type="hidden" value="" name="">
                                            <?php echo $this->Form->file('docs.',array('id'=>'doc0','class'=>'form-control','onchange'=>'chckDoc(this,value,0);')); ?>
                                        </span>
                                        <br><a onclick="rmvDoc('0');" id="doc0" class="btn default"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                        <div id="doc1" class="col-md-2 hidden">
                                        <span class="btn btn-lg success btn-file">
                                            <span class="fileinput-exists">
                                                <img id="doc1" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                            <input type="hidden" value="" name="">
                                            <?php echo $this->Form->file('docs.',array('id'=>'doc1','class'=>'form-control','onchange'=>'chckDoc(this,value,1);')); ?>
                                        </span>
                                        <br><a data-dismiss="fileinput" onclick="rmvDoc('1');" id="doc1" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                        <div id="doc2" class="col-md-2 hidden">
                                        <span class="btn btn-lg success btn-file">
                                            <span class="fileinput-exists">
                                                <img id="doc2" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                            <input type="hidden" value="" name="">
                                            <?php echo $this->Form->file('docs.',array('id'=>'doc2','class'=>'form-control','onchange'=>'chckDoc(this,value,2);')); ?>
                                        </span>
                                        <br><a data-dismiss="fileinput" onclick="rmvDoc('2');" id="doc2" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                        <div id="doc3" class="col-md-2 hidden">
                                        <span class="btn btn-lg success btn-file">
                                            <span class="fileinput-exists">
                                                <img id="doc3" src="<?php echo BASE_URL.'admin/img/plus.gif'; ?>">
                                            </span>
                                            <input type="hidden" value="" name="">
                                            <?php echo $this->Form->file('docs.',array('id'=>'doc3','class'=>'form-control','onchange'=>'chckDoc(this,value,3);')); ?>
                                        </span>
                                        <br><a data-dismiss="fileinput" onclick="rmvDoc('3');" id="doc3" class="btn default imageinput-exists" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i>Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->text('hashTags',array('type'=>'text','hidden'=>'true','id'=>'textHash')); ?>
                        <div class="form-actions center-block txt-center">
                            <a href="<?php echo BASE_URL . 'admin/UserPost/userPostLists';?>" type="button" class="btn default">Cancel</a>
                            <button type="submit" class="btn green2">Post</button>
                            <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Youtube path </h4>
      </div>
      <div class="modal-body">
        <table>
        <tr><td><input type="text" id="youtubePath"  name ="data[addBeat][youtubePath]" placeholder="search keyword" /></td><td>&nbsp;<button type="button" class="btn btn-default youtubePath" >Search youtube videos</button></td></tr>
        <tr><td colspan='2'>
      <div id="loader"></div>
      <div id="showData"><div></td></tr>

        </table>
      </div><div class="clearfix"></div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php echo $this->Form->textarea('img0',array('class'=>'hidden','value'=>'','id'=>'txteditimg0'));?>
<?php echo $this->Form->textarea('img1',array('class'=>'hidden','value'=>'','id'=>'txteditimg1'));?>
<?php echo $this->Form->textarea('img2',array('class'=>'hidden','value'=>'','id'=>'txteditimg2'));?>
<?php echo $this->Form->textarea('img3',array('class'=>'hidden','value'=>'','id'=>'txteditimg3'));?>
<?php echo $this->Form->textarea('img4',array('class'=>'hidden','value'=>'','id'=>'txteditimg4'));?>
<?php echo $this->Form->textarea('img5',array('class'=>'hidden','value'=>'','id'=>'txteditimg5'));?>
<script type="text/javascript">
function youtubePathVideo(path){
     document.getElementById("videoPath").value = 'http://www.youtube.com/watch?v='+path;
      $('.close').click();  
    }
    $("#menu>ul>li").eq(2).addClass('active open');
</script>