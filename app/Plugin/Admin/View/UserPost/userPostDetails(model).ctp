<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="closeModal();">&times;</button>
          <h4 class="modal-title">Beat Details</h4>
        </div>
        <div class="modal-body">
              <table border="0" align="center" width="100%">
                  <tr>
                    <!-- Column one -->
                    <td width="50%" style = "border-right : 2px Solid #000; ">
                        <table border="0" align="center" width="100%" cellspacing="4" cellpadding="4">
                            <tr>
                                <td><strong>Post Date</strong></td>
                                <td>&nbsp;</td>
                                <td> <?php echo date('d-m-Y', strtotime($userPostdata[0]['UserPost']['post_date']));?></td>
                            </tr>
                            <tr>
                                <td><strong>Title</strong></td>
                                <td>&nbsp;</td>
                                <td> <?php echo $userPostdata[0]['UserPost']['title'];?></td>
                            </tr>
                            <tr>
                                <td><strong>Specialities</strong></td>
                                <td>&nbsp;</td>
                                <td> <?php echo $userPostdata[0]['specialitystring'];?></td>
                            </tr>
                            <tr>
                                <td><strong>Owner Name</strong></td>
                                <td>&nbsp;</td>
                                <td> <?php echo $userPostdata['User']['name'];?></td>
                            </tr>
                            <tr>
                                <td><strong>Attributes</strong></td>
                                <td>&nbsp;</td>
                                <td> &nbsp;</td>
                            </tr>
                             <tr>
                                  <td colspan = "3">
                                      <table border="0" align="center" width="100%" cellspacing="4" cellpadding="4">
                                      <?php
                                foreach( $userPostdata[0]['UserPostAttribute'] as $uAttr ){
                            ?>
                              <tr>
                              <td  align="left" valign="top"><?php echo ucfirst($uAttr['attribute_type']); ?></td>
                              <td  align="left" valign="top">
                                <?php if( $uAttr['attribute_type'] == "text" ){ echo $uAttr['content'];  ?>
                                <?php }else if( $uAttr['attribute_type']=="image" ){  
                                      $img_path = AMAZON_PATH . 'posts/'.$uAttr['user_post_id'].'/image/'.$uAttr['content'];
                                ?>
                                <img src="<?php echo $img_path; ?>" width="100px" height="100px" />
                                <?php } else if($uAttr['attribute_type'] == "video") { ?>
                                  <a href = "<?php echo $uAttr['content']?>" target = "_blank"><?php echo $uAttr['content']?></a>
                                <?php } else if(in_array($uAttr['attribute_type'], array('ppt','pdf','pptx','doc','docx','xls','xlsx'))) { ?>
                                  <a href = "<?php echo AMAZON_PATH . 'posts/'.$uAttr['user_post_id'].'/doc/'.$uAttr['content']?>" target = "_blank"><?php echo $uAttr['content']?></a>
                                <?php } ?>
                              </td>
                            
                            </tr>
              
                            <?php
                              }
                            ?>
                          </table>
                                  </td>
                            </tr>
                            <tr>
                                 <td><strong>Status</strong></td>
                                 <td>&nbsp;</td>
                                 <td> <?php 
                                            echo ($userPostdata[0]['UserPost']['status'] == 1 ? 'Active':'Inactive'); 
                                            ?>
                                  </td>
                            </tr>
                          
                        </table>
                    </td>
                    <!-- Column two -->
                    <td width="50%" valign="top">
                            <!-- Upbeat Section -->
                            <table border="0" align="center" width="100%" cellspacing="4" cellpadding="4">
                                <tr>
                                    <td valign="top" width="30%"><strong>Up Beats</strong> (<?php echo count($userPostdata[0]['beat_details']['upbeat']); ?>)</td>
                                    <td width="10%">&nbsp;</td>
                                    <td width="60%">
                                        <?php if (count($userPostdata[0]['beat_details']['upbeat']) > 0){ ?>
                                              <table border="0" align="center" width="100%">
                                                  <?php 
                                                    foreach($userPostdata[0]['beat_details']['upbeat'] as $upbeat){
                                                  ?>
                                                      <tr>
                                                        <td>
                                                            <?php echo $upbeat['username']?>
                                                        </td>
                                                      </tr>
                                                  <?php
                                                    }
                                                  ?>
                                              </table>
                                        <?php
                                        }?>
                                    </td>
                                </tr>
                            </table>

                            <!-- Down beat Section -->

                            <table border="0" align="center" width="100%" cellspacing="4" cellpadding="4">
                                <tr>
                                    <td valign="top" width="30%"><strong>Down Beats</strong> (<?php echo count($userPostdata[0]['beat_details']['downbeat']); ?>)</td>
                                    <td width="10%">&nbsp;</td>
                                    <td width="60%">
                                        <?php if (count($userPostdata[0]['beat_details']['downbeat']) > 0){ ?>
                                              <table border="0" align="center" width="100%">
                                                  <?php 
                                                    foreach($userPostdata[0]['beat_details']['downbeat'] as $downbeat){
                                                  ?>
                                                      <tr>
                                                        <td>
                                                            <?php echo $downbeat['username']?>
                                                        </td>
                                                      </tr>
                                                  <?php
                                                    }
                                                  ?>
                                              </table>
                                        <?php
                                        }?>
                                    </td>
                                </tr>
                            </table>

                              <!-- User Comments Section -->

                            <table border="0" align="center" width="100%" cellspacing="4" cellpadding="4">
                                <tr>
                                    <td valign="top" width="30%"><strong>Comments</strong> (<?php echo count($userPostdata[0]['comment_details']); ?>)</td>
                                    <td width="10%">&nbsp;</td>
                                    <td width="60%">
                                        <?php if (count($userPostdata[0]['comment_details']) > 0){ ?>
                                              <table border="0" align="center" width="100%">
                                                  <?php 
                                                    foreach($userPostdata[0]['comment_details'] as $comment){
                                                  ?>
                                                      <tr>
                                                        <td>
                                                            <?php echo $comment['user_name']?>
                                                        </td>
                                                        <td>
                                                            <?php echo $comment['content']?>
                                                        </td>
                                                      </tr>
                                                  <?php
                                                    }
                                                  ?>
                                              </table>
                                        <?php
                                        }?>
                                    </td>
                                </tr>
                            </table>

                            <!-- User Share Section -->

                            <table border="0" align="center" width="100%" cellspacing="4" cellpadding="4">
                                <tr>
                                    <td valign="top" width="30%"><strong>Share</strong> (<?php echo count($userPostdata[0]['share_details']); ?>)</td>
                                    <td width="10%">&nbsp;</td>
                                    <td width="60%">
                                        <?php if (count($userPostdata[0]['share_details']) > 0){ ?>
                                              <table border="0" align="center" width="100%">
                                                  <?php 
                                                    foreach($userPostdata[0]['share_details'] as $share){
                                                  ?>
                                                      <tr>
                                                        <td>
                                                            <?php echo $share['share_by']?>
                                                        </td>
                                                      </tr>
                                                  <?php
                                                    }
                                                  ?>
                                              </table>
                                        <?php
                                        }?>
                                    </td>
                                </tr>
                            </table>
                    </td>
                  </tr>
              </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="closeModal();">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

