<?php
class HomeController extends AdminAppController {
    public $uses = array('User', 'UserPost','TempRegistration');
    /*
    On: 
    I/P:
    O/P:
    Desc:
    */
    
    public function dashboard(){ 
      	//** All deleted users 
         $totalUsers = $this->User->find('count', 
         				array(
                            'conditions'=>array('User.status'=> array(0,1)),
                            'joins'=>array(
                                  array(
                                    'table' => 'user_employments',
                                    'alias' => 'UserEmployment',
                                    'type' => 'left',
                                    'conditions'=> array('User.id = UserEmployment.user_id')
                                )
                            ),
                            'group'=> array('User.id'),
                        )
         				);
         $this->set('totalUsers', $totalUsers);
         	//** All Approved and active users 
         $registeredUsers = $this->User->find('count', 
         				array('conditions'=>
         					array('User.status'=>array(0,1) , 'User.approved'=> 1 ),
                            'joins'=>array(
                                  array(
                                    'table' => 'user_employments',
                                    'alias' => 'UserEmployment',
                                    'type' => 'left',
                                    'conditions'=> array('User.id = UserEmployment.user_id')
                                )
                            ),
                            'group'=> array('User.id'),
                        )
         				);
         $this->set('registeredUsers', $registeredUsers);
         //** All active but unapproved users 
         $pendingUsers = $this->User->find('count', 
                        array('conditions'=>
                            array('User.status'=> array(0,1), 'User.approved'=> 0 ),
                            'joins'=>array(
                                  array(
                                    'table' => 'user_employments',
                                    'alias' => 'UserEmployment',
                                    'type' => 'left',
                                    'conditions'=> array('User.id = UserEmployment.user_id')
                                )
                            ),
                            'group'=> array('User.id'),
                        )
                        );
         $this->set('pendingUsers', $pendingUsers);
         //** All Incomplete users 
         $incompleteUsers = $this->TempRegistration->find('count', 
         				array(
                            'conditions'=>array(
                                            'TempRegistration.status'=>array(0,1),
                                            'User.status'=>NULL
                                            ),
                            'joins'=>array(
                                array(
                                  'table' => 'users',
                                  'alias' => 'User',
                                  'type' => 'LEFT',
                                  'conditions'=> array('User.email = TempRegistration.email')
                              )
                            ),
                            'group'=> array('TempRegistration.email'),
                        )
         				);
         $this->set('incompleteUsers', $incompleteUsers);
         //** All Beats
         $totalBeats = $this->UserPost->find('count', 
                array('conditions'=>
                  array('UserPost.status'=>array(0,1)))
                );
         $this->set('totalBeats', $totalBeats);
         //** All Reported Beats
         $reportedBeats = $this->UserPost->find('count', 
                array('conditions'=>
                  array('UserPost.spam_confirmed'=> 1 ))
                );
         $this->set('reportedBeats', $reportedBeats);
    }

    /*
    On: 
    I/P:
    O/P:
    Desc:
    */
    public function dashboardFilterData(){ 
        $this->layout = null;
        $startDate = date('Y-m-d H:i:s', strtotime($this->data['start']));
        // $startDate = $this->data['start'];

        $endDate = date('Y-m-d H:i:s', strtotime($this->data['end']));
        // $endDate = $this->data['end'];
         $totalUsers = $this->User->find('count', 
                        array('conditions'=>
                            array(
                                'User.status'=> array(0,1),
                                'User.registration_date <= ' => $endDate,
                                'User.registration_date >= ' => $startDate
                                )
                            )
                        );
         $dataList['totalUsers'] = $totalUsers;
            //** All Approved and active users 
         $registeredUsers = $this->User->find('count', 
                        array('conditions'=>
                            array(
                                'User.status'=> 1 , 'User.approved'=> 1,
                                'User.registration_date <= ' => $endDate,
                                'User.registration_date >= ' => $startDate 
                                )
                            )
                        );
         $dataList['registeredUsers'] = $registeredUsers;
         //** All active but unapproved users 
         $pendingUsers = $this->User->find('count', 
                        array('conditions'=>
                            array(
                                'User.status'=> 1, 'User.approved'=> 0,
                                'User.registration_date <= ' => $endDate,
                                'User.registration_date >= ' => $startDate 
                                )
                            )
                        );
         $dataList['pendingUsers'] = $pendingUsers;
         //** All Incomplete users 
         $incompleteUsers = $this->TempRegistration->find('count', 
                        array(
                            'conditions'=>array(
                                            'TempRegistration.status'=>array(0,1),
                                            'User.status'=>NULL,
                                            'TempRegistration.created <= ' => $endDate,
                                            'TempRegistration.created >= ' => $startDate 
                                            ),
                            'joins'=>array(
                                array(
                                  'table' => 'users',
                                  'alias' => 'User',
                                  'type' => 'LEFT',
                                  'conditions'=> array('User.email = TempRegistration.email')
                              )
                            ),
                            'group'=> array('TempRegistration.email'),
                        )
                        );
         $dataList['incompleteUsers'] = $incompleteUsers;
         //** All Beats
         $totalBeats = $this->UserPost->find('count', 
                array('conditions'=>
                  array(
                    'UserPost.status'=> 1,
                    'UserPost.post_date <= ' => $endDate,
                    'UserPost.post_date >= ' => $startDate 
                    )
                  )
                );
         $dataList['totalBeats'] = $totalBeats;
         //** All Reported Beats
         $reportedBeats = $this->UserPost->find('count', 
                array('conditions'=>
                  array(
                    'UserPost.spam_confirmed'=> 1,
                    'UserPost.post_date <= ' => $endDate,
                    'UserPost.post_date >= ' => $startDate
                    )
                  )
                );
         $dataList['reportedBeats'] = $reportedBeats; 
         $this->set('dataList', $dataList);
             $this->render('/Elements/dashboard_filter_data');
        //exit;
    }
}