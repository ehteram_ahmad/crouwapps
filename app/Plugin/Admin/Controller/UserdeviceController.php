<?php
class UserdeviceController extends AdminAppController {
    public $uses = array('Admin.UserDeviceInfo','Admin.User');
    public $components = array('RequestHandler','Paginator','Session', 'Common', 'Image');
    public $helpers = array('Js','Html', 'Paginator');

    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
    );

    /*
    On: 11-10-2017
    I/P:
    O/P: 
    Desc: Get User Device Info Detail
    */

    public function userDeviceInfoList()
    {
        
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit= ADMIN_PAGINATION;
        }
        $fields = array("UserDeviceInfo.device_info,User.email");

        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'left',
                      'conditions'=> array('UserDeviceInfo.user_id = User.id')
                  ),
                ),
                'fields'=> $fields,
                'order'=> 'UserDeviceInfo.id DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        $tCount=$this->UserDeviceInfo->find('count',array('fields'=>$fields));

        $this->Paginator->settings = $options;
        $data = $this->Paginator->paginate('UserDeviceInfo');
        $this->set(array("userDeviceInfoData"=>$data,"limit"=>$limit,'tCount'=>$tCount));
    }

    /*
    On: 11-10-2017
    I/P:
    O/P: 
    Desc: get filtered data userDevice
    */

    public function userDeviceAllFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='email'){
              $sort="User.email $order";
            }
            else{
            $sort="UserDeviceInfo.id DESC $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='UserDeviceInfo.id DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=ADMIN_PAGINATION;
        }
            $sortPost=$this->data;
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $userEmail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $userEmail=array('LOWER(User.email) LIKE'=>strtolower('%'.$sortPost['textEmail'].'%'));
            }
            if(!isset($sortPost['textDeviceId']) || $sortPost['textDeviceId']==""){
                $deviceId=array();
            }
            else{
                $sortPost['textDeviceId']=trim($sortPost['textDeviceId']);
                $deviceId=array("UserDeviceInfo.device_info RLIKE"=> '"device_id":"[[:<:]]'.$sortPost['textDeviceId'].'[[:>:]]"');
            }
            if(!isset($sortPost['textDeviceModel']) || $sortPost['textDeviceModel']==""){
                $deviceModel=array();
            }
            else{
                $sortPost['textDeviceModel']=trim($sortPost['textDeviceModel']);
                $deviceModel=array("UserDeviceInfo.device_info RLIKE"=> '"model":"[[:<:]]'.$sortPost['textDeviceModel'].'[[:>:]]"');
            }
            if(!isset($sortPost['textManufacturer']) || $sortPost['textManufacturer']==""){
                $manufacturer=array();
            }
            else{
                $sortPost['textManufacturer']=trim($sortPost['textManufacturer']);
                $manufacturer=array("UserDeviceInfo.device_info RLIKE"=> '"manufacturer":"[[:<:]]'.$sortPost['textManufacturer'].'[[:>:]]"');
            }
            if(!isset($sortPost['textApiLevel']) || $sortPost['textApiLevel']==""){
                $apiLevel=array();
            }
            else{
                $sortPost['textApiLevel']=trim($sortPost['textApiLevel']);
                $apiLevel=array("UserDeviceInfo.device_info RLIKE"=> '"api_level":"[[:<:]]'.$sortPost['textApiLevel'].'[[:>:]]"');
            }
            if(!isset($sortPost['textOsVersion']) || $sortPost['textOsVersion']==""){
                $osVersion=array();
            }
            else{
                $sortPost['textOsVersion']=trim($sortPost['textOsVersion']);
                $osVersion=array("UserDeviceInfo.device_info RLIKE"=> '"os_version":"[[:<:]]'.$sortPost['textOsVersion'].'[[:>:]]"');
            }
            if(!isset($sortPost['textVersion']) || $sortPost['textVersion']==""){
                $version=array();
            }
            else{
                $sortPost['textVersion']=trim($sortPost['textVersion']);
                $version=array("UserDeviceInfo.device_info RLIKE"=> '"version":"[[:<:]]'.$sortPost['textVersion'].'[[:>:]]"');
            }
            if(!isset($sortPost['textDeviceType']) || $sortPost['textDeviceType']==""){
                $deviceType=array();
            }
            else{
                $sortPost['textDeviceType']=trim($sortPost['textDeviceType']);
                $deviceType=array("UserDeviceInfo.device_info RLIKE"=> '"device_type":"[[:<:]]'.$sortPost['device_type'].'[[:>:]]"');
            }

             $conditions= array_merge($userEmail,$deviceId,$deviceModel,$manufacturer,$apiLevel,$osVersion,$version,$deviceType);
             $fields = array("UserDeviceInfo.device_info,User.email");
            $options  = 
            array(
                    'joins'=>array(
                        array(
                          'table' => 'users',
                          'alias' => 'User',
                          'type' => 'left',
                          'conditions'=> array('UserDeviceInfo.user_id = User.id')
                      ),
                    ),
                    'fields'=> $fields,
                    'conditions'=> $conditions,
                    'group'=> array('UserDeviceInfo.id'),
                    'order'=> $sort,
                    'limit'=> $limit,
                    'page'=>$j
                    );
            $tCount=$this->UserDeviceInfo->find('count',array('fields'=>$fields));

        $this->Paginator->settings = $options;
        $data = $this->Paginator->paginate('UserDeviceInfo');
        $this->set(array('userDeviceInfoData'=>$data,'limit'=>$limit,'tCount'=>count($data)));
        //$this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        $this->render('/Elements/user_device_filtered_data');

    }

}