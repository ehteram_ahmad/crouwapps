<?php
class TransactionController extends AdminAppController {

    public $components = array('Cookie','Session','Common','Paginator','Admin.AdminEmail');
    public $helpers = array('Html','Js','Form','Session','Time');
    public $uses = array('Admin.User','Admin.ScreenShotTransaction','Admin.UserLoginTransaction','Admin.UserDeviceInfo','Admin.UserProfile');

    /*
    ------------------------------------------------------------------------------------------
    On: 09-10-2018
    I/P:
    O/P:
    Desc:Screen Shot Transaction Listing
    ------------------------------------------------------------------------------------------
    */

    public function screenShotTransactionListing(){
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        
        //** Get User Data
        $fields = array("ScreenShotTransaction.dialog_id,ScreenShotTransaction.dialog_name,ScreenShotTransaction.device_id,ScreenShotTransaction.created,User.email");
        $options  = 
        array(
            'joins'=>array(
                array(
                  'table' => 'users',
                  'alias' => 'User',
                  'type' => 'left',
                  'conditions'=> array('ScreenShotTransaction.user_id = User.id')
              ),
            ),
            'fields'=> $fields,
            'order'=> 'ScreenShotTransaction.id DESC', 
            'limit'=> $limit,
            'page'=>$j
            );
    // pr($conditions); 
            $tCount=$this->ScreenShotTransaction->find('count');
            $this->Paginator->settings = $options;
            $screenShottransactionData = $this->Paginator->paginate('ScreenShotTransaction');

        $this->set(array("screenShotTransactionData"=>$screenShottransactionData,"limit"=>$limit,'tCount'=>$tCount));
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 13-11-2018
    I/P:
    O/P:
    Desc: User login transaction(Ex. Login/Logout time, Device info etc.)
    ------------------------------------------------------------------------------------------
    */

    public function userLoginTransaction()
    {
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        
        //** Get User Data
        $fields = array("UserLoginTransaction.login_status,UserLoginTransaction.login_date,UserLoginTransaction.application_type,UserLoginTransaction.device_info,User.email,UserProfile.first_name,UserProfile.last_name");
        $options  = 
        array(
            'joins'=>array(
                array(
                  'table' => 'users',
                  'alias' => 'User',
                  'type' => 'left',
                  'conditions'=> array('UserLoginTransaction.user_id = User.id')
              ),
                array(
                  'table' => 'user_profiles',
                  'alias' => 'UserProfile',
                  'type' => 'left',
                  'conditions'=> array('UserLoginTransaction.user_id = UserProfile.id')
              ),
            ),
            'fields'=> $fields,
            'order'=> 'UserLoginTransaction.id DESC', 
            'limit'=> $limit,
            'page'=>$j
            );
    // pr($conditions); 
            $tCount=$this->UserLoginTransaction->find('count');
            $this->Paginator->settings = $options;
            $loginTransactionData = $this->Paginator->paginate('UserLoginTransaction');

        $this->set(array("loginTransactionData"=>$loginTransactionData,"limit"=>$limit,'tCount'=>$tCount));
    }
}