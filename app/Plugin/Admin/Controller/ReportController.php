<?php
class ReportController extends AdminAppController {
    public $uses = array('User', 'Country', 'UserColleague', 'AppDownloadLog');
    
    /*
    ------------------------------------------
    On: 
    I/P:
    O/P:
    Desc:
    ------------------------------------------
    */
    public function getUserColleagueCount(){
        $userDetails = $this->User->find("all", 
                                    array("conditions"=> 
                                        array("User.status"=> 1, "User.approved"=> 1)
                                        ),
                                    array("fields"=>
                                            "User.id",
                                            "UserProfile.first_name",
                                            "UserProfile.last_name",
                                            "UserProfile.country_id"
                                        )
                                    );
        $tableHeader = "<table border=1 align='center'>";
        $tableHeader .= "<tr><td><strong>Name</strong></td><td><strong>Country Name</strong></td><td><strong>Colleague Count</strong></td></tr>";
        
        echo $tableHeader;
        foreach($userDetails as $ud){
            $countryData = array(); $userCountry = ""; $colleagueCount = 0;
            if( !empty($ud['UserProfile']['country_id']) ){
                    $countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
                    $userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';
            }
            //$colleagueCount = $this->UserColleague->find("count", array("conditions"=> array("OR"=> array( array("user_id"=> $ud['User']['id']), array("colleague_user_id"=> $ud['User']['id'])), "status"=> 1))); 
            //** Count colleagues[START]
                App::import('model','UserColleague');
                $UserColleague = new UserColleague();
                $conditions = ' AND User.status =  1 AND  User.approved = 1';
                //** Colleague list invited by me
                    $myColleagueQuer = "SELECT count(*)  AS totCnt 
                    FROM user_colleagues UserColleague 
                    INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (1) AND UserColleague.user_id = " . $ud['User']['id'] . $conditions;
                    $myColleague = $UserColleague->query( $myColleagueQuer );
                    //** Colleague List Invited to me
                    $meColleagueQuer = "SELECT count(*)  AS totCnt 
                    FROM user_colleagues UserColleague 
                    INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (1) AND UserColleague.colleague_user_id = " . $ud['User']['id'] . $conditions;
                    $meColleague = $UserColleague->query( $meColleagueQuer );
                    $colleagueCount = $myColleague[0][0]['totCnt'] + $meColleague[0][0]['totCnt'];
            //** Count colleagues[END]
            $userName = $ud['UserProfile']['first_name'] ." " . $ud['UserProfile']['last_name'];
            
            $tableData = "<tr>";
            $tableData .= "<td>" . $userName . "</td>";
            $tableData .= "<td>" . $userCountry . "</td>";
            $tableData .= "<td>" . $colleagueCount . "</td>";
            $tableData .= "</tr>";
            echo $tableData;
        }
        echo "</table>";
        exit;
    }

    /*
    ------------------------------------------
    On: 10-08-2016
    I/P:
    O/P:
    Desc: Colleague List user wise
    ------------------------------------------
    */
    public function getUserColleagueList(){
        $userDetails = $this->User->find("all", 
                                    array("conditions"=> 
                                        array("User.status"=> 1, "User.approved"=> 1)
                                        ),
                                    array("fields"=>
                                            "User.id",
                                            "User.email",
                                            "UserProfile.first_name",
                                            "UserProfile.last_name",
                                            "UserProfile.country_id"
                                        )
                                    );
        $tableHeader = "<table border=1 align='center'>";
        $tableHeader .= "<tr><td><strong>Email</strong></td><td><strong>Name</strong></td><td><strong>Country Name</strong></td><td><strong>Colleague Count</strong></td></tr>";
        
        echo $tableHeader;
        foreach($userDetails as $ud){
            $countryData = array(); $userCountry = ""; $colleagueCount = 0;
            if( !empty($ud['UserProfile']['country_id']) ){
                    $countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
                    $userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';
            }
            //$colleagueCount = $this->UserColleague->find("count", array("conditions"=> array("OR"=> array( array("user_id"=> $ud['User']['id']), array("colleague_user_id"=> $ud['User']['id'])), "status"=> 1))); 
            //** Count colleagues[START]
                App::import('model','UserColleague');
                $UserColleague = new UserColleague();
                $conditions = ' AND User.status =  1 AND  User.approved = 1';
                //** Colleague list invited by me
                    $myColleagueQuer = "SELECT count(*)  AS totCnt 
                    FROM user_colleagues UserColleague 
                    INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (1) AND UserColleague.user_id = " . $ud['User']['id'] . $conditions;
                    $myColleague = $UserColleague->query( $myColleagueQuer );
                    //** Colleague List Invited to me
                    $meColleagueQuer = "SELECT count(*)  AS totCnt 
                    FROM user_colleagues UserColleague 
                    INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (1) AND UserColleague.colleague_user_id = " . $ud['User']['id'] . $conditions;
                    $meColleague = $UserColleague->query( $meColleagueQuer );
                    $colleagueCount = $myColleague[0][0]['totCnt'] + $meColleague[0][0]['totCnt'];
            //** Count colleagues[END]
            //if($colleagueCount > 0){
                $userName = $ud['UserProfile']['first_name'] ." " . $ud['UserProfile']['last_name'];
                $colleagueRecordUrl = BASE_URL . 'admin/Report/userColleagueDetail/' . $ud['User']['id']. '/'. $ud['UserProfile']['country_id'];
                $tableData = "<tr>";
                $tableData .= "<td>" . $ud['User']['email'] . "</td>";
                $tableData .= "<td>" . $userName . "</td>";
                $tableData .= "<td>" . $userCountry . "</td>";
                if($colleagueCount > 0){
                    $tableData .= "<td><a href='". $colleagueRecordUrl ."' target='_blank'>" . $colleagueCount . "</a></td>";
                }else{
                    $tableData .= "<td>" . $colleagueCount . "</td>";
                }
                $tableData .= "</tr>";
            //}
            
             //echo "<pre>";print_r($myColleagueData);  echo "<pre>";print_r($meColleagueData);    
            echo $tableData;
        }
        echo "</table>";
        exit;
    }

    /*

    */
    public function userColleagueDetail($userId = NULL, $countryId = NULL){
        //$params = $this->params['pass'];
        //$userId = $params[0];
        if(!empty($userId)){
            App::import('model','UserColleague');
            $UserColleague = new UserColleague();
            $conditions = ' AND User.status =  1 AND  User.approved = 1';
            //** Colleague list invited by me
            $myColleagueQuerData = "SELECT User.email, UserProfile.first_name, UserProfile.last_name ,UserProfile.country_id, Country.country_name, UserColleague.colleague_user_id   
                    FROM user_colleagues UserColleague 
                    INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (1) AND UserColleague.user_id = " . $userId . $conditions;
                    $myColleagueData = $UserColleague->query( $myColleagueQuerData );
            //** Colleague List Invited to me
            $meColleagueQuerData = "SELECT User.email, UserProfile.first_name, UserProfile.last_name ,UserProfile.country_id, Country.country_name, UserColleague.user_id  
                    FROM user_colleagues UserColleague  
                    INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (1) AND UserColleague.colleague_user_id = " . $userId . $conditions;
                    $meColleagueData = $UserColleague->query( $meColleagueQuerData );
            //** Show colleague list
            $tableHeader = "<table border=1 align='center'>";
            $tableHeader .= "<tr><td><strong>Email</strong></td><td><strong>Name</strong></td><td><strong>Country Name</strong></td><td><strong>Action</strong></tr>";
            echo $tableHeader;
            //$tableData = "<table border=1>";
            if(!empty($myColleagueData) || !empty($meColleagueData)){
                foreach($myColleagueData as $myCol){
                    $collegaueCountryId = ""; $colleagueUserId = "";
                    $myColName = $myCol['UserProfile']['first_name']. ' ' .$myCol['UserProfile']['last_name'];
                    $collegaueCountryId = $myCol['UserProfile']['country_id'];
                    $colleagueUserId = $myCol['UserColleague']['colleague_user_id'];
                    $removeUrl = "";
                    if(!empty($collegaueCountryId) && !empty($countryId) && $collegaueCountryId != $countryId){
                        $removeUrl = BASE_URL . 'admin/Report/removeOtherCountryColleague/' . $userId . '/'. $colleagueUserId;
                    }
                    $tableData .= "<tr>";
                    $tableData .= "<td>" . $myCol['User']['email'] . "</td>";
                    $tableData .= "<td>" . $myColName . "</td>";
                    $tableData .= "<td>" . $myCol['Country']['country_name'] . "</td>";
                    if(!empty($removeUrl)){
                        $tableData .= "<td><a href='". $removeUrl ."' target='_blank'>" . 'Remove' . "</a></td>";
                    }else{
                       $tableData .= "<td>" . 'Remove' . "</td>"; 
                    }
                    $tableData .= "</tr>";
                }
            
                foreach($meColleagueData as $meCol){
                    $collegaueCountryId = ""; $colleagueUserId = ""; 
                    $meColName = $meCol['UserProfile']['first_name']. ' ' .$meCol['UserProfile']['last_name'];
                    $collegaueCountryId = $meCol['UserProfile']['country_id'];
                    $colleagueUserId = $meCol['UserColleague']['user_id'];
                    $removeUrl = "";
                    if(!empty($collegaueCountryId) && !empty($countryId) && $collegaueCountryId != $countryId){
                        $removeUrl = BASE_URL . 'admin/Report/removeOtherCountryColleague/' . $userId . '/'. $colleagueUserId;
                    }
                    $tableData .= "<tr>";
                    $tableData .= "<td>" . $meCol['User']['email'] . "</td>";
                    $tableData .= "<td>" . $meColName . "</td>";
                    $tableData .= "<td>" . $meCol['Country']['country_name'] . "</td>";
                    if(!empty($removeUrl)){
                        $tableData .= "<td><a href='". $removeUrl ."' target='_blank'>" . 'Remove' . "</a></td>";
                    }else{
                       $tableData .= "<td>" . 'Remove' . "</td>"; 
                    }
                    $tableData .= "</tr>";
                }
                $tableData .= "</table>";
                echo $tableData;
            }else{
                echo "Colleague Not Found";
            }
        }else{
            echo "User Not Found";
        }
        
        exit;
    }

    /*
        Remove other country colleague
    */
    public function removeOtherCountryColleague($userId = NULL, $colleagueUserId = NULL){
        //** Get user details
        $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
        //** Get colleague user details
        $colleagueUserData = $this->User->find("first", array("conditions"=> array("User.id"=> $colleagueUserId)));
        if($userData['UserProfile']['country_id'] != $colleagueUserData['UserProfile']['country_id']){
            //** Delete colleague me colleague
            $removeMeColleague = $this->UserColleague->updateAll(array("status"=> 0),array("user_id"=> $userId, "colleague_user_id"=> $colleagueUserId));
            //** Delete colleague me colleague
            $removeMyColleague = $this->UserColleague->updateAll(array("status"=> 0),array("user_id"=> $colleagueUserId, "colleague_user_id"=> $userId));
            if($removeMeColleague && $removeMyColleague){
                $msg = "Colleague Removed.";
            }else{
                $msg = "Sorry! Some problem. Try Again.";
            }
        }else{
            $msg = "Both have same country.";
        }
        echo $msg;
        exit;
    }

    /*
    ------------------------------------------
    On: 11-08-2016
    I/P:
    O/P:
    Desc: Pending Colleague List user wise
    ------------------------------------------
    */
    public function getPendingColleagueList($fromDate = NULL, $toDate = NULL ){
        //echo $toDate , $fromDate ;
        $toDate = !empty($toDate ) ? $toDate : 0;
        $fromDate = !empty($fromDate) ? $fromDate : 0;
        if(strtotime($toDate) > strtotime($fromDate)){
        $userDetails = $this->User->find("all", 
                                    array("conditions"=> 
                                        array("User.status"=> 1, "User.approved"=> 1)
                                        ),
                                    array("fields"=>
                                            "User.id",
                                            "User.email",
                                            "UserProfile.first_name",
                                            "UserProfile.last_name",
                                            "UserProfile.country_id"
                                        )
                                    );

        $tableHeader = "<table border=1 align='center'>";
        $tableHeader .= "<tr><td><strong>Email</strong></td><td><strong>Name</strong></td><td><strong>Country Name</strong></td><td><strong>Pending Colleague Count</strong></td></tr>";
        
        echo $tableHeader;
        //** Count colleagues[START]
            App::import('model','UserColleague');
            $UserColleague = new UserColleague();
            $conditions = ' AND User.status =  1 AND  User.approved = 1 ';
            if(!empty($toDate) && !empty($fromDate)){
                $toDateFilter = date('Y-m-d', strtotime($toDate));
                $fromDateFilter = date('Y-m-d', strtotime($fromDate));
                $conditions .= " AND UserColleague.created > '". $fromDateFilter ."' AND UserColleague.created < '". $toDateFilter. "'"  ;
            }
        foreach($userDetails as $ud){
            $countryData = array(); $userCountry = ""; $colleagueCount = 0;
            if( !empty($ud['UserProfile']['country_id']) ){
                    $countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
                    $userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';
            }
            //$colleagueCount = $this->UserColleague->find("count", array("conditions"=> array("OR"=> array( array("user_id"=> $ud['User']['id']), array("colleague_user_id"=> $ud['User']['id'])), "status"=> 1))); 
            
                //** Colleague list invited by me
                    $myColleagueQuer = "SELECT count(*)  AS totCnt 
                    FROM user_colleagues UserColleague 
                    INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (2) AND UserColleague.user_id = " . $ud['User']['id'] . $conditions;
                    $myColleague = $UserColleague->query( $myColleagueQuer );
                    //** Colleague List Invited to me
                    $meColleagueQuer = "SELECT count(*)  AS totCnt 
                    FROM user_colleagues UserColleague 
                    INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (2) AND UserColleague.colleague_user_id = " . $ud['User']['id'] . $conditions;
                    $meColleague = $UserColleague->query( $meColleagueQuer );
                    $colleagueCount = $myColleague[0][0]['totCnt'] + $meColleague[0][0]['totCnt'];
            //** Count colleagues[END]
            //if($colleagueCount > 0){
                $userName = $ud['UserProfile']['first_name'] ." " . $ud['UserProfile']['last_name'];
                $colleagueRecordUrl = BASE_URL . 'admin/Report/pendingColleagueDetail/' . $ud['User']['id']. '/'. $ud['UserProfile']['country_id']. '/'. $fromDate. '/'. $toDate ;
                $tableData = "<tr>";
                $tableData .= "<td>" . $ud['User']['email'] . "</td>";
                $tableData .= "<td>" . $userName . "</td>";
                $tableData .= "<td>" . $userCountry . "</td>";
                if($colleagueCount > 0){
                    $tableData .= "<td><a href='". $colleagueRecordUrl ."' target='_blank'>" . $colleagueCount . "</a></td>";
                }else{
                    $tableData .= "<td>" . $colleagueCount . "</td>";
                }
                $tableData .= "</tr>";
            //}
            
             //echo "<pre>";print_r($myColleagueData);  echo "<pre>";print_r($meColleagueData);    
            echo $tableData;
        }
            echo "</table>";
        }else{
            echo "To date Should be gretaer than from date";
        }
        exit;
    }

    /*

    */
    public function pendingColleagueDetail($userId = NULL, $countryId = NULL, $fromDate = NULL, $toDate = NULL){
        //$params = $this->params['pass'];
        //$userId = $params[0];
        $toDate = !empty($toDate ) ? $toDate : 0;
        $fromDate = !empty($fromDate) ? $fromDate : 0;
        if(strtotime($toDate) > strtotime($fromDate)){
        if(!empty($userId)){
            App::import('model','UserColleague');
            $UserColleague = new UserColleague();
            $conditions = ' AND User.status =  1 AND  User.approved = 1 ';
            if(!empty($toDate) && !empty($fromDate)){
                $toDateFilter = date('Y-m-d', strtotime($toDate));
                $fromDateFilter = date('Y-m-d', strtotime($fromDate));
                $conditions .= " AND UserColleague.created > '". $fromDateFilter ."' AND UserColleague.created < '". $toDateFilter. "'"  ;
            }
            //** Colleague list invited by me
            $myColleagueQuerData = "SELECT User.email, UserProfile.first_name, UserProfile.last_name ,UserProfile.country_id, Country.country_name, UserColleague.colleague_user_id   
                    FROM user_colleagues UserColleague 
                    INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (2) AND UserColleague.user_id = " . $userId . $conditions;
                    $myColleagueData = $UserColleague->query( $myColleagueQuerData );
            //** Colleague List Invited to me
            $meColleagueQuerData = "SELECT User.email, UserProfile.first_name, UserProfile.last_name ,UserProfile.country_id, Country.country_name, UserColleague.user_id  
                    FROM user_colleagues UserColleague  
                    INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
                    INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
                    LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
                    LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
                    WHERE 
                    User.status = 1 AND UserColleague.status IN (2) AND UserColleague.colleague_user_id = " . $userId . $conditions;
                    $meColleagueData = $UserColleague->query( $meColleagueQuerData );
            //** Show colleague list
            $tableHeader = "<table border=1 align='center'>";
            $tableHeader .= "<tr><td><strong>Email</strong></td><td><strong>Name</strong></td><td><strong>Country Name</strong></td><td><strong>Action</strong></tr>";
            echo $tableHeader;
            //$tableData = "<table border=1>";
            if(!empty($myColleagueData) || !empty($meColleagueData)){
                foreach($myColleagueData as $myCol){
                    $collegaueCountryId = ""; $colleagueUserId = "";
                    $myColName = $myCol['UserProfile']['first_name']. ' ' .$myCol['UserProfile']['last_name'];
                    $collegaueCountryId = $myCol['UserProfile']['country_id'];
                    $colleagueUserId = $myCol['UserColleague']['colleague_user_id'];
                    $removeUrl = "";
                    //if(!empty($collegaueCountryId) && !empty($countryId) && $collegaueCountryId != $countryId){
                        $removeUrl = BASE_URL . 'admin/Report/removePendingColleague/' . $userId . '/'. $colleagueUserId;
                    //}
                    $tableData .= "<tr>";
                    $tableData .= "<td>" . $myCol['User']['email'] . "</td>";
                    $tableData .= "<td>" . $myColName . "</td>";
                    $tableData .= "<td>" . $myCol['Country']['country_name'] . "</td>";
                    if(!empty($removeUrl)){
                        $tableData .= "<td><a href='". $removeUrl ."' target='_blank'>" . 'Remove' . "</a></td>";
                    }else{
                       $tableData .= "<td>" . 'Remove' . "</td>"; 
                    }
                    $tableData .= "</tr>";
                }
            
                foreach($meColleagueData as $meCol){
                    $collegaueCountryId = ""; $colleagueUserId = ""; 
                    $meColName = $meCol['UserProfile']['first_name']. ' ' .$meCol['UserProfile']['last_name'];
                    $collegaueCountryId = $meCol['UserProfile']['country_id'];
                    $colleagueUserId = $meCol['UserColleague']['user_id'];
                    $removeUrl = "";
                    //if(!empty($collegaueCountryId) && !empty($countryId) && $collegaueCountryId != $countryId){
                        $removeUrl = BASE_URL . 'admin/Report/removePendingColleague/' . $userId . '/'. $colleagueUserId;
                    //}
                    $tableData .= "<tr>";
                    $tableData .= "<td>" . $meCol['User']['email'] . "</td>";
                    $tableData .= "<td>" . $meColName . "</td>";
                    $tableData .= "<td>" . $meCol['Country']['country_name'] . "</td>";
                    if(!empty($removeUrl)){
                        $tableData .= "<td><a href='". $removeUrl ."' target='_blank'>" . 'Remove' . "</a></td>";
                    }else{
                       $tableData .= "<td>" . 'Remove' . "</td>"; 
                    }
                    $tableData .= "</tr>";
                }
                $tableData .= "</table>";
                echo $tableData;
                }else{
                    echo "Colleague Not Found";
                }
            }else{
                echo "User Not Found";
            }
        }else{
            echo "To date Should be gretaer than from date";
        }
        
        exit;
    }

    /*
        Remove other country colleague
    */
    public function removePendingColleague($userId = NULL, $colleagueUserId = NULL){
        //** Get user details
        $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
        //** Get colleague user details
        $colleagueUserData = $this->User->find("first", array("conditions"=> array("User.id"=> $colleagueUserId)));
        
            //** Delete colleague me colleague
            $removeMeColleague = $this->UserColleague->updateAll(array("status"=> 0),array("user_id"=> $userId, "colleague_user_id"=> $colleagueUserId));
            //** Delete colleague me colleague
            $removeMyColleague = $this->UserColleague->updateAll(array("status"=> 0),array("user_id"=> $colleagueUserId, "colleague_user_id"=> $userId));
            if($removeMeColleague && $removeMyColleague){
                $msg = "Colleague Removed.";
            }else{
                $msg = "Sorry! Some problem. Try Again.";
            }
        
        echo $msg;
        exit;
    }

    /*
        get userwise appdownload reports
    */
    public function getAppDownloadByUser($fromDate = NULL, $toDate = NULL){
        $toDate = !empty($toDate ) ? date('Y-m-d', strtotime($toDate)) : 0;
        $fromDate = !empty($fromDate) ? date('Y-m-d', strtotime($fromDate)) : 0;
            //** table Header [START]
            $tableHeader = "<table border=1 align='center'>";
            $tableHeader .= "<tr><td><strong>Email</strong></td><td><strong>Name</strong></td><td><strong>Version</strong></td><td><strong>Download Date</strong></td><td><strong>Device Type</strong></td></tr>";
            echo $tableHeader;
            //** table Header [END]
            $conditions = "";
            if(!empty($fromDate)){
                if(strlen($conditions) > 0){
                    $conditions .= " AND DATE(AppDownloadLog.created) >= '$fromDate' ";
                }else{
                    $conditions .= " DATE(AppDownloadLog.created) >= '$fromDate' ";
                }
            }
            if(!empty($toDate)){
                if(strlen($conditions) > 0){
                    $conditions .= " AND DATE(AppDownloadLog.created) <= '$toDate' ";
                }else{
                    $conditions .= " DATE(AppDownloadLog.created) <= '$toDate' ";
                }
            }
            $fields = array('User.id, User.email, UserProfile.first_name, UserProfile.last_name, AppDownloadLog.id, AppDownloadLog.version, AppDownloadLog.created, AppDownloadLog.device_type');
            $appDownloadData = $this->AppDownloadLog->find("all",
                array(
                    'joins'=> array(
                        array(
                          'table' => 'users',
                          'alias' => 'User',
                          'type' => 'INNER',
                          'conditions'=> array('AppDownloadLog.user_id = User.id')
                      ),
                        array(
                          'table' => 'user_profiles',
                          'alias' => 'UserProfile',
                          'type' => 'INNER',
                          'conditions'=> array('User.id = UserProfile.user_id')
                      ),
                    ),
                    'fields'=> $fields,
                    'conditions' => $conditions,
                    'order'=> 'AppDownloadLog.created DESC',
                    'group'=> 'User.email'
                    //'limit'=> $limit
                )
            );
            //echo "<pre>"; print_r($appDownloadData);die;
            //** table Data [START]
            $tableData = "";
            if(!empty($appDownloadData)){
                foreach($appDownloadData as $appDownload){
                    $tableData .= "<tr>";
                    $tableData .= "<td>" . $appDownload['User']['email'] . "</td>";
                    $tableData .= "<td>" . $appDownload['UserProfile']['first_name'] . ' '. $appDownload['UserProfile']['last_name'] . "</td>";
                    $tableData .= "<td>" . $appDownload['AppDownloadLog']['version'] . "</td>";
                    $tableData .= "<td>" . $appDownload['AppDownloadLog']['created'] . "</td>";
                    $tableData .= "<td>" . $appDownload['AppDownloadLog']['device_type'] . "</td>";
                    $tableData .= "</tr>";
                }
            }else{
                $tableData .= "<tr>";
                $tableData .= "<td colspan='6' align='center'><span style='color:red;'>" . 'Record(s) Not Found!' . "</span></td>";
                $tableData .= "</table>"; 
            }
            //** table Data [END]
            $tableData .= "</table>";
            echo $tableData;
        exit;
    }

}