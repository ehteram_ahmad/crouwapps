<?php
class SettingController extends AdminAppController {
    public $uses = array('Admin.User','Admin.CipOnOff','Admin.ForceLogoutSetting', 'Admin.UserDndStatus','Admin.DndStatusOption');
    public $components = array('RequestHandler','Paginator','Session');
    public $helpers = array('Js','Html', 'Paginator');

    /*
    ---------------------------------------------------------------------
    On: 05-04-2017
    I/P: 
    O/P: 
    Desc: Manage Settings
    ---------------------------------------------------------------------
    */
    public function manageCip(){
        if(!empty($this->request->data)){
            $updateVal = 0;
            if($this->request->data['onOffVal']=='ON'){
                $updateVal = 1;
            }
            $this->CipOnOff->updateAll(array("value"=> $updateVal), array("id"=> 1));
        }
        $cipOnOffData = $this->CipOnOff->find("first", array("conditions"=> array("status"=> 1)));
        $this->set('cipOnOffval', $cipOnOffData);
    }

    /*
    ---------------------------------------------------------------------
    On: 05-04-2017
    I/P: 
    O/P: 
    Desc: Manage Force Logout
    ---------------------------------------------------------------------
    */
    public function manageForceLogout(){
        if(!empty($this->request->data)){
            $forceLogoutVal = 0;
            if($this->request->data['forceLogoutVal']=='ON'){
                $forceLogoutVal = 1;
            }
            $this->ForceLogoutSetting->updateAll(array("value"=> $forceLogoutVal), array("id"=> 1));
        }
        $forceLogoutValData = $this->ForceLogoutSetting->find("first", array("conditions"=> array("status"=> 1)));
        $this->set('forceLogoutVal', $forceLogoutValData['ForceLogoutSetting']['value']);
    }

    /*
    ---------------------------------------------------------------------
    On: 25-Feb-2019
    I/P: 
    O/P: 
    Desc: Dnd Status Option Listing
    ---------------------------------------------------------------------
    */

    public function dndStatusOptions(){
        if(!empty($this->data['spclId'])){
            $this->layout=null;
            $spclId=$this->data['spclId'];
            $dndStatusOption = $this->DndStatusOption->find("first",array('conditions'=>array('id'=>"$spclId")));
            $this->set('dndStatusOption', $dndStatusOption);
            $this->render('/Elements/edit_speciality');
        }
        if((!empty($this->data['addDndStatusOptions']['name']))){
            // if($this->data['addSpeciality']['icon']['error']=="0"){
                $specData=$this->data['addDndStatusOptions'];
                if(!empty($specData['type'])){
                    $condition=array('id'=>$specData['id']);
                }
                else{
                    $condition=array('dnd_name'=>$specData['dnd_name']);
                }
                if($specData['icon']['error']=="0"){
                    move_uploaded_file($specData['icon']['tmp_name'],WWW_ROOT . 'img/specilities/'. $specData['icon']['name']);
                    $imgCon=1;
                }
                else{
                    $imgCon=0; 
                }
                $exstSpec = $this->DndStatusOption->find("first",array('conditions'=>$condition));
                if(!empty($exstSpec)){
                    if($imgCon==1){
                    unlink(WWW_ROOT . 'img/specilities/'.$exstSpec['Specilities']['img_name']);
                    $specialityData = array("dnd_name"=>"'".$specData['dnd_name']."'","img_name"=>"'".$specData['icon']['name']."'","status"=>$specData['status']);
                    }
                    else{
                        $specialityData = array("name"=>"'".$specData['name']."'","status"=>$specData['status']);
                    }
                    $this->DndStatusOption->updateAll($specialityData,array('DndStatusOption.id' => $specData['id']));
                }
                else{
                    $specialityData = array("dnd_name"=>$specData['name'],"img_name"=>$specData['icon']['name'],"status"=>$specData['status']);
                    $this->DndStatusOption->saveAll($specialityData);
                }
        }
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='dnd_name'){
              $sort="DndStatusOption.dnd_name $order";
            }
            else{
            $sort="DndStatusOption.dnd_name $order";
            }
        }
        else{
            $sort="DndStatusOption.dnd_name ASC";
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $conditions=array('DndStatusOption.status'=>array(0,1));
        $options=array( 'order'=> $sort, 
                        'conditions'=> $conditions,
                        'limit'=> $limit,
                        'page'=>$j
                    );
        $this->Paginator->settings = $options;
        $data = $this->Paginator->paginate('DndStatusOption');
        $this->set(array('dndStatusOption'=>$data,'limit'=>$limit));
        /*$specilities = $this->Specilities->find("all");
        //echo '<pre>';print_r($specilities);die;
        $this->set('specilities', $specilities);*/
    }

}