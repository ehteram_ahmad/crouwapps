<?php
class QrCodeController extends AdminAppController {

    public $components = array('Cookie','Session','Common','Paginator','Admin.AdminEmail');
    public $helpers = array('Html','Js','Form','Session','Time');
    public $uses = array('Admin.CompanyName', 'Admin.QrCodeDetail','Admin.QrCodeRequest','Admin.UserSubscriptionLog','Admin.InstituteSelectionTransaction','Admin.UserDevice','Admin.User','Admin.EnterpriseUserList');

    /*
    ----------------------------------------------------------------------------------------
    On: 14-08-2018
    I/P:
    O/P:
    Desc: genrate Qr code requested by subadmin
    ----------------------------------------------------------------------------------------
    */

    public function qrCodeIndex(){
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $conditions = array('QrCodeRequest.status'=> [1,2,3]);
        $fields = array(
            "CompanyName.company_name,
            QrCodeRequest.id,
            QrCodeRequest.number_of_qr_codes,
            QrCodeRequest.status,
            QrCodeRequest.requested_at,
            QrCodeRequest.validity"
        );
        $options  =
            array(
               'joins'=>array(
                    array(
                        'table' => 'company_names',
                        'alias' => 'CompanyName',
                        'type' => 'left',
                        'conditions'=> array('QrCodeRequest.institute_id = CompanyName.id')
                    )
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'order'=> 'QrCodeRequest.requested_at DESC',
                'limit'=> $limit,
                'page'=>$j,
            );

        $this->Paginator->settings = $options;
        $qrCodeRequests = $this->Paginator->paginate('QrCodeRequest');
        // $qrCodeRequests = $this->QrCodeRequest->find("all", $options);

        $master_data = [];
        foreach ($qrCodeRequests as $requestId) {
            $batchNumber = $this->QrCodeDetail->find('first',array('conditions'=>array('request_id'=>$requestId['QrCodeRequest']['id'])));
            $final_data = $requestId;
            $final_data['batch_number'] = $batchNumber;
            array_push($master_data,$final_data);
        }


        $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0,"is_subscribed"=>1),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));


        $tCount=$this->QrCodeRequest->find('count',array('conditions'=>$conditions,'group'=> array('QrCodeRequest.id')));

        $this->set(array("qrCodeRequests"=>$master_data, "companyNames"=> $companyNames, "condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
    }

    /*
    ----------------------------------------------------------------------------------------
    On: 14-08-2018
    I/P:
    O/P:
    Desc: genrate Qr code requested by subadmin
    ----------------------------------------------------------------------------------------
    */

    public function qrCodeIndexFilter(){
        $this->layout = null;
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $conditions = array('QrCodeRequest.status'=> [1,2,3]);
        $fields = array(
            "CompanyName.company_name,
            QrCodeRequest.id,
            QrCodeRequest.number_of_qr_codes,
            QrCodeRequest.status,
            QrCodeRequest.requested_at,
            QrCodeRequest.validity"
        );
        $options  =
            array(
               'joins'=>array(
                    array(
                        'table' => 'company_names',
                        'alias' => 'CompanyName',
                        'type' => 'left',
                        'conditions'=> array('QrCodeRequest.institute_id = CompanyName.id')
                    )
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'order'=> 'QrCodeRequest.requested_at DESC',
                'limit'=> $limit,
                'page'=>$j,
            );

        $this->Paginator->settings = $options;
        $qrCodeRequests = $this->Paginator->paginate('QrCodeRequest');
        // $qrCodeRequests = $this->QrCodeRequest->find("all", $options);

        $master_data = [];
        foreach ($qrCodeRequests as $requestId) {
            $batchNumber = $this->QrCodeDetail->find('first',array('conditions'=>array('request_id'=>$requestId['QrCodeRequest']['id'])));
            $final_data = $requestId;
            $final_data['batch_number'] = $batchNumber;
            array_push($master_data,$final_data);
        }


        $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0,"is_subscribed"=>1),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));


        $tCount=$this->QrCodeRequest->find('count',array('conditions'=>$conditions,'group'=> array('QrCodeRequest.id')));

        $this->set(array("qrCodeRequests"=>$master_data, "companyNames"=> $companyNames, "condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        $this->render('/Elements/Qr/qr_code_index_filter');
    }

    public function genrateQrCodeByAdmin(){
        $data = array('');
        $requestData = array(
            "institute_id"=> $this->request->data['institute_id'],
            "number_of_qr_codes"=> $this->request->data['number_of_qr_codes'],
            "validity"=> $this->request->data['validity'],
            "status"=> 0
        );

        $this->QrCodeRequest->saveAll( $requestData );

        $this->request->data['request_id'] = $this->QrCodeRequest->getLastInsertId();

        $this->genrateQrCode($this->request->data);
    }

    public function genrateQrCode()
    {
        App::import('Vendor','QRcode',array('file' => 'phpqrcode/qrlib.php'));
        App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));
        App::import('Vendor','FPDI',array('file' => 'fpdi/fpdi.php'));
        App::import('Vendor','FPDF',array('file' => 'fpdf/fpdf.php'));
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $fpdi = new FPDI();
        $fpdf = new FPDF('P', 'mm', 'A4');

        $fpdf->AddPage();
        $numberOfQrCode = $this->request->data['number_of_qr_codes'];
        $validity = $this->request->data['validity'];
        $instituteId =  $this->request->data['institute_id'];
        $requestId =  $this->request->data['request_id'];

        $batchNumber = "MB-${instituteId}-".strtotime(date('Y-m-d H:i:s'));

        for($i= 1; $i <= $numberOfQrCode; $i++){
            $qrcodenumber = $instituteId.rand(1000,100000).$i;
            $arr = array("batch_number" => $batchNumber, "qr_code_number"=> $qrcodenumber,"institute_id" => $instituteId);
            $json_data = json_encode($arr);
            // echo "$json_data";exit();
            $text= $json_data;
            $folder = APP.'webroot/qrCode/';
            $file_name= "qrcode$requestId".'-'."$i.png";
            $file_name=$folder.$file_name;
            QRcode::png($text,$file_name);
            $fpdf->image($file_name, 10, $fpdf->SetX(+40), 30, 30, "png");
            $qrCodeData=array('card_id'=>"${instituteId}1",'request_id'=>$requestId,'batch_number'=>$batchNumber,'qr_code_number'=>$qrcodenumber,'validity'=>$validity, "institute_id"=> $instituteId, "is_used"=>"1", "is_valid"=> "1");

            $this->QrCodeDetail->saveAll($qrCodeData);
        }

        $finalFileName = "QrCode$requestId"."-". strtotime(date('Y-m-d H:i:s')) . '.pdf';
        $output = $fpdf->Output(APP . 'webroot/qrCode/'.$finalFileName, 'F');
        if(file_exists(APP . 'webroot/qrCode/'.$finalFileName)){
            $userDetails = $this->QrCodeRequest->find('first',array('conditions'=>array('QrCodeRequest.id'=>$requestId)));
            if(!empty($userDetails)){
                if(isset($userDetails['QrCodeRequest']['user_id'])){
                    if($userDetails['QrCodeRequest']['user_id'] == 0){
                        $email = 'ehteram@mediccreations.com';
                        $name = 'Admin';
                    }else{
                        $user = $this->User->find('first',array('conditions'=>array('User.id'=>$userDetails['QrCodeRequest']['user_id'])));
                        $email = $user['User']['email'];
                        $name = $user['UserProfile']['first_name'];
                    }
                    //** Send Consent mail to patient
                    chmod(APP . 'webroot/qrCode/'.$finalFileName, 0777);

                    $client_params = array();
                    $client_params['file_name'] = $finalFileName;
                    $client_params['file_path']= base64_encode(file_get_contents(APP . 'webroot/qrCode/'.$finalFileName));
                    // $client_params['file_path'] = APP . 'webroot/qrCode/'.$finalFileName;
                    $client_params['file_type'] = 'application/pdf';
                    $client_params['to_email'] = $email;
                    $client_params['fname'] = $name;
                    $client_params['quantity'] = $numberOfQrCode;
                    $client_params['batch'] = $batchNumber;


                    $sendMail = $this->AdminEmail->sendQRCode( $client_params );
                    // ** If mail send successfully, update table
                    if(!empty($sendMail[0])){
                        $company = $this->CompanyName->find('first',array('conditions'=>array('CompanyName.id'=>$userDetails['QrCodeRequest']['institute_id'])));
                        $admin_params = array();
                        $admin_params['file_name'] = $finalFileName;
                        $admin_params['file_path']= base64_encode(file_get_contents(APP . 'webroot/qrCode/'.$finalFileName));
                        // $admin_params['file_path'] = APP . 'webroot/qrCode/'.$finalFileName;
                        $admin_params['file_type'] = 'application/pdf';
                        $admin_params['to_email'] = 'ehteram@mediccreations.com';
                        $admin_params['fname'] = 'Admin';
                        $admin_params['quantity'] = $numberOfQrCode;
                        $admin_params['batch'] = $batchNumber;
                        $admin_params['trust'] = $company['CompanyName']['company_name'];
                        $admin_params['date'] = date('d M Y');
                        $sendMail = $this->AdminEmail->sendQRCodeToAdmin( $admin_params );

                        $this->QrCodeRequest->updateAll(array("status"=> 1), array("id"=> $requestId));
                        $filePath = APP . 'webroot/qrCode/'.$finalFileName;
                        if( !empty($filePath) && file_exists( $filePath )){
                            chmod($filePath, 777);
                            unlink( $filePath );
                            $return = true;
                        }
                    }
                }
            }
            for($j= 1; $j <= $numberOfQrCode; $j++){

                $filePath = APP."webroot/qrCode/qrcode$requestId".'-'."$j.png";

                if( !empty($filePath) && file_exists( $filePath )){
                    chmod($filePath, 777);
                    unlink( $filePath );
                    $return = true;
                }
            }
        }
        exit();
    }




    /*
    ------------------------------------------------------------------------------------------
    On: 17-08-2018
    I/P:
    O/P:
    Desc:Qr Code Request Listing
    ------------------------------------------------------------------------------------------
    */

    public function requestQrCodeListing(){
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }

        //** Get User Data
        $conditions = array('QrCodeRequest.status'=>"0");
        $fields = array("QrCodeRequest.id,QrCodeRequest.number_of_qr_codes,QrCodeRequest.validity,QrCodeRequest.requested_at,CompanyName.id,CompanyName.company_name,QrCodeRequest.status");
        $options  =
        array(
            'joins'=>array(
                array(
                  'table' => 'company_names',
                  'alias' => 'CompanyName',
                  'type' => 'left',
                  'conditions'=> array('QrCodeRequest.institute_id = CompanyName.id')
              ),
            ),
            'fields'=> $fields,
            'conditions'=> $conditions,
            'group'=> array('QrCodeRequest.id'),
            'order'=> 'QrCodeRequest.id DESC',
            'limit'=> $limit,
            'page'=>$j
            );
    // pr($conditions);
            $tCount=$this->QrCodeRequest->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('QrCodeRequest.id')));
            $this->Paginator->settings = $options;
            $qrCodeData = $this->Paginator->paginate('QrCodeRequest');

            $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0,'CompanyName.is_subscribed'=>1),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));

        $this->set(array("qrCodeData"=>$qrCodeData, "companyNames"=> $companyNames, "condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
    }



    /*
    ------------------------------------------------------------------------------------------
    On: 17-08-2018
    I/P:
    O/P:
    Desc:Qr Code Request Listing
    ------------------------------------------------------------------------------------------
    */

    public function listFilter(){
        $this->layout = null;
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }

        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        if(!isset($this->data['company'])|| $this->data['company']==""){
            $company=array();
        }
        else{
            $company=array('QrCodeRequest.institute_id'=>$this->data['company']);
        }

        //** Get User Data
        $conditions = array('QrCodeRequest.status'=>"0");
        $new_condition = array_merge($company,$conditions);
        $fields = array("QrCodeRequest.id,QrCodeRequest.number_of_qr_codes,QrCodeRequest.validity,QrCodeRequest.requested_at,CompanyName.id,CompanyName.company_name,QrCodeRequest.status");
        $options  =
        array(
            'joins'=>array(
                array(
                  'table' => 'company_names',
                  'alias' => 'CompanyName',
                  'type' => 'left',
                  'conditions'=> array('QrCodeRequest.institute_id = CompanyName.id')
              ),
            ),
            'fields'=> $fields,
            'conditions'=> $new_condition,
            'group'=> array('QrCodeRequest.id'),
            'order'=> 'QrCodeRequest.id DESC',
            'limit'=> $limit,
            'page'=>$j
            );
    // pr($conditions);
            $tCount=$this->QrCodeRequest->find('count',array('conditions'=>$new_condition,'group'=> array('QrCodeRequest.id')));
            $this->Paginator->settings = $options;
            $qrCodeData = $this->Paginator->paginate('QrCodeRequest');

            $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0,'CompanyName.is_subscribed'=>1),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));

        $this->set(array("qrCodeData"=>$qrCodeData, "condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        $this->render('/Elements/Qr/list_filter');
    }


    /*
    ------------------------------------------------------------------------------------------
    On: 20-08-2018
    I/P:
    O/P:
    Desc: Delete user from list (soft delete)
    ------------------------------------------------------------------------------------------
    */
    public function rejectQrCodeRequest(){
        if(isset($this->request->data['requestId'])){
            $updateRequest = $this->QrCodeRequest->updateAll(array("QrCodeRequest.status"=> 2,"QrCodeRequest.reason"=>'"'.$this->request->data['reason'].'"'), array("QrCodeRequest.id"=> $this->request->data['requestId']));
            if($updateRequest){
                $qrRequest = $this->QrCodeRequest->find('first',array('conditions'=>array("QrCodeRequest.id"=> $this->request->data['requestId'])));
                if((int)$qrRequest['QrCodeRequest']['user_id'] != 0){
                    $user = $this->User->find('first',array('conditions'=>array('User.id'=>$qrRequest['QrCodeRequest']['user_id'])));

                    $paramsConsent = array();
                    $paramsConsent['to_email'] = $user['User']['email'];
                    $paramsConsent['fname'] = $user['UserProfile']['first_name'];
                    $paramsConsent['quantity'] = $qrRequest['QrCodeRequest']['number_of_qr_codes'];
                    $paramsConsent['reason'] = $this->request->data['reason'];

                    $this->AdminEmail->rejectQRCodeRequest( $paramsConsent );
                }
            //** In case of deleted user make entry in cache last modified user[END]
                echo "request Rejected";
            }else{
                echo "Some issue occured. Please try again.";
            }
        }else{
            echo "Request Not Deleted! Try Again.";
        }
        exit;
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 21-08-2018
    I/P:
    O/P:
    Desc: View And Edit Qr Batch And QrCode.
    ------------------------------------------------------------------------------------------
    */
    // public function qrCodeBatchView($id){
    //     $batchId = $id;
    //     $batchData = $this->QrCodeDetail->find('all', array('conditions'=> array('User.id'=> $batchId)));
    //     $this->set(array('batchData'=>$batchData,'batchId'=>$batchId));
    // }

    public function qrCodeBatchView()
    {
        $requestId = $this->request->params['pass'][0];
        // print_r($batchId);
        // exit();

        $fields = array("CompanyName.company_name,QrCodeDetail.qr_code_number, QrCodeDetail.is_used, QrCodeDetail.is_valid");
        $conditions = array('QrCodeDetail.batch_number'=>$requestId);
        $options  =
        array(
                'joins'=>array(
                    array(
                      'table' => 'company_names',
                      'alias' => 'CompanyName',
                      'type' => 'left',
                      'conditions'=> array('QrCodeDetail.institute_id = CompanyName.id')
                  ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions
                );

        $tCount = $this->QrCodeDetail->find('count',array('conditions'=>array('QrCodeDetail.batch_number'=>$requestId)));

        $batchData = $this->QrCodeDetail->find("all", $options);

        $this->set(array('batchData'=>$batchData,'batchId'=>$requestId,'tCount'=>$tCount));

    }

    /*
    ------------------------------------------------------------------------------------------
    On: 21-08-2018
    I/P:
    O/P:
    Desc: Expire All Qr Code releted to a perticular batch number
    ------------------------------------------------------------------------------------------
    */
    public function expireBatchNumber(){
        if(isset($this->request->data['batchNumber'])){
            $request_id = $this->request->data['requestId'];
            $this->QrCodeRequest->updateAll(array('QrCodeRequest.status'=>3),array('QrCodeRequest.id'=>$request_id));
            $updateRequest = $this->QrCodeDetail->updateAll(array("QrCodeDetail.is_valid"=> 0), array("QrCodeDetail.batch_number"=> $this->request->data['batchNumber']));


            $userDetails = $this->QrCodeRequest->find('first',array('conditions'=>array('QrCodeRequest.id'=>$request_id)));
            if(!empty($userDetails)){
                if($userDetails['QrCodeRequest']['user_id'] == 0){
                    $email = 'ehteram@mediccreations.com';
                    $name = 'Admin';
                }else{
                    $user = $this->User->find('first',array('conditions'=>array('User.id'=>$userDetails['QrCodeRequest']['user_id'])));
                    $email = $user['User']['email'];
                    $name = $user['UserProfile']['first_name'];
                }

                $client_params = array();
                $client_params['to_email'] = $email;
                $client_params['fname'] = $name;
                $client_params['quantity'] = $userDetails['QrCodeRequest']['number_of_qr_codes'];
                $client_params['batch'] = $this->request->data['batchNumber'];
                $client_params['date'] = date('M D Y');

                $this->AdminEmail->expireQrCode( $client_params );

                $company = $this->CompanyName->find('first',array('conditions'=>array('CompanyName.id'=>$userDetails['QrCodeRequest']['institute_id'])));

                $admin_params = array();
                $admin_params['to_email'] = 'deepakkumar@mediccreations.com';
                $admin_params['fname'] = 'Admin';
                $admin_params['quantity'] = $userDetails['QrCodeRequest']['number_of_qr_codes'];
                $admin_params['batch'] = $this->request->data['batchNumber'];
                $admin_params['trust'] = $company['CompanyName']['company_name'];

                $this->AdminEmail->expireQrCodeToAdmin( $admin_params );
            }
            if($updateRequest){
                $expiration_user_data = $this->QrCodeDetail->find('all',array('conditions'=>array('QrCodeDetail.is_used'=>0,'QrCodeDetail.batch_number'=>$this->request->data['batchNumber'])));
                foreach ($expiration_user_data as $expiration_user) {
                    $this->expireUserSubscription($expiration_user['QrCodeDetail']['qr_code_number']);
                }
            //** In case of deleted user make entry in cache last modified user[END]
                echo "Qr Code Expired";
            }else{
                echo "Some issue occured. Please try again.";
            }
        }else{
            echo "Request Not Deleted! Try Again.";
        }
        exit;
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 21-08-2018
    I/P:
    O/P:
    Desc: Expire All Qr Code releted to a perticular batch number
    ------------------------------------------------------------------------------------------
    */
    public function expireQRCodeNumber(){
        if(isset($this->request->data['QRCodeNumber'])){
            $batchNumber = $this->request->data['batchNumber'];
            $updateRequest = $this->QrCodeDetail->updateAll(array("QrCodeDetail.is_valid"=> 0), array("QrCodeDetail.qr_code_number"=> $this->request->data['QRCodeNumber']));
            if($updateRequest){
                $batch_details = $this->QrCodeDetail->find('first',array('conditions'=>array('QrCodeDetail.is_valid'=>1,'QrCodeDetail.batch_number'=>$batchNumber)));
                if(empty($batch_details)){
                    $condition_id = $this->QrCodeDetail->find('first',array('conditions'=>array('QrCodeDetail.batch_number'=>$batchNumber)));
                    $this->QrCodeRequest->updateAll(array('QrCodeRequest.status'=>3),array('QrCodeRequest.id'=>$condition_id['QrCodeDetail']['request_id']));
                }
                $this->expireUserSubscription($this->request->data['QRCodeNumber']);
            //** In case of deleted user make entry in cache last modified user[END]
                echo "Qr Code Expired";
            }else{
                echo "Some issue occured. Please try again.";
            }
        }else{
            echo "Request Not Deleted! Try Again.";
        }
        exit;
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 21-08-2018
    I/P:
    O/P:
    Desc: Expire All Qr Code releted to a perticular batch number
    ------------------------------------------------------------------------------------------
    */
    public function expireUserSubscription($qrCodeNumber){
        $expiration_user_data = $this->InstituteSelectionTransaction->find("first",array('conditions'=>array("InstituteSelectionTransaction.qr_code_number"=>$qrCodeNumber,"InstituteSelectionTransaction.status"=>1)));
        if(!empty($expiration_user_data)){
            $userIds = $expiration_user_data['InstituteSelectionTransaction']['user_id'];
            $company_id = $expiration_user_data['InstituteSelectionTransaction']['in_intitute'];
            print_r($userIds);
            print_r($company_id);
            if($userIds){
                $this->UserSubscriptionLog->updateAll(
                    array("UserSubscriptionLog.subscribe_type"=>3),
                    array("UserSubscriptionLog.user_id"=>$userIds)
                );
                $this->UserDevice->updateAll(
                    array("status"=> 0),
                    array("user_id"=> $userIds)
                );
                $userData = $this->User->findById( $userIds );

                $this->EnterpriseUserList->updateAll(
                    array("status"=> 0),
                    array(
                        "email" => $userData['User']['email'],
                        "company_id" => $company_id
                    )
                );
            }
        }
        // exit;
    }


}
