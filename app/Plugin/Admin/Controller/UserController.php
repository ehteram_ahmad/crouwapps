<?php
class UserController extends AdminAppController {
    //public $uses = array('Admin.User', 'User', 'EmailTemplate', 'EmailSmsLog', 'Country');
    public $uses = array('Admin.User',
     'Admin.UserProfile',
     'EmailTemplate',
     'EmailSmsLog',
     'Country',
     'UserPost',
     'UserColleague',
     'UserFollow',
     'Admin.UserInstitution',
     'Admin.UserEmployment',
     'Profession',
     'Specilities',
     'Admin.UserSpecilities',
     'Admin.UserInterest',
     'AdminUserSentMail',
     'AdminActivityLog',
     'Admin.CompanyName',
     'Designation',
     'EducationDegree',
     'InstituteName',
     'Admin.UserDevice',
     'AdminEmailCategorie',
     'AdminEmailTemplate',
     'TempRegistration',
     'AdminFeedbackResponse',
     'Admin.LivestreamBeatPostAccessUser',
     'Admin.AdminUser',
     'Admin.UserDeviceInfo',
     'Admin.CacheLastModifiedUser',
     'Admin.EnterpriseUserList',
     'Admin.UserRoleTag',
     'Admin.RoleTag',
     'Admin.AvailableAndOncallTransaction');
    public $components = array('RequestHandler','Paginator','Session', 'Common', 'Admin.AdminEmail', 'Image','Cache');
    public $helpers = array('Js','Html','Admin.Csv', 'Paginator');

    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
    );

    public function mail_conf() {
        // App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
        // $mandrill = new Mandrill('Mb39AZ8BVLBYO2N4SLmUjA');
        App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
        $mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');

    }

    /*
    On: 26-08-2015
    I/P:
    O/P:
    Desc:
    */
    public function userLists(){
        $this->Session->delete('User.approved');
        $this->Session->write('User.approved', "All");
        $params = array();
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
                $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
                $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
                $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
                $sort="User.email $order";
            }
            elseif($con=='country'){
                $sort="Country.country_name $order";
            }
            else{
                $sort="User.registration_date $order";
            }
        }else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }else{
            $limit=20;
        }
        $sortPost=$this->data;
        if(!isset($sortPost['to']) || $sortPost['to']==""){
          $endDate = date("Y-m-d");
        }else{
          $endDate = date('Y-m-d', strtotime($sortPost['to']));
        }
        if(!isset($sortPost['from']) || $sortPost['from']==""){
          $date = array();
        }else{
          $startDate = date('Y-m-d', strtotime($sortPost['from']));
          $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
        }
        if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
            $firstName=array();
        }else{
            $sortPost['textFirstName']=trim($sortPost['textFirstName']);
            $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower('%'.$sortPost['textFirstName'].'%'));
        }
        if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
            $lastName=array();
        }else{
            $sortPost['textLastName']=trim($sortPost['textLastName']);
            $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower('%'.$sortPost['textLastName'].'%'));
        }
        if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
            $mail=array();
        }else{
            $sortPost['textEmail']=trim($sortPost['textEmail']);
            $mail=array('LOWER(User.email) LIKE'=>strtolower('%'.$sortPost['textEmail'].'%'));
        }
        if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
            $interest=array();
        }else{
            $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);
        }
        if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
            $status=array('User.status'=>array(0,1));
        }else{
            switch($sortPost['statusVal']){
                case 0:
                    $status=array('User.status'=>array(0,1));
                    break;
                case 1:
                    $status=array('User.status'=>1,'User.approved'=>0);
                    break;
                case 2:
                    $status=array('User.status'=>0,'User.approved'=>1);
                    break;
                case 3:
                    $status=array('User.status'=>1,'User.approved'=>1);
                    break;
                case 4:
                    $status=array('User.status'=>0,'User.approved'=>0);
                    break;
                case 5:
                    $status=array('User.status'=>2);
                    break;
            }
         }
         if(!empty($sortPost['country'])){
             $country=array('UserProfile.country_id'=>$sortPost['country']);
         }
         else{
            $country=array();
         }
         if(!empty($sortPost['profession'])){
             $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
         }
         else{
            $profession=array();
         }
         if(!empty($sortPost['company'])){
             $company=array('UserEmployment.company_id'=>$sortPost['company'],'UserEmployment.is_current' => 1);
         }
         else{
            $company=array();
         }
         if(!isset($sortPost['exccompanies'])|| $sortPost['exccompanies']==""){
            $exccompanies=array();
        }else{
            $exccompanies=array('UserEmployment.company_id !='=>$sortPost['exccompanies'], 'UserEmployment.is_current' => 1);
        }

         if(!isset($sortPost['subscriptionVal'])||$sortPost['subscriptionVal']==0){
             $subscription = array();
         }else{
             switch($sortPost['subscriptionVal']){
                 case 0:
                     $subscription = array();
                     break;
                 case 1:
                     // $subscription = array('User.status'=>1,'User.approved'=>0);
                     $subscription = array('EnterpriseUserList.status'=>array(0,1));
                     break;
                 case 2:
                     $subscription = array('EnterpriseUserList.status'=>1);
                     break;
                 case 3:
                     $subscription = array('EnterpriseUserList.status'=>0);
                     break;
                 case 4:
                     // $subscription = array('User.status'=>0,'User.approved'=>0);
                     $subscription = array('UserSubscriptionLog.subscribe_type'=>3);
                     break;
                 case 5:
                     $subscription = array('EnterpriseUserList.status'=>2);
                     break;
                 case 6:
                     // $subscription = array('User.status'=>0,'User.approved'=>0);
                     $subscription = array('EnterpriseUserList.status'=>1,'UserSubscriptionLog.subscribe_type != '=>3);
                     break;
                 case 7:
                     $subscription = array('EnterpriseUserList.status'=>0,'UserSubscriptionLog.subscribe_type != '=>3);
                     break;
             }
          }

        $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$profession, $company,$exccompanies, $subscription);
        // print_r($conditions);
        // exit();

        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1 GROUP BY profession_type ORDER BY profession_type ASC');
        $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        $interestLists['interestlist'] = $interestlists;
        $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));
        $this->set(array('interestLists'=>$interestLists,'country'=>$country,'professions'=>$profession, "companyNames"=> $companyNames));
        //** Get User Data

        $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
        $options  =
            array(
            'joins'=>array(
                array(
                  'table' => 'countries',
                  'alias' => 'Country',
                  'type' => 'left',
                  'conditions'=> array('UserProfile.country_id = Country.id')
              ),
                array(
                  'table' => 'professions',
                  'alias' => 'Profession',
                  'type' => 'left',
                  'conditions'=> array('UserProfile.profession_id = Profession.id')
              ),
                array(
                  'table' => 'user_specilities',
                  'alias' => 'UserSpecility',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserSpecility.user_id')
              ),
                array(
                  'table' => 'user_employments',
                  'alias' => 'UserEmployment',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserEmployment.user_id')
              ),
              array(
                'table' => 'enterprise_user_lists',
                'alias' => 'EnterpriseUserList',
                'type' => 'left',
                'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
              ),
              array(
                'table' => 'user_subscription_logs',
                'alias' => 'UserSubscriptionLog',
                'type' => 'left',
                'conditions'=> array('User.id = UserSubscriptionLog.user_id')
              ),
            ),
            'fields'=> $fields,
            'conditions'=> $conditions,
            'group'=> array('User.id'),
            // 'group'=> array('UserEmployment.user_id'),
            'order'=> $sort,
            'limit'=> $limit,
            'page'=>$j
        );
        // pr($conditions);
        // $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('User');

        $total=$this->request->params;
        $tCount=$total['paging']['User']['count'];

        if(isset($users) && !empty($users)){
            foreach($users as $user){
                $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                $user['mapped_user'] = $this->get_admin_mapped_user($user['User']['email']);
                $userS[]=$user;
            }
        }
        else{
          $userS=array();
        }
        $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        // if($this->request->is("ajax")){
        //     $this->render('/Elements/users/user_lists_ajax');
        // }
    }
    public function get_admin_user_mail($id){
        $mail=array();
        $conditions = array('AdminUserSentMail.user_id'=> $id);
        $mail=$this->AdminUserSentMail->find('all',array('conditions'=>$conditions));
        return $mail;
    }
    public function allFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
                $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
                $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
                $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
                $sort="User.email $order";
            }
            elseif($con=='country'){
                $sort="Country.country_name $order";
            }
            else{
                $sort="User.registration_date $order";
            }
        }else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }else{
            $limit=20;
        }
        $sortPost=$this->data;
        if(!isset($sortPost['to']) || $sortPost['to']==""){
          $endDate = date("Y-m-d");
        }else{
          $endDate = date('Y-m-d', strtotime($sortPost['to']));
        }
        if(!isset($sortPost['from']) || $sortPost['from']==""){
          $date = array();
        }else{
          $startDate = date('Y-m-d', strtotime($sortPost['from']));
          $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
        }
        if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
            $firstName=array();
        }else{
            $sortPost['textFirstName']=trim($sortPost['textFirstName']);
            $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower('%'.$sortPost['textFirstName'].'%'));
        }
        if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
            $lastName=array();
        }else{
            $sortPost['textLastName']=trim($sortPost['textLastName']);
            $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower('%'.$sortPost['textLastName'].'%'));
        }
        if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
            $mail=array();
        }else{
            $sortPost['textEmail']=trim($sortPost['textEmail']);
            $mail=array('LOWER(User.email) LIKE'=>strtolower('%'.$sortPost['textEmail'].'%'));
        }
        if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
            $interest=array();
        }else{
            $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);
        }
        if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
            $status=array('User.status'=>array(0,1));
        }else{
            switch($sortPost['statusVal']){
                case 0:
                    $status=array('User.status'=>array(0,1));
                    break;
                case 1:
                    $status=array('User.status'=>1,'User.approved'=>0);
                    break;
                case 2:
                    $status=array('User.status'=>0,'User.approved'=>1);
                    break;
                case 3:
                    $status=array('User.status'=>1,'User.approved'=>1);
                    break;
                case 4:
                    $status=array('User.status'=>0,'User.approved'=>0);
                    break;
                case 5:
                    $status=array('User.status'=>2);
                    break;
            }
         }
         if(!empty($sortPost['country'])){
             $country=array('UserProfile.country_id'=>$sortPost['country']);
         }
         else{
            $country=array();
         }
         if(!empty($sortPost['profession'])){
             $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
         }
         else{
            $profession=array();
         }
         if(!empty($sortPost['company'])){
             $company=array('UserEmployment.company_id'=>$sortPost['company'],'UserEmployment.is_current' => 1);
         }
         else{
            $company=array();
         }
        if(!isset($sortPost['exccompanies'])|| $sortPost['exccompanies']==""){
            $exccompanies=array();
        }else{
            $exccompanies=array('UserEmployment.company_id !='=>$sortPost['exccompanies'],'UserEmployment.is_current' => 1);
        }

         if(!isset($sortPost['subscriptionVal'])||$sortPost['subscriptionVal']==0){
             $subscription = array();
         }else{
             switch($sortPost['subscriptionVal']){
                 case 0:
                     $subscription = array();
                     break;
                 case 1:
                     // $subscription = array('User.status'=>1,'User.approved'=>0);
                     $subscription = array('EnterpriseUserList.status'=>array(0,1));
                     break;
                 case 2:
                     $subscription = array('EnterpriseUserList.status'=>1);
                     break;
                 case 3:
                     $subscription = array('EnterpriseUserList.status'=>0);
                     break;
                 case 4:
                     // $subscription = array('User.status'=>0,'User.approved'=>0);
                     $subscription = array('UserSubscriptionLog.subscribe_type'=>3);
                     break;
                 case 5:
                     $subscription = array('EnterpriseUserList.status'=>2);
                     break;
                 case 6:
                     // $subscription = array('User.status'=>0,'User.approved'=>0);
                     $subscription = array('EnterpriseUserList.status'=>1,'UserSubscriptionLog.subscribe_type != '=>3);
                     break;
                 case 7:
                     $subscription = array('EnterpriseUserList.status'=>0,'UserSubscriptionLog.subscribe_type != '=>3);
                     break;
             }
          }

        $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$profession, $company,$exccompanies, $subscription);
        $fields =array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
        $options  =
            array(
            'joins'=>array(
                array(
                  'table' => 'countries',
                  'alias' => 'Country',
                  'type' => 'left',
                  'conditions'=> array('UserProfile.country_id = Country.id')
              ),
                array(
                  'table' => 'professions',
                  'alias' => 'Profession',
                  'type' => 'left',
                  'conditions'=> array('UserProfile.profession_id = Profession.id')
              ),
                array(
                  'table' => 'user_specilities',
                  'alias' => 'UserSpecility',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserSpecility.user_id')
              ),
                array(
                  'table' => 'user_employments',
                  'alias' => 'UserEmployment',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserEmployment.user_id')
              ),
              array(
                'table' => 'enterprise_user_lists',
                'alias' => 'EnterpriseUserList',
                'type' => 'left',
                'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
              ),
              array(
                'table' => 'user_subscription_logs',
                'alias' => 'UserSubscriptionLog',
                'type' => 'left',
                'conditions'=> array('User.id = UserSubscriptionLog.user_id')
              ),
            ),
            'fields'=> $fields,
            'conditions'=> $conditions,
            'group'=> array('User.id'),
            // 'group'=> array('UserEmployment.user_id'),
            'order'=> $sort,
            'limit'=> $limit,
            'page'=>$j
        );
        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('User');
        if(isset($users) && !empty($users)){
            foreach($users as $user){
                $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                $user['mapped_user'] = $this->get_admin_mapped_user($user['User']['email']);
                $userS[]=$user;
            }
        }else{
          $userS=array();
        }
        $total=$this->request->params;
        $tCount=$total['paging']['User']['count'];
        $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        $this->render('/Elements/users/user_lists_ajax');

    }

    /*
    On: 27-08-2015
    I/P:
    O/P:
    Desc:
    */
    public function userListsSearch(){
        if ($this->request->data) {
            $data = $this->request->data;
            $search['firstname'] = $data['textFirstName'];
            $search['lastname'] = $data['textLastName'];
            $search['email'] = $data['textEmail'];
            $search['status'] = $data['selectStatus'];
            if(isset($data['interestlist']) && count($data['interestlist']) > 0){
                $search['interestlist'] = $data['interestlist'];
            }
            $this->Session->write('searchdata', $search);
            $this->redirect(array('action' => 'userLists'));
        }
    }

    /*
    On: 31-08-2015
    I/P:
    O/P:
    Desc: Change user status
    */
    public function changeUserStatus(){
        $userId = $this->request->data('user_id');
        $statusVal = $this->request->data('statusVal');
        $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
        $enterpriseUserListData= $this->EnterpriseUserList->find("first", array("conditions"=> array("EnterpriseUserList.email"=> $userDetails['User']['email'])));
        //echo "<pre>";print_r($userId);exit();
        if( trim($statusVal) == 'Active' ){
            $chngeStatus = 0;
            //** In case of user deleted deactivate all tokens[START]
            $tokenDeactivate = $this->UserDevice->updateAll(array("status"=> 0), array("user_id"=> $userId));
            //** In case of user deleted deactivate all tokens[END]
            $deletedStatus = 1;
        }else if( trim($statusVal) == 'Inactive' ){
            $chngeStatus = 1;
            $deletedStatus = 0;
        }
        if(!empty($enterpriseUserListData)){
          $approvedStatus = 1;
          $data = array(
                    'User.status'=> $chngeStatus,
                    'User.approved'=> $approvedStatus
                );
        }else{
          $data = array(
                    'User.status'=> $chngeStatus,
                    //'User.approved'=> $chngeStatus
                );
        }
        try {
          if( trim($statusVal) == 'Active' ){
            $params['user_id'] = $userId;
            $this->dissmissDndOnOcrAndQb($params);
          }
        } catch (Exception $eDND) {

        }

        $conditions = array('User.id'=> $userId);
        // $data = array(
        //             'User.status'=> $chngeStatus,
        //             //'User.approved'=> $chngeStatus
        //         );
        // echo "<pre>";print_r($userId);
        // echo "<pre>";print_r($deletedStatus);exit();
        if( $this->User->updateAll( $data ,$conditions )){

            if( $chngeStatus == 1 ){ $statusString = 'Active'; }
            if( $chngeStatus == 0 ){ $statusString = 'Inactive'; }

            $logData=$this->getFrontServerInfo();
            $activityData=array('log_info'=>$logData,'user_id'=>$userId,'activity_type'=>'Email Verification','message'=>$statusString,'status'=>1);
            $this->AdminActivityLog->saveAll($activityData);
            $updateCachevalue = $this->updateLastModifiedUserCache($userId, $deletedStatus);

            echo "OK~$statusString";
        }else{
            echo "ERROR~".ERROR_615;
        }
        //** On verify email update cache[START]
        // $getUserDetails = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        // if(($getUserDetails['User']['status'] == 1) && ($getUserDetails['User']['approved'] == 1))
        // {

          // $updateCachevalue = $this->updateLastModifiedUserCache($userId, $deletedStatus);
        // }
        //** On verify email update cache[END]
        exit;
    }
        /*
        On: 05-10-2016
        I/P:
        O/P:
        Desc: Change Account status
        */
        public function changeAccountStatus(){
            $userId = $this->request->data('userId');
            $status = $this->request->data('status');
            $modifiedType = "deleted_user";
            $deletedStatus = 0;
            $chngeStatus = '';
            if( trim($status) == 'Activate' ){
                $chngeStatus = 0;
                //** In case of user deleted deactivate all tokens[START]
                $tokenDeactivate = $this->UserDevice->updateAll(array("status"=> 0), array("user_id"=> $userId));
                //** In case of user deleted deactivate all tokens[END]

                //** In case of deleted user make entry in cache last modified user[START]
                $deletedStatus = 1;
                $updateCachevalue = $this->updateLastModifiedUserCache($userId, $deletedStatus);
                //** In case of deleted user make entry in cache last modified user[END]
                try {
                  $params['user_id'] = $userId;
                  $this->dissmissDndOnOcrAndQb($params);
                } catch (Exception $eDND) {

                }
            }else if( trim($status) == 'Deactivate' ){
                $chngeStatus = 1;
                $updateCachevalue = $this->updateLastModifiedUserCache($userId, $deletedStatus);
            }

            $conditions = array('User.id'=> $userId);
            $data = array(
                        'User.status'=> $chngeStatus,
                        'User.approved'=> $chngeStatus,
                    );
            if( $this->User->updateAll( $data ,$conditions )){

                if( $chngeStatus == 1 ){
                    $statusString = 'Activated';

                    $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
                    // $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'User Approve By Admin')));
                    // if($userDetails['User']['registration_source'] == "MB"){
                    //     $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'MedicBleep User Approve By Admin')));
                    //     $fromMail = array("no-reply@medicbleep.com"=> "Medic Bleep");
                    // }else{
                    //     $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'User Approve By Admin')));
                    //     $fromMail = array(NO_REPLY_EMAIL => "The On Call Room");
                    // }
                    // //** Send email to user for approving user

                    //     $params['temp'] = $templateContent['EmailTemplate']['content'];
                    //     $params['toMail'] = $userDetails['User']['email'];
                    //     // $params['toMail'] = "deepakkumar@mediccreations.com";
                    //     $params['name'] = $userDetails['UserProfile']['first_name'];
                    //     $params['fromMail'] = $fromMail;
                    //     $params['regards'] = "SITE_NAME";
                    //     $params['registration_source'] = $userDetails['User']['registration_source'];
                    //     try{
                    //         $mailSend = $this->AdminEmail->userApproveByAdmin( $params );
                    //         $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
                    //     }catch(Exception $e){
                    //         $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $e->getMessage(), "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
                    //     }
                }
                if( $chngeStatus == 0 ){ $statusString = 'Deactivated'; }
                echo $statusString;
                $logData=$this->getFrontServerInfo();
                $activityData=array('log_info'=>$logData,'user_id'=>$userId,'activity_type'=>'Account Activation','message'=>$statusString,'status'=>1);
                $this->AdminActivityLog->saveAll($activityData);
            }else{
                echo "ERROR~".ERROR_615;
            }
            exit;
        }
    /*
    On:
    I/P:
    O/P:
    Desc:
    */
    public function getModalBox(){
        $userId = $this->request->data['user_id'];
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        if(!empty($userData['UserProfile']['country_id'])){
            $userCountry = $this->Country->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['country_id'])));
            $userData['UserProfile']['country'] = $userCountry['Country']['country_name'];
        }else{
            $userData['UserProfile']['country'] = '';
        }
        //** Get User Interests
        $interestlists = $this->User->query('SELECT user_interests.interest_id,specilities.name FROM user_interests , specilities WHERE user_interests.interest_id = specilities.id and user_interests.user_id = '.$userId.'  and user_interests.status = 1 ORDER by specilities.name ASC');
        $userData['UserProfile']['interestlist'] = $interestlists;
        //** Get User Posted Beats
            $beatCount = $this->UserPost->find("count", array("conditions"=> array("UserPost.user_id"=> $userId, "status"=> 1, "spam_confirmed"=> 0)));
            $userData['UserProfile']['userBeatPosted'] = $beatCount;
        //** Get User Colleagues
            $myColleagues = $this->UserColleague->find("count", array("conditions"=> array("colleague_user_id"=> $userId, "status"=> 1)));
            $meColleagues = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userId, "status"=> 1)));
            $colleagueCount = ($myColleagues + $meColleagues);
            $userData['UserProfile']['userColleagues'] = $colleagueCount;
        //** Get User Followers
            $followersCount = $this->UserFollow->find("count", array("conditions"=> array("followed_to"=> $userId,"follow_type"=> 1, "status"=> 1)));
            $userData['UserProfile']['userFollowers'] = $followersCount;
        //** Get User Followers
            $followingCount = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $userId, "follow_type"=> 1, "status"=> 1)));
            $userData['UserProfile']['userFollowings'] = $followingCount;
        //** Get User Educational Details
            $educationDetails = $this->UserInstitution->getUserInstitutions($userId);
                $userData['UserProfile']['userInstitutions'] = $educationDetails;
        //** Get User Employment Details
            $employmentDetails = $this->UserEmployment->getUserEmployment($userId);
                $userData['UserProfile']['userEmployments'] = $employmentDetails;
        $this->set('userData', $userData);
        echo $this->render('Admin.User/userViewSub');
        exit;
    }
    /*
    On:
    I/P:
    O/P:
    Desc:
    */
    public function clearSearch(){
        $this->Session->delete('searchdata');
        $this->redirect(array('action' => 'userLists'));
    }

    /*
    ---------------------------------------------------------------------------
    On: 21-01-2015
    I/P:
    O/P:
    Desc: Admin Approve/UnApprove user
    ---------------------------------------------------------------------------
    */
    public function changeUserApproveStatus(){
        $userId = $this->request->data('user_id');
        $statusVal = $this->request->data('statusVal');
        if( trim($statusVal) == 'Approved' ){
            $chngeStatus = 0;
            $deletedStatus = 1;
            //** In case of user deleted deactivate all tokens[START]
            $tokenDeactivate = $this->UserDevice->updateAll(array("status"=> 0), array("user_id"=> $userId));
            //** In case of user deleted deactivate all tokens[END]
        }else if( trim($statusVal) == 'Unapproved' ){
            $chngeStatus = 1;
            $deletedStatus = 0;
        }

        $conditions = array('User.id'=> $userId);
        $data = array(
                    'User.approved'=> $chngeStatus
                );

        try {
          if( trim($statusVal) == 'Approved' ){
            $params['user_id'] = $userId;
            $this->dissmissDndOnOcrAndQb($params);
          }
        } catch (Exception $eDND) {

        }

        if( $this->User->updateAll( $data ,$conditions )){
            if( $chngeStatus == 1 ){
                $statusString = 'Approved';
                    $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
                    if($userDetails['User']['registration_source'] == "MB"){
                        $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'MedicBleep User Approve By Admin')));
                        $fromMail = array("no-reply@medicbleep.com"=> "Medic Bleep");
                    }else{
                        $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'User Approve By Admin')));
                        $fromMail = array(NO_REPLY_EMAIL => "The On Call Room");
                    }
                //** Send email to user when admin approve user

                    $params['temp'] = $templateContent['EmailTemplate']['content'];
                    $params['toMail'] = $userDetails['User']['email'];
                    // $params['toMail'] = "deepakkumar@mediccreations.com";
                    $params['name'] = $userDetails['UserProfile']['first_name'];
                    $params['fromMail'] = $fromMail;
                    $params['regards'] = "SITE_NAME";
                    $params['registration_source'] = $userDetails['User']['registration_source'];
                    try{
                        $mailSend = $this->AdminEmail->userApproveByAdmin( $params );
                        $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
                    }catch(Exception $e){
                        $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $e->getMessage(), "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
                    }


            }
            if( $chngeStatus == 0 ){
                $statusString = 'Unapproved';
            }
                echo "OK~$statusString";
                $logData=$this->getFrontServerInfo();
                $activityData=array('log_info'=>$logData,'user_id'=>$userId,'activity_type'=>'Account Approval','message'=>$statusString,'status'=>1);
                $this->AdminActivityLog->saveAll($activityData);

                //** In case of user is verified and approved update cache[START]
                // $getUserDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
                // if(($getUserDetails['User']['status'] == 1) && ($getUserDetails['User']['approved'] == 1))
                // {
                  $updateCachevalue = $this->updateLastModifiedUserCache($userId, $deletedStatus);
                // }
                //** In case of user is verified and approved update cache[END]

        }else{
            echo "ERROR~".ERROR_615;
            exit();
        }
        exit;
    }

    /*

    */
    public function userCsvDownload(){
        $this->layout = null;
        $this->autoLayout = false;
        $sortPost = array();
        $sortPost['textFirstName']=$_REQUEST['FirstName'];
        $sortPost['textLastName']=$_REQUEST['LastName'];
        $sortPost['textEmail']=$_REQUEST['Email'];
        $sortPost['statusVal']=$_REQUEST['statusVal'];
        $sortPost['specilities']=$_REQUEST['specilities'];
        $sortPost['from']=$_REQUEST['from'];
        $sortPost['to']=$_REQUEST['to'];
        $sortPost['country']=$_REQUEST['country'];
        $sortPost['profession']=$_REQUEST['profession'];
        $sortPost['company']=$_REQUEST['company'];
        $sortPost['subscriptionVal']=$_REQUEST['subscriptionVal'];

            if(!isset($sortPost['to']) || $sortPost['to']==""){
              $endDate = date("Y-m-d");
            }
            else{
              $endDate = date('Y-m-d', strtotime($sortPost['to']));
            }
            if(!isset($sortPost['from']) || $sortPost['from']==""){
              $date = array();
            }
            else{
              $startDate = date('Y-m-d', strtotime($sortPost['from']));
              $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
            }
            if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
                $firstName=array();
            }
            else{
                $sortPost['textFirstName']=trim($sortPost['textFirstName']);
                $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textFirstName'].'%'));
            }
            if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
                $lastName=array();
            }
            else{
                $sortPost['textLastName']=trim($sortPost['textLastName']);
                $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower($sortPost['textLastName'].'%'));
            }
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $mail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $mail=array('LOWER(User.email) LIKE'=>strtolower($sortPost['textEmail'].'%'));
            }
            if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
                $interest=array();
            }
            else{
                $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);
            }
            if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
                $status=array('User.status'=>array(0,1));
            }
            else{
                switch($sortPost['statusVal']){
                    case 0:
                        $status=array('User.status'=>array(0,1));
                        break;
                    case 1:
                        $status=array('User.status'=>array(1),'User.approved'=>array(0));
                        break;
                    case 2:
                        $status=array('User.status'=>array(0),'User.approved'=>array(1));
                        break;
                    case 3:
                        $status=array('User.status'=>array(1),'User.approved'=>array(1));
                        break;
                    case 4:
                        $status=array('User.status'=>array(0),'User.approved'=>array(0));
                        break;
                    case 5:
                        $status=array('User.status'=>array(2));
                        break;
                }
             }
             if(!empty($sortPost['country'])){
                 $country=array('UserProfile.country_id'=>$sortPost['country']);
             }
             else{
                $country=array();
             }
             if(!empty($sortPost['profession'])){
                 $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
             }
             else{
                $profession=array();
             }
             if(!empty($sortPost['company'])){
                 $company=array('UserEmployment.company_id'=>$sortPost['company'],'UserEmployment.is_current' => 1);
             }else{
                $company=array();
             }

             if(!isset($sortPost['exccompanies'])|| $sortPost['exccompanies']==""){
                $exccompanies=array();
             }else{
                $exccompanies=array('UserEmployment.company_id !='=>$sortPost['exccompanies'],'UserEmployment.is_current' => 1);
             }
             if(!isset($sortPost['subscriptionVal'])||$sortPost['subscriptionVal']==0){
                 $subscription = array();
             }else{
                 switch($sortPost['subscriptionVal']){
                     case 0:
                         $subscription = array();
                         break;
                     case 1:
                         // $subscription = array('User.status'=>1,'User.approved'=>0);
                         $subscription = array('EnterpriseUserList.status'=>array(0,1));
                         break;
                     case 2:
                         $subscription = array('EnterpriseUserList.status'=>1);
                         break;
                     case 3:
                         $subscription = array('EnterpriseUserList.status'=>0);
                         break;
                     case 4:
                         // $subscription = array('User.status'=>0,'User.approved'=>0);
                         $subscription = array('UserSubscriptionLog.subscribe_type'=>3);
                         break;
                     case 5:
                         $subscription = array('EnterpriseUserList.status'=>2);
                         break;
                     case 6:
                         // $subscription = array('User.status'=>0,'User.approved'=>0);
                         $subscription = array('EnterpriseUserList.status'=>1,'UserSubscriptionLog.subscribe_type != '=>3);
                         break;
                     case 7:
                         $subscription = array('EnterpriseUserList.status'=>0,'UserSubscriptionLog.subscribe_type != '=>3);
                         break;

                 }
              }


            $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$profession, $company,$exccompanies, $subscription);
             // $userDetails=array();
            $fields =array("User.id,User.email,User.activation_key,User.last_loggedin_date,User.login_device,User.registration_date,User.status,User.approved,User.registration_source,UserProfile.first_name,UserProfile.last_name,UserProfile.dob,UserProfile.gmcnumber,UserProfile.county,UserProfile.city,UserProfile.contact_no,UserProfile.contact_no_is_verified,UserProfile.registration_image,UserProfile.address,UserProfile.profile_img,UserProfile.gender,Country.country_name,Profession.profession_type");
            $options  =
            array(
                    'joins'=>array(
                        array(
                          'table' => 'countries',
                          'alias' => 'Country',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.country_id = Country.id')
                      ),
                        array(
                          'table' => 'professions',
                          'alias' => 'Profession',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.profession_id = Profession.id')
                      ),
                        array(
                          'table' => 'user_specilities',
                          'alias' => 'UserSpecility',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserSpecility.user_id')
                      ),
                      array(
                          'table' => 'user_employments',
                          'alias' => 'UserEmployment',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserEmployment.user_id')
                      ),
                      array(
                        'table' => 'enterprise_user_lists',
                        'alias' => 'EnterpriseUserList',
                        'type' => 'left',
                        'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
                      ),
                      array(
                        'table' => 'user_subscription_logs',
                        'alias' => 'UserSubscriptionLog',
                        'type' => 'left',
                        'conditions'=> array('User.id = UserSubscriptionLog.user_id')
                      ),
                    ),
                    'fields'=> $fields,
                    'conditions'=> $conditions,
                    'group'=> array('User.id'),
                    // 'group'=> array('UserEmployment.user_id'),
                    'order'=> 'User.registration_date DESC',
                    // 'limit'=> $limit,
                    );
            // exit();
            $limit = $this->User->find("count", $options);
            $userLists = $this->User->find("all",$options,array('limit'=>$limit));
            foreach ($userLists as  $value) {
                // $specilities_id=$this->UserSpecilities->find("all",array('conditions'=>array('UserSpecilities.user_id='.$value['User']['id'],'UserSpecilities.status'=>'1')));
                // $specilities_id=$this->UserSpecilities->specName($value['User']['id']);
                $devices[]=$this->UserDevice->deviceStatusCount($value['User']['id']);
                // $interest_id=$this->UserInterest->specInterest($value['User']['id']);
                // $interestId[]=$interest_id;
                // $value['interestId']=$interest_id;
                // $specilities[]=$specilities_id;
                // $value['specilities']=$specilities_id;
            }
        // $this->set(array('userLists'=>$userLists,'specilities'=>$specilities));
        $this->set(array('userLists'=>$userLists,'device'=>$devices));
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 16-02-2016
    I/P:
    O/P:
    Desc: User profile view to update
    ------------------------------------------------------------------------------------------
    */
    public function editUserProfileView(){
        if(isset($this->request->data) && !empty($this->request->data)){
            $userId = $this->request->data['user_id'];
            $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
            $this->set('userData', $userData);
        }
        echo $this->render('Admin.User/edit_user_profile_view');
        exit;
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 16-02-2016
    I/P:
    O/P:
    Desc: User profile update
    ------------------------------------------------------------------------------------------
    */
    public function editUserProfile(){ //echo "<pre>";print_r($this->request->data);die;
        //** Update GMC Number
        if(isset($this->request->data['textGmcNumber']) && !empty($this->request->data['textGmcNumber'])){
            $userId = $this->request->data['userId'];
            $profileUpdate = $this->User->updateAll(array('UserProfile.gmcnumber'=> "'". $this->request->data['textGmcNumber'] ."'"), array("UserProfile.user_id"=> $userId));
            if($profileUpdate){
               $msg = "Profile Updated";
            }else{
               $msg = "Profile Not Updated";
            }
        }else{
            $msg = "Some Error! Please Try Again.";
        }
        echo $msg;
        exit;
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 22-02-2016
    I/P:
    O/P:
    Desc: Delete user from list (soft delete)
    ------------------------------------------------------------------------------------------
    */
    public function deleteUser(){
        if(isset($this->request->data['userId'])){
            $updateUser = $this->User->updateAll(array("User.status"=> 2), array("User.id"=> $this->request->data['userId']));
            if($updateUser){
            //** In case of user deleted deactivate all tokens[START]
            $tokenDeactivate = $this->UserDevice->updateAll(array("status"=> 0), array("user_id"=> $this->request->data['userId']));
            //** In case of user deleted deactivate all tokens[END]
            $logData=$this->getFrontServerInfo();
            $activityData=array('log_info'=>$logData,'user_id'=>$this->request->data['userId'],'activity_type'=>'userDeletion','message'=>$this->request->data['reason'],'status'=>1);
            $this->AdminActivityLog->saveAll($activityData);

            //** In case of deleted user make entry in cache last modified user[START]
            $updateCachevalue = $this->updateLastModifiedUserCache($this->request->data['userId'], 1);

            //** In case of deleted user make entry in cache last modified user[END]
                echo "User Deleted";
                try {
                    $params['user_id'] = $this->request->data['userId'];
                    $this->dissmissDndOnOcrAndQb($params);
                } catch (Exception $eDND) {

                }
            }else{
                echo "Some issue occured. Please try again.";
            }
        }else{
            echo "User Not Deleted! Try Again.";
        }
        exit;
    }

    /*
    On: 26-08-2015
    I/P:
    O/P:
    Desc:
    */
    public function userListsForsubadmin(){
        $this->layout = 'subadmin';
        $params = array();
        $conditions = "";
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
              $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
              $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
              $sort="User.email $order";
            }
            else{
            $sort="User.registration_date $order";
            }
        }
        else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        if ($this->request->data) {
            $data = $this->request->data;
            $params['textFirstName'] = $data['textFirstName'];
            $params['textLastName'] = $data['textLastName'];
            $params['textEmail'] = $data['textEmail'];
            $params['statusVal'] = $data['statusVal'];
                if(isset($data['specilities']) && !empty($data['specilities'])){
                    $params['specilities'] = implode(",", $data['specilities']);
                }
            if( isset($params['statusVal']) && $params['statusVal'] !=''){
                if($params['statusVal'] == 1){
                    $activeStatus = 1;
                }elseif($params['statusVal'] == 2){
                    $activeStatus = 0;
                }elseif($params['statusVal'] == 3){
                    $approveStatus = 1;
                }elseif($params['statusVal'] == 4){
                    $approveStatus = 0;
                }
                //** User active status check
                if(isset($activeStatus) && ($activeStatus == 1 || $activeStatus == 0)){
                    if(strlen($conditions) == 0){
                      $conditions .= '  User.status = "'.$activeStatus.'" ';
                    }else{
                      $conditions .= ' AND User.status = "'.$activeStatus.'" ';
                    }
                }
                //** User approve status check
                if(isset($approveStatus) && ($approveStatus == 1 || $approveStatus == 0)){
                    if(strlen($conditions) == 0){
                      $conditions .= '  User.approved = "'.$approveStatus.'" ';
                    }else{
                      $conditions .= ' AND User.approved = "'.$approveStatus.'" ';
                    }
                }
            }
            if( isset($params['textFirstName']) && $params['textFirstName'] !=''){
                if(strlen($conditions) == 0){
                  $conditions .= '  User.status IN (0,1) AND LOWER(UserProfile.first_name) LIKE LOWER("'.$params['textFirstName'].'%") ';
                }else{
                  $conditions .= ' AND User.status IN (0,1) AND LOWER(UserProfile.first_name) LIKE LOWER("'.$params['textFirstName'].'%") ';
                }
            }
            if( isset($params['textLastName']) && $params['textLastName'] !=''){
                if(strlen($conditions) == 0){
                  $conditions .= '  User.status IN (0,1) AND LOWER(UserProfile.last_name) LIKE LOWER("'.$params['textLastName'].'%") ';
                }else{
                  $conditions .= ' AND User.status IN (0,1) AND LOWER(UserProfile.last_name) LIKE LOWER("'.$params['textLastName'].'%") ';
                }
            }
            if( isset($params['textEmail']) && $params['textEmail'] !=''){
                if(strlen($conditions) == 0){
                  $conditions .= '  User.status IN (0,1) AND LOWER(User.email) LIKE LOWER("'.$params['textEmail'].'%") ';
                }else{
                  $conditions .= ' AND User.status IN (0,1) AND LOWER(User.email) LIKE LOWER("'.$params['textEmail'].'%") ';
                }
            }
            if( isset($params['specilities']) && !empty($params['specilities'])){
                if(strlen($conditions) == 0){
                  $conditions .= '  User.status IN (0,1) AND UserSpecility.specilities_id IN ("'.$params['specilities'].'")';
                }else{
                  $conditions .= '  AND User.status IN (0,1) AND UserSpecility.specilities_id IN ("'.$params['specilities'].'")';
                }
            }
        }else{
            $conditions .= " User.status IN (0,1)";
        }
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $interestLists['interestlist'] = $interestlists;
        $this->set('interestLists', $interestLists);
        //** Get User Data

        $options  =
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                ),
                'fields'=> array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county,city, Country.country_name, Profession.profession_type"),
                'conditions'=> $conditions,
                // 'group'=> array('User.id'),
                'order'=> $sort,
                'limit'=> $limit,
                'page'=>$j
                );
                $this->Paginator->settings = $options;
                $users = $this->Paginator->paginate('User');

        $this->set(array('userData'=>$users,'j'=>$j,'limit'=>$limit));
        if($this->request->is("ajax")){
            $this->layout = '';
            $this->render('/Elements/users/user_lists_forsubadmin_ajax');
        }

    }

    /*
    ----------------------------------------------------------------------------------------------
    On:
    I/P:
    O/P:
    Desc:
    ----------------------------------------------------------------------------------------------
    */
    public function userView($id){
        $userId = $id;
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        //** User Country
        if(!empty($userData['UserProfile']['country_id'])){
            $userCountry = $this->Country->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['country_id'])));
            $userData['UserProfile']['countryId'] = $userCountry['Country']['id'];
            $userData['UserProfile']['countryName'] = $userCountry['Country']['country_name'];
        }else{
            $userData['UserProfile']['countryName'] = '';
        }
        //** User Profession
        if(!empty($userData['UserProfile']['profession_id'])){
            $userProfession = $this->Profession->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['profession_id'])));
            $userData['UserProfile']['professionId'] = $userProfession['Profession']['id'];
            $userData['UserProfile']['professionName'] = $userProfession['Profession']['profession_type'];
        }else{
            $userData['UserProfile']['professionName'] = '';
        }
        //** Get User Interests
        $interestlists = array();
        $interestlists = $this->User->query('SELECT GROUP_CONCAT(specilities.name) AS interestLists FROM user_interests , specilities WHERE user_interests.interest_id = specilities.id and user_interests.user_id = '.$userId.'  and user_interests.status = 1 ORDER by specilities.name ASC');
        $userData['UserProfile']['interestlist'] = $interestlists[0][0]['interestLists'];
        //** Get User Specilities
        $interestlists = array();
        $interestlists = $this->User->query('SELECT GROUP_CONCAT(specilities.name) AS specilityLists FROM user_specilities , specilities WHERE user_specilities.specilities_id = specilities.id and user_specilities.user_id = '.$userId.'  and user_specilities.status = 1 ORDER by specilities.name ASC');
        $userData['UserProfile']['specilityLists'] = $interestlists[0][0]['specilityLists'];
        //** Get User Posted Beats
            $beatCount = $this->UserPost->find("count", array("conditions"=> array("UserPost.user_id"=> $userId, "status"=> 1, "spam_confirmed"=> 0)));
            $userData['UserProfile']['userBeatPosted'] = $beatCount;
        //** Get User Colleagues
            $myColleagues = $this->UserColleague->find("count", array("conditions"=> array("colleague_user_id"=> $userId, "status"=> 1)));
            $meColleagues = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userId, "status"=> 1)));
            $colleagueCount = ($myColleagues + $meColleagues);
            $userData['UserProfile']['userColleagues'] = $colleagueCount;
        //** Get User Followers
            // $followersCount = $this->UserFollow->find("count", array("conditions"=> array("followed_to"=> $userId,"follow_type"=> 1, "status"=> 1)));

            $followersCount=$this->UserFollow->followCount( $userId );
        //** Get User Followers
            // $followingCount = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $userId, "follow_type"=> 1, "status"=> 1)));

            $followingCount=$this->UserFollow->followingCount( $userId );

            $userData['UserProfile']['userFollowers'] = $followingCount;
            $userData['UserProfile']['userFollowings'] = $followersCount;
        //** Get User Educational Details
            $userEducation = array();
            $educationDetails = $this->UserInstitution->getUserInstitutions($userId);
            // foreach($educationDetails as $education){
            //     $userEducation[] = $education['EducationDegree']['degree_name'];
            // }
                $userData['UserProfile']['userEducation'] = $educationDetails;
        //** Get User Employment Details
            $employmentDetails = $this->UserEmployment->getUserEmployment($userId);
                $userData['UserProfile']['userEmployments'] = $employmentDetails;
        //** Set User Id
                $userData['User']['id'] = $userId;
        $activityLog=$this->AdminActivityLog->find('all',array('conditions'=>array('AdminActivityLog.user_id'=>$id)));
        $companies=$this->CompanyName->find('all',array('conditions'=>array('status'=>1)));
        $designation=$this->Designation->find('all',array('conditions'=>array('status'=>1)));

        $degree=$this->EducationDegree->find('all',array('conditions'=>array('status'=>1)));
        $institutes=$this->InstituteName->find('all',array('conditions'=>array('status'=>1)));

        // $devices=$this->UserDevice->find('all',array('conditions'=>array('user_id1'=>$id)));
        // var_dump($userId);

        // $devices=$this->UserDevice->deviceStatusCount($userId);

        $devices['androidMB']=$this->UserDevice->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("mb","mb-web"),'LOWER(device_type)'=>array('android'))));
        $devices['iosMB']=$this->UserDevice->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("mb","mb-web"),'LOWER(device_type)'=>array('ios'))));
        $devices['webMB']=$this->UserDevice->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("mb","mb-web"),'LOWER(device_type)'=>array('web','mbweb','mb-web'))));
        $devices['androidOCR']=$this->UserDevice->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("ocr","theocr","the-ocr","the_ocr"),'LOWER(device_type)'=>array('android'))));
        $devices['iosOCR']=$this->UserDevice->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("ocr","theocr","the-ocr","the_ocr"),'LOWER(device_type)'=>array('ios'))));
        $devices['webOCR']=$this->UserDevice->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("ocr","theocr","the-ocr","the_ocr"),'LOWER(device_type)'=>array('web','web-ocr','ocrweb'))));

        // print_r($devices);
        // exit();
        $subjects=$this->AdminEmailCategorie->find('all',array('conditions'=>array('status'=>1)));

        $feedResp=$this->AdminFeedbackResponse->find('all',array('conditions'=>array('email_id'=>$userData['User']['email'])));
        $mailLog=$this->get_admin_user_mail($id);
        // $userDeviceInfo=$this->UserDeviceInfo->find('first',array('conditions'=>array('user_id'=>$userId))); 1342            $userDeviceInfo=$this->UserDeviceInfo->find('first',array('conditions'=>array('user_id'=>$userId)));
        $userDeviceInfo=$this->UserDeviceInfo->find('first',array('conditions'=>array('user_id'=>$userId), "order"=> array("id DESC")));
        $this->set(array('userData'=>$userData,'activityLog'=>$activityLog,'mailLog'=>$mailLog,'comapnies'=>$companies,'designation'=>$designation,'degree'=>$degree,'institutes'=>$institutes,'devices'=>$devices,'subjects'=>$subjects,'feedResp'=>$feedResp, 'userDeviceInfo'=>$userDeviceInfo));
    }

    public function sendMail($id){
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $id)));
        $subjects=$this->AdminEmailCategorie->find('all',array('conditions'=>array('status'=>1)));
        $this->set(array('subjects'=>$subjects,'userData'=>$userData));
    }

    public function templateSelect(){
        if($this->data['type']=="comment"){
            $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$this->data['contentId'])));
            echo $template['AdminEmailTemplate']['content'];
            exit();
        }
        $id=$this->data['id'];
        $templates=$this->AdminEmailTemplate->find('all',array('conditions'=>array('email_category_id'=>$id)));
        $this->set('templates',$templates);
        $this->render('/Elements/users/user_template_select');
    }

    public function templatePreview(){
        if($this->data['type']=="send"){
        $id=$this->data['id'];
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$id),'fields'=>array('title')));
        echo $template['AdminEmailTemplate']['title'];
        exit();
        }
       if($this->data['type']=="view"){
        $id=$this->data['id'];
        $email=$this->data['email'];
        $view=$this->AdminUserSentMail->find('first',array('conditions'=>array('id'=>$id)));
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$view['AdminUserSentMail']['email_template_id']),'fields'=>array('title')));
        $data=array($template['AdminEmailTemplate']['title'],$view['AdminUserSentMail']['content'],$view['AdminUserSentMail']['created_date']);
        $this->set(array('data'=>$data,'email'=>$email));
        $this->render('/Elements/action_mail_preview');
       }
    }

    public function adminToUserMail(){
        $data=$this->data;
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$data['template']),'fields'=>array('title')));
        $params['temp'] = $data['content'];
        $params['subject'] = $template['AdminEmailTemplate']['title'];
        $params['toMail'] = $data['email'];
        // $params['toMail'] = "deepakkumar@mediccreations.com";
        $params['registration_source'] = $data['source'];
        try{
            $mailSend = $this->AdminEmail->sendMailByAdmin( $params );

            $mailData=array('user_id'=>$data['userId'],'email_category_id'=>$data['subject'],'email_template_id'=>$data['template'],'comment'=>$template['AdminEmailTemplate']['title'],'content'=>$data['content'],'status'=>1);
            $this->AdminUserSentMail->saveAll($mailData);

            echo $mailSend;
            exit();
        }catch(Exception $e){
        }
    }

    public function addEmployement(){
        $data=$this->data;
        $from=date("Y-m-d", strtotime($data['from']));
        if($data['to'] == "0000-00-00 00:00:00"){
            $present="1";
            $to=date("Y-m-d", strtotime($data['to']));
            $to=trim($to,'-');
        }
        else{
            $present="0";
            $to=date("Y-m-d", strtotime($data['to']));
        }
        $empData=array('user_id'=>$data['userId'],'company_id'=>$data['orgainsation'],'from_date'=>$from,'to_date'=>$to,'location'=>$data['location'],'description'=>$data['description'],'is_current'=>$present,'designation_id'=>$data['designation'],'status'=>1);
        $this->UserEmployment->saveAll($empData);

        $employmentDetails = $this->UserEmployment->getUserEmployment($data['userId']);
            $userData['UserProfile']['userEmployments'] = $employmentDetails;
        $this->set(array('userData'=>$userData));
        $this->render('/Elements/users/add_employee');
    }

    public function addEducation(){
        $data=$this->data;
        $eduData=array('user_id'=>$data['userId'],'institution_id'=>$data['institution'],'education_degree_id'=>$data['degree'],'description'=>$data['description'],'from_date'=>$data['from'],'to_date'=>$data['to'],'status'=>1);
        $this->UserInstitution->saveAll($eduData);

        $educationDetails = $this->UserInstitution->getUserInstitutions($data['userId']);
        $userData['UserProfile']['userEducation'] = $educationDetails;
        $this->set(array('userData'=>$userData));
        $this->render('/Elements/users/add_education');
    }

    /*
    ---------------------------------------------------------------------------------------------
    On: 15-06-2016
    I/P:
    O/P:
    Desc: Fetches approved user lists
    ---------------------------------------------------------------------------------------------
    */
    public function approvedUserLists(){
        $this->Session->delete('User.approved');
        $this->Session->write('User.approved', "Approved");
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1  GROUP BY profession_type ORDER BY profession_type ASC');
        $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        $interestLists['interestlist'] = $interestlists;
        $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));
        $this->set(array('interestLists'=>$interestLists,'country'=>$country,'professions'=>$profession, "companyNames"=> $companyNames));

        //** Get User Data
        $conditions = array('User.status'=>array(0,1),'User.approved=1');
        $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
        $options  =
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                    array(
                          'table' => 'user_employments',
                          'alias' => 'UserEmployment',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserEmployment.user_id')
                      ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('User.id'),
                'order'=> 'User.registration_date DESC',
                'limit'=> $limit,
                'page'=>$j
                );
        // pr($conditions);
                // $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                $this->Paginator->settings = $options;
                $users = $this->Paginator->paginate('User');

                $total=$this->request->params;
                $tCount=$total['paging']['User']['count'];

            if(isset($users) && !empty($users)){
                foreach($users as $user){
                    $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                    $user['mapped_user'] = $this->get_admin_mapped_user($user['User']['email']);
                    $userS[]=$user;
                }
            }
            else{
              $userS=array();
            }
        $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        // if($this->request->is("ajax")){
        //     $this->render('/Elements/users/user_lists_ajax');
        // }
    }
    public function approvedFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
              $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
              $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
              $sort="User.email $order";
            }
            elseif($con=='country'){
              $sort="Country.country_name $order";
            }
            else{
            $sort="User.registration_date $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
            $sortPost=$this->data;
            if(!isset($sortPost['to']) || $sortPost['to']==""){
              $endDate = date("Y-m-d");
            }else{
              $endDate = date('Y-m-d', strtotime($sortPost['to']));
            }
            if(!isset($sortPost['from']) || $sortPost['from']==""){
              $date = array();
            }else{
              $startDate = date('Y-m-d', strtotime($sortPost['from']));
              $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
            }
            if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
                $firstName=array();
            }
            else{
                $sortPost['textFirstName']=trim($sortPost['textFirstName']);
                $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textFirstName'].'%'));
            }
            if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
                $lastName=array();
            }
            else{
                $sortPost['textLastName']=trim($sortPost['textLastName']);
                $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower($sortPost['textLastName'].'%'));
            }
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $mail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $mail=array('LOWER(User.email) LIKE'=>strtolower($sortPost['textEmail'].'%'));
            }
            if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
                $interest=array();
            }
            else{
                $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);            }
            if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
                $status=array('User.status'=>array(0,1),'User.approved'=>'1');
            }
            else{
                if($sortPost['statusVal']==1){
                  $status=array('User.status'=>'1','User.approved'=>array(0));
                }
                else if($sortPost['statusVal']==2){
                    $status=array('User.status'=>'0','User.approved'=>array(1));
                }
                else if($sortPost['statusVal']==3){
                    $status=array('User.status'=>array(1),'User.approved'=>'1');
                }
                else if($sortPost['statusVal']==4){
                    $status=array('User.status'=>array(0),'User.approved'=>'0');
                }
                else{
                    $status=array('User.status'=>'2');
                }
             }
             if(!empty($sortPost['country'])){
                 $country=array('UserProfile.country_id'=>$sortPost['country']);
             }
             else{
                $country=array();
             }
             if(!empty($sortPost['profession'])){
                 $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
             }
             else{
                $profession=array();
             }
             if(!empty($sortPost['company'])){
                 $company=array('UserEmployment.company_id'=>$sortPost['company'],'UserEmployment.is_current' => 1);
             }else{
                $company=array();
             }
             if(!isset($sortPost['subscriptionVal'])||$sortPost['subscriptionVal']==0){
                 $subscription = array();
             }else{
                 switch($sortPost['subscriptionVal']){
                     case 0:
                         $subscription = array();
                         break;
                     case 1:
                         // $subscription = array('User.status'=>1,'User.approved'=>0);
                         $subscription = array('EnterpriseUserList.status'=>array(0,1));
                         break;
                     case 2:
                         $subscription = array('EnterpriseUserList.status'=>1);
                         break;
                     case 3:
                         $subscription = array('EnterpriseUserList.status'=>0);
                         break;
                     case 4:
                         // $subscription = array('User.status'=>0,'User.approved'=>0);
                         $subscription = array('UserSubscriptionLog.subscribe_type'=>3);
                         break;
                     case 5:
                         $subscription = array('EnterpriseUserList.status'=>2);
                         break;
                     case 6:
                         // $subscription = array('User.status'=>0,'User.approved'=>0);
                         $subscription = array('EnterpriseUserList.status'=>1,'UserSubscriptionLog.subscribe_type != '=>3);
                         break;
                     case 7:
                         $subscription = array('EnterpriseUserList.status'=>0,'UserSubscriptionLog.subscribe_type != '=>3);
                         break;

                 }
              }
             $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$profession, $company,$subscription);
             $fields =array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
            $options  =
            array(
                    'joins'=>array(
                        array(
                          'table' => 'countries',
                          'alias' => 'Country',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.country_id = Country.id')
                      ),
                        array(
                          'table' => 'professions',
                          'alias' => 'Profession',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.profession_id = Profession.id')
                      ),
                        array(
                          'table' => 'user_specilities',
                          'alias' => 'UserSpecility',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserSpecility.user_id')
                      ),
                        array(
                          'table' => 'user_employments',
                          'alias' => 'UserEmployment',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserEmployment.user_id')
                      ),
                      array(
                        'table' => 'enterprise_user_lists',
                        'alias' => 'EnterpriseUserList',
                        'type' => 'left',
                        'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
                      ),
                      array(
                        'table' => 'user_subscription_logs',
                        'alias' => 'UserSubscriptionLog',
                        'type' => 'left',
                        'conditions'=> array('User.id = UserSubscriptionLog.user_id')
                      ),
                    ),
                    'fields'=> $fields,
                    'conditions'=> $conditions,
                    'group'=> array('User.id'),
                    'order'=> $sort,
                    'limit'=> $limit,
                    'page'=>$j
                    );
            //pr($conditions);
                    // $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                    $this->Paginator->settings = $options;
                    $users = $this->Paginator->paginate('User');
                if(isset($users) && !empty($users)){
                    foreach($users as $user){
                        $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                        $user['mapped_user'] = $this->get_admin_mapped_user($user['User']['email']);
                        $userS[]=$user;
                    }
                }
                else{
                  $userS=array();
                }
                $total=$this->request->params;
            $tCount=$total['paging']['User']['count'];
            $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
            $this->render('/Elements/users/user_lists_ajax');

    }
    /*
    ---------------------------------------------------------------------------------------------
    On: 15-06-2016
    I/P:
    O/P:
    Desc: Fetches all pending user lists
    ---------------------------------------------------------------------------------------------
    */
    public function pendingUserLists(){
        $this->Session->delete('User.approved');
        $this->Session->write('User.approved', "Pending");
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1  GROUP BY profession_type ORDER BY profession_type ASC');
        $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        $interestLists['interestlist'] = $interestlists;
        $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));
        $this->set(array('interestLists'=>$interestLists,'country'=>$country,'professions'=>$profession, "companyNames"=> $companyNames));
        //** Get User Data
        $conditions = array('User.status'=>array(0,1),'User.approved=0');
        $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
        $options  =
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                    array(
                          'table' => 'user_employments',
                          'alias' => 'UserEmployment',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserEmployment.user_id')
                      ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('User.id'),
                'order'=> 'User.registration_date DESC',
                'limit'=> $limit,
                'page'=>$j
                );
        // pr($conditions);
                // $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                $this->Paginator->settings = $options;
                $users = $this->Paginator->paginate('User');
                $total=$this->request->params;
                $tCount=$total['paging']['User']['count'];
        if(isset($users) && !empty($users)){
            foreach($users as $user){
                $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                $userS[]=$user;
            }
        }
        else{
          $userS=array();
        }
        $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        // if($this->request->is("ajax")){
        //     $this->render('/Elements/users/user_lists_ajax');
        // }
    }
    public function pendingFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
              $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
              $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
              $sort="User.email $order";
            }
            elseif($con=='country'){
              $sort="Country.country_name $order";
            }
            else{
            $sort="User.registration_date $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
            $sortPost=$this->data;
            if(!isset($sortPost['to']) || $sortPost['to']==""){
              $endDate = date("Y-m-d");
            }
            else{
              $endDate = date('Y-m-d', strtotime($sortPost['to']));
            }
            if(!isset($sortPost['from']) || $sortPost['from']==""){
              $date = array();
            }
            else{
              $startDate = date('Y-m-d', strtotime($sortPost['from']));
              $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
            }
            if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
                $firstName=array();
            }
            else{
                $sortPost['textFirstName']=trim($sortPost['textFirstName']);
                $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textFirstName'].'%'));
            }
            if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
                $lastName=array();
            }
            else{
                $sortPost['textLastName']=trim($sortPost['textLastName']);
                $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower($sortPost['textLastName'].'%'));
            }
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $mail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $mail=array('LOWER(User.email) LIKE'=>strtolower($sortPost['textEmail'].'%'));
            }
            if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
                $interest=array();
            }
            else{
                $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);
            }
            if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
                $status=array('User.status'=>array(0,1),'User.approved'=>'0');
            }
            else{
                if($sortPost['statusVal']==1){
                  $status=array('User.status'=>'1','User.approved'=>array(0));
                }
                else if($sortPost['statusVal']==2){
                    $status=array('User.status'=>'0','User.approved'=>array(1));
                }
                else if($sortPost['statusVal']==3){
                    $status=array('User.status'=>array(1),'User.approved'=>'1');
                }
                else if($sortPost['statusVal']==4){
                    $status=array('User.status'=>array(0),'User.approved'=>'0');
                }
                else{
                    $status=array('User.status'=>'2');
                }
             }
             if(!empty($sortPost['country'])){
                 $country=array('UserProfile.country_id'=>$sortPost['country']);
             }
             else{
                $country=array();
             }
             if(!empty($sortPost['profession'])){
                 $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
             }
             else{
                $profession=array();
             }
             if(!empty($sortPost['company'])){
                 $company=array('UserEmployment.company_id'=>$sortPost['company'],'UserEmployment.is_current' => 1);
             }else{
                $company=array();
             }
             if(!isset($sortPost['subscriptionVal'])||$sortPost['subscriptionVal']==0){
                 $subscription = array();
             }else{
                 switch($sortPost['subscriptionVal']){
                     case 0:
                         $subscription = array();
                         break;
                     case 1:
                         // $subscription = array('User.status'=>1,'User.approved'=>0);
                         $subscription = array('EnterpriseUserList.status'=>array(0,1));
                         break;
                     case 2:
                         $subscription = array('EnterpriseUserList.status'=>1);
                         break;
                     case 3:
                         $subscription = array('EnterpriseUserList.status'=>0);
                         break;
                     case 4:
                         // $subscription = array('User.status'=>0,'User.approved'=>0);
                         $subscription = array('UserSubscriptionLog.subscribe_type'=>3);
                         break;
                     case 5:
                         $subscription = array('EnterpriseUserList.status'=>2);
                         break;
                     case 6:
                         // $subscription = array('User.status'=>0,'User.approved'=>0);
                         $subscription = array('EnterpriseUserList.status'=>1,'UserSubscriptionLog.subscribe_type != '=>3);
                         break;
                     case 7:
                         $subscription = array('EnterpriseUserList.status'=>0,'UserSubscriptionLog.subscribe_type != '=>3);
                         break;

                 }
              }
             $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$profession, $company,$subscription);
             $fields =array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
            $options  =
            array(
                    'joins'=>array(
                        array(
                          'table' => 'countries',
                          'alias' => 'Country',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.country_id = Country.id')
                      ),
                        array(
                          'table' => 'professions',
                          'alias' => 'Profession',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.profession_id = Profession.id')
                      ),
                        array(
                          'table' => 'user_specilities',
                          'alias' => 'UserSpecility',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserSpecility.user_id')
                      ),
                        array(
                          'table' => 'user_employments',
                          'alias' => 'UserEmployment',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserEmployment.user_id')
                      ),
                      array(
                        'table' => 'enterprise_user_lists',
                        'alias' => 'EnterpriseUserList',
                        'type' => 'left',
                        'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
                      ),
                      array(
                        'table' => 'user_subscription_logs',
                        'alias' => 'UserSubscriptionLog',
                        'type' => 'left',
                        'conditions'=> array('User.id = UserSubscriptionLog.user_id')
                      ),
                    ),
                    'fields'=> $fields,
                    'conditions'=> $conditions,
                    'group'=> array('User.id'),
                    'order'=> $sort,
                    'limit'=> $limit,
                    'page'=>$j
                    );
            //pr($conditions);
                    // $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                    $this->Paginator->settings = $options;
                    $users = $this->Paginator->paginate('User');
                if(isset($users) && !empty($users)){
                    foreach($users as $user){
                        $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                        $userS[]=$user;
                    }
                }
                else{
                  $userS=array();
                }
                $total=$this->request->params;
        $tCount=$total['paging']['User']['count'];
            $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
            $this->render('/Elements/users/user_lists_ajax');

    }

    /*
    ----------------------------------------------------------------------------------
    On: 20-06-2016
    I/P:
    O/P:
    Desc: Incomplete User SignUp Email Id
    ----------------------------------------------------------------------------------
    */

    public function incompleteUserLists(){

        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=0;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        //** Get User Data
        $conditions = array('TempRegistration.status'=>array(0,1),'User.status'=>NULL);
        $fields = array("TempRegistration.id, TempRegistration.email,TempRegistration.registration_source,TempRegistration.device_type,TempRegistration.version,TempRegistration.created,TempRegistration.status");
        $options  =
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'LEFT',
                      'conditions'=> array('User.email = TempRegistration.email')
                  )
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('TempRegistration.email'),
                'order'=> 'TempRegistration.created DESC',
                'limit'=> $limit,
                'page'=>$j
                );
            $this->Paginator->settings = $options;
            $users = $this->Paginator->paginate('TempRegistration');
            $total=$this->request->params;
            $tCount=$total['paging']['TempRegistration']['count'];
        $this->set(array("userData"=>$users,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
    }

    public function incompleteUserFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='email'){
              $sort="TempRegistration.email $order";
            }else{
            $sort="TempRegistration.created $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='TempRegistration.created DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=0;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $sortPost=$this->data;
        if(!isset($sortPost['platform'])||$sortPost['platform']==0){
            $platform=array();
        }
        else{
            if($sortPost['platform']==1){
              $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'android');
            }
            else if($sortPost['platform']==2){
                $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'ios');
            }
            else if($sortPost['platform']==3){
                $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'ocrweb');
            }
         }
         if(!isset($sortPost['source'])||$sortPost['source']==0){
            $source=array();
        }
        else{
            if($sortPost['source']==1){
              $source=array('LOWER(TempRegistration.registration_source) LIKE'=>'ocr');
            }
            else if($sortPost['source']==2){
                $source=array('LOWER(TempRegistration.registration_source) LIKE'=>'mb');
            }
         }
         $conditions=array_merge($source,$platform,array('User.status'=>NULL));
         $fields = array("TempRegistration.id, TempRegistration.email,TempRegistration.registration_source,TempRegistration.device_type,TempRegistration.version,TempRegistration.created,TempRegistration.status");
        $options  =
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'LEFT',
                      'conditions'=> array('User.email = TempRegistration.email')
                  )
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('TempRegistration.email'),
                'order'=> $sort,
                'limit'=> $limit,
                'page'=>$j
                );
        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('TempRegistration');
        $total=$this->request->params;
        $tCount=$total['paging']['TempRegistration']['count'];
        $count=count($users);
        $this->set(array("userData"=>$users,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
            $this->render('/Elements/users/incomplete_user_lists_ajax');

    }

    public function incompleteUserExport(){
        $this->layout = null;
        $this->autoLayout = false;
        $sortPost['source']=$_REQUEST['source'];
        $sortPost['platform']=$_REQUEST['platform'];
        if(!isset($sortPost['platform'])||$sortPost['platform']==0){
            $platform=array();
        }
        else{
            if($sortPost['platform']==1){
              $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'android');
            }
            else if($sortPost['platform']==2){
                $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'ios');
            }
            else if($sortPost['platform']==3){
                $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'ocrweb');
            }
         }
         if(!isset($sortPost['source'])||$sortPost['source']==0){
            $source=array();
        }
        else{
            if($sortPost['source']==1){
              $source=array('LOWER(TempRegistration.registration_source) LIKE'=>'ocr');
            }
            else if($sortPost['source']==2){
                $source=array('LOWER(TempRegistration.registration_source) LIKE'=>'mb');
            }
         }
         $conditions=array_merge($source,$platform,array('User.status'=>NULL));
         $fields = array("TempRegistration.id, TempRegistration.email,TempRegistration.registration_source,TempRegistration.device_type,TempRegistration.version,TempRegistration.created,TempRegistration.status");
        $options  =
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'LEFT',
                      'conditions'=> array('User.email = TempRegistration.email')
                  )
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('TempRegistration.email'),
                'order'=> 'TempRegistration.created DESC',
                );
            $limit = $this->TempRegistration->find("count", $options);
            $userLists = $this->TempRegistration->find("all",$options,array('limit'=>$limit));
            $this->set(array('userLists'=>$userLists));

    }

    /*
    ----------------------------------------------------------------------------------
    On: 20-06-2016
    I/P:
    O/P:
    Desc: Edit user by admin
    ----------------------------------------------------------------------------------
    */
    public function editUser(){
      $userId = $this->request->params['pass'][0];//$this->request->data['user_id'];
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        //** User Country
        if(!empty($userData['UserProfile']['country_id'])){
            $userCountry = $this->Country->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['country_id'])));
            $userData['UserProfile']['countryId'] = $userCountry['Country']['id'];
            $userData['UserProfile']['countryName'] = $userCountry['Country']['country_name'];
        }else{
            $userData['UserProfile']['countryName'] = '';
        }
        //** User Profession
        if(!empty($userData['UserProfile']['profession_id'])){
            $userProfession = $this->Profession->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['profession_id'])));
            $userData['UserProfile']['professionId'] = $userProfession['Profession']['id'];
            $userData['UserProfile']['professionName'] = $userProfession['Profession']['profession_type'];
        }else{
            $userData['UserProfile']['professionName'] = '';
        }
        //** Get User Interests
        $interestlistsData = array();
        $interestlists = $this->User->query('SELECT specilities.id AS userinterest FROM user_interests , specilities WHERE user_interests.interest_id = specilities.id and user_interests.user_id = '.$userId.'  and user_interests.status = 1 ORDER by specilities.name ASC');
        foreach($interestlists as $userInterest){
            $interestlistsData[] = $userInterest['specilities']['userinterest'];
        }
        $userData['UserProfile']['interestlist'] = $interestlistsData;
        //** Get User Specilities
        $specilitylistsData = array();
        $specilitiesList = $this->User->query('SELECT specilities.id AS userspecility FROM user_specilities , specilities WHERE user_specilities.specilities_id = specilities.id and user_specilities.user_id = '.$userId.'  and user_specilities.status = 1 ORDER by specilities.name ASC');
        foreach($specilitiesList as $userSpecility){
            $specilitylistsData[] = $userSpecility['specilities']['userspecility'];
        }
        $userData['UserProfile']['specilityLists'] = $specilitylistsData;
        //** Get User Posted Beats
            $beatCount = $this->UserPost->find("count", array("conditions"=> array("UserPost.user_id"=> $userId, "status"=> 1, "spam_confirmed"=> 0)));
            $userData['UserProfile']['userBeatPosted'] = $beatCount;
        //** Get User Colleagues
            $myColleagues = $this->UserColleague->find("count", array("conditions"=> array("colleague_user_id"=> $userId, "status"=> 1)));
            $meColleagues = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userId, "status"=> 1)));
            $colleagueCount = ($myColleagues + $meColleagues);
            $userData['UserProfile']['userColleagues'] = $colleagueCount;
        //** Get User Followers
            $followersCount = $this->UserFollow->find("count", array("conditions"=> array("followed_to"=> $userId,"follow_type"=> 1, "status"=> 1)));
            $userData['UserProfile']['userFollowers'] = $followersCount;
        //** Get User Followers
            $followingCount = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $userId, "follow_type"=> 1, "status"=> 1)));
            $userData['UserProfile']['userFollowings'] = $followingCount;
        //** Get User Educational Details
            $educationDetails = $this->UserInstitution->getUserInstitutions($userId);
                $userData['UserProfile']['userInstitutions'] = $educationDetails;
        //** Get User Employment Details
            $employmentDetails = $this->UserEmployment->getUserEmployment($userId);
                $userData['UserProfile']['userEmployments'] = $employmentDetails;
        //** Get All Countries lists
                $countryList = $this->Country->find("all", array("fields"=> array("id","country_name")));
                foreach($countryList as $countryL){
                    $countryData[] = array("id"=> $countryL['Country']['id'], "name"=> $countryL['Country']['country_name']);
                }
              $userData['UserProfile']['countryList'] =  $countryData;
        //** Get All Profession lists
                $professionList = $this->Profession->find("all", array("fields"=> array("id","profession_type"), "conditions"=> array("status"=> 1)));
                foreach($professionList as $professionL){
                    $professionData[] = array("id"=> $professionL['Profession']['id'], "name"=> $professionL['Profession']['profession_type']);
                }
              $userData['UserProfile']['professionList'] =  $professionData;
        //** Get All Interests lists
                $interestMasterList = $this->Specilities->find("all", array("fields"=> array("id","name"), "conditions"=> array("status"=> 1)));
                foreach($interestMasterList as $interestL){
                    $interestMasterListData[] = array("id"=> $interestL['Specilities']['id'], "name"=> $interestL['Specilities']['name']);
                }
              $userData['UserProfile']['interestMasterList'] =  $interestMasterListData;
        //** Set userid
              $userData['UserProfile']['userId'] = $userId;
        $this->set('userData', $userData);
    }

    /*
    -----------------------------------------------------------------------------------
    On:
    I/P:
    O/P:
    Desc
    -----------------------------------------------------------------------------------
    */
    public function editUserProfileAccount(){
        if(isset($this->request->data['userId']) && !empty($this->request->data['userId'])){
            $userId = $this->request->data['userId'];
            $firstName = isset($this->request->data['firstName']) ? $this->request->data['firstName'] : '';
            $lastName = isset($this->request->data['lastName']) ? $this->request->data['lastName'] : '';
            $contactNumber = isset($this->request->data['contactNumber']) ? $this->request->data['contactNumber'] : '';
            $dob = isset($this->request->data['dob']) ? $this->request->data['dob'] : '0000-00-00';
            $joiningDate = isset($this->request->data['joiningDate']) ? $this->request->data['joiningDate'] : '';
            $gmcNumber = isset($this->request->data['gmcNumber']) ? $this->request->data['gmcNumber'] : '';
            $userStatus = isset($this->request->data['userStatus']) ? $this->request->data['userStatus'] : 0;
            $address = isset($this->request->data['address']) ? $this->request->data['address'] : '';
            $city = isset($this->request->data['city']) ? $this->request->data['city'] : '';
            $county = isset($this->request->data['county']) ? $this->request->data['county'] :'';
            $country = isset($this->request->data['countryList']) ? $this->request->data['countryList'] : '';
            $education = isset($this->request->data['education']) ? $this->request->data['education'] : '';
            $userProfession = isset($this->request->data['userProfession']) ? $this->request->data['userProfession'] : 0;
            $interests = isset($this->request->data['interests']) ? $this->request->data['interests'] : array();
            $specilities = isset($this->request->data['specilities']) ? $this->request->data['specilities'] : array();
            $userData = array(
                                "UserProfile.first_name"=> "'". $firstName ."'",
                                "UserProfile.last_name"=> "'". $lastName ."'",
                                "UserProfile.contact_no"=> "'". $contactNumber ."'",
                                "UserProfile.dob"=> "'". date('Y-m-d', strtotime($dob)) ."'",
                                "User.registration_date"=> "'". date('Y-m-d H:i:s', strtotime($joiningDate)) ."'",
                                "UserProfile.gmcnumber"=> "'". $gmcNumber ."'",
                                "User.status"=> $userStatus,
                                "UserProfile.address"=> "'". $address ."'",
                                "UserProfile.city"=> "'". $city ."'",
                                "UserProfile.county"=> "'". $county ."'",
                                "UserProfile.country_id"=> $country,
                                "UserProfile.profession_id"=> $userProfession,
                            );
            $updateConditions = array("User.id"=> $userId);
            $updateUser = $this->User->updateAll($userData, $updateConditions);
            if($updateUser){
                $msg = "User Updated.";
            }else{
                $msg = "Something going wrong! Try Again.";
            }
            //** Edit user specilities [START]
                if(!empty($specilities)){
                    //** At first unmark all specilities of user
                    $updateData = $this->UserSpecilities->updateAll( array('status'=> 0), array('user_id'=> $userId));
                    //** Update One by One specility
                    foreach($specilities as $specilitiesId){
                        $specilitiesData = array('user_id'=> $userId , 'specilities_id'=> $specilitiesId);
                        if( $this->UserSpecilities->find("count", array("conditions"=> array(array('user_id'=> $userId, 'specilities_id'=> $specilitiesId)))) == 0 ){
                            $saveData = $this->UserSpecilities->saveAll( $specilitiesData );
                        }else{
                            $updateData = $this->UserSpecilities->updateAll( array('status'=> 1), array('user_id'=> $userId, 'specilities_id'=> $specilitiesId) );
                        }
                    }
                }
            //** Edit user specilities [END]

            //** Edit user interests [START]
            if(!empty($interests)){
                //** update all Interests
                        $this->UserInterest->updateAll(array("status"=>0), array('user_id'=> $userId));
                        //** add or update interest one by one according to request
                        foreach( $interests as $interestId ){  //echo "<pre>";print_r($interestId);die;
                            $interestData = array('user_id'=> $userId,'interest_id'=> $interestId, 'status'=> 1 );
                            if($this->UserInterest->find("count", array("conditions"=>array('user_id'=> $userId,'interest_id'=> $interestId))) ==0 ){
                                $this->UserInterest->saveAll( $interestData );
                            }else{
                                $this->UserInterest->updateAll(array("status"=> 1), array('user_id'=> $userId,'interest_id'=> $interestId));
                            }
                        }
                }
            //** Edit user interests [END]
        }else{
            $msg = "User Not Found!";
        }
        echo $msg;
        exit;
    }
    /*
    ----------------------------------------------------------------------------------------
    On:
    I/P:
    O/P:
    Desc: Reset any user's password
    ----------------------------------------------------------------------------------------
    */
    public function resetUserPassword(){
        if(isset($this->request->data['userId']) && !empty($this->request->data['userId'])){
            $userId = $this->request->data['userId'];
            $currentPassword = $this->request->data['currentPassword'];
            $newPassword = $this->request->data['newPassword'];
            $getUserDetails = $this->User->findById($userId);
            if($getUserDetails['User']['password'] == md5($currentPassword)){
                $updateData = array(
                        "User.password"=> "'". md5($newPassword) ."'"
                    );
                $updateDataConditions = array(
                        "User.id"=> $userId
                    );
                $resetPassword = $this->User->updateAll($updateData, $updateDataConditions);
                if($resetPassword){
                    $msg = "Password Updated";
                }else{
                   $msg = "Some error occured! Try again";
                }
            }else{
                $msg = "Current Password not match";
            }
        }else{
            $msg = "Userid Blank";
        }
        echo $msg;
        exit;
    }

    /*
    ---------------------------------------------------------------------------------------
    On:
    I/P:
    O/P:
    Desc:
    ---------------------------------------------------------------------------------------
    */
    public function updateUserProfileImage(){
        $userId = $this->request->data['hiddenUserId'];
        //echo "<pre>"; print_r($_FILES);
        $fileBaseName = basename($_FILES['profileImg']['name']);
        $imgNameArr = explode('.', $fileBaseName);
        $imgExt = end($imgNameArr);
        $imageName = md5($userId .'_' . strtotime(date('Y-m-d H:i:s')) ) . '.' . $imgExt;
        $tempFileDestination = WWW_ROOT . 'img/uploader_tmp/';
        $uploadFileToTemp = $tempFileDestination . $imageName;
        //** Move file to temp destination
        if (move_uploaded_file($_FILES['profileImg']['tmp_name'], $uploadFileToTemp)) {
            if( !empty($imageName) ){
                $this->Image->moveImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$imageName, 'img_name'=> $userId . '/profile/' .$imageName));
            }
            //** Update Image name on table
            $profileData = array("UserProfile.profile_img"=> "'".$imageName."'");
            $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $userId));
            //** Remove physical file from Temp Directory
            $this->Common->removeTempFile( WWW_ROOT . 'img/uploader_tmp/' . $imageName );
        } else {
            $msg = "Some error occured! Try Again.";
        }
        $this->redirect('editUser/' . $userId);
    }

     /*
    ---------------------------------------------------------------------------------------
    On: 08-02-2017
    I/P:
    O/P:
    Desc: Lists all users who has livestream beat post permission
    ---------------------------------------------------------------------------------------
    */
    public function liveStreamAccessUsers(){
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1   ORDER BY profession_type ASC');
        $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        $interestLists['interestlist'] = $interestlists;
        $this->set(array('interestLists'=>$interestLists,'country'=>$country,'professions'=>$profession));
        //** Get User Data
        $conditions = array('User.status'=>array(0,1));
        $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type, lbpau.status");
        $options  =
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                    array(
                      'table' => 'livestream_beat_post_access_users',
                      'alias' => 'lbpau',
                      'type' => 'left',
                      'conditions'=> array('User.id = lbpau.user_id')
                  ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('User.id'),
                'order'=> 'User.registration_date DESC',
                'limit'=> $limit,
                'page'=>$j
                );
        // pr($conditions);
                $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                $this->Paginator->settings = $options;
                $users = $this->Paginator->paginate('User');
        if(isset($users) && !empty($users)){
            foreach($users as $user){
                $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                $userS[]=$user;
            }
        }
        else{
          $userS=array();
        }
        $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));

    }

    /*
    ---------------------------------------------------------------------------------------
    On: 08-02-2017
    I/P:
    O/P:
    Desc: Add access to user for livestream beat post
    ---------------------------------------------------------------------------------------
    */
    public function addLiveStreamAccess(){
        if(isset($this->request->data['userId'])){
          $userId = $this->request->data['userId'];
          if($this->LivestreamBeatPostAccessUser->find("count", array("conditions"=>array("user_id"=> $userId))) > 0){
            $updateUserAccess = $this->LivestreamBeatPostAccessUser->updateAll(array("status"=> 1), array("user_id"=> $userId));
            if($updateUserAccess){
              echo "Permission Given";
            }else{
                echo "Some error! Try Again.";
            }
          }else{
            $addAcessUser = $this->LivestreamBeatPostAccessUser->save(array("user_id"=> $userId, "status"=> 1));
            if($addAcessUser){
                echo "Permission Given";
            }else{
                echo "Some error! Try Again.";
            }
          }
        }else{
            echo "User Not Found!";
        }
        exit;
    }

    /*
    ---------------------------------------------------------------------------------------
    On: 08-02-2017
    I/P:
    O/P:
    Desc: Remove access to user for livestream beat post
    ---------------------------------------------------------------------------------------
    */
    public function removeLiveStreamAccess(){
        if(isset($this->request->data['userId'])){
          $userId = $this->request->data['userId'];
          if($this->LivestreamBeatPostAccessUser->find("count", array("conditions"=>array("user_id"=> $userId))) > 0){
            $updateUserAccess = $this->LivestreamBeatPostAccessUser->updateAll(array("status"=> 0), array("user_id"=> $userId));
            if($updateUserAccess){
              echo "Permission Removed";
            }else{
                echo "Some error! Try Again.";
            }
          }
        }else{
            echo "User Not Found!";
        }
        exit;
    }
    /*
    ---------------------------------------------------------------------------------------
    On: 27-03-2017
    I/P:
    O/P:
    Desc: get user front end info
    ---------------------------------------------------------------------------------------
    */
    public function getFrontServerInfo(){
        $userServerInfo = json_encode($_SERVER);
        return $userServerInfo;
    }

    /*
    ---------------------------------------------------------------------------------------
    On: 01-09-2017
    I/P:
    O/P:
    Desc: Mapping by admin
    ---------------------------------------------------------------------------------------
    */

    public function mapUser()
    {
        $userId = $this->request->params['pass'][0];//$this->request->data['user_id'];
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        $companyDetail = $this->CompanyName->find('all', array('conditions'=> array('CompanyName.is_subscribed'=> 1, 'CompanyName.status'=> 1, 'CompanyName.created_by'=> 0)));
        $this->set(array("userData"=>$userData,"companyDetail"=>$companyDetail));
    }


    /*
    ---------------------------------------------------------------------------------------
    On: 01-09-2017
    I/P:
    O/P:
    Desc: add Mapping by admin
    ---------------------------------------------------------------------------------------
    */

    public function addUserMapping()
    {
        //print_r($this->request->data);exit();
        $email = $this->request->data['User']['email'];
        $companyId = $this->request->data['User']['company_id'];
        $userName = $this->request->data['User']['username'];
        // $status = $this->request->data['User']['status'];
        $role_id = $this->request->data['User']['status'];
        $status = '1';
        if((string)$role_id == '2'){
            $status = '0';
            $role_id = '0';
        }

        $adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $userName)));
        if(empty($adminUserData)){
            $data = array(
                'username' => $userName,
                'password' => md5(rand()),
                'email'=> $email,
                'status'=> $status,
                'role_id'=> $role_id,
                'company_id'=> $companyId,
                'added_date' => date("Y-m-d H:i:s")
            );
            $this->AdminUser->saveAll($data);
            $this->roleUpdationMail($email,$role_id,$companyId);
        }
        $this->redirect(array('action' => 'userLists'));

    }

    /*
    ---------------------------------------------------------------------------------------
    On: 01-09-2017
    I/P:
    O/P:
    Desc: add Mapping by admin
    ---------------------------------------------------------------------------------------
    */

    public function editUserMapping()
    {
        $email = $this->request->params['pass'][0];
        $adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.username' => $email)));
        $companyDetail = $this->CompanyName->find('all', array('conditions'=> array('CompanyName.is_subscribed'=> 1, 'CompanyName.status'=> 1, 'CompanyName.created_by'=> 0)));
        $companyByuserId = $this->CompanyName->find('first', array('conditions'=> array('CompanyName.id'=> $adminUserData['AdminUser']['company_id'])));
        //print_r($companyByuserId);exit();
        $this->set(array("adminUserData"=>$adminUserData,"companyDetail"=>$companyDetail, "companyByuserId" => $companyByuserId));
    }

    /*
    ---------------------------------------------------------------------------------------
    On: 01-09-2017
    I/P:
    O/P:
    Desc: add Mapping by admin
    ---------------------------------------------------------------------------------------
    */

    public function updateUserMapping(){
        $companyId = $this->request->data['User']['company_id'];
        $role_id = $this->request->data['User']['status'];
        $status = '1';
        if((string)$role_id == '2'){
            $status = '0';
            $updateData = array(
                                "company_id"=> $companyId,
                                "status"=> $status,
                            );
        }else{
          $updateData = array(
                              "company_id"=> $companyId,
                              "status"=> $status,
                              'role_id'=> $role_id
                          );
        }
        $updateConditions = array("username"=> $this->request->data['User']['email']);
        $updateAdminUserMapping = $this->AdminUser->updateAll($updateData, $updateConditions);
        $this->roleUpdationMail($this->request->data['User']['email'],$role_id,$companyId);
        $this->redirect(array('action' => 'userLists'));
    }

    /*
    ---------------------------------------------------------------------------------------
    On: 03-10-2017
    I/P:
    O/P:
    Desc: Send mail on user mapping
    ---------------------------------------------------------------------------------------
    */

    public function roleUpdationMail($email,$role,$companyId){
    		$role = (string)$role;
    		if($role == '2'){
    			$adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $email)));
    			$role_name = $adminUserData['AdminUser']['role_id'] == '0'?'Admin':'Subadmin';
    			$status = 0;
    		}else{
    			$role_name = $role == '0'?'Admin':'Subadmin';
    			$status = 1;
    		}
    		try {
    			$userData_cust = $this->User->find('first',array('conditions'=>array('email'=>$email)));
    			$company_data = $this->CompanyName->find('first',array('conditions'=>array('CompanyName.id'=>$companyId)));

    			$params['trust_name'] = "'".$company_data['CompanyName']['company_name']."'";
    			$params['status'] = $status;
    			$params['f_name'] = $userData_cust['UserProfile']['first_name'];
    			$params['role'] = $role_name;
    			$params['toMail'] = $email;


    			$mailSend = $this->AdminEmail->assignRole( $params );
    		} catch (Exception $e) {}
    	}


    /*
    ---------------------------------------------------------------------------------------
    On: 03-10-2017
    I/P:
    O/P:
    Desc: get mapped user from admin user
    ---------------------------------------------------------------------------------------
    */
    public function get_admin_mapped_user($email){
        $conditions = array('AdminUser.email'=> $email);
        $mappedUser=$this->AdminUser->find('count',array('conditions'=>$conditions));
        return $mappedUser;
    }

    /*
    ---------------------------------------------------------------------------------------
    On: 17-11-2017
    I/P: $userId
    O/P:
    Desc: update Last modified user cache
    ---------------------------------------------------------------------------------------
    */
    // public function updateLastModifiedUserCache($userId, $deletedStatus)
    // {
    //     $modifiedType = "deleted_user";
    //     $lastModified = date('Y-m-d H:i:s');
    //     $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
    //     $userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $userId, "is_current"=> 1), "order"=> array("id DESC")));
    //     if(!empty($userCurrentCompany)){
    //         $companyId = $userCurrentCompany['UserEmployment']['company_id'];
    //         $userEmail = $userDetails['User']['email'];
    //         App::import('model','User');
    //         $User = new User();
    //         $userEnterpriseCheck = $User->query("SELECT * FROM enterprise_user_lists WHERE email='$userEmail' AND company_id = $companyId AND status =1");
    //         if(!empty($userEnterpriseCheck)){
    //             $getCacheLastModifiedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType)));
    //             if($getCacheLastModifiedUser > 0)
    //             {
    //                 $this->CacheLastModifiedUser->updateDeletedUser($userId, $companyId, $modifiedType, $deletedStatus);
    //             }
    //             else
    //             {
    //                 $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType, "status"=>"1", "last_modified"=>$lastModified);
    //                 $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
    //             }

    //             //Update cache for dynamic user //
    //             if($deletedStatus == 0)
    //             {
    //               $dynamicModifiedType = "dynamic_directory_changed";
    //               $getDynamicCacheData = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$dynamicModifiedType)));

    //               if(count($getDynamicCacheData) > 0)
    //               {
    //                 $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, $dynamicModifiedType);
    //               }
    //               else
    //               {
    //                 $saveDynamicCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$dynamicModifiedType, "status"=>"1", "last_modified"=>$lastModified);
    //                 $this->CacheLastModifiedUser->saveAll($saveDynamicCacheLastModifiedData);
    //               }
    //             }

    //             //** Update static cache[START]
    //             $this->CacheLastModifiedUser->updateLastModifiedDate(0, $companyId, 'static_directory_changed');
    //             //** Update static cache[END]

    //             //******* Delete Cache in case of Delete, Activate, Deactivate[START] ********//
    //             $cacObj = $this->Cache->redisConn();
    //             if(($cacObj['connection']) && empty($cacObj['errors'])){
    //                 if($cacObj['robj']->exists('institutionDirectory:'.$companyId))
    //                 {
    //                     $key = 'institutionDirectory:'.$companyId;
    //                     $cacObj = $this->Cache->delRedisKey($key);
    //                 }
    //             }
    //             //******* Delete Cache in case of Delete, Activate, Deactivate[END] ********//
    //     }
    //   }
    // }

    public function updateLastModifiedUserCache($userId, $deletedStatus)
    {
        $dataParams = array();
        $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
        $userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $userId, "is_current"=> 1), "order"=> array("id DESC")));
        //echo "";print_r($userCurrentCompany);exit();
        $dataParams['company_id'] = isset($userCurrentCompany['UserEmployment']['company_id']) ? $userCurrentCompany['UserEmployment']['company_id'] : 0;
        $dataParams['email'] = stripslashes($userDetails['User']['email']);
        $dataParams['user_id'] = isset($userId) ? $userId: 0;
        $dataParams['deleted_status'] = isset($deletedStatus) ? $deletedStatus: 0;
         //echo "<pre>";print_r($dataParams);exit();
        $checkCompanySubscription = $this->CompanyName->getComapanySubscriptionDetails($dataParams);
        //echo "<pre>";print_r($checkCompanySubscription);exit();
        if($checkCompanySubscription > 0)
        {
          //echo "TEST";exit();
          $checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);
          if(!empty($checkUserForPaid))
          {
            //***** Static cache update And remove cache **********//
            //echo "TEST!!";exit();
            $updateStaticCache = $this->updateCache($dataParams);
          }
          // else
          // {
          //   echo "TEST!!!";exit();
          // }
        }
        else
        {
           //echo "TEST!";exit();
          $updateStaticCache = $this->updateCache($dataParams);
          //echo "<pre>";print_r($updateStaticCache);exit();
        }
    }

    public function updateCache($dataParams=array())
    {
      $companyId = $dataParams['company_id'];
      $userId = $dataParams['user_id'];
      $lastModified = date('Y-m-d H:i:s');
      $modifiedType = "deleted_user";
      $deletedStatus = $dataParams['deleted_status'];
      //echo "<pre>";print_r($deletedStatus);exit();
      if(! empty($dataParams))
      {

        $getCacheLastModifiedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType)));
        //echo "<pre>";print_r($getCacheLastModifiedUser);exit();
        if($getCacheLastModifiedUser > 0)
        {
            $this->CacheLastModifiedUser->updateDeletedUser($userId, $companyId, $modifiedType, $deletedStatus);
        }
        else
        {
            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType, "status"=>$deletedStatus, "last_modified"=>$lastModified);
            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
            //** here "status"=>"1" cahnged to $deletedStatus

        }

        //Update cache for dynamic user //
        if($deletedStatus == 0)
        {
          $dynamicModifiedType = "dynamic_directory_changed";
          $getDynamicCacheData = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$dynamicModifiedType)));

          if(count($getDynamicCacheData) > 0)
          {
            $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, $dynamicModifiedType);
          }
          else
          {
            $saveDynamicCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$dynamicModifiedType, "status"=>"1", "last_modified"=>$lastModified);
            $this->CacheLastModifiedUser->saveAll($saveDynamicCacheLastModifiedData);
          }
        }

        //** Get last modified date dynamic directory[START]
        $getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
        if(!empty($getDynamicDirectoryLastModified)){
            $paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
            $paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
            $this->updateDynamicEtagData($paramsDynamicEtagUpdate);
        }
        //** Get last modified date dynamic directory[END]
        //** Update static cache[START]
        $this->CacheLastModifiedUser->updateLastModifiedDate(0, $companyId, 'static_directory_changed');
        //** Update static cache[END]

        //** Set eTag values for static directory[START]
        $paramsStaticEtagUpdate['key'] = 'staticDirectoryEtag:'.$companyId;
        $this->updateStaticEtagData($paramsStaticEtagUpdate);
        //** Set eTag values for static directory[END]

        //******* Delete Cache in case of Delete, Activate, Deactivate[START] ********//
        $cacObj = $this->Cache->redisConn();
        if(($cacObj['connection']) && empty($cacObj['errors'])){
            if($cacObj['robj']->exists('institutionDirectory:'.$companyId))
            {
                $key = 'institutionDirectory:'.$companyId;
                //echo "<pre>";print_r($key);exit();
                $cacObj = $this->Cache->delRedisKey($key);
            }
        }
      }
    }

  /*
  --------------------------------------------------------------------
  On:
  I/P:
  O/P:
  Desc:
  --------------------------------------------------------------------
  */
  public function updateStaticEtagData($params = array()){
    $cacObjEtag = $this->Cache->redisConn();
    if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
        $etagLastModified = strtotime(date('Y-m-d H:i:s'));
        $eTagNewVal = md5($params['key'] . rand(1,10000) . $etagLastModified);
        $cacObjEtag['robj']->hset($params['key'],'eTag' , $eTagNewVal);
        $cacObjEtag['robj']->hset($params['key'],'lastmodified' , $etagLastModified);
    }
  }

  /*
  --------------------------------------------------------------------
  On:
  I/P:
  O/P:
  Desc:
  --------------------------------------------------------------------
  */
  public function updateDynamicEtagData($params = array()){
    $cacObjEtag = $this->Cache->redisConn();
    if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
        $etagLastModified = strtotime($params['lastmodified']);
        $eTagNewVal = md5($params['key'] . rand(1,10000) . $etagLastModified);
        $cacObjEtag['robj']->hset($params['key'],'eTag' , $eTagNewVal);
        $cacObjEtag['robj']->hset($params['key'],'lastmodified' , $etagLastModified);
    }
  }



  /*
  --------------------------------------------------------------------
  On: 22-06-2018
  I/P:
  O/P:
  Desc: User availble and oncall transaction records
  --------------------------------------------------------------------
  */

  public function userAvailableOncallData()
  {

      // echo "<pre>";print_r("expression");exit();
      // $tCount=$this->AvailableAndOncallTransaction->find('all');
      // echo "<pre>";print_r($tCount);exit();
      $params = array();
      if(isset($this->data['j'])){
          $j=$this->data['j'];
      }
      else{
          $j=1;
      }
      if(isset($this->data['limit'])){
          $limit=$this->data['limit'];
      }
      else{
          $limit= ADMIN_PAGINATION;
      }
      $fields = array("User.email, UserProfile.first_name, UserProfile.last_name,CompanyName.company_name,AvailableAndOncallTransaction.type,AvailableAndOncallTransaction.status,AvailableAndOncallTransaction.device_type,AvailableAndOncallTransaction.platform,AvailableAndOncallTransaction.created");

      $options  =
      array(
              'joins'=>array(
                  array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'left',
                    'conditions'=> array('User.id = AvailableAndOncallTransaction.user_id')
                ),
                array(
                    'table' => 'company_names',
                    'alias' => 'CompanyName',
                    'type' => 'left',
                    'conditions'=> array('AvailableAndOncallTransaction.company_id = CompanyName.id')
                ),
                array(
                    'table' => 'user_profiles',
                    'alias' => 'UserProfile',
                    'type' => 'left',
                    'conditions'=> array('AvailableAndOncallTransaction.user_id = UserProfile.user_id')
                ),
              ),
              'fields'=> $fields,
              'order'=> 'AvailableAndOncallTransaction.created DESC',
              'limit'=> $limit,
              'page'=>$j
              );
      $tCount=$this->AvailableAndOncallTransaction->find('count',array('fields'=>$fields));

      $this->Paginator->settings = $options;
      $data = $this->Paginator->paginate('AvailableAndOncallTransaction');
      $this->set(array("availableAndOncallTransactionData"=>$data,"limit"=>$limit,'tCount'=>$tCount));
  }

  /*
    On: 22-06-2018
    I/P:
    O/P:
    Desc: get filtered data availableAndOncall
    */

    public function availableAndOncallAllFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='email'){
              $sort="User.email $order";
            }
            else{
            $sort="AvailableAndOncallTransaction.id DESC $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='availableAndOncallAllFilter.id DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=ADMIN_PAGINATION;
        }
            $sortPost=$this->data;
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $userEmail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $userEmail=array('LOWER(User.email) LIKE'=>strtolower('%'.$sortPost['textEmail'].'%'));
            }
           $conditions= $userEmail;
           $fields = array("User.email, CompanyName.company_name,AvailableAndOncallTransaction.type,AvailableAndOncallTransaction.status,AvailableAndOncallTransaction.device_type,AvailableAndOncallTransaction.platform,AvailableAndOncallTransaction.created");

      $options  =
      array(
              'joins'=>array(
                  array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'left',
                    'conditions'=> array('User.id = AvailableAndOncallTransaction.user_id')
                ),
                array(
                    'table' => 'company_names',
                    'alias' => 'CompanyName',
                    'type' => 'left',
                    'conditions'=> array('AvailableAndOncallTransaction.company_id = CompanyName.id')
                ),
              ),
              'fields'=> $fields,
              'order'=> 'AvailableAndOncallTransaction.created DESC',
              'limit'=> $limit,
              'page'=>$j
              );
            $tCount=$this->AvailableAndOncallTransaction->find('count',array('fields'=>$fields));

        $this->Paginator->settings = $options;
        $data = $this->Paginator->paginate('AvailableAndOncallTransaction');
        $this->set(array('availableAndOncallTransactionData'=>$data,'limit'=>$limit,'tCount'=>count($data)));
        //$this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        $this->render('/Elements/user_available_and_oncall_filtered_data');

    }


    /*
    ---------------------------------------------------------------------
    On: 03-03-2016
    I/P:
    O/P:
    Desc: User post CSV download
    ---------------------------------------------------------------------
    */
    public function exportAvailableAndOnCallCsvDownload(){
      // $params = array();
      // if(isset($this->data['j'])){
      //     $j=$this->data['j'];
      // }
      // else{
      //     $j=1;
      // }
      // if(isset($this->data['limit'])){
      //     $limit=$this->data['limit'];
      // }
      // else{
      //     $limit= ADMIN_PAGINATION;
      // }

      // $fields = array("User.email, CompanyName.company_name,AvailableAndOncallTransaction.type,AvailableAndOncallTransaction.status,AvailableAndOncallTransaction.device_type,AvailableAndOncallTransaction.platform,AvailableAndOncallTransaction.created");

      // $options  =
      // array(
      //         'joins'=>array(
      //             array(
      //               'table' => 'users',
      //               'alias' => 'User',
      //               'type' => 'left',
      //               'conditions'=> array('User.id = AvailableAndOncallTransaction.user_id')
      //           ),
      //           array(
      //               'table' => 'company_names',
      //               'alias' => 'CompanyName',
      //               'type' => 'left',
      //               'conditions'=> array('AvailableAndOncallTransaction.company_id = CompanyName.id')
      //           ),
      //         ),
      //         'fields'=> $fields,
      //         'order'=> 'AvailableAndOncallTransaction.created DESC',
      //         'limit'=> $limit,
      //         'page'=>$j
      //         );

      $data = $this->User->query("SELECT User.email, CompanyName.company_name,AvailableAndOncallTransaction.type,AvailableAndOncallTransaction.status,AvailableAndOncallTransaction.device_type,AvailableAndOncallTransaction.platform,AvailableAndOncallTransaction.created
                                                FROM `available_and_oncall_transactions` AS AvailableAndOncallTransaction
                                                LEFT JOIN `users` AS User
                                                ON User.id = AvailableAndOncallTransaction.user_id
                                                LEFT JOIN `company_names` AS CompanyName
                                                ON AvailableAndOncallTransaction.company_id = CompanyName.id
                                                ORDER BY AvailableAndOncallTransaction.created DESC");

      $this->set(array("availableAndOncallTransactionDataDetails"=>$data));
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 09-10-2018
    I/P:
    O/P:
    Desc:Screen Shot Transaction Listing
    ------------------------------------------------------------------------------------------
    */

    public function adminRoles(){
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }

        //** Get User Data
        $conditions = array('User.status = 1','User.approved=1');
        $fields = array("AdminUser.email,AdminUser.added_date,AdminUser.status,AdminUser.role_id,User.id,UserProfile.first_name,UserProfile.last_name");
        $options  =
        array(
            'joins'=>array(
                array(
                  'table' => 'users',
                  'alias' => 'User',
                  'type' => 'left',
                  'conditions'=> array('AdminUser.email = User.email')
              ),
                array(
                  'table' => 'user_profiles',
                  'alias' => 'UserProfile',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserProfile.user_id')
              ),
            ),
            'fields'=> $fields,
            'conditions'=> $conditions,
            'order'=> 'AdminUser.id DESC',
            'limit'=> $limit,
            'group' => array('AdminUser.email'),
            'order' => 'UserProfile.first_name ASC',
            'page'=>$j
            );
    // pr($conditions);
            $tCount=$this->AdminUser->find('count');
            $this->Paginator->settings = $options;
            $adminUserData = $this->Paginator->paginate('AdminUser');


            $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));

        $this->set(array("adminUser"=>$adminUserData, "companyNames"=>$companyNames, "limit"=>$limit,'tCount'=>$tCount));
    }


    public function adminFilters(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='email'){
                $sort="AdminUser.email $order";
            }
            else{
                $sort="AdminUser.email $order";
            }
        }else{
            $con="";
            $type="";
            $sort='AdminUser.added_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }else{
            $limit=20;
        }
        $sortPost=$this->data;
         if(!empty($sortPost['company'])){
             $company=array('AdminUser.company_id'=>$sortPost['company'],'UserEmployment.is_current' => 1);
         }
         else{
            $company=array();
         }
        $conditions = array('AdminUser.company_id'=> $company, 'User.status = 1','User.approved=1');
       $fields = array("AdminUser.email,AdminUser.added_date,AdminUser.status,User.id,UserProfile.first_name,UserProfile.last_name");
        $options  =
        array(
            'joins'=>array(
                array(
                  'table' => 'users',
                  'alias' => 'User',
                  'type' => 'left',
                  'conditions'=> array('AdminUser.email = User.email')
              ),
                array(
                  'table' => 'user_profiles',
                  'alias' => 'UserProfile',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserProfile.user_id')
              ),
            ),
            'fields'=> $fields,
            'conditions'=> $conditions,
            'group'=> array('User.id'),
            // 'group'=> array('UserEmployment.user_id'),
            'order'=> $sort,
            'limit'=> $limit,
            'page'=>$j
        );

        $tCount=$this->AdminUser->find('count');
        $this->Paginator->settings = $options;
        $adminUserData = $this->Paginator->paginate('AdminUser');


        $companyNames = $this->CompanyName->find("all", array("conditions"=> array("status"=> 1, "created_by"=> 0),"fields"=> array("CompanyName.id, CompanyName.company_name"),'order'=> 'company_name'));

        $this->set(array("adminUser"=>$adminUserData, "companyNames"=>$companyNames, "limit"=>$limit,'tCount'=>$tCount));
        $this->render('/Elements/users/admin_list_ajax');

    }

    public function userReport(){
        $companyDetail = $this->CompanyName->find('all', array('conditions'=> array('CompanyName.is_subscribed'=> 1, 'CompanyName.status'=> 1, 'CompanyName.created_by'=> 0)));
        $this->set(array("companyDetail"=>$companyDetail));
    }

    public function reportDetail(){
        $companyId = $this->request->data['User']['company_id'];
        $companyDetail = $this->CompanyName->find('first', array('conditions'=> array('CompanyName.id'=>$companyId,'CompanyName.is_subscribed'=> 1, 'CompanyName.status'=> 1, 'CompanyName.created_by'=> 0)));
        $this->set(array("companyDetail"=>$companyDetail));

    }

    public function getProfessionList(){
      $sortPost=$this->data;
      if(!isset($sortPost['to']) || $sortPost['to']==""){
        $endDate = date("Y-m-d");
      }else{
        $endDate = date('Y-m-d', strtotime($sortPost['to']));
      }

      if(!isset($sortPost['from']) || $sortPost['from']==""){
        $date = array();
      }else{
        $startDate = date('Y-m-d', strtotime($sortPost['from']));
        $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
      }

      $conditions = array('User.status' => 1,'User.approved' => 1);
      $company = array('UserEmployment.company_id' => $sortPost['company'],'UserEmployment.is_current' => 1);

      $conditions=array_merge($conditions,$date,$company);

      $fields = array("User.email,User.registration_date,Profession.profession_type,Profession.id,UserEmployment.company_id");
      $options  =
          array(
          'joins'=>array(
              array(
                  'table' => 'professions',
                  'alias' => 'Profession',
                  'type' => 'left',
                  'conditions'=> array('UserProfile.profession_id = Profession.id')
              ),
              array(
                  'table' => 'user_employments',
                  'alias' => 'UserEmployment',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserEmployment.user_id')
              ),
              array(
                  'table' => 'enterprise_user_lists',
                  'alias' => 'EnterpriseUserList',
                  'type' => 'left',
                  'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
              ),
          ),
          'fields'=> $fields,
          'conditions'=> $conditions,
          'group'=> array('User.id')
      );

        $adminUserData = $this->User->find('all',$options);
        $arr = array();

        foreach($adminUserData as $key => $item){
           $arr[$item['Profession']['id']][$key] = $item;
        }

        $finalData = array();
        foreach ($arr as $value) {
          $newCount = 0;
          if(!empty($value)){
            foreach ($value as $list) {
              $current = strtotime(date("Y-m-d"));
              $dateNew    = strtotime(date('Y-m-d',strtotime($list['User']['registration_date'])));

              $datediff = $dateNew - $current;
              $difference = floor($datediff/(60*60*24));
              if($difference==0){
                $newCount++;
              }
              if(empty($date)){
                $userList['today'] = $newCount;
              }
              $userList['total'] = count($value);
              $userList['profession'] = $list['Profession']['profession_type'];
            }
            $finalData[] = $userList;
          }
        }

        if(empty($date)){
          $headers = array('Profession','Today Count','Total Count (Till Today)');
        }else{
          $headers = array('Profession','Total Count ('.date('d-m-Y', strtotime($startDate)).' To '.date('d-m-Y', strtotime($endDate)).')');
        }
        $this->set(array("data"=>$finalData,'headers'=>$headers));
        $this->render('/Elements/users/custom_list_ajax');
    }
    public function getBaselocationList(){
      $sortPost=$this->data;
      if(!isset($sortPost['to']) || $sortPost['to']==""){
        $endDate = date("Y-m-d");
      }else{
        $endDate = date('Y-m-d', strtotime($sortPost['to']));
      }

      if(!isset($sortPost['from']) || $sortPost['from']==""){
        $date = array();
      }else{
        $startDate = date('Y-m-d', strtotime($sortPost['from']));
        $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
      }
      $conditions = array('User.status' => 1,'User.approved' => 1);
      $company = array('UserEmployment.company_id' => $sortPost['company'],'UserEmployment.is_current' => 1);
      $tag = array('RoleTag.key' => 'Base Location');

      $conditions=array_merge($conditions,$date,$company,$tag);

      $fields = array("User.email,User.registration_date,UserEmployment.company_id,RoleTag.value");
      $options  =
          array(
          'joins'=>array(
              array(
                  'table' => 'user_role_tags',
                  'alias' => 'UserRoleTag',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserRoleTag.user_id')
              ),
              array(
                  'table' => 'role_tags',
                  'alias' => 'RoleTag',
                  'type' => 'left',
                  'conditions'=> array('UserRoleTag.role_tag_id = RoleTag.id')
              ),
              array(
                  'table' => 'user_employments',
                  'alias' => 'UserEmployment',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserEmployment.user_id')
              ),
              array(
                  'table' => 'enterprise_user_lists',
                  'alias' => 'EnterpriseUserList',
                  'type' => 'left',
                  'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
              ),
          ),
          'fields'=> $fields,
          'conditions'=> $conditions,
      );

        $adminUserData = $this->User->find('all',$options);
        $arr = array();

        foreach($adminUserData as $key => $item){
           $arr[$item['RoleTag']['value']][$key] = $item;
        }

        $finalData = array();
        foreach ($arr as $value) {
          $newCount = 0;
          if(!empty($value)){
            foreach ($value as $list) {
              if($list['RoleTag']['value'] != ''){
                $current = strtotime(date("Y-m-d"));
                $dateNew    = strtotime(date('Y-m-d',strtotime($list['User']['registration_date'])));

                $datediff = $dateNew - $current;
                $difference = floor($datediff/(60*60*24));
                if($difference==0){
                  $newCount++;
                }
                $data = explode("$",$list['RoleTag']['value']);
                if(isset($data[1])){
                  $str_value = $data[0].' ('.$data[1].')';
                }else{
                  $str_value = $data[0];
                }
                if(empty($date)){
                  $userList['today'] = $newCount;
                }
                $userList['total'] = count($value);
                $userList['profession'] = $str_value;
              }
            }
            $finalData[] = $userList;
          }
        }

        if(empty($date)){
          $headers = array('Base Location','Today Count','Total Count (Till Today)');
        }else{
          $headers = array('Base Location','Total Count ('.date('d-m-Y', strtotime($startDate)).' To '.date('d-m-Y', strtotime($endDate)).')');
        }
        $this->set(array("data"=>$finalData,'headers'=>$headers));
        $this->render('/Elements/users/custom_list_ajax');
    }
    public function getSpecialityList(){
      $sortPost=$this->data;
      if(!isset($sortPost['to']) || $sortPost['to']==""){
        $endDate = date("Y-m-d");
      }else{
        $endDate = date('Y-m-d', strtotime($sortPost['to']));
      }

      if(!isset($sortPost['from']) || $sortPost['from']==""){
        $date = array();
      }else{
        $startDate = date('Y-m-d', strtotime($sortPost['from']));
        $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
      }
      $conditions = array('User.status' => 1,'User.approved' => 1);
      $company = array('UserEmployment.company_id' => $sortPost['company'],'UserEmployment.is_current' => 1);
      $tag = array('RoleTag.key' => 'Speciality');

      $conditions=array_merge($conditions,$date,$company,$tag);

      $fields = array("User.email,User.registration_date,UserEmployment.company_id,RoleTag.value");
      $options  =
          array(
          'joins'=>array(
              array(
                  'table' => 'user_role_tags',
                  'alias' => 'UserRoleTag',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserRoleTag.user_id')
              ),
              array(
                  'table' => 'role_tags',
                  'alias' => 'RoleTag',
                  'type' => 'left',
                  'conditions'=> array('UserRoleTag.role_tag_id = RoleTag.id')
              ),
              array(
                  'table' => 'user_employments',
                  'alias' => 'UserEmployment',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserEmployment.user_id')
              ),
              array(
                  'table' => 'enterprise_user_lists',
                  'alias' => 'EnterpriseUserList',
                  'type' => 'left',
                  'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
              ),
          ),
          'fields'=> $fields,
          'conditions'=> $conditions,
      );

        $adminUserData = $this->User->find('all',$options);
        $arr = array();

        foreach($adminUserData as $key => $item){
           $arr[$item['RoleTag']['value']][$key] = $item;
        }
        $finalData = array();
        foreach ($arr as $value) {
          $newCount = 0;
          if(!empty($value)){
            foreach ($value as $list) {
              if($list['RoleTag']['value'] != ''){
                $current = strtotime(date("Y-m-d"));
                $dateNew    = strtotime(date('Y-m-d',strtotime($list['User']['registration_date'])));

                $datediff = $dateNew - $current;
                $difference = floor($datediff/(60*60*24));
                if($difference==0){
                  $newCount++;
                }
                $data = explode("$",$list['RoleTag']['value']);
                if(isset($data[1])){
                  $str_value = $data[0].' ('.$data[1].')';
                }else{
                  $str_value = $data[0];
                }
                if(empty($date)){
                  $userList['today'] = $newCount;
                }
                $userList['total'] = count($value);
                $userList['profession'] = $str_value;
              }
            }
            $finalData[] = $userList;
          }
        }

        if(empty($date)){
          $headers = array('Speciality','Today Count','Total Count (Till Today)');
        }else{
          $headers = array('Speciality','Total Count ('.date('d-m-Y', strtotime($startDate)).' To '.date('d-m-Y', strtotime($endDate)).')');
        }
        $this->set(array("data"=>$finalData,'headers'=>$headers));
        $this->render('/Elements/users/custom_list_ajax');
    }

    public function userReportDownload(){
        $this->layout = null;
        $this->autoLayout = false;
        $this->autoRender = false;

        $sortPost = array();
        $sortPost['from']=$_REQUEST['from'];
        $sortPost['to']=$_REQUEST['to'];
        $sortPost['type']=$_REQUEST['type'];
        $sortPost['company']=$_REQUEST['company'];

        if($sortPost['type'] == 'All'){
          $type = array('Profession','Base Location','Speciality');
        }else{
          $type = array((string)$sortPost['type']);
        }
        foreach ($type as $fetchType) {
            if(!isset($sortPost['to']) || $sortPost['to']==""){
              $endDate = date("Y-m-d");
            }else{
              $endDate = date('Y-m-d', strtotime($sortPost['to']));
            }

            if(!isset($sortPost['from']) || $sortPost['from']==""){
              $date = array();
            }else{
              $startDate = date('Y-m-d', strtotime($sortPost['from']));
              $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
            }

            $conditions = array('User.status' => 1,'User.approved' => 1);
            $company = array('UserEmployment.company_id' => $sortPost['company'],'UserEmployment.is_current' => 1);
            $joins = array(
                array(
                    'table' => 'user_employments',
                    'alias' => 'UserEmployment',
                    'type' => 'left',
                    'conditions'=> array('User.id = UserEmployment.user_id')
                ),
                array(
                    'table' => 'enterprise_user_lists',
                    'alias' => 'EnterpriseUserList',
                    'type' => 'left',
                    'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
                ),
            );

            if($fetchType != 'Profession'){
              $tag = array('RoleTag.key' => $fetchType);
              $conditions=array_merge($conditions,$date,$company,$tag);
              $fields = array("User.email,User.registration_date,UserEmployment.company_id,RoleTag.value");
              $cust_join = array(
                array(
                      'table' => 'user_role_tags',
                      'alias' => 'UserRoleTag',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserRoleTag.user_id')
                  ),
                  array(
                      'table' => 'role_tags',
                      'alias' => 'RoleTag',
                      'type' => 'left',
                      'conditions'=> array('UserRoleTag.role_tag_id = RoleTag.id')
                  )
                );
            }else{
              $conditions=array_merge($conditions,$date,$company);
              $fields = array("User.email,User.registration_date,Profession.profession_type,Profession.id,UserEmployment.company_id");
              $cust_join = array(
                array(
                  'table' => 'professions',
                  'alias' => 'Profession',
                  'type' => 'left',
                  'conditions'=> array('UserProfile.profession_id = Profession.id')
                )
              );
            }
            $joins = array_merge($joins,$cust_join);

            $options  =
                array(
                'joins'=> $joins,
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('User.id')
            );

            $adminUserData = $this->User->find('all',$options);
            $arr = array();

            foreach($adminUserData as $key => $item){
              if($fetchType == 'Profession'){
                $arr[$item['Profession']['id']][$key] = $item;
              }else{
                $arr[$item['RoleTag']['value']][$key] = $item;
              }
            }

            $finalData = array();
            foreach ($arr as $value) {
              $newCount = 0;
              if(!empty($value)){
                if($fetchType == 'Profession'){
                  foreach ($value as $list) {
                    $current = strtotime(date("Y-m-d"));
                    $dateNew    = strtotime(date('Y-m-d',strtotime($list['User']['registration_date'])));

                    $datediff = $dateNew - $current;
                    $difference = floor($datediff/(60*60*24));
                    if($difference==0){
                      $newCount++;
                    }
                    if(empty($date)){
                      $userList['today'] = $newCount;
                    }
                    $userList['total'] = count($value);
                    $userList['profession'] = $list['Profession']['profession_type'];
                  }
                }else{
                  foreach ($value as $list) {
                    if($list['RoleTag']['value'] != ''){
                      $current = strtotime(date("Y-m-d"));
                      $dateNew    = strtotime(date('Y-m-d',strtotime($list['User']['registration_date'])));

                      $datediff = $dateNew - $current;
                      $difference = floor($datediff/(60*60*24));
                      if($difference==0){
                        $newCount++;
                      }
                      $data = explode("$",$list['RoleTag']['value']);
                      if(isset($data[1])){
                        $str_value = $data[0].' ('.$data[1].')';
                      }else{
                        $str_value = $data[0];
                      }
                      if(empty($date)){
                        $userList['today'] = $newCount;
                      }
                      $userList['total'] = count($value);
                      $userList['profession'] = $str_value;
                    }
                  }
                }
                $finalData[] = $userList;
              }
            }
            if(empty($date)){
              $headers[] = array($fetchType,'Today Count','Total Count (Till Today)');
            }else{
              $headers[] = array($fetchType,'Total Count ('.date('d-m-Y', strtotime($startDate)).' To '.date('d-m-Y', strtotime($endDate)).')');
            }
            $totallData[] = $finalData;

        }
        // $totalHeaders[] = $headers;
        $date = date("d-m-Y");
        $file = $this->createPdf($totallData,$headers,$type,$date);
        // $this->set(array("data"=>$finalData,'headers'=>$headers));
        $path = BASE_URL . 'app/webroot/chatHistory/'.$file;
        echo $path;
        exit();
        // $this->set(array("path"=>(string)$path));
        // echo "<script>window.open(".$path.");</script>";
        // echo "<script>window.close();</script>";
        // exit();
    }

    public function createPdf($data,$headers,$type,$date){
      set_time_limit(180);
  		ini_set('memory_limit','256M');

  		App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));
  		App::import('Vendor','FPDI',array('file' => 'fpdi/fpdi.php'));
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  $fpdi = new FPDI();

      // $textHtml = '<table cellspacing="2" cellpadding="5"><thead><tr>';
      $textHtml = '<table style="margin:0 auto;" border="0" cellspacing="0" cellpadding="5">';
      for ($i=0; $i < count($data) ; $i++) {
        if($i > 0){
          $textHtml .= '<tr width="100%" height="20px;"><td colspan="8"></td></tr>';
        }
        $textHtml .= '<tr>';
        $textHtml .= '<td>';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" height="76" alt="">';
        $textHtml .= '</td>';
        $textHtml .= '<td bgcolor="#10A5DA" colspan="5">';
        $textHtml .= '<h4 style="margin-top: 16px;">';
        $textHtml .= '<font color="white">Daily Report: '.$type[$i].'-wise</font></h4>';
        $textHtml .= '</td>';
        $textHtml .= '<td bgcolor="#10A5DA" colspan="0">';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="149" height="76" alt="">';
        $textHtml .= '</td>';
        $textHtml .= '<td bgcolor="#10A5DA" colspan="3">';
        $textHtml .= '<h4>';
        $textHtml .= '<font color="white">'.$date.'</font></h4>';
        $textHtml .= '</td>';
        $textHtml .= '</tr>';
        $textHtml .= '<tr>';
        $textHtml .= '<td rowspan="'.(count($data[$i])+2).'">';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
        $textHtml .= '</td>';
        $textHtml .= '<td colspan="8">';
        $textHtml .= '<p style="color: #231F20;  margin: 4px;font-size: 15px;">';
        $textHtml .= $type[$i].' Summary Table</p>';
        $textHtml .= '</td>';
        $textHtml .= '<td rowspan="'.(count($data[$i])+2).'">';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="31"  alt="">';
        $textHtml .= '</td>';
        $textHtml .= '</tr>';
        $textHtml .= '<tr bgcolor=" #F4F4F5"; style="font-size: 12px;">';
        if(count($headers[$i]) == 3){
          $textHtml .= '<td style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; border-right: 1px solid #E8EBEE;"  colspan="3"><strong>'.$headers[$i][0].'</strong></td>';
          $textHtml .= '<td style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; border-right: 1px solid #E8EBEE;"  colspan="2"><strong>'.$headers[$i][1].'</strong></td>';
          $textHtml .= '<td style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; border-right: 1px solid #E8EBEE;"  colspan="3"><strong>'.$headers[$i][2].'</strong></td>';
        }else{
          $textHtml .= '<td style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; border-right: 1px solid #E8EBEE;"  colspan="4"><strong>'.$headers[$i][0].'</strong></td>';
          $textHtml .= '<td style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; border-right: 1px solid #E8EBEE;"  colspan="4"><strong>'.$headers[$i][1].'</strong></td>';
        }
        $textHtml .= '</tr>';

        if(!empty($data[$i])){
          foreach ($data[$i] as $list):
            $textHtml .= '<tr style="font-size: 12px;">';
            if(count($headers[$i]) == 3){
              $val1 = 3;
              $val2 = 3;
            }else{
              $val1 = 4;
              $val2 = 4;
            }
            isset($list['profession'])?$textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="'.$val2.'"><p>'.$list['profession'].'</p></td>':'';
            isset($list['today'])?$textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; text-align: center; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="2"><p>'.$list['today'].'</p></td>':'';
            isset($list['total'])?$textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; text-align: center; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="'.$val1.'"><p>'.$list['total'].'</p></td>':'';
            $textHtml .= '</tr>';
          endforeach;
        }else{
          $textHtml .= '<tr>';
          $textHtml .= '<td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>';
          $textHtml .= '</tr>';
        }
      }

      // $textHtml .= '<tr width="100%" height="20px;"><td colspan="8"></td></tr>';

      $textHtml .= '</table>';

      // Chat pdf Main Chats [END] ---------------------------
      // print_r($textHtml);
  		$headerTemplate = APP . 'webroot/dailyReport/templates/header.pdf';
  		$files = array($headerTemplate);
  		$pageCount = 0;
  		foreach($files AS $file) {
  		       $pagecount = $fpdi->setSourceFile($file);
  		       for ($i = 1; $i <= $pagecount; $i++) {
  		            $tplidx = $fpdi->ImportPage($i);
  		            // $s = $fpdi->getTemplatesize($tplidx);
                  $fpdi->setPrintHeader(false);
                  $fpdi->setPrintFooter(false);
  		            $fpdi->AddPage('P');
  		            $fpdi->useTemplate($tplidx,null, 0, 0, 0, FALSE);
  		            $fpdi->SetXY(0, 30);
  		            // $fpdi->SetFont('helvetica', '', 11);
  		            $fpdi->writeHTML($textHtml, true, true, true, 0);
  		       }
  		  }

  		$finalFileName = 'Chathistory' . strtotime(date('Y-m-d H:i:s')) . '.pdf';
  		$output = $fpdi->Output(APP . 'webroot/chatHistory/'.$finalFileName, 'F');
      chmod(APP . 'webroot/chatHistory/'.$finalFileName, 0777);
      return $finalFileName;
    }

    public function getSetDndStatus()
    {
        $params['user_id'] ="1";
        $oneTimeTokenForDnd = $this->dissmissDndOnOcrAndQb($params);
        echo "<pre>";print_r($oneTimeTokenForDnd);exit();
    }

}
