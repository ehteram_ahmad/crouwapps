<?php

/*
 * Email Component
 *
 * Contains all related function for email sending
 *
 */

App::uses('CakeEmail', 'Network/Email');

class AdminEmailComponent extends Component{

	/*
	On: 03-08-2015
	I/P: $params = array()
	O/P:
	Desc: Sending email to user with template.
	*/
	public function verifyEmail( $params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$params['subject'] = 'Please verify your email';
			$Email->template('verify_email', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['ACTIVATION_KEY'] = $params['activationLink'];
			$replacearray['BASEURL'] = BASE_URL;
			//$replacearray['SITE_NAME'] = 'The On Call Room Team!';
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'mail sent';
				}else{
					$msg = 'mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 10-08-2015
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to user with template for forgot password.
	--------------------------------------------------------------------------------
	*/
	public function forgotPassword( $params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$params['subject'] = 'Your new password';
			$Email->template('forgot_password', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['PASS'] = $params['password'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					$msg = "Mail Sent";
				}else{
					$msg = "Mail Not Sent";
				}
			}catch( Exception $e ){
				return $msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 17-12-2015
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to user whose beat marked as spam.
	--------------------------------------------------------------------------------
	*/
	public function markSpamBeatEmailToBeatOwner( $params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$Email->template('mark_spam_beat_to_beatowner', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['BEATNAME'] = $params['beat_title'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			$replacearray['REASON'] = $params['spam_reason'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 17-12-2015
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to admin beat marked as spam.
	--------------------------------------------------------------------------------
	*/
	public function markSpamBeatEmailToAdmin( $params = array() ){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$Email->template('mark_spam_beat_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['SENDERNAME'] = $params['name'];
			$replacearray['BEATNAME'] = $params['beat_title'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			$replacearray['REASON'] = $params['spam_reason'];
			$replacearray['BEAT_OWNER'] = $params['beat_owner_name'];
			$replacearray['SPAM_DATE'] = $params['spam_date'];
			$replacearray['SITE_REDIRECT_URL'] = $params['site_url_path'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 21-12-2015
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to patient when any doctor send consent.
	--------------------------------------------------------------------------------
	*/
	public function sendConsentMailToPatient( $params = array() ){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$Email->template('consent_form', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['patientName'];
			$replacearray['DOCTORNAME'] = $params['doctorName'];
			$replacearray['BASEURL'] = BASE_URL;
			$replacearray['SITE_NAME'] = 'OCR Team';
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			$Email->attachments(array(
				$params['file_name'] => array(
							'file' => $params['file_path'],
							'mimetype' => 'image/png',
							)
				)
			);
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	}


	/*
	On: 29-12-2015
	I/P: $params = array()
	O/P:
	Desc: Sending signup welcome email to user with template.
	*/
	public function signupWelcomeEmail( $params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$params['subject'] = 'Welcome';
			$Email->template('signup_welcome', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['REGARDS'] = $params['regards'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Welcome mail sent';
				}else{
					$msg = 'Welcome mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Replace variable and put values from email template.
	--------------------------------------------------------------------
	*/
	function strreplacetextreplace(&$t, $d) {
		preg_match_all ( '/{\%(\w*)\%\}/' , $t , $matches );
		foreach($matches[1] as $m){
						$pattern = "/{\%".$m."\%\}/";
						$t = preg_replace( $pattern, $d[$m], $t);
		}
		return $t;
    }

    /*
	On: 21-01-2015
	I/P:
	O/P:
	Desc: Sending APP download link email to visitor .
	*/
	public function appDownloadLinkMail( $params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$params['subject'] = 'APP Download Link';
			$Email->template('app_download_link', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['REGARDS'] = $params['regards'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Mail sent';
				}else{
					$msg = 'Mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	On: 21-01-2015
	I/P:
	O/P:
	Desc: Sending mail to admin when user fills contact us page.
	*/
	public function contactUsMail( $params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$Email->template('contact_us', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['SITE'] = "OCR Team";
			$replacearray['EMAIL'] = $params['from'];
			$replacearray['MESSAGE'] = $params['message'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Contact Us Mail sent';
				}else{
					$msg = 'Contact Us Mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	------------------------------------------------------------------------
	On: 22-01-2015
	I/P:
	O/P:
	Desc: Sending mail user when admin approves the user.
	------------------------------------------------------------------------
	*/
	public function userApproveByAdmin( $params = array()){
		$msg = "";
		if( !empty($params) ){
			// $Email = new CakeEmail();
			//$Email->config('smtp');
			// $Email->config('mailchimp');
			// $params['subject'] = 'Approval Confirmed';
			// if($params['registration_source'] == "MB"){
			// 	$Email->template('medicbleep_user_approve_by_admin', '');
			// 	$Email->from(array(NO_REPLY_EMAIL => "Medic Bleep"));
			// }else{
			// 	$Email->template('user_approve_by_admin', '');
			// 	$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			// }
			// $Email->emailFormat('html');
			// //** Replace Dynamic variables with values.
			// $replacearray = array();
			// $replacearray['NAME'] = $params['name'];
			//$replacearray['SITE_NAME'] = "OCR Team";
			/*===========End values to be replaced in the template==========*/
		 //  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			// $Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			// $Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			// $Email->from($params['fromMail']);
			// $Email->to( $params['toMail'] );
			// $Email->subject( $params['subject'] );

			// try{
			// 	if($Email->send()){
			// 		$msg = 'Admin Approve User Mail sent';
			// 	}else{
			// 		$msg = 'Admin Approve User Mail not sent';
			// 	}
			// }catch( Exception $e ){
			// 	$msg = $e->getMessage();
			// }
			// return $msg;
			try {
				App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
				$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
				$fromName = '';
				$params['subject'] = 'Approval Confirmed';
				$template_name = "MB_Manual_Approval_Confirmed";
				if($params['registration_source'] == "MB"){
					//$template_name = 'Congratulation MB Jan 2017';
					$fromName = 'Medic Bleep';
					//$Email->from(array(NO_REPLY_EMAIL => "Medic Bleep"));
				}else{
					//$template_name = 'Congratulation MB Jan 2017';
					$fromName = 'The On Call Room';
					//$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
				}
			    $template_content = array(
			        array(
			            'name' => $template_name,
			            'content' => 'Approval Confirmed'
			        )
			    );
				$message = array(
				'html' => 'Approval Confirmed',
		        'text' => 'Approval Confirmed',
		        'subject' => $params['subject'],
		        'from_email' => 'support@medicbleep.com',//NO_REPLY_EMAIL
		        'from_name' => $fromName,
		        'to' => array(
		            array(
		                'email' => $params['toMail'],
		                'name' => $params['name'],
		                'type' => 'to'
		            )
		        ),
		        //'headers' => array('Reply-To' => 'ehteram@mediccreations.com'),
		        //'important' => false,
		        //'track_opens' => null,
		        // 'track_clicks' => null,
		        // 'auto_text' => null,
		        // 'auto_html' => null,
		        // 'inline_css' => null,
		        // 'url_strip_qs' => null,
		        // 'preserve_recipients' => null,
		        // 'view_content_link' => null,
		        // 'bcc_address' => 'ehteram333@gmail.com',
		        // 'merge' => true,
		        // 'merge_language' => 'mailchimp',
		        'global_merge_vars' => array(
		            array(
		                'name' => 'NAME',
		                'content' => $params['name']
		            ),
		            array(
		                'name' => 'SITE_NAME',
		                'content' => 'OCR Team'
		            )
		        )

				);
			    $async = false;
			    $ip_pool = 'Main Pool';
			    $send_at = date('d-m-Y');
			    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
    			$msg = json_encode($result);

			}
			catch(Exception $e) {
			    // Mandrill errors are thrown as exceptions
			    //$result = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			    $msg = $e->getMessage();
			    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			    //throw $e;
			}
		}
	 return $msg;
	}

	/*
	-----------------------------------------------------------------------------
	On: 26-01-2016
	I/P:
	O/P:
	Desc: Sending contact response mail to visitor.
	-----------------------------------------------------------------------------
	*/
	public function contactUsMailResponseToVisitor( $params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$params['subject'] = 'Contact Us';
			$Email->template('contact_us', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['SITE'] = "OCR Team";
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Contact Us Response Mail to visitor  sent';
				}else{
					$msg = 'Contact Us Response Mail to visitor not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	-----------------------------------------------------------------------------
	On: 12-02-2016
	I/P:
	O/P:
	Desc: Sending promt mail to admin when user clicks on verify link.
	-----------------------------------------------------------------------------
	*/
	public function userActivationPromtToAdmin($params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$params['subject'] = 'New User Activated Account';
			$Email->template('user_activation_promt_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['EMAIL'] = $params['email'];
			$replacearray['COUNTRY'] = $params['country'];
			$replacearray['SITE_URL'] = $params['siteUrl'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'New User Active Promt to Admin Sent';
				}else{
					$msg = 'New User Active Promt to Admin Not Sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}
	public function unmarkSpamBeatEmailToBeatOwnerByAdmin($params = array()){
	if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$Email->template('mark_spam_beat_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['SENDERNAME'] = $params['name'];
			$replacearray['BEATNAME'] = $params['beat_title'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			$replacearray['NAME'] = $params['name'];
			$replacearray['BEAT_OWNER'] = $params['beat_owner_name'];

			$replacearray['SITE_REDIRECT_URL'] = $params['site_url_path'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	}

public function markSpamBeatEmailToBeatOwnerByAdmin($params = array()){

	if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$Email->template('mark_spam_beat_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['SENDERNAME'] = $params['name'];
			$replacearray['BEATNAME'] = $params['beat_title'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			$replacearray['REASON'] = $params['spam_reason'];
			$replacearray['BEAT_OWNER'] = $params['beat_owner_name'];
			$replacearray['SPAM_DATE'] = $params['spam_date'];
			$replacearray['SITE_REDIRECT_URL'] = $params['site_url_path'];
			$replacearray['COMMENT'] = $params['comment'];
			$replacearray['NAME'] = $params['name'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}

	}

	/*
	--------------------------------------------------------------------------------
	On: 16-03-2016
	I/P: $params = array()
	O/P: Message
	Desc: Sending email admin when visitor fills form and send sCV.
	--------------------------------------------------------------------------------
	*/
	public function visitorJoinTeamToAdmin( $params = array() ){
		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$Email->template('join_team_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['EMAIL'] = $params['email'];
			$replacearray['MESSAGE'] = $params['message'];

			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			$Email->attachments(array(
				$params['file_name'] => array(
							'file' => $params['file_path'],
							'mimetype' => $params['file_type'],
							)
				)
			);
			try{
				if($Email->send()){
					$msg = "Mail Sent";
				}else{
					$msg = "Mail Not Sent";
				}
			}catch( Exception $e ){
				$msg = "Exception Occured";
			}
		}
		return $msg;
	}

	/*
	--------------------------------------------------------------------------------
	On: 14-10-2016
	I/P: $params = array()
	O/P: Message
	Desc: Sending email to User by Admin from User Details Section.
	--------------------------------------------------------------------------------
	*/
	public function sendMailByAdmin($params = array()){

		if( !empty($params) ){
			$Email = new CakeEmail();
			$Email->config('mailchimp');
			$Email->emailFormat('html');
			$Email->viewVars(array('msg' => $params['temp']));
			if($params['registration_source'] == "MB"){
				$Email->template('medicbleep_verify_email', '');
				$Email->from(array("feedback@theoncallroom.com" => "Medic Bleep"));
			}else{
				$Email->template('user_approve_by_admin', '');
				$Email->from(array("feedback@theoncallroom.com" => "The On Call Room"));
			}
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					$msg = "Mail Sent";
				}else{
					$msg = "Mail Not Sent";
				}
			}catch( Exception $e ){
				return $msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 16-01-2017
	I/P: $params = array()
	O/P: Message
	Desc: Sending survey mail to registered users when admin approves
	--------------------------------------------------------------------------------
	*/
	public function surveyMailSend( $params=array() ){
		if( !empty($params) ){
			$Email = new CakeEmail();

			$Email->config('mailchimp');
			$Email->template('survey_mail_user_approve', '');
			$Email->emailFormat('html');

			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					$msg = "Mail Sent";
				}else{
					$msg = "Mail Not Sent";
				}
			}catch( Exception $e ){
				$msg = "Exception Occured";
			}
		}
		return $msg;
	}

	/*
	--------------------------------------------------------------------------------
	On: 28-08-2018
	I/P: $params = array()
	O/P: True/False
	Desc: Sending QR code pdf in email.
	--------------------------------------------------------------------------------
	*/

	public function sendQRCode( $params = array()){

		try{
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
			//$template = "VERIFY_LINK";
			$template = 'MB_Order_Processed';

			$template_content = array(
			array(
			'name' => $template,
			'content' => 'MB_Order_Processed'
			)
			);

			$to[]=array('email'=> $params['to_email'],
			'name' => $params['to_email'],
			'type' => 'to');


			$var[]=array(
				'rcpt' => $params['to_email'],
				'vars' => array(
					array(
						'name' => 'FNAME',
						'content' => $params['fname']
					),
					array(
						'name' => 'QUANTITY',
						'content' => $params['quantity']
					),
					array(
						'name' => 'BATCH',
						'content' => $params['batch']
					),
				)
			);
			$attachments[]=
	            array(
	            	"type" => $params['file_type'],
	            	"name"=> $params['file_name'],
	            	"content"=> $params['file_path']

	        );
			$message = array(
			'html' => 'MB ORDER PROCESSED',
			'text' => 'MB Order Processed',
			'subject' => 'MB Order Processed',
			//'from_email' => 'medicbleep@theoncallroom.com',
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
			// "subaccount" => MANDRILL_SUBACCOUNT,
			"attachments"=> $attachments
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = date('d-m-Y');
			$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
			$msg = $result;
		}
		catch(Exception $e) {
		    $msg = $e->getMessage();
		}
	 	return $msg;
	}

	public function sendQRCodeToAdmin( $params = array()){

		try{
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
			//$template = "VERIFY_LINK";
			$template = 'MB_ProcessedQR_Internal';

			$template_content = array(
			array(
			'name' => $template,
			'content' => 'MB_ProcessedQR_Internal'
			)
			);

			$to[]=array('email'=> $params['to_email'],
			'name' => $params['to_email'],
			'type' => 'to');


			$var[]=array(
				'rcpt' => $params['to_email'],
				'vars' => array(
					array(
						'name' => 'FNAME',
						'content' => $params['fname']
					),
					array(
						'name' => 'QUANTITY',
						'content' => $params['quantity']
					),
					array(
						'name' => 'BATCH',
						'content' => $params['batch']
					),
					array(
						'name' => 'TRUSTNAME',
						'content' => $params['trust']
					),
					array(
						'name' => 'DATETIME',
						'content' => $params['date']
					),
				)
			);
			$attachments[]=
	            array(
	            	"type" => $params['file_type'],
	            	"name"=> $params['file_name'],
	            	"content"=> $params['file_path']

	        );
			$message = array(
			'html' => 'QR Code Generated',
			'text' => 'QR Code Generated',
			'subject' => 'QR Code Generated',
			//'from_email' => 'medicbleep@theoncallroom.com',
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
			// "subaccount" => MANDRILL_SUBACCOUNT,
			"attachments"=> $attachments
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = date('d-m-Y');
			$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
			$msg = $result;
		}
		catch(Exception $e) {
		    $msg = $e->getMessage();
		}
	 	return $msg;
	}

	/*
	--------------------------------------------------------------------------------
	On: 04-09-2018
	I/P: $params = array()
	O/P: True/False
	Desc: Reject QR code request and send mail.
	--------------------------------------------------------------------------------
	*/

	public function rejectQRCodeRequest( $params = array()){

		try{
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
			//$template = "VERIFY_LINK";
			$template = 'MB_Order_Declined';

			$template_content = array(
			array(
			'name' => $template,
			'content' => 'MB_Order_Declined'
			)
			);

			$to[]=array('email'=> $params['to_email'],
			'name' => $params['to_email'],
			'type' => 'to');


			$var[]=array(
				'rcpt' => $params['to_email'],
				'vars' => array(
					array(
						'name' => 'FNAME',
						'content' => $params['fname']
					),
					array(
						'name' => 'QUANTITY',
						'content' => $params['quantity']
					),
					array(
						'name' => 'REASON',
						'content' => $params['reason']
					),
				)
			);

			$message = array(
			'html' => 'MB ORDER DECLINED',
			'text' => 'MB Order DECLINED',
			'subject' => 'MB Order Declined',
			//'from_email' => 'medicbleep@theoncallroom.com',
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = date('d-m-Y');
			$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
			$msg = $result;
		}
		catch(Exception $e) {
		    $msg = $e->getMessage();
		}
	 	return $msg;
	}

	public function expireQrCode( $params = array()){

		try{
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
			//$template = "VERIFY_LINK";
			$template = 'MB_BatchExpired';

			$template_content = array(
			array(
			'name' => $template,
			'content' => 'MB_BatchExpired'
			)
			);

			$to[]=array('email'=> $params['to_email'],
			'name' => $params['to_email'],
			'type' => 'to');


			$var[]=array(
				'rcpt' => $params['to_email'],
				'vars' => array(
					array(
						'name' => 'FNAME',
						'content' => $params['fname']
					),
					array(
						'name' => 'QUANTITY',
						'content' => $params['quantity']
					),
					array(
						'name' => 'DATETIME',
						'content' => $params['date']
					),
					array(
						'name' => 'BATCH',
						'content' => $params['batch']
					),
				)
			);

			$message = array(
			'html' => 'Batch Expired',
			'text' => 'Batch Expired',
			'subject' => 'Batch Expired',
			//'from_email' => 'medicbleep@theoncallroom.com',
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = date('d-m-Y');
			$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
			$msg = $result;
		}
		catch(Exception $e) {
		    $msg = $e->getMessage();
		}
	 	return $msg;
	}

	public function expireQrCodeToAdmin( $params = array()){

		try{
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
			//$template = "VERIFY_LINK";
			$template = 'MB_BatchExpired_Internal';

			$template_content = array(
			array(
			'name' => $template,
			'content' => 'MB_BatchExpired_Internal'
			)
			);

			$to[]=array('email'=> $params['to_email'],
			'name' => $params['to_email'],
			'type' => 'to');


			$var[]=array(
				'rcpt' => $params['to_email'],
				'vars' => array(
					array(
						'name' => 'FNAME',
						'content' => $params['fname']
					),
					array(
						'name' => 'QUANTITY',
						'content' => $params['quantity']
					),
					array(
						'name' => 'TRUSTNAME',
						'content' => $params['trust']
					),
					array(
						'name' => 'BATCH',
						'content' => $params['batch']
					),
				)
			);

			$message = array(
			'html' => 'Batch Expired',
			'text' => 'Batch Expired',
			'subject' => 'Batch Expired',
			//'from_email' => 'medicbleep@theoncallroom.com',
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = date('d-m-Y');
			$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
			$msg = $result;
		}
		catch(Exception $e) {
		    $msg = $e->getMessage();
		}
	 	return $msg;
	}

	public function assignRole($params=array()){
	 	App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');

		if((string)$params['status'] == '1'){
			$template = "MB_Role_Assign";
			$nameTxt = 'Role Updated';
		}else{
			$template = "MB_Role_Removed";
			$nameTxt = 'Role Removed';
		}
		$template_content = array(
			array(
				'name' => $template,
				'content' => $nameTxt
			)
		);

		$to[]=array('email'=> $params['toMail'],
			'name' => $params['f_name'],
			'type' => 'to'
		);


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
				array(
					'name' => 'FNAME',
					'content' => $params['f_name']
				),
				array(
					'name' => 'ROLE',
					'content' => $params['role']
				),
				array(
					'name' => 'TRUSTNAME',
					'content' => $params['trust_name']
				),
			)
		);


		$message = array(
			'html' => $nameTxt,
			'text' => $nameTxt,
			'subject' => $nameTxt,
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
		$msg = $result;
		return $msg;
	}

}
?>
