<?php
class MandrillComponent extends Component{
	
	var $components = array('Session', 'RestRequest');
	
	var $settings = array(
		'host'		=> 'smtp.mandrillapp.com',
		'port'		=> '587',
		'username'	=> 'username',
		'api_key'	=> 'randomapikey'
	);
	
	/*
		A Default $request_array for convenience purposes.
	*/
	var $request_array = array(
	    'key' => $this->settings['api_key'],
	    'message'=> array(
	        'html'=> '<p>Example HTML content</p>',
	        'text'=> 'Example text content',
	        'subject'=> 'example subject',
	        'from_email'=> 'message.from_email@example.com',
	        'from_name'=> 'Example Name',
	        'to' => array( 
/*
	        	'0' => array(
	                'email'=> 'recipient.email@example.com',
	                'name'=> 'Resident'
				),
	        	'1' => array(
	                'email'=> 'recipient.email@example.com',
	                'name'=> 'Resident'
				)
*/
            ),
	        'headers'=> array(
	            'Reply-To'=> 'message.reply@example.com'
	        ),
	        'important'=> false,
	        'track_opens'=> null,
	        'track_clicks'=> null,
	        'auto_text'=> null,
	        'auto_html'=> null,
	        'inline_css'=> null,
	        'url_strip_qs'=> null,
	        'preserve_recipients'=> null,
	        'view_content_link'=> null,
	        'bcc_address'=> 'message.bcc_address@example.com',
	        'tracking_domain'=> null,
	        'signing_domain'=> null,
	        'return_path_domain'=> null,
	        'tags'=> array(
	        	'notification-email'
	        ),
	        'metadata'=> array(
	            'website'=> 'www.example.com'
	        )
	    ),
	    'async'=> false,
	    'ip_pool'=> 'Main Pool'
	);
	

	/*
		When VARS ATTACKS!
		
		@param (string)$recipient, the person you're sending the email to.
		@param (string)$sender, the person who's sending the email.
		@param (string)$subject, the subject.
		@param (string)$html, The HTML email, could be just plain text if you wanted.
		@param (string)$html, indexless array of tags, defaults to just notification-email
		
		example request:
		Mandrill->send_email('me@test.com', 'admin@test.com', 'Subject'. 'Please provide new Credit Card data.', array('billing', 'expired-card'));
	*/
	function send_email($recipient = null, $sender = null, $subject = null, $html = null, $tags = array())
	{
		
		if(!empty($tags)){
			unset($request_array['message']['tags']);
			$request_array['message']['tags'] =  $tags;
		}
		
		// all the Mandrill sendness
		$request_array = $this->request_array;
		$request_array['message']['from_email'] = $sender;
		
		$recipient = array('email' => $recipient, 'name' => 'Resident');
		
		$request_array['message']['to'][] = $recipient;
		debug($request_array['message']['to'][0]['email']);
		//$request_array['message']['headers']['Reply-To'] = $html['message']['headers']['Reply-To'];
		$request_array['message']['html'] = $html;
		$request_array['message']['subject'] = $subject;

	    $request = new $this->RestRequest('https://mandrillapp.com/api/1.0/messages/send.json', 'POST', $request_array);
	    $request->execute();
	    //$responseObject = json_decode($request->getResponseBody());
		//return $responseObject;
		
		return $request->getResponseBody();
	}
	
	/* 
	 *
	 *	Users Methods
	 *
	 *  https://mandrillapp.com/api/docs/users.JSON.html
	 */
	function users_info(){
		
		$data = array(
			'key' => $this->settings->api_key
		);
		
		// create a new rest requst
		$request = $this->_make_request('users/info.json', 'POST', $data);
		$request->execute();
		return $request->getResponseBody();	
	}

	function users_ping(){
		
		$data = array(
			'key' => $this->settings->api_key
		);
		
		// create a new rest requst
		$request = $this->_make_request('users/ping.json', 'POST', $data);
		$request->execute();
		return $request->getResponseBody();	
	}
	
	function users_ping2(){
		
		$data = array(
			'key' => $this->settings->api_key
		);
		
		// create a new rest requst
		$request = $this->_make_request('users/ping2.json', 'POST', $data);
		$request->execute();
		return $request->getResponseBody();	
	}
	
	function users_senders(){
		
		$data = array(
			'key' => $this->settings->api_key
		);
		
		// create a new rest requst
		$request = $this->_make_request('users/senders.json', 'POST', $data);
		$request->execute();
		return $request->getResponseBody();	
	}
	
	/* 
	 *
	 *	Messages Methods
	 *
	 *  https://mandrillapp.com/api/docs/messages.JSON.html
	 */
	function messages_send($request_data = array()){
				
		if(!empty($request_data)){

			$default = $this->_return_default_send_email();
			$data = array_merge($default, $request_data);
		}
		
		// create a new rest requst
		$request = $this->_make_request('users/info.json', 'POST', $data);
		$request->execute();
		return $request->getResponseBody();	
	}
	
	
	
	
	
	// Utility methods
	// creat Rest Request
	function _make_request($method = null, $action = 'POST', $data = array()){
	
		if($method) {
			$url = "https://mandrillapp.com/api/1.0/" . $method;
		}
		
		if(!empty($data)) {
			return new $this->RestRequest($url, $action, $data);			
		} else {
			return false;
		}
	}
	
	function _return_default_send_email(){
		
			$default = array(
			    "key"=> $this->settings->api_key,
			    "message"=> array(
			        "html"=> "<p>Example HTML content</p>",
			        "text"=> "Example text content",
			        "subject"=> "example subject",
			        "from_email"=> "message.from_email@example.com",
			        "from_name"=> "Example Name",
			        "to"=> array(
		                "email"=> "recipient.email@example.com",
		                "name"=> "Recipient Name",
		                "type"=> "to"
		            ),
			        "headers"=> array(
			            "Reply-To"=> "message.reply@example.com"
			        ),
			        "important"=> false,
			        "track_opens"=> null,
			        "track_clicks"=> null,
			        "auto_text"=> null,
			        "auto_html"=> null,
			        "inline_css"=> null,
			        "url_strip_qs"=> null,
			        "preserve_recipients"=> null,
			        "view_content_link"=> null,
			        "bcc_address"=> "message.bcc_address@example.com",
			        "tracking_domain"=> null,
			        "signing_domain"=> null,
			        "return_path_domain"=> null,
			        "merge"=> true,
			        "global_merge_vars"=> array(
		                "name"=> "merge1",
		                "content"=> "merge1 content"
		            ),
			        "merge_vars"=> array(
		                "rcpt"=> "recipient.email@example.com",
		                "vars"=> array(
	                        "name"=> "merge2",
	                        "content"=> "merge2 content"
	                    )
		            ),
			        "tags"=> array(
			            "password-resets"
			        ),
			        "subaccount"=> "customer-123",
			        "google_analytics_domains"=> array(
			            "example.com"
			        ),
			        "google_analytics_campaign"=> "message.from_email@example.com",
			        "metadata"=> array(
			            "website"=> "www.example.com"
			        ),
			        "recipient_metadata"=> array(
		                "rcpt"=> "recipient.email@example.com",
		                "values"=> array(
		                    "user_id"=> 123456
		                )
		            ),
			        "attachments"=> array(
		                "type"=> "text/plain",
		                "name"=> "myfile.txt",
		                "content"=> "ZXhhbXBsZSBmaWxl"
		            ),
			        "images"=> array(
		                "type"=> "image/png",
		                "name"=> "IMAGECID",
		                "content"=> "ZXhhbXBsZSBmaWxl"
		            )
			    ),
			    "async"=> false,
			    "ip_pool"=> "Main Pool",
			    "send_at"=> "example send_at"
			);
		return $default;
	}
	
}
?>