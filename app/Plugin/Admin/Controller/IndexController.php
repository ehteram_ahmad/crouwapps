<?php
class IndexController extends AdminAppController {
    public $components = array('Admin.Image','RequestHandler', 'Cookie','Session','Common','Paginator','Admin.AdminEmail');
    public $uses = array('AdminUser','EmailTemplate');
    public $helpers = array('Form', 'Html', 'Js', 'Time');
    
    public function index($id=" ") {
        if( null !== $this->Session->read('userName') && $this->Session->read('userName') !='' ){
        $this->redirect('/admin/Home/dashboard');
        }
    }

    public function login(){
    	if( $this->request->is('ajax') ) {
    		$this->autoRender = false;
    		if( !empty($this->request->data['userName']) && !empty($this->request->data['password']) ){
        		$this->request->data['UserAdmin']['username'] = $this->request->data['userName'];
    			$this->request->data['UserAdmin']['password'] = $this->request->data['password'];
    			$userData = $this->AdminUser->find('count', 
    						array('conditions'=> 
    							array(
    								'username'=> $this->request->data['userName'],
    								'password'=> md5( $this->request->data['password'] ),
    								'status'=> 1
    								)
    						));
    			if( $userData > 0 ){
                    $this->Session->write('userName', $this->request->data['userName']);
    				echo "success";
                    //$this->redirect('/admin/home/dashboard');
    			}else{
    				echo 'Invalid Login Credentials';
    			}
				/*if ( $this->Auth->login() ) {
					$this->redirect('/admin/index/dashboard');
				}*/
    		}else{
    			echo ERROR_603;
    		}
		}
    	exit;
    }

    public function logout(){
    	$this->Session->destroy('userName');
    	$this->redirect('/admin');
    }
    public function forgotPwd(){
        if( $this->request->is('ajax') ) {
            $this->autoRender = false;
            if( !empty($this->request->data['email']) ){
                $userData = $this->AdminUser->find('first',array('conditions'=>array('email'=> $this->request->data['email'], 'status'=> 1)));
                if( count($userData) > 0 ){
                    $randompass = $this->randomPassword();
                    $params['name'] = $userData['AdminUser']['username'];
                    $params['toMail'] = $this->data['email'];
                    $params['from'] = NO_REPLY_EMAIL;
                    $params['password'] = $randompass;
                    $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Forgot password')));
                    $params['temp'] = $templateContent['EmailTemplate']['content'];
                     try{
                     // Sending Email
                     if($this->AdminEmail->forgotPassword( $params )){
                         //** Update new password to DB
                         $this->AdminUser->updateAll(array('AdminUser.password'=>'"'.md5($randompass).'"'),array('AdminUser.email'=>$this->data['email']));
                     }else{
                     }
                    }catch(Exception $e){
                    } 

                    echo 'success';
                }else{
                    echo 'Wrong Email Id';
                }
            }else{
                echo ERROR_603;
            }
        }
        exit;
    }
    /*
    ________________________________________________
    Method Name:Main=>randomPassword
    Purpose:
    Detail: 
    created by :Developer
    Params:NA
    _________________________________________________
    */

        public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        
        // Small,capital,number minimum 6 char
        $smallAlphabet = "abcdefghijklmnopqrstuwxyz";
        $capitalAlphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
        $number = "0123456789";

        $string = $capitalAlphabet[rand(0, strlen($capitalAlphabet)-1 )].$smallAlphabet[rand(0, strlen($smallAlphabet)-1)].$number[rand(0, strlen($number)-1 )].$capitalAlphabet[rand(0, strlen($capitalAlphabet)-1 )].$number[rand(0, strlen($number)-1 )].$smallAlphabet[rand(0, strlen($smallAlphabet)-1 )];
        
        return $string; //turn the array into a string
        }
    /*
    ________________________________________________
    Method Name:Main=>randomPassword
    Purpose:
    Detail: 
    created by :Developer
    Params:NA
    _________________________________________________
    */

        public function getUserPresence(){
            $this->autoRender = false;
            $responseData = array();
            if(null !== $this->Session->read('userName') && $this->Session->read('userName') !='' ){
                $responseData['status'] = 1;
                $responseData['message'] = 'ok.';
                echo json_encode($responseData);
            }else{
                $responseData['status'] = 0;
                $responseData['message'] = 'Session out.';
                echo json_encode($responseData);
            }       
        }

    
}