<?php

/**********************************************************
*Developed for :YHR 
*Controller Name : EmailsController
*Purpose : Email controller handle the email template management 
*Created date : 30-09-2015
*
*
*********************************************************** 
*/

App::uses('AppController', 'Controller');


class EmailsController extends AdminAppController {



	public $uses = array('Admin.Emailtemplate');
   
    //public $layout = 'admin_home';
    public $components = array('Paginator');
    //public $helpers = array('Ck');
    


/*
________________________________________________
Method Name:Main=>index
Purpose:Used to login the admin in the administrator task 
Detail: Admin email and password required to validate the admin 
created by :Developer
Created date:31-08-15
Params:Via post

_________________________________________________
*/
 public function index() {
      $limit=20;
      $paginate=array(
        'conditions'=> array('Emailtemplate.status'=>array(0,1)),
        'limit'=>$limit,
        'order'=> 'Emailtemplate.modified DESC', 
        'fields'=>array('id','name','created','modified','status'),
      );
      $this->Paginator->settings = $paginate;
      $dataResult = $this->Paginator->paginate('Emailtemplate');
      $this->set(array('data'=>$dataResult,'limit'=>$limit));
  

 }

 public function emailFilter(){
  $this->layout=null;
  if(isset($this->data['limit'])){
        $limit=$this->data['limit'];
    }
    else{
        $limit=20;
    }
    if(isset($this->data['sort'])){
      $con=$this->data['sort'];
      $type=$this->data['order'];
      $order=$type."SC";
      if($con=='name'){
        $sort="Emailtemplate.name $order";
      }
      elseif($con=='Created'){
        $sort="Emailtemplate.created $order";
      }
      elseif($con=='Modified'){
        $sort="Emailtemplate.modified $order";
      }
    }
    else{
        $con="";
        $type="";
        $sort="Emailtemplate.modified DESC";
    }
    if($this->data['statusVal']!==""){
        $status=array('Emailtemplate.status'=>$this->data['statusVal']);
      }
    else{
        $status=array('Emailtemplate.status'=>array(0,1));
     }
    if(isset($this->data['j'])){
        $j=$this->data['j'];
    }
    else{
        $j=1;
    }
    $paginate=array(
        'conditions'=> $status,
        'limit'=>$limit,
        'order'=> $sort, 
        'fields'=>array('id','name','created','modified','status'),
        'page'=>$j
      );
      $this->Paginator->settings = $paginate;
      $dataResult = $this->Paginator->paginate('Emailtemplate');
      $this->set(array('data'=>$dataResult,'limit'=>$limit,'j'=>$j,'con'=>$con,'order'=>$type));
      $this->render('/Elements/email_filter');
 }

 /*
 ------------------------------------------------------------------------------------------
 On: 13-09-2016
 I/P:
 O/P:
 Desc: Delete Email from list (soft delete)
 ------------------------------------------------------------------------------------------
 */
 public function deleteEmail(){
     if(isset($this->request->data['Id'])){
         $updateUser = $this->Emailtemplate->updateAll(array("Emailtemplate.status"=> 2), array("Emailtemplate.id"=> $this->request->data['Id']));
         if($updateUser){
             echo "Emailtemplate Deleted";
         }else{
             echo "Some issue occured. Please try again.";
         }
     }else{
         echo "Emailtemplate Not Deleted! Try Again.";
     }
     exit;
 }
     /*
  On: 13-09-2016
  I/P:
  O/P:
  Desc: Change Email-Template status 
     */
     public function changeEmailStatus(){
      $emailId = $this->request->data('id');
      $statusVal = $this->request->data('statusVal');
      if( trim($statusVal) == 'Active' ){ 
        $chngeStatus = 0;
      }else if( trim($statusVal) == 'Inactive' ){ 
        $chngeStatus = 1;
      }
      
      $conditions = array('Emailtemplate.id'=> $emailId);
      $data = array(
            'Emailtemplate.status'=> $chngeStatus,
          );

      if( $this->Emailtemplate->updateAll( $data ,$conditions )){
        
        if( $chngeStatus == 1 ){ $statusString = 'Active'; }
        if( $chngeStatus == 0 ){ $statusString = 'Inactive'; }
        echo "OK~$statusString";
      }else{
        echo "ERROR~".ERROR_615;
      }
      exit;
     }

/*
________________________________________________
Method Name:Main=>emaildetail
Purpose:Admin can view the email detail
Detail: 
created by :Developer
Created date:30-09-15
Params:None

_________________________________________________
*/
public function emaildetail($id){
   // $this->layout='ajax';
   // if($this->request->is('ajax')){ 
      $value = $this->Emailtemplate->find('all',array('conditions'=>array('Emailtemplate.id'=>$id),'fields'=>array('id','name','content','created','modified')));  
      $this->set('data',$value);
 //   }else{
 //        echo 'This is not a Ajax call';
 // }
}
/*
________________________________________________
Method Name:Main=>emaildetail
Purpose:Admin can view the email detail
Detail: 
created by :Developer
Created date:30-09-15
Params:None

_________________________________________________
*/
public function emaileditdetail($id=NULL){
      if(!empty($this->data)){
        $email = array('content'=>$this->data['addEmail']['content'],'id'=>$id);
        $this->Emailtemplate->saveAll($email);
        $this->Session->setFlash("Edited successfully");
        // $this->redirect(array('plugin'=>'admin','controller'=>'emails','action'=>'index'));
        $this->redirect(BASE_URL . 'admin/emails/index');

      }
      $value = $this->Emailtemplate->find('all',array('conditions'=>array('Emailtemplate.id'=>$id),'fields'=>array('id','name','content','created','modified')));  
      $this->set('data',$value);
   
}
/*
________________________________________________
Method Name:Main=>emaildetail
Purpose:Admin can view the email detail
Detail: 
created by :Developer
Created date:30-09-15
Params:None

_________________________________________________
*/
function add() {
        if (!empty($this->data)) {
            $templateAlreadyExists = array('name'=>$this->data['addEmail']['title']);
            if ( $this->Emailtemplate->find("count", array("conditions"=> $templateAlreadyExists )) == 0 ) {
              $email = array('name'=>$this->data['addEmail']['title'],'content'=>$this->data['addEmail']['content']);
              try{
                  if( $this->Emailtemplate->saveAll($email) ){
                    $message = "Template added successfully";
                  }else{
                    $message = "Some System error.";
                  }
              }catch(Exception $e){
                $message = $e->getMessage();
              }
              $this->Session->setFlash( $message );
            }else{
              $this->Session->setFlash("Template with this same name already exists");
            }
            $this->redirect(BASE_URL . 'admin/emails/index');
        }
    }
function delete_template($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for email template', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Emailtemplate->delete($id)) {
            $this->Session->setFlash(__('Email template deleted successfully', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Email template was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }

}// End Class