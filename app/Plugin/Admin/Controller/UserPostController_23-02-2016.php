<?php
class UserPostController extends AdminAppController {
    public $components = array('Cookie','Session','Common','Paginator','Email');
    
    public $helpers = array('Html','Js','Form','Session','Time');

    public $uses = array('Admin.UserPost','Admin.User','Admin.Specilities','UserPostBeat','UserPostShare','PostSpamContent','AdminPostSpam','EmailTemplate');

    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
        'order' => array(
            'UserPost.id' => 'desc'
        )
    );

    /*
	On: 02-09-2015
	I/P: 
	O/P: 
	Desc: All user post lists
    */
    public function userPostLists( $postId = NULL ){
        $userPostData = array();
        if( !empty($postId) ){
            $conditions = array('UserPost.id'=> $postId);
            $userPostData = $this->find("all", array('conditions'=> $conditions)); 
        }else{
            //$userPostData = $this->find("all"); 
            $this->Paginator->settings = $this->paginate;
            $userPostData = $this->Paginator->paginate('UserPost');
        }
        foreach($userPostData as $userPost){
        		// get post speciality from user post
        		$userPost['speciality_list'] = $this->speciality_string($userPost['UserPostSpecilities']);
        		$userPost['beat_details'] = $this->get_beats_details($userPost['UserPost']['id']);
        		$userPost['share_details'] = count($this->get_share_details($userPost['UserPost']['id']));
            $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userPost['UserPost']['user_id'])));
            if(!empty($userDetails)){
                //$name = isset($userDetails['UserProfile']['first_name']) ? $userDetails['UserProfile']['first_name'] : ''. ' ' . isset($userDetails['UserProfile']['last_name']) ? $userDetails['UserProfile']['last_name'] : '';
                $name = $userDetails['UserProfile']['first_name'].' '.$userDetails['UserProfile']['last_name'];
                $email = isset($userDetails['User']['email']) ? $userDetails['User']['email'] : '';
                $userData['User'] = array("name"=> $name, "email"=> $email);
            }else{
                $userData['User'] = array("name"=> '', "email"=> '');
            }
            $userPosts[] = array_merge($userPost, $userData);
        }
        //print_r($userPosts);exit();
        $this->set("userPostData", $userPosts);
    }

    /*
    On: 02-09-2015
    I/P: 
    O/P:
    Desc: user post status change like Active/Inactive
    */
    public function changeUserStatus(){
        $specId = $this->request->data('specId');
        $statusVal = $this->request->data('statusVal');
        if( trim($statusVal) == 'Active' ){
            $chngeStatus = 0;
        }else if( trim($statusVal) == 'Inactive' ){
            $chngeStatus = 1;
        }
        //$this->User->id = $userId;
        $conditions = array('UserPost.id'=> $specId);
        $data = array(
                    'UserPost.status'=> $chngeStatus,
                );

        if( $this->UserPost->updateAll( $data ,$conditions )){
            $statusString = 'Inactive';
            if( $chngeStatus == 1 ){ $statusString = 'Active'; }
            if( $chngeStatus == 0 ){ $statusString = 'Inactive'; }
            echo "OK~$statusString";
        }else{
            echo "ERROR~".ERROR_615;
        }
        exit;
    }

    /*
	On: 02-09-2015
	I/P:
	O/P:
	Desc: 
    */
    public function viewPostAttributes(){
    	$this->layout = NULL;
    	$userPostAttributes = array();
    	$postId = $this->request->data['post_id'];
    	if( !empty($postId) ){
    		$userPostData = $this->UserPost->userPostLists( $postId );
    		$userPostAttributes = $userPostData[0]['UserPostAttribute'];
    	}
    	$this->set("userPostAttributes", $userPostAttributes);
    	echo $this->render('Admin.UserPost/userPostAttributes');
    	exit;
    }

    /*
	On: 02-09-2015
	I/P:
	O/P:
	Desc: 
    */
    public function viewPostComments($postId = 0){
    	$userPostComments = array();
    	$userPostCommentDetails = array();
    	if( !empty($postId) ){
    		$userPostData = $this->UserPost->userPostLists( $postId );
    		$userPostComments = $userPostData[0]['UserPostComment'];
    	}
    	if(count($userPostComments) > 0){
    		foreach($userPostComments as $comment){
    			$name = "";
    			$email = "";
    			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $comments['user_id'])));
            if(!empty($userDetails)){
                //$name = isset($userDetails['UserProfile']['first_name']) ? $userDetails['UserProfile']['first_name'] : ''. ' ' . isset($userDetails['UserProfile']['last_name']) ? $userDetails['UserProfile']['last_name'] : '';
                $name = $userDetails['UserProfile']['first_name'].' '.$userDetails['UserProfile']['last_name'];
                $email = isset($userDetails['User']['email']) ? $userDetails['User']['email'] : ''; 
            }
            $comment['user_name'] = $name;
    			$userPostCommentDetails[] = $comment;
    		}
    	}
    /*	$this->set("userPostComments", $userPostComments);
    	echo $this->render('Admin.UserPost/userPostComments');
    	exit;*/
    	return $userPostCommentDetails;
    }
    /*
	On: 18-02-2016
	I/P:
	O/P:
	Desc: 
    */
    public function speciality_string($specialities = array()){
    			$speciality_string = "";
        		$specialityData = array();
        		if(count($specialities) > 0 && is_array($specialities)){
        			foreach($specialities as $postspeciality){
        				$specialityData = $this->Specilities->findById( $postspeciality["specilities_id"] );
        				if($speciality_string == ""){
        					$speciality_string .= $specialityData['Specilities']['name'];
        				}else{
        					$speciality_string .= ', '.$specialityData['Specilities']['name'];
        				}
        			}
        		}
        		return $speciality_string;
    }
    public function get_beats_details($post_id = ""){
    		//$conditions = array('UserPostBeat.post_id'=> $post_id);
    		$beat_array = $this->UserPostBeat->find('all', array(
		    'joins' => array(
		        array(
		            'table' => 'user_profiles',
		            'alias' => 'UserJoin',
		            'type' => 'INNER',
		            'conditions' => array(
		                'UserJoin.user_id = UserPostBeat.user_id'
		            )
		        )
		    ),
		    'conditions' => array(
		        'UserPostBeat.post_id'=> $post_id
		    ),
    			'fields' => array('UserJoin.*', 'UserPostBeat.*'),
    			'order' => 'UserPostBeat.id DESC'
		));
        	$upbeat_array = array();
        	$downbeat_array = array();
        	foreach($beat_array as $beat){
        		$beat['UserPostBeat']['username'] = $beat['UserJoin']['first_name'].' '.$beat['UserJoin']['last_name'];
        		if($beat['UserPostBeat']['beat_type'] == 1){
        			$upbeat_array[] = $beat['UserPostBeat'];
        		}else if($beat['UserPostBeat']['beat_type'] == 0){
        			$downbeat_array[] = $beat['UserPostBeat'];
        		}
        	}
        	$beat_details = array('upbeat' => $upbeat_array , 'downbeat' => $downbeat_array);
        	return $beat_details;
    }
    public function get_share_details($post_id = ""){
    		$share_details = array();
    		$conditions = array('UserPostShare.post_id'=> $post_id); 
        	$share_array = $this->UserPostShare->find("all", array(
		    'joins' => array(
		        array(
		            'table' => 'user_profiles',
		            'alias' => 'UserJoin',
		            'type' => 'INNER',
		            'conditions' => array(
		                'UserJoin.user_id = UserPostShare.shared_from'
		            )
		        )
		    ),
		    'conditions' => array(
		        'UserPostShare.post_id'=> $post_id,
		    		'UserPostShare.status'=> 1,
		    ),
    			'fields' => array('UserJoin.*', 'UserPostShare.*'),
    			'order' => 'UserPostShare.id DESC'
		));
		if(count($share_array) > 0){
			foreach($share_array as $share){
				$share_details[] = array(
									'share_id' => $share['UserPostShare']['id'],
									'share_by' => $share['UserJoin']['first_name'].' '.$share['UserJoin']['last_name'],
									);
			}
		}
        	return $share_details;
    }
    /*
	On: 19-02-2016
	I/P:
	O/P:
	Desc: 
    */
    public function viewPostDetails(){
    	$this->layout = NULL;
    	$userPostData = array();
    	$postId = $this->request->data['post_id'];
    	$userPostData = $this->UserPost->userPostLists( $postId );
    	$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userPostData[0]['UserPost']['user_id'])));
            if(!empty($userDetails)){
                $name = $userDetails['UserProfile']['first_name'].' '.$userDetails['UserProfile']['last_name'];
                $email = isset($userDetails['User']['email']) ? $userDetails['User']['email'] : '';
                $userData['User'] = array("name"=> $name, "email"=> $email);
            }else{
                $userData['User'] = array("name"=> '', "email"=> '');
            }
    $userPostData[0]['specialitystring'] = $this->speciality_string($userPostData[0]['UserPostSpecilities']);
    $userPostData[0]['beat_details'] = $this->get_beats_details($postId);
    $userPostData[0]['share_details'] = $this->get_share_details($postId);
    $userPostData[0]['comment_details'] = $this->viewPostComments($postId);
    $userPosts = array_merge($userPostData, $userData);
    	$this->set("userPostdata", $userPosts);
    	echo $this->render('Admin.UserPost/userPostDetails');
    	exit;
    }
    /*
	On: 22-02-2016
	I/P:
	O/P:
	Desc: 
    */
    public function toggleTopPost(){
    	$postId = $this->request->data['post_id'];
	    	if( !empty($postId) ){
	    		$userPostData = $this->UserPost->userPostLists( $postId );
	    		if($userPostData[0]['UserPost']['top_beat'] == 0){
	    			$update_status = 1;
	    		}else{
	    			$update_status = 0;
	    		}
	    	}
	    	$conditions = array('UserPost.id'=> $postId);
        $data = array(
                    'UserPost.top_beat'=> $update_status,
                );

        if( $this->UserPost->updateAll( $data ,$conditions )){
            $statusString = 'Top Beat';
            if( $update_status == 1 ){ $statusString = 'Top Beat'; }
            if( $update_status == 0 ){ $statusString = 'Not a top beat'; }
            echo "OK~$statusString";
        }else{
            echo "ERROR~".ERROR_615;
        }
        exit;
    }
    /*
	On: 22-02-2016
	I/P:
	O/P:
	Desc: 
    */
    public function flagPostView(){
    	$this->layout = NULL;
    	$postSpamData = array();
    	$postId = $this->request->data['post_id'];
    $spamContents = $this->PostSpamContent->spamContentList();
	if( !empty($spamContents) ){
		foreach( $spamContents as $spamC){
			$postSpamData[] = array("id"=> $spamC['PostSpamContent']['id'], "spam_name"=> $spamC['PostSpamContent']['spam_name']);
		}
	}
	$spam_details = array('spam_type' => $postSpamData,'post_id' => $postId);
	$this->set("userPostdata", $spam_details);
    	echo $this->render('Admin.UserPost/flagPostview');
    	exit;
    }
    public function markFlag(){
    			$this->layout = NULL;
    			$postId = $this->request->data['post_id'];
    			$comment = $this->request->data['comment'];
    			$spam_type = $this->request->data['spam_type'];
    			$conditions = array('UserPost.id'=> $postId);
        		$data = array(
                    'UserPost.spam_confirmed	'=> 1,
                );

        		if( $this->UserPost->updateAll( $data ,$conditions )){
        			$spamPostData = array(
    								"user_post_id"=> $postId,
    								"content"=> $comment,
    								"post_spam_content_id"=> $spam_type,
    				);
    				$spamPost = $this->AdminPostSpam->save( $spamPostData );
    				$spamReason = $this->PostSpamContent->find("first", array("conditions"=> array("id"=> $spam_type)));
    				$beatDetails = $this->UserPost->find("first", array("conditions"=> array("UserPost.id"=> $postId)));
				$beatOwnerDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $beatDetails['UserPost']['user_id'])));
        				$params = array();
					$params['subject'] = 'Your Beat marked as Spam';
					$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Mark Spam Beat to beat owner by admin')));
					$params['temp'] = $templateContent['EmailTemplate']['content'];
					$params['toMail'] = $beatOwnerDetails['User']['email'];
					//$params['toMail'] = 'devi@mediccreations.com';
					$params['from'] = NO_REPLY_EMAIL;
					$params['name'] = $beatOwnerDetails['UserProfile']['first_name'] . " " . $beatOwnerDetails['UserProfile']['last_name'] ;
					$params['beat_title'] = $beatDetails['UserPost']['title'];
					$params['spam_reason'] = $spamReason['PostSpamContent']['spam_name'];
					$params['comment'] = $comment;
					
					try{
						if(!empty($params['toMail'])){
							$this->Email->markSpamBeatEmailToBeatOwnerByAdmin( $params );
						}
					}catch(Exception $e){}
					echo 'success';exit();
        		}else{
        			echo 'failure';exit();
        		}
    			
    			
    }
    public function unFlagbeat(){
    			$this->layout = NULL;
    			$postSpamData = array();
    			$postId = $this->request->data['post_id'];
    			$conditions = array('UserPost.id'=> $postId);
    			$data = array(
                    'UserPost.spam_confirmed	'=> 0,
                );

        		if( $this->UserPost->updateAll( $data ,$conditions )){
    				$this->AdminPostSpam->deleteAll(array('AdminPostSpam.user_post_id' => $postId), false);
    				$beatDetails = $this->UserPost->find("first", array("conditions"=> array("UserPost.id"=> $postId)));
				$beatOwnerDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $beatDetails['UserPost']['user_id'])));
        				$params = array();
					$params['subject'] = 'Your Beat  unmarked as Spam';
					$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Unmark Spam Beat to beat owner by admin')));
					$params['temp'] = $templateContent['EmailTemplate']['content'];
					//$params['toMail'] = $beatOwnerDetails['User']['email'];
					$params['toMail'] = 'devi@mediccreations.com';
					$params['from'] = NO_REPLY_EMAIL;
					$params['name'] = $beatOwnerDetails['UserProfile']['first_name'] . " " . $beatOwnerDetails['UserProfile']['last_name'] ;
					$params['beat_title'] = $beatDetails['UserPost']['title'];
					try{
						if(!empty($params['toMail'])){
							$this->Email->unmarkSpamBeatEmailToBeatOwnerByAdmin( $params );
						}
					}catch(Exception $e){}
					echo 'success';exit();
        		}else{
        			echo 'failure';exit();
        		}
    }
}