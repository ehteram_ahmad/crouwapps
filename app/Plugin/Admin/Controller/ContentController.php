<?php
class ContentController extends AdminAppController {
    public $components = array('Cookie','Session','Common','Paginator');
    
    public $helpers = array('Html','Js','Form','Session','Time');


    public $name = "Content";
    public $uses = array('Admin.Content');


    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
        'order' => array(
        'Content.content_added' => 'desc'
        )
    );

    /*
    On: 
    I/P:
    O/P:
    Desc:
    */
    public function listContents(){
        $limit=20;
        $options=array(
            'conditions'=>array('Content.content_status'=>array(0,1)),
            'order'=>'Content.added_date DESC',
            'limit'=>$limit,
            );
         $this->Paginator->settings = $options;
         $data = $this->Paginator->paginate('Content');
         $this->set(array('Contents'=>$data,'limit'=>$limit));
    }
    public function contentFilter(){
        $this->layout=null;
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='name'){
                $sort="Content.content_title $order";
            }elseif ($con == "created") {
                $sort="Content.added_date $order";
            }
        }
        else{
            $con="";
            $type="";
            $sort='Content.added_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        if(isset($this->data['status']) & $this->data['status']!==""){
            $status=array('Content.content_status'=>$this->data['status']);
        }
        else{
            $status=array('Content.content_status'=>array(0,1));
        }
        $options=array(
            'conditions'=>$status,
            'order'=> $sort,
            'limit'=> $limit,
            'page'=>$j
            );
         $this->Paginator->settings = $options;
         $data = $this->Paginator->paginate('Content');
         $this->set(array('Contents'=>$data,'limit'=>$limit));
         $this->render('/Elements/content_filter');
    }
    public function addContents(){

            if ($this->request->data) {
                if ($this->Content->validates()) {    
                    $data = array(
                        // 'content_id' => $id, 
                        'content_title' => $this->request->data['editContent']['title'],
                        'content_text' => $this->request->data['editContent']['content'],
                        'content_status'=>'1'
                        // 'edit_date' => date("Y-m-d H:i:s")
                    );
                    // This will update page with id = $id
                    //$this->Content->content_id = $id;
                    $this->Content->saveAll($data);
                    $this->redirect(array('action' => 'listContents'));
                }else{
                    $errors = $this->Content->validationErrors;
                    $this->set('errors', $errors);
                } 
                }
    }
    public function editContents($id){
        if ($this->request->data) {
            if ($this->Content->validates()) {    
                $data = array(
                    // 'content_id' => $id, 
                    'content_title' => "'".$this->request->data['editContent']['title']."'",
                    'content_text' => "'".$this->request->data['editContent']['content']."'",
                    'content_status'=>'1'
                    // 'edit_date' => date("Y-m-d H:i:s")
                );
                // This will update page with id = $id
                //$this->Content->content_id = $id;
                $this->Content->updateAll($data,array("Content.content_id"=> $id));
                $this->redirect(array('action' => 'listContents'));
            }else{
                $errors = $this->Content->validationErrors;
                $this->set('errors', $errors);
            }
        }
        $contentData = $this->Content->find('first', array('conditions'=> array('Content.content_id'=> $id)));
        $this->set('contentData', $contentData);
    }
    public function deleteContent(){
        if(isset($this->request->data['id'])){
            $updateContent = $this->Content->updateAll(array("Content.content_status"=> 2), array("Content.content_id"=> $this->request->data['id']));
            if($updateContent){
                echo "Content Deleted";
            }else{
                echo "Some issue occured. Please try again.";
            }
        }else{
            echo "Content Not Deleted! Try Again.";
        }
        exit;
    }
    public function changeContentStatus(){
        $contentId = $this->request->data('contentId');
        $contentStatus = $this->request->data('status');
        if( trim($contentStatus) == 'Active' ){ 
            $chngeStatus = 0;
        }else if( trim($contentStatus) == 'Inactive' ){ 
            $chngeStatus = 1;
        }
        $conditions = array('Content.content_id'=> $contentId);
        $data = array(
                    'Content.content_status'=> $chngeStatus,
                );

        if( $this->Content->updateAll( $data ,$conditions )){
            
            if( $chngeStatus == 1 ){ $statusString = 'Active'; }
            if( $chngeStatus == 0 ){ $statusString = 'Inactive'; }
            echo "OK~$statusString";
        }else{
            echo "ERROR~".ERROR_615;
        }
        exit;
    }
    public function contentdetail($id){
          $value = $this->Content->find('all',array('conditions'=>array('Content.content_id'=>$id)));  
          $this->set('data',$value);
    }
}