<?php
class AppVersionController extends AdminAppController {
    public $uses = array('Admin.AppVersion','Admin.MbAppVersion');
    public $components = array('RequestHandler','Paginator','Session');
    public $helpers = array('Js','Html', 'Paginator');

    /*
    ---------------------------------------------------------------------
    On:
    I/P:
    O/P:
    Desc: Manage App version for Android/ios
    ---------------------------------------------------------------------
    */
    public function manageAppVersion(){
        if(isset($this->request->data['appVersion']) && !empty($this->request->data['appVersion'])){

            $appVersion=$this->request->data['appVersion'];

            $androidAppVersionOCR = $appVersion['androidOcr'];
            $iosAppVersionOCR = $appVersion['iosOcr'];
            $webAppVersionOCR = $appVersion['webOcr'];

            $androidAppVersionMB = $appVersion['androidMb'];
            $iosAppVersionMB = $appVersion['iosMb'];
            $webAppVersionMB = $appVersion['webMb'];

            //** Update Android Version
            $updateAndroidDataOCR = array("version"=> "'". $androidAppVersionOCR ."'",);
            $updateAndroidDataMB = array("version"=> "'". $androidAppVersionMB ."'",);
            $updateAndroidVersionOCR = $this->AppVersion->updateAll($updateAndroidDataOCR, array("application_type"=>"OCR","device_type"=> "Android"));
            $updaMBteAndroidVersionMB = $this->AppVersion->updateAll($updateAndroidDataMB, array("application_type"=>"MB","device_type"=> "Android"));
            //** Update Ios version
            $updateIosDataOCR = array("version"=> "'". $iosAppVersionOCR ."'",);
            $updateIosDataMB = array("version"=> "'". $iosAppVersionMB ."'",);
            $updateIosVersionOCR = $this->AppVersion->updateAll($updateIosDataOCR, array("application_type"=>"OCR","device_type"=> "ios"));
            $updateIosVersionMB = $this->AppVersion->updateAll($updateIosDataMB, array("application_type"=>"MB","device_type"=> "ios"));

            //** Update Web version
            $updateWebDataOCR = array("version"=> "'". $webAppVersionOCR ."'",);
            $updateWebDataMB = array("version"=> "'". $webAppVersionMB ."'",);
            $updateIosVersionOCR = $this->AppVersion->updateAll($updateWebDataOCR, array("application_type"=>"OCR","device_type"=> "web"));
            $updateIosVersionMB = $this->AppVersion->updateAll($updateWebDataMB, array("application_type"=>"MB","device_type"=> "web"));
        }
        $appVersions = $this->AppVersion->find("all"); 
        $this->set("appVersions", $appVersions);
    }
    /*
    ---------------------------------------------------------------------
    On:
    I/P:NULL
    O/P:NULL
    Desc: Add App version for Android/ios on MB
    ---------------------------------------------------------------------
    */
    public function manageMbAppVersion(){
        if($this->data){
            $data=$this->data['add'];
            if($data['device']=="0"){
                $device="Android";
            }elseif ($data['device']=="1") {
               $device="ios";
            }
            $newData=array('version'=>$data['version'],'force_update'=>$data['update'],'update_frequency'=>$data['frequency'],'device_type'=>$device,'status'=>1);
            $this->MbAppVersion->saveAll($newData);
            $this->redirect(array('plugin'=>'admin','controller'=>'AppVersion','action'=>'manageMbAppVersion'));
        }
        $appVersions = $this->MbAppVersion->find("all"); 
        $this->set("appVersions", $appVersions);
    }
}