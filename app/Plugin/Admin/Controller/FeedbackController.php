<?php
class FeedbackController extends AdminAppController {
    public $uses = array('UserFeedback','AdminFeedbackResponse');
    public $components = array('RequestHandler','Paginator','Session', 'Admin.AdminEmail');
    public $helpers = array('Js','Html');

    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
        'order'=> array("UserFeedback.created" => "desc")
    );

    /*
    -----------------------------------------------------------------------------------
    ON: 
    I/P: 
    O/P: 
    Desc: 
    -----------------------------------------------------------------------------------
    */
    public function feedbackList(){
        $limit=20;
        $options=array( 
            'conditions'=> array('UserFeedback.status'=>array(0,1)),
            'order'=> array("UserFeedback.created" => "desc"),
            'limit'=> $limit,
        );
        $this->Paginator->settings = $options;
        $feedbackLists = $this->Paginator->paginate('UserFeedback'); 
        foreach ($feedbackLists as $feedId) {
            $id=$feedId['UserFeedback']['id'];
            $feedResp[]=$this->AdminFeedbackResponse->find('all',array('conditions'=>array('feedback_id'=>$id)));
        }
        $this->set(array('feedbackLists'=>$feedbackLists,'limit'=>$limit,'feedResp'=>$feedResp));
    }
    public function feedbackFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='name'){
                $sort="UserProfile.first_name $order";
            }elseif($con=='email') {
                $sort="User.email $order";
            }elseif($con=='Subject') {
                $sort="UserFeedback.subject $order";
            }elseif($con=='Source') {
                $sort="UserFeedback.feedback_source $order";
            }else{
               $sort="UserFeedback.created $order";
            }
        }
        else{
            $con="";
            $type="";
            $sort='UserFeedback.created DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
            $options=array( 
                'conditions'=> array('UserFeedback.status'=>array(0,1)),
                'order'=> $sort,
                'limit'=> $limit,
                'page'=>$j
            );
        $this->Paginator->settings = $options;
        $feedbackLists = $this->Paginator->paginate('UserFeedback'); 
        foreach ($feedbackLists as $feedId) {
            $id=$feedId['UserFeedback']['id'];
            $feedResp[]=$this->AdminFeedbackResponse->find('all',array('conditions'=>array('feedback_id'=>$id)));
        }
        $this->set(array('feedbackLists'=>$feedbackLists,'limit'=>$limit,'feedResp'=>$feedResp));
        $this->render('/Elements/feedback_filter');
    }
    public function deleteFeedback(){
        if(isset($this->request->data['Id'])){
            $updateFeedback = $this->UserFeedback->updateAll(array("UserFeedback.status"=> 2), array("UserFeedback.id"=> $this->request->data['Id']));
            if($updateFeedback){
                echo "Feedback Deleted";
            }else{
                echo "Some issue occured. Please try again.";
            }
        }else{
            echo "Feedback Not Deleted! Try Again.";
        }
        exit;
    }

    public function feedbackResponse($id){
        if($this->request->data){
            $resData=$this->data['sendmail'];
            $responseData=array('feedback_id'=>$id,'title'=>$resData['subject'],'email_id'=>$resData['to'],'content'=>$resData['content'],'status'=>1);
            $this->AdminFeedbackResponse->saveAll($responseData);

            $params['temp'] = $resData['content'];
            $params['subject'] = $resData['subject'];
            $params['toMail'] = $resData['to'];
            $params['registration_source'] = $resData['source'];
            try{
                $mailSend = $this->AdminEmail->sendMailByAdmin( $params );

                $mailData=array('user_id'=>$resData['id'],'title'=>$resData['subject'],'email_id'=>$resData['to'],'content'=>$resData['content'],'status'=>1);

                $this->AdminFeedbackResponse->saveAll($mailData);

                echo $mailSend;
            }catch(Exception $e){
            }
            
            $this->redirect(BASE_URL . 'admin/feedback/feedbackList');
        }
        $data=$this->UserFeedback->find('first',array('conditions'=>array('UserFeedback.id'=>$id)));
        $this->set('feedbackLists',$data);
    }



}