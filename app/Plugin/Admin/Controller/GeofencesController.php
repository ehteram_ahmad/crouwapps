<?php
class GeofencesController extends AdminAppController {
    public $uses = array('Admin.CompanyGeofenceParameter','Admin.CompanyName');
    public $components = array('RequestHandler','Paginator','Session', 'Common', 'Image');
    public $helpers = array('Js','Html', 'Paginator');

    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
    );

    /*
    On: 30-08-2017
    I/P:
    O/P: 
    Desc: Geofence Listing Function
    */

    public function geofenceList()
    {
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $conditions=array('CompanyGeofenceParameter.status'=>1);
        $fields = array("CompanyName.company_name,CompanyGeofenceParameter.company_id,CompanyGeofenceParameter.company_address,CompanyGeofenceParameter.latitude,CompanyGeofenceParameter.longitude,CompanyGeofenceParameter.radius,CompanyGeofenceParameter.status,CompanyGeofenceParameter.created");

        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'company_names',
                      'alias' => 'CompanyName',
                      'type' => 'left',
                      'conditions'=> array('CompanyGeofenceParameter.company_id = CompanyName.id')
                  ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'order'=> 'CompanyGeofenceParameter.id DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        $tCount=$this->CompanyGeofenceParameter->find('count',array('conditions'=>$conditions,'fields'=>$fields));

        $this->Paginator->settings = $options;
        $data = $this->Paginator->paginate('CompanyGeofenceParameter');
        // $geofenceListData = $this->CompanyGeofenceParameter->query('SELECT CompanyName.company_name,CompanyGeofenceParameter.company_id,CompanyGeofenceParameter.company_address,CompanyGeofenceParameter.latitude,CompanyGeofenceParameter.longitude,CompanyGeofenceParameter.radius,CompanyGeofenceParameter.status,CompanyGeofenceParameter.created FROM `company_geofence_parameters` AS CompanyGeofenceParameter LEFT JOIN company_names AS CompanyName ON CompanyGeofenceParameter.company_id = CompanyName.id  ORDER BY CompanyGeofenceParameter.id DESC');
        //print_r($data);
        $this->set(array("geofenceListData"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
    }

    /*
    On: 30-08-2017
    I/P:
    O/P: 
    Desc: Add Geofence Data 
    */

    public function addGeofenceData()
    {
        $companyDetail = $this->CompanyName->query("SELECT id,company_name FROM company_names WHERE is_subscribed =1 AND status =1 AND created_by = 0");
        $this->set(array("companyDetail"=>$companyDetail));
    }

    /*
    On: 30-08-2017
    I/P:
    O/P: 
    Desc: Add Geofence Data 
    */

    public function insertGeofencData(){
            if ($this->request->data) {  
                $data = array(
                    'company_id' => $this->request->data['geofence']['company_id'],
                    'company_address' => $this->request->data['geofence']['company_address'],
                    'latitude'=> $this->request->data['geofence']['latitude'],
                    'longitude'=> $this->request->data['geofence']['longitude'],
                    'radius'=> $this->request->data['geofence']['radius'],
                    'status'=>'1',
                    'created' => date("Y-m-d H:i:s")
                );
                $this->CompanyGeofenceParameter->saveAll($data);
                $this->redirect(array('action' => 'geofenceList'));
            }
    }

    /*
    On: 30-08-2017
    I/P:
    O/P: 
    Desc: Add Geofence Data 
    */

    public function editGeofenceData()
    {
        $company_id = $this->request->params['pass'][0];
        $geofenceEditData = $this->CompanyGeofenceParameter->query("SELECT CompanyName.company_name,CompanyGeofenceParameter.company_id,CompanyGeofenceParameter.company_address,CompanyGeofenceParameter.latitude,CompanyGeofenceParameter.longitude,CompanyGeofenceParameter.radius,CompanyGeofenceParameter.status,CompanyGeofenceParameter.created FROM `company_geofence_parameters` AS CompanyGeofenceParameter LEFT JOIN company_names AS CompanyName ON CompanyGeofenceParameter.company_id = CompanyName.id WHERE CompanyName.id = $company_id ORDER BY CompanyGeofenceParameter.id DESC");
        //echo "<pre>";print_r($geofenceEditData);exit();
        $this->set(array("geofenceEditData"=>$geofenceEditData));
    }

    /*
    On: 30-08-2017
    I/P:
    O/P: 
    Desc: Add Geofence Data 
    */

    public function updateGeofenceData()
    {
        //print_r($this->request->data);exit();
        $geofenceData = array(
                            "latitude"=> $this->request->data['geofence']['latitude'],
                            "longitude"=> $this->request->data['geofence']['longitude'],
                            "radius"=> $this->request->data['geofence']['radius']
                            //'created' => date("Y-m-d H:i:s")
                        );
        $updateConditions = array("company_id"=> $this->request->data['geofence']['company_id']);
        $updateGeofence = $this->CompanyGeofenceParameter->updateAll($geofenceData, $updateConditions);
        $this->redirect(array('action' => 'geofenceList'));
    }

    /*
    On: 31-08-2017
    I/P:
    O/P: 
    Desc: Add Geofence Data 
    */


    public function geofenceFilter()
    {
        $this->layout=null;
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='company_address'){
              $sort="CompanyGeofenceParameter.id $order";
            }
            else{
            $sort="CompanyGeofenceParameter.id $order";
            }
        }
        else{
            $sort="CompanyGeofenceParameter.id ASC";
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        if(isset($this->data['companyAddress'])){
            $companyaddress=trim($this->data['companyAddress']);
            $companyaddress=array('LOWER(CompanyGeofenceParameter.company_address) LIKE'=>strtolower($companyaddress.'%'));
        }
        else{
            $companyaddress="";
        }
        $conditions=$companyaddress;
        $fields = array("CompanyName.company_name,CompanyGeofenceParameter.company_id,CompanyGeofenceParameter.company_address,CompanyGeofenceParameter.latitude,CompanyGeofenceParameter.longitude,CompanyGeofenceParameter.radius,CompanyGeofenceParameter.status,CompanyGeofenceParameter.created");

        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'company_names',
                      'alias' => 'CompanyName',
                      'type' => 'left',
                      'conditions'=> array('CompanyGeofenceParameter.company_id = CompanyName.id')
                  ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'order'=> 'CompanyGeofenceParameter.id DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        $tCount=$this->CompanyGeofenceParameter->find('count',array('conditions'=>$conditions,'fields'=>$fields));

        $this->Paginator->settings = $options;
        $data = $this->Paginator->paginate('CompanyGeofenceParameter');
        $this->set(array('geofenceListData'=>$data,'limit'=>$limit,'j'=>$j));
        $this->render('/Elements/geofence_filtered_data');
    }
}