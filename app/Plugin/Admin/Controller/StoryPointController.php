<?php
class StoryPointController extends AdminAppController {
    public $components = array('Cookie','Session','Common','Paginator');
    public $helpers = array('Html','Js','Form','Session','Time');
    public $uses = array('Admin.TopStoryPoint');

    /*
	On: 19-11-2015
	I/P:
	O/P:
	Desc: Manage story point by admin which will reflect top story posts(Beat) display.
    */
    public function manageStoryPoint(){
    	if( isset($this->request->data['storyPoint']) && !empty($this->request->data['storyPoint']) ){
            $storyPoint=$this->data['storyPoint'];
    		try{
    			//** Update Top Shared
    			$this->TopStoryPoint->updateAll( array("point"=> $storyPoint['share']), array("name"=> 'Top Shared') );
    			//** Update Up Beat
    			$this->TopStoryPoint->updateAll( array("point"=> $storyPoint['upbeat']), array("name"=> 'Up Beat') );
    			//** Update Top Comment
    			$this->TopStoryPoint->updateAll( array("point"=> $storyPoint['comments']), array("name"=> 'Top Comment') );
    			//** Update Top Follow
    			$this->TopStoryPoint->updateAll( array("point"=> $storyPoint['downbeat']), array("name"=> 'Down Beat') );
    		}catch( Exception $e ){}
    	}
    	$storyPointData = $this->TopStoryPoint->find("all");
    	$this->set("storyPoints", $storyPointData);
    }
}