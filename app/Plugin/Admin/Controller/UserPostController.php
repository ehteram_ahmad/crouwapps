<?php
class UserPostController extends AdminAppController {
    public $components = array('Admin.Image','RequestHandler', 'Cookie','Session','Common','Paginator', 'Admin.AdminEmail');
    
    public $helpers = array('Html','Js','Form','Session','Time', 'Paginator', 'Admin.Csv');

    public $uses = array('Admin.UserPost','Admin.UserFollow','Admin.UserColleague','Admin.User','Admin.UserPostAttribute','Admin.UserPostSpecilities','Admin.Specilities','Admin.HashTag','Admin.BeatHashMapping','Admin.BeatActivityLog','UserPostBeat','UserPostShare','PostSpamContent','AdminPostSpam','EmailTemplate','AdminUserSentMail','User','AdminEmailCategorie','AdminEmailTemplate','AdminPostSpam','NotificationQueque','FeaturedActivityLog');

    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
        'order' => array(
            'UserPost.id' => 'desc'
        )
    );
    public $ImageSizeToUpload = 10485760;
    public $DocSizeToUpload = 10485760;
    public $ImageSizeLimitToCheckUpload = 2097152;

    /*
  On: 02-09-2015
  I/P: 
  O/P: 
  Desc: All user post lists
    */
    public function userPostLists(){ 
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $userPostData = array(); $userPosts = array();
        //** Get interest lists [START]
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $interestLists['interestlist'] = $interestlists; 
        $this->set('interestLists', $interestLists);
        //** Get interest lists [END]
        
        //** get Post Lists
         
        $conditions = array('UserPost.status'=>array(0,1));
        $fields = array("User.status,User.id,UserPost.id, UserPost.title, UserPost.post_date, UserPost.status, UserPost.top_beat, UserPost.spam_confirmed, UserProfile.first_name, UserProfile.last_name");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'left',
                      'conditions'=> array('UserPost.user_id = User.id')
                    ),
                     array(
                      'table' => 'beat_views',
                      'alias' => 'BeatView',
                      'type' => 'left',
                      'conditions'=> array('BeatView.post_id = User.id')
                    ),
                    array(
                      'table' => 'user_post_specilities',
                      'alias' => 'UserPostSpecilities',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.user_post_id = UserPost.id')
                    ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.specilities_id = UserSpecility.id')
                    ),
                   
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                 'group'=> array('UserPost.id'),
                'order'=> 'UserPost.post_date DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        $this->Paginator->settings = $options;
        $userPostData = $this->Paginator->paginate('UserPost');
        foreach($userPostData as $userPost){
                $userPost['speciality_list'] = $this->speciality_string($userPost['UserPostSpecilities']);
                $userPost['beat_details'] = $this->get_beats_details($userPost['UserPost']['id']);
                $userPost['share_details'] = count($this->get_share_details($userPost['UserPost']['id']));
                $userPost['mail'] = count($this->get_admin_user_mail($userPost['UserPost']['id']));
                $userPosts[] = $userPost;
        }
        $data=array($this->UserPost->find("count",array('conditions'=>array('UserPost.top_beat'=>1))));
        $this->set(array("userPostData"=>$userPosts,"condition"=>$conditions,"featuredCount"=>$data,"limit"=>$limit));
    }
    public function allFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='title'){
              $sort="UserPost.title $order";
            }
            else{
            $sort="UserPost.post_date $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='UserPost.post_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        // pr($limit);
        $sortPost=$this->data;
        if(!isset($sortPost['to']) || $sortPost['to']==""){
          $endDate = date("Y-m-d");
        }
        else{
          $endDate = date('Y-m-d', strtotime($sortPost['to']));
        }
        if(!isset($sortPost['from']) || $sortPost['from']==""){
          $date = array();
        }
        else{
          $startDate = date('Y-m-d', strtotime($sortPost['from']));
          $date=array('UserPost.post_date <= ' => $endDate." 23:59:59",'UserPost.post_date >= ' => $startDate);
        }
        if(!isset($sortPost['textBeatTitle']) || $sortPost['textBeatTitle']==""){
            $title=array();
        }
        else{
            $sortPost['textBeatTitle']=trim($sortPost['textBeatTitle']);
            $title=array('LOWER(UserPost.title) LIKE'=>strtolower($sortPost['textBeatTitle'].'%'));
        }
        if(!isset($sortPost['textBeatPostedBy'])||$sortPost['textBeatPostedBy']==""){
            $name=array();
        }
        else{ 
          $sortPost['textBeatPostedBy']=trim($sortPost['textBeatPostedBy']);
          $name=explode(" ",$sortPost['textBeatPostedBy']);
          $len=count($name);
          for ($i=0; $i < $len; $i++) { 
            if(!$name[$i]==""){
              $postedBy[]=$name[$i];
            }
          }
          $count=count($postedBy);
          if($count!=2){
            $name=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textBeatPostedBy'].'%'));
          }
          else{
            $name=array('LOWER(UserProfile.first_name) ='=>strtolower($postedBy[0]),'LOWER(UserProfile.last_name) ='=>strtolower($postedBy[1]));
          }
        }
        if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
            $interest=array();
        }
        else{
            $interest=array('UserPostSpecilities.specilities_id'=>$sortPost['specilities']);
        }
        if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==""){
            $status=array('UserPost.status'=>array(0,1));
        }
        else{
            if($sortPost['statusVal']==3){
              $status=array('UserPost.status'=>array(0,1),'UserPost.spam_confirmed'=>'0','UserPost.top_beat'=>'1');
            }
            else if($sortPost['statusVal']==2){
                $status=array('UserPost.status'=>array(0,1),'UserPost.spam_confirmed'=>'1');
            }
            else if($sortPost['statusVal']==4){
                $status=array('UserPost.status'=>array(2,3,4));
            }
            else{
            $status=array('UserPost.status'=>$sortPost['statusVal']);
            }
         }
         $conditions=array_merge($title,$name,$status,$interest,$date);
         $fields = array("User.status,User.id,UserPost.id, UserPost.title, UserPost.post_date, UserPost.status, UserPost.top_beat, UserPost.spam_confirmed, UserProfile.first_name, UserProfile.last_name");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'left',
                      'conditions'=> array('UserPost.user_id = User.id')
                    ),
                     array(
                      'table' => 'beat_views',
                      'alias' => 'BeatView',
                      'type' => 'left',
                      'conditions'=> array('BeatView.post_id = User.id')
                    ),
                    array(
                      'table' => 'user_post_specilities',
                      'alias' => 'UserPostSpecilities',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.user_post_id = UserPost.id')
                    ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.specilities_id = UserSpecility.id')
                    ),
                   
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                 'group'=> array('UserPost.id'),
                'order'=> $sort, 
                'limit'=> $limit,
                'page'=>$j
                );
        $this->Paginator->settings = $options;
        $userPostData = $this->Paginator->paginate('UserPost');
        
        if(isset($userPostData) && !empty($userPostData)){
            foreach($userPostData as $userPost){

                $userPost['speciality_list'] = $this->speciality_string($userPost['UserPostSpecilities']);
                $userPost['beat_details'] = $this->get_beats_details($userPost['UserPost']['id']);
                $userPost['share_details'] = count($this->get_share_details($userPost['UserPost']['id']));
                $userPost['mail'] = count($this->get_admin_user_mail($userPost['UserPost']['id']));
                $userPosts[] = $userPost;
            }
        }
        else{
          $userPosts=array();
        }
        
        $data=array($this->UserPost->find("count",array('conditions'=>array('UserPost.top_beat'=>1))));
        // pr($con);
        $this->set(array("userPostData"=>$userPosts,"condition"=>$conditions,"featuredCount"=>$data,"limit"=>$limit,'con'=>$con,'order'=>$type));
        $this->render('/Elements/userposts/allPosts');
    }

    public function flaggedPostLists(){
      if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $userPostData = array(); $userPosts = array();
        //** Get interest lists [START]
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $interestLists['interestlist'] = $interestlists; 
        $this->set('interestLists', $interestLists);
        //** Get interest lists [END]
        
        //** get flagged Post Lists
         
        $conditions = array('UserPost.top_beat'=>0,'UserPost.spam_confirmed'=>1,'UserPost.status'=>array(0,1));
        $fields = array("User.status,User.id,UserPost.id, UserPost.title, UserPost.post_date, UserPost.status, UserPost.top_beat, UserPost.spam_confirmed, UserProfile.first_name, UserProfile.last_name");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'left',
                      'conditions'=> array('UserPost.user_id = User.id')
                    ),
                     array(
                      'table' => 'beat_views',
                      'alias' => 'BeatView',
                      'type' => 'left',
                      'conditions'=> array('BeatView.post_id = User.id')
                    ),
                    array(
                      'table' => 'user_post_specilities',
                      'alias' => 'UserPostSpecilities',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.user_post_id = UserPost.id')
                    ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.specilities_id = UserSpecility.id')
                    ),
                   
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                 'group'=> array('UserPost.id'),
                'order'=> 'UserPost.post_date DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        
        $this->Paginator->settings = $options;
        $userPostData = $this->Paginator->paginate('UserPost');

        foreach($userPostData as $userPost){

                $userPost['speciality_list'] = $this->speciality_string($userPost['UserPostSpecilities']);
                $userPost['beat_details'] = $this->get_beats_details($userPost['UserPost']['id']);
                $userPost['share_details'] = count($this->get_share_details($userPost['UserPost']['id']));
                $userPost['mail'] = count($this->get_admin_user_mail($userPost['UserPost']['id']));
                $userPosts[] = $userPost;
        }
        $data=array($this->UserPost->find("count",array('conditions'=>array('UserPost.top_beat'=>1))));
        $this->set(array("userPostData"=>$userPosts,"condition"=>$conditions,"featuredCount"=>$data,"limit"=>$limit));  
    }
    public function flaggedFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='title'){
              $sort="UserPost.title $order";
            }
            else{
            $sort="UserPost.post_date $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='UserPost.post_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        // pr($limit);
        $sortPost=$this->data;
        if(!isset($sortPost['to']) || $sortPost['to']==""){
          $endDate = date("Y-m-d");
        }
        else{
          $endDate = date('Y-m-d', strtotime($sortPost['to']));
        }
        if(!isset($sortPost['from']) || $sortPost['from']==""){
          $date = array();
        }
        else{
          $startDate = date('Y-m-d', strtotime($sortPost['from']));
          $date=array('UserPost.post_date <= ' => $endDate." 23:59:59",'UserPost.post_date >= ' => $startDate);
        }
        if(!isset($sortPost['textBeatTitle']) || $sortPost['textBeatTitle']==""){
            $title=array();
        }
        else{
            $sortPost['textBeatTitle']=trim($sortPost['textBeatTitle']);
            $title=array('LOWER(UserPost.title) LIKE'=>strtolower($sortPost['textBeatTitle'].'%'));
        }
        if(!isset($sortPost['textBeatPostedBy'])||$sortPost['textBeatPostedBy']==""){
            $name=array();
        }
        else{
            $sortPost['textBeatPostedBy']=trim($sortPost['textBeatPostedBy']);
          $name=explode(" ",$sortPost['textBeatPostedBy']);
          $len=count($name);
          for ($i=0; $i < $len; $i++) { 
            if(!$name[$i]==""){
              $postedBy[]=$name[$i];
            }
          }
          $count=count($postedBy);
          if($count!=2){
            $name=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textBeatPostedBy'].'%'));
          }
          else{
            $name=array('LOWER(UserProfile.first_name) ='=>strtolower($postedBy[0]),'LOWER(UserProfile.last_name) ='=>strtolower($postedBy[1]));
          }
        }
        if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
            $interest=array();
        }
        else{
            $interest=array('UserPostSpecilities.specilities_id'=>$sortPost['specilities']);
        }
        if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==""){
            $status=array('UserPost.top_beat'=>0,'UserPost.spam_confirmed'=>1,'UserPost.status'=>array(0,1));
        }
        else{
            $status=array('UserPost.top_beat'=>0,'UserPost.spam_confirmed'=>1,'UserPost.status'=>$sortPost['statusVal']);
        }
         $conditions=array_merge($title,$name,$status,$interest,$date);
         $fields = array("User.status,User.id,UserPost.id, UserPost.title, UserPost.post_date, UserPost.status, UserPost.top_beat, UserPost.spam_confirmed, UserProfile.first_name, UserProfile.last_name");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'left',
                      'conditions'=> array('UserPost.user_id = User.id')
                    ),
                     array(
                      'table' => 'beat_views',
                      'alias' => 'BeatView',
                      'type' => 'left',
                      'conditions'=> array('BeatView.post_id = User.id')
                    ),
                    array(
                      'table' => 'user_post_specilities',
                      'alias' => 'UserPostSpecilities',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.user_post_id = UserPost.id')
                    ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.specilities_id = UserSpecility.id')
                    ),
                   
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                 'group'=> array('UserPost.id'),
                'order'=> $sort, 
                'limit'=> $limit,
                'page'=>$j
                );
        $this->Paginator->settings = $options;
        $userPostData = $this->Paginator->paginate('UserPost');
        
        if(isset($userPostData) && !empty($userPostData)){
            foreach($userPostData as $userPost){

                $userPost['speciality_list'] = $this->speciality_string($userPost['UserPostSpecilities']);
                $userPost['beat_details'] = $this->get_beats_details($userPost['UserPost']['id']);
                $userPost['share_details'] = count($this->get_share_details($userPost['UserPost']['id']));
                $userPost['mail'] = count($this->get_admin_user_mail($userPost['UserPost']['id']));
                $userPosts[] = $userPost;
            }
        }
        else{
          $userPosts=array();
        }
        
        $data=array($this->UserPost->find("count",array('conditions'=>array('UserPost.top_beat'=>1))));
        $this->set(array("userPostData"=>$userPosts,"condition"=>$conditions,"featuredCount"=>$data,"limit"=>$limit,'con'=>$con,'order'=>$type));
        $this->render('/Elements/userposts/flaggedPosts');
    }

    public function featuredPostLists(){
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $userPostData = array(); $userPosts = array();
        //** Get interest lists [START]
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $interestLists['interestlist'] = $interestlists; 
        $this->set('interestLists', $interestLists);
        //** Get interest lists [END]
        
        //** get flagged Post Lists
        $conditions = array('UserPost.spam_confirmed'=>0,'UserPost.top_beat'=>1,'UserPost.status'=>array(0,1));
        $fields = array("User.status,User.id,UserPost.id, UserPost.title, UserPost.post_date, UserPost.status, UserPost.top_beat, UserPost.spam_confirmed, UserProfile.first_name, UserProfile.last_name");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'left',
                      'conditions'=> array('UserPost.user_id = User.id')
                    ),
                     array(
                      'table' => 'beat_views',
                      'alias' => 'BeatView',
                      'type' => 'left',
                      'conditions'=> array('BeatView.post_id = User.id')
                    ),
                    array(
                      'table' => 'user_post_specilities',
                      'alias' => 'UserPostSpecilities',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.user_post_id = UserPost.id')
                    ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.specilities_id = UserSpecility.id')
                    ),
                   
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                 'group'=> array('UserPost.id'),
                'order'=> 'UserPost.post_date DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        $this->Paginator->settings = $options;
        $userPostData = $this->Paginator->paginate('UserPost');
        
        foreach($userPostData as $userPost){

                $userPost['speciality_list'] = $this->speciality_string($userPost['UserPostSpecilities']);
                $userPost['beat_details'] = $this->get_beats_details($userPost['UserPost']['id']);
                $userPost['share_details'] = count($this->get_share_details($userPost['UserPost']['id']));
                $userPost['mail'] = count($this->get_admin_user_mail($userPost['UserPost']['id']));
                $userPosts[] = $userPost;
        }
        $data=array($this->UserPost->find("count",array('conditions'=>array('UserPost.top_beat'=>1))));
        $this->set(array("userPostData"=>$userPosts,"condition"=>$conditions,"featuredCount"=>$data,"limit"=>$limit)); 
    }
    public function featuredFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='title'){
              $sort="UserPost.title $order";
            }
            else{
            $sort="UserPost.post_date $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='UserPost.post_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $sortPost=$this->data;
        if(!isset($sortPost['to']) || $sortPost['to']==""){
          $endDate = date("Y-m-d");
        }
        else{
          $endDate = date('Y-m-d', strtotime($sortPost['to']));
        }
        if(!isset($sortPost['from']) || $sortPost['from']==""){
          $date = array();
        }
        else{
          $startDate = date('Y-m-d', strtotime($sortPost['from']));
          $date=array('UserPost.post_date <= ' => $endDate." 23:59:59",'UserPost.post_date >= ' => $startDate);
        }
        if(!isset($sortPost['textBeatTitle']) || $sortPost['textBeatTitle']==""){
            $title=array();
        }
        else{
            $sortPost['textBeatTitle']=trim($sortPost['textBeatTitle']);
            $title=array('LOWER(UserPost.title) LIKE'=>strtolower($sortPost['textBeatTitle'].'%'));
        }
        if(!isset($sortPost['textBeatPostedBy'])||$sortPost['textBeatPostedBy']==""){
            $name=array();
        }
        else{
           $sortPost['textBeatPostedBy']=trim($sortPost['textBeatPostedBy']);
          $name=explode(" ",$sortPost['textBeatPostedBy']);
          $len=count($name);
          for ($i=0; $i < $len; $i++) { 
            if(!$name[$i]==""){
              $postedBy[]=$name[$i];
            }
          }
          $count=count($postedBy);
          if($count!=2){
            $name=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textBeatPostedBy'].'%'));
          }
          else{
            $name=array('LOWER(UserProfile.first_name) ='=>strtolower($postedBy[0]),'LOWER(UserProfile.last_name) ='=>strtolower($postedBy[1]));
          }
        }
        if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
            $interest=array();
        }
        else{
            $interest=array('UserPostSpecilities.specilities_id'=>$sortPost['specilities']);
        }
        if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==""){
            $status=array('UserPost.top_beat'=>1,'UserPost.spam_confirmed'=>0,'UserPost.status'=>array(0,1));
        }
        else{
            $status=array('UserPost.top_beat'=>1,'UserPost.spam_confirmed'=>0,'UserPost.status'=>$sortPost['statusVal']);
         }
         $conditions=array_merge($title,$name,$status,$interest,$date);
         $fields = array("User.status,User.id,UserPost.id, UserPost.title, UserPost.post_date, UserPost.status, UserPost.top_beat, UserPost.spam_confirmed, UserProfile.first_name, UserProfile.last_name");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'left',
                      'conditions'=> array('UserPost.user_id = User.id')
                    ),
                     array(
                      'table' => 'beat_views',
                      'alias' => 'BeatView',
                      'type' => 'left',
                      'conditions'=> array('BeatView.post_id = User.id')
                    ),
                    array(
                      'table' => 'user_post_specilities',
                      'alias' => 'UserPostSpecilities',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.user_post_id = UserPost.id')
                    ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.specilities_id = UserSpecility.id')
                    ),
                   
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                 'group'=> array('UserPost.id'),
                'order'=> $sort, 
                'limit'=> $limit,
                'page'=>$j
                );
        $this->Paginator->settings = $options;
        $userPostData = $this->Paginator->paginate('UserPost');
        
        if(isset($userPostData) && !empty($userPostData)){
            foreach($userPostData as $userPost){

                $userPost['speciality_list'] = $this->speciality_string($userPost['UserPostSpecilities']);
                $userPost['beat_details'] = $this->get_beats_details($userPost['UserPost']['id']);
                $userPost['share_details'] = count($this->get_share_details($userPost['UserPost']['id']));
                $userPost['mail'] = count($this->get_admin_user_mail($userPost['UserPost']['id']));
                $userPosts[] = $userPost;
            }
        }
        else{
          $userPosts=array();
        }
        
        $data=array($this->UserPost->find("count",array('conditions'=>array('UserPost.top_beat'=>1))));
        $this->set(array("userPostData"=>$userPosts,"condition"=>$conditions,"featuredCount"=>$data,"limit"=>$limit,'con'=>$con,'order'=>$type));
        $this->render('/Elements/userposts/featuredPosts');
    }
    /*
    On: 02-09-2015
    I/P: 
    O/P:
    Desc: user post status change like Active/Inactive
    */
    public function changeUserStatus(){

        $specId= $this->request->data('user_id');
        $statusVal = $this->request->data('statusVal');
        if( trim($statusVal) == 'Active' ){
            $chngeStatus = 0;
        }else if( trim($statusVal) == 'Inactive' ){
            $chngeStatus = 1;
        }
        //$this->User->id = $userId;
        $conditions = array('UserPost.id'=> $specId);
        $data = array(
                    'UserPost.status'=> $chngeStatus,
                );
        if( $this->UserPost->updateAll( $data ,$conditions )){
            
            if( $chngeStatus == 1 ){ $statusString = 'Active'; }
            if( $chngeStatus == 0 ){ $statusString = 'Inactive'; }
            echo "OK~$statusString";
        }else{
            echo "ERROR~".ERROR_615;
        }
        exit;
    }

    /*
  On: 02-09-2015
  I/P:
  O/P:
  Desc: 
    */
    public function viewPostAttributes(){
      $this->layout = NULL;
      $userPostAttributes = array();
      $postId = $this->request->data['post_id'];
      if( !empty($postId) ){
        $userPostData = $this->UserPost->userPostLists( $postId );
        $userPostAttributes = $userPostData[0]['UserPostAttribute'];
      }
      $this->set("userPostAttributes", $userPostAttributes);
      echo $this->render('Admin.UserPost/userPostAttributes');
      exit;
    }

    /*
  On: 02-09-2015
  I/P:
  O/P:
  Desc: 
    */
    public function viewPostComments($postId = 0){
      $userPostComments = array();
      $userPostCommentDetails = array();
      if( !empty($postId) ){
        $userPostData = $this->UserPost->userPostLists( $postId );
        $userPostComments = $userPostData[0]['UserPostComment'];
      }
      if(count($userPostComments) > 0){
        foreach($userPostComments as $comment){
          $name = "";
          $email = "";
          $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $comment['user_id'])));
            if(!empty($userDetails)){
                $name = isset($userDetails['UserProfile']['first_name']) ? $userDetails['UserProfile']['first_name'] : ''. ' ' . isset($userDetails['UserProfile']['last_name']) ? $userDetails['UserProfile']['last_name'] : '';
                $name = $userDetails['UserProfile']['first_name'].' '.$userDetails['UserProfile']['last_name'];
                $email = isset($userDetails['User']['email']) ? $userDetails['User']['email'] : ''; 
            }
            $comment['user_name'] = $name;
          $userPostCommentDetails[] = $comment;
        }
      }
    /*  $this->set("userPostComments", $userPostComments);
      echo $this->render('Admin.UserPost/userPostComments');
      exit;*/
      return $userPostCommentDetails;
    }
    /*
  On: 18-02-2016
  I/P:
  O/P:
  Desc: 
    */
    public function speciality_string($specialities = array()){
          $speciality_string = "";
            $specialityData = array();
            if(count($specialities) > 0 && is_array($specialities)){
              foreach($specialities as $postspeciality){
                $specialityData = $this->Specilities->findById( $postspeciality["specilities_id"] );
                if($speciality_string == ""){
                  $speciality_string .= $specialityData['Specilities']['name'];
                }else{
                  $speciality_string .= ', '.$specialityData['Specilities']['name'];
                }
              }
            }
            return $speciality_string;
    }
    public function get_beats_details($post_id = ""){
        //$conditions = array('UserPostBeat.post_id'=> $post_id);
        $beat_array = $this->UserPostBeat->find('all', array(
        'joins' => array(
            array(
                'table' => 'user_profiles',
                'alias' => 'UserJoin',
                'type' => 'INNER',
                'conditions' => array(
                    'UserJoin.user_id = UserPostBeat.user_id'
                )
            )
        ),
        'conditions' => array(
            'UserPostBeat.post_id'=> $post_id
        ),
          'fields' => array('UserJoin.*', 'UserPostBeat.*'),
          'order' => 'UserPostBeat.id DESC'
    ));
          $upbeat_array = array();
          $downbeat_array = array();
          foreach($beat_array as $beat){
            $beat['UserPostBeat']['username'] = $beat['UserJoin']['first_name'].' '.$beat['UserJoin']['last_name'];
            if($beat['UserPostBeat']['beat_type'] == 1){
              $upbeat_array[] = $beat['UserPostBeat'];
            }else if($beat['UserPostBeat']['beat_type'] == 0){
              $downbeat_array[] = $beat['UserPostBeat'];
            }
          }
          $beat_details = array('upbeat' => $upbeat_array , 'downbeat' => $downbeat_array);
          return $beat_details;
    }
    public function get_share_details($post_id = ""){
        $share_details = array();
        $conditions = array('UserPostShare.post_id'=> $post_id); 
          $share_array = $this->UserPostShare->find("all", array(
        'joins' => array(
            array(
                'table' => 'user_profiles',
                'alias' => 'UserJoin',
                'type' => 'INNER',
                'conditions' => array(
                    'UserJoin.user_id = UserPostShare.shared_from'
                )
            )
        ),
        'conditions' => array(
            'UserPostShare.post_id'=> $post_id,
            'UserPostShare.status'=> 1,
        ),
          'fields' => array('UserJoin.*', 'UserPostShare.*'),
          'order' => 'UserPostShare.id DESC'
    ));
    if(count($share_array) > 0){
      foreach($share_array as $share){
        $share_details[] = array(
                  'share_id' => $share['UserPostShare']['id'],
                  'share_by' => $share['UserJoin']['first_name'].' '.$share['UserJoin']['last_name'],
                                    'share_time'=> $share['UserPostShare']['shared_at'],
                  );
      }
    }
          return $share_details;
    }
    /*
  On: 19-02-2016
  I/P:
  O/P:
  Desc: 
    */
    public function userPostDetails($id=NULL){
      $userPostData = array();
        $this->Paginator->settings = array('UserPost' => array('limit' => 2));
        // $postId = $this->request->data['post_id'];
        $postId = $id;
      // $pageId = $_GET['page'];
        
      $userPostData = $this->UserPost->userPostLists( $postId );
    $fields = array("HashTag.id,HashTag.name,HashTag.status,ConsentForm.id,ConsentForm.patient_name,ConsentForm.patient_email,PostUserTag.user_id,PostUserTag.created,UserProfilesTag.first_name, UserProfilesTag.last_name");
    $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'beat_hash_mappings',
                      'alias' => 'BeatHashMapping',
                      'type' => 'left',
                      'conditions'=> array('BeatHashMapping.post_id='.$postId)
                    ),
                    array(
                      'table' => 'hash_tags',
                      'alias' => 'HashTag',
                      'type' => 'left',
                      'conditions'=> array('HashTag.id = BeatHashMapping.hash_id')
                    ),
                    array(
                      'table' => 'consent_forms',
                      'alias' => 'ConsentForm',
                      'type' => 'left',
                      'conditions'=> array('ConsentForm.id = UserPost.consent_id')
                    ),
                    array(
                      'table' => 'post_user_tags',
                      'alias' => 'PostUserTag',
                      'type' => 'left',
                      'conditions'=> array('PostUserTag.post_id = UserPost.id')
                    ),
                    array(
                      'table' => 'user_profiles',
                      'alias' => 'UserProfilesTag',
                      'type' => 'left',
                      'conditions'=> array('UserProfilesTag.id =PostUserTag.user_id')
                    ),
                    ),
                'fields'=> $fields,
                'conditions'=> array('UserPost.id='.$postId),
                 'group'=> array('BeatHashMapping.hash_id','UserPost.id','HashTag.id','PostUserTag.user_id'),
            );
        $userPostDetail=$this->UserPost->find('all',$options);
        if(!empty($userPostDetail[0]['BeatView'])){
        foreach ($userPostDetail[0]['BeatView'] as $value) {
            $beatDetails[]=$value['user_id'];
            $beatUser[]=$data=$this->User->beatViewDetails($value['user_id']);
        }}
        else{$beatUser=array('NULL');
        }
    $userPostData[0]['UserProfileSpam']=$this->spam_profile($userPostData[0]['UserPostSpam']);
    $userPostData[0]['specialitystring'] = $this->speciality_string($userPostData[0]['UserPostSpecilities']);
    $userPostData[0]['beat_details'] = $this->get_beats_details($postId);
    $userPostData[0]['share_details'] = $this->get_share_details($postId);
    $userPostData[0]['comment_details'] = $this->viewPostComments($postId);
    $userPosts = array_merge_recursive($userPostData,$beatUser,$userPostDetail);

    $adminSpam=$this->AdminPostSpam->find('all',array('conditions'=>array('user_post_id'=>$postId)));
    $adminFeature=$this->FeaturedActivityLog->find('all',array('conditions'=>array('user_post_id'=>$postId)));
    $mailLog=$this->get_admin_user_mail($postId);
      $email=$this->User->find('first',array('conditions'=>array('User.id'=>$userPosts[0]['UserProfile']['user_id'])));
      $email=$email['User']['email'];
      $this->set(array("adminFeature"=>$adminFeature,"adminSpam"=>$adminSpam,"userPostdata"=>$userPosts,'id'=>$id,'email'=>$email,'mailLog'=>$mailLog));
    }

    public function beatMail($id){
      $userPostData = $this->UserPost->userPostLists( $id );
      $userId=$userPostData[0]['UserProfile']['user_id'];
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        $subjects=$this->AdminEmailCategorie->find('all',array('conditions'=>array('status'=>1)));
        $this->set(array('subjects'=>$subjects,'userData'=>$userData,'id'=>$id));
    }
    public function templateSelect(){
        if($this->data['type']=="comment"){
            $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$this->data['contentId'])));
            echo $template['AdminEmailTemplate']['content'];
            exit();
        }
        $id=$this->data['id'];
        $templates=$this->AdminEmailTemplate->find('all',array('conditions'=>array('email_category_id'=>$id)));
        $this->set('templates',$templates);
        $this->render('/Elements/users/user_template_select');
    }
    public function templatePreview(){
        if($this->data['type']=="send"){
        $id=$this->data['id'];
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$id),'fields'=>array('title')));
        echo $template['AdminEmailTemplate']['title'];
        exit();
        }
       if($this->data['type']=="view"){
        $id=$this->data['id'];
        $email=$this->data['email'];
        $view=$this->AdminUserSentMail->find('first',array('conditions'=>array('id'=>$id)));
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$view['AdminUserSentMail']['email_template_id']),'fields'=>array('title')));
        $data=array($template['AdminEmailTemplate']['title'],$view['AdminUserSentMail']['content'],$view['AdminUserSentMail']['created_date']);
        $this->set(array('data'=>$data,'email'=>$email));
        $this->render('/Elements/action_mail_preview');
       }
    }

    public function adminToUserMail(){
        $data=$this->data;
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$data['template']),'fields'=>array('title')));
        $params['temp'] = $data['content'];
        $params['subject'] = $template['AdminEmailTemplate']['title'];
        $params['toMail'] = $data['email'];
        // $params['toMail'] = "deepakkumar@mediccreations.com";
        $params['registration_source'] = $data['source'];
        try{
            $mailSend = $this->AdminEmail->sendMailByAdmin( $params );

            $mailData=array('post_id'=>$data['postId'],'email_category_id'=>$data['subject'],'email_template_id'=>$data['template'],'comment'=>$template['AdminEmailTemplate']['title'],'content'=>$data['content'],'status'=>1);
            $this->AdminUserSentMail->saveAll($mailData);

            echo $mailSend;
            exit();
        }catch(Exception $e){
        }
    }

    // ...................................................
    public function get_admin_user_mail($id){
        $mail=array();
        $conditions = array('AdminUserSentMail.post_id'=> $id); 
        $mail=$this->AdminUserSentMail->find('all',array('conditions'=>$conditions));
        return $mail;
    }

    public function spam_profile($spam=array()){
        $spamprofile = array();
                if(count($spam) > 0 && is_array($spam) && isset($spam)){
                    foreach($spam as $value){
                        // pr($value);
                        // exit();
                        $spamProfile=$this->User->find("first",array('fields'=>array('UserProfile.first_name','UserProfile.last_name'),'conditions'=>array('UserProfile.user_id='.$value['reported_by'])));
                        $user[]=$spamProfile;
                    }
                }
                else{
                    $user=NULL;
                }
                return $user;
    }
    // ** STATISTICS START--------
    public function upBeat(){
        if(isset($this->data['j'])){
            $j=$this->data['j']-1;
        }
        else{
            $j=0;
        }
        if(isset($this->data['perPage'])){
            $perPage=$this->data['perPage'];
        }
        else{
            $perPage=5;
        }
        $postId=$this->data['postId'];
        // $userPostData = $this->UserPost->userPostLists( $postId );
        $userPostData= $this->get_beats_details($postId);
        $userPostData=$userPostData['upbeat'];
        $count=count($userPostData);
        $this->set(array("userPostData"=>$userPostData,'j'=>$j,'id'=>$postId,'perPage'=>$perPage,'count'=>$count));
        $this->render('/Elements/userposts/upBeat');
    }
    public function downBeat(){
        if(isset($this->data['j'])){
            $j=$this->data['j']-1;
        }
        else{
            $j=0;
        }
        if(isset($this->data['perPage'])){
            $perPage=$this->data['perPage'];
        }
        else{
            $perPage=5;
        }
        $postId=$this->data['postId'];
        // $userPostData = $this->UserPost->userPostLists( $postId );
        $userPostData= $this->get_beats_details($postId);
        $userPostData= $userPostData['downbeat'];
        $count=count($userPostData);
        $this->set(array("userPostdata"=>$userPostData,'j'=>$j,'id'=>$postId,'perPage'=>$perPage,'count'=>$count));
        $this->render('/Elements/userposts/downBeat');
    }
    public function comments(){
        if(isset($this->data['j'])){
            $j=$this->data['j']-1;
        }
        else{
            $j=0;
        }
        if(isset($this->data['perPage'])){
            $perPage=$this->data['perPage'];
        }
        else{
            $perPage=5;
        }
        $postId=$this->data['postId'];
        $userPostData = $this->viewPostComments($postId);
        $count=count($userPostData);
        $this->set(array("userPostdata"=>$userPostData,'j'=>$j,'id'=>$postId,'perPage'=>$perPage,'count'=>$count));
        $this->render('/Elements/userposts/comments');
    }
    public function shares(){
        if(isset($this->data['j'])){
            $j=$this->data['j']-1;
        }
        else{
            $j=0;
        }
        if(isset($this->data['perPage'])){
            $perPage=$this->data['perPage'];
        }
        else{
            $perPage=5;
        }
        $postId=$this->data['postId'];
        // $userPostData = $this->UserPost->userPostLists( $postId );
        $userPostData = $this->get_share_details($postId);
        $count=count($userPostData);
        $this->set(array("userPostdata"=>$userPostData,'j'=>$j,'id'=>$postId,'perPage'=>$perPage,'count'=>$count));
        $this->render('/Elements/userposts/shares');
    }
    public function views(){
        if(isset($this->data['j'])){
            $j=$this->data['j']-1;
        }
        else{
            $j=0;
        }
        if(isset($this->data['perPage'])){
            $perPage=$this->data['perPage'];
        }
        else{
            $perPage=5;
        }
        $postId=$this->data['postId'];
        $userPostData = $this->UserPost->userPostLists( $postId );
        if(!empty($userPostData[0]['BeatView'])){
        foreach ($userPostData[0]['BeatView'] as $value) {
            $beatDetails[]=$value['user_id'];
            $beatUser[]=$this->User->beatViewDetails($value['user_id']);
        }}
        else{
            $beatUser=NULL;
        }
        $count=count($beatUser);
        $this->set(array("userPostdata"=>$beatUser,'j'=>$j,'id'=>$postId,'perPage'=>$perPage,'count'=>$count));
        $this->render('/Elements/userposts/views');
    }
    public function tagged(){
        if(isset($this->data['j'])){
            $j=$this->data['j']-1;
        }
        else{
            $j=0;
        }
        if(isset($this->data['perPage'])){
            $perPage=$this->data['perPage'];
        }
        else{
            $perPage=5;
        }
        $postId=$this->data['postId'];
        $fields = array("PostUserTag.user_id,PostUserTag.created,UserProfilesTag.first_name, UserProfilesTag.last_name");
        $options  =array(
            'joins'=>array(
                array(
                  'table' => 'post_user_tags',
                  'alias' => 'PostUserTag',
                  'type' => 'left',
                  'conditions'=> array('PostUserTag.post_id = UserPost.id')
                ),
                array(
                  'table' => 'user_profiles',
                  'alias' => 'UserProfilesTag',
                  'type' => 'left',
                  'conditions'=> array('UserProfilesTag.id =PostUserTag.user_id')
                ),
            ),
        'fields'=> $fields,
        'conditions'=> array('UserPost.id='.$postId),
        'group'=> array('PostUserTag.user_id'),
        );
        $userPostDetail=$this->UserPost->find('all',$options);
        foreach ($userPostDetail as $key => $value) {
           $tag[]=$value['PostUserTag'];
           $tagUser[]=$value['UserProfilesTag'];
        }
        $count=count($tag);
        // $this->set(array('tag'=>$tag,'tagUser'=>$tagUser));
        $this->set(array('tag'=>$tag,'tagUser'=>$tagUser,'j'=>$j,'id'=>$postId,'perPage'=>$perPage,'count'=>$count));
        $this->render('/Elements/userposts/tagged');
    }
    public function flagged(){
        if(isset($this->data['j'])){
            $j=$this->data['j']-1;
        }
        else{
            $j=0;
        }
        if(isset($this->data['perPage'])){
            $perPage=$this->data['perPage'];
        }
        else{
            $perPage=5;
        }
        $postId=$this->data['postId'];
        $userPostData = $this->UserPost->userPostLists( $postId );
        $userPostData[0]['UserProfileSpam']=$this->spam_profile($userPostData[0]['UserPostSpam']);
        foreach ($userPostData as $key => $value) {
           $spam=$value['UserPostSpam'];
           $spamUser=$value['UserProfileSpam'];
        }
        $count=count($spam);
        $this->set(array('spam'=>$spam,'spamUser'=>$spamUser,'j'=>$j,'id'=>$postId,'perPage'=>$perPage,'count'=>$count));
        $this->render('/Elements/userposts/flagged');
    }
// ** STATISTICS END---------
    
//** Add BEAT [START]--------
    public function addBeat(){
      if($this->data){
        $addData=$this->data['addBeat'];
        $userId=$this->User->find("first",array('conditions'=>array('User.email'=>'ocr@theoncallroom.com'),'fields'=>'User.id'));
// Title and active status Updation
          $postData = array("title"=> ($this->data['addBeat']['title']!='')?$this->data['addBeat']['title']:'',"user_id"=> $userId['User']['id'],"is_anonymous"=> 0,"consent_id"=>0,"status"=>1,);
          $this->UserPost->saveAll($postData);
          $id = $this->UserPost->id;
// Image Uploading
          for ($k=0; $k < 6; $k++) { 
  if($addData['img'.$k]!==""){
        $addData['img'.$k] = str_replace("data:image/png;base64,", "", $addData['img'.$k]);
        $tmpImg[$k]=$this->Image->createimage($addData['img'.$k],WWW_ROOT . 'img/uploader_tmp/',$k.'_'.time(), 'png');
        if($tmpImg[$k]){
           $size[$k]=filesize(WWW_ROOT . 'img/uploader_tmp/'.$tmpImg[$k]);
          if($size[$k] > $this->ImageSizeLimitToCheckUpload){
           $contentImage = $this->uploadFileToAmazonCompressedAndNonCompressed($tmpImg[$k],WWW_ROOT . 'img/uploader_tmp/'.$tmpImg[$k],'png',$id,$image_doc='image', $sizeOfImage = $size[$k] ,$passNum=md5($k+200));
          }else{
          $contentImage = $this->uploadFileToAmazonFiles($tmpImg[$k],WWW_ROOT . 'img/uploader_tmp/'.$tmpImg[$k],'png',$id,$image_doc='image',$passNum=md5($k+200));
          }
          $postAttributeImage = array("user_post_id"=> $id, "attribute_type"=> 'image', "content"=> $contentImage, "status"=> 1);
          $this->UserPostAttribute->saveAll( $postAttributeImage );
          unlink(WWW_ROOT . 'img/uploader_tmp/'.$tmpImg[$k]);
        }
    }
}
// Image Uploading
        // if( !empty($addData['images']) ){
        //   for($k=0;$k<count($addData['images']);$k++){
        //         $ext1 =array();
        //         $ext ='';
        //         $contentImage ='';
        //         $postAttributeImage = '';
        //        if($addData['images'][$k]['error'] == 0 ){
                  
        //          $ext1 = explode(".", $addData['images'][$k]['name']);
        //          $ext = end($ext1); 

                                              
        //           if($addData['images'][$k]['size'] > $this->ImageSizeLimitToCheckUpload){
        //            $contentImage = $this->uploadFileToAmazonCompressedAndNonCompressed($addData['images'][$k]['name'],$addData['images'][$k]['tmp_name'],$addData['images'][$k]['type'],$postId,$image_doc='image', $sizeOfImage = $addData['images'][$k]['size'] ,$passNum=md5($k+200));
        //           }else{
        //           $contentImage = $this->uploadFileToAmazonFiles($addData['images'][$k]['name'],$addData['images'][$k]['tmp_name'],$addData['images'][$k]['type'],$id,$image_doc='image',$passNum=md5($k+200));
        //           }
        //           $postAttributeImage = array("user_post_id"=> $id, "attribute_type"=> 'image', "content"=> $contentImage, "status"=> 1);
        //           $this->UserPostAttribute->saveAll( $postAttributeImage );
        //        }
        //   }
        // }
// Doc Uploading
        if( !empty($addData['docs']) ){
            for($newDocK=0;$newDocK<count($addData['docs']);$newDocK++){
                     
                 if($addData['docs'][$newDocK]['error'] == 0 ){
                    
                   $ext1 = explode(".", $addData['docs'][$newDocK]['name']);
                   $ext = end($ext1); 
                   $contentDoc = $this->uploadFileToAmazonFiles($addData['docs'][$newDocK]['name'],$addData['docs'][$newDocK]['tmp_name'],$addData['docs'][$newDocK]['type'],$id,$image_doc='doc',$passNum=md5($newDocK+100));
                   $postAttributeDoc = array("user_post_id"=> $id, "attribute_type"=> $ext, "content"=> $contentDoc, "status"=> 1);
                   $this->UserPostAttribute->saveAll( $postAttributeDoc );

                  }

            }
        }
// Interest List Updating
        if($addData['interest']){
          for($i=0;$i < count($addData['interest']);$i++){
            $postSpecilities = array("id"=>"","user_post_id"=> $id, "specilities_id"=> $addData['interest'][$i]);
            $this->UserPostSpecilities->saveAll($postSpecilities);
          }
        }
// HashTags Updating
        if($addData['hashTags']!=="null,null"){
          $addData['hashTags']=str_replace("#","",$addData['hashTags']);
          $hashAr=explode(",",$addData['hashTags']);
          foreach ($hashAr as $value) {
            if($value!=="null"){
              $hash=$this->HashTag->find("first",array('conditions'=>array('LOWER(HashTag.name)'=>''.strtolower($value).''),'fields'=>'HashTag.id'));
                if(empty($hash)){
                  $this->HashTag->saveAll(array("name"=>$value,"status"=>1));
                }
                $hashIds=$this->HashTag->find("first",array('conditions'=>array('LOWER(HashTag.name)'=>''.strtolower($value).''),'fields'=>'HashTag.id'));
                $hashId[]=$hashIds['HashTag']['id'];
            }
          }
          for($i=0;$i < count($hashId);$i++){
            $postHash = array("post_id"=> $id, "hash_id"=> $hashId[$i],"status"=>1);
            $this->BeatHashMapping->saveAll($postHash);
          }
        }
// Description
        if($addData['description']){
          $postAttributeTxt = array("user_post_id"=> $id, "attribute_type"=> 'text', "content"=> $addData['description'], "status"=> 1);
        $this->UserPostAttribute->saveAll( $postAttributeTxt );
        }
// Video
        if($addData['video_path']){
          $postAttributeVideo = array("user_post_id"=> $id, "attribute_type"=> 'video', "content"=> $addData['video_path'], "status"=> 1);
        $this->UserPostAttribute->saveAll( $postAttributeVideo );
        }
        //** Add information to send notification[START]
                  $notificationData = array(
                              "item_id"=> $id,
                              "user_id"=> $userId['User']['id'],
                              "type"=> 'addPost',
                              "added_source"=>'Admin'
                            );
                  $addNotificationInfo = $this->NotificationQueque->saveAll($notificationData);
                //** Add information to send notification[END]

          $this->redirect(BASE_URL . 'admin/UserPost/userPostDetails/'.$id);
      }
      //** Get interest lists [START]
      $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
      $interestLists['interestlist'] = $interestlists; 
      $this->set('interestLists', $interestLists);
      //** Get interest lists [END]
        }
/*
________________________________________________
Method Name:Main=>youtubepaths
Purpose:Make login to user
Detail: 
created by :Developer
Created date:16-03-16
Params:Via post
_________________________________________________
*/

public function youtubepaths() {
    // $this->beatUserAuthorisation();
    if( $this->request->is('ajax') ) {
        $this->autoRender = false;
          
            if($_REQUEST['key']!=''){
                $keys = explode(' ', $_REQUEST['key']);
                if(count($keys) > 0){
                   $query= (String) preg_replace("/ /","+",$_REQUEST['key']);
                }else{
                    $query = (String) $_REQUEST['key'];
                }
                
                $searchResponse= json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/search?order=date&q='.$query.'&part=snippet&type=video&maxResults=5&key=AIzaSyCUs2ypUMJZ_MuhWWyzLnlo3641QqTkDOo'));
               
                if(!empty($searchResponse)){
                   echo '<table>';
                   foreach ($searchResponse->items as $key=>$searchResult) { 
                     $pathId = $searchResult->id->videoId; 
                     $completePath ="http://www.youtube.com/watch?v=".$pathId;
                     echo '<tr><td>'.$searchResult->snippet->title.'</td><td><img width="100" src="'.$searchResult->snippet->thumbnails->default->url.' " /><br><a href="javascript:void(0);" id="'.$pathId.'" onClick="youtubePathVideo(this.id);">[select]</a><a href="'.$completePath.'" target="__blank" >[play]</a></td></tr>';
                     echo '<tr><td claspan="2">&nbsp;</td></tr>';
                    }
                    echo '</table>';
                }else{
                    echo "No result found";
                }
                
            }
    }
    exit;
}    

//** Add BEAT [END]--------
//** EDIT BEAT [START]--------

public function editBeat($id=Null){
      if ($this->request->data){
        $upData=$this->data['Update'];
        for ($j=0; $j <6 ; $j++) { 
          $upData['img'.$j]= basename($upData['img'.$j]);
        }
        $doc=$this->UserPostAttribute->find("all",array('conditions'=>array('UserPostAttribute.user_post_id'=>$id,'UserPostAttribute.attribute_type'=>array('doc','docx','DOC','DOCX','ppt','PPT','pptx','PPTX','xls','XLS','xlsx','XLSX','pdf','PDF','rtf','RTF'),'UserPostAttribute.status'=>1)));
        $imgs=$this->UserPostAttribute->find("all",array('conditions'=>array('UserPostAttribute.user_post_id'=>$id,'UserPostAttribute.attribute_type'=>'image','UserPostAttribute.status'=>1)));
        if(count($doc)>0){
          for ($i=1; $i <=4; $i++) { 
            if(!isset($doc[$i-1]['UserPostAttribute']['content'])){
              $doc[$i-1]['UserPostAttribute']['content']="";
            }
            if($doc[$i-1]['UserPostAttribute']['content']!==$upData['doc'.$i]){
              $removed_docs[]=$doc[$i-1];
            }
          }
        }
        if(count($imgs)>0){
          for ($i=0; $i <6; $i++) { 
            if(!isset($imgs[$i]['UserPostAttribute']['content'])){
              $imgs[$i]['UserPostAttribute']['content']="";
            }
            if($imgs[$i]['UserPostAttribute']['content']!==$upData['img'.$i]){
              $removed_imgs[]=$imgs[$i];
            }
          }
        }
if(isset($removed_imgs) || !empty($removed_imgs)){
  foreach ($removed_imgs as $value) {
    if (!empty($value['UserPostAttribute']['content'])) {
      $this->UserPostAttribute->deleteAll(array('UserPostAttribute.user_post_id'=>$id,'UserPostAttribute.id'=>$value['UserPostAttribute']['id']));
      $beatActivityDocs = array("id"=>"","user_post_id"=> $id, "attribute_type"=> $value['UserPostAttribute']['attribute_type'], "content"=> $value['UserPostAttribute']['content'], "status"=> 0);
      $this->BeatActivityLog->saveAll( $beatActivityDocs);

    }
  }
}  
if(isset($removed_docs) || !empty($removed_docs)){
  foreach ($removed_docs as $value) {
    if (!empty($value['UserPostAttribute']['content'])) {
      $this->UserPostAttribute->deleteAll(array('UserPostAttribute.user_post_id'=>$id,'UserPostAttribute.id'=>$value['UserPostAttribute']['id']));
      $beatActivityDocs = array("id"=>"","user_post_id"=> $id, "attribute_type"=>$value['UserPostAttribute']['attribute_type'], "content"=> $value['UserPostAttribute']['content'], "status"=> 0);
      $this->BeatActivityLog->saveAll( $beatActivityDocs);

    }
  }
}       
// Notification for updation of beat
        $colleagueIds = array(); $colleagueUserIds = array();
        $postTitle=$this->UserPost->find('all',array('fields'=>'UserPost.title','conditions'=> array('UserPost.id'=>$id) ));
          $titleSendMsg = ($postTitle[0]['UserPost']['title'])?$postTitle[0]['UserPost']['title']:'';
          $meColleagues = $this->UserColleague->find("all", array("conditions"=> array("user_id"=> $upData['userId'], "status"=> 1)));
          foreach($meColleagues as $colleagueIds){
            $colleagueUserIds[] = $colleagueIds['UserColleague']['colleague_user_id'];
          }

          $myColleagues = $this->UserColleague->find("all", array("conditions"=> array("colleague_user_id"=> $upData['userId'], "status"=> 1)));
          foreach($myColleagues as $colleagueIds){
            $colleagueUserIds[] = $colleagueIds['UserColleague']['user_id'];
          }

          //** Send Notification to followers
          $followers = $this->UserFollow->find("all", array("conditions"=> array("followed_to"=> $upData['userId'], "follow_type"=> 1,"status"=> 1)));
          $followerUserIds = array();
          foreach($followers as $followerId){
            $followerUserIds[] = $followerId['UserFollow']['followed_by'];
          }

          $finalUsers = array_unique(array_merge($colleagueUserIds, $followerUserIds));
          $finalUserRemoveTaggedUsers = array_diff($finalUsers,array()); //** Don't send tagged user message again
          // $this->sendNotificationToFollowersNew($finalUserRemoveTaggedUsers, $upData['userId'], $id, $titleSendMsg, 0 , NULL, 1 );// last parameter for edit beat type

          $this->Session->setFlash("Content edited sucessfully");

// Image Uploading
          for ($k=0; $k < 6; $k++) { 
            if($upData['editedImg'.$k]!==""){
              $upData['editedImg'.$k] = str_replace("data:image/png;base64,", "", $upData['editedImg'.$k]);
              $tmpImg[$k]=$this->Image->createimage($upData['editedImg'.$k],WWW_ROOT . 'img/uploader_tmp/',$k.'_'.time(), 'png');
              if($tmpImg[$k]){
                 $size[$k]=filesize(WWW_ROOT . 'img/uploader_tmp/'.$tmpImg[$k]);
                if($size[$k] > $this->ImageSizeLimitToCheckUpload){
                 $contentImage = $this->uploadFileToAmazonCompressedAndNonCompressed($tmpImg[$k],WWW_ROOT . 'img/uploader_tmp/'.$tmpImg[$k],'png',$id,$image_doc='image', $sizeOfImage = $size[$k] ,$passNum=md5($k+200));
                }else{
                $contentImage = $this->uploadFileToAmazonFiles($tmpImg[$k],WWW_ROOT . 'img/uploader_tmp/'.$tmpImg[$k],'png',$id,$image_doc='image',$passNum=md5($k+100));
                }
                $postAttributeImage = array("user_post_id"=> $id, "attribute_type"=> 'image', "content"=> $contentImage, "status"=> 1);
                $this->UserPostAttribute->saveAll( $postAttributeImage );
                unlink(WWW_ROOT . 'img/uploader_tmp/'.$tmpImg[$k]);
              }
            }
          }
//Doc Process
        if(!empty($upData['doc']['0']['name'])||!empty($upData['doc']['1']['name'])||!empty($upData['doc']['2']['name'])||!empty($upData['doc']['3']['name'])){
          // $weightDoc=0;
          // $weightDocExtention=0;

          //   for($newDoc=0;$newDoc<count($upData['doc']);$newDoc++){
          //       // if($upData['doc'][$newDoc]['size'] > $this->ImageSizeToUpload ){
          //       //     $weightDoc++;      
          //       //  }
          //        if($upData['doc'][$newDoc]['name']!='' && $this->checkExtentionForDoc($upData['doc'][$newDoc]['name'])!=1){
          //           $weightDocExtention++;   
          //        }

          //   }
            // if($weightDoc==0){
            //    if($weightDocExtention==0){

                  // Add Doc
                            for($newDocK=0;$newDocK<count($upData['doc']);$newDocK++){
                                     
                                 if($upData['doc'][$newDocK]['error'] == 0 ){
                                    
                                   $ext1 = explode(".", $upData['doc'][$newDocK]['name']);
                                   $ext = end($ext1); 
                                   $contentDoc = $this->uploadFileToAmazonFiles($upData['doc'][$newDocK]['name'],$upData['doc'][$newDocK]['tmp_name'],$upData['doc'][$newDocK]['type'],$id,$image_doc='doc',$passNum=md5($newDocK+100));
                                   $postAttributeDoc = array("user_post_id"=> $id, "attribute_type"=> $ext, "content"=> $contentDoc, "status"=> 1);
                                   $this->UserPostAttribute->saveAll( $postAttributeDoc );

                              }
                
                          }
                    
                   // $this->Session->setFlash("Document uploaded sucessfully");
                   
        //        }else{
        //         $this->Session->setFlash("Allowed extention are doc,docx,ppt,pptx,xls,xlsx,pdf");
        //        }

        //     }else{
        //       $this->Session->setFlash("Please upload less than 10MB image");
        //     }
        }
//interest list
        $this->UserPostSpecilities->deleteAll(array('UserPostSpecilities.user_post_id'=>$id));
       
        if((!empty($upData['interests'])) & ($upData['interests']!="Null")){
          $SpecilityAr=explode(", ",$upData['interests']);
          foreach ($SpecilityAr as $value) {
            if($value!==""){
            $specilities=$this->Specilities->find("first",array('conditions'=>array('Specilities.name'=>''.$value.''),'fields'=>'Specilities.id'));
            $SpecilitiesId[]=$specilities['Specilities']['id'];
            }
          }
          for($i=0;$i < count($SpecilitiesId);$i++){
            $postSpecilities = array("id"=>"","user_post_id"=> $id, "specilities_id"=> $SpecilitiesId[$i]);
            $this->UserPostSpecilities->saveAll($postSpecilities);
          }
        }
//Hash tags
        $this->BeatHashMapping->deleteAll(array('BeatHashMapping.post_id'=>$id));
        if($upData['newHash']!=="null,null"){
          $upData['newHash']=str_replace("#","",$upData['newHash']);
          $hashAr=explode(",",$upData['newHash']);
          foreach ($hashAr as $value) {
            if($value!=="null"){
              $hash=$this->HashTag->find("first",array('conditions'=>array('LOWER(HashTag.name)'=>''.strtolower($value).''),'fields'=>'HashTag.id'));
                if(empty($hash)){
                  $this->HashTag->saveAll(array("name"=>$value,"status"=>1));
                }
                $hashIds=$this->HashTag->find("first",array('conditions'=>array('LOWER(HashTag.name)'=>''.strtolower($value).''),'fields'=>'HashTag.id'));
                $hashId[]=$hashIds['HashTag']['id'];
            }
          }
          for($i=0;$i < count($hashId);$i++){
            $postHash = array("post_id"=> $id, "hash_id"=> $hashId[$i],"status"=>1);
            $this->BeatHashMapping->saveAll($postHash);
          }
        }
//status and title
        if($upData['status1']=="Active"){
          $statusA=1;
        }
        else{
          $statusA=0;
        }
        if($upData['status2']=="Flagged"){
          $flag=1;
          $feature=0;
        }
        else if($upData['status2']=="Featured"){
          $flag=0;
          $feature=1;
        }
        else{
          $flag=0;
          $feature=0;
        }
        $this->UserPost->updateAll(array('UserPost.title'=>'"'.$upData['title'].'"'), array('UserPost.id' => $id));
//Description
        $this->UserPostAttribute->deleteAll(array('UserPostAttribute.user_post_id'=>$id,'UserPostAttribute.attribute_type'=>'text'));
          if($upData['descriptions']!==""){
            $postAttributeTxt = array("user_post_id"=> $id, "attribute_type"=> 'text', "content"=> $upData['descriptions'], "status"=> 1);
          $this->UserPostAttribute->saveAll( $postAttributeTxt );
          }
//video
           $this->UserPostAttribute->deleteAll(array('UserPostAttribute.user_post_id'=>$id,'UserPostAttribute.attribute_type'=>'video'));
          if($upData['video']!==""){
            $postAttributeVideo = array("user_post_id"=> $id, "attribute_type"=> 'video', "content"=> $upData['video'], "status"=> 1);
          $this->UserPostAttribute->saveAll( $postAttributeVideo );
          }

        $this->redirect(BASE_URL . 'admin/UserPost/userPostDetails/'.$id);
      }
      $this->layout='adminDefault';
        $userPostData = array();
          $id = $id;
        // interest list[START]
          $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $interestLists['interestlist'] = $interestlists; 
        $this->set('interestLists', $interestLists);
        // interest list[END]
        $userPostData = $this->UserPost->userPostLists( $id );
      $fields = array("HashTag.id,HashTag.name,HashTag.status,ConsentForm.id,ConsentForm.patient_name,ConsentForm.patient_email,PostUserTag.user_id,PostUserTag.created,UserProfilesTag.first_name, UserProfilesTag.last_name");
      $options  = 
          array(
                  'joins'=>array(
                      array(
                        'table' => 'beat_hash_mappings',
                        'alias' => 'BeatHashMapping',
                        'type' => 'left',
                        'conditions'=> array('BeatHashMapping.post_id='.$id)
                      ),
                      array(
                        'table' => 'hash_tags',
                        'alias' => 'HashTag',
                        'type' => 'left',
                        'conditions'=> array('HashTag.id = BeatHashMapping.hash_id')
                      ),
                      array(
                        'table' => 'consent_forms',
                        'alias' => 'ConsentForm',
                        'type' => 'left',
                        'conditions'=> array('ConsentForm.id = UserPost.consent_id')
                      ),
                      array(
                        'table' => 'post_user_tags',
                        'alias' => 'PostUserTag',
                        'type' => 'left',
                        'conditions'=> array('PostUserTag.post_id = UserPost.id')
                      ),
                      array(
                        'table' => 'user_profiles',
                        'alias' => 'UserProfilesTag',
                        'type' => 'left',
                        'conditions'=> array('UserProfilesTag.id =PostUserTag.user_id')
                      ),
                      ),
                  'fields'=> $fields,
                  'conditions'=> array('UserPost.id='.$id),
                   'group'=> array('BeatHashMapping.hash_id','UserPost.id','HashTag.id','PostUserTag.user_id'),
              );
          $userPostDetail=$this->UserPost->find('all',$options);
          if(!empty($userPostDetail[0]['BeatView'])){
          foreach ($userPostDetail[0]['BeatView'] as $value) {
              $beatDetails[]=$value['user_id'];
              $beatUser[]=$data=$this->User->beatViewDetails($value['user_id']);
          }}
          else{$beatUser=array('NULL');
          }
      $userPostData[0]['UserProfileSpam']=$this->spam_profile($userPostData[0]['UserPostSpam']);
      $userPostData[0]['specialitystring'] = $this->speciality_string($userPostData[0]['UserPostSpecilities']);
      $userPostData[0]['beat_details'] = $this->get_beats_details($id);
      $userPostData[0]['share_details'] = $this->get_share_details($id);
      $userPostData[0]['comment_details'] = $this->viewPostComments($id);
      $userPosts = array_merge_recursive($userPostData,$beatUser,$userPostDetail);
        $this->set(array("userPostdata"=>$userPosts,'id'=>$id));
    }
    /*
      --------------------------------------------------------------------------
      I/P:
      O/P:
      Desc: Send notification to all Users when beat edited by any Admin(user later on)..
      --------------------------------------------------------------------------
      */
      public function sendNotificationToFollowersNew($followerUserIds = array(), $userId = NULL, $postId = NULL, $postTitle = '', $isAnonymous = 0, $sharedBeat = NULL , $beatTypeAction=NULL){
        if( !empty($postId) ){
          //** Send GCM to colleague or follower when beat posted or shared[START]
              if(!empty($sharedBeat) && $sharedBeat == 'shared'){
                //** If Post is anonymous don't show beat owner name
                if($isAnonymous == 1){
                  if(!empty($postTitle)){
                    $message = "OCR Member just shared " . $postTitle;
                  }else{
                    $message = "OCR Member just shared a Beat";
                  }
                }else{
                  $userDetails = $this->User->findById( $userId );
                  if(!empty($postTitle)){
                    $message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just shared " . $postTitle;
                  }else{
                    $message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just shared a Beat";
                  }
                }
              }else{
                //** If Post is anonymous don't show beat owner name
                if($isAnonymous == 1){
                  if(!empty($postTitle)){
                    $message = "OCR Member just posted " . $postTitle;
                  }else{
                    $message = "OCR Member just posted a Beat";
                  }
                }else{
                  $userDetails = $this->User->findById( $userId );
                  
                  if($beatTypeAction){
                    $beatTypeByInstitute = "just edited";
                  
                  }else{
                    $beatTypeByInstitute = "just posted";
                  }
                  
                  //$beatTypeByInstitute = "just edited";
                  if(!empty($postTitle)){

                    $message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . $beatTypeByInstitute . $postTitle;
                  }else{
                    $message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . $beatTypeByInstitute . " a Beat";
                  }
                }
              }
            //** Send GCM to colleague or follower when beat posted or shared[END]
            foreach( $followerUserIds as $uFollow ){
              $userRegisteredDevices = array(); $followerUserId = ""; $notificationSetting = ""; $userDevices = array(); $followerUserId = ""; $userAndroidDevice = array();
              $followerUserId = $uFollow;
              $notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $followerUserId, "status"=> 1 )));
              $userDevices = $this->NotificationUser->find("all", array("conditions"=> array("user_id"=> $followerUserId, "status"=> 1)));
              foreach( $userDevices as $userDevice ){
                $userRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];
                //** Get Device id and send New Beat Available to android only
                  //if(strlen($userDevice['NotificationUser']['device_id']) < 20 ){
                    $userAndroidDevice[] = $userDevice['NotificationUser']['device_reg_id'];
                  //}
              }
              try{
                  if(empty($sharedBeat)){ //** Don't show in case of shared beat
                    $notifyResponse = "";$notificationId = 0; $totUnreadNotificationCount = 0;
                    if( ($notificationSetting == (int) 0) ){ //** Check user notification setting ON/OFF 
                      $notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $followerUserId, "item_id"=> $postId ,"notify_type"=> "beatToFollower" ,"content"=> $message);
                      $this->NotificationLog->saveAll( $notifyLogData );
                      $notificationId = $this->NotificationLog->getLastInsertId();
                      //** Send GCM message
                      //$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $followerUserId, "read_status"=> 0)));
                      $postByUserId = $userId;
                      $unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
                      $totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
                      $notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "item_id"=> $postId ,"notify_type"=> "beatToFollower", "tot_unread_notification"=> $totUnreadNotificationCount) );
                      //** Update GCM API response
                      try{
                        $this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
                      }catch(Exception $e){
                        $notifyResponse = $this->Common->sendGcmMessage( $userAndroidDevice, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> $postId), array("priority"=> "normal", "content_available"=> false) );
                      }
                    }
                  }
                  //** Send New Beat Available GCM Message [START]
                    //if($userId !=$followerUserId){
                        $notifyResponse = $this->Common->sendGcmMessage( $userAndroidDevice, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> $postId), array("priority"=> "normal", "content_available"=> false) );
                      //}
                    //** Send New Beat Available push to devices
                      /*if($userId !=$followerUserId){
                        $foregroundDevices = array();
                        $foregroundDevices = $this->newBeatAvailableGcmIds($followerUserId);
                        if(!empty($foregroundDevices)){
                          $notifyResponse = $this->Common->sendGcmMessage( $foregroundDevices, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> (int) $postId, "tot_unread_notification"=> $totUnreadNotificationCount));
                        }
                      }*/
                  //** Send New Beat Available GCM Message [END]
              }catch( Exception $e ){
                if($userId != $followerUserId){
                  $notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $followerUserId, "item_id"=> $postId ,"notify_type"=> "beatToFollower" ,"content"=> $message ,"api_response"=> $e->getMessage() );
                  $this->NotificationLog->save( $notifyLogData );
              }
            }
          }
        }
      }

    /*
    ________________________________________________
    Method Name:Main=>uploadFileToAmazonFiles
    Purpose:Upload image to Amazon
    Detail: 
    created by :Developer
    Params:Via post
    _________________________________________________
    */
    public function uploadFileToAmazonFiles($filesName=NULL,$fileTempName=NULL,$filetype=NULL,$postId=NULL,$image_doc=NULL,$passNum=NULL){
     $imageName=""; 
     $ext1 = explode(".", $filesName);
     $ext = end($ext1);
     if($passNum!=""){
        $imageName =  md5($postId .'_' . $passNum .strtotime(date('Y-m-d H:i:s'))).'.'.$ext;  
     }else{
        $imageName =  md5($postId .'_' . strtotime(date('Y-m-d H:i:s'))).'.'.$ext;  
     }
      //$imageName =  md5($postId .'_' . strtotime(date('Y-m-d H:i:s'))).'.'.$ext;  
      $imagePath =  $fileTempName;
      $contentType = $filetype;
      $this->Image->moveImageToAmazon( array('img_path'=>$imagePath , 'img_name'=>'posts/'.$postId.'/'.$image_doc.'/'.$imageName) , $contentType);                   
      return $imageName;
    }
    /*
    ________________________________________________
    Method Name:uploadFileToAmazonCompressedAndNonCompressed
    Purpose:Upload image to Amazon
    Detail: 
    created by :Developer
    Params:Via post
    _________________________________________________
    */
    public function uploadFileToAmazonCompressedAndNonCompressed($filesName=NULL,$fileTempName=NULL,$filetype=NULL,$postId=NULL,$image_doc=NULL, $size=NULL ,$passNum=NULL){
       /* Process Image 
        1=>Create image name of going to compressed
        */
                           $imageName=""; 
                           $ext1 = explode(".", $filesName);
                           $ext = end($ext1);
                           if($passNum!=""){
                              $imageName =  md5($postId .'_' . $passNum .strtotime(date('Y-m-d H:i:s'))).'.'.$ext;  
                           }else{
                              $imageName =  md5($postId .'_' . strtotime(date('Y-m-d H:i:s'))).'.'.$ext;  
                           }
      /*2=>Create image name of going to compressed*/
                           $imageInfo = getimagesize($fileTempName); 
                           if(isset($imageInfo['channels'])){
                            $channel=$imageInfo['channels'];
                           }else{
                            $channel=8;
                           }

                           if(isset($imageInfo['bits'])){
                            $bits=$imageInfo['bits'];
                           }else{
                            $bits=8;
                           }

                           
                           $requiredMemoryMB = ( $imageInfo[0] * $imageInfo[1] * ($bits / 8) * $channel * 2.5 ) / 1024;
                           
                           ini_set('memory_limit',$requiredMemoryMB.'M');
                           $source_photo = $fileTempName;
                           $dest_photo = WWW_ROOT . 'img/uploader_tmp/'.$imageName;
                           $AmazonImageToUpload = $this->compress_image($source_photo, $dest_photo, filesize($source_photo));
       /*3=>Upload to Amazon as compressed image  */                    
                           $imagePath =  $AmazonImageToUpload;
                           $contentType = $filetype;
                           $this->Image->moveImageToAmazon( array('img_path'=>$imagePath , 'img_name'=>'posts/'.$postId.'/'.$image_doc.'/'.$imageName) , $contentType);                   
                           $this->Image->moveImageToAmazon( array('img_path'=>$fileTempName , 'img_name'=>'posts/'.$postId.'/'.$image_doc.'/original_'.$imageName) , $contentType);                   
                            
                           if( file_exists( WWW_ROOT . 'img/uploader_tmp/'.$imageName )){
                                        chmod(WWW_ROOT . 'img/uploader_tmp/'.$imageName, 777);
                                        unlink( WWW_ROOT . 'img/uploader_tmp/'.$imageName );
                            } 



                            return $imageName;




    }
  function compress_image($source_url, $destination_url, $size) {
    $info = getimagesize($source_url);
   
    if ($info['mime'] == 'image/jpeg') $image    = imagecreatefromjpeg($source_url);
    elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
    elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
   
    //save it
    if($size <= 3145728){
      $quality=1;
      
    }else if($size <= 6291456){
      $quality=90;
      
    }else if($size <= 10485760){
      $quality=0;
      
    }
   
    imagejpeg($image, $destination_url, $quality);
    imagedestroy($image);
    return $destination_url;
  }
    /*
    ________________________________________________
    Method Name:Main=>checkExtentionForImage
    Purpose:Upload image to Amazon
    Detail: 
    created by :Developer
    Params:Via post
    _________________________________________________
    */
    public function checkExtentionForImage($filesName=NULL){
     $image_ext = array('png','PNG','JPEG','jpeg','jpg','JPG'); 
     $ext1 = explode(".", $filesName);
     $ext = end($ext1);
      if(in_array($ext, $image_ext)){
        return 1;
      }else{
        return 2;
      }
    }

    /*
    /*
    ________________________________________________
    Method Name:Main=>checkExtentionForDoc
    Purpose:Upload image to Amazon
    Detail: 
    Params:Via post
    _________________________________________________
    */
    public function checkExtentionForDoc($filesName=NULL){
     $doc_ext = array('doc','docx','DOC','DOCX','ppt','PPT','pptx','PPTX','xls','XLS','xlsx','XLSX','pdf','PDF');  
     $ext1 = explode(".", $filesName);
     $ext = end($ext1);
     if(in_array($ext, $doc_ext)){
        return 1;
      }else{
        return 2;
      }
    }


//** EDIT BEAT [END]--------
    
    // public function download() {
        
    //     $id=$_REQUEST['id'];
    //     $file=$_REQUEST['file'];
    //     $type=explode(".",$file);
    //     $this->viewClass = 'Media';
    //     // Download app/outside_webroot_dir/example.zip
    //     $params = array(
    //         'id'        => $file,
    //         'name'      => 'example',
    //         'download'  => true,
    //         'extension' => $type[1],
    //         'path'      => AMAZON_PATH.'posts/'.$id.'/docs/',
    //     );
    //     // pr($params);
    //     // exit();
    //     $this->set($params);
    // }
    public function markFeature(){
        $this->layout = NULL;
        $postId = $this->request->data['post_id'];
        $conditions = array('UserPost.id'=> $postId);
        $data = array(
            'UserPost.top_beat'=> 1,
            'UserPost.status'=> 1,
        );
        $beatDetails = $this->UserPost->find("first", array("conditions"=> array("UserPost.id"=> $postId)));
        $featureData=array('user_id'=>$beatDetails['UserPost']['user_id'],'user_post_id'=>$postId,'status'=>1);
        $this->FeaturedActivityLog->saveAll($featureData);
        if( $this->UserPost->updateAll( $data ,$conditions )){
            echo 'success';exit();
                }else{
                    echo 'failure';exit();
                }
    }
    public function unMarkFeature(){
        $this->layout = NULL;
        $postId = $this->request->data['post_id'];
        $conditions = array('UserPost.id'=> $postId);
        $data = array(
            'UserPost.top_beat'=>0,
        );
        $beatDetails = $this->UserPost->find("first", array("conditions"=> array("UserPost.id"=> $postId)));
        $featureData=array('user_id'=>$beatDetails['UserPost']['user_id'],'user_post_id'=>$postId,'status'=>0);
        $this->FeaturedActivityLog->saveAll($featureData);
        if( $this->UserPost->updateAll( $data ,$conditions )){
            echo 'success';exit();
                }else{
                    echo 'failure';exit();
                }
    }
/*
  On: 22-02-2016
  I/P:
  O/P:
  Desc: 
    */
    public function toggleTopPost(){
      $postId = $this->request->data['post_id'];
        if( !empty($postId) ){
          $userPostData = $this->UserPost->userPostLists( $postId );
          if($userPostData[0]['UserPost']['top_beat'] == 0){
            $topBeatCount = $this->UserPost->find("count", array("conditions"=> array("top_beat"=> 1)));
            if($topBeatCount >= 10 ){
              echo "NOTOK~Top beat exceeds maximum limit(10).Please unmark some top beat to proceed";exit();
            }else{
              $update_status = 1;
            }
          }else{
            $update_status = 0;
          }
        }
        $conditions = array('UserPost.id'=> $postId);
        $data = array(
                    'UserPost.top_beat'=> $update_status,
                );

        if( $this->UserPost->updateAll( $data ,$conditions )){
            $statusString = 'Top Beat';
            if( $update_status == 1 ){ $statusString = 'Top Beat'; }
            if( $update_status == 0 ){ $statusString = 'Not a top beat'; }
            echo "OK~$statusString";
        }else{
            echo "ERROR~".ERROR_615;
        }
        exit;
    }
    /*
  On: 22-02-2016
  I/P:
  O/P:
  Desc: 
    */
    public function flagPostView(){
      $this->layout = NULL;
      $postSpamData = array();
      $postId = $this->request->data['post_id'];
    $spamContents = $this->PostSpamContent->spamContentList();
  if( !empty($spamContents) ){
    foreach( $spamContents as $spamC){
      $postSpamData[] = array("id"=> $spamC['PostSpamContent']['id'], "spam_name"=> $spamC['PostSpamContent']['spam_name']);
    }
  }
  $spam_details = array('spam_type' => $postSpamData,'post_id' => $postId);
  $this->set("userPostdata", $spam_details);
      echo $this->render('Admin.UserPost/flagPostview');
      exit;
    }
    public function markFlag(){
          $this->layout = NULL;
          $postId = $this->request->data['post_id'];
          $comment = $this->request->data['comment'];
          $spam_type = $this->request->data['spam_type'];
          $conditions = array('UserPost.id'=> $postId);
            $data = array(
                    'UserPost.spam_confirmed  '=> 1,
                    'UserPost.top_beat'=>0,
                    'UserPost.status'=>0,
                );
            if( $this->UserPost->updateAll( $data ,$conditions )){
              echo "success";
              $spamPostData = array(
                    "user_post_id"=> $postId,
                    "content"=> $comment,
                    "post_spam_content_id"=> $spam_type,
            );
            $spamPost = $this->AdminPostSpam->saveAll( $spamPostData );
            $spamReason = $this->PostSpamContent->find("first", array("conditions"=> array("id"=> $spam_type)));
            $beatDetails = $this->UserPost->find("first", array("conditions"=> array("UserPost.id"=> $postId)));
        $beatOwnerDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $beatDetails['UserPost']['user_id'])));
                $params = array();
          $params['subject'] = 'Your Beat marked as Spam';
          $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Mark Spam Beat to beat owner')));
          $params['temp'] = $templateContent['EmailTemplate']['content'];
          $params['toMail'] = $beatOwnerDetails['User']['email'];
          // $params['toMail'] = 'deepakkumar@mediccreations.com';
          $params['name'] = $beatOwnerDetails['UserProfile']['first_name'] . " " . $beatOwnerDetails['UserProfile']['last_name'] ;
          $params['beat_title'] = $beatDetails['UserPost']['title'];
          $params['spam_reason'] = $spamReason['PostSpamContent']['spam_name'];
          $params['comment'] = $comment;
          
          try{
            if(!empty($params['toMail'])){
              $this->AdminEmail->markSpamBeatEmailToBeatOwner( $params );
            }
          }
          catch(Exception $e){}
            }
            else{
              echo 'failure';
              exit();
            }
          
        exit(); 
    }
    public function unFlagbeat(){
          $this->layout = NULL;
          $postSpamData = array();
          $postId = $this->request->data['post_id'];
          $conditions = array('UserPost.id'=> $postId);
          $data = array(
                    'UserPost.spam_confirmed  '=> 0,
                );

            if( $this->UserPost->updateAll( $data ,$conditions )){
    //        $this->AdminPostSpam->deleteAll(array('AdminPostSpam.user_post_id' => $postId), false);
    //        $beatDetails = $this->UserPost->find("first", array("conditions"=> array("UserPost.id"=> $postId)));
        // $beatOwnerDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $beatDetails['UserPost']['user_id'])));
    //            $params = array();
        //  $params['subject'] = 'Your Beat  unmarked as Spam';
        //  $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Unmark Spam Beat to beat owner by admin')));
        //  $params['temp'] = $templateContent['EmailTemplate']['content'];
        //  $params['toMail'] = $beatOwnerDetails['User']['email'];
        //  //$params['toMail'] = 'devi@mediccreations.com';
        //  $params['from'] = NO_REPLY_EMAIL;
        //  $params['name'] = $beatOwnerDetails['UserProfile']['first_name'] . " " . $beatOwnerDetails['UserProfile']['last_name'] ;
        //  $params['beat_title'] = $beatDetails['UserPost']['title'];
        //  try{
        //    if(!empty($params['toMail'])){
        //      $this->AdminEmail->unmarkSpamBeatEmailToBeatOwnerByAdmin( $params );
        //    }
        //  }catch(Exception $e){}
          echo 'success';exit();
            }else{
              echo 'failure';exit();
            }
    }
    /*
    ------------------------------------------------------------------------------------------
    On: 06-07-2016
    I/P:
    O/P:
    Desc: Delete Beat from list (soft delete)
    ------------------------------------------------------------------------------------------
    */
    public function deleteBeat(){
       
        if(isset($this->request->data['beatId'])){
            $updateUser = $this->UserPost->updateAll(array("UserPost.status"=> 2,"UserPost.top_beat"=>0), array("UserPost.id"=> $this->request->data['beatId']));
            if($updateUser){
                echo "Post Deleted";
            }else{
                echo "Some issue occured. Please try again.";
            }
        }else{
            echo "Post Not Deleted! Try Again.";
        }
        exit;
    }

    /*
    ---------------------------------------------------------------------
    On: 02-03-2016
    I/P: 
    O/P:
    Desc: user post status change like Active/Inactive
    ---------------------------------------------------------------------
    */
    public function activeInactiveBeat(){
        $specId = $this->request->data('specId');
        $statusVal = (int) $this->request->data('statusVal');
        $conditions = array('UserPost.id'=> $specId);
        $data = array(
                    'UserPost.status'=> $statusVal,
                );

        if( $this->UserPost->updateAll( $data ,$conditions )){
            /*$statusVal = 'Inactive';
            if( $statusVal == 1 ){ $statusString = 'Active'; }
            if( $statusVal == 0 ){ $statusString = 'Inactive'; }*/
            //echo "OK~$statusString";
            echo "OK~$statusVal";
        }else{
            echo "ERROR~".ERROR_615;
        }
        exit;
    }

    /*
    ---------------------------------------------------------------------
    On: 03-03-2016
    I/P: 
    O/P:
    Desc: User post CSV download
    ---------------------------------------------------------------------
    */
    public function beatCsvDownload(){
      $sortPost['textBeatPostedBy']=$_REQUEST['BeatPostedBy'];
      $sortPost['textBeatTitle']=$_REQUEST['BeatTitle'];
      $sortPost['specilities']=$_REQUEST['specilities'];
      $sortPost['statusVal']=$_REQUEST['statusVal'];
      $sortPost['from']=$_REQUEST['from'];
      $sortPost['to']=$_REQUEST['to'];
      $beat=$_REQUEST['Beat'];
        if(!isset($sortPost['to']) || $sortPost['to']==""){
          $endDate = date("Y-m-d");
        }
        else{
          $endDate = date('Y-m-d', strtotime($sortPost['to']));
        }
        if(!isset($sortPost['from']) || $sortPost['from']==""){
          $date = array();
        }
        else{
          $startDate = date('Y-m-d', strtotime($sortPost['from']));
          $date=array('UserPost.post_date <= ' => $endDate." 23:59:59",'UserPost.post_date >= ' => $startDate);
        }
        if(!isset($sortPost['textBeatTitle']) || $sortPost['textBeatTitle']==""){
            $title=array();
        }
        else{
          $sortPost['textBeatTitle']=trim($sortPost['textBeatTitle']);
            $title=array('LOWER(UserPost.title) LIKE'=>strtolower($sortPost['textBeatTitle'].'%'));
        }
        if(!isset($sortPost['textBeatPostedBy'])||$sortPost['textBeatPostedBy']==""){
            $name=array();
        }
        else{
            $sortPost['textBeatPostedBy']=trim($sortPost['textBeatPostedBy']);
          $name=explode(" ",$sortPost['textBeatPostedBy']);
          $len=count($name);
          for ($i=0; $i < $len; $i++) { 
            if(!$name[$i]==""){
              $postedBy[]=$name[$i];
            }
          }
          $count=count($postedBy);
          if($count!=2){
            $name=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textBeatPostedBy'].'%'));
          }
          else{
            $name=array('LOWER(UserProfile.first_name) ='=>strtolower($postedBy[0]),'LOWER(UserProfile.last_name) ='=>strtolower($postedBy[1]));
          }
        }
        if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
            $interest=array();
        }
        else{
            $interest=array('UserPostSpecilities.specilities_id'=>$sortPost['specilities']);
        }
        switch ($beat){
           case 'all':
              if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==""){
                          $status=array('UserPost.status'=>array(0,1));
                      }
                      else{
                          if($sortPost['statusVal']==3){
                              $status=array('UserPost.status'=>array(0,1),'UserPost.spam_confirmed'=>'0','UserPost.top_beat'=>'1');
                          }
                          else if($sortPost['statusVal']==2){
                              $status=array('UserPost.status'=>array(0,1),'UserPost.spam_confirmed'=>'1');
                          }
                          else if($sortPost['statusVal']==4){
                              $status=array('UserPost.status'=>array(2,3,4));
                          }
                          else{
                          $status=array('UserPost.status'=>$sortPost['statusVal']);
                          }
                       }
              break;  
           
           case 'flagged':
              if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==""){
                          $status=array('UserPost.spam_confirmed'=>1,'UserPost.status'=>array(0,1));
                      }
                      else{
                          $status=array('UserPost.spam_confirmed'=>1,'UserPost.status'=>$sortPost['statusVal']);
                      }
              break;

           case 'featured':
              if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==""){
                          $status=array('UserPost.top_beat'=>1,'UserPost.status'=>array(0,1));
                      }
                      else{
                          $status=array('UserPost.top_beat'=>1,'UserPost.status'=>$sortPost['statusVal']);
                       }
              break;
            }
         $conditions=array_merge($title,$name,$status,$interest,$date);
        $this->layout = '';
        $beatDetails = array();
        // $conditions = " UserPost.status IN (0,1) AND UserPost.spam_confirmed !=1 ";
         $fields = array("ConsentForm.id,ConsentForm.patient_name,ConsentForm.patient_email,UserPost.id, UserPost.title, UserPost.post_date, UserPost.status, UserPost.top_beat, UserPost.spam_confirmed,UserPost.consent_id, UserProfile.first_name, UserProfile.last_name, GROUP_CONCAT(Specility.name) as beatInterests");
        $options=
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'left',
                      'conditions'=> array('UserPost.user_id = User.id')
                    ),
                    array(
                      'table' => 'user_post_specilities',
                      'alias' => 'UserPostSpecilities',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.user_post_id = UserPost.id')
                    ),
                    array(
                      'table' => 'specilities',
                      'alias' => 'Specility',
                      'type' => 'left',
                      'conditions'=> array('UserPostSpecilities.specilities_id = Specility.id')
                    ),
                    array(
                      'table' => 'consent_forms',
                      'alias' => 'ConsentForm',
                      'type' => 'left',
                      'conditions'=> array('ConsentForm.id = UserPost.consent_id')
                    ),
                    
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('UserPost.id'),
                'order'=> 'UserPost.post_date DESC', 
                // 'limit'=> $limit
            );
        $limit = $this->UserPost->find("count", $options);
            $beatDetails=$this->UserPost->find('all',$options,array('limit'=>$limit));
              foreach($beatDetails as $userPost){
                
                if(!empty($userPost['BeatHashMapping'])){
                  // pr($userPost['BeatHashMapping']);
                  $hashMapp[]=$userPost['BeatHashMapping'];
                  foreach ($userPost['BeatHashMapping'] as $value) {
                  $hashName[]=$this->HashTag->find('first',array('conditions'=>array('HashTag.id='.$value['hash_id'])));
                  $map=$hashName;
                  }
                }
                else{
                  $hashName=Null;
                  $map=Null;
                }
                $userPost['tag_name']=$map;
                $userPost['speciality_list'] = $this->speciality_string($userPost['UserPostSpecilities']);
                $userPost['beat_details'] = $this->get_beats_details($userPost['UserPost']['id']);
                $userPost['share_details'] = count($this->get_share_details($userPost['UserPost']['id']));
                //pr($UserPost['tag_name']);
                $userPosts[] = $userPost;
                 // pr($hashName[]);

        }
        $this->set("beatDetails", $userPosts); //echo "<pre>";print_r($beatDetails);die;
    }
  }


  // public function hash_tag($hashTag=array()){
  //   foreach ($hashTag as $value) {
  //     $hash = $this->HashTag->find('all', array(
  //           'conditions' => array(
  //               'HashTag.id'=> $value['hash_id'];
  //           )
  //               // 'fields' => array('UserJoin.*', 'UserPostBeat.*'),
  //               // 'order' => 'UserPostBeat.id DESC'
  //       ));
  //   }
  //   return $hash;
  // }

