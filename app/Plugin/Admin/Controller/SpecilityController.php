<?php
class SpecilityController extends AdminAppController {
    public $components = array('Cookie','Session','Common','Paginator');
    public $name = "Specility";
    public $uses = array('Admin.Specilities');

    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
        'order' => array(
            'Specilities.id' => 'desc'
        )
    );

    /*
    On: 
    I/P:
    O/P:
    Desc:
    */
    public function allSpecilities(){
        if(!empty($this->data['spclId'])){
            $this->layout=null;
            $spclId=$this->data['spclId'];
            $specilities = $this->Specilities->find("first",array('conditions'=>array('id'=>"$spclId")));
            $this->set('specilities', $specilities);
            $this->render('/Elements/edit_speciality');
        }
        if((!empty($this->data['addSpeciality']['name']))){
            // if($this->data['addSpeciality']['icon']['error']=="0"){
                $specData=$this->data['addSpeciality'];
                if(!empty($specData['type'])){
                    $condition=array('id'=>$specData['id']);
                }
                else{
                    $condition=array('name'=>$specData['name']);
                }
                if($specData['icon']['error']=="0"){
                    move_uploaded_file($specData['icon']['tmp_name'],WWW_ROOT . 'img/specilities/'. $specData['icon']['name']);
                    $imgCon=1;
                }
                else{
                    $imgCon=0; 
                }
                $exstSpec = $this->Specilities->find("first",array('conditions'=>$condition));
                if(!empty($exstSpec)){
                    if($imgCon==1){
                    unlink(WWW_ROOT . 'img/specilities/'.$exstSpec['Specilities']['img_name']);
                    $specialityData = array("name"=>"'".$specData['name']."'","img_name"=>"'".$specData['icon']['name']."'","status"=>$specData['status']);
                    }
                    else{
                        $specialityData = array("name"=>"'".$specData['name']."'","status"=>$specData['status']);
                    }
                    $this->Specilities->updateAll($specialityData,array('Specilities.id' => $specData['id']));
                }
                else{
                    $specialityData = array("name"=>$specData['name'],"img_name"=>$specData['icon']['name'],"status"=>$specData['status']);
                    $this->Specilities->saveAll($specialityData);
                }
        }
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='name'){
              $sort="Specilities.name $order";
            }
            else{
            $sort="Specilities.name $order";
            }
        }
        else{
            $sort="Specilities.name ASC";
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $conditions=array('Specilities.status'=>array(0,1));
        $options=array( 'order'=> $sort, 
                        'conditions'=> $conditions,
                        'limit'=> $limit,
                        'page'=>$j
                    );
        $this->Paginator->settings = $options;
        $data = $this->Paginator->paginate('Specilities');
        $this->set(array('specilities'=>$data,'limit'=>$limit));
        /*$specilities = $this->Specilities->find("all");
        //echo '<pre>';print_r($specilities);die;
        $this->set('specilities', $specilities);*/
    }

    public function specialityFilterData(){
        $this->layout=null;
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='name'){
              $sort="Specilities.name $order";
            }
            else{
            $sort="Specilities.name $order";
            }
        }
        else{
            $sort="Specilities.name ASC";
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        if(isset($this->data['textName'])){
            $name=trim($this->data['textName']);
            $name=array('LOWER(Specilities.name) LIKE'=>strtolower($name.'%'));
        }
        else{
            $name="";
        }
        if($this->data['status']!==""){
            $status=array('Specilities.status'=>$this->data['status']);
        }
        else{
            $status=array('Specilities.status'=>array(0,1));
        }
        $conditions=array_merge($name,$status);
        $options=array( 'order'=> $sort, 
                        'conditions'=> $conditions,
                        'limit'=> $limit,
                        'page'=>$j
                    );
        // pr($options);
        $this->Paginator->settings = $options;
        $data = $this->Paginator->paginate('Specilities');
        $this->set(array('specilities'=>$data,'limit'=>$limit,'j'=>$j));
        $this->render('/Elements/speciality_filter_data');
    }

    /*
    On: 
    I/P: 
    O/P:
    Desc: 
    */
    function editSpeciality(){
        $spclId=$this->data['spclId'];
        if(!empty($this->data['addSpeciality'])){
            $specData=$this->data['addSpeciality'];

            $exstSpec = $this->Specilities->find("first",array('conditions'=>array('id'=>$spclId)));

            if($specData['icon']['error']=="0"){
                $filename = md5($specData['name']);
                move_uploaded_file($specData['icon']['tmp_name'],WWW_ROOT . 'img/specilities/'. $filename);

                unlink(WWW_ROOT . 'img/specilities/'.$exstSpec['Specilities']['img_name']);

                $specialityData = array("name"=>"'".$specData['name']."'","img_name"=>"'".$filename."'","status"=>$specData['status']);
            }
            else{
                $specialityData = array("name"=>"'".$specData['name']."'","status"=>$specData['status']);
            }
            
                $this->Specilities->updateAll($specialityData,array('Specilities.id' => $spclId));
           $this->redirect(BASE_URL . 'admin/Specility/allSpecilities');
        }
        $this->layout=null;
        $specilities = $this->Specilities->find("first",array('conditions'=>array('id'=>"$spclId")));
        $this->set('specilities', $specilities);
        $this->render('/Elements/edit_speciality');
    }

    public function deleteSpec(){
        $specId = $this->request->data('specId');
        $conditions = array('Specilities.id'=> $specId);
        $data = array(
                    'Specilities.status'=>"2",
                );
        if( $this->Specilities->updateAll( $data ,$conditions )){
            echo "Deleted";
        }
        exit;
    }

    /*
    On: 
    I/P: 
    O/P:
    Desc: 
    */
    public function changeSpecilityStatus(){
        $specId = $this->request->data('specId');
        $statusVal = $this->request->data('statusVal');
        if( trim($statusVal) == 'Active' ){
            $chngeStatus = 0;
        }else if( trim($statusVal) == 'Inactive' ){
            $chngeStatus = 1;
        }
        //$this->User->id = $userId;
        $conditions = array('Specilities.id'=> $specId);
        $data = array(
                    'Specilities.status'=> $chngeStatus,
                );

        if( $this->Specilities->updateAll( $data ,$conditions )){
            
            if( $chngeStatus == 1 ){ $statusString = 'Active'; }
            if( $chngeStatus == 0 ){ $statusString = 'In Active'; }
            echo "OK~$statusString";
        }else{
            echo "ERROR~".ERROR_615;
        }
        exit;
    }
}