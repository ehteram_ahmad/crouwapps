<div class="right_col" role="main">
<div class="x_panel">
<div class="x_panel" id="userList">
                                <div class="x_title">
                                    <h2>Post Lists </h2>
                                    <p align="right"><span align="right"> Total: <?php echo $this->Paginator->counter(array(
    'separator' => ' of a total of '
)); ?></span></p>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="x_content">

                                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                        <thead>
                                            <tr class="headings">
                                                <th class="column-title" width = "15%"> Post Name</th>
                                                <th class="column-title" width = "15%"> Speciality</th>
                                                <th class="column-title" width = "15%"> Posted By</th>
                                                <th class="column-title" width = "15%"> Posted  On</th>
                                                <th class="column-title" width = "15%"> Counts</th>
                                                <th class="column-title" width = "10%">Status </th>
                                                <th class="column-title no-link last" width = "15%"><span class="nobr">Action</span>
                                                </th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach( $userPostData as $upd){ ?>
                                    <tr class="even pointer">
                                        <td class=" "><?php echo $upd['UserPost']['title'];?></td>
                                        <td class=" "><?php echo $upd['speciality_list'];?></td>
                                        <td class=" ">
                                            <?php 
                                            /*echo $upd['User']['name'] . PHP_EOL . 
                                             '('.$upd['User']['email'].')';*/
                                             echo $upd['User']['name'];
                                        ?>
                                        </td>
                                        <td class=" ">
                                            <?php echo $upd['UserPost']['post_date'];?>
                                        </td>
                                        <td class=" ">
                                                Upbeats (<?php echo count($upd['beat_details']['upbeat']); ?> ) <br />
                                                Downbeats (<?php echo count($upd['beat_details']['downbeat']); ?>) <br />
                                                Comments (<?php echo count($upd['UserPostComment']); ?>) <br />
                                                Share (<?php echo $upd['share_details']; ?>) 
                                        </td>
                                        <td class=" ">
                                            <span id="<?php echo $upd['UserPost']['id']; ?>" onclick="updatePostStatus(<?php echo $upd['UserPost']['id']; ?>);">
                                            <?php 
                                            echo ($upd['UserPost']['status'] == 1 ? 'Active':'Inactive'); 
                                            ?>
                                        </span>
                                        </td>
                                        <td class=" last">
                                            <button type="button"  data-toggle="modal" data-target="#myModal" onclick="viewPostDetails(<?php echo $upd['UserPost']['id']; ?>)">Details</button>
                                            &nbsp;
                                            <button type="button" onclick="toggleTop(<?php echo $upd['UserPost']['id']; ?>)">
                                                <?php 
                                                    if($upd['UserPost']['top_beat'] == 1){
                                                        echo 'Top Beat';
                                                    }else{
                                                        echo 'Not a top beat';
                                                    }
                                                ?>
                                            </button>
                                            &nbsp;
                                            <button type="button"  data-toggle="modal" data-target="#myModal" onclick="viewPostComments(<?php echo $upd['UserPost']['id']; ?>)">Mark flag</button>
                                        </td>
                                        </tr>
                                     <?php } ?>       
                                            </tbody>

                                    </table>
                                    <?php   echo $this->Paginator->numbers(array('first' => 2, 'last' => 2)); ?>
                            </div>
     </div>  
     <!-- Modal Box [START] -->
        <div id="modalBox"></div>
     <!-- Modal Box [END] -->
                   