<?php 
// pr($adminFeature);
// exit();
$count=count($userPostdata);
 ?>
 <style type="text/css">
     div.image.cbp-item img{
        height:110px;
     }
     p{
        width: 100%;word-wrap: break-word;
     }
 </style>
<!-- BEGIN CONTENT -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Beat Details</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo BASE_URL . 'subadmin/SubadminHome/dashboard';?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo BASE_URL . 'subadmin/SubadminUserPost/userPostLists';?>">All Posts</a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span class="active">Post Details</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            Beat Details </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <!--<h2 class="margin-bottom-20"> View User Info - Bob Nilson </h2>
                                <h3 class="form-section">Person Info</h3>-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Posted On:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <?php
                                              $datetime = explode(" ",$userPostdata[0]['UserPost']['post_date']);
                                              $date = $datetime[0];
                                              $time = $datetime[1];
                                              echo date('d-m-Y',strtotime($date))."  ".date ('g:i:s a',strtotime($time));
                                              ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Posted By:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <?php 
                                                echo $userPostdata[0]['UserProfile']['first_name'].' '.$userPostdata[0]['UserProfile']['last_name'];
                                                 ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Title:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <?php 
                                                echo $userPostdata[0]['UserPost']['title'];
                                                 ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Interests:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <?php
                                                if($userPostdata[0]['specialitystring']==""){
                                                    echo "NULL";
                                                }
                                                else{ 
                                                echo $userPostdata[0]['specialitystring'];}
                                                 ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                	<div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Description:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <?php
                                            foreach ($userPostdata as $key => $value) {
                                                    if(isset($value['UserPostAttribute'])){
                                                    $descAtt=$value['UserPostAttribute'];
                                                }}
                                                // $attr=array_merge($descAtt);
                                                foreach ($descAtt as $key => $value) {
                                                    if($value['attribute_type']=='text'){
                                                        echo $value['content'];
                                                    }
                                                    } 

                                            ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Hashtags:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <?php 
                                                foreach ($userPostdata as $key => $value) {
                                                    if(isset($value['HashTag']))
                                                    {
                                                        $tag[]=$value['HashTag'];
                                                    }
                                                    else{
                                                        $tag=NULL;
                                                    }
                                                }
                                                $tag = array_map("unserialize", array_unique(array_map("serialize", $tag)));
                                                $tagCount=count($tag);
                                                if(isset($tag[0]['id'])){
                                                for ($i=0; $i<$tagCount ; $i++) { 
                                                   echo "#".$tag[$i]['name'].",";
                                                }}
                                                else{
                                                    echo "NULL";
                                                }
                                                 ?> </p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    
                                </div>
                                <!--/row-->
                                <div class="row">
                                	
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Video:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                <?php 
                                                foreach ($userPostdata as $key => $value) {
                                                    if(isset($value['UserPostAttribute'])){
                                                    $att=$value['UserPostAttribute'];
                                                }}
                                                $attr=array_merge($att);
                                                foreach ($att as $key => $value) {
                                                    if($value['attribute_type']=='video'){
                                                        ?>
                                                        <a class="fontSize20" data-toggle="modal" href="<?php echo $value['content']; ?>" target="_blank"><i class="fa fa-file-video-o" aria-hidden="true"></i></a>
                                                   <?php
                                                    }}                                                 ?>
                                                
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Documents:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                <?php
                                                foreach ($userPostdata as $key => $value) {
                                                    if(isset($value['UserPostAttribute'])){
                                                    $attr=$value['UserPostAttribute'];
                                                }}
                                                $docCnt = 1;
                                                $attr=array_merge($attr);
                                                $attrCount=count($attr);
                                                for ($i=0; $i < $attrCount; $i++) { 
                                                if(in_array($userPostdata[0]['UserPostAttribute'][$i]['attribute_type'],array('doc','docx','DOC','DOCX','ppt','PPT','pptx','PPTX','xls','XLS','xlsx','XLSX','pdf','PDF','rtf','RTF'))){
                                                $doc[$docCnt]=$userPostdata[0]['UserPostAttribute'][$i]['content'];
                                                $type[$docCnt]=explode(".",$doc[$docCnt]);
                                                $type[$docCnt]=$type[$docCnt][1];
                                                $docCnt++;
                                                    }
                                                }
                                                for($j=1;$j<=4;$j++){
                                                    if(isset($type[$j])){
                                                   switch (strtolower($type[$j])) {
                                                        case 'doc':case 'docx':case 'rtf':
                                                            $file="word";
                                                        break;
                                                        case 'xls':case 'xlsx':
                                                            $file="excel";
                                                        break;
                                                        case 'pdf':
                                                            $file="pdf";
                                                        break;
                                                        case 'ppt':case 'pptx':
                                                            $file="powerpoint";
                                                        break;
                                                    }}
                                                    if(isset($doc[$j])){ ?>
                                                        <a class="fontSize20" href="<?php echo AMAZON_PATH.'posts/'.$id.'/doc/'.$doc[$j];?>" target="_blank"> <i class="fa fa-file-<?php echo $file; ?>-o" aria-hidden="true"></i></a>
                                                    <?php } }
                                                        ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--<div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Consent Form:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> </p>
                                            </div>
                                        </div>
                                    </div>-->
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Consent Form:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                            <?php 
                                if(!$userPostdata[0]['UserPost']['consent_id']>0){
                                    echo "NULL";
                                }
                                else{
                                    $consentId=$userPostdata[0]['UserPost']['consent_id'];
                                
                                foreach ($userPostdata as $key => $value) {
                                   if(isset($value['ConsentForm'])){
                                    $ConsentForm[]=$value['ConsentForm'];
                                   }
                                }
                                $ConsentForm = array_map("unserialize", array_unique(array_map("serialize", $ConsentForm)));
                                // pr($ConsentForm);
                                $consnetCount=count($ConsentForm);
                                for($i=0;$i<$consnetCount;$i++){
                                    echo $ConsentForm[$i]['patient_name']."<br>";
                                    echo $ConsentForm[$i]['patient_email'];
                                }
                                  }   ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <?php 
                                    if(count($adminSpam)>0){
                                        $adminStatus="<b> ( By Admin )</b>";
                                    }else{
                                        $adminStatus="";
                                    }

                                     ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 bold">Status:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    <?php
                                                    if($userPostdata[0]['UserPost']['status']=="1")
                                                    {
                                                        echo "Active";
                                                    }
                                                    elseif ($userPostdata[0]['UserPost']['status']=="0") {
                                                        echo "Inactive";
                                                    }
                                                    else{
                                                        echo "Deleted";
                                                    }
                                                    if($userPostdata[0]['UserPost']['spam_confirmed']=="1"){
                                                        echo ", Flagged".$adminStatus;
                                                    }
                                                    if($userPostdata[0]['UserPost']['top_beat']=="1"){
                                                        echo ", Featured";
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="form-section">Images</h3>
                                <div class="row">
                                    <div id="js-grid-full-width" class="tiles cbp">
                            <?php
                            if(!empty($userPostdata[0]['UserPostAttribute']) && isset($userPostdata[0]['UserPostAttribute'])){
                                foreach ($userPostdata[0]['UserPostAttribute'] as $key => $value) {
                                   if($value['attribute_type']=="image"){
                                    $imgData[]=$value['content'];
                                   }
                                }
                                if(!isset($imgData)){
                                    $imgData=NULL;
                                }
                                $imgcount=count($imgData);
                            }
                                if(!empty($imgData)){
                                for ($i=0; $i< $imgcount ; $i++) { 
                                    
                                        ?>
                                        <div class="tile image cbp-item">
                                            <div class="tile-body ">
                                                <a href="<?php echo AMAZON_PATH.'posts/'.$userPostdata[0]['UserPost']['id'].'/image/'.$imgData[$i]; ?>" class="cbp-caption cbp-lightbox">
                                                    <div class="cbp-caption-defaultWrap">
                                                        <img src="<?php echo AMAZON_PATH.'posts/'.$userPostdata[0]['UserPost']['id'].'/image/'.$imgData[$i]; ?>" alt="">
                                                    </div>
                                                </a>
                                            </div>
                                            </div>
                                        <?php
                                            }

                                        }
                                        else{ ?>
                                              <div class="tile image cbp-item">
                                            <div class="tile-body ">
                                                <a href="" class="cbp-caption cbp-lightbox">
                                                    <div class="cbp-caption-defaultWrap">
                                                        <img width="100%" src="<?php echo BASE_URL.'subadmin/img/noimage.jpg'; ?>" alt="">
                                                    </div>
                                                </a>
                                            </div>
                                            </div>
                                          <?php 
                                           }
                                        ?>
                                    </div>
                                </div>
                        <!--/row-->
                    
                            <div class="form-actions">
                                <div class="row">
                                            <div class="txtCenter">
                                                <!-- <a type="submit" class="btn green2" href="<?php echo BASE_URL . 'subadmin/UserPost/editBeat/'.$userPostdata[0]['UserPost']['id'];?>">
                                                    <i class="fa fa-pencil"></i> Edit</a> -->
                                                    <a class="btn default" href="<?php echo BASE_URL . 'subadmin/SubadminUserPost/userPostLists';?>">Cancel</a>
                                                <!-- <button id="sendEmailBtn" type="button" class="btn btn-info">Send Mail</button> -->
                                            </div>
                                        </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                <!-- BEGIN SAMPLE TABLE PORTLET--></form></div>
                <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-green2-haze bold uppercase">STATISTICS</span>
                                    </div>
                                    <div class="tools">
                                        <a href="" class="collapse"> </a>
                                        <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                        <a href="" class="reload"> </a>
                                        <a href="" class="remove"> </a>-->
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                     <!-- BEGIN WIDGET TAB -->
                                    <div class="widget-bg-color-white widget-tab">
                                        <ul class="nav nav-tabs">
                                            <li id="li_1_1" class="">
                                                <a id="pan_1_1" href="#tab_1_1" data-toggle="tab" class="iconCounts tooltips" data-original-title="Up Beats">
                                                    <i class="demo-icon icon2-up fontSize20"></i>
                                                    <span class="badge badge-success2"><?php echo count($userPostdata[0]['beat_details']['upbeat']); ?></span>
                                                </a>
                                            </li>
                                            <li id="li_1_2" class="">
                                                <a id="pan_1_2" href="#tab_1_2" data-toggle="tab" class="iconCounts tooltips" data-original-title="Down Beats">
                                                    <i class="demo-icon icon2-down fontSize20"></i>
                                                    <span class="badge badge-success2"><?php echo count($userPostdata[0]['beat_details']['downbeat']); ?></span>
                                                </a>
                                            </li>
                                            <li id="li_1_3" class="">
                                                <a id="pan_1_3" href="#tab_1_3" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Comments">
                                                    <i class="fa fa-comment-o fontSize20"></i>
                                                    <span class="badge badge-success2"><?php echo count($userPostdata[0]['UserPostComment']); ?></span>
                                                </a>
                                            </li>
                                            <li id="li_1_4" class="">
                                                <a id="pan_1_4" href="#tab_1_4" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Shares">
                                                    <i class="fa fa-share-alt fontSize20"></i>
                                                    <span class="badge badge-success2"><?php echo count($userPostdata[0]['share_details']); ?></span>
                                                </a>
                                            </li>
                                            <li id="li_1_5" class="">
                                                <a id="pan_1_5" href="#tab_1_5" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Views">
                                                    <i class="fa fa-eye fontSize20"></i>
                                                    <span class="badge badge-success2"><?php echo count($userPostdata[0]['BeatView']); ?></span>
                                                </a>
                                            </li>
                                            <li id="li_1_6" class="">
                                                <a id="pan_1_6" href="#tab_1_6" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Tagged">
                                                    <i class="fa fa-tag fontSize20"></i>
                                                    <span class="badge badge-success2">
                                                    <?php 
                                                    $tagcount='0';
                                                    for ($i=1; $i<$count ; $i++){
                                                        if(!empty($userPostdata[$i]['PostUserTag']['user_id'])){
                                                            // pr($userPostdata[$i]['PostUserTag']);
                                                            $tagcount++;
                                                        }
                                                    }
                                                    echo $tagcount;
                                                     ?>
                                                     </span>
                                                </a>
                                            </li>
                                            <li id="li_1_7" class="">
                                                <a id="pan_1_7" href="#tab_1_7" data-toggle="tab" class="iconCounts marTop2 tooltips" data-original-title="Flaged">
                                                    <i class="fa fa-flag fontSize20"></i>
                                                    <span class="badge badge-success2">
                                                        <?php 
                                                        if(!empty($userPostdata[0]['UserPostSpam'])){
                                                            $spam=$userPostdata[0]['UserPostSpam']; 
                                                            }
                                                            else{
                                                                $spam=NULL;
                                                            }
                                                            $userSpamCount=count($spam);
                                                            echo $userSpamCount;
                                                         ?>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content scroller2" style="height: 380px;" data-always-visible="1" data-handle-color="#D7DCE2">
                                            <div class="tab-pane fade" id="tab_1_1"></div>
                                            <div class="tab-pane fade" id="tab_1_2"></div>
                                            <div class="tab-pane fade" id="tab_1_3"></div>
                                            <div class="tab-pane fade" id="tab_1_4"></div>
                                            <div class="tab-pane fade" id="tab_1_5"></div>
                                            <div class="tab-pane fade" id="tab_1_6"></div>
                                            <div class="tab-pane fade" id="tab_1_7"></div>
                                        </div>
                                    </div>
                                    <!-- END WIDGET TAB -->
                                </div>
                            </div>
                <!-- END SAMPLE TABLE PORTLET-->
                <div class="portlet light bordered customePadding">
                  <div class="portlet-title">
                    <div class="caption"><span class="caption-subject font-green2-sharp bold uppercase">Action History</span> </div>
                    <div class="tools"> <a href="javascript:;" class="expand"> </a> </div>
                  </div>
                  <div class="portlet-body flip-scroll" style="display: none;">
                    <div class="row">
                      <div class="col-md-12 col-xs-12">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                          <thead class="flip-content">
                            <tr>
                              <th class="width40px txtCenter">#</th>
                              <th class="txtCenter"> Type </th>
                              <th class="txtCenter"> Subject </th>
                              <th class="txtCenter"> Comments </th>
                              <th class="txtCenter"> Date </th>
                              <th class="txtCenter width60px">&nbsp;  </th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php
                          if((count($adminFeature) > 0) | (count($adminSpam) > 0) | (count($mailLog) > 0)){ 
                              if((count($adminFeature) > 0)){
                          for ($k=0; $k < count($adminFeature) ; $k++) {
                          if($adminFeature[$k]['FeaturedActivityLog']['status']==1){
                                  $state= "Featured";
                              }elseif ($adminFeature[$k]['FeaturedActivityLog']['status']==0) {
                                  $state= "Unfeatured";
                              } ?>
                            <tr>
                              <td class="txtCenter"><?php echo $k+1; ?></td>
                              <td class="txtCenter"> Feature Status </td>
                              <td class="txtCenter"> <?php echo $state." By Admin"; ?> </td>
                              <td class="txtCenter"> <?php echo $state; ?> </td>
                              <td class="txtCenter"><?php echo $adminFeature[$k]['FeaturedActivityLog']['created']; ?></td>
                              <td class="txtCenter">
                              <!-- <button id="viewMailBtn" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="View"><i class="fa fa-eye"></i></button> -->
                              </td>
                            </tr>
                          <?php }} 
                              if((count($adminSpam) > 0)){
                          for ($i=0; $i < count($adminSpam) ; $i++) { ?>
                            <tr>
                              <td class="txtCenter"><?php echo $k+$i+1; ?></td>
                              <td class="txtCenter"> Mark Flagged </td>
                              <td class="txtCenter"> Flagged By Admin </td>
                              <td class="txtCenter"> <?php echo $adminSpam[$i]['AdminPostSpam']['content']; ?> </td>
                              <td class="txtCenter"><?php echo $adminSpam[$i]['AdminPostSpam']['created']; ?></td>
                              <td class="txtCenter">
                              <!-- <button id="viewMailBtn" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="View"><i class="fa fa-eye"></i></button> -->
                              </td>
                            </tr>
                          <?php }}
                              if(count($mailLog) > 0){
                         for ($j=0; $j < count($mailLog); $j++) { ?>
                         <tr>
                           <td class="txtCenter"><?php echo $i+$j+$k+1; ?></td>
                           <td class="txtCenter"> User Mailing </td>
                           <td class="txtCenter">
                               <?php echo $mailLog[$j]['AdminUserSentMail']['email_category_id'];  ?>
                           </td>
                           <td class="txtCenter"> <?php echo $mailLog[$j]['AdminUserSentMail']['comment']; ?> </td>
                           <td class="txtCenter"><?php echo $mailLog[$j]['AdminUserSentMail']['created_date']; ?></td>
                           <td class="txtCenter"><button id="<?php echo $mailLog[$j]['AdminUserSentMail']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 tooltips viewMailBtn" data-original-title="View"><i class="fa fa-eye"></i></button></td>
                         </tr>
                         <?php }}}else{ ?>
                          <tr>
                              <td colspan="6" class="text-center"><h3> No Action Has Taken Yet. </h3></td>
                          </tr>
                         <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!--/row--> 
                  </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN VIEW MAIL POP-UP --> 
<div id="viewMail" class="modal fade" tabindex="-1" data-replace="true">
</div>
<!-- END VIEW MAIL POP-UP -->
<?php 
// $id=$_REQUEST['id'];
 ?>
<script type="text/javascript">
    $(document).ready(function(){
var id= '<?php echo $id; ?>';
// Default comment tab open--------START
            $('.nav-tabs li').removeClass("active");
            $('.tab-content .active').removeClass("active").removeClass("in");
            $('#li_1_3').addClass("active");
            $('#tab_1_3').addClass("active").addClass("in");
            $.ajax({
                url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/comments',
                type: "POST",
                data: {'postId':id,'tabId':'comments'},
                success: function(data) { 
                    $('#tab_1_1,#tab_1_2,#tab_1_4,#tab_1_5,#tab_1_6,#tab_1_7').empty();
                    $('#tab_1_3').html(data);
                }
            });
// Default comment tab open--------END
        $("#pan_1_1").click(function(){
            $('.nav-tabs li').removeClass("active");
            $('.tab-content .active').removeClass("active").removeClass("in");
            $('#li_1_1').addClass("active");
            $('#tab_1_1').addClass("active").addClass("in");
            $.ajax({
                url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/upBeat',
                type: "POST",
                data: {'postId':id,'tabId':'upBeat'},
                success: function(data) { 
                    // alert(data);
                    $('#tab_1_1').html(data);
                    $('#tab_1_2,#tab_1_3,#tab_1_4,#tab_1_5,#tab_1_6,#tab_1_7').empty();
                }
            });
            // return false; 
        });
        $("#pan_1_2").click(function(){
            $('.nav-tabs li').removeClass("active");
            $('.tab-content .active').removeClass("active").removeClass("in");
            $('#li_1_2').addClass("active");
            $('#tab_1_2').addClass("active").addClass("in");
            $.ajax({
                url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/downBeat',
                type: "POST",
                data: {'postId':id,'tabId':'downBeat'},
                success: function(data) { 
                    // alert(data);
                    $('#tab_1_2').html(data);
                    $('#tab_1_1,#tab_1_3,#tab_1_4,#tab_1_5,#tab_1_6,#tab_1_7').empty();
                }
            });
            return false;
        });
        $("#pan_1_3").click(function(){
            $('.nav-tabs li').removeClass("active");
            $('.tab-content .active').removeClass("active").removeClass("in");
            $('#li_1_3').addClass("active");
            $('#tab_1_3').addClass("active").addClass("in");
            $.ajax({
                url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/comments',
                type: "POST",
                data: {'postId':id,'tabId':'comments'},
                success: function(data) { 
                    // alert(data);
                    $('#tab_1_3').html(data);
                    $('#tab_1_1,#tab_1_2,#tab_1_4,#tab_1_5,#tab_1_6,#tab_1_7').empty();
                }
            });
            return false;
        });
        $("#pan_1_4").click(function(){
            $('.nav-tabs li').removeClass("active");
            $('.tab-content .active').removeClass("active").removeClass("in");
            $('#li_1_4').addClass("active");
            $('#tab_1_4').addClass("active").addClass("in");
            $.ajax({
                url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/shares',
                type: "POST",
                data: {'postId':id,'tabId':'shares'},
                success: function(data) { 
                    // alert(data);
                    $('#tab_1_4').html(data);
                    $('#tab_1_1,#tab_1_2,#tab_1_3,#tab_1_5,#tab_1_6,#tab_1_7').empty();
                }
            });
            return false;
        });
        $("#pan_1_5").click(function(){
            $('.nav-tabs li').removeClass("active");
            $('.tab-content .active').removeClass("active").removeClass("in");
            $('#li_1_5').addClass("active");
            $('#tab_1_5').addClass("active").addClass("in");
            $.ajax({
                url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/views',
                type: "POST",
                data: {'postId':id,'tabId':'views'},
                success: function(data) { 
                    // alert(data);
                    $('#tab_1_5').html(data);
                    $('#tab_1_1,#tab_1_2,#tab_1_3,#tab_1_4,#tab_1_6,#tab_1_7').empty();
                }
            });
            return false;
        });
        $("#pan_1_6").click(function(){
            $('.nav-tabs li').removeClass("active");
            $('.tab-content .active').removeClass("active").removeClass("in");
            $('#li_1_6').addClass("active");
            $('#tab_1_6').addClass("active").addClass("in");
            $.ajax({
                url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/tagged',
                type: "POST",
                data: {'postId':id,'tabId':'tagged'},
                success: function(data) { 
                    // alert(data);
                    $('#tab_1_6').html(data);
                    $('#tab_1_1,#tab_1_2,#tab_1_3,#tab_1_4,#tab_1_5,#tab_1_7').empty();
                }
            });
            return false;
        });
        $("#pan_1_7").click(function(){
            $('.nav-tabs li').removeClass("active");
            $('.tab-content .active').removeClass("active").removeClass("in");
            $('#li_1_7').addClass("active");
            $('#tab_1_7').addClass("active").addClass("in");
            $.ajax({
                url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/flagged',
                type: "POST",
                data: {'postId':id,'tabId':'flagged'},
                success: function(data) { 
                    // alert(data);
                    $('#tab_1_7').html(data);
                    $('#tab_1_1,#tab_1_2,#tab_1_3,#tab_1_4,#tab_1_5,#tab_1_6').empty();
                }
            });
            return false;
        });

    });
$(".viewMailBtn").click(function(){
  id=$(this).attr('id');
  email='<?php echo $email; ?>';
  $.ajax({
           url: '<?php echo BASE_URL; ?>subadmin/SubadminUserPost/templatePreview',
           type: "POST",
           data: {'id': id,'type':"view",'email':email},
           success: function(data) { 
            $("#viewMail").html(data);
            $("#viewMail").modal('show');
          }
      });

});
$("#sendEmailBtn").click(function(){
    id='<?php echo $id; ?>';
  window.location.href= BASE_URL +'subadmin/UserPost/beatMail/'+id;
});
$("#menu>ul>li").eq(2).addClass('active open');
</script>