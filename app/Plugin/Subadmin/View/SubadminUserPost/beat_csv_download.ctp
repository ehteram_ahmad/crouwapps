<?php 
	$header = array("S. No.", "Beat Title", "Posted By","Posted On","Status", "Interests","Hash Tags","Consent Form", "Text", "Video", "Image1", "Image2", "Image3", "Image4", "Image5", "Image6",  "Document1", "Document2", "Document3", "Document4","UpBeat Count","DownBeat Count","Comments Count","Shares Count","Views Count","Tagged Count","Flagged Count");
	 $this->CSV->addRow($header);
	 $sno = 1;

	 foreach ($beatDetails as $beatDetail)
	 {
	 	$tagCount=count($beatDetail['tag_name']);
	 	if($tagCount>0){
	 	for ($i=0; $i<$tagCount ; $i++) { 
	 	   $m[$i]= "#".$beatDetail['tag_name'][$i]['HashTag']['name'];
	 	   $hashTag=implode(",",$m);
	 	}}
	 	else{$hashTag=" N/A ";}
	 // pr($beatDetail['PostUserTag']);	

	 	$beatTitle = !empty($beatDetail['UserPost']['title']) ? $beatDetail['UserPost']['title'] : "N/A";
	 	$postedBy = $beatDetail['UserProfile']['first_name'] . " " . $beatDetail['UserProfile']['last_name'] ;

	 	// $datetime = explode(" ",$beatDetail['UserPost']['post_date']);
	 	// $date = $datetime[0];
	 	// $date= date('d-m-Y',strtotime($date));
	 	// $postedOn = $date;
	 	$postedOn = $beatDetail['UserPost']['post_date'];

	 	$beatInterests = !empty($beatDetail[0]['beatInterests']) ? $beatDetail[0]['beatInterests'] : "N/A";
	 	$UpBeat = count($beatDetail['beat_details']['upbeat']);
	 	$DownBeat = count($beatDetail['beat_details']['downbeat']);
	 	$Comments = count($beatDetail['UserPostComment']);
	 	if($beatDetail['share_details']=="0"){
	 	    $Shares="0";
	 	}
	 	    else{
	 	$Shares = $beatDetail['share_details']; 
	 }
	 	$Views = count($beatDetail['BeatView']);
	 	$tags = count($beatDetail['PostUserTag']);
	 	$spams = count($beatDetail['UserPostSpam']);
	 	if(!empty($beatDetail['ConsentForm']['patient_name'])){
	 		$ConsentForm = $beatDetail['ConsentForm']['patient_name'].",".$beatDetail['ConsentForm']['patient_email'];
	 	}
	 	else{
	 		$ConsentForm = " N/A ";
	 	}
	 	if($beatDetail['UserPost']['status']=="1")
	 	{
	 	    $status1="Active";
	 	}
	 	elseif ($beatDetail['UserPost']['status']=="0") {
	 	    $status1= "Inactive";
	 	}
	 	else{
	 	    $status1="Deleted";
	 	}
	 	if($beatDetail['UserPost']['spam_confirmed']=="1"){
	 	    $status2=", Flagged";
	 	}
	 	else{
	 		$status2="";
	 	}
	 	if($beatDetail['UserPost']['top_beat']=="1"){
	 	    $status3=", Featured";
	 	}
	 	else{
	 		$status3="";
	 	}
	 	$status=$status1.$status2.$status3;
	 	$attrCount=count($beatDetail['UserPostAttribute']);
	 		//** Initialize variables [START]
	 		$imgCnt = 1; 
	 		$docCnt = 1;
	 		$textAttribute = " N/A ";
	 		$videoAttribute = " N/A ";
	 		$imageAttribute[1] = " N/A ";
	 		$imageAttribute[2] = " N/A ";
		 	$imageAttribute[3] = " N/A ";
		 	$imageAttribute[4] = " N/A ";
		 	$imageAttribute[5] = " N/A ";
		 	$imageAttribute[6] = " N/A ";
		 	$docAttribute[1] = " N/A ";
		 	$docAttribute[2] = " N/A ";
		 	$docAttribute[3] = " N/A ";
		 	$docAttribute[4] = " N/A ";
		 	$imgPath = ""; 
		 	$docPath = "";
		 	$imgPath = AMAZON_PATH . 'posts/'.$beatDetail['UserPost']['id'].'/image/';
		 	$docPath = AMAZON_PATH . 'posts/'.$beatDetail['UserPost']['id'].'/doc/';
		 	//** Initialize variables [END]
			for($i=0; $i < $attrCount; $i++){
				if($beatDetail['UserPostAttribute'][$i]['attribute_type']=="text"){
					$textAttribute=$beatDetail['UserPostAttribute'][$i]['content'];
				}
				if($beatDetail['UserPostAttribute'][$i]['attribute_type']=="video"){
					$videoAttribute=$beatDetail['UserPostAttribute'][$i]['content'];
				}
				if(in_array($beatDetail['UserPostAttribute'][$i]['attribute_type'],array("doc","docx","xls","xlsx","pdf","ppt","pptx","rtf"))){
					$docAttribute[$docCnt]=$docPath.$beatDetail['UserPostAttribute'][$i]['content'];
					$docCnt++;
				}
				if(in_array($beatDetail['UserPostAttribute'][$i]['attribute_type'],array("image"))){
						$imageAttribute[$imgCnt]=$imgPath.$beatDetail['UserPostAttribute'][$i]['content'];
						$imgCnt++;
					}
			}
	 		
	 	//** Set beat values to CSV
	 	$beatList = array(
	      					$sno, 
	      					$beatTitle,
	      					$postedBy,
	      					$postedOn,
	      					$status,
	      					$beatInterests,
	      					$hashTag,
	      					$ConsentForm,
	      					$textAttribute,
	      					$videoAttribute,
	      					$imageAttribute[1],
	      					$imageAttribute[2],
	      					$imageAttribute[3],
	      					$imageAttribute[4],
	      					$imageAttribute[5],
	      					$imageAttribute[6],
	      					$docAttribute[1],
	      					$docAttribute[2],
	      					$docAttribute[3],
	      					$docAttribute[4],
	      					$UpBeat,
	      					$DownBeat,
	      					$Comments,
	      					$Shares,
	      					$Views,
	      					$tags,
	      					$spams,
	      					

	      				);
	       $this->CSV->addRow($beatList);
	       $sno++;
	 }
	 $filename='BeatLists '. date("d/m/Y");
	 echo  $this->CSV->render($filename);
 ?>