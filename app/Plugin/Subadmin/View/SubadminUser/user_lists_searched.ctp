<div class="x_title">
                                    <h2>User Lists </h2>
                                    <p align="right"><span align="right"></span></p>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="x_content">
<table class="table table-striped responsive-utilities jambo_table bulk_action">
    <?php if( !empty($userData) ){ ?>
                                        <thead>

                                            <tr class="headings">
                                                <th>
                                                    <!--<div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" class="flat" id="check-all" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>-->
                                                </th>
                                                <th class="column-title"> First Name</th>
                                                <th class="column-title">Last Name</th>
                                                <th class="column-title">Email</th>
                                                <th class="column-title">Joining Date</th>
                                                <th class="column-title">Status </th>
                                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                                </th>
                                                
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach( $userData as $ud){ ?>
                                    <tr class="even pointer">
                                        <td class="a-center ">
                                            <!--<div class="icheckbox_flat-green" style="position: relative;"><input type="checkbox" name="table_records" class="flat" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>-->
                                        </td>
                                        <td class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
                                        <td class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
                                        <td class=" "><?php echo $ud['User']['email'];?> </td>
                                        <td class=" "><?php echo date('Y-m-d', strtotime($ud['User']['registration_date']));?></td>
                                        <td class=" ">
                                            <span id="<?php echo $ud['User']['id']; ?>" onclick="updateStatus(<?php echo $ud['User']['id']; ?>);">
                                            <?php 
                                            echo ($ud['User']['status'] == 1 ? 'Active':'In Active'); 
                                            ?>
                                            </span>
                                        </td>
                                        <td class=" last">
                                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" onclick="getModalBox(<?php echo $ud['User']['id']; ?>)">View</button>
                                        </td>
                                        </tr>
                                     <?php } ?>       
                                            </tbody>
                                            <?php } else { ?>
                                           <thead>

                                            <tr class="headings" align="center">
                                                <td colspan="7"><span style="font-color:red;">Record(s) Not Found! </span></td>
                                            </tr> 
                                            <thead>
                                            <?php } ?>
                                    </table>
                                     <?php   echo $this->Paginator->numbers(array('first' => 2, 'last' => 2)); ?>
</div>                                  