<?php 
$header = array("S. No.", "First Name", "Last Name","Gender","Contact Number","Number Verified","User Approved","Date Of Birth", "Email", "Joining Date","GMC Number","Status","iOS-MB","Android-MB","Web-MB","Colleagues Count","Follower Count","Following Count","Address","City","County","Country","Profession","Interests","Specialities","Profile Image","Verification Image","Registration Source");
 $this->CSV->addRow($header);
 $sno = 1;
 $count=count($userLists);
 foreach ($userLists as $users){
 	$fname[]=!empty($users['UserProfile']['first_name']) ? $users['UserProfile']['first_name'] : 'Not Available';
 	$lname[]=!empty($users['UserProfile']['last_name']) ? $users['UserProfile']['last_name'] : 'Not Available';
 	$gender[]=!empty($users['UserProfile']['gender']) ? $users['UserProfile']['gender'] : 'Not Available';
 	$Cno[]=!empty($users['UserProfile']['contact_no']) ? $users['UserProfile']['contact_no'] : 'Not Available';
 	if($users['UserProfile']['contact_no_is_verified']=='0'){
 		$noVerif[]="Not Verified";
 	}
 	else{
 		$noVerif[]="Verified";
 	}
 	if($users['User']['approved']=='0'){
 		$userApprove[]="Not Approved";
 	}
 	else{
 		$userApprove[]="Approved";
 	}
 	if($users['UserProfile']['dob']=='0000-00-00'){
 		$dob[]="Not Available";
 	}
 	else{
 		$dob[]=$users['UserProfile']['dob'];
 	}
 	$mail[]=!empty($users['User']['email']) ? $users['User']['email'] : 'Not Available';
 	$doj[]=!empty($users['User']['registration_date']) ? $users['User']['registration_date'] : 'Not Available';
 	$gmcNo[]=!empty($users['UserProfile']['gmcnumber']) ? $users['UserProfile']['gmcnumber'] : 'Not Available';
 	if($users['User']['status']=='0'){
 		$status[]="Inactive";
 	}
 	else{
 		$status[]="Active";
 	}
 	$address[]=!empty($users['UserProfile']['address']) ? $users['UserProfile']['address'] : 'Not Available';
 	$city[]=!empty($users['UserProfile']['city']) ? $users['UserProfile']['city'] : 'Not Available';
 	$county[]=!empty($users['UserProfile']['county']) ? $users['UserProfile']['county'] : 'Not Available';
 	$country[]=!empty($users['Country']['country_name']) ? $users['Country']['country_name'] : 'Not Available';
 	$proff[]=!empty($users['Profession']['profession_type']) ? $users['Profession']['profession_type'] : 'Not Available';
 	$prfImg[]=!empty($users['UserProfile']['profile_img']) ? $users['UserProfile']['profile_img'] : 'Not Available';
 	$verifImg[]=!empty($users['UserProfile']['registration_image']) ? $users['UserProfile']['registration_image'] : 'Not Available';
 	$source[]=!empty($users['User']['registration_source']) ? $users['User']['registration_source'] : 'Not Available';
}
	foreach ($interests as $value) {
		if(!empty($value)){
			$nm=implode(",", $value);
		}
		else{
			$nm="Not Available";
		}
		$interest[]=$nm;
	}
	foreach ($specilities as $key) {
		if(!empty($key)){
			$sm=implode(",", $key);
		}
		else{
			$sm="Not Available";
		}
		$speciality[]=$sm;
	}
	foreach ($device as $data) {
		$android='Inactive';
		$ios='Inactive';
		$web='Inactive';
		if($data['androidMB'] > '0'){
			$android='Active';
		}
		if($data['iosMB'] > '0'){
			$ios='Active';
		}
		if($data['webMB'] > '0'){
			$web='Active';
		}
		$androidMB[]=$android;
		$iosMB[]=$ios;
		$webMB[]=$web;
	}
	foreach ($colleage as $colCount) {
		$colleages[]=$colCount;
	}
	foreach ($follower as $followerCount) {
		$followr[]=$followerCount;
	}
	foreach ($following as $followingCount) {
		$followng[]=$followingCount;
	}
	for ($i=0; $i < $count ; $i++) { 
$userList=array(
	$sno,
	$fname[$i],
	$lname[$i],
	$gender[$i],
	$Cno[$i],
	$noVerif[$i],
	$userApprove[$i],
	$dob[$i],
	$mail[$i],
	$doj[$i],
	$gmcNo[$i],
	$status[$i],
	$iosMB[$i],
	$androidMB[$i],
	$webMB[$i],
	$colleages[$i],
	$followr[$i],
	$followng[$i],
	$address[$i],
	$city[$i],
	$county[$i],
	$country[$i],
	$proff[$i],
	$interest[$i],
	$speciality[$i],
	$prfImg[$i],
	$verifImg[$i],
	$source[$i],
	);
$this->CSV->addRow($userList);
	       $sno++;
	       // pr($userList);
	   }
// exit();
	   $filename='UserList '. date("d/m/Y");
	 echo  $this->CSV->render($filename);
 ?>