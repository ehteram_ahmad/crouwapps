<?php
    echo $this->Html->script(array('Subadmin.ckeditor/ckeditor.js'));   
    $email = !empty($userData['User']['email']) ? $userData['User']['email']:'Not Available';
    $source=!empty($userData['User']['registration_source'])? $userData['User']['registration_source']:'Not Available';
      if (stripos($source, "MB") !== false) {
          $type="medicbleep_";
      }
      else{
          $type="";
      }
?>
<!-- BEGIN CONTENT -->
<?php echo $this->Html->css(array('Subadmin.toastr')); ?>
<div id="content" class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Send Mail</h1>
            </div>
            <!-- END PAGE TITLE -->
            <!-- BEGIN PAGE TOOLBAR -->
            <!--<div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                    <i class="icon-calendar"></i>&nbsp;
                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>-->
            <!-- END PAGE TOOLBAR -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
          <li>
              <a href="<?php echo BASE_URL . 'subadmin/SubadminHome/dashboard'; ?>">Home</a>
              <i class="fa fa-circle"></i>
          </li>
          <li>
              <a href="<?php echo BASE_URL . 'subadmin/SubadminUser/userLists'; ?>">Users</a>
              <i class="fa fa-circle"></i>
          </li>
          <li>
              <a href="<?php echo BASE_URL . 'subadmin/SubadminUser/userView/'.$userData['User']['id']; ?>">User Details</a>
              <i class="fa fa-circle"></i>
          </li>
          <li>
              <span class="active">Send Mails</span>
          </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box blue2">
                    <div class="portlet-title">
                        <div class="caption">
                            Send Mail </div>
                        <div class="tools">
                            
                            <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>-->
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                    <?php echo $this->Form->create('mail'); ?>
                    <div class="modal-body">
                        <div class="form-group marBottom20 leftFloat width100full">
                        <label class="control-label">Select Subject</label>
                        <?php 
                        if(!empty($subjects)){
                  foreach ($subjects as $value) {
                  $options1[$value['AdminEmailCategorie']['id']]=$value['AdminEmailCategorie']['name'];
                  }}else{
                    $options1=array();
                  }
                  echo $this->Form->select('subject',$options1,array('class'=>'form-control select4me','id'=>'mailSub','empty'=>' --Select Subject-- ','onchange'=>'templatesFetch();')); ?>                          
                      </div>
                      <div class="form-group marBottom20 leftFloat width100full">
                        <label class="control-label">Select Email Template</label>
                          <div id="tempContainer">
                              <?php 
                              $options2="";
                              echo $this->Form->select('template',$options2,array('class'=>'form-control select4me','id'=>'mailTemplate','empty'=>' --Select Email Template-- ','onchange'=>'showTemplate();'));
                              ?>
                          </div>     
                      </div>
                        <div class="form-group">
                        <label class="control-label">Enter Additional Comments (Optional)</label>
                          <?php echo $this->Form->textarea('comments',array('class'=>'ckeditor','id'=>'extraComment','rows'=>'6','placeholder'=>'Comments','value'=>'')); ?>
                      </div>
                    </div>
                    <div class="modal-footer textCenter">
                        <button id="sendEmailThanks" type="button" class="btn green2">Send Email</button>
                        <a id="prevShow" data-toggle="modal" href="#previewMail" class="hidden"></a>
                        <button type="button" onclick="prviewMail('<?php echo $email."','".$source; ?>');" class="btn default">Preview Email</button>
                        <button type="button" onclick="javascript:window.location.href='<?php echo BASE_URL . 'subadmin/SubadminUser/userView/'.$userData['User']['id']; ?>'; return false;" class="btn default pull-right"> Cancel </button>
                    </div>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
                
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<div id="previewMail" class="modal fade" tabindex="-1" data-replace="true">
<div class="modal-dialog">
 <div class="modal-content"> 
   <div class="modal-header"> 
     <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
     <h4 class="modal-title">PREVIEW MAIL</h4>
   </div>
   <div class="modal-body">
     <div class="row">
         <div class="col-md-12">
           <div class="form-group">
               <label class="control-label col-md-3 bold">To:</label>
                   <div class="col-md-9">
                       <p class="form-control-static bold"> <?php echo $email; ?> </p>
                     </div>
              </div>
         </div>
         <!--/span-->
         <div class="col-md-12">
           <div class="form-group">
               <label class="control-label col-md-3 bold">Subject:</label>
                   <div class="col-md-9">
                       <p class="form-control-static" id="mailTempPrev"> </p>
                     </div>
              </div>
         </div>
         <!--/span-->
     </div>
     <div class="form-group">
       <label class="control-label col-md-12 bold" id="mailDescPrev">Description:</label>
         <div class="clearfix"></div>
         <div class="col-md-12">
           <?php 
           echo $this->element($type.'email_template_header'); ?>
           <div id="con"></div>
          <?php echo $this->element($type.'email_template_footer');
            ?><br>
         </div>
     </div>
   </div>
   <div class="modal-footer textCenter"><br>
     <button id="sendEmailCloseBtn" type="button" class="btn green2">Send Email</button>
     <button type="button" data-dismiss="modal" class="btn default">Back</button>
   </div>
</div>
</div>
</div>
<?php echo $this->Html->script(array('Subadmin.toastr','Subadmin.ui-toastr',)); ?>
<!-- END CONTENT -->
<!-- Form validation[START] -->
<script type="text/javascript">
function templatesFetch(){
val=$('#mailSub').val();
$.ajax({
         url: '<?php echo BASE_URL; ?>subadmin/SubadminUser/templateSelect',
         type: "POST",
         data: {'id': val,'type':"none"},
         success: function(data) { 
            $('#tempContainer').html(data);
        }
    });
}

function showTemplate(){
val=$('#mailTemplate').val();
  $.ajax({
           url: '<?php echo BASE_URL; ?>subadmin/SubadminUser/templateSelect',
           type: "POST",
           data: {'contentId': val,'type':"comment"},
           success: function(data) { 
              CKEDITOR.instances.extraComment.setData(data);
          }
      });
}
function prviewMail(mail,type){
  if($('#mailSub').val() == ""){
    alert("Please select Subject.");
    return false;
  }
  if($('#mailTemplate').val() == ""){
    alert("Please select Email Template.");
    return false;
  }
val=$('#mailTemplate').val();
comments=CKEDITOR.instances.extraComment.getData();
$.ajax({
         url: '<?php echo BASE_URL; ?>subadmin/SubadminUser/templatePreview',
         type: "POST",
         data: {'id': val,'type':"send"},
         success: function(data) { 
            $('#mailTempPrev').html(data);
            $('#con').html(comments);
            $('#prevShow').click();
        }
    });
}
toastr.options = {
  "closeButton": false,
  "debug": false,
  "positionClass": "toast-top-center2",
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "3000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
$('#sendEmailThanks').click(function () {
  
  id='<?php echo $userData ['User']['id']; ?>';
  email='<?php echo $email; ?>';
  source='<?php echo $source; ?>';
  comments=CKEDITOR.instances.extraComment.getData();

  if($('#mailSub').val() == ""){
    alert("Please select Subject.");
    return false;
  }
  if($('#mailTemplate').val() == ""){
    alert("Please select Email Template.");
    return false;
  }
  if(comments == ""){
    alert("Please enter the content.");
    CKEDITOR.instances.extraComment.focus();
    return false;
  }
  $.ajax({
           url: BASE_URL+'subadmin/SubadminUser/adminToUserMail',
           type: "POST",
           data: {'userId':id,'subject':$('#mailSub').val(),'template':$('#mailTemplate').val(),'source':source,'content':comments,'email':email},
           success: function(data) { 
                if(data=="Mail Sent"){
              toastr.success("Email has been sent succesfully.", "Success!");
              setTimeout(function(){
                $('#previewMail').modal('hide');
              }, 2500);
              window.location.href= BASE_URL +'/subadmin/SubadminUser/userView/'+id;
            }
          }
      });
});
$('#sendEmailCloseBtn').click(function(){
    $('#sendEmailThanks').click();
});

    $("#menu>ul>li").eq(1).addClass('active open');
</script>