<?php
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
    .calCustom {
        font-size: 1.4em;
        top:8px;
        right:14px;
    }
    #content td{
        word-break: break-all;
    }
</style>
<div class="right_col" role="main">
<div class="x_panel">
<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>User Search</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <!--<div class="page-toolbar">
                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green2" data-placement="top" data-original-title="Change dashboard date range">
                        <i class="icon-calendar"></i>&nbsp;
                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>-->
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="<?php echo BASE_URL . 'admin/home/dashboard';?>">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo BASE_URL . 'subadmin/SubadminUser/incompleteUserLists';?>">Users</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Incomplete User List</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->

                    <!-- <div class="portlet box blue2 ">
                        <div class="portlet-title">
                            <div class="caption">
                                 User Search </div>
                            <div class="tools">
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="form-horizontal form-label-left" data-parsley-validate="" id="formSearch" novalidate=""  onsubmit="return searchUser();">
                                <div class="form-body">
                                    
                                    
                                    <div class="form-group">
                                        
                                        <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" placeholder="First Name" name="textFirstName" id="textFirstName"> </div>
                                            <div class="col-md-6">
                                            <input type="text" class="form-control input-sm" placeholder="Last Name" name="textLastName" id="textLastName"> </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="email" class="form-control input-sm" placeholder="Email" name="textEmail" id="textEmail"> </div>
                                        <div class="col-md-6">
                                            <select name="specilities[]"  multiple="" class="form-control select2 spec select2-hidden-accessible" id="multi-prepend" tabindex="-1" aria-hidden="true">
                                        
                                        <optgroup label="">
                                            <?php 
                                                foreach($interestLists['interestlist'] as $interest){
                                                    echo '<option value="'.$interest['specilities']['id'].'" > '.$interest['specilities']['name'].'</option>';}
                                            ?>
                                        </optgroup>
                                    </select>
                                        </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <input placeholder="Registration - From Date" class="form-control datePick" id="dateFrom">
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                    <div class="col-md-6">
                                        <input placeholder="Registration - To Date" class="form-control datePick" id="dateTo">
                                        <i class="fa fa-calendar calCustom form-control-feedback"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <?php 
                                                foreach ($country as  $value) {
                                                $countries[$value['countries']['id']]=$value['countries']['country_name'];
                                                }
                                        echo $this->Form->select('country',$countries,array('class'=>'form-control','empty'=>'Choose Country','id'=>'countrySearch')); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php 
                                                foreach ($professions as  $value) {
                                                $profession[$value['professions']['id']]=$value['professions']['profession_type'];
                                                }
                                        echo $this->Form->select('profession',$profession,array('class'=>'form-control','empty'=>'Choose Profession','id'=>'profession')); ?>
                                    </div>
                                </div>
                             </div>
                                <div class="form-actions center-block txt-center">
                                    <button type="submit" class="btn green2" name="btn_search" value="Search" >Search</button>
                                    <button onclick="resetVal();" type="reset" class="btn default">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div> -->

                    <!-- END SAMPLE FORM PORTLET-->
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    
                    <!-- END SAMPLE TABLE PORTLET-->
                
                    <div class="portlet box blue2">
                        <div class="portlet-title">
                            <div class="caption">
                                User List </div>
                            <div class="tools">
                                <!--<a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="remove"> </a>
                                <a href="javascript:;" class="reload"> </a>-->
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body flip-scroll">
                        <div class="mainAction marBottom10">
                        <div class="leftFloat">
                         <div class="actions">
                                 <div class="btn-group">
                                  <select class="btn btn-md green2" aria-expanded="false" id = "selectSource" name="selectSource" onchange="return searchUser();" >
                                          <option value="0" > --Select Source-- </option>
                                          <option value="1" >OCR</option>
                                          <option value="2" >MB</option>
                                      </select>
                                 </div>
                                <div class="btn-group">
                                    <select class="btn btn-md green2" aria-expanded="false" id = "selectPlatform" name="selectPlatform" onchange="return searchUser();" >
                                            <option value="0" > --Select Platform-- </option>
                                            <option value="1" >Android</option>
                                            <option value="2" >iOS</option>
                                            <option value="3" >Web</option>
                                        </select>
                                   </div> 
                                </div>
                            </div>

                            <div class="rightFloat">
                            <div class="actions marRight10">
                                <!--<div class="btn-group">
                                    <a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-plus"></i> Add New User
                                    </a>
                                </div>-->
                            </div>
                            
                            </div>
                            </div>
                            
                            <!-- </div> -->
                            <div class="clearfix"></div>
                            <div id="content" class="paddingSide15">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
                                                <th> Platform </th>
                                                <th > OS version </th>
                                                <th > Source </th>
                                                <th id="Date" class="DESC" onclick="sort('Date',<?php echo $limit; ?>);"><a> Date </a></th>
                                            </tr>
                                        </thead>
                                    <input type="text" class="hidden" value="<?php echo $limit; ?>" id="perPg" name="">
                                        <tbody>
                                            <?php 
                                        $count=count($userData);
                                        if( $count > 0){
                                        for ($i=0; $i<$count ; $i++){
                                        ?>
                                    <tr class="even pointer" id="user<?php echo $userData[$i]['TempRegistration']['id']; ?>">
                                        <td style="width:38%;"><?php echo $userData[$i]['TempRegistration']['email'];?></td>
                                        <td style="width:15%;" class="text"><?php echo $userData[$i]['TempRegistration']['device_type'];?> </td>
                                        <td style="width:15%;"><?php echo $userData[$i] ['TempRegistration']['version'];?> </td>
                                        <td style="width:10%;"><?php echo $userData[$i] ['TempRegistration']['registration_source'];?> </td>
                                        <td style="width:27%;">
                                        <?php 
                                         $datetime = explode(" ",$userData[$i]['TempRegistration']['created']);
                                         $date = $datetime[0];
                                         $time = $datetime[1];
                                         echo date('d-m-Y',strtotime($date))." / ".date ('h:i:s A',strtotime($time));
                                         ?> </td>
                                        
                                    </tr>
                                     <?php } }else{?> 

                                      <tr>
                                            <td colspan="6" align="center"><font color="red">No Record(s) Found!</font></td>
                                      </tr> 
                                      <?php } ?> 
                                        </tbody>
                                    </table>
                                    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
                                    <div class="actions marBottom10 paddingSide15">
                                    <?php 
                                                if($count > 0){
                                                     echo $this->element('pagination');
                                                }
                                            ?>
                                </div>
                         </div>   
                    </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    </div>
</div>
</div>
<!-- END CONTENT -->
     <?php
        echo $this->Js->writeBuffer();
     ?> 
<script>
$(document).ready(function() {
    $(".datePick").datepicker({
    dateFormat: 'yy-mm-dd'
});
 
});
function sort(val,limit){
    $(".loader").fadeIn("fast");
        var a=$('#'+val).attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='DE';
        }
        else{
            var order='A';
        }
    $.ajax({
                 url: '<?php echo BASE_URL; ?>subadmin/SubadminUser/incompleteUserFilter',
                 type: "POST",
                 data: {'sort':val,'order':order, 'source': $('#selectSource').val(),'platform':$('#selectPlatform').val(),'limit':limit},
                 success: function(data) { 
                    $('#content').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                    $('#'+val).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    else{
                    $('#'+val).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
      return false; 
    }
    function searchUser(){
        $(".loader").fadeIn("fast");
         $.ajax({
                 url: '<?php echo BASE_URL; ?>subadmin/SubadminUser/incompleteUserFilter',
                 type: "POST",
                 data: {'source': $('#selectSource').val(),'platform':$('#selectPlatform').val()},
                 success: function(data) { 
                    $('#content').html(data);
                    $(".loader").fadeOut("fast");
                }
            });
      return false;  
    }
    
    function xyz(){
    var source = $('#selectSource').val();
    var platform =$('#selectPlatform').val();
    window.open('<?php echo BASE_URL; ?>subadmin/SubadminUser/incompleteUserExport/?source='+source+'&platform='+platform, '_blank');
    }

     function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
      var goVal=$("#chngPage").val();
      if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
        $(".loader").fadeIn("fast");
        $.ajax({
                  url:'<?php echo BASE_URL; ?>subadmin/SubadminUser/incompleteUserFilter',
                  type: "POST",
                  data: {'source': $('#selectSource').val(),'platform':$('#selectPlatform').val(),'j':goVal,'limit':limit,'sort':a,'order':order},
                  success: function(data) { 
                      $('#content').html(data);
                        $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                        if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                        }
                        else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                        }
                        $(".loader").fadeOut("fast");
                  }
              });
      }
      else{
        // alert("more");
      }
      
     }
     function chngCount(val){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
      $.ajax({
                  url:'<?php echo BASE_URL; ?>subadmin/SubadminUser/incompleteUserFilter',
                  type: "POST",
                  data: {'limit':val,'sort':a,'order':order,'source': $('#selectSource').val(),'platform':$('#selectPlatform').val()},
                  success: function(data) { 
                      $('#content').html(data);
                      $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                      if(a==b){
                      $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                      }
                      else{
                      $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                      }
                      $(".loader").fadeOut("fast");
                  }
              });
     }

    function abc(val,limit){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
      $.ajax({
               url:'<?php echo BASE_URL; ?>subadmin/SubadminUser/incompleteUserFilter',
               type: "POST",
               data: {'j':val,'limit':limit,'sort':a,'order':order,'source': $('#selectSource').val(),'platform':$('#selectPlatform').val()},
               success: function(data) { 
                    $('#content').html(data);
                   $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                   if(a==b){
                   $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                   }
                   else{
                   $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                   }
                   $(".loader").fadeOut("fast");
               }
           });
    }
$("#menu>ul>li").eq(1).addClass('active open');
$(".sub-menu>li").eq(3).addClass('active open');
</script> 
