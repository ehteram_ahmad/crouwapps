
<!-- Modal -->
  <div class="modal fade updateProfile" id="updateProfile" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onclick="closeModal();">&times;</button>
          <h4 class="modal-title">Registration Number/GMC Update</h4>
        </div>
        <div class="modal-body">
            <input type="hidden" name="hiddenUserId" id="hiddenUserId" value="<?php echo isset($userData['UserProfile']['user_id']) ? $userData['UserProfile']['user_id'] : '' ;?>">
          <table border="0">
            <tr>
                <td width="100%" colspan="3">
                  <?php $regImage =  !empty($userData['UserProfile']['registration_image']) ? AMAZON_PATH . $userData['User']['id']. '/certificate/' . $userData['UserProfile']['registration_image']:''; 
                  if (file_exists($regImage)  ){
                ?>
                  <img src="<?php echo $regImage; ?>" width="100px;" height="100px;">
                <?php 
                  } else{
                    echo $this->Html->image('profilePicNotFound.png', array("width"=> "100px", "height"=>"100px"));
                  }

                ?>
                </td>
             </tr>
             <tr>
                <td width="100%" colspan="3">&nbsp;</td>
             </tr>
            <tr>
                <td width="50%"><strong>Registration Number/GMC</strong></td>
                <td width="5%">:</td>
                <td width="45%"><input type="text" name="textGmcNumber" value="<?php echo isset($userData['UserProfile']['gmcnumber']) ? $userData['UserProfile']['gmcnumber'] : '' ;?>" class="form-control col-md-7 col-xs-12" required="required" id="textGmcNumber" data-parsley-id="0573" maxlength="15"></td>
              </tr>
              <tr>
                <td width="100%" colspan="3">&nbsp;</td>
             </tr>
              <tr>
                <td width="100%" colspan="3" align="right">
                    <input type="submit" name="btn_search" value="Update" class="btn btn-success" onclick="return updateUserProfile();" />
                </td>
                </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
function updateUserProfile(){  
     $.ajax({
             url: '<?php echo BASE_URL; ?>subadmin/SubadminUser/editUserProfile',
             type: "POST",
             data: { 'textGmcNumber': $('#textGmcNumber').val(), "userId": $('#hiddenUserId').val() },
             success: function(data) { 
                alert(data);
                $("#modalBox").html(); 
                $(".modal").hide();
            }
        });
  return false;  
}
</script> 

