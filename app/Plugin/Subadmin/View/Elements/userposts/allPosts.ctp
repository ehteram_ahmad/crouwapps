<?php
// pr($condition);
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
         'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
    
?>
<table class="table table-bordered table-striped table-condensed flip-content">
        <thead class="flip-content">
            <tr>
                <!-- <th>&nbsp;  </th> -->
                <th id="title" class="DESC" onclick="sort('title',<?php echo $limit; ?>);"><a> Post Name </a></th>
                <th> Interests </th>
                <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> Posted By </a></th>
                <th id="date" class="DESC" onclick="sort('date',<?php echo $limit; ?>);"><a> Posted On </a></th>
                <th> Counts </th>
                <th> Status </th>
                <th> Action </th>
            </tr>
        </thead>
         <tbody>
<?php 

if(count($userPostData) > 0){
    foreach( $userPostData as $key => $value){ 
?>
<tr id="<?php echo "beat".$value ['UserPost']['id']; ?>">
    <!-- <td style="width:1%;"> <input type="checkbox" value="<?php echo $value['UserPost']['spam_confirmed']; ?>" id="<?php echo $value['UserPost']['id']; ?>" > </td> -->
    <td style="width:29%;"><?php echo $value['UserPost']['title']; ?></td>
    <td  style="width:28%;"><?php echo $value['speciality_list']; ?></td>
    <td style="width:12%;"><?php echo $value['UserProfile']['first_name']." ".$value['UserProfile']['last_name']; ?></td>
    <td style="width:15%;"><?php $datetime = explode(" ",$value['UserPost']['post_date']);
        $date = $datetime[0];
        $time = $datetime[1];
        echo date('d-m-Y',strtotime($date))."  ".date ('G:i:s',strtotime($time));
     ?></td>
    <td  style="width:5%;"class="txtCenter width80px"> 
    <a class="iconCounts tooltips" data-original-title="Up Beats">
        <i class="demo-icon icon2-up"></i>
        <span class="badge badge-success2"><?php echo count($value['beat_details']['upbeat']); ?></span>
    </a>
    
    <a class="iconCounts tooltips" data-original-title="Down Beats">
        <i class="demo-icon icon2-down"></i>
        <span class="badge badge-success2"><?php echo count($value['beat_details']['downbeat']); ?></span>
    </a> 
    
    <a class="iconCounts tooltips" data-original-title="Comments">
        <i class="fa fa-comment-o"></i>
        <span class="badge badge-success2"><?php echo count($value['UserPostComment']); ?></span>
    </a>
    
    <a class="iconCounts tooltips" data-original-title="Shares">
        <i class="fa fa-share-alt"></i>
        <span class="badge badge-success2"><?php
        if($value['share_details']=="0"){
            echo "0";
        }
            else{
         echo count($value['share_details']); }
         ?></span>
    </a>
    
    <a class="iconCounts tooltips" data-original-title="Views">
        <i class="fa fa-eye"></i>
        <span class="badge badge-success2"><?php echo count($value['BeatView']); ?></span>
    </a>
    </td>
    
    <td style="width:5%;" class="beatBtnBottom4">
            <?php 
             if($value ['UserPost']['status'] == 1){
            ?>
            <button type="button" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
            <?php }else{ ?>
            <button type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
            <?php } ?>
            <span id="flagButton<?php echo $value ['UserPost']['id']; ?>">
                <?php 
                 if($value ['UserPost']['spam_confirmed'] == 1){
                ?>
                
                    <button data-original-title="Flaged" class="btn btn-circle btn-icon-only btn-info3 delBt tooltips"><i class="fa fa-flag"></i></button>
                </span>
                <?php }else{ ?>
                    <button type="button" data-original-title="Unflaged" class="btn btn-circle btn-icon-only btn-default2 tooltips"><i class="fa fa-flag"></i></button>
                    </span>
                   
                <?php } ?>
       
       <span id="featuredButton<?php echo $value ['UserPost']['id']; ?>">
                <?php 
                 if($value ['UserPost']['top_beat'] == 1){
                ?>
                
                    <button data-original-title="Featured" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-star"></i></button>
                </span>
                <?php }else{ ?>
                    <button type="button" data-original-title="Unfeatured" class="btn btn-circle btn-icon-only btn-default2 tooltips"><i class="fa fa-star"></i></button>
                    </span>
                   
                <?php }
                        if($value ['mail'] > 0)
                         {
                     ?>
                    <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
                    <?php }else{ ?>
                    <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="No Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
                    <?php } ?>
        
    </td>
    <td style="width:5%;" class="beatBtnBottom4">
    <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Details" type="button" onclick="javascript:window.location.href='userPostDetails/<?php echo $value ['UserPost']['id'];?>'"><i class="fa fa-eye"></i></button>
        <!-- <div class="radialMenu menuButton3">
        <nav class="circular-menu">
          <div class="circle">
           <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Details" type="button" onclick="javascript:window.location.href='userPostDetails/<?php echo $value ['UserPost']['id'];?>'"><i class="fa fa-eye"></i></button>
           <button type="button" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Edit" type="button" onclick="javascript:window.location.href='editBeat/<?php echo $value ['UserPost']['id'];?>'"><i class="fa fa-pencil-square-o"></i></button>
           <span id="statusButton<?php echo $value ['UserPost']['id']; ?>">
               <?php 
                if($value ['UserPost']['status'] == 1){
               ?>
               <button type="button" onclick="updatePostStatus(<?php echo $value ['UserPost']['id']; ?>, 'Active');" data-original-title="Mark Inactive" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
               <?php }else{ ?>
               <button onclick="updatePostStatus(<?php echo $value ['UserPost']['id']; ?>, 'Inactive');" type="button" data-original-title="Mark Active" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i style="color: black;" class="fa fa-check"></i></button>
               <?php } ?>
           </span>
           <?php 
              if($value ['UserPost']['top_beat'] == 1){
             ?>
                 <button onclick="unfeature(<?php echo $value ['UserPost']['id']; ?>); " class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Unfeature" type="button"><i class="fa fa-star"></i></button>
             </span>
             <?php }else {
                 if($value ['UserPost']['status'] !=1 ){ ?>
                 <button onclick="alert('Beat Is Inactive,Cannot Mark Feature.');" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Feature" type="button"><i class="fa fa-star"></i></button>
             <?php }elseif($value['UserPost']['spam_confirmed']==0){ ?>
                 <button type="button" onclick="feature(<?php echo $value ['UserPost']['id'].','.$featuredCount[0]; ?>);" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Feature" type="button"><i class="fa fa-star"></i></button>
             <?php }else{ ?>
                 <button type="button" onclick="alert('Beat Is Flagged,Cannot Mark Feature.');" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Feature" type="button"><i class="fa fa-star"></i></button>
                 <?php }
             }
              if($value ['UserPost']['spam_confirmed'] == 1){
             ?>
                 <button onclick="Unflag(<?php echo $value ['UserPost']['id'];?>);" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Unmark Flag" type="button"><i class="fa fa-flag"></i></button>
                  <?php }elseif ($value ['UserPost']['spam_confirmed'] == 0) {
                     if($value ['UserPost']['status'] !=1 ){ ?>
                     <button onclick="alert('Beat Is Inactive,Cannot Mark Flagg.');" class="btn btn-circle btn-icon-only btn-info2 tooltips flagging" data-original-title="Mark Flag" type="button"><i class="fa fa-flag"></i></button>
                 <?php }else{ ?>
                 <button id="<?php echo $value ['UserPost']['id'];?>" class="btn btn-circle btn-icon-only btn-info2 tooltips flagging" data-toggle="modal" href='#modal-id' data-original-title="Mark Flag" type="button"><i class="fa fa-flag"></i></button>
                 <?php }} ?>
           <button onclick="deleteBeat(<?php echo $value ['UserPost']['id'].','."'".$value ['UserPost']['title']."'"; ?>);" type="button" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
          </div>
          <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
        </nav>
        </div> -->
</td>
</tr>
     <?php } ?>       
            </tbody>

    </table>
    <?php   
        
            echo $this->element('pagination');
       
    }
    else{
    ?>
    </div>
    <tr>
            <td colspan="8" align="center"><font color="red">Record(s) Not  Found!</font></td>
      </tr> 
    <?php } ?>

    <?php echo $this->Js->writeBuffer(); ?>
<script type="text/javascript">
    $('.tooltips').tooltip();
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
</script> 