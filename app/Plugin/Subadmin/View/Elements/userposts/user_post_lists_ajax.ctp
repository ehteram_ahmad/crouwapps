<?php
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
         'update' => '#content',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
    
?>
<table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th>&nbsp;  </th>
                                                <th> Post Name </th>
                                                <th> Interests </th>
                                                <th> Posted By </th>
                                                <th> Posted On </th>
                                                <th> Counts </th>
                                                <th> Status </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php 
                                if(count($userPostData) > 0){
                                    foreach( $userPostData as $key => $value){ 
                                        // pr($value);
                                ?>
                                    <tr>
                                                <td> <input type="checkbox"> </td>
                                                <td><?php echo $value['UserPost']['title']; ?></td>
                                                <td><?php echo $value['speciality_list']; ?></td>
                                                <td><?php echo $value['UserProfile']['first_name']." ".$value['UserProfile']['last_name']; ?></td>
                                                <td><?php echo $value['UserPost']['post_date']; ?></td>
                                                <td class="txtCenter width80px"> 
                                                <a class="iconCounts tooltips" data-original-title="Up Beats">
                                                    <i class="demo-icon icon2-up"></i>
                                                    <span class="badge badge-success2"><?php echo count($value['beat_details']['upbeat']); ?></span>
                                                </a>
                                                
                                                <a class="iconCounts tooltips" data-original-title="Down Beats">
                                                    <i class="demo-icon icon2-down"></i>
                                                    <span class="badge badge-success2"><?php echo count($value['beat_details']['downbeat']); ?></span>
                                                </a> 
                                                
                                                <a class="iconCounts tooltips" data-original-title="Comments">
                                                    <i class="fa fa-comment-o"></i>
                                                    <span class="badge badge-success2"><?php echo count($value['UserPostComment']); ?></span>
                                                </a>
                                                
                                                <a class="iconCounts tooltips" data-original-title="Shares">
                                                    <i class="fa fa-share-alt"></i>
                                                    <span class="badge badge-success2"><?php echo count($value['share_details']); ?></span>
                                                </a>
                                                
                                                <a class="iconCounts tooltips" data-original-title="Views">
                                                    <i class="fa fa-eye"></i>
                                                    <span class="badge badge-success2"><?php echo count($value['BeatView']); ?></span>
                                                </a>
                                                </td>
                                                
                                                <td class="beatBtnBottom4">

                                                    <span id="statusButton<?php echo $value ['UserPost']['id']; ?>">
                                                        <?php 
                                                         if($value ['UserPost']['status'] == 1){
                                                        ?>
                                                        
                                                            <button type="button" onclick="updatePostStatus(<?php echo $value ['UserPost']['id']; ?>, 'Active');" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                                                        </span>
                                                        <?php }else{ ?>
                                                        
                                                            <button onclick="updatePostStatus(<?php echo $value ['UserPost']['id']; ?>, 'Inactive');" type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                                                           
                                                        <?php } ?></span>
                                                        <span id="flagButton<?php echo $value ['UserPost']['id']; ?>">
                                                            <?php 
                                                             if($value ['UserPost']['spam_confirmed'] == 1){
                                                            ?>
                                                            
                                                                <button type="button" onclick="updateStatus(<?php echo $value ['UserPost']['id']; ?>, 'Active');" data-original-title="Flaged" class="btn btn-circle btn-icon-only btn-info3 delBt tooltips"><i class="fa fa-flag"></i></button>
                                                            </span>
                                                            <?php }else{ ?>
                                                            
                                                                <button onclick="updateStatus(<?php echo $value ['UserPost']['id']; ?>, 'Inactive');" type="button" data-original-title="Unflaged" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-flag"></i></button>
                                                               
                                                            <?php } ?></span>
                                                    <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Features" type="button"><i class="fa fa-star"></i></button>
                                                    
                                                </td>
                                                <td class="beatBtnBottom4">
                                                    <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Details" type="button" onclick="javascript:window.location.href='postDetails.html'; return false;"><i class="fa fa-eye"></i></button>
                                                    <button type="button" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i></button>
                                                    <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Feature" type="button"><i class="fa fa-star"></i></button>
                                                    <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Mark Flag" type="button"><i class="fa fa-flag"></i></button>
                                                    <button type="button" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-original-title="Delete"><i class="fa fa-times"></i></button>
                                            </td>
                                            </tr>
                                     <?php } ?>       
                                            </tbody>

                                    </table>
                                    <div class="actions marBottom10">
                                        <div class="btn-group">
                                            <a class="btn btn-sm green2 dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="false"> Group Action
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu bottom-up2">
                                                <li>
                                                    <a href="javascript:;">Active</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Inactive</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Flagged</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Featured</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php   
                                        
                                            echo $this->element('pagination');
                                       
                                    }
                                    else{
                                    ?>
                                    <tr>
                                            <td colspan="6" align="center"><font color="red">Record(s) Not  Found!</font></td>
                                      </tr> 
                                    <?php } ?>

    <?php echo $this->Js->writeBuffer(); ?>
    