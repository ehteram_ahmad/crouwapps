<?php
// debug($this->Paginator->params());
$this->Js->JqueryEngine->jQueryObject = 'jQuery'; 
$this->paginator->options(
    array(
        'update' => '#tab_1_4',
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))));

if(!empty($userPostdata)){
foreach ($userPostdata as $key => $value) {
    if(!empty($value['share_by'])){
   $share_by[]=$value['share_by'];
   $share_time[]=$value['share_time'];
}
else{
    $share_by[]=NULL;
    $share_time[]=NULL;
}
}
$share_by=array_unique($share_by);
$share_time=array_unique($share_time);
}
if(empty($userPostdata[0]['share_id'])){
  $shareCount=0;
}
 ?>
	<div class="widget-news margin-bottom-20 bold">
    	<span class="widget-news-left-elem">S. NO.</span>
        <div class="widget-news-right-body">
            <p class="widget-news-right-body-title width87">
            	User Name
                <span class="widget-news-right-elem"> Time </span>
                
            </p>
        </div>
    </div>
    <?php
    if(!empty($userPostdata[0]['share_id'])){
    $shareCount=count($share_by);

        $totalPage=$shareCount/$perPage;

        if($shareCount>0){
            for ($i=($j*$perPage); $i<(($j+1)*$perPage) ; $i++){
                if(isset($share_by[$i])){
    ?>
    <div class="widget-news margin-bottom-20">
        <span class="widget-news-left-elem"><?php echo $i+1; ?></span>
        <div class="widget-news-right-body">
            <p class="widget-news-right-body-title width50"><?php echo $share_by[$i]; ?>
                <span class="label label-default">
                <?php 
                 $datetime = explode(" ",$share_time[$i]);
                 $date = $datetime[0];
                 $time = $datetime[1];
                 echo date('d-m-Y',strtotime($date))."  ".date ('g:i:s A',strtotime($time));
                 ?>
                </span>
            </p>
        </div>
    </div>
     <?php } }}}
        else{ ?>
        <p align="center"><font color="red">No Record(s)!</font><p>
           <?php }
     ?>
    <div class="clearfix"></div>
    <div class="row paginationTopBorder">
        <?php 
        if($shareCount>0){
             echo $this->element('advancePagination');
        }
         ?>
   </div>
   <script type="text/javascript">
 var id='<?php echo $id; ?>';
 var perPage='<?php echo $perPage; ?>';
 var totalPage='<?php echo ceil($totalPage); ?>';
     function abc(val,perPage){
     $.ajax({
              url:'<?php echo BASE_URL; ?>subadmin/SubadminUserPost/shares',
              type: "POST",
              data: {'postId':id,'j':val,'perPage':perPage},
              success: function(data) { 
                  // alert(data);
                  $('#tab_1_4').html(data);
              }
          });
 }
 function chngCount(val,id){
  $.ajax({
              url:'<?php echo BASE_URL; ?>subadmin/SubadminUserPost/shares',
              type: "POST",
              data: {'postId':id,'perPage':val},
              success: function(data) { 
                  // alert(data);
                  $('#tab_1_4').html(data);
              }
          });
 }
 function chngPage(val) {
  var goVal=$("#chngPage").val();
  // var chVal=0+goVal;
  if(parseInt(goVal) <= Math.ceil(totalPage) && Math.ceil(goVal)>0){
    $.ajax({
              url:'<?php echo BASE_URL; ?>subadmin/SubadminUserPost/shares',
              type: "POST",
              data: {'postId':id,'j':goVal,'perPage':perPage,'totalPage':totalPage},
              success: function(data) { 
                  // alert(data);
                  $('#tab_1_4').html(data);
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 </script>