<?php
// debug($this->Paginator->params());
$this->Js->JqueryEngine->jQueryObject = 'jQuery'; 
$this->paginator->options(
    array(
        'update' => '#tab_1_7',
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))));
?>
	<div class="widget-news margin-bottom-20 bold">
    	<span class="widget-news-left-elem">S. NO.</span>
        <div class="widget-news-right-body">
            <p class="widget-news-right-body-title">
            	<span class="widget-news-left-elem width20">User Name</span>
                <span class="widget-news-left-elem width160px left"> Time </span>
                <span class="widget-news-right-elem">Reasons</span>
            </p>
        </div>
    </div>
    <style type="text/css">
      .form .widget-news .widget-news-right-elem {
          width: 50%;
      }
    </style>
    <?php
    if(empty($spam)){
        $spam=NULL;
        $spamUser=NULL;
    }
    $userSpamCount=count($spam);
        $totalPage=$userSpamCount/$perPage;

        if($userSpamCount>0){
            for ($i=($j*$perPage); $i<(($j+1)*$perPage) ; $i++){
                if(isset($spam[$i]['created'])){
                 ?>
                   <div class="widget-news margin-bottom-20">
                    <span class="widget-news-left-elem"><?php echo $i+1; ?></span>
                       <div class="widget-news-right-body">
                           <p class="widget-news-right-body-title">
                            <span class="widget-news-left-elem width20"><?php echo $spamUser[$i]['UserProfile']['first_name']." ".$spamUser[$i]['UserProfile']['last_name']; ?></span>
                               <span class="widget-news-left-elem label width160px left">
                                <?php 
                                 $datetime = explode(" ",$spam[$i]['created']);
                                 $date = $datetime[0];
                                 $time = $datetime[1];
                                 echo date('d-m-Y',strtotime($date))."  ".date ('g:i:s A',strtotime($time));
                                 ?> 
                               </span>
                               <span class="widget-news-right-elem">
                            <?php
                            echo $spam[$i]['content'];
                            ?>
                               </span>
                           </p>
                       </div>
                   </div>
               <?php } }}
        else{ ?>
        <p align="center"><font color="red">No Record(s)!</font><p>
           <?php }
     ?>
   <div class="clearfix"></div>
    <div class="row paginationTopBorder">
        <?php 
        if($userSpamCount>0){
            echo $this->element('advancePagination'); 
        }
         ?>
   </div>
   <script type="text/javascript">
 var id='<?php echo $id; ?>';
 var perPage='<?php echo $perPage; ?>';
 var totalPage='<?php echo ceil($totalPage); ?>';
     function abc(val,perPage){
     $.ajax({
              url:'<?php echo BASE_URL; ?>subadmin/SubadminUserPost/flagged',
              type: "POST",
              data: {'postId':id,'j':val,'perPage':perPage},
              success: function(data) { 
                  // alert(data);
                  $('#tab_1_7').html(data);
              }
          });
 }
 function chngCount(val,id){
  $.ajax({
              url:'<?php echo BASE_URL; ?>subadmin/SubadminUserPost/flagged',
              type: "POST",
              data: {'postId':id,'perPage':val},
              success: function(data) { 
                  // alert(data);
                  $('#tab_1_7').html(data);
              }
          });
 }
function chngPage(val) {
  var goVal=$("#chngPage").val();
  // var chVal=0+goVal;
  if(parseInt(goVal) <= Math.ceil(totalPage) && Math.ceil(goVal)>0){
    $.ajax({
              url:'<?php echo BASE_URL; ?>subadmin/SubadminUserPost/flagged',
              type: "POST",
              data: {'postId':id,'j':goVal,'perPage':perPage,'totalPage':totalPage},
              success: function(data) { 
                  // alert(data);
                  $('#tab_1_7').html(data);
              }
          });
  }
  else{
    // alert("more");
  }
  
 }
 </script>