<table class="table table-bordered table-striped table-condensed flip-content">
    <thead class="flip-content">
        <tr>
            <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> Title </a></th>
            <th id="created" class="DESC txtCenter" onclick="sort('created',<?php echo $limit; ?>);"><a> Created / Modified </a></th>
            <th class="txtCenter width90px"> Status </th>
            <th class="txtCenter width140px"> Action </th>
        </tr>
    </thead>
    <tbody>
    <?php 
    if(count($Contents) > 0){
    foreach( $Contents as $content){ 
        ?>
        <tr>
            <td style="width: 40%;"> <?php echo ucfirst($content['Content']['content_title']);?> </td>
            <td style="width: 40%;" class="txtCenter"> 
             <?php echo $content['Content']['added_date']; ?> 
            </td>
            <td style="width: 10%;" class="txtCenter" >
                <?php 
                 if($content['Content']['content_status'] == 1){
                ?>
                    <button type="button" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                <?php }else{ ?>
                    <button type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                <?php } ?>
            </td>
            <td style="width: 10%;" class="beatBtnBottom4 txtCenter">
              <div class="radialMenu menuButton3">
              <nav class="circular-menu">
                <div class="circle">
                      <span id="statusButton<?php echo $content['Content']['content_id']; ?>">
                    <?php 
                       if($content['Content']['content_status'] == 1){
                      ?>
                      
                          <button type="button" onclick="changeContentStatus(<?php echo $content['Content']['content_id']; ?>, 'Active');" data-original-title="Inactivate" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                      </span>
                      <?php }else{ ?>
                          <button onclick="changeContentStatus(<?php echo $content['Content']['content_id']; ?>, 'Inactive');" type="button" data-original-title="Activate" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i style="color: black;" class="fa fa-check"></i></button>
                         
                      <?php } ?>
                      </span>
                  <button class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="View" onclick="javascript:window.location.href='contentdetail/<?php echo $content['Content']['content_id'];?>'"><i class="fa fa-eye"></i></button>
                  <button type="button" class="btn btn-circle btn-icon-only btn-info2 tooltips" data-original-title="Edit" onclick="javascript:window.location.href='editContents/<?php echo $content['Content']['content_id']; ?>'"><i class="fa fa-pencil-square-o"></i></button>
                  <button type="button" data-original-title="Delete" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-toggle="modal" data-target="#myModal3" onclick="deleteContent(<?php echo $content['Content']['content_id']; ?>);"><i class="fa fa-trash-o"></i></button>
              </div>
                <a href="" class="menu-button fa fa-cogs fa-1x btn-default2"></a>
              </nav>
              </div>
            </td>
        </tr>
        <?php }}else{ ?>
          <tr>
                <td colspan="4" align="center"><font color="red">No Record(s) Found!</font></td>
          </tr> 
        <?php  } ?>
    </tbody>
</table>
<div class="row">
    <div class="col-md-12 col-sm-12">
    <?php echo $this->element('pagination'); ?>
    </div>
</div>
<script>
// Group Action Icons
var items = $(this).find('.circle button');
for(var i = 0, l = items.length; i < l; i++) {
    items[i].style.right = ((18)*i).toFixed(4) + "%";
    items[i].style.top = (50).toFixed(4) + "%";
}
$('.menu-button').click(function() {
    $(this).parent('.circular-menu').toggleClass('open');
    $('#cn-overlay').toggleClass('on-overlay');
    return false;
});
$('.circular-menu .circle').click(function() {
    $(this).parent('.circular-menu').removeClass('open');
    $('#cn-overlay').removeClass('on-overlay');
});
$('.tooltips').tooltip();
</script> 
