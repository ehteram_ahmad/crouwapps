<!-- REQUIREMENTS
$count= total count of data to be shown.
$j= loop starting limit and also the (current page no.-1).
$perPage= no. of records shown.

<script>
 var perPage='<?php echo $perPage; ?>';
 var totalPage='<?php echo ceil($totalPage); ?>';
function abc(val,perPage)          //val=page on which to go
{
  $.ajax({
           url:'',
           type: "POST",
           data: {'j':val,'perPage':perPage},
           success: function(data) { 
               // alert(data);
               $('#content').html(data);
           }
       });
}
function chngCount(val,id)        //val=no. of data to be shown using select
{
  $.ajax({
              url:'',
              type: "POST",
              data: {'perPage':val},
              success: function(data) { 
                  // alert(data);
                  $('content').html(data);
              }
          });
}
function chngPage(val) {
  var goVal=$("#chngPage").val();
  if(parseInt(goVal) <= parseInt(totalPage) && parseInt(goVal)>0){
    $.ajax({
              url:'',
              type: "POST",
              data: {'j':goVal,'perPage':perPage,'totalPage':totalPage},
              success: function(data) { 
                  // alert(data);
                  $('#content').html(data);
              }
          });
  }
  else{
    
  }
  
 }
</script>
-->
<div class="col-md-12">
 <?php
 $totalPage=$count/$perPage;
 $pageNo=$j+1;
 $textValue=($pageNo)." / ".ceil($totalPage);
 $count=array("5"=>"5","10"=>"10","20"=>"20");
 echo $this->Form->input('Data_Count',array('options'=>$count,'type'=>'select','div'=>false,'onchange' =>'chngCount(this.value,'.$id.')','default'=>$perPage));
 
 if($pageNo<2){
  echo ' First ';
  echo '<< ';
 }
 else{
  echo '<a onclick="abc(1,'.$perPage.')"> First </a>';
  echo '<a onclick="abc('.($pageNo-1).','.$perPage.')"><< </a>';
 }
 echo $this->Form->text('a',array('value'=>$textValue,'disabled','div'=>false));
 if($pageNo>=ceil($totalPage)){
  echo ' >>';
  echo ' Last ';
 }
 else{
 echo '<a onclick="abc('.($pageNo+1).','.$perPage.')"> >></a>';
 echo '<a onclick="abc('.ceil($totalPage).','.$perPage.')"> Last </a>';
 }

 echo $this->Form->input('Go_to',array('type'=>'number','value'=>$pageNo,'div'=>false,'min'=>'01','max'=>ceil($totalPage),'id'=>'chngPage'));
 echo $this->Form->button('GO',array('onclick'=>'chngPage()'));

?>
</div>