<div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div id="menu" class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start">
                            <a href="<?php echo BASE_URL . 'subadmin/SubadminHome/dashboard';?>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                                <!--<span class="arrow open"></span>-->
                            </a>
                        </li>
                        <!--<li class="heading">
                            <h3 class="uppercase">Inner Pages</h3>
                        </li>-->
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Users</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL . 'subadmin/SubadminUser/userLists';?>" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">All</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL . 'subadmin/SubadminUser/approvedUserLists';?>" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Approved</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL . 'subadmin/SubadminUser/pendingUserLists';?>" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Pending</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL . 'subadmin/SubadminUser/incompleteUserLists';?>" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Incomplete</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-desktop"></i>
                                <span class="title">Posts</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/SubadminUserPost/userPostLists" class="nav-link ">
                                        <i class="fa fa-desktop"></i>
                                        <span class="title">All</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/SubadminUserPost/featuredPostLists" class="nav-link ">
                                        <i class="fa fa-desktop"></i>
                                        <span class="title">Featured</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/SubadminUserPost/flaggedPostLists" class="nav-link ">
                                        <i class="fa fa-desktop"></i>
                                        <span class="title">Flagged</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                      <!--   <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-users"></i>
                                <span class="title">Specialties</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/SubadminSpecility/allSpecilities" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Specialties Listings</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-clone"></i>
                                <span class="title">Manage Content</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/SubadminContent/listContents" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Content Listings</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-envelope-o"></i>
                                <span class="title">Email Templates</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/Subadminemails/index" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Template Listing</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-commenting-o"></i>
                                <span class="title">Manage Story Points</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/SubadminStoryPoint/manageStoryPoint" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Manage Story Point</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-comments-o"></i>
                                <span class="title">User Feedbacks</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/Subadminfeedback/feedbackList" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">User Feedback Lists</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-mobile"></i>
                                <span class="title">Manage App Versions</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/SubadminAppVersion/manageAppVersion" class="nav-link ">
                                        <i class="fa fa-mobile"></i>
                                        <span class="title">Manage TheOCR App Versions</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="<?php echo BASE_URL;?>subadmin/SubadminAppVersion/manageMbAppVersion" class="nav-link ">
                                        <i class="fa fa-mobile"></i>
                                        <span class="title">Manage MB App Versions</span>
                                    </a>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>