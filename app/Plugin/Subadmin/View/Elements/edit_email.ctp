<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Email Template</h4>
    </div>
    <div class="modal-body">
        <form action="#" class="form-horizontal" role="form">
            <div class="form-group">
                <label class="control-label col-md-2">Template Title</label>
                <div class="col-md-10">
                        <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Template Editor</label>
                <div class="col-md-9 disableFullScreen">
                <?php echo $this->Form->textarea('Emailtemplate.content', array('class'=>'ckeditor','value'=>'','rows'=>'100')); ?>
                </div>
                <div class="col-md-10">
                        <div name="summernote" id="summernote_1"> </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">Status</label>
                <div class="col-md-10">
                        <div class="radio-list">
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked> Active </label>
                        <label class="radio-inline">
                            <input type="radio" name="optionsRadios" id="optionsRadios5" value="option2"> Inactive </label>
                    </div>
               </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn green2" data-dismiss="modal">Add</button>
        <button class="btn default" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
</div>
