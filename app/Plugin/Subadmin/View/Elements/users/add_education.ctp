<?php
if(isset($userData['UserProfile']['userEducation'])){
foreach ($userData['UserProfile']['userEducation'] as $value) {
?>
<div class="row">
  <div class="col-md-2 col-xs-12">
    <div class="profile-userpic leftFloat"> <img src="<?php echo BASE_URL.'subadmin/img/building.png'; ?>" class="img-responsive" alt=""> </div>
  </div>
  <div class="col-md-10 col-xs-12">
    <div class="profile-usertitle">
      <div class="profile-stat-text"> <?php echo $value['InstituteName']['institute_name']; ?> <span> <?php echo $value['EducationDegree']['degree_name']." ( ".$value['EducationDegree']['short_name']." ) "; ?> </span></div>
    </div>
    <div class="profile-stat2 width100full">
      <div class="profile-stat-text"> <?php echo $value['UserInstitution']['from_date']." - ".$value['UserInstitution']['to_date']; ?> </div>
    </div>
  </div>
  <div class="col-md-12 col-xs-12">
    <p class="eduText"><?php echo $value['UserInstitution']['description']; ?></p>
    <p class="lightBordered width100full leftFloat"></p>
  </div>
</div>
<?php }}?>