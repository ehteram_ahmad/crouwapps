
    <table class="table table-bordered table-striped table-condensed flip-content">
        <thead class="flip-content">
            <tr>
                <!-- <th><input type="checkbox" id="selectAll"></th> -->
                <th id="first_name" class="DESC" onclick="sort('first_name',<?php echo $limit; ?>);"><a> First Name </a></th>
                <th id="last_name" class="DESC" onclick="sort('last_name',<?php echo $limit; ?>);"><a> Last Name </a></th>
                <th id="email" class="DESC" onclick="sort('email',<?php echo $limit; ?>);"><a> Email </a></th>
                <th id="country" class="DESC" onclick="sort('country',<?php echo $limit; ?>);"><a> Country </a></th>
                <th id="county" class="DESC" onclick="sort('county',<?php echo $limit; ?>);"><a> County </a></th>
                <th id="profession" class="DESC" onclick="sort('profession',<?php echo $limit; ?>);"><a> Profession </a></th>
                <th id="joining_date" class="DESC" onclick="sort('joining_date',<?php echo $limit; ?>);"><a> Joining Date </a></th>
                <th> Status </th>
                <th> Action </th>
            </tr>
        </thead>
        <tbody>
            <?php 
            // echo "<pre>"; print_r($userData);die;
    if(count($userData) > 0){
    foreach( $userData as $ud){ 
    ?>
    <tr class="even pointer" id="user<?php echo $ud['User']['id']; ?>">
        <td style="width:15%;word-break: break-word;" class=" "><?php echo $ud['UserProfile']['first_name'];?></td>
        <td style="width:15%;word-break: break-word;" class=" "><?php echo $ud['UserProfile']['last_name'];?> </td>
        <td style="width:25%;word-break: break-word;" class=" "><?php echo $ud ['User']['email'];?> </td>
        <td style="width:10%;word-break: break-word;" class=" "><?php echo !empty($ud ['Country']['country_name']) ? $ud ['Country']['country_name'] : 'N/A';?> </td>
        <td style="width:11%;word-break: break-word;" class=" "><?php echo !empty($ud ['UserProfile']['county']) ? $ud ['UserProfile']['county'] : 'N/A';?> </td>
        <td style="width:14%;word-break: break-word;" class=" "><?php echo !empty($ud ['Profession']['profession_type']) ? $ud ['Profession']['profession_type'] : 'N/A';?> </td>
        <td style="width:10%;word-break: break-word;" class=" "><?php echo date('d-m-Y', strtotime($ud ['User']['registration_date']));?></td>
        <td style="width:1%;word-break: break-word;" class="text-center">
            <?php 
            switch($ud['User']['approved']){
                case 0:
                    $approveStatus="Unapproved";
                    break;
                case 1:
                    $approveStatus="Approved";
                    break;
            }
            switch($ud['User']['status']){
                case 0:
                    $emailStatus="Email-Id is Unverified";
                    $status="";
                    break;
                case 1:
                    $emailStatus="Email-Id is Verified";
                    $status="";
                    break;
                case 2:
                    $status="Deleted";
                    break;
            }
            if( $status=="Deleted"){
            }else{
                $status=$emailStatus." and ".$approveStatus;
            }
             if(($ud['User']['status']==1)&&($ud['User']['approved'] == 1)){
            ?>
                <button class="btn btn-circle btn-icon-only btn-info3 tooltips" type="button" data-original-title="<?php echo "Active - ".$status; ?>" ><i class="fa fa-thumbs-up"></i></button>
            </span>
            <?php }else{ ?>
                <button type="button" title="<?php echo "Inactive - ".$status; ?>" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
               
            <?php }
                if($ud['mail'] > 0){
             ?>
            <button class="btn btn-circle btn-icon-only btn-info3 tooltips" data-original-title="Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
            <?php }else{ ?>
            <button class="btn btn-circle btn-icon-only btn-default2 tooltips" data-original-title="No Email Sent" type="button"><i class="fa fa-envelope-o"></i></button>
            <?php } ?>
        </td>
        <td style="width:1%;word-break: break-word;" class="last">
                <?php
                 //if(($ud['User']['status'] !=2)){ ?>
            <!-- <div class="radialMenu menuButton3">
            <nav class="circular-menu">
              <div class="circle">
                <button onclick="userView(<?php //echo $ud ['User']['id']; ?>)" type="button" data-original-title="View" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-eye"></i></button>
                <button data-toggle="modal" onclick="javascript:window.location.href='<?php //echo BASE_URL . 'subadmin/SubadminUser/editUser/' . $ud['User']['id'] ; ?>'; return false;" type="button" data-original-title="Edit" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-pencil-square-o"></i></button>
                <button type="button" id="deleteModalBtn" data-original-title="Delete" value="<?php //echo $ud ['User']['id']; ?>" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips deleteModalBtn" data-toggle="modal" ><i class="fa fa-trash-o"></i></button>
              </div>
              <a  data-original-title="Actions" class="menu-button fa fa-cogs fa-1x btn-default2 tooltips"></a>
            </nav>
         </div> -->
                 <?php //}else{ ?>
                 <button onclick="userView(<?php echo $ud['User']['id']; ?>)" type="button" data-original-title="View" class="btn btn-circle btn-icon-only btn-info2 tooltips"><i class="fa fa-eye"></i></button>
                 <?php //} ?>
        </td>
        
        </tr>
     <?php } }else{?> 

      <tr>
            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
      </tr> 
      <?php } ?> 
        </tbody>
    </table>

    <span class="pull-right">Total Count : <?php echo $tCount; ?></span>
    <div class="actions marBottom10 paddingSide15">
    <?php 
                if(count($userData) > 0){  
                    echo $this->element('pagination');
                }
            ?>
</div>
<script type="text/javascript">
$(".deleteModalBtn").click(function(){
  $("#deleteModal").modal('show');
  $('#delUser').val($(this).val());
});
 $('.tooltips').tooltip();
     // Group Action Icons
     var items = $(this).find('.circle button');
     for(var i = 0, l = items.length; i < l; i++) {
         items[i].style.right = ((18)*i).toFixed(4) + "%";
         items[i].style.top = (50).toFixed(4) + "%";
     }
     $('.menu-button').click(function() {
         $(this).parent('.circular-menu').toggleClass('open');
         $('#cn-overlay').toggleClass('on-overlay');
         return false;
     });
     $('.circular-menu .circle').click(function() {
         $(this).parent('.circular-menu').removeClass('open');
         $('#cn-overlay').removeClass('on-overlay');
     });
</script>