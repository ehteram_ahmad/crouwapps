<table class="table table-bordered table-striped table-condensed flip-content">
        <thead class="flip-content">
            <tr>
                <!-- <th class="width40px">&nbsp;  </th> -->
                <!-- <th class="width60px"> Sr. No. </th> -->
                <th id="name" class="DESC" onclick="sort('name',<?php echo $limit; ?>);"><a> Speciality Name </a></th>
                <th class="txtCenter smallDeviceIconHeight"> Speciality Icon </th>
                <th class="txtCenter width90px"> Status </th>
                <th class="txtCenter width90px"> Actions </th>
            </tr>
        </thead>
        <tbody>
            <?php 
            // pr($specilities);
                if(count($specilities) > 0){
                for ($i=0; $i < count($specilities) ; $i++) { 
             ?>
                <tr>
                   <!-- <td><?php //echo $i+1; ?> </td>-->
                    <td><?php echo $specilities[$i]['Specilities']['name']; ?></td>
                    <td class="txtCenter">
                    <span onclick="viewImg('<?php echo BASE_URL . 'img/specilities/'.$specilities[$i]['Specilities']['img_name']; ?>');">
                        <img id="icon1" src="<?php echo BASE_URL . 'img/specilities/'.$specilities[$i]['Specilities']['img_name']; ?>" alt="No Image To Display" />
                    </span>
                    </td>
                    <td class="txtCenter">
                            <?php 
                             if($specilities[$i]['Specilities']['status'] == 1){
                            ?>
                                <button type="button" data-original-title="Active" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                            <?php }else{ ?>
                            
                                <button type="button" data-original-title="Inactive" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                            <?php } ?>
                    </td>
                    <td class="txtCenter">
                    <span id="statusButton<?php echo $specilities[$i]['Specilities']['id']; ?>">
                           <?php 
                            if($specilities[$i]['Specilities']['status'] == 1){
                           ?>
                           
                               <button type="button" onclick="updateSpecilityStatus(<?php echo $specilities[$i]['Specilities']['id']; ?>, 'Active');" data-original-title="Inactivate" class="btn btn-circle btn-icon-only btn-info3 tooltips"><i class="fa fa-check"></i></button>
                           <?php }else{ ?>
                           
                               <button onclick="updateSpecilityStatus(<?php echo $specilities[$i]['Specilities']['id']; ?>, 'Inactive');" type="button" data-original-title="Activate" class="btn btn-circle btn-icon-only btn-default2 tooltips" aria-describedby="tooltip70701"><i class="fa fa-check"></i></button>
                           <?php } ?>
                       </span>
                        <a class="btn btn-circle btn-icon-only btn-info2 tooltips editModal" href="#todo-members-modal1" data-original-title="Edit" id="<?php echo $specilities[$i]['Specilities']['id']; ?>" data-toggle="modal"><i class="fa fa-pencil-square-o"></i></a>
                        <button onclick="deleteSpec(<?php echo $specilities[$i]['Specilities']['id'].','."'".$specilities[$i]['Specilities']['name']."'"; ?>);" type="button" class="btn btn-circle btn-icon-only btn-info2 delBt tooltips" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
                <?php }}else{ ?>
                    <tr>
                        <td colspan="4" align="center"><font color="red">No Record(s) Found!</font></td>
                      </tr> 
                  <?php  } ?>
            </tbody>
    </table>
<div class="row">

    <div class="col-md-12 col-sm-12">
        <?php echo $this->element('pagination'); ?>
    </div>
</div>
<script type="text/javascript">
     $('.tooltips').tooltip();
</script>