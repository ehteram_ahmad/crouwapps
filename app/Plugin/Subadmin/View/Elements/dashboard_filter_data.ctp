<?php
    $totalUsers = isset($dataList['totalUsers']) ? $dataList['totalUsers'] : 0;
    $pendingUsers = isset($dataList['pendingUsers']) ? $dataList['pendingUsers'] : 0;
    $registeredUsers = isset($dataList['registeredUsers']) ? $dataList['registeredUsers'] : 0;
    $incompleteUsers = !empty($dataList['incompleteUsers']) ? $dataList['incompleteUsers'] : 0;
    $totalBeats = isset($dataList['totalBeats']) ? $dataList['totalBeats'] : 0;
    $reportedBeats = isset($dataList['reportedBeats']) ? $dataList['reportedBeats'] : 0;
?>
<div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue2">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($totalUsers) ? $totalUsers : 0; ?>"><?php echo isset($totalUsers) ? $totalUsers : 0; ?></span>
                                    </div>
                                    <div class="desc"> Total Users </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'subadmin/SubadminUser/userLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($pendingUsers) ? $pendingUsers : 0; ?>"><?php echo isset($pendingUsers) ? $pendingUsers : 0; ?></span>
                                    </div>
                                    <div class="desc"> Pending Approval </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'subadmin/SubadminUser/pendingUserLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat grey-salsa">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($registeredUsers) ? $registeredUsers : 0; ?>"><?php echo isset($registeredUsers) ? $registeredUsers : 0; ?></span> </div>
                                    <div class="desc"> Approved Users </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'subadmin/SubadminUser/approvedUserLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($incompleteUsers) ? $incompleteUsers : 0; ?>"><?php echo isset($incompleteUsers) ? $incompleteUsers : 0; ?></span> </div>
                                    <div class="desc"> Incomplete Users </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'admin/user/incompleteUserLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green2">
                                <div class="visual">
                                    <i class="fa fa-heartbeat"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($totalBeats) ? $totalBeats : 0; ?>"><?php echo isset($totalBeats) ? $totalBeats : 0; ?></span>
                                    </div>
                                    <div class="desc"> Total Beats </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'subadmin/SubadminUserPost/userPostLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat purple">
                                <div class="visual">
                                    <i class="fa fa-heartbeat"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo isset($reportedBeats) ? $reportedBeats : 0; ?>"><?php echo isset($reportedBeats) ? $reportedBeats : 0; ?></span> </div>
                                    <div class="desc"> Reported Beats </div>
                                </div>
                                <a class="more" href="<?php echo BASE_URL . 'subadmin/SubadminUserPost/flaggedPostLists'; ?>"> View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
           <?php 
        echo $this->Html->script(array('Subadmin.app.min'));
        ?>            
                    