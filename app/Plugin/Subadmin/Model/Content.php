<?php 

App::uses('AppModel', 'Model');

class Content extends AppModel {
	public $useTable = 'contents';
	var $primaryKey= 'content_id';
	public $validate = array(
				/*'content_title' =>array(
					'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Page title can not be left blank'
				)),*/
		'content_title' =>array(
        'rule1' => array(
					'rule' => 'isUnique',
					'allowEmpty' => false,
					'on' => 'create',
					'message' => 'Page title should be unique and can not be left blank',
				),
        
    ),
				'content_text' =>array(
					'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Page content can not be left blank'
				)),
		    );
}
?>