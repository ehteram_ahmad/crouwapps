<?php 
App::uses('AppModel', 'Model');

class UserDevice extends AppModel {

	function deviceStatusCount($id){
		$statusData = array();
		$statusData['androidMB']=$this->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("mb","mb-web"),'LOWER(device_type)'=>array('android'))));
		$statusData['iosMB']=$this->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("mb","mb-web"),'LOWER(device_type)'=>array('ios'))));
		$statusData['webMB']=$this->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("mb","mb-web"),'LOWER(device_type)'=>array('web','mbweb','mb-web'))));
		$statusData['androidOCR']=$this->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("ocr","theocr","the-ocr","the_ocr"),'LOWER(device_type)'=>array('android'))));
		$statusData['iosOCR']=$this->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("ocr","theocr","the-ocr","the_ocr"),'LOWER(device_type)'=>array('ios'))));
		$statusData['webOCR']=$this->find('count',array('conditions'=>array('user_id'=>$id,'LOWER(application_type)'=>array("ocr","theocr","the-ocr","the_ocr"),'LOWER(device_type)'=>array('web','web-ocr','ocrweb'))));
		return $statusData;

	}

}
?>