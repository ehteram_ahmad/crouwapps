<?php
class SubadminAppController extends AppController {
	public $components = array('Session');
	public $uses = array('User', 'UserPost');
    public function beforeFilter(){
        if($_SERVER['SERVER_NAME'] == 'institution.medicbleep.com'){
            header("Location: https://institution.medicbleep.com/institution");
            exit();
        }
        if( null !== $this->Session->read('userName') && $this->Session->read('userName') !='' ){
            if (in_array($this->Session->read('userName'), array('vivek','akshat'))) {
                $this->redirect(BASE_URL . 'campaign/CampaignHome/index');
              exit;
            }else{
            $this->layout = 'subAdminDefault';
			//** All deleted users 
	         $totalUsers = $this->User->find('count', 
         				array('conditions'=>
         					array('User.status'=> array(0,1)))
         				);
         $this->set('totalUsers', $totalUsers);
         	//** All Approved and active users 
         $registeredUsers = $this->User->find('count', 
         				array('conditions'=>
         					array('User.status'=>array(0,1) , 'User.approved'=> 1 ))
         				);
         $this->set('registeredUsers', $registeredUsers);
         //** All active but unapproved users 
         $pendingUsers = $this->User->find('count', 
         				array('conditions'=>
         					array('User.status'=> array(0,1), 'User.approved'=> 0 ))
         				);
         $this->set('pendingUsers', $pendingUsers);
         //** All Beats
         $totalBeats = $this->UserPost->find('count', 
                array('conditions'=>
                  array('UserPost.status'=>array(0,1)))
                );
         $this->set('totalBeats', $totalBeats);
         //** All Reported Beats
         $reportedBeats = $this->UserPost->find('count', 
                array('conditions'=>
                  array('UserPost.spam_confirmed'=> 1 ))
                );
	         $this->set('reportedBeats', $reportedBeats);
		}}else{
			$this->layout = 'subAdminLogin';
		}
	}
}