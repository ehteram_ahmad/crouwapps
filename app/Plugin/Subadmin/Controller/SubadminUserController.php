<?php
class SubadminUserController extends SubadminAppController {
    //public $uses = array('Subadmin.User', 'User', 'EmailTemplate', 'EmailSmsLog', 'Country');
    public $uses = array('Subadmin.User',
     'Subadmin.UserProfile','EmailTemplate','EmailSmsLog','Country','UserPost','UserColleague','UserFollow','Subadmin.UserInstitution','Subadmin.UserEmployment','Profession','Specilities','Subadmin.UserSpecilities','Subadmin.UserInterest','AdminUserSentMail','AdminActivityLog','CompanyName','Designation','EducationDegree','InstituteName','Subadmin.UserDevice','AdminEmailCategorie','AdminEmailTemplate','TempRegistration');
    public $components = array('RequestHandler','Paginator','Session', 'Common', 'Subadmin.Email', 'Image');
    public $helpers = array('Js','Html','Subadmin.Csv', 'Paginator');

    public $paginate = array(
        'limit' => ADMIN_PAGINATION,
    );

    /*
    On: 26-08-2015
    I/P:
    O/P: 
    Desc:
    */
    public function userLists(){ 
        $this->Session->delete('User.approved'); 
        $this->Session->write('User.approved', "All");
        $params = array();
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1   ORDER BY profession_type ASC');
        $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        $interestLists['interestlist'] = $interestlists; 
        $this->set(array('interestLists'=>$interestLists,'country'=>$country,'professions'=>$profession));
        //** Get User Data
        $conditions = array('User.status'=>array(0,1));
        $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('User.id'),
                'order'=> 'User.registration_date DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        // pr($conditions);
                $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                $this->Paginator->settings = $options;
                $users = $this->Paginator->paginate('User');
        if(isset($users) && !empty($users)){
            foreach($users as $user){
                $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                $userS[]=$user;
            }
        }
        else{
          $userS=array();
        }
        $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        // if($this->request->is("ajax")){
        //     $this->render('/Elements/users/user_lists_ajax');
        // }
    }
    public function get_admin_user_mail($id){
        $mail=array();
        $conditions = array('AdminUserSentMail.user_id'=> $id); 
        $mail=$this->AdminUserSentMail->find('all',array('conditions'=>$conditions));
        return $mail;
    }
    public function allFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
              $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
              $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
              $sort="User.email $order";
            }
            elseif($con=='country'){
              $sort="Country.country_name $order";
            }
            elseif($con=='county'){
              $sort="UserProfile.county $order";
            }
            else{
            $sort="User.registration_date $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
            $sortPost=$this->data;
            if(!isset($sortPost['to']) || $sortPost['to']==""){
              $endDate = date("Y-m-d");
            }
            else{
              $endDate = date('Y-m-d', strtotime($sortPost['to']));
            }
            if(!isset($sortPost['from']) || $sortPost['from']==""){
              $date = array();
            }
            else{
              $startDate = date('Y-m-d', strtotime($sortPost['from']));
              $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
            }
            if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
                $firstName=array();
            }
            else{
                $sortPost['textFirstName']=trim($sortPost['textFirstName']);
                $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textFirstName'].'%'));
            }
            if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
                $lastName=array();
            }
            else{
                $sortPost['textLastName']=trim($sortPost['textLastName']);
                $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower($sortPost['textLastName'].'%'));
            }
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $mail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $mail=array('LOWER(User.email) LIKE'=>strtolower('%'.$sortPost['textEmail'].'%'));
            }
            if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
                $interest=array();
            }
            else{
                $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);
            }
            if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
                $status=array('User.status'=>array(0,1));
            }
            else{
                switch($sortPost['statusVal']){
                    case 0:
                        $status=array('User.status'=>array(0,1));
                        break;
                    case 1:
                        $status=array('User.status'=>1,'User.approved'=>0);
                        break;
                    case 2:
                        $status=array('User.status'=>0,'User.approved'=>1);
                        break;
                    case 3:
                        $status=array('User.status'=>1,'User.approved'=>1);
                        break;
                    case 4:
                        $status=array('User.status'=>0,'User.approved'=>0);
                        break;
                    case 5:
                        $status=array('User.status'=>2);
                        break;

                }
             }
             if(!empty($sortPost['country'])){
                 $country=array('UserProfile.country_id'=>$sortPost['country']);
             }
             else{
                $country=array();
             }
             if(!isset($sortPost['county']) || $sortPost['county']==""){
                 $county=array();
             }
             else{
                 $sortPost['county']=trim($sortPost['county']);
                 $county=array('LOWER(UserProfile.county) LIKE'=>strtolower('%'.$sortPost['county'].'%'));
             }
             if(!empty($sortPost['profession'])){
                 $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
             }
             else{
                $profession=array();
             }
             $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$county,$profession);
             $fields =array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
            $options  = 
            array(
                    'joins'=>array(
                        array(
                          'table' => 'countries',
                          'alias' => 'Country',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.country_id = Country.id')
                      ),
                        array(
                          'table' => 'professions',
                          'alias' => 'Profession',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.profession_id = Profession.id')
                      ),
                        array(
                          'table' => 'user_specilities',
                          'alias' => 'UserSpecility',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserSpecility.user_id')
                      ),
                    ),
                    'fields'=> $fields,
                    'conditions'=> $conditions,
                    'group'=> array('User.id'),
                    'order'=> $sort, 
                    'limit'=> $limit,
                    'page'=>$j
                    );
            // $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                    $this->Paginator->settings = $options;
                    $users = $this->Paginator->paginate('User');
                    if(isset($users) && !empty($users)){
            foreach($users as $user){
                $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                $userS[]=$user;
            }
        }
        else{
          $userS=array();
        }
        $total=$this->request->params;
        $tCount=$total['paging']['User']['count'];
            $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
            $this->render('/Elements/users/user_lists_ajax');

    }

    /*
    On: 27-08-2015
    I/P:
    O/P: 
    Desc:
    */
    public function userListsSearch(){
        if ($this->request->data) {
            $data = $this->request->data;
            $search['firstname'] = $data['textFirstName'];
            $search['lastname'] = $data['textLastName'];
            $search['email'] = $data['textEmail'];
            $search['status'] = $data['selectStatus'];
            if(isset($data['interestlist']) && count($data['interestlist']) > 0){
                $search['interestlist'] = $data['interestlist'];
            }
            $this->Session->write('searchdata', $search);
            $this->redirect(array('action' => 'userLists'));
        }
    }

    /*
    On: 31-08-2015
    I/P:
    O/P:
    Desc: Change user status 
    */
    public function changeUserStatus(){
        $userId = $this->request->data('user_id');
        $statusVal = $this->request->data('statusVal');
        if( trim($statusVal) == 'Active' ){ 
            $chngeStatus = 0;
            //** In case of user deleted deactivate all tokens[START]
            $tokenDeactivate = $this->UserDevice->updateAll(array("status"=> 0), array("user_id"=> $userId));
            //** In case of user deleted deactivate all tokens[END]
        }else if( trim($statusVal) == 'Inactive' ){ 
            $chngeStatus = 1;
        }
        
        $conditions = array('User.id'=> $userId);
        $data = array(
                    'User.status'=> $chngeStatus,
                    //'User.approved'=> $chngeStatus
                );

        if( $this->User->updateAll( $data ,$conditions )){
            
            if( $chngeStatus == 1 ){ $statusString = 'Active'; }
            if( $chngeStatus == 0 ){ $statusString = 'Inactive'; }

            $logData=$this->getFrontServerInfo();
            $activityData=array('log_info'=>$logData,'user_id'=>$userId,'activity_type'=>'Email Verification','message'=>$statusString,'status'=>1);
            $this->AdminActivityLog->saveAll($activityData);

            echo "OK~$statusString";
        }else{
            echo "ERROR~".ERROR_615;
        }
        exit;
    }
        /*
        On: 05-10-2016
        I/P:
        O/P:
        Desc: Change Account status 
        */
        public function changeAccountStatus(){
            $userId = $this->request->data('userId');
            $status = $this->request->data('status');
            if( trim($status) == 'Activate' ){ 
                $chngeStatus = 0;
                //** In case of user deleted deactivate all tokens[START]
                $tokenDeactivate = $this->UserDevice->updateAll(array("status"=> 0), array("user_id"=> $userId));
                //** In case of user deleted deactivate all tokens[END]
            }else if( trim($status) == 'Deactivate' ){ 
                $chngeStatus = 1;
            }
            
            $conditions = array('User.id'=> $userId);
            $data = array(
                        'User.status'=> $chngeStatus,
                        'User.approved'=> $chngeStatus,
                    );
            if( $this->User->updateAll( $data ,$conditions )){
                
                if( $chngeStatus == 1 ){ 
                    $statusString = 'Activated'; 

                    $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
                    $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'User Approve By Admin')));
                    if($userDetails['User']['registration_source'] == "MB"){
                        $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'MedicBleep User Approve By Admin')));
                        $fromMail = array("no-reply@medicbleep.com"=> "Medic Bleep");
                    }else{
                        $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'User Approve By Admin')));
                        $fromMail = array(NO_REPLY_EMAIL => "The On Call Room");
                    }
                    //** Send email to user for approving user 

                        $params['temp'] = $templateContent['EmailTemplate']['content'];
                        $params['toMail'] = $userDetails['User']['email'];
                        // $params['toMail'] = "deepakkumar@mediccreations.com";
                        $params['name'] = $userDetails['UserProfile']['first_name'];
                        $params['fromMail'] = $fromMail;
                        $params['regards'] = "SITE_NAME";
                        $params['registration_source'] = $userDetails['User']['registration_source'];
                        try{
                            $mailSend = $this->Email->userApproveByAdmin( $params );
                            $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
                        }catch(Exception $e){
                            $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $e->getMessage(), "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
                        }
                }
                if( $chngeStatus == 0 ){ $statusString = 'Deactivated'; }
                echo $statusString;
                $logData=$this->getFrontServerInfo();
                $activityData=array('log_info'=>$logData,'user_id'=>$userId,'activity_type'=>'Account Activation','message'=>$statusString,'status'=>1);
                $this->AdminActivityLog->saveAll($activityData);
            }else{
                echo "ERROR~".ERROR_615;
            }
            exit;
        }
    /*
    On:
    I/P:
    O/P:
    Desc:
    */
    public function getModalBox(){
        $userId = $this->request->data['user_id'];
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        if(!empty($userData['UserProfile']['country_id'])){
            $userCountry = $this->Country->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['country_id'])));
            $userData['UserProfile']['country'] = $userCountry['Country']['country_name'];
        }else{
            $userData['UserProfile']['country'] = '';
        }
        //** Get User Interests
        $interestlists = $this->User->query('SELECT user_interests.interest_id,specilities.name FROM user_interests , specilities WHERE user_interests.interest_id = specilities.id and user_interests.user_id = '.$userId.'  and user_interests.status = 1 ORDER by specilities.name ASC');
        $userData['UserProfile']['interestlist'] = $interestlists;
        //** Get User Posted Beats
            $beatCount = $this->UserPost->find("count", array("conditions"=> array("UserPost.user_id"=> $userId, "status"=> 1, "spam_confirmed"=> 0)));
            $userData['UserProfile']['userBeatPosted'] = $beatCount;
        //** Get User Colleagues
            $myColleagues = $this->UserColleague->find("count", array("conditions"=> array("colleague_user_id"=> $userId, "status"=> 1)));
            $meColleagues = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userId, "status"=> 1)));
            $colleagueCount = ($myColleagues + $meColleagues);
            $userData['UserProfile']['userColleagues'] = $colleagueCount;
        //** Get User Followers
            $followersCount = $this->UserFollow->find("count", array("conditions"=> array("followed_to"=> $userId,"follow_type"=> 1, "status"=> 1)));
            $userData['UserProfile']['userFollowers'] = $followersCount;
        //** Get User Followers
            $followingCount = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $userId, "follow_type"=> 1, "status"=> 1)));
            $userData['UserProfile']['userFollowings'] = $followingCount;
        //** Get User Educational Details
            $educationDetails = $this->UserInstitution->getUserInstitutions($userId);
                $userData['UserProfile']['userInstitutions'] = $educationDetails;
        //** Get User Employment Details
            $employmentDetails = $this->UserEmployment->getUserEmployment($userId);
                $userData['UserProfile']['userEmployments'] = $employmentDetails;
        $this->set('userData', $userData);
        echo $this->render('Subadmin.User/userViewSub');
        exit;
    }
    /*
    On:
    I/P:
    O/P:
    Desc:
    */
    public function clearSearch(){
        $this->Session->delete('searchdata');
        $this->redirect(array('action' => 'userLists'));
    }

    /*
    ---------------------------------------------------------------------------
    On: 21-01-2015
    I/P:
    O/P:
    Desc: Admin Approve/UnApprove user
    ---------------------------------------------------------------------------
    */
    public function changeUserApproveStatus(){
        $userId = $this->request->data('user_id');
        $statusVal = $this->request->data('statusVal');
        if( trim($statusVal) == 'Approved' ){ 
            $chngeStatus = 0;
            //** In case of user deleted deactivate all tokens[START]
            $tokenDeactivate = $this->UserDevice->updateAll(array("status"=> 0), array("user_id"=> $userId));
            //** In case of user deleted deactivate all tokens[END]
        }else if( trim($statusVal) == 'Unapproved' ){ 
            $chngeStatus = 1;
        }
        
        $conditions = array('User.id'=> $userId);
        $data = array(
                    'User.approved'=> $chngeStatus
                );
        
        if( $this->User->updateAll( $data ,$conditions )){
            if( $chngeStatus == 1 ){ 
                $statusString = 'Approved';
                    $userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
                    if($userDetails['User']['registration_source'] == "MB"){
                        $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'MedicBleep User Approve By Admin')));
                        $fromMail = array("no-reply@medicbleep.com"=> "Medic Bleep");
                    }else{
                        $templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'User Approve By Admin')));
                        $fromMail = array(NO_REPLY_EMAIL => "The On Call Room");
                    }
                //** Send email to user when admin approve user 

                    $params['temp'] = $templateContent['EmailTemplate']['content'];
                    $params['toMail'] = $userDetails['User']['email'];
                    // $params['toMail'] = "deepakkumar@mediccreations.com";
                    $params['name'] = $userDetails['UserProfile']['first_name'];
                    $params['fromMail'] = $fromMail;
                    $params['regards'] = "SITE_NAME";
                    $params['registration_source'] = $userDetails['User']['registration_source'];
                    try{
                        $mailSend = $this->Email->userApproveByAdmin( $params );
                        $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
                    }catch(Exception $e){
                        $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $e->getMessage(), "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
                    }


            }
            if( $chngeStatus == 0 ){ 
                $statusString = 'Unapproved'; 
            }
                echo "OK~$statusString";
                $logData=$this->getFrontServerInfo();
                $activityData=array('log_info'=>$logData,'user_id'=>$userId,'activity_type'=>'Account Approval','message'=>$statusString,'status'=>1);
                $this->AdminActivityLog->saveAll($activityData);

        }else{
            echo "ERROR~".ERROR_615;
            exit();
        }
        exit;
    }

    /*

    */
    public function userCsvDownload(){  
        $this->layout = null;
        $this->autoLayout = false;
        $sortPost['textFirstName']=$_REQUEST['FirstName'];
        $sortPost['textLastName']=$_REQUEST['LastName'];
        $sortPost['textEmail']=$_REQUEST['Email'];
        $sortPost['statusVal']=$_REQUEST['statusVal'];
        $sortPost['specilities']=$_REQUEST['specilities'];
        $sortPost['from']=$_REQUEST['from'];
        $sortPost['to']=$_REQUEST['to'];
        $sortPost['country']=$_REQUEST['country'];
        $sortPost['county']=$_REQUEST['county'];
        $sortPost['profession']=$_REQUEST['profession'];
        // $type=$_REQUEST['type'];
            // $sortPost=$this->data;
            if(!isset($sortPost['to']) || $sortPost['to']==""){
              $endDate = date("Y-m-d");
            }
            else{
              $endDate = date('Y-m-d', strtotime($sortPost['to']));
            }
            if(!isset($sortPost['from']) || $sortPost['from']==""){
              $date = array();
            }
            else{
              $startDate = date('Y-m-d', strtotime($sortPost['from']));
              $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
            }
            if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
                $firstName=array();
            }
            else{
                $sortPost['textFirstName']=trim($sortPost['textFirstName']);
                $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textFirstName'].'%'));
            }
            if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
                $lastName=array();
            }
            else{
                $sortPost['textLastName']=trim($sortPost['textLastName']);
                $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower($sortPost['textLastName'].'%'));
            }
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $mail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $mail=array('LOWER(User.email) LIKE'=>strtolower('%'.$sortPost['textEmail'].'%'));
            }
            if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
                $interest=array();
            }
            else{
                $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);
            }
            if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
                $status=array('User.status'=>array(0,1));
            }
            else{
                switch($sortPost['statusVal']){
                    case 0:
                        $status=array('User.status'=>array(0,1));
                        break;
                    case 1:
                        $status=array('User.status'=>'1','User.approved'=>'0');
                        break;
                    case 2:
                        $status=array('User.status'=>'0','User.approved'=>'1');
                        break;
                    case 3:
                        $status=array('User.status'=>'1','User.approved'=>'1');
                        break;
                    case 4:
                        $status=array('User.status'=>'0','User.approved'=>'0');
                        break;
                    case 5:
                        $status=array('User.status'=>'2');
                        break;

                }
             }
             if(!empty($sortPost['country'])){
                 $country=array('UserProfile.country_id'=>$sortPost['country']);
             }
             else{
                $country=array();
             }
             if(!isset($sortPost['county']) || $sortPost['county']==""){
                 $county=array();
             }
             else{
                 $sortPost['county']=trim($sortPost['county']);
                 $county=array('LOWER(UserProfile.county) LIKE'=>strtolower('%'.$sortPost['county'].'%'));
             }
             if(!empty($sortPost['profession'])){
                 $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
             }
             else{
                $profession=array();
             }
             $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$county,$profession);
             $userDetails=array();
             $limit = $this->User->find("count", array("conditions"=> $conditions));
             $fields =array("User.id,User.email,User.activation_key,User.last_loggedin_date,User.login_device,User.registration_date,User.status,User.approved,User.registration_source,UserProfile.first_name,UserProfile.last_name,UserProfile.dob,UserProfile.gmcnumber,UserProfile.county,UserProfile.city,UserProfile.contact_no,UserProfile.contact_no_is_verified,UserProfile.registration_image,UserProfile.address,UserProfile.profile_img,UserProfile.gender,Country.country_name,Profession.profession_type");
            $options  = 
            array(
                    'joins'=>array(
                        array(
                          'table' => 'countries',
                          'alias' => 'Country',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.country_id = Country.id')
                      ),
                        array(
                          'table' => 'professions',
                          'alias' => 'Profession',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.profession_id = Profession.id')
                      ),
                        array(
                          'table' => 'user_specilities',
                          'alias' => 'UserSpecility',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserSpecility.user_id')
                      ),
                        array(
                          'table' => 'user_interests',
                          'alias' => 'UserInterest',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserInterest.user_id')
                      )
                    ),
                    'fields'=> $fields,
                    'conditions'=> $conditions,
                    'group'=> array('User.id'),
                    'order'=> 'User.registration_date DESC', 
                    'limit'=> $limit,
                    );
        $userLists = $this->User->find("all",$options);
            foreach ($userLists as  $value) {            
            // $specilities_id=$this->UserSpecilities->find("all",array('conditions'=>array('UserSpecilities.user_id='.$value['User']['id'],'UserSpecilities.status'=>'1')));
            $specilities_id=$this->UserSpecilities->specName($value['User']['id']);
            $interest_id=$this->UserInterest->specInterest($value['User']['id']);
            $devices=$this->UserDevice->deviceStatusCount($value['User']['id']);

            $myColleagues = $this->UserColleague->find("count", array("conditions"=> array("colleague_user_id"=> $value['User']['id'], "status"=> 1)));
            $meColleagues = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $value['User']['id'], "status"=> 1)));
            $colleage = ($myColleagues + $meColleagues);

            $following=$this->UserFollow->followCount( $value['User']['id'] );
            $follower=$this->UserFollow->followingCount( $value['User']['id'] );
            $interestId[]=$interest_id;
            $specilities[]=$specilities_id;
            $devicesStatus[]=$devices;
            $colleageStatus[]=$colleage;
            $follwerStatus[]=$follower;
            $followingStatus[]=$following;
        }
        $this->set(array('userLists'=>$userLists,'specilities'=>$specilities,'interests'=>$interestId,'device'=>$devicesStatus,'colleage'=>$colleageStatus,'follower'=>$follwerStatus,'following'=>$followingStatus));
    }
    /*
    ------------------------------------------------------------------------------------------
    On: 16-02-2016
    I/P:
    O/P:
    Desc: User profile view to update
    ------------------------------------------------------------------------------------------
    */
    public function editUserProfileView(){
        if(isset($this->request->data) && !empty($this->request->data)){
            $userId = $this->request->data['user_id'];
            $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
            $this->set('userData', $userData);
        }
        echo $this->render('Subadmin.User/edit_user_profile_view');
        exit;
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 16-02-2016
    I/P:
    O/P:
    Desc: User profile update
    ------------------------------------------------------------------------------------------
    */
    public function editUserProfile(){ //echo "<pre>";print_r($this->request->data);die;
        //** Update GMC Number
        if(isset($this->request->data['textGmcNumber']) && !empty($this->request->data['textGmcNumber'])){
            $userId = $this->request->data['userId'];
            $profileUpdate = $this->User->updateAll(array('UserProfile.gmcnumber'=> "'". $this->request->data['textGmcNumber'] ."'"), array("UserProfile.user_id"=> $userId)); 
            if($profileUpdate){
               $msg = "Profile Updated"; 
            }else{
               $msg = "Profile Not Updated";  
            }
        }else{
            $msg = "Some Error! Please Try Again.";
        }
        echo $msg;
        exit;
    }

    /*
    ------------------------------------------------------------------------------------------
    On: 22-02-2016
    I/P:
    O/P:
    Desc: Delete user from list (soft delete)
    ------------------------------------------------------------------------------------------
    */
    public function deleteUser(){
        if(isset($this->request->data['userId'])){
            $updateUser = $this->User->updateAll(array("User.status"=> 2), array("User.id"=> $this->request->data['userId']));
            if($updateUser){
            $activityData=array('user_id'=>$this->request->data['userId'],'activity_type'=>'userDeletion','message'=>$this->request->data['reason'],'status'=>1);
            $this->AdminActivityLog->saveAll($activityData);
                echo "User Deleted";
            }else{
                echo "Some issue occured. Please try again.";
            }
        }else{
            echo "User Not Deleted! Try Again.";
        }
        exit;
    }

    /*
    On: 26-08-2015
    I/P:
    O/P: 
    Desc:
    */
    public function userListsForsubadmin(){ 
        $this->layout = 'subadmin';
        $params = array();
        $conditions = "";
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
              $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
              $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
              $sort="User.email $order";
            }
            else{
            $sort="User.registration_date $order";
            }
        }
        else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        if ($this->request->data) {
            $data = $this->request->data;
            $params['textFirstName'] = $data['textFirstName'];
            $params['textLastName'] = $data['textLastName'];
            $params['textEmail'] = $data['textEmail'];
            $params['statusVal'] = $data['statusVal'];
                if(isset($data['specilities']) && !empty($data['specilities'])){
                    $params['specilities'] = implode(",", $data['specilities']);
                }
            if( isset($params['statusVal']) && $params['statusVal'] !=''){
                if($params['statusVal'] == 1){
                    $activeStatus = 1;
                }elseif($params['statusVal'] == 2){
                    $activeStatus = 0;
                }elseif($params['statusVal'] == 3){
                    $approveStatus = 1;
                }elseif($params['statusVal'] == 4){
                    $approveStatus = 0;
                }
                //** User active status check
                if(isset($activeStatus) && ($activeStatus == 1 || $activeStatus == 0)){
                    if(strlen($conditions) == 0){
                      $conditions .= '  User.status = "'.$activeStatus.'" ';
                    }else{
                      $conditions .= ' AND User.status = "'.$activeStatus.'" ';
                    }
                }
                //** User approve status check
                if(isset($approveStatus) && ($approveStatus == 1 || $approveStatus == 0)){
                    if(strlen($conditions) == 0){
                      $conditions .= '  User.approved = "'.$approveStatus.'" ';
                    }else{
                      $conditions .= ' AND User.approved = "'.$approveStatus.'" ';
                    }
                }
            }
            if( isset($params['textFirstName']) && $params['textFirstName'] !=''){
                if(strlen($conditions) == 0){
                  $conditions .= '  User.status IN (0,1) AND LOWER(UserProfile.first_name) LIKE LOWER("'.$params['textFirstName'].'%") ';
                }else{
                  $conditions .= ' AND User.status IN (0,1) AND LOWER(UserProfile.first_name) LIKE LOWER("'.$params['textFirstName'].'%") ';
                }
            }
            if( isset($params['textLastName']) && $params['textLastName'] !=''){
                if(strlen($conditions) == 0){
                  $conditions .= '  User.status IN (0,1) AND LOWER(UserProfile.last_name) LIKE LOWER("'.$params['textLastName'].'%") ';
                }else{
                  $conditions .= ' AND User.status IN (0,1) AND LOWER(UserProfile.last_name) LIKE LOWER("'.$params['textLastName'].'%") ';
                }
            }
            if( isset($params['textEmail']) && $params['textEmail'] !=''){
                if(strlen($conditions) == 0){
                  $conditions .= '  User.status IN (0,1) AND LOWER(User.email) LIKE LOWER("'.$params['textEmail'].'%") ';
                }else{
                  $conditions .= ' AND User.status IN (0,1) AND LOWER(User.email) LIKE LOWER("'.$params['textEmail'].'%") ';
                }
            }
            if( isset($params['specilities']) && !empty($params['specilities'])){
                if(strlen($conditions) == 0){
                  $conditions .= '  User.status IN (0,1) AND UserSpecility.specilities_id IN ("'.$params['specilities'].'")';
                }else{
                  $conditions .= '  AND User.status IN (0,1) AND UserSpecility.specilities_id IN ("'.$params['specilities'].'")';
                }
            }
        }else{
            $conditions .= " User.status IN (0,1)";
        }
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $interestLists['interestlist'] = $interestlists; 
        $this->set('interestLists', $interestLists);
        //** Get User Data
        
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                ),
                'fields'=> array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county,city, Country.country_name, Profession.profession_type"),
                'conditions'=> $conditions,
                // 'group'=> array('User.id'),
                'order'=> $sort, 
                'limit'=> $limit,
                'page'=>$j
                );
                $this->Paginator->settings = $options;
                $users = $this->Paginator->paginate('User');
        
        $this->set(array('userData'=>$users,'j'=>$j,'limit'=>$limit));
        if($this->request->is("ajax")){
            $this->layout = '';
            $this->render('/Elements/users/user_lists_forsubadmin_ajax');
        }
        
    }

    /*
    ----------------------------------------------------------------------------------------------
    On: 
    I/P: 
    O/P: 
    Desc: 
    ----------------------------------------------------------------------------------------------
    */
    public function userView($id=null){
        $userId = $id;
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        //** User Country
        if(!empty($userData['UserProfile']['country_id'])){
            $userCountry = $this->Country->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['country_id'])));
            $userData['UserProfile']['countryId'] = $userCountry['Country']['id'];
            $userData['UserProfile']['countryName'] = $userCountry['Country']['country_name'];
        }else{
            $userData['UserProfile']['countryName'] = '';
        }
        //** User Profession
        if(!empty($userData['UserProfile']['profession_id'])){
            $userProfession = $this->Profession->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['profession_id'])));
            $userData['UserProfile']['professionId'] = $userProfession['Profession']['id'];
            $userData['UserProfile']['professionName'] = $userProfession['Profession']['profession_type'];
        }else{
            $userData['UserProfile']['professionName'] = '';
        }
        //** Get User Interests
        $interestlists = array();
        $interestlists = $this->User->query('SELECT GROUP_CONCAT(specilities.name) AS interestLists FROM user_interests , specilities WHERE user_interests.interest_id = specilities.id and user_interests.user_id = '.$userId.'  and user_interests.status = 1 ORDER by specilities.name ASC');
        $userData['UserProfile']['interestlist'] = $interestlists[0][0]['interestLists'];
        //** Get User Specilities
        $interestlists = array();
        $interestlists = $this->User->query('SELECT GROUP_CONCAT(specilities.name) AS specilityLists FROM user_specilities , specilities WHERE user_specilities.specilities_id = specilities.id and user_specilities.user_id = '.$userId.'  and user_specilities.status = 1 ORDER by specilities.name ASC');
        $userData['UserProfile']['specilityLists'] = $interestlists[0][0]['specilityLists'];
        //** Get User Posted Beats
            $beatCount = $this->UserPost->find("count", array("conditions"=> array("UserPost.user_id"=> $userId, "status"=> 1, "spam_confirmed"=> 0)));
            $userData['UserProfile']['userBeatPosted'] = $beatCount;
        //** Get User Colleagues
            $myColleagues = $this->UserColleague->find("count", array("conditions"=> array("colleague_user_id"=> $userId, "status"=> 1)));
            $meColleagues = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userId, "status"=> 1)));
            $colleagueCount = ($myColleagues + $meColleagues);
            $userData['UserProfile']['userColleagues'] = $colleagueCount;
        //** Get User Followers
            // $followersCount = $this->UserFollow->find("count", array("conditions"=> array("followed_to"=> $userId,"follow_type"=> 1, "status"=> 1)));

            $followersCount=$this->UserFollow->followCount( $userId );
        //** Get User Followers
            // $followingCount = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $userId, "follow_type"=> 1, "status"=> 1)));

            $followingCount=$this->UserFollow->followingCount( $userId );
            
            $userData['UserProfile']['userFollowers'] = $followingCount;
            $userData['UserProfile']['userFollowings'] = $followersCount;
        //** Get User Educational Details
            $userEducation = array();
            $educationDetails = $this->UserInstitution->getUserInstitutions($userId);
            // foreach($educationDetails as $education){
            //     $userEducation[] = $education['EducationDegree']['degree_name'];
            // }
                $userData['UserProfile']['userEducation'] = $educationDetails; 
        //** Get User Employment Details
            $employmentDetails = $this->UserEmployment->getUserEmployment($userId);
                $userData['UserProfile']['userEmployments'] = $employmentDetails;
        //** Set User Id
                $userData['User']['id'] = $userId;
        $activityLog=$this->AdminActivityLog->find('all',array('conditions'=>array('AdminActivityLog.user_id'=>$id)));
        $companies=$this->CompanyName->find('all',array('conditions'=>array('status'=>1)));
        $designation=$this->Designation->find('all',array('conditions'=>array('status'=>1)));

        $degree=$this->EducationDegree->find('all',array('conditions'=>array('status'=>1)));
        $institutes=$this->InstituteName->find('all',array('conditions'=>array('status'=>1)));

        $devices=$this->UserDevice->deviceStatusCount($id);

        $subjects=$this->AdminEmailCategorie->find('all',array('conditions'=>array('status'=>1)));

        $mailLog=$this->get_admin_user_mail($id);
        $this->set(array('userData'=>$userData,'activityLog'=>$activityLog,'mailLog'=>$mailLog,'comapnies'=>$companies,'designation'=>$designation,'degree'=>$degree,'institutes'=>$institutes,'devices'=>$devices,'subjects'=>$subjects));
    }

    public function sendMail($id){
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $id)));
        $subjects=$this->AdminEmailCategorie->find('all',array('conditions'=>array('status'=>1)));
        $this->set(array('subjects'=>$subjects,'userData'=>$userData));
    }

    public function templateSelect(){
        if($this->data['type']=="comment"){
            $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$this->data['contentId'])));
            echo $template['AdminEmailTemplate']['content'];
            exit();
        }
        $id=$this->data['id'];
        $templates=$this->AdminEmailTemplate->find('all',array('conditions'=>array('email_category_id'=>$id)));
        $this->set('templates',$templates);
        $this->render('/Elements/users/user_template_select');
    }

    public function templatePreview(){
        if($this->data['type']=="send"){
        $id=$this->data['id'];
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$id),'fields'=>array('title')));
        echo $template['AdminEmailTemplate']['title'];
        exit();
        }
       if($this->data['type']=="view"){
        $id=$this->data['id'];
        $email=$this->data['email'];
        $view=$this->AdminUserSentMail->find('first',array('conditions'=>array('id'=>$id)));
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$view['AdminUserSentMail']['email_template_id']),'fields'=>array('title')));
        $data=array($template['AdminEmailTemplate']['title'],$view['AdminUserSentMail']['content'],$view['AdminUserSentMail']['created_date']);
        $this->set(array('data'=>$data,'email'=>$email));
        $this->render('/Elements/action_mail_preview');
       }
    }

    public function adminToUserMail(){
        $data=$this->data;
        $template=$this->AdminEmailTemplate->find('first',array('conditions'=>array('id'=>$data['template']),'fields'=>array('title')));
        $params['temp'] = $data['content'];
        $params['subject'] = $template['AdminEmailTemplate']['title'];
        $params['toMail'] = $data['email'];
        // $params['toMail'] = "deepakkumar@mediccreations.com";
        $params['registration_source'] = $data['source'];
        try{
            $mailSend = $this->Email->sendMailByAdmin( $params );

            $mailData=array('user_id'=>$data['userId'],'email_category_id'=>$data['subject'],'email_template_id'=>$data['template'],'comment'=>$template['AdminEmailTemplate']['title'],'content'=>$data['content'],'status'=>1);
            $this->AdminUserSentMail->saveAll($mailData);

            echo $mailSend;
            exit();
        }catch(Exception $e){
        }
    }

    public function addEmployement(){
        $data=$this->data;
        $from=date("Y-m-d", strtotime($data['from']));
        if($data['to'] == "0000-00-00 00:00:00"){
            $present="1";
            $to=date("Y-m-d", strtotime($data['to']));
            $to=trim($to,'-');
        }
        else{
            $present="0";
            $to=date("Y-m-d", strtotime($data['to']));
        }
        $empData=array('user_id'=>$data['userId'],'company_id'=>$data['orgainsation'],'from_date'=>$from,'to_date'=>$to,'location'=>$data['location'],'description'=>$data['description'],'is_current'=>$present,'designation_id'=>$data['designation'],'status'=>1);
        $this->UserEmployment->saveAll($empData);

        $employmentDetails = $this->UserEmployment->getUserEmployment($data['userId']);
            $userData['UserProfile']['userEmployments'] = $employmentDetails;
        $this->set(array('userData'=>$userData));
        $this->render('/Elements/users/add_employee');
    }

    public function addEducation(){
        $data=$this->data;
        $eduData=array('user_id'=>$data['userId'],'institution_id'=>$data['institution'],'education_degree_id'=>$data['degree'],'description'=>$data['description'],'from_date'=>$data['from'],'to_date'=>$data['to'],'status'=>1);
        $this->UserInstitution->saveAll($eduData);

        $educationDetails = $this->UserInstitution->getUserInstitutions($data['userId']);
        $userData['UserProfile']['userEducation'] = $educationDetails;
        $this->set(array('userData'=>$userData));
        $this->render('/Elements/users/add_education');
    }

    /*
    ---------------------------------------------------------------------------------------------
    On: 15-06-2016
    I/P: 
    O/P: 
    Desc: Fetches approved user lists
    ---------------------------------------------------------------------------------------------
    */
    public function approvedUserLists(){ 
        $this->Session->delete('User.approved');  
        $this->Session->write('User.approved', "Approved");
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1   ORDER BY profession_type ASC');
        $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        $interestLists['interestlist'] = $interestlists; 
        $this->set(array('interestLists'=>$interestLists,'country'=>$country,'professions'=>$profession));
        //** Get User Data
        $conditions = array('User.status'=>array(0,1),'User.approved=1');
        $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('User.id'),
                'order'=> 'User.registration_date DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        // pr($conditions);
                $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                $this->Paginator->settings = $options;
                $users = $this->Paginator->paginate('User');
            if(isset($users) && !empty($users)){
                foreach($users as $user){
                    $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                    $userS[]=$user;
                }
            }
            else{
              $userS=array();
            }
        $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        // if($this->request->is("ajax")){
        //     $this->render('/Elements/users/user_lists_ajax');
        // }
    }
    public function approvedFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
              $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
              $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
              $sort="User.email $order";
            }
            elseif($con=='country'){
              $sort="Country.country_name $order";
            }
            elseif($con=='county'){
              $sort="UserProfile.county $order";
            }
            else{
            $sort="User.registration_date $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
            $sortPost=$this->data;
            if(!isset($sortPost['to']) || $sortPost['to']==""){
              $endDate = date("Y-m-d");
            }
            else{
              $endDate = date('Y-m-d', strtotime($sortPost['to']));
            }
            if(!isset($sortPost['from']) || $sortPost['from']==""){
              $date = array();
            }
            else{
              $startDate = date('Y-m-d', strtotime($sortPost['from']));
              $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
            }
            if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
                $firstName=array();
            }
            else{
                $sortPost['textFirstName']=trim($sortPost['textFirstName']);
                $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textFirstName'].'%'));
            }
            if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
                $lastName=array();
            }
            else{
                $sortPost['textLastName']=trim($sortPost['textLastName']);
                $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower($sortPost['textLastName'].'%'));
            }
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $mail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $mail=array('LOWER(User.email) LIKE'=>strtolower('%'.$sortPost['textEmail'].'%'));
            }
            if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
                $interest=array();
            }
            else{
                $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);            }
            if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
                $status=array('User.status'=>array(0,1),'User.approved'=>'1');
            }
            else{
                if($sortPost['statusVal']==1){
                  $status=array('User.status'=>'1','User.approved'=>array(0));
                }
                else if($sortPost['statusVal']==2){
                    $status=array('User.status'=>'0','User.approved'=>array(1));
                }
                else if($sortPost['statusVal']==3){
                    $status=array('User.status'=>array(1),'User.approved'=>'1');
                }
                else if($sortPost['statusVal']==4){
                    $status=array('User.status'=>array(0),'User.approved'=>'0');
                }
                else{
                    $status=array('User.status'=>'2');
                }
             }
             if(!empty($sortPost['country'])){
                 $country=array('UserProfile.country_id'=>$sortPost['country']);
             }
             else{
                $country=array();
             }
             if(!isset($sortPost['county']) || $sortPost['county']==""){
                 $county=array();
             }
             else{
                 $sortPost['county']=trim($sortPost['county']);
                 $county=array('LOWER(UserProfile.county) LIKE'=>strtolower('%'.$sortPost['county'].'%'));
             }
             if(!empty($sortPost['profession'])){
                 $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
             }
             else{
                $profession=array();
             }
             $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$county,$profession);
             $fields =array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
            $options  = 
            array(
                    'joins'=>array(
                        array(
                          'table' => 'countries',
                          'alias' => 'Country',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.country_id = Country.id')
                      ),
                        array(
                          'table' => 'professions',
                          'alias' => 'Profession',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.profession_id = Profession.id')
                      ),
                        array(
                          'table' => 'user_specilities',
                          'alias' => 'UserSpecility',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserSpecility.user_id')
                      ),
                    ),
                    'fields'=> $fields,
                    'conditions'=> $conditions,
                    'group'=> array('User.id'),
                    'order'=> $sort, 
                    'limit'=> $limit,
                    'page'=>$j
                    );
            //pr($conditions);
                    // $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                    $this->Paginator->settings = $options;
                    $users = $this->Paginator->paginate('User');
                if(isset($users) && !empty($users)){
                    foreach($users as $user){
                        $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                        $userS[]=$user;
                    }
                }
                else{
                  $userS=array();
                }
                $total=$this->request->params;
        $tCount=$total['paging']['User']['count'];
            $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
            $this->render('/Elements/users/user_lists_ajax');

    }
    /*
    ---------------------------------------------------------------------------------------------
    On: 15-06-2016
    I/P: 
    O/P: 
    Desc: Fetches all pending user lists
    ---------------------------------------------------------------------------------------------
    */
    public function pendingUserLists(){ 
        $this->Session->delete('User.approved'); 
        $this->Session->write('User.approved', "Pending");
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $interestlists = $this->User->query('SELECT id,name FROM specilities  WHERE status = 1  ORDER BY name ASC');
        $profession = $this->User->query('SELECT id,profession_type FROM professions WHERE status = 1   ORDER BY profession_type ASC');
        $country = $this->User->query('SELECT id,country_code,country_name FROM countries   ORDER BY country_name ASC');
        $interestLists['interestlist'] = $interestlists; 
        $this->set(array('interestLists'=>$interestLists,'country'=>$country,'professions'=>$profession));
        //** Get User Data
        $conditions = array('User.status'=>array(0,1),'User.approved=0');
        $fields = array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'countries',
                      'alias' => 'Country',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.country_id = Country.id')
                  ),
                    array(
                      'table' => 'professions',
                      'alias' => 'Profession',
                      'type' => 'left',
                      'conditions'=> array('UserProfile.profession_id = Profession.id')
                  ),
                    array(
                      'table' => 'user_specilities',
                      'alias' => 'UserSpecility',
                      'type' => 'left',
                      'conditions'=> array('User.id = UserSpecility.user_id')
                  ),
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('User.id'),
                'order'=> 'User.registration_date DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
        // pr($conditions);
                $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                $this->Paginator->settings = $options;
                $users = $this->Paginator->paginate('User');
        if(isset($users) && !empty($users)){
            foreach($users as $user){
                $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                $userS[]=$user;
            }
        }
        else{
          $userS=array();
        }
        $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        // if($this->request->is("ajax")){
        //     $this->render('/Elements/users/user_lists_ajax');
        // }
    }
    public function pendingFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='first_name'){
              $sort="UserProfile.first_name $order";
            }
            elseif($con=='last_name'){
              $sort="UserProfile.last_name $order";
            }
            elseif($con=='profession'){
              $sort="Profession.profession_type $order";
            }
            elseif($con=='email'){
              $sort="User.email $order";
            }
            elseif($con=='country'){
              $sort="Country.country_name $order";
            }
            elseif($con=='county'){
              $sort="UserProfile.county $order";
            }
            else{
            $sort="User.registration_date $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='User.registration_date DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=1;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
            $sortPost=$this->data;
            if(!isset($sortPost['to']) || $sortPost['to']==""){
              $endDate = date("Y-m-d");
            }
            else{
              $endDate = date('Y-m-d', strtotime($sortPost['to']));
            }
            if(!isset($sortPost['from']) || $sortPost['from']==""){
              $date = array();
            }
            else{
              $startDate = date('Y-m-d', strtotime($sortPost['from']));
              $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate);
            }
            if(!isset($sortPost['textFirstName']) || $sortPost['textFirstName']==""){
                $firstName=array();
            }
            else{
                $sortPost['textFirstName']=trim($sortPost['textFirstName']);
                $firstName=array('LOWER(UserProfile.first_name) LIKE'=>strtolower($sortPost['textFirstName'].'%'));
            }
            if(!isset($sortPost['textLastName']) || $sortPost['textLastName']==""){
                $lastName=array();
            }
            else{
                $sortPost['textLastName']=trim($sortPost['textLastName']);
                $lastName=array('LOWER(UserProfile.last_name) LIKE'=>strtolower($sortPost['textLastName'].'%'));
            }
            if(!isset($sortPost['textEmail']) || $sortPost['textEmail']==""){
                $mail=array();
            }
            else{
                $sortPost['textEmail']=trim($sortPost['textEmail']);
                $mail=array('LOWER(User.email) LIKE'=>strtolower('%'.$sortPost['textEmail'].'%'));
            }
            if(!isset($sortPost['specilities'])|| $sortPost['specilities']==""){
                $interest=array();
            }
            else{
                $interest=array('UserSpecility.specilities_id'=>$sortPost['specilities']);
            }
            if(!isset($sortPost['statusVal'])||$sortPost['statusVal']==0){
                $status=array('User.status'=>array(0,1),'User.approved'=>'0');
            }
            else{
                if($sortPost['statusVal']==1){
                  $status=array('User.status'=>'1','User.approved'=>array(0));
                }
                else if($sortPost['statusVal']==2){
                    $status=array('User.status'=>'0','User.approved'=>array(1));
                }
                else if($sortPost['statusVal']==3){
                    $status=array('User.status'=>array(1),'User.approved'=>'1');
                }
                else if($sortPost['statusVal']==4){
                    $status=array('User.status'=>array(0),'User.approved'=>'0');
                }
                else{
                    $status=array('User.status'=>'2');
                }
             }
             if(!empty($sortPost['country'])){
                 $country=array('UserProfile.country_id'=>$sortPost['country']);
             }
             else{
                $country=array();
             }
             if(!isset($sortPost['county']) || $sortPost['county']==""){
                 $county=array();
             }
             else{
                 $sortPost['county']=trim($sortPost['county']);
                 $county=array('LOWER(UserProfile.county) LIKE'=>strtolower('%'.$sortPost['county'].'%'));
             }
             if(!empty($sortPost['profession'])){
                 $profession=array('UserProfile.profession_id'=>$sortPost['profession']);
             }
             else{
                $profession=array();
             }
             $conditions=array_merge($firstName,$lastName,$mail,$status,$interest,$date,$country,$county,$profession);
             $fields =array("User.id, User.email, User.activation_key, User.last_loggedin_date, User.login_device, User.registration_date, User.status, User.approved, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id, UserProfile.county, UserProfile.county, UserProfile.city, Country.country_name, Profession.profession_type");
            $options  = 
            array(
                    'joins'=>array(
                        array(
                          'table' => 'countries',
                          'alias' => 'Country',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.country_id = Country.id')
                      ),
                        array(
                          'table' => 'professions',
                          'alias' => 'Profession',
                          'type' => 'left',
                          'conditions'=> array('UserProfile.profession_id = Profession.id')
                      ),
                        array(
                          'table' => 'user_specilities',
                          'alias' => 'UserSpecility',
                          'type' => 'left',
                          'conditions'=> array('User.id = UserSpecility.user_id')
                      ),
                    ),
                    'fields'=> $fields,
                    'conditions'=> $conditions,
                    'group'=> array('User.id'),
                    'order'=> $sort, 
                    'limit'=> $limit,
                    'page'=>$j
                    );
            //pr($conditions);
                    // $tCount=$this->User->find('count',array('conditions'=>$conditions,'fields'=>$fields,'group'=> array('User.id')));
                    $this->Paginator->settings = $options;
                    $users = $this->Paginator->paginate('User');
                if(isset($users) && !empty($users)){
                    foreach($users as $user){
                        $user['mail'] = count($this->get_admin_user_mail($user['User']['id']));
                        $userS[]=$user;
                    }
                }
                else{
                  $userS=array();
                }
                $total=$this->request->params;
        $tCount=$total['paging']['User']['count'];
            $this->set(array("userData"=>$userS,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
            $this->render('/Elements/users/user_lists_ajax');

    }

    /*
    ----------------------------------------------------------------------------------
    On: 20-06-2016
    I/P:
    O/P:
    Desc: Incomplete User SignUp Email Id
    ----------------------------------------------------------------------------------
    */

    public function incompleteUserLists(){

        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=0;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        //** Get User Data
        $conditions = array('TempRegistration.status'=>array(0,1),'User.status'=>NULL);
        $fields = array("TempRegistration.id, TempRegistration.email,TempRegistration.registration_source,TempRegistration.device_type,TempRegistration.version,TempRegistration.created,TempRegistration.status");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'LEFT',
                      'conditions'=> array('User.email = TempRegistration.email')
                  )
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('TempRegistration.email'),
                'order'=> 'TempRegistration.created DESC', 
                'limit'=> $limit,
                'page'=>$j
                );
            $this->Paginator->settings = $options;
            $users = $this->Paginator->paginate('TempRegistration');
            $total=$this->request->params;
            $tCount=$total['paging']['TempRegistration']['count'];
        $this->set(array("userData"=>$users,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));     
    }

    public function incompleteUserFilter(){
        if(isset($this->data['sort'])){
            $con=$this->data['sort'];
            $type=$this->data['order'];
            $order=$type."SC";
            if($con=='email'){
              $sort="TempRegistration.email $order";
            }else{
            $sort="TempRegistration.created $order";
        }
        }
        else{
            $con="";
            $type="";
            $sort='TempRegistration.created DESC';
        }
        if(isset($this->data['j'])){
            $j=$this->data['j'];
        }
        else{
            $j=0;
        }
        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }
        $sortPost=$this->data;
        if(!isset($sortPost['platform'])||$sortPost['platform']==0){
            $platform=array();
        }
        else{
            if($sortPost['platform']==1){
              $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'android');
            }
            else if($sortPost['platform']==2){
                $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'ios');
            }
            else if($sortPost['platform']==3){
                $platform=array('LOWER(TempRegistration.device_type) LIKE'=>'ocrweb');
            }
         }
         if(!isset($sortPost['source'])||$sortPost['source']==0){
            $source=array();
        }
        else{
            if($sortPost['source']==1){
              $source=array('LOWER(TempRegistration.registration_source) LIKE'=>'ocr');
            }
            else if($sortPost['source']==2){
                $source=array('LOWER(TempRegistration.registration_source) LIKE'=>'mb');
            }
         }
         $conditions=array_merge($source,$platform,array('User.status'=>NULL));
         $fields = array("TempRegistration.id, TempRegistration.email,TempRegistration.registration_source,TempRegistration.device_type,TempRegistration.version,TempRegistration.created,TempRegistration.status");
        $options  = 
        array(
                'joins'=>array(
                    array(
                      'table' => 'users',
                      'alias' => 'User',
                      'type' => 'LEFT',
                      'conditions'=> array('User.email = TempRegistration.email')
                  )
                ),
                'fields'=> $fields,
                'conditions'=> $conditions,
                'group'=> array('TempRegistration.email'),
                'order'=> $sort, 
                'limit'=> $limit,
                'page'=>$j
                );
        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('TempRegistration');
        $total=$this->request->params;
        $tCount=$total['paging']['TempRegistration']['count'];
        $count=count($users);
        $this->set(array("userData"=>$users,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));     
            $this->render('/Elements/users/incomplete_user_lists_ajax');

    }

    /*
    ----------------------------------------------------------------------------------
    On: 20-06-2016
    I/P:
    O/P:
    Desc: Edit user by admin
    ----------------------------------------------------------------------------------
    */
    public function editUser(){
      $userId = $this->request->params['pass'][0];//$this->request->data['user_id'];
        $userData = $this->User->find('first', array('conditions'=> array('User.id'=> $userId)));
        //** User Country
        if(!empty($userData['UserProfile']['country_id'])){
            $userCountry = $this->Country->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['country_id'])));
            $userData['UserProfile']['countryId'] = $userCountry['Country']['id'];
            $userData['UserProfile']['countryName'] = $userCountry['Country']['country_name'];
        }else{
            $userData['UserProfile']['countryName'] = '';
        }
        //** User Profession
        if(!empty($userData['UserProfile']['profession_id'])){
            $userProfession = $this->Profession->find("first", array("conditions"=> array("id"=> $userData['UserProfile']['profession_id'])));
            $userData['UserProfile']['professionId'] = $userProfession['Profession']['id'];
            $userData['UserProfile']['professionName'] = $userProfession['Profession']['profession_type'];
        }else{
            $userData['UserProfile']['professionName'] = '';
        }
        //** Get User Interests
        $interestlistsData = array();
        $interestlists = $this->User->query('SELECT specilities.id AS userinterest FROM user_interests , specilities WHERE user_interests.interest_id = specilities.id and user_interests.user_id = '.$userId.'  and user_interests.status = 1 ORDER by specilities.name ASC');
        foreach($interestlists as $userInterest){
            $interestlistsData[] = $userInterest['specilities']['userinterest'];
        }
        $userData['UserProfile']['interestlist'] = $interestlistsData;
        //** Get User Specilities
        $specilitylistsData = array();
        $specilitiesList = $this->User->query('SELECT specilities.id AS userspecility FROM user_specilities , specilities WHERE user_specilities.specilities_id = specilities.id and user_specilities.user_id = '.$userId.'  and user_specilities.status = 1 ORDER by specilities.name ASC');
        foreach($specilitiesList as $userSpecility){
            $specilitylistsData[] = $userSpecility['specilities']['userspecility'];
        }
        $userData['UserProfile']['specilityLists'] = $specilitylistsData;
        //** Get User Posted Beats
            $beatCount = $this->UserPost->find("count", array("conditions"=> array("UserPost.user_id"=> $userId, "status"=> 1, "spam_confirmed"=> 0)));
            $userData['UserProfile']['userBeatPosted'] = $beatCount;
        //** Get User Colleagues
            $myColleagues = $this->UserColleague->find("count", array("conditions"=> array("colleague_user_id"=> $userId, "status"=> 1)));
            $meColleagues = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userId, "status"=> 1)));
            $colleagueCount = ($myColleagues + $meColleagues);
            $userData['UserProfile']['userColleagues'] = $colleagueCount;
        //** Get User Followers
            $followersCount = $this->UserFollow->find("count", array("conditions"=> array("followed_to"=> $userId,"follow_type"=> 1, "status"=> 1)));
            $userData['UserProfile']['userFollowers'] = $followersCount;
        //** Get User Followers
            $followingCount = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $userId, "follow_type"=> 1, "status"=> 1)));
            $userData['UserProfile']['userFollowings'] = $followingCount;
        //** Get User Educational Details
            $educationDetails = $this->UserInstitution->getUserInstitutions($userId);
                $userData['UserProfile']['userInstitutions'] = $educationDetails;
        //** Get User Employment Details
            $employmentDetails = $this->UserEmployment->getUserEmployment($userId);
                $userData['UserProfile']['userEmployments'] = $employmentDetails;
        //** Get All Countries lists
                $countryList = $this->Country->find("all", array("fields"=> array("id","country_name")));
                foreach($countryList as $countryL){
                    $countryData[] = array("id"=> $countryL['Country']['id'], "name"=> $countryL['Country']['country_name']); 
                }
              $userData['UserProfile']['countryList'] =  $countryData;
        //** Get All Profession lists
                $professionList = $this->Profession->find("all", array("fields"=> array("id","profession_type"), "conditions"=> array("status"=> 1)));
                foreach($professionList as $professionL){
                    $professionData[] = array("id"=> $professionL['Profession']['id'], "name"=> $professionL['Profession']['profession_type']); 
                }
              $userData['UserProfile']['professionList'] =  $professionData;
        //** Get All Interests lists
                $interestMasterList = $this->Specilities->find("all", array("fields"=> array("id","name"), "conditions"=> array("status"=> 1)));
                foreach($interestMasterList as $interestL){
                    $interestMasterListData[] = array("id"=> $interestL['Specilities']['id'], "name"=> $interestL['Specilities']['name']); 
                }
              $userData['UserProfile']['interestMasterList'] =  $interestMasterListData;
        //** Set userid
              $userData['UserProfile']['userId'] = $userId;
        $this->set('userData', $userData);  
    }

    /*
    -----------------------------------------------------------------------------------
    On: 
    I/P:
    O/P:
    Desc
    -----------------------------------------------------------------------------------
    */
    public function editUserProfileAccount(){  
        if(isset($this->request->data['userId']) && !empty($this->request->data['userId'])){
            $userId = $this->request->data['userId'];
            $firstName = isset($this->request->data['firstName']) ? $this->request->data['firstName'] : '';
            $lastName = isset($this->request->data['lastName']) ? $this->request->data['lastName'] : '';
            $contactNumber = isset($this->request->data['contactNumber']) ? $this->request->data['contactNumber'] : '';
            $dob = isset($this->request->data['dob']) ? $this->request->data['dob'] : '0000-00-00';
            $joiningDate = isset($this->request->data['joiningDate']) ? $this->request->data['joiningDate'] : '';
            $gmcNumber = isset($this->request->data['gmcNumber']) ? $this->request->data['gmcNumber'] : '';
            $userStatus = isset($this->request->data['userStatus']) ? $this->request->data['userStatus'] : 0;
            $address = isset($this->request->data['address']) ? $this->request->data['address'] : '';
            $city = isset($this->request->data['city']) ? $this->request->data['city'] : '';
            $county = isset($this->request->data['county']) ? $this->request->data['county'] :'';
            $country = isset($this->request->data['countryList']) ? $this->request->data['countryList'] : '';
            $education = isset($this->request->data['education']) ? $this->request->data['education'] : '';
            $userProfession = isset($this->request->data['userProfession']) ? $this->request->data['userProfession'] : 0;
            $interests = isset($this->request->data['interests']) ? $this->request->data['interests'] : array(); 
            $specilities = isset($this->request->data['specilities']) ? $this->request->data['specilities'] : array(); 
            $userData = array(
                                "UserProfile.first_name"=> "'". $firstName ."'",
                                "UserProfile.last_name"=> "'". $lastName ."'",
                                "UserProfile.contact_no"=> "'". $contactNumber ."'",
                                "UserProfile.dob"=> "'". date('Y-m-d', strtotime($dob)) ."'",
                                "User.registration_date"=> "'". date('Y-m-d H:i:s', strtotime($joiningDate)) ."'",
                                "UserProfile.gmcnumber"=> "'". $gmcNumber ."'",
                                "User.status"=> $userStatus,
                                "UserProfile.address"=> "'". $address ."'",
                                "UserProfile.city"=> "'". $city ."'",
                                "UserProfile.county"=> "'". $county ."'",
                                "UserProfile.country_id"=> $country,
                                "UserProfile.profession_id"=> $userProfession,
                            );
            $updateConditions = array("User.id"=> $userId);
            $updateUser = $this->User->updateAll($userData, $updateConditions);
            if($updateUser){
                $msg = "User Updated.";
            }else{
                $msg = "Something going wrong! Try Again.";
            }
            //** Edit user specilities [START]
                if(!empty($specilities)){
                    //** At first unmark all specilities of user
                    $updateData = $this->UserSpecilities->updateAll( array('status'=> 0), array('user_id'=> $userId));
                    //** Update One by One specility
                    foreach($specilities as $specilitiesId){ 
                        $specilitiesData = array('user_id'=> $userId , 'specilities_id'=> $specilitiesId);
                        if( $this->UserSpecilities->find("count", array("conditions"=> array(array('user_id'=> $userId, 'specilities_id'=> $specilitiesId)))) == 0 ){
                            $saveData = $this->UserSpecilities->saveAll( $specilitiesData );
                        }else{
                            $updateData = $this->UserSpecilities->updateAll( array('status'=> 1), array('user_id'=> $userId, 'specilities_id'=> $specilitiesId) );
                        }
                    }
                }
            //** Edit user specilities [END]

            //** Edit user interests [START]
            if(!empty($interests)){
                //** update all Interests
                        $this->UserInterest->updateAll(array("status"=>0), array('user_id'=> $userId));
                        //** add or update interest one by one according to request
                        foreach( $interests as $interestId ){  //echo "<pre>";print_r($interestId);die;
                            $interestData = array('user_id'=> $userId,'interest_id'=> $interestId, 'status'=> 1 );
                            if($this->UserInterest->find("count", array("conditions"=>array('user_id'=> $userId,'interest_id'=> $interestId))) ==0 ){
                                $this->UserInterest->saveAll( $interestData );
                            }else{
                                $this->UserInterest->updateAll(array("status"=> 1), array('user_id'=> $userId,'interest_id'=> $interestId));
                            }
                        }
                }
            //** Edit user interests [END]
        }else{
            $msg = "User Not Found!";
        }
        echo $msg;
        exit;
    }
    /*
    ----------------------------------------------------------------------------------------
    On: 
    I/P: 
    O/P:
    Desc: Reset any user's password
    ----------------------------------------------------------------------------------------
    */
    public function resetUserPassword(){
        if(isset($this->request->data['userId']) && !empty($this->request->data['userId'])){
            $userId = $this->request->data['userId'];
            $currentPassword = $this->request->data['currentPassword'];
            $newPassword = $this->request->data['newPassword'];
            $getUserDetails = $this->User->findById($userId);
            if($getUserDetails['User']['password'] == md5($currentPassword)){
                $updateData = array(
                        "User.password"=> "'". md5($newPassword) ."'"
                    );
                $updateDataConditions = array(
                        "User.id"=> $userId
                    );
                $resetPassword = $this->User->updateAll($updateData, $updateDataConditions);
                if($resetPassword){
                    $msg = "Password Updated";
                }else{
                   $msg = "Some error occured! Try again"; 
                }
            }else{
                $msg = "Current Password not match";
            }
        }else{
            $msg = "Userid Blank";
        }
        echo $msg;
        exit;
    }

    /*
    ---------------------------------------------------------------------------------------
    On: 
    I/P: 
    O/P: 
    Desc: 
    ---------------------------------------------------------------------------------------
    */
    public function updateUserProfileImage(){
        $userId = $this->request->data['hiddenUserId'];
        //echo "<pre>"; print_r($_FILES);
        $fileBaseName = basename($_FILES['profileImg']['name']);
        $imgNameArr = explode('.', $fileBaseName);
        $imgExt = end($imgNameArr);
        $imageName = md5($userId .'_' . strtotime(date('Y-m-d H:i:s')) ) . '.' . $imgExt;
        $tempFileDestination = WWW_ROOT . 'img/uploader_tmp/';
        $uploadFileToTemp = $tempFileDestination . $imageName;
        //** Move file to temp destination
        if (move_uploaded_file($_FILES['profileImg']['tmp_name'], $uploadFileToTemp)) {
            if( !empty($imageName) ){
                $this->Image->moveImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$imageName, 'img_name'=> $userId . '/profile/' .$imageName));
            }
            //** Update Image name on table
            $profileData = array("UserProfile.profile_img"=> "'".$imageName."'");
            $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $userId));
            //** Remove physical file from Temp Directory
            $this->Common->removeTempFile( WWW_ROOT . 'img/uploader_tmp/' . $imageName );
        } else {
            $msg = "Some error occured! Try Again.";
        }   
        $this->redirect('editUser/' . $userId);
    }
    /*
    ---------------------------------------------------------------------------------------
    On: 27-03-2017
    I/P: 
    O/P: 
    Desc: get user front end info
    ---------------------------------------------------------------------------------------
    */
    public function getFrontServerInfo(){
        $userServerInfo = json_encode($_SERVER);
        return $userServerInfo;
    }
}