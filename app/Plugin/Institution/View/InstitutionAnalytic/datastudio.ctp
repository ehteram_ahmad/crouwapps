<style type="text/css" media="screen">
	.loader {
	    position: fixed;
	    left:0px;
	    top: 0px;
	    width: 100%;
	    height: 100%;
	    z-index: 9999;
	    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
	}
	iframe{
		width: 100%;
		min-height: 800px;
		max-height: 1200px;
		padding-bottom: 10px;
		border:0
	}
	.nav_btn{
		margin-bottom: 20px;
		text-align: center;
	}
</style>
<div class="loader" style="display: none;"></div>
<div class="row" id="ajaxContent">
	<div class="col-md-12">
		<iframe src="<?php echo $url; ?>" frameborder="0" ></iframe>
	</div>
	<div class="col-md-12 nav_btn">
		<button class="btn btn-success switchPage" data-value="1" type="button">Page 1</button>
		<button class="btn btn-info switchPage" data-value="2" type="button">Page 2</button>
		<button class="btn btn-info switchPage" data-value="3" type="button">Page 3</button>
	</div>
</div>
<script type="text/javascript">
	$("#sidebarnav>li#menu8").addClass('active').find('a').addClass('active');
	$("#sidebarnav>li#menu8").find("li").eq(0).addClass('active').find('a').addClass('active');
	$(document).ready(function(){
		$('body').on('click','.switchPage',function(){
			var page_no = $(this).data('value');
			if($('.switchPage[data-value="'+page_no+'"]').hasClass('btn-success')){
				return false;
			}
			$('.loader').fadeIn('fast');
			$.ajax({
			  url:'<?php echo BASE_URL; ?>institution/InstitutionAnalytic/dataStudioFilter',
			  type: "POST",
			  data: {'page_number': page_no},
			  success: function(data) {
			  	$('.loader').fadeOut('fast');
			    $('#ajaxContent').html(data);
			  }
			});
		});
	});
</script>
