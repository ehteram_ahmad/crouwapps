<?php echo $this->Html->script(
		array(
				'Institution.powerbi'
		)
); ?>
<style type="text/css" media="screen">
	body{
		/* position: fixed; */
	  width: 100%
	}
	#reportstatic{
		min-height: 500px;
		height: 600px;
		max-height: 2000px;
		/* margin-right: -200px; */
	}
	iframe{
		width: 100%;
		padding-bottom: 10px;
		border:0
	}
</style>
<div id="reportstatic" style="height: 635px;"></div>
<script type="text/javascript">
	$("#sidebarnav>li#menu8").addClass('active').find('a').addClass('active');
	$("#sidebarnav>li#menu8").find("li").eq(1).addClass('active').find('a').addClass('active');
	$('body').on('mousemove','#reportstatic iframe',function(){
		// $('body').css('position', 'absolute');
	});
	$(function () {
	  var models = window['powerbi-client'].models;
	  var $staticReportContainer = $('#reportstatic');
	  var staticReport;
	  var staticReportUrl = '<?php echo $main_url; ?>';
	  var repost_utl = '<?php echo $sub_url; ?>';
	  fetch(staticReportUrl)
	    .then(function (response) {
	      if (response.ok) {
	        return response.json()
	          .then(function (embedConfig) {
	            var token_val = embedConfig.key;
	            fetch(repost_utl, {
	               method: 'get',
	               headers: new Headers({
	                 'Authorization': 'Bearer '+token_val,
	                 'Content-Type': 'application/x-www-form-urlencoded'
	               })
	             })
	             .then(function (report_details) {
	               report_details.json().then(function(final){
	                 var config = {
	                   type: 'report',
	                   uniqueId: final.uniqueId,
	                   embedUrl: final.embedUrl,
	                   accessToken: token_val,
										 settings: {
											 filterPaneEnabled: false
										 },
										 Permissions:0
	                 };
	                 var report = powerbi.embed($staticReportContainer.get(0),config);
	               });
	             });
	          });
	      }else {
	        return response.json()
	          .then(function (error) {
	            throw new Error(error);
	          });
	      }
	    });
	});
</script>
