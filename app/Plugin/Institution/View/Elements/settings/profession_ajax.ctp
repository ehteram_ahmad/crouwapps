<?php
if(!empty($data)){ ?>
  <table class="table stylish-tableNew">
      <thead>
        <tr>
          <th style="width:45%; padding-left:20px">Profession</th>
          <th style="width:20%; text-align:center;">Status</th>
          <th style="width:15%; text-align:center;">Edit</th>
          <th style="width:20%;">Actions</th>
        </tr>
      </thead>
      <tbody>
          <?php
            foreach ($data as $key => $list) {
          ?>
          <tr data-target="<?php echo "#".$list['id']; ?>">

              <td onclick="showData('<?php echo "#".$list['id']; ?>');" style="padding-left:20px"><?php echo $key." (".count($list['tags']).")"; ?></td>
              <td style="text-align:center;">
                <?php
                    if($list['status'] == 1){ ?>
                        <button onclick="changeStatus('<?php echo $list['id']; ?>',0,'','status');" type="button" class="btn btn-primary"> Active</button>
                 <?php }else{ ?>
                        <button onclick="changeStatus('<?php echo $list['id']; ?>',1,'','status');" type="button" class="btn btn-outline-primary">Inactive</button>
                <?php } ?>
              </td>
              <td style="text-align:center;" class="editBlock_main">
                <img data-toggle="modal" data-target="#updateProf" data-id="<?php echo $list['id']; ?>" data-name="<?php echo $key; ?>" style="width: 15px;" src="<?php echo BASE_URL ?>institution/img/edit.svg" alt="">
              </td>
              <td onclick="location.href='<?php echo BASE_URL.'institution/InstitutionSetting/professionDetail/'.$list['id']; ?>'">
                <button class="btn btn-outline-primary" type="button" name="button">Details</button>
                <span class="openSpan"><i class="demo-icon icon-angle-right"></i></span>
              </td>
          </tr>
          <tr id="<?php echo $list['id']; ?>" class="collapse active transition_div">
            <td colspan="8">
              <div style="padding-left: 0;padding-right:0px;" class="col-12 panel-body">
                    <!-- <div class="card"> -->
                        <!-- <div class="card-block padTop30 padBottom0"> -->
                            <div class="table-responsive wordBreak panel-body">
                                <table class="table stylish-tableNew panel-body">
                                    <tbody>
                                        <?php
                                            foreach( $list['tags'] as $tagKey => $user){
                                        ?>
                                        <tr class="panel-body">
                                            <td style="width:45%;"><?php echo $tagKey." (".count($user).")"; ?></span></td>
                                            <td style="width:20%;"><button onclick="location.href='<?php echo BASE_URL.'institution/InstitutionSetting/professionDetail/'.$list['id'].'?action='.$tagKey; ?>'" class="btn btn-outline-primary" type="button" name="button">Details</button></td>
                                            <!-- <td style="width:35%;text-align:left;" class="editBlock">
                                              <img style="width: 15px;" src="<?php //echo BASE_URL ?>institution/img/edit.svg" alt="">
                                            </td> -->
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                          <!-- </div> -->
                        <!-- </div> -->
                    </div>
                </div>
            </td>
          </tr>
          <?php }?>
      </tbody>
  </table>
      <?php
        if(!empty($data)){
          echo '<div class="actions marBottom10 paddingSide15 leftFloat98per">';
            echo $this->element('pagination');
          echo "</div>";
        }
}else{ ?>
  <span class="is-hidden" id="report_count"><?php echo isset($totalCount)?$totalCount:0; ?></span>
  <table class="table stylish-tableNew">
      <thead>
        <th style="width:45%; padding-left:20px">Profession</th>
        <th style="width:20%; text-align:center;">Status</th>
        <th style="width:15%; text-align:center;">Edit</th>
        <th style="width:20%;">Actions</th>
      </thead>
      <tbody>
        <tr>
            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
      </tbody>
  </table>
<?php }
?>
