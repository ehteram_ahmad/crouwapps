<div class="custom_divs">
  <div class="data-cell">
<?php if(!empty($data)){
  foreach ($data as $value) { ?>

        <div class="lb-left">
            <div class="row" style="width:100%;margin-left:0px !important;">
                <div class="col-3 align-self-center img-div">
                    <span class="round <?php echo $value['cust_class']; ?>" onclick="userProfile($(this), <?php echo $value['user_id']; ?>);">
                        <img 'width'='50' 'height'='50' src="<?php echo $value['profile_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                    </span>
                </div>
                <div class="col-9 align-self-center">
                  <span class="data_name"><?php echo $value['user_name']; ?></span>
                  <span class="data_status"><?php echo $value['user_role']; ?></span>
                  <span class="data_email"><?php echo $value['user_email']; ?></span>
                </div>
                <div class="col-12">
                  <span class="data_role">"<?php echo $value['role_name']; ?>"</span>
                </div>
            </div>
        </div>
<?php }}else{ ?>
  <div class="mainBatonList">
    <span style="color:red;font-size:16px;">No Record(s) Found!</span>
  </div>
<?php } ?>
</div>
</div>
<script type="text/javascript">
  var element = $('.lb-left');
  console.log(element.css('width'));
  console.log(element.length);
  $('.custom_divs .data-cell').css('width',(parseInt(element.css('width')) * element.length + 50)+'px');
</script>
