<?php

// print_r($data['baton_list']);

// exit;
?>
<table class="table stylish-table customPading lineHeight36">
    <thead>
        <tr>
            <th>Baton Roles </th>
            <th>On Call </th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
      <?php
          if(isset($data['baton_list']) && count($data['baton_list']) > 0){
          foreach( $data['baton_list'] as $baton){
      ?>
      <tr>
          <td style="width:25%;"><?php echo $baton['name']; ?></td>
          <td style="width:15%;">
            <?php echo ($baton['on_call_value'] == 1)?'<span class="green">Yes</span>':'<span class="red">No</span>'; ?>
          </td>
          <td class="statusBtn" style="width:20%;">
            <?php if($baton['is_occupied'] == 1){ ?>
              <span style="line-height: 24px!important;" onclick="userProfile($(this), <?php echo $baton['active_user_id']; ?>);">
                <?php if($baton['role_status'] != '1'){ ?>
                  <?php echo $baton['active_user_name']; ?>
                  <span>(Assigned)</span><br style="margin-bottom: 10px;">
                  <?php echo $baton['user_name']; ?>
                  <?php if($baton['role_status'] != 1){ ?>
                    <span>(Request pending)</span>
                  <?php } ?>
                <?php }else{ ?>
                  <?php echo $baton['active_user_name']; ?>
                <?php } ?>
              </span>
              <!-- <button type="button" disabled="disabled" class="btn btn-primary-gray2"> Assigned</button> -->
            <?php }else{ ?>
              <button type="button" data-toggle="modal" data-oncall="<?php echo $baton['on_call_value']; ?>" data-roleid="<?php echo $baton['id']; ?>" data-target="#userAssignPopUp" class="btn btn-primary2" onclick="assignBatonPopUpFunc('<?php echo $baton['name']; ?>')"> Assign</button>
            <?php } ?>
          </td>
          <td style="width:10%;">
            <div class="btn-group">
              <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ti-settings"></i>
              </button>
              <div class="dropdown-menu">
                <?php if($baton['is_occupied'] == 1){ ?>
                  <a class="dropdown-item" href="javascript: void(0);" onclick="unassignBatonRole('<?php echo $baton['name']; ?>','<?php echo $baton['id']; ?>','<?php echo $baton['active_user_id']; ?>');" data-toggle="modal" data-target="#unassignBatonPopup">Unassign</a>
                <?php } ?>
                <?php /* <a data-toggle="modal" data-target="#editBatonPopup" class="dropdown-item" href="" data-rolename="<?php echo $baton['name']; ?>" data-roleid="<?php echo $baton['id']; ?>" data-roleoncall="<?php echo ($baton['on_call_value'] == 1)?'Yes':'No'; ?>">Edit</a> */ ?>
                <a class="dropdown-item" href="javascript: void(0);" onclick="editBatonRole('<?php echo $baton['name']; ?>','<?php echo $baton['id']; ?>','<?php echo ($baton['on_call_value'] == 1)?'Yes':'No'; ?>','<?php echo $baton['active_user_id'] ? $baton['active_user_id']:0; ?>');">Edit</a>
                <a class="dropdown-item"  href="javascript: void(0);" onclick="deleteBatonRole('<?php echo $baton['name']; ?>','<?php echo $baton['id']; ?>','<?php echo $baton['active_user_id'] ? $baton['active_user_id']:0; ?>');" data-toggle="modal" data-target="#deleteBatonPopup">Delete</a>
              </div>
            </div>
          </td>
      </tr>
    <?php }}elseif(isset($tCount) && $tCount > 0){?>

    <tr>
       <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
    </tr>
    <?php }else{ ?>
    <tr>
       <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
    </tr>
    <?php  } ?>
    </tbody>
</table>
<span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<span id="tCountAssigned" class="pull-right" style="display:none;"><?php echo $tCountAssigned; ?></span>
<span id="totalUnassigned" class="pull-right" style="display:none;"><?php echo $totalUnassigned; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">

<?php
    if(count($data['baton_list']) > 0){
        echo $this->element('pagination');
    }
?>
</div>
