<?php
   if(!empty($data)){
     ?>
     <div class="row">
         <div class="col-4 lb-left">
             <div class="row">
                 <div class="col-4 align-self-center">
                     <span class="round <?php echo $data['cust_class']; ?>" onclick="userProfile($(this), <?php echo $data['user_id']; ?>);">
                         <img 'width'='50' 'height'='50' src="<?php echo $data['profile_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                     </span>
                     <div class="lb-arrow <?php if($data['type'] == '4'){ echo 'arrow-transfer'; } ?>">
                         <?php echo $this->Html->image('Institution.arrow-blue-right.svg'); ?>
                     </div>
                 </div>
                 <div class="col-8 align-self-center">
                     <h4><?php echo $data['user_name']; ?></h4>
                     <?php if (strlen($data['user_role']) > 30){
                       echo substr($data['user_role'], 0, 27) . '...';
                     }else{
                       echo $data['user_role'];
                     } ?>
                     <?php // echo $data['user_role']; ?><br>
                     <?php echo $data['user_email']; ?>
                 </div>
             </div>
         </div>
         <div class="col-8 align-self-center">
             <p>“<?php echo $data['user_name']; ?>” send a <strong>“<?php echo $data['role_name']; ?>”</strong> <?php
                        if($data['type'] == '2'){
                          echo 'Take On';
                        }else if($data['type'] == '4'){
                          echo 'Transfer';
                        }
                       ?> baton role request. Are you sure you want to accept/reject the request?</p>
             <div class="lb-btns">
               <span class="user_note" style="display:none;"><?php echo $data['notes']; ?></span>
               <div id="lt-bt-notes" style="display: none;"><?php echo $data['notes']; ?></div>
               <?php if($data['type'] == '2'){ ?>
                 <button class="btBatonAccept btn btn-success waves-effect" type="button" onclick="changeRequestFinal('<?php echo $data['user_id']; ?>',1,'<?php echo $data['role_id']; ?>');" name="button">Accept</button>
                 <button type="button" onclick="btBatonReject();" data-type="request" data-user="<?php echo $data['user_id']; ?>" data-role="<?php echo $data['role_id']; ?>" id="btBatonRejectButton" class="btn btn-info waves-effect">Reject</button>
              <?php }else if($data['type'] == '4'){ ?>
                <button class="btBatonAccept btn btn-success waves-effect" type="button" onclick="changeTransferRequestFinal('<?php echo $data['user_id']; ?>',1,'<?php echo $data['role_id']; ?>');" name="button">Accept</button>
                <button type="button" onclick="btBatonReject();" data-type="transfer" data-user="<?php echo $data['user_id']; ?>" data-role="<?php echo $data['role_id']; ?>" id="btBatonRejectButton" class="btn btn-info waves-effect">Reject</button>
              <?php } ?>
                 <!-- <button type="button" class="btn btn-success waves-effect" onclick="">Accept</button> -->
                 <!-- <button type="button" class="btn btn-info waves-effect">Reject</button> -->
             </div>
         </div>
     </div>
     <?php
   }else{ ?>
     <div class="row">
         <div class="col-md-12" style="text-align: center;">
              <font color="red">No Request Pending</font>
         </div>
     </div>
<?php
  }
 ?>
