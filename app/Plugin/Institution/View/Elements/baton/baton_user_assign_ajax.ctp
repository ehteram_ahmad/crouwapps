<?php
if(isset($data['user_list']) && count($data['user_list']) > 0){
  foreach( $data['user_list'] as $userRequest){ ?>
  <li class="assignUser" data-user="<?php echo $userRequest['id']; ?>">
    <div class="mainBatonList <?php echo $userRequest['cust_class']; ?>">
      <div class="leftBatonList">
        <img src="<?php echo $userRequest['profile_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
      </div>
      <div class="rightBatonList">
        <div class="titleBatonList"><?php echo $userRequest['UserName']; ?></div>
        <div class="emailBatonList"><?php echo $userRequest['email']; ?></div>
      </div>
    </div>
  </li>
<?php }}else{ ?>
  <li>
    <div class="mainBatonList">
      <span style="color:red;margin-left: 33%;">No Record(s) Found!</span>
    </div>
  </li>
<?php } ?>
