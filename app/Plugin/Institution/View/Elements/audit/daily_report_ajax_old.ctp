<?php
if(!empty($data)){ ?>
      <div class="col-12">
          <div class="card">
              <div class="card-block padTop30 padBottom0">
                <div class="tableTop floatLeft">
                  <span class="card-title">Summary</span>
                </div>
                  <div class="table-responsive wordBreak">
                      <table class="table stylish-table customPading">
                          <thead>
                            <tr>
                                <th>Base Location </th>
                                <th>Total Count </th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php
                                  foreach ($data as $list) {
                              ?>
                              <tr>
                                  <td><?php echo $list['key']; ?></span></td>
                                  <td><?php echo $list['count']; ?></td>
                              </tr>
                              <?php }?>
                          </tbody>
                      </table>
                </div>
              </div>
          </div>
      </div>
      <?php
    foreach ($data as $value) { ?>
        <div class="col-4">
          <div class="card">
              <div class="card-block padTop30 padBottom0">
                <div class="tableTop floatLeft">
                  <span class="card-title"><?php echo $value['key']; ?> : </span>
                  <span class="display-6"><?php echo $value['count']; ?></span>
                </div>
                  <div class="table-responsive wordBreak">
                      <table class="table stylish-table customPading">
                          <thead>
                            <tr>
                                <th>Profession </th>
                                <th>Count </th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php
                                  foreach( $value['list'] as $user){
                              ?>
                              <tr>
                                  <td><?php echo $user['profession']; ?></span></td>
                                  <td><?php echo $user['count']; ?></td>
                              </tr>
                              <?php }?>
                          </tbody>
                      </table>
                </div>
              </div>
          </div>
      </div>
<?php
  }
}
?>
