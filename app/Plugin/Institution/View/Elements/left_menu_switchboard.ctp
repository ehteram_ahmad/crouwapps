<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" style="position: fixed;">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar switchboard-sidebar">
        <!-- User profile -->

        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <div class="lm-switchboad">
                <?php echo $this->Html->image('Institution.switchboard-icon.svg',array('class'=>'switchboard-icon')); ?>
                <span style="margin-left: 3px;" class="hide-menu">Switchboard</span>
            </div>
            <ul id="sidebarnav">
                <li id="menu1">
                    <a href="<?php echo BASE_URL.'institution/Switchboard'; ?>" aria-expanded="false">
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li id="menu2" class="is-hidden">
                    <a href="<?php echo BASE_URL.'institution/Switchboard/getRequest'; ?>" aria-expanded="false">
                        <span class="title">Pending Baton Role Request</span>
                        <?php
                          $custBatonClass = 'none';
                          if(isset($baton_count) && $baton_count > 0){
                            $custBatonClass = 'inline-block';
                          }
                        ?>
                        <span id="baton_request_count_latest" class="badge badge-successNew2 batonBadgeCount" style="display:<?php echo $custBatonClass; ?>"><?php echo $baton_count; ?></span>
                    </a>
                </li>
                <li id="menu4">
                    <a href="<?php echo BASE_URL.'institution/Switchboard/manageBatonRoles'; ?>" aria-expanded="false">
                        <span class="hide-menu">Manage Baton Roles</span>
                    </a>
                </li>
                <li id="menu4">
                    <a href="<?php echo BASE_URL.'institution/Switchboard/generateQrCode'; ?>" aria-expanded="false">
                        <span class="hide-menu">Generate QR Code</span>
                    </a>
                </li>
            </ul>
            <?php if($_COOKIE['adminRoleId']!='3'){ ?>
              <a href="<?php echo BASE_URL.'institution/InstitutionHome/index'; ?>" class="backadmin-link">
                <?php echo $this->Html->image('Institution.back-icon.svg',array('class'=>'backadmin-icon')); ?>
                Back to Trust Admin
              </a>
            <?php } ?>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
