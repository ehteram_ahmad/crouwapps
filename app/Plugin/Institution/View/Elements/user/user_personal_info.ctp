<?php if($user_active == true){ ?>
  <div class="row">
    <span class="is-hidden" id="userApprovedStatus" data-status="<?php echo $status; ?>"></span>
    <div class="col-md-3">
      <span class="round <?php echo $duty_status; ?>">
        <img width="165" height="165" src="<?php echo $user_img; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
      </span>
      <span class="login_status">
      </span>
      <span id="change_password" onclick="editMode('password','','');">
        Reset Password
      </span>
    </div>
    <div class="col-md-9">
      <h3 class="label-sub-div editable" onclick="editMode('name','<?php echo addslashes($user_first_name)?>','<?php echo addslashes($user_last_name); ?>');">
        <?php echo $user_first_name.' '.$user_last_name; ?>
        <span class="fa fa-pencil editPencil"></span>
      </h3>
      <div class="label-content main-div">
        <div class="label-sub-div">
          <?php if(isset($dnd_active) && $dnd_active == 1){ ?>
            <span class="label-dnd">Do Not Disturb: <?php echo $dnd_name; ?></span>
          <?php }else{ ?>
            <span class="label-notactive">Do Not Disturb: Not Active</span>
          <?php } ?>
        </div>
        <div class="label-sub-div">
          <span class="label-head">Base Location:</span>
          <span class="label-value"><?php
           echo $baselocation; 
           ?></span>
        </div>
        <div class="label-sub-div">
          <span class="label-head">Profession:</span>
          <span class="label-value"><?php echo $profession; ?></span>
        </div>
        <div class="label-sub-div">
          <span class="label-head">GMC Number:</span>
          <span class="label-value"><?php
            if(strtolower($profession) == 'doctor'){
              if($gmc_number == ''){
                echo 'Not Provided';
              }else{
                echo $gmc_number;
              }
            }else{
              echo 'Not Applicable';
            }
          ?></span>
        </div>
        <!-- <div class="label-sub-div editable" onclick="editMode('email','<?php //echo addslashes($user_email); ?>');"> -->
        <div class="label-sub-div">
          <span class="label-head">Email:</span>
          <span class="label-value"><?php echo $user_email; ?></span>
          <!-- <span class="fa fa-pencil editPencil"></span> -->
        </div>
        <div class="label-sub-div editable" onclick="editMode('phone','<?php echo $phone; ?>');">
          <span class="label-head">Phone:</span>
          <span class="label-value"><?php echo $phone; ?></span>
          <span class="fa fa-pencil editPencil"></span>
        </div>
      </div>
    </div>
  </div>
<?php }else{ echo "User Deleted"; } ?>
