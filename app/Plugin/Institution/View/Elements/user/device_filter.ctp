<?php
// print_r($data);
// exit();
 ?>
<table class="table stylish-table customPading">
    <thead>
        <tr>
            <th style="width:15%; padding-left:20px;">Date</th>
            <th style="width:12%; text-align:center;">Logout/Login</th>
            <th style="width:13%; text-align:center;">Device Model</th>
            <th style="width:20%; text-align:center;">Device Version</th>
            <th style="width:20%; text-align:center;">Platform</th>
            <th style="width:20%; text-align:center;">Mb Version</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(isset($data['user_list']) && count($data['user_list']['UserLoginTransaction']) > 0){
            foreach( $data['user_list'] as $user){
              // print_r($user);
                $app = array();
                $app['device'] = '--';
                $app['mb_version'] = '--';
                $app['type'] = '--';
                $app['platform'] = '--';
                $app['status'] = '--';

                foreach ($user as $value) {
                    if(!empty($value)){
                      $app['type'] = isset($value["type"]) ? $value["type"] : '--';
                      $app['device'] = isset($value['device']) ? $value['device'] : '--';
                      $app['platform'] = isset($value['platform']) ? $value['platform'] : '--';
                      $app['mb_version'] = isset($value['mb_version']) ? $value['mb_version'] : '--';
                      $app['status'] = isset($value['status']) ? ($value['status'] == '1') ? '<span style="color:#00a777;">Login</span>' : '<span style="color:#d0021b;">Logout</span>' : '--';

                      $user_country_id = $_COOKIE['adminCountryId'];
                      date_default_timezone_set('UTC');
                      
                      $date_cust = isset($value['login_date']) ? strtotime($value['login_date']) : '--';

                      if($user_country_id==226){
                          // date_default_timezone_set('Europe/Amsterdam');
                          date_default_timezone_set("Europe/London");
                      }else if($user_country_id==99){
                          date_default_timezone_set('Asia/Kolkata');
                      }
        ?>
        <tr>
            <td style="width:15%; padding-left:20px;"><?php echo date("d-m-Y H:i", $date_cust); ?></td>
            <td style="width:12%; text-align:center;"><?php echo $app['status']; ?></td>
            <td style="width:13%; text-align:center;"><?php echo $app['type']; ?></td>
            <td style="width:20%; text-align:center;"><?php echo $app['platform']; ?></td>
            <td style="width:20%; text-align:center;"><?php echo $app['device']; ?></td>
            <td style="width:20%; text-align:center;"><?php echo $app['mb_version']; ?></td>
        </tr>
      <?php }}}}else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
<span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">
<?php
  if(isset($section) && $section == 'all'){
    if(isset($data['user_list']) && count($data['user_list']['UserLoginTransaction']) > 0){
        echo $this->element('pagination');
    }
  }else if(isset($data['user_list']) && count($data['user_list']['UserLoginTransaction']) > 0){ ?>
    <div class="card-note">
      <label>Note</label>
      <div class="note-des">
        Click on view all button, to view list of User’s Login & Logout History.
      </div>
    </div>
    <button type="button" onclick="window.location.href='<?php echo BASE_URL.'institution/InstitutionUser/deviceInfo/'.$user_id; ?>'" name="button" class="btn btn-success">View All</button>
  <?php } ?>
</div>
