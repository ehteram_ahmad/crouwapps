<?php
  if(isset($data['user_list']) && count($data['user_list']) > 0){
    $login = 0;
    foreach( $data['user_list'] as $user){

      $app = $web = array();
      $web['device'] = $app['device'] = '--';
      $web['mb_version'] = $app['mb_version'] = '--';
      $web['type'] = $app['type'] = '--';
      $web['platform'] = $app['platform'] = '--';

      foreach ($user as $value) {
        if(!empty($value)){
          if((string)$value['status'] == '1'){
            $login = 1;
            if($value['type'] == 'Web'){
                $web['type'] = $value['type'];
                $web['status'] = (string)$value['status'] == '1'?'Active':'Not active';
                $web['device'] = $value['device'];
                $web['platform'] = $value['platform'];
                $web['mb_version'] = $value['mb_version'];
            }else{
                $app['type'] = $value["type"];
                $app['status'] = (string)$value['status'] == '1'?'Active':'Not active';
                $app['device'] = $value['device'];
                $app['platform'] = $value['platform'];
                $app['mb_version'] = $value['mb_version'];
            }
          }
        }
      }
    }
  }
?>
<table class="table stylish-table customPading" data-login="<?php echo $login; ?>">
  <thead>
    <tr>
      <th>Currently Logged In in Device Info</th>
      <th>Web Device</th>
      <th>Mobile Device</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="width:20%;">Platform</td>
      <td style="width:15%;"><?php echo $web['device']; ?></td>
      <td style="width:15%;"><?php echo $app['device']; ?></td>
    </tr>
    <tr>
      <td style="width:20%;">Device</td>
      <td style="width:15%;"><?php echo $web['platform']; ?></td>
      <td style="width:15%;"><?php echo $app['platform']; ?></td>
    </tr>
    <tr>
      <td style="width:20%;">Device Model</td>
      <td style="width:15%;"><?php echo $web['type']; ?></td>
      <td style="width:15%;"><?php echo $app['type']; ?></td>
    </tr>
    <tr>
      <td style="width:20%;">MB Version</td>
      <td style="width:15%;"><?php echo $web['mb_version']; ?></td>
      <td style="width:15%;"><?php echo $app['mb_version']; ?></td>
    </tr>
  </tbody>
</table>
