<?php
// print_r($data);
// exit();
 ?>
<table class="table stylish-table customPading">
    <thead>
        <tr>
          <th>Photo</th>
          <th>Name</th>
          <th>Email</th>
          <th>Admin Role</th>
          <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(isset($data['user_list']) && count($data['user_list']) > 0){
            foreach( $data['user_list'] as $user){
        ?>
        <tr>
          <td style="width:5%;"><span class="round">
            <?php //echo strtoupper(substr($user['AdminUser']['email'],0,1)); ?>
            <img onclick="location.href='<?php echo BASE_URL.'institution/InstitutionUser/userInfo/'.$user['User']['id']; ?>'" 'width'='50' 'height'='50' src="<?php echo BASE_URL ?>institution/img/ava-single.png" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
          </span></td>
            <td style="width:20%;cursor:pointer;" onclick="location.href='<?php echo BASE_URL.'institution/InstitutionUser/userInfo/'.$user['User']['id']; ?>'"><?php echo $user['userProfiles']['first_name'].' '.$user['userProfiles']['last_name']; ?></td>
            <td style="width:25%;"><?php echo $user['AdminUser']['email']; ?></td>
            <td style="width:10%;">
              <?php if($user['AdminUser']['role_id']==1){ ?>
                        <!-- <button onclick="changeStatus('<?php //echo $user['email']; ?>',0);" type="button" class="btn btn-primary"> Unassign </button> -->
                        Subadmin
              <?php }else if($user['AdminUser']['role_id']==3){ ?>
                        Switchboard
              <?php }else{ ?>
                        <!-- <button onclick="changeStatus('<?php //echo $user['email']; ?>',1);" type="button" class="btn btn-outline-primary"> Assign </button> -->
                        Admin
              <?php }
                ?>
            </td>
            <td style="width:5%;">
                <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-settings"></i>
                    </button>
                    <div class="dropdown-menu pad0">
                        <!-- <a onclick="removeUser('<?php //echo $user['AdminUser']['email']; ?>')" class="dropdown-item" href="javascript:void(0)">Remove User</a> -->
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#privilegePopUp" data-whatever="@mdo" data-useremail="<?php echo $user['AdminUser']['email']; ?>" data-userid="<?php echo $user['User']['id']; ?>">Manage Privilege</a>
                    </div>
                </div>
            </td>
        </tr>
        <?php }}elseif(isset($tCount) && $tCount > 0){?>

        <tr>
           <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
        </tr>
        <?php }else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
<span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">
<?php
    if(isset($data['user_list']) && count($data['user_list']) > 0){
        echo $this->element('pagination');
    }
?>
</div>
