<table class="table stylish-table customPading">
    <thead>
        <tr>
            <th class="selectiveCheck" style="width:1%; display: none;">
                <!-- <input type="checkbox" id="selectAll">Select all -->
                <span id="selectAll">
                    All
                </span>
            </th>
            <th style="width:5%;">Photo </th>
            <th style="width:20%;" class="sort" data-sort="name"><span>Name</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
            <th style="width:25%;" class="sort" data-sort="email"><span>Email</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
            <th style="width:15%;" class="sort" data-sort="profession"><span>Profession</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
            <th style="width:15%;" class="sort" data-sort="registration_date"><span>Registration Date</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
            <th style="width:15%;" class="sort" data-sort="expiry_date"><span>Subscription Expiry Date</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
            <th style="width:5%;">User Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(isset($data['user_list']) && count($data['user_list']) > 0){
            foreach( $data['user_list'] as $user){
        ?>
        <tr id="<?php echo $user['id']; ?>" data-email="<?php echo $user['email']; ?>">
            <td class="selectiveCheck" style="display: none;">
                <input type="checkbox" name="">
            </td>
            <td>
                <span class="round <?php echo $user['cust_class']; ?>" onclick="location.href='<?php echo BASE_URL.'institution/InstitutionUser/userInfo/'.$user['id']; ?>'">
                  <img 'width'='50' 'height'='50' src="<?php echo $user['profile_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                </span>
            </td>
            <td style="cursor:pointer;" onclick="location.href='<?php echo BASE_URL.'institution/InstitutionUser/userInfo/'.$user['id']; ?>'"><?php echo $user['UserName']; ?></td>
            <td><?php echo $user['email']; ?></td>
            <td><?php echo $user['profession_type']; ?></td>
            <td><?php echo date('d M Y', strtotime($user['registration_date'])); ?></td>
            <td><?php echo date('d M Y', strtotime($user['SubscriptionExpiryDate'])); ?></td>
            <td class="statusBtn">
                <?php
                    if($user['status'] == 1){ ?>
                        <button onclick="changeStatus('<?php echo addslashes($user['email']); ?>', '<?php echo $user['id']; ?>',0);" type="button" class="btn btn-primary"> Unapprove</button>
                 <?php }else{ ?>
                        <button onclick="changeStatus('<?php echo addslashes($user['email']); ?>', '<?php echo $user['id']; ?>',1);" type="button" class="btn btn-outline-primary">Approve</button>
                <?php } ?>
            </td>
            <td style="width:5%;">
                <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-settings"></i>
                    </button>
                    <div class="dropdown-menu">
                        <?php if($user['status'] == 1){ ?>
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#availableStatusPopup" data-whatever="@mdo" data-atwork="<?php echo $user['AtWorkStatus']; ?>" data-oncall="<?php echo $user['OnCallStatus']; ?>" data-subsriptiontype="<?php echo $user['SubscriptionType']; ?>" data-professionid="<?php echo $user['ProfessionsId']; ?>" data-useremail="<?php echo $user['email']; ?>" data-userid="<?php echo $user['id']; ?>">Available/On Call</a>
                        <?php } ?>

                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#customSubscriptionPopUp" data-whatever="@mdo" data-useremail="<?php echo $user['email']; ?>" data-userid="<?php echo $user['id']; ?>" data-usersubscriptionexpiry="<?php echo $user['SubscriptionExpiryDate']; ?>">Update Subscription</a>
                        <?php if((string)$_COOKIE['adminRoleId'] == '0'){ ?>
                            <?php if($user['status'] == 1){ ?>
                                <a class="dropdown-item" href="" data-toggle="modal" data-target="#privilegePopUp" data-whatever="@mdo" data-useremail="<?php echo $user['email']; ?>" data-userid="<?php echo $user['id']; ?>">Manage Privilege</a>
                                <a class="dropdown-item" href="" data-toggle="modal" data-target="#batonRolePopUp" data-whatever="@mdo" data-useremail="<?php echo $user['email']; ?>" data-userid="<?php echo $user['id']; ?>">Manage Baton Roles</a>
                                <!-- <a class="dropdown-item" href="" data-toggle="modal" data-target="#sessionPopUp" data-whatever="@mdo" data-useremail="<?php echo $user['email']; ?>" data-userid="<?php //echo $user['id']; ?>">Manage Session Timeout</a> -->
                            <?php } ?>
                            <a class="dropdown-item" href="javascript:void(0)" onclick="removeUser('<?php echo addslashes($user['email']); ?>', '<?php echo $user['id']; ?>')">Remove User</a>
                            <?php if($user['status'] == 1){ ?>
                                <a id="exportuserchat" onclick="exportuserChat('<?php echo $user['id']; ?>');" class="dropdown-item">Export Chat</a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </td>
        </tr>
        <?php }}elseif(isset($tCount) && $tCount > 0){?>

        <tr>
           <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
        </tr>
        <?php }else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
<span id="totalCount" data-all="<?php echo $tCount; ?>" data-await="<?php echo $tCount; ?>" data-approved="<?php echo $tCount; ?>" data-new="<?php echo $tCount; ?>" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">

<?php
    if(count($data['user_list']) > 0){
        echo $this->element('pagination');
    }
?>
</div>


<script type="text/javascript">
    $('#selectAll').on('click',function(){
        if($('.selectiveCheck input[type="checkbox"]:checked').length == $('.selectiveCheck input[type="checkbox"]').length){
            $('.selectiveCheck input[type="checkbox"]').prop('checked', false);
            $('.bulk_action_btn').attr('disabled',true);
        }else{
            $('.selectiveCheck input[type="checkbox"]').prop('checked', true);
            $('.bulk_action_btn').removeAttr('disabled');
        }
    });
    $('.selectiveCheck input[type="checkbox"]').on('click',function(){
        if($('.selectiveCheck input[type="checkbox"]:checked').length > 0){
            $('.bulk_action_btn').removeAttr('disabled');
        }else{
            $('.bulk_action_btn').attr('disabled',true);
        }
    });
</script>
