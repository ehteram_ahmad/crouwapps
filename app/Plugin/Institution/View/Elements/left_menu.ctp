<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" style="position: fixed;">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->

        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li id="menu1">
                    <a href="<?php echo BASE_URL.'institution/InstitutionHome/index'; ?>" aria-expanded="false">
                        <!-- <i class="mdi mdi-gauge"></i> -->
                        <i style="font-size: 21px; margin-left: -3px;" class="demo-icon icon-dashboard_icon"></i>
                        <span style="margin-left: 3px;" class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li id="menu2">
                    <a href="<?php echo BASE_URL.'institution/InstitutionUser/index'; ?>" aria-expanded="false">
                        <!-- <i class="mdi mdi-email"></i> -->
                        <i style="font-size: 17px;" class="demo-icon icon-approved_email_icon"></i>
                        <span class="hide-menu">Pre-approved Emails</span>
                    </a>
                </li>
                <?php if((string)$_COOKIE['adminRoleId'] == '0'){ ?>
                <li id="menu3">
                    <a href="<?php echo BASE_URL.'institution/InstitutionHome/privilegeList'; ?>" aria-expanded="false">
                        <!-- <i class="mdi mdi-account-multiple" style="font-size: 24px;"></i> -->
                        <i style="font-size: 19px;" class="demo-icon icon-adminteam_icon"></i>
                        <span class="hide-menu">Admin Team</span>
                    </a>
                </li>
                <li id="menu4">
                    <a href="<?php echo BASE_URL.'institution/InstitutionGroup/index'; ?>" aria-expanded="false">
                        <!-- <i class="mdi mdi-account-multiple" style="font-size: 24px;"></i> -->
                        <i style="font-size: 20px;" class="demo-icon icon-group_icon"></i>
                        <span class="hide-menu">Groups</span>
                    </a>
                </li>
                <li id="menu5">
                    <a href="javascript:;" class="nav-link nav-toggle">
                         <!-- <i class="mdi mdi-gauge"></i> -->
                         <i style="font-size: 20px;" class="demo-icon icon-audit-trails-copy"></i>
                        <span class="hide-menu">Audit Trails</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo BASE_URL.'institution/InstitutionAudit/screenShot'; ?>" aria-expanded="false">
                                 <!-- <i class="mdi mdi-gauge"></i> -->
                                 <i style="margin-right: 8px; float: left; height: 50px;" class="demo-icon icon-screenshot"></i>
                                <span class="title">Screen Shot Transactions</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL.'institution/InstitutionAudit/status'; ?>" aria-expanded="false">
                                 <!-- <i class="mdi mdi-gauge"></i> -->
                                 <i style="margin-right: 8px; float: left; height: 50px;" class="demo-icon icon-oncalltransactions"></i>
                                <span class="title">Available/OnCall Transactions</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="menu6">
                    <a href="<?php echo BASE_URL.'institution/InstitutionSetting/index'; ?>" aria-expanded="false">
                        <!-- <i class="mdi mdi-account-multiple" style="font-size: 24px;"></i> -->
                        <i style="font-size: 17px; margin-top: -2px;" class="demo-icon icon-settings_icon"></i>
                        <span class="hide-menu">Settings</span>
                    </a>
                </li>
                <li id="menu7" class="is-hidden">
                    <a href="<?php echo BASE_URL.'institution/InstitutionUser/deviceInfo'; ?>" aria-expanded="false">
                        <!-- <i class="mdi mdi-account-multiple" style="font-size: 24px;"></i> -->
                        <i style="font-size: 19px; margin-top: -6px;" class="demo-icon icon-device-info"></i>
                        <span class="hide-menu">Users Device Info</span>
                    </a>
                </li>
                <?php if((string)$_COOKIE['adminInstituteId'] == '336'){ ?>
                <li id="menu8">
                    <a href="javascript:;" class="nav-link nav-toggle">
                         <i class="mdi mdi-gauge"></i>
                        <span class="hide-menu">Reports</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo BASE_URL.'institution/InstitutionAnalytic/datastudio'; ?>" aria-expanded="false">
                                <i class="demo-icon icon-datastudio"></i>
                                <span class="title">Data-Studio</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL.'institution/InstitutionAnalytic/powerbi'; ?>" aria-expanded="false">
                              <i class="demo-icon icon-powerbi"></i>
                                <span class="title">Power-Bi</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
                <!-- To Do (New switch board implementation ) [START] -->

                <li id="menu10">
                    <a href="<?php echo BASE_URL.'institution/Switchboard'; ?>" aria-expanded="false">
                        <i style="font-size: 17px; margin-top: 0px; float: left; height: 50px; margin-right: 8px;" class="demo-icon icon-batonmainicon"></i>
                        <span class="hide-menu">Switch Board
                          <?php
                              $custBatonClass = 'none';
                                if(isset($baton_count) && $baton_count > 0){
                                  $custBatonClass = 'inline-block';
                                }
                          ?>
                          <span class="badge badge-successNew2 batonBadgeCount" id="baton_request_count_latest" style="display:<?php echo $custBatonClass; ?>"><?php echo $baton_count; ?></span>
                        </span>
                    </a>
                    <?php /*<ul class="sub-menu">
                        <li>
                            <a href="<?php echo BASE_URL.'institution/InstitutionBaton/index'; ?>" aria-expanded="false">
                                <i style="margin-right: 8px; float: left;" class="demo-icon icon-batonroleicon"></i>
                                <span class="title">Baton Roles</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL.'institution/InstitutionBaton/getRequest'; ?>" aria-expanded="false">
                              <i style="margin-right: 8px; float: left; height: 50px;" class="demo-icon icon-batonroletransfericon"></i>
                                <span class="title">Baton Role Requests</span>
                                <?php
                                  $custBatonClass = 'none';
                                  if(isset($baton_count) && $baton_count > 0){
                                    $custBatonClass = 'block';
                                  }
                                ?>
                                <span id="baton_request_count_latest" class="badge badge-successNew2 batonBadgeCount" style="display:<?php echo $custBatonClass; ?>"><?php echo $baton_count; ?></span>
                            </a>
                        </li>
                    </ul> */ ?>
                </li>

                <!-- To Do (New switch board implementation ) [END] -->

                <?php } ?>
                <li id="menu9">
                    <a href="<?php echo BASE_URL.'institution/InstitutionAudit/registrationReport'; ?>" aria-expanded="false">
                        <i style="font-size: 17px; margin-top: -1px; float: left; height: 50px; margin-right: 8px;" class="demo-icon icon-reporticon"></i>
                        <span class="hide-menu">User Registration Report</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
