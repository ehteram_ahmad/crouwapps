<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Settings: <span style="color:#10a5da;">Pre-Approved Emails</span></h3>
        </div>
        <div class="buttonTop col-md-4 col-4">
            <button class="btn hidden-sm-down" data-toggle="modal" data-target="#exampleModalTest" data-whatever="@mdo" id="addUserButton"><i class="mdi mdi-plus-circle"></i> Add Pre-Approved Email(s)</button>
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">Pre-approved emails</li> -->
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
      <div class="card">
         <div class="card-block padLeftRight0 pad0">
            <div class="floatLeftFull padLeftRight14 newListStyle">

              <div class="modal fade" id="exampleModalTest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                  <div class="loader" style="height: 64%; display:none;" id="adduserLoader"></div>
                  <div class="modal-dialog" role="document">
                      <div class="modal-content uploadFileBlock" id="bulkUploadUser">
                      <div class="loaderImg" style="display:none;">
                          <?php //echo $this->Html->image('Institution.page-loader.gif', array("class"=> "loading-image" )); ?>

                          </div>
                          <div class="modal-header">
                              <h4 class="modal-title" id="exampleModalLabel1">Add Pre-Approved Email(s)</h4>
                              <p>You can add Pre-Approved Email(s) by providing information below:</p>
                          </div>
                          <div class="modal-body">
                              <form id="bulkEmailUpload">
                                  <div id="inputEmail" class="form-group">
                                      <input type="email" class="form-control" id="userEmail" name="userEmail" placeholder="Enter user’s email address">
                                      <div class="form-control-feedback" id="emptyEmail" style="display:none">Please Enter Email address</div>
                                      <div class="form-control-feedback" id="invalidEmail" style="display:none">Please enter a valid email address</div>
                                      <div class="form-control-feedback" id="alreadyExist" style="display:none"></div>
                                  </div>
                                  <div class="row">
                                      <div class="col-lg-12 col-md-12">
                                          <div class="card">
                                              <div class="">
                                                <div class="borderSeparation">
                                                  <h4>OR</h4>
                                                </div>
                                                  <label for="input-file-now" class="nodefault">Add Multiple Pre-Approved Emails</label>
                                                  <div class="pull-right">
                                                    <span class="sampleTxt">Sample File Formats- </span>
                                                    <a target="_blank" class="downloadLink" href="<?php echo BASE_URL; ?>institution/sample.csv" title="Sample">Download</a>
                                                  </div>
                                                  <input type="file" name="emailfile" id="input-file-now" class="dropify" accept=".csv, .xls, .xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                                  <input type="hidden" name="company_id" type="test" value="<?php echo $_COOKIE['adminInstituteId']; ?>" />
                                                  <div class="form-control-feedback" id="uploadResult" style="display:none">Upload Result:-</div>
                                                  <div class="form-control-feedback" id="uploadSuccess" style="display:none"></div>
                                                  <div class="form-control-feedback" id="uploadError" style="display:none"></div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                          </div>
                          <div class="onCallBtMain txtCenter marBottom30">
                            <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" onclick="addUser();" id="addUserbtn1">Add Email</button>
                            <button type="submit" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" id="addUserbtn2" style="display:none;">Upload</button>
                            <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
                          </div>
                          <!-- <div style="border-top: 1px solid #eceeef !important;" class="modal-footer">
                              <button type="button" class="btn btn-success waves-effect" onclick="addUser();" id="addUserbtn1">Save</button>
                              <button class="btn btn-success waves-effect" id="addUserbtn2" style="display:none;">Upload</button>
                          </div> -->
                          </form>
                      </div>
                  </div>
              </div>

              <div class="tableTop floatLeft">
                 <div id="list-filter-options">
                   <input type="radio" name="filter_option" value="all" id="filter_option_all" checked="checked">
                   <label for="filter_option_all">All (<span id="showTotCount">0</span>)</label>
                   <input type="radio" name="filter_option" value="1" id="filter_option_signed">
                   <label for="filter_option_signed">Signed Up (<span id="showSignedCount">0</span>)</label>
                   <input type="radio" name="filter_option" value="0" id="filter_option_pending">
                   <label for="filter_option_pending">Pending (<span id="showPendingCount">0</span>)</label>
                 </div>
              </div>
               <div id="userlisting_filter" class="dataTables_filter">
                  <label><i class="demo-icon icon-sub-lense"></i></label>
                  <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
               </div>
            </div>

            <div class="table-responsive wordBreak" id="ajaxContent">
              <table class="table stylish-table customPading">
                  <thead>
                      <tr>
                        <th style="width:40%; padding-left: 25px;">E-mail </th>
                        <th style="width:20%;text-align: center;">Status</th>
                        <th style="width:20%;text-align: center;">Registered</th>
                        <th style="width:20%; text-align: center;">Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                          if(isset($data['user_list']) && count($data['user_list']) > 0){
                          foreach( $data['user_list'] as $user){
                      ?>
                      <tr>
                          <td style="width:50px;"><span class="round"><?php echo strtoupper(substr($user['email'],0,1)); ?></span></td>
                          <td><?php echo $user['email']; ?></td>
                          <td>
                              <?php
                                  if($user['status']==2){ ?>
                                      <button type="button" class="btn btn-danger"> Removed </button>
                            <?php }else if($user['status']==1){ ?>
                                      <button onclick="changeStatus('<?php echo addslashes($user['email']); ?>',0);" type="button" class="btn btn-primary"> Unassign </button>
                            <?php }else{ ?>
                                      <button onclick="changeStatus('<?php echo addslashes($user['email']); ?>',1);" type="button" class="btn btn-outline-primary"> Assign </button>
                            <?php }
                              ?>
                          </td>
                          <td>
                              <div class="btn-group">
                                  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <i class="ti-settings"></i>
                                  </button>
                                  <div class="dropdown-menu pad0">
                                      <a onclick="removeUser('<?php echo $user['email']; ?>')" class="dropdown-item" href="javascript:void(0)">Remove User</a>
                                  </div>
                              </div>
                          </td>
                      </tr>
                      <?php }}elseif(isset($tCount) && $tCount > 0){?>

                      <tr>
                         <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                      </tr>
                      <?php }else{ ?>
                      <tr>
                         <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                      </tr>
                      <?php  } ?>
                  </tbody>
              </table>
              <span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
              <div class="actions marBottom10 paddingSide15 leftFloat98per">
              <?php
                  if(isset($data['user_list']) && count($data['user_list']) > 0){
                      echo $this->element('pagination');
                  }
              ?>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
    $('#input-file-now').change(function(){
        $("#userEmail").attr("disabled", "disabled");
        $("#addUserbtn1").hide();
        $("#addUserbtn2").show();

        $("#inputEmail").removeClass('has-danger');
        $("#emptyEmail").hide();
        $("#invalidEmail").hide();

        $("#uploadResult").hide();
        $("#uploadSuccess").hide();
        $("#uploadError").hide();

    });

    $("#userEmail").keyup(function() {
        var input = $(this);

        if( input.val() == "" ) {
          $("#addUserbtn1").show();
          $("#addUserbtn2").hide();
          $(".dropify-wrapper").removeClass('disable');
          $("#input-file-now").removeClass('disable');
        }
        else{
            $(".dropify-wrapper").addClass('disable');
            $("#input-file-now").addClass('disable');
            $("#addUserbtn1").show();
            $("#addUserbtn2").hide();
        }
    });

    $("#addUserButton").click(function(){
        $("#userEmail").val('');
        $("#inputEmail").removeClass('has-danger');
        $("#alreadyExist").hide();
        $("#emptyEmail").hide();
        $("#invalidEmail").hide();
        $(".dropify-wrapper").removeClass('disable');
        $("#input-file-now").removeClass('disable');
        $("#userEmail").attr("disabled", false);

        $(".dropify-clear").trigger("click");

    });

    $("form#bulkEmailUpload").submit(function(){
        var formData = new FormData(this);
        $('#adduserLoader').fadeIn('fast');

       $.ajax({
           url: '<?php echo BASE_URL; ?>institution/InstitutionHome/addUserFileUpload',
           type: 'POST',
           data: formData,
           success: function (data) {
               console.log(data);
               var responseArray = JSON.parse(data);
               if( responseArray['status'] == 1 ){
                   $('#adduserLoader').fadeOut('fast');
                   $("#uploadResult").show();
                   $("#uploadSuccess").show();
                   $("#uploadError").show();
                   $("#uploadSuccess").html("Success: " + responseArray['data']['addUserData']['valid_email_count']);
                   $("#uploadError").html("Error: " + responseArray['data']['addUserData']['invalid_email_count']);
               }else{

               }
           },
           cache: false,
           contentType: false,
           processData: false
       });

        return false;
    });

    function addUser(){
        var userEmail = $("#userEmail").val().trim();
        var regex = /^[_A-Za-z0-9-']+(\.[_A-Za-z0-9-']+)*@[A-Za-z0-9-]+(\.[a-z0-9-]+)*(\.[A-Za-z]{2,4})$/;
        if(userEmail == '' || userEmail == null){
            $("#inputEmail").addClass('has-danger');
            $("#emptyEmail").show();
            $("#invalidEmail").hide();
        }else if(!regex.test(userEmail)){
             $("#inputEmail").addClass('has-danger');
             $("#invalidEmail").show();
             $("#emptyEmail").hide();
        }else{
            $('#adduserLoader').fadeIn('fast');
            $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionHome/addUser',
            type: 'POST',
            data: { email: userEmail },
            success: function( result ){
                $('#adduserLoader').fadeOut('fast');
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                    // $.toast({
                    //     heading: 'New User Adding',
                    //     text: 'Please wait while a new user added.',
                    //     position: 'top-right',
                    //     loaderBg:'#2c2b2e',
                    //     icon: 'success',
                    //     hideAfter: 3000,
                    //     stack: 6
                    // });
                    var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                    $('#customModalLabel').html('Confirmation');
                    // $('#customModalBody').html('Please wait while a new user added');
                    $('#customModalBody').html('New User added successfully');
                    $('#customModalFoot').html(buttonHtml);
                    $('#customBox').modal('show');

                    $("#customBox .loaderImg").hide();

                    $('#exampleModalTest').modal('toggle');
                    // location.reload();
                }else{
                    $("#inputEmail").addClass('has-danger');
                    $("#alreadyExist").show();
                    $("#alreadyExist").html('* '+responseArray['message']+'.' );
                    $("#emptyEmail").hide();
                    $("#invalidEmail").hide();

                }
            },
            error: function( result ){
                $('#adduserLoader').fadeOut('fast');
                var responseArray = JSON.parse(result);
                    $("#inputEmail").addClass('has-danger');
                    $("#alreadyExist").show();
                    $("#alreadyExist").html('* '+responseArray['message']+'.' );
                    $("#emptyEmail").hide();
                    $("#invalidEmail").hide();

            }
        });
        }
    }
</script>
<script type="text/javascript">
    // $("#sidebarnav>li#menu2").addClass('active').find('a').addClass('active');
    $("#sidebarnav>li#menu4").addClass('active').find('a').eq(0).addClass('active');
    $("#sidebarnav>li#menu4").find("li").eq(1).addClass('active').find('a').addClass('active');

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#user_search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(searchUser, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function searchUser() {
       $search_text = $input.val().trim();
       $input.blur();
       $(".loader").fadeIn("fast");
       getDashCount($search_text);
       var list_type = $('input[name="filter_option"]:checked').val();
       $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionUser/userListFilter',
           type: "POST",
           data: {'search_text':$search_text,'type':list_type},
           success: function(data) {
               $('#ajaxContent').html(data);
               $('#showTotCount').html($('#totalCount').html());
               $(".loader").fadeOut("fast");
               $('#assign_btn').hide();
               $('#unassign_btn').hide();
           }
       });
    }
    // --------- INITIAL FUNCTIONS ------------
    $('body').on('click','input[name="filter_option"]',function(){
      getUserList('1','20',$input.val().trim(),$(this).val());
    });

    getUserList('1','20',$input.val().trim(),'all');

    // --------- INITIAL FUNCTIONS ------------

    function getUserList(page,limit,$search_text,type){
       $(".loader").fadeIn("fast");
       getDashCount($search_text);
       $('input[name="filter_option"]').attr('checked',false);
       $('input[name="filter_option"][value="'+type+'"]').attr('checked',true);
       $.ajax({
         url:'<?php echo BASE_URL; ?>institution/InstitutionUser/userListFilter',
         type: "POST",
         data: {'page_number':page,'limit':limit,'search_text':$search_text,'type':type},
         success: function(data) {
            console.log(data);
           $('#ajaxContent').html(data);
           // $('#showTotCount').html($('#totalCount').html());
           $(".loader").fadeOut("fast");
         },
         error: function(){
           $(".loader").fadeOut("fast");
         }
       });
    }

    function getDashCount($search_text){
        $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionUser/getDashCount',
          type: "POST",
          data: {'search_text':$search_text},
          success: function(data) {
            console.log(data);
            data = JSON.parse(data);
            console.log(data);
            $('#showTotCount').html(data.total);
            $('#showSignedCount').html(data.signed_in);
            $('#showPendingCount').html(data.pending_in);
          }
        });
    }

    function changeStatus(email, status){
        $(".loader").fadeIn("fast");
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionUser/changeAssignStatus',
            type: 'POST',
            data: {'email':email, 'status': status},
            success: function( result ){
                $(".loader").fadeOut("fast");
                $("#customBox .loaderImg").hide();

                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                  var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                  $('#customModalLabel').html('Confirmation');
                  $('#customModalBody').html(responseArray['message']);
                  $('#customModalFoot').html(buttonHtml);
                  $('#customBox').modal('show');
                }else if(responseArray['status'] == 2){
                  var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                  $('#customModalLabel').html('Error');
                  $('#customModalBody').html(responseArray['message']);
                  $('#customModalFoot').html(buttonHtml);
                  $('#customBox').modal('show');
                  $(".loading_overlay1").hide();
                }else{
                  var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                  $('#customModalLabel').html('Error');
                  $('#customModalBody').html('An error has occurred. Please try again.');
                  $('#customModalFoot').html(buttonHtml);
                  $('#customBox').modal('show');
                  $(".loading_overlay1").hide();
                }
                $(".loader").fadeOut("fast");
            },
            error: function( result ){
                // $.toast({
                //     heading: 'Error',
                //     text: 'An error has occurred. Please try again.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'error',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $(".loading_overlay1").hide();
                $(".loader").fadeOut("fast");
            }
        });
    }

    function removeUser(userEmail, userId){
        var buttonHtml = '<div class="row mainDiv" style="text-align: center;">';
            buttonHtml += '<button type="button" class="btn btn-info waves-effect redBtCustom" id="removeReqBtn">Remove</button>';
            buttonHtml += '<button type="button" onClick="removeClass();" class="btn btn-info waves-effect whiteBtCustom" data-dismiss="modal">Cancel</button>';
            buttonHtml += '</div>';
            buttonHtml += '<div class="row showWarning" style="display:none">';
            buttonHtml += '<div class="col-md-6 pull-left">';
            buttonHtml += '<img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">';
            buttonHtml += '<p>Are you sure you want to Remove User from your Institution?</p>';
            buttonHtml += '</div>';
            buttonHtml += '<div class="col-md-6 txtRight pull-right">';
            buttonHtml += '<button type="button" class="btn btn-success waves-effect" onclick="removeUserFinal(\''+userEmail+'\', '+userId+');">Yes</button>';
            buttonHtml += '<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>';
            buttonHtml += '</div>';
            buttonHtml += '</div>';
            buttonHtml += '<div class="row showSucess" style="display:none">';
            buttonHtml += '<div class="col-md-8 pull-left">';
            buttonHtml += '<img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">';
            buttonHtml += '<p style="margin-top: 7px;">User has been removed successfully.</p>';
            buttonHtml += '</div>';
            buttonHtml += '<div class="col-md-4 txtRight pull-right">';
            buttonHtml += '<button type="button" class="btn btn-info waves-effect" onClick="reload();" data-dismiss="modal">Close</button>';
            buttonHtml += '</div>';
            buttonHtml += '</div>';


        $('#customModalLabel').html('Remove User<p>Remove user from your institution.</p>');
        $('#customModalBody').addClass('bodyCustom').html('<span><img src="<?php echo BASE_URL ?>institution/img/sub-removeUser.svg" alt=""></span><p>It can’t be undone . User will be removed from your institution immediately and would not be able to access the Directory.</p>');
        $('#customModalFoot').css('text-align','left').html(buttonHtml);
        $('#customBox').modal('show');
    }

    $('body').on('click','#removeReqBtn',function(){
      $('#customBox .row.mainDiv').hide();
      $('#customBox .row.showWarning').show();
      $('#customBox .row.showSucess').hide();
    });

    function removeClass(){
      $('#customModalBody').removeClass('bodyCustom');
      $('#customModalFoot').css('text-align','center');
    }

    function removeUserFinal(userEmail, userId){
        // $('#customBox').modal('hide');
        // $('#customModalBody').removeClass('bodyCustom');
        var email = userEmail;
        var userId = userId;
        $("#customBox .loaderImg").show();
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionHome/removeUser',
            type: 'POST',
            data: {'email': email, 'user_id': userId},
            success: function( result ){
                console.log(result);
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                    $("#customBox .loaderImg").hide();
                    $('#customBox .row.mainDiv').hide();
                    $('#customBox .row.showWarning').hide();
                    $('#customBox .row.showSucess').show();
                }else if(responseArray['status'] == 2){
                  var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                  $('#customModalLabel').html('Error');
                  $('#customModalBody').removeClass('bodyCustom').html(responseArray['message']);
                  $('#customModalFoot').html(buttonHtml);
                  $('#customModalFoot').css('text-align','center');
                  $("#customBox .loaderImg").hide();
                }else{
                   var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                   $('#customModalLabel').html('Error');
                   $('#customModalBody').removeClass('bodyCustom').html('An error has occurred. Please try again');
                   $('#customModalFoot').html(buttonHtml);
                   $('#customModalFoot').css('text-align','center');
                   $("#customBox .loaderImg").hide();
                }
            },
            error: function( result ){
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').removeClass('bodyCustom').html('An error has occurred. Please try again');
                $('#customModalFoot').html(buttonHtml);
                $('#customModalFoot').css('text-align','center');
                $("#customBox .loaderImg").hide();
            }
        });

    }

    function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        var goVal=$("#chngPage").val();
        $search_text = $input.val().trim();
        var list_type = $('input[name="filter_option"]:checked').val();
        if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
            $(".loader").fadeIn("fast");
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionUser/userListFilter',
                type: "POST",
                data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'search_text':$search_text,'type':list_type},
                success: function(data) {
                    $('#ajaxContent').html(data);
                    $('#showTotCount').html($('#totalCount').html());
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
        }

    }
    function chngCount(val){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        $search_text = $input.val().trim();
        var list_type = $('input[name="filter_option"]:checked').val();
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionUser/userListFilter',
            type: "POST",
            data: {'limit':val,'sort':a,'order':order,'search_text':$search_text,'type':list_type},
            success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
                $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                }
                else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                }
                $(".loader").fadeOut("fast");
            }
        });
    }

    function abc(val,limit){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        $search_text = $input.val().trim();
        var list_type = $('input[name="filter_option"]:checked').val();
      $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionUser/userListFilter',
           type: "POST",
           data: {'page_number':val,'limit':limit,'sort':a,'order':order,'search_text':$search_text,'type':list_type},
           success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
        });
    }

    function reload(){
        $('#customModalBody').removeClass('bodyCustom');
        $('#customModalFoot').css('text-align','center');
        var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
        var limit = $('#Data_Count').val();
        var $search_text = $input.val().trim();
        var list_type = $('input[name="filter_option"]:checked').val();
        getDashCount($search_text);
        getUserList(page,limit,$search_text,list_type);
        notifications();
    }

    // function getDashCount(){
    //     $.ajax({
    //       url:'<?php //echo BASE_URL; ?>institution/InstitutionHome/getDashboardCounts',
    //       type: "POST",
    //       success: function(data) {
    //         $('.widgetMain.dashCounts').html(data);
    //       }
    //     });
    // }
    function getListing($type){
       if($('.card[data-value="'+$type+'"]').hasClass('selected')){
           return false;
       }
       if($type == 'all'){
           custom_val = '';
       }else{
           custom_val = '/'+$type;
       }
       window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionHome/index" + custom_val;
    }
</script>
