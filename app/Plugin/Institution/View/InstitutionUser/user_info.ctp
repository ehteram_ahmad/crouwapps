<?php
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
    echo $this->Html->css(
        array(
            'Institution.user_info'
        )
    );
?>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-12 align-self-center col-5">
            <h3 class="text-themecolor m-b-0 m-t-0 col-1 window_history" ><img src="<?php echo BASE_URL; ?>institution/img/left-arrow.svg" /></h3>
            <h3 class="text-themecolor m-b-0 m-t-0 col-11" style="float: left;"><?php echo $userName; ?> Profile</h3>
            <!-- <span class="statusSpan" id="subscriptionStatus">Expired</span> -->
            <!-- <span class="statusSpan" id="activeStatus">Active</span> -->
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">Dashboard / User Profile</li> -->
      </ol>
    </div>


    <!-- ============================================================== -->
  <!-- Start HTML Content -->
  <!-- ============================================================== -->
  <div class="row boaderLines">

     <!-- Popup content -->
     <div class="modal fade" id="batonRolePopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
         <div class="loader" id="addbatonRoleLoader"></div>
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h4 class="modal-title" id="exampleModalLabel1">Update User Baton Roles</h4>
                     <p>You can update user Baton Roles below:</p>
                 </div>
                 <div class="modal-body">
                   <div class="baton_listing">
                       <div class="col-md-12 baton_role_search">
                         <label><i class="demo-icon icon-sub-lense"></i></label>
                         <input type="text" placeholder="Search" id="search_unoccupied_role" name="" value="">
                       </div>
                       <div id="batonListing" style="display:none;">
                         <ul id="baton_unoccupied_role"></ul>
                       </div>
                   </div>
                 </div>

                 <div class="modal-footer">
                   <div class="row showWarning" style="display:none">
                       <div class="col-md-6 pull-left">
                         <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                         <p>Are you sure you want to update user baton role?</p>
                       </div>
                       <div class="col-md-6 txtRight pull-right">
                         <button type="button" class="btn btn-success waves-effect" onclick="setBatonRole();">Yes</button>
                         <button type="button" class="btn btn-info waves-effect toMainContent" data-dismiss="modal">Cancel</button>
                       </div>
                     </div>
                     <div class="row showSucess" style="display:none">
                         <div class="col-md-8 pull-left">
                           <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                           <p>User Baton role has been updated successfully!</p>
                         </div>
                         <div class="col-md-4 txtRight pull-right">
                           <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                         </div>
                     </div>
                     <div class="row showBack" style="display:none">
                         <div class="col-md-6 txtRight pull-right">
                           <button type="button" class="btn btn-info waves-effect toMainContent">Cancel</button>
                         </div>
                       </div>
                 </div>
             </div>
         </div>
     </div>
     <!-- Popup content end -->

     <div class="col-12">
      <span id="user_id" style="display: none;"><?php echo isset($user_id)? $user_id :'User'; ?></span>
      <span id="user_email" style="display: none;"><?php echo isset($user_email)? $user_email :'email'; ?></span>
      <span id="user_oncallaccess" style="display: none;"><?php echo isset($oncall_access)? $oncall_access :0; ?></span>

      <div class="row padreduce">
        <div class="col-lg-6 col-md-6">
          <div class="card block">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 sub-section left-section" id="userInfoBlock">
                <div id="piaLoader" class="loader"></div>
                <div id="personalInformationAjax">
                  <div class="row">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                      <span class="round notAtWork">
                        <img width="165" height="165" src="<?php echo BASE_URL ?>institution/img/ava-single.png" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                      </span>
                    </div>
                    <div class="col-sm-9 col-md-9 col-lg-9">
                      <h3 class="label-sub-div editable" onclick="editMode('name','Deepak','Kumar');">User Name<span class="fa fa-pencil editPencil"></span></h3>
                      <div class="label-content main-div">
                        <div class="label-sub-div">
                          <?php if(isset($dnd['active']) && $dnd['active'] == 1){ ?>
                            <span class="label-dnd">Do Not Disturb: Scrubbed</span>
                          <?php }else{ ?>
                            <span class="label-notactive">Do Not Disturb: Not Active</span>
                          <?php } ?>
                        </div>
                        <div class="label-sub-div">
                          <span class="label-head">Base Location:</span>
                          <span class="label-value"></span>
                        </div>
                        <div class="label-sub-div">
                          <span class="label-head">Profession:</span>
                          <span class="label-value"></span>
                        </div>
                        <div class="label-sub-div">
                          <span class="label-head">GMC Number:</span>
                          <span class="label-value"></span>
                        </div>
                        <div class="label-sub-div editable" onclick="editMode('email','deepakkumar@mediccreations.com');">
                          <span class="label-head">Email:</span>
                          <span class="label-value"></span>
                          <span class="fa fa-pencil editPencil"></span>
                        </div>
                        <div class="label-sub-div editable" onclick="editMode('phone','9999999999');">
                          <span class="label-head">Phone:</span>
                          <span class="label-value"></span>
                          <span class="fa fa-pencil editPencil"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="name_editor" class="is-hidden edit-section">
                  <div class="card-title edit">
                    <h3>Edit Name</h3>
                  </div>
                  <div class="row card-block">
                    <div class="col-md-6">
                      <span class="userName">First Name</span>
                      <input type="text" onkeyup="nameValidate();" name="" class="form-control first_name_edit input-bottom-border" value="">
                    </div>
                    <div class="col-md-6">
                      <span class="userName">Last Name</span>
                      <input type="text" onkeyup="nameValidate();" name="" class="form-control last_name_edit input-bottom-border" value="">
                    </div>
                    <span class="is-hidden error-icon error-msg">Email that you've entered doesn't match.</span>
                    <button type="button" class="btn btn-success btn-blueDef" onclick="updateUser('name');" name="button">Save</button>
                    <button type="button" class="btn btn-light-red" onclick="closeEditMode();" name="button">Cancel</button>
                  </div>
                </div>
                <div id="email_editor" class="is-hidden edit-section">
                  <div class="card-title edit">
                    <h3>Edit Email</h3>
                  </div>
                  <div class="row card-block">
                    <div class="col-md-12">
                      <span class="userName">Update Email:</span>
                      <input type="text" onkeyup="emailValidate();" name="" autocomplete="off" class="form-control input-bottom-border email_edit" value="">
                    </div>
                    <div class="col-md-12">
                      <span class="userName">Confirm Email:</span>
                      <input type="text" onkeyup="emailValidate();" name="" autocomplete="off" class="form-control input-bottom-border email_edit" value="">
                      <i class="fa fa-check-circle success-icon is-hidden" aria-hidden="true"></i>
                      <i class="fa fa-exclamation-circle error-icon is-hidden" aria-hidden="true"></i>
                      <span class="is-hidden error-icon error-msg">Email that you've entered doesn't match.</span>
                    </div>
                    <button type="button" class="btn btn-success btn-blueDef" onclick="updateUser('email');" name="button">Save</button>
                    <button type="button" class="btn btn-light-red" onclick="closeEditMode();" name="button">Cancel</button>
                  </div>
                </div>
                <div id="phone_editor" class="is-hidden edit-section">
                  <div class="card-title edit">
                    <h3>Edit Phone</h3>
                  </div>
                  <div class="row card-block">
                    <div class="col-md-12" style="pointer-events: none;margin-top: -15px;margin-bottom: 20px;">
                      <span class="userName">User's Current Phone No.:</span>
                      <span class="phone_edit"></span>
                      <!-- <input style="width:40% !important; border:none;" type="text" onkeyup="phoneValidate();" name="" autofocus="off" autoRender="off" auto autocomplete="off" onkeypress="return event.charCode>=48&&event.charCode<=57||0==event.charCode||43==event.charCode" class="form-control input-bottom-border phone_edit" value=""> -->
                    </div>
                    <div class="col-md-12">
                      <span class="userName">Enter New Phone No.:</span>
                      <!-- <div class="country-select"> -->
                        <select id="countrySelect" name="x_auth_country_code">
                        </select>
                      <!-- </div> -->
                      <input style="width:40% !important;" type="text" onkeyup="phoneValidate();" placeholder="Enter Phone Number" name="" autocomplete="off" onkeypress="return event.charCode>=48&&event.charCode<=57||0==event.charCode||43==event.charCode" class="form-control input-bottom-border phone_edit_new" value="">
                      <i class="fa fa-check-circle success-icon is-hidden" aria-hidden="true"></i>
                      <i class="fa fa-exclamation-circle error-icon is-hidden" aria-hidden="true"></i>
                      <span class="is-hidden error-icon error-msg">Phone no that you've entered doesn't match.</span>
                    </div>
                    <button type="button" class="btn btn-success btn-blueDef" onclick="updateUser('phone');" name="button">Save</button>
                    <button type="button" class="btn btn-light-red" onclick="closeEditMode();" name="button">Cancel</button>
                  </div>
                </div>
                <div id="password_editor" class="is-hidden edit-section">
                  <div class="card-title edit">
                    <h3>Reset Password</h3>
                  </div>
                  <div class="row card-block">
                    <div class="col-md-12">
                      <span class="userName">Enter Password:</span>
                      <input type="text" onkeyup="passwordValidate();" name="" autocomplete="off" class="form-control input-bottom-border password_edit" value="">
                    </div>
                    <div class="col-md-12">
                      <span class="userName">Confirm Password:</span>
                      <input type="text" onkeyup="passwordValidate();" name="" autocomplete="off" class="form-control input-bottom-border password_edit" value="">
                      <i class="fa fa-check-circle success-icon is-hidden" aria-hidden="true"></i>
                      <i class="fa fa-exclamation-circle error-icon is-hidden" aria-hidden="true"></i>
                      <span class="is-hidden error-icon error-msg">Password that you've entered doesn't match.</span>
                    </div>
                    <button type="button" class="btn btn-success btn-blueDef" onclick="updateUser('password');" name="button">Save</button>
                    <button type="button" class="btn btn-light-red" onclick="closeEditMode();" name="button">Cancel</button>
                    <div class="card-note">
                      <label>Note</label>
                      <div class="note-des">Your password must include minimum 8 characters. Use at least One UPPERCASE letter, One lowercase letter, One number and at least One special character.</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 sub-section left-section" id="permanentBlock">
                <div id="permanentLoader" class="loader"></div>
                <div class="card-title">
                  <h3>Permanent Role(s)</h3>
                </div>
                <ul id="userPermanentRole">
                  <li class="no_record">
                    <span><font color="green">Wait while we are fetching the list</font></span>
                  </li>
                </ul>
              </div>
              <div class="col-lg-12 col-md-12 sub-section left-section" id="batonBlock">
                <div id="batonLoader" class="loader"></div>
                <div class="card-title">
                  <h3>Baton Role(s)</h3>
                </div>
                <ul id="userbatonRole">
                  <li class="no_record">
                    <span><font color="green">Wait while we are fetching the list</font></span>
                  </li>
                  <!-- <li>
                    <span>dvs,md  sbdjk</span>
                    <a href="">Click to Unassign</a>
                  </li>
                  <li>
                    <span>Oncall testing cardiolog</span>
                    <span class="disabled">Request Awaiting</span>
                  </li> -->
                </ul>
                <div class="card-note">
                  <label>Note</label>
                  <div class="note-des">User’s Baton Roles List</div>
                  <span data-toggle="modal" data-target="#batonRolePopUp" class="assign_baton">+Assign New Role</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6">
          <div class="card block">
            <div class="row">
              <div class="col-lg-12 col-md-12 sub-section right-section" id="user-status-block" >
                <div id="availableLoader" class="loader"></div>
                <div class="card-title">
                  <h3>Availability Status</h3>
                </div>
                <p>You can change user’s current availability status by the options below:</p>
                <span id="userstatus_val" style="display: none;"></span>
                <span id="total_baton" style="display: none;"></span>
                <span id="on_call_baton" style="display: none;"></span>
                <span id="dnd_status" style="display: none;"></span>
                <ul class="radioBtns-list" id="userStatusAjax">
                  <div>
                    <input type="radio" id="userstatus_0" name="userstatus" value="0">
                    <label for="userstatus_0">Available and On Call</label>
                  </div>
                  <div>
                    <input type="radio" id="userstatus_1" name="userstatus" value="1">
                    <label for="userstatus_1">Available</label>
                  </div>
                  <div>
                    <input type="radio" id="userstatus_2" name="userstatus" value="2">
                    <label for="userstatus_2">Not Available</label>
                  </div>
                </ul>
                <div class="card-footer">
                  <div class="card-update">
                    <p>Are you sure you want to change user's availability status?</p>
                    <i class="demo-icon icon-checkfilterarticle" onclick="updateStatus();"></i>
                    <i class="demo-icon icon-cancel"></i>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 sub-section right-section" id="user-privilege-block">
                <div id="priviligeLoader" class="loader"></div>
                <div class="card-title">
                  <h3>Privilege</h3>
                </div>
                <p>You can update user privilege by changing role type above.</p>
                <span id="privilige-val" style="display: none;"></span>
                <ul class="radioBtns-list" id="userPrivilegeAjax">
                  <div>
                    <input type="radio" id="privilige_0" name="privilige" value="0">
                    <label for="privilige_0">Admin</label>
                  </div>
                  <div>
                    <input type="radio" id="privilige_1" name="privilige" value="1">
                    <label for="privilige_1">Sub-Admin</label>
                  </div>
                  <div>
                    <input type="radio" id="privilige_3" name="privilige" value="3">
                    <label for="privilige_3">Switchboard</label>
                  </div>
                  <div>
                    <input type="radio" id="privilige_2" name="privilige" value="2">
                    <label for="privilige_2">None</label>
                  </div>
                </ul>
                <div class="card-footer">
                  <div class="card-update">
                    <p>Are you sure you want to allow this user to access all features of Institution Panel?</p>
                    <i class="demo-icon icon-checkfilterarticle" onclick="setPrivilege();"></i>
                    <i class="demo-icon icon-cancel"></i>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 sub-section right-section" id="user-expiry-date">
                <div id="subscriptionLoader" class="loader"></div>
                <div class="card-title">
                  <h3>Subscription</h3>
                </div>
                <form>
                  <p>Click to update User’s Subscription Expiry Date:</p>
                  <span class="is-hidden" id="usersubscriptionexpiryDate"></span>
                  <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                      <input class="form-control" type="text" autocorrect="off" autocapitalize="off" autocomplete="off" value="" id="subscription_expiry_date" />
                      <span class="input-group-addon">
                        <i class="demo-icon icon-calendar-icon"></i>
                      </span>
                  </div>

                </form>
                <div class="card-footer">
                  <div class="card-update">
                    <p>Are you sure you want to change User Subscription Date?</p>
                    <i class="demo-icon icon-checkfilterarticle" onclick="updateSubscription();"></i>
                    <i class="demo-icon icon-cancel"></i>
                  </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 sub-section right-section" id="removeUserBlock">
                <div class="card-title">
                  <h3>Remove User</h3>
                </div>
                <p>You can remove user. Once removed, they can't be restored.</p>
                <div class="remove-ele-block">
                    <button type="button" class="btn btn-info waves-effect redBtCustom">Remove</button>
                </div>
                <div class="card-footer"></div>
                <div class="remove-step-2">
                  <div class="remove-notify">User will be removed from your institution immediately and would not be able to access the Directory.</div>
                  <div class="remove-error">Are you sure you want to Remove User?</div>
                  <button type="button" class="btn btn-info waves-effect redBtCustom" id="removeReqBtn" onclick="removeUserFinal();">Remove</button>
                  <button type="button" class="btn btn-info waves-effect whiteBtCustom" id="cancelRemoveReqBtn">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="card block" id="audits">
            <div id="auditLoader" class="loader"></div>
            <div class="card-title">
              <span class="history-dropdown"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
              <h3 class="active available_oncall"><a href="javascript: getAvailableOncallTransaction('<?php echo isset($user_id)? $user_id :'0'; ?>');">Available and On Call Transactions</a></h3>
              <h3 class="screenshot"><a href="javascript: getScreenshotTransaction('<?php echo isset($user_id)? $user_id :'0'; ?>');">Screenshot Transactions</a></h3>
            </div>
            <div class="table-responsive wordBreak" id="ajaxAvailableContent">
              <table class="table stylish-table customPading">
                <thead>
                  <tr>
                    <th style="width:20%; text-align:center;">Type</th>
                    <th style="width:20%; text-align:center;">Status</th>
                    <th style="width:20%; text-align:center;">Platform</th>
                    <th style="width:20%; text-align:center;">Modified Date</th>
                    <th style="width:20%; text-align:center;">Modified Time</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card block" id="device-history">
            <div id="deviceLoader" class="loader"></div>
            <div class="card-title">
              <span class="history-dropdown"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
              <h3 class="deviceInfo active"><a href="javascript: deviveInfo('<?php echo isset($user_id)? $user_id :'0'; ?>');">User Device Info: Currently Logged In</a></h3>
              <h3 class="deviceHistory"><a href="javascript: deviveHistory('<?php echo isset($user_id)? $user_id :'0'; ?>');">User Device Info: History</a></h3>
            </div>
            <div class="table-responsive wordBreak" id="ajaxCurrentDeviceContent">
              <table class="table stylish-table customPading">
                <thead>
                  <tr>
                    <th>Currently Logged In in Device Info</th>
                    <th>Web Device</th>
                    <th>Mobile Device</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="table-responsive wordBreak" id="ajaxDeviceHistoryContent"></div>
          </div>
        </div>
      </div>

     </div>
  </div>
  <!-- ============================================================== -->
  <!-- End HTML Content -->
  <!-- ============================================================== -->

</div>

<script type="text/javascript">
  $("#sidebarnav>li#menu1").addClass('active').find('a').addClass('active');
  var typingTimerRole;                //timer identifier
  var doneTypingIntervalBaton = 1500;  //time in ms, 5 second for example
  var $role_input = $('#search_unoccupied_role');

  $role_input.on('keyup', function () {
    clearTimeout(typingTimerRole);
    typingTimerRole = setTimeout(searchRole, doneTypingIntervalBaton);
  });
  $role_input.on('keydown', function () {
    clearTimeout(typingTimerRole);
  });
  var restrictClass = 'eventOff';
  $(document).ready(function () {
      var admin_role_id = '<?php echo $_COOKIE['adminRoleId']; ?>';
      if(admin_role_id == 1){
        restrictClass = 'subadmin';
      }
      var user_id = $('#user_id').html();
      var email = $('span#user_email').html();
      var on_call_access = $('span#user_oncallaccess').html();
      usersubscriptionexpiryDate(user_id);
      personalInformationAjax(user_id);
      userStatusAjax(user_id);
      userPrivilegeAjax(user_id,email);
      getAvailableOncallTransaction(user_id);
      deviveInfo(user_id);
      getPermanentList(user_id);
      getBatonList(user_id);

      if(admin_role_id == 1){
        $('#audits, #device-history').hide();
        $('#removeUserBlock, #userInfoBlock, #permanentBlock, #batonBlock, #user-status-block, #user-privilege-block').addClass(restrictClass);
      }
      if(on_call_access == 0){
        $('#batonBlock').addClass(restrictClass);
      }


      // --- Active menu in Available and Oncall Transaction [START]
      var date = new Date();
      date.setTime(date.getTime()+(10 * 60 * 1000));
      var expires = "; expires="+date.toGMTString();
      document.cookie = "av/on=info"+expires+"; path=/";
      // --- Active menu in Available and Oncall Transaction [END]

      $.getJSON('<?php echo BASE_URL; ?>institution/country_code.json', function( data ) {
          var list="";
          $.each( data, function( key, val ) {
            list += '<option data-countrycode="'+val.iso2_cc+'" value="'+val.e164_cc+'">'+val.e164_cc+'</option>';
          });

          $('#countrySelect').html(list);

          var country_code = '<?php echo $country_code; ?>';
          console.log(country_code);
          var code = $('#countrySelect option[data-countrycode="'+country_code+'"]').val();
          var text = $('#countrySelect option[data-countrycode="'+country_code+'"]').text();
          // $('#countrySelect').val(code);
          $('#countrySelect option[data-countrycode="'+country_code+'"]').attr("selected","selected");
          $(".country-selected").html(text);

          $("select#countrySelect").change(function(){
            // updateSignInButtonUI();
              var selectedOption = $(this).find(":selected").text();
              $(".country-selected").html(selectedOption);
          }).trigger('change');

          // $('body').on('click','.country-select',function(){
              $('#countrySelect').focus();
          // });
      });

  });

  function searchRole() {
    try {window.searchBaton.abort();} catch (e) {}
    $('#batonRolePopUp .showWarning').hide();
    $('#batonRolePopUp .showSucess').hide();
     $search_text = $role_input.val().trim();
     if($search_text == ''){
       $('#batonListing').hide();
       return false;
     }
     $('#addbatonRoleLoader').fadeIn('fast');
      window.searchBaton = $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUnoccupiedBatonRole',
        type: "POST",
        data: {'search_text':$search_text},
        success: function(data) {
          console.log(data);
          var responseArray = JSON.parse(data);
          if( responseArray['status'] == 1 ){
            var html = '';
            var list = responseArray['data'];
            var oncall = 'No';
            if(list.length > 0){
              html += '<div><span class="role_name head">Role Name</span><span class="role_type head">On Call</span></div>';
              for (var i = 0; i < list.length; i++) {
                if(list[i].on_call_value == 1){
                  oncall = 'Yes';
                }else{
                  oncall = 'No';
                }
                html +='<div><li class="role_name" data-id="'+list[i].id+'" data-oncall="'+list[i].on_call_value+'">'+list[i].name+'</li><li class="role_type">'+oncall+'</li></div>';
              }
            }else{
              html +='<div style="text-align: center;color: red;">No Record(s) Found!</div>';
            }
            $('#batonListing').show();
            $('#addbatonRoleLoader').fadeOut('fast');
            $('#baton_unoccupied_role').html(html);
          }
        }
      });
  }

  $('body').on('click','#batonRolePopUp #baton_unoccupied_role li',function(){
    $('#batonRolePopUp #baton_unoccupied_role div').removeClass('selected');
    $(this).parent('div').addClass('selected');
    $('#batonRolePopUp .showBack').hide();
    $('#batonRolePopUp .showSucess').hide();
    $('#batonRolePopUp .showWarning').show();
    $('#batonRolePopUp').attr('data-status',1);
    $('#batonRolePopUp').attr('data-roleid',$(this).data('id'));
  });

  function setBatonRole(){
    var role_id = $('#batonRolePopUp').attr('data-roleid');
    var status = $('#batonRolePopUp').attr('data-status');
    changeUserBatonStatus(role_id,status);
  }

  function changeUserBatonStatus(role_id,status){
    var user_id = $('#user_id').html();
    var return_type = 'false';

    if(status == 0){
      var conform = confirm("Are you sure, you want to unassign Baton Role.");
      if(conform == true){
        $('#batonLoader').fadeIn('fast');
        return_type = 'true';
      }else{
        return false;
      }
    }else{
      return_type = 'true';
      $('#addbatonRoleLoader').fadeIn('fast');
    }
    if(return_type == 'false' || return_type == false){
      return false;
    }else{
      updateBaton(role_id,status);
    }
  }

  function updateBaton(role_id,status){
    var user_id = $('#user_id').html();
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/assignUnassignBatonRole',
        type: "POST",
        data: {'user_id':user_id, 'role_id':role_id,'status':status},
        success: function(data) {
          console.log(data);
          if(status == 0){
            getBatonList(user_id);
          }else{
            $('#addbatonRoleLoader').fadeOut('fast');
          }
          $role_input.val('');
          $('#batonListing').hide();
          $('#batonRolePopUp .showWarning').hide();
          $('#batonRolePopUp .showSucess').show();
          $('#batonRolePopUp .main_content').show();
          $('#batonRolePopUp #batonListing').hide();
        }
    });
  }
  $('#batonRolePopUp').on('show.bs.modal', function (event) {
    $role_input.val('');
    $('#batonRolePopUp .showWarning').hide();
    $('#batonRolePopUp .showSucess').hide();
    $('#batonRolePopUp .showBack').hide();
    $('#batonRolePopUp #batonListing').hide();
    $('#addbatonRoleLoader').fadeOut('fast');
  });
  $('#batonRolePopUp').on('hide.bs.modal', function (event) {
    var user_id = $('#user_id').html();
    $('#addbatonRoleLoader').fadeOut('fast');
    if($('#batonRolePopUp .showSucess').css('display')=='block'){
      getBatonList(user_id);
    }
  });

  function editMode(element,var1,var2){
    switch (element) {
      case 'name':
        $('.first_name_edit').val(var1);
        $('.last_name_edit').val(var2);
        break;
      case 'email':
        $('.email_edit').val(var1);
        break;
      case 'phone':
        $('.phone_edit').html(var1);
        $('.phone_edit_new').val('');
        break;
      case 'password':
        $('.password_edit').val('');
        break;
      default:

    }
    $('#personalInformationAjax').addClass('is-hidden');
    $('.edit-section').addClass('is-hidden');
    $('#'+element+'_editor').removeClass('is-hidden');
  }

  function closeEditMode(){
    var dom = $('.edit-section');
    var error_icon = dom.find('.error-icon');
    var success_icon = dom.find('.success-icon');
    var error_msg = dom.find('.error-msg');
    success_icon.addClass('is-hidden');
    error_icon.addClass('is-hidden');
    $('#personalInformationAjax').removeClass('is-hidden');
    $('.edit-section').addClass('is-hidden');
  }

  function personalInformationAjax(user_id){
    $('#piaLoader').fadeIn('fast');
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionUser/getUserPersonalInfo',
      type: "POST",
      data: {'user_id':user_id},
      success: function(data) {
        if(data == 'User Deleted'){
          $('.padreduce').hide();
          $('#customModalLabel').html('Access Denied!</p>');
          $('#customModalBody').addClass('bodyCustom').html('<span><img src="<?php echo BASE_URL ?>institution/img/sub-removeUser.svg" alt=""></span><p>You can\'t access the profile of this user, because this user removed from your institution.</p>').css('margin-bottom', 10);
          $('#customModalFoot').hide();
          $('#customBox').modal('show');
          setTimeout(function(){
            $('.window_history').click();
          }, 2000);
        }
        $('#piaLoader').fadeOut('fast');
        $('#personalInformationAjax').html(data);
        var login = $('#ajaxCurrentDeviceContent table.table').data('login');
        if(login == 1){
          $('span.login_status').addClass('loggedIn').removeClass('loggedOut').html('Logged In');
        }else{
          $('span.login_status').removeClass('loggedIn').addClass('loggedOut').html('Logged Out');
        }
        closeEditMode();
      },
      error: function( result ){
          $('#piaLoader').fadeOut('fast');
      }
    });
  }

  function userStatusAjax(user_id){
    $('#availableLoader').fadeIn('fast');
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserAvailableStatus',
      type: "POST",
      data: {'user_id':user_id},
      success: function(data) {
        $('#availableLoader').fadeOut('fast');
        var responseArray = JSON.parse(data);
        var atwork = responseArray['data']['at_work'],
            oncall = responseArray['data']['on_call'],
            total_batons = responseArray['data']['total_baton'],
            onCall_batons = responseArray['data']['On_call_batons'],
            dnd_status = responseArray['data']['dnd'];

        $('#total_baton').html(total_batons);
        $('#on_call_baton').html(onCall_batons);
        $('#dnd_status').html(dnd_status);

        if(oncall == 1){
          $('#userstatus_0').prop('checked', true);
          $('#userstatus_val').html(0);
        }else if(atwork == 1){
          $('#userstatus_1').prop('checked', true);
          $('#userstatus_val').html(1);
        }else{
          $('#userstatus_2').prop('checked', true);
          $('#userstatus_val').html(2);
        }
      },
      error: function( result ){
          $('#availableLoader').fadeOut('fast');
      }
    });
  }

  function userPrivilegeAjax(userid,email){
    $("#priviligeLoader").fadeIn("fast");
      $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserPrivilegeStatus',
          type: 'POST',
          data: {'user_email': email, 'user_id': userid},
          success: function( result ){
              $('#priviligeLoader').fadeOut('fast');
              var responseArray = JSON.parse(result);
              if( responseArray['status'] == 1 ){
                  $('input[name="privilige"][value="'+responseArray['data']['role_id']+'"]').prop('checked', true);
                  $('#privilige-val').html(responseArray['data']['role_id']);
              }
          },
          error: function( result ){
              $('#priviligeLoader').fadeOut('fast');
          }
      });
  }

  function usersubscriptionexpiryDate(userid){
    $("#subscriptionLoader").fadeIn("fast");
    $.ajax({
          url: '<?php echo BASE_URL; ?>institution/InstitutionUser/usersubscriptionexpiryDate',
          type: "POST",
          data: {'user_id':userid},
          success: function(result) {
            $('#subscriptionLoader').fadeOut('fast');
            var responseArray = JSON.parse(result);
            console.log(responseArray);
            var selfExpiryDate = responseArray['expiry_date'];
            var companyExpiryDate = responseArray['company_expiry_date'];
            var companySubscriptionType = responseArray['subscribe_type'];
            $('#usersubscriptionexpiryDate').attr('data-expirydate', selfExpiryDate).attr('data-expirytype', companySubscriptionType);
            $('#subscription_expiry_date').val(selfExpiryDate);
            if(companySubscriptionType != 1){
              $('#userInfoBlock, #permanentBlock, #batonBlock, #user-status-block, #user-privilege-block').addClass(restrictClass);
            }

            // $('#datepicker').data("DateTimePicker").date(new Date());
            $('#datepicker').data("DateTimePicker").minDate(moment().subtract(1,'d'));
            $('#datepicker').data("DateTimePicker").maxDate(companyExpiryDate);
          },
          error: function( data ){
              $("#subscriptionLoader").fadeOut("fast");
          }
      });

  }

  function getPermanentList(user_id){
    $("#permanentLoader").fadeIn("fast");
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionUser/getPermanentRoles',
        type: "POST",
        data: {'limit':5,'user_id': user_id,'section':'profile'},
        success: function(data) {
          $("#permanentLoader").fadeOut("fast");
          // $('.deviceInfo').removeClass('active');
          // $('.deviceHistory').addClass('active');
          $('#userPermanentRole').html(data);
        }
    });
  }

  function getBatonList(user_id){
    $("#batonLoader").fadeIn("fast");
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserBatonRole',
        type: "POST",
        data: {'user_id': user_id},
        success: function(data) {
          console.log(data);
          $("#batonLoader").fadeOut("fast");
          data = JSON.parse(data);
          manageBatonList(data.data, function(list){
            $('#userbatonRole').html(list);
          });
          // $('.deviceInfo').removeClass('active');
          // $('.deviceHistory').addClass('active');
        }
    });
  }

  function manageBatonList(data, callBack){
    html = '';
    var baton_data = data;
    if(baton_data.length > 0){
        for (var i = 0; i < baton_data.length; i++) {
          disabled_condition = baton_data[i].type != '0' ? 'disabled="true"' : '';
          if(baton_data[i].other_user_name.trim() == ''){
            baton_data[i].other_user_name = "Switchboard";
          }
          status =  baton_data[i].type == '1' ?
                    'Assigned' : baton_data[i].type == '2' ?
                    'Requested from '+baton_data[i].other_user_name : baton_data[i].type == '3' ?
                    'Request awaiting' : baton_data[i].type == '4' ?
                    'Transfer awaiting' : baton_data[i].type == '5' ?
                    'Transfering to '+baton_data[i].other_user_name : baton_data[i].type == '6' ?
                    'Request rejected' : baton_data[i].type == '7' ?
                    'Transfer rejected' : baton_data[i].type == '8' ?
                    'Request timeout' : baton_data[i].type == '9' ?
                    'Transfer rejected' : 'None';

          html += '<li>';
          html += '<span>'+baton_data[i].role_name+'</span>';
          if($.inArray(baton_data[i].type.toString(), ['1','2','5','7']) != -1) {
            html += '<a title="Click to unassign" onClick="changeUserBatonStatus('+baton_data[i].role_id+',0);" >'+status+'</a>';
          }else{
            html += '<span class="disabled">'+status+'</span>';
          }
          html += '</li>';
        }
    }else{
      html += '<li class="no_record" data-id=""><span><font color="red">No Baton Role(s) assigned to User.</font></span></li>';
    }
    return callBack(html);
  }

  function getAvailableOncallTransaction(user_id){
    $("#auditLoader").fadeIn("fast");
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/getTransactionFilter',
        type: "POST",
        data: {'limit':5,'user_id': user_id,'section':'profile'},
        success: function(data) {
          $("#auditLoader").fadeOut("fast");
          $('.screenshot').removeClass('active');
          $('.available_oncall').addClass('active');
          $('#ajaxAvailableContent').html(data);
        }
    });
  }

  function getScreenshotTransaction(user_id){
    $("#auditLoader").fadeIn("fast");
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/screenShotFilter',
        type: "POST",
        data: {'limit':5,'user_id': user_id,'section':'profile'},
        success: function(data) {
          $("#auditLoader").fadeOut("fast");
          $('.screenshot').addClass('active');
          $('.available_oncall').removeClass('active');
          $('#ajaxAvailableContent').html(data);
        }
    });
  }

  function deviveInfo(user_id){
    $("#deviceLoader").fadeIn("fast");
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionUser/deviceFilter_view',
        type: "POST",
        data: {'user_id': user_id,'section':'profile'},
        success: function(data) {
          $("#deviceLoader").fadeOut("fast");
          $('.deviceInfo').addClass('active');
          $('.deviceHistory').removeClass('active');
          $('#ajaxCurrentDeviceContent').html(data);
          var login = $('#ajaxCurrentDeviceContent table.table').data('login');
          if(login == 1){
            $('span.login_status').addClass('loggedIn').removeClass('loggedOut').html('Logged In');
          }else{
            $('span.login_status').removeClass('loggedIn').addClass('loggedOut').html('Logged Out');
          }
        }
    });
  }

  function deviveHistory(user_id){
    $("#deviceLoader").fadeIn("fast");
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionUser/deviceFilter',
        type: "POST",
        data: {'limit':5,'user_id': user_id,'section':'profile'},
        success: function(data) {
          $("#deviceLoader").fadeOut("fast");
          $('.deviceInfo').removeClass('active');
          $('.deviceHistory').addClass('active');
          $('#ajaxCurrentDeviceContent').html(data);
        }
    });
  }

  function updateUser(validation_type){
    var user_id = $('#user_id').html();
    var email = $('#user_email').html();
    var dom = '';
    if(validation_type == 'email'){
      dom = $('#email_editor');
      return_data = emailValidate();
    }else if(validation_type == 'name'){
      dom = $('#name_editor');
      return_data = nameValidate();
    }else if(validation_type == 'phone'){
      dom = $('#phone_editor');
      return_data = phoneValidate();
    }else if(validation_type == 'password'){
      dom = $('#password_editor');
      return_data = passwordValidate();
    }
    return_data.type = validation_type;
    return_data.user_id = user_id;

    var error_icon = dom.find('.error-icon');
    var success_icon = dom.find('.success-icon');
    var error_msg = dom.find('.error-msg');

    if(return_data.status == 1){
      success_icon.addClass('is-hidden');
      error_icon.addClass('is-hidden');
      $('#piaLoader').fadeIn('fast');
      $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionUser/userDataUpdate',
        type: 'POST',
        data: {'user_id': user_id,'user_email':email,'return_data': JSON.stringify(return_data)},
        success: function( result ){
          $('#piaLoader').fadeOut('fast');
          console.log(result);
          result = JSON.parse(result);
          if(result.status == 1){
            closeEditMode();
            personalInformationAjax(user_id);
          }else{
            error_icon.removeClass('is-hidden');
            error_msg.html(result.message);
          }
        },
        error: function(){
            $('#piaLoader').fadeOut('fast');
            // msgshow('default',0,$('#user-status-block'));
        }
      });
    }
  }

  function emailValidate(){
    var dom = $('#email_editor');
    var email = dom.find('.email_edit');
    var success_icon = dom.find('.success-icon');
    var error_icon = dom.find('.error-icon');
    var error_msg = dom.find('.error-msg');
    var exist_email = '';
    var return_type = {};
    return_type.status = 0;
    return_type.value = email.eq(0).val().trim();

    success_icon.addClass('is-hidden');
    error_icon.addClass('is-hidden');

    if(email.eq(0).val().trim() == '' && email.eq(1).val().trim() == ''){
      error_icon.removeClass('is-hidden');
      error_msg.html('Please enter the Email.');
    }else if(email.eq(0).val().trim() != email.eq(1).val().trim()){
      error_icon.removeClass('is-hidden');
      error_msg.html('Email that you\'ve entered doesn\'t match.');
    }else{
      success_icon.removeClass('is-hidden');
      return_type.status = 1;
    }
    return return_type;
  }

  function nameValidate(){

    var dom = $('#name_editor');
    var first_name = dom.find('.first_name_edit').val().trim();
    var last_name = dom.find('.last_name_edit').val().trim();
    var error_msg = dom.find('.error-msg');
    var error_icon = dom.find('.error-icon');
    var exist_email = '';
    var return_type = {};
    return_type.status = 0;

    first_name = first_name.charAt(0).toUpperCase() + first_name.slice(1);
    last_name = last_name.charAt(0).toUpperCase() + last_name.slice(1);

    return_type.first_name = first_name;
    return_type.last_name = last_name;
    return_type.value = first_name+' '+last_name;

    error_icon.addClass('is-hidden');

    if(first_name == ''){
      error_icon.removeClass('is-hidden');
      error_msg.html('Please enter First Name.');
    }else if(last_name == ''){
      error_icon.removeClass('is-hidden');
      error_msg.html('Please enter Last Name.');
    }else{
      return_type.status = 1;
    }
    return return_type;
  }

  function phoneValidate(){
    var dom = $('#phone_editor');
    var phone = dom.find('.phone_edit_new');
    var success_icon = dom.find('.success-icon');
    var error_icon = dom.find('.error-icon');
    var error_msg = dom.find('.error-msg');
    var return_type = {};
    return_type.status = 0;
    return_type.value = $('#countrySelect').val()+''+phone.val().trim();

    success_icon.addClass('is-hidden');
    error_icon.addClass('is-hidden');

    if(phone.val().trim() == ''){
      error_icon.removeClass('is-hidden');
      error_msg.html('Please enter the Phone No.');
    }else{
      success_icon.removeClass('is-hidden');
      return_type.status = 1;
    }
    return return_type;
  }

  function passwordValidate(){
    var dom = $('#password_editor');
    var password = dom.find('.password_edit');
    var success_icon = dom.find('.success-icon');
    var error_icon = dom.find('.error-icon');
    var error_msg = dom.find('.error-msg');
    var return_type = {};
    return_type.status = 0;
    return_type.value = password.eq(0).val().trim();

    success_icon.addClass('is-hidden');
    error_icon.addClass('is-hidden');
    var passReg ="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!#$%&'()*+,-./:;<=>?@^\\[\\]\"_`{|}~\\\\]).{8,}$";

    if(password.eq(0).val().trim() == '' && password.eq(1).val().trim() == ''){
      error_icon.removeClass('is-hidden');
      error_msg.html('Please enter the Password');
    }else if(!password.eq(0).val().trim().match(passReg)){
      error_icon.removeClass('is-hidden');
      error_msg.html('Enter a valid Password.');
    }else if(password.eq(0).val().trim() != password.eq(1).val().trim()){
      error_icon.removeClass('is-hidden');
      error_msg.html('Password that you\'ve entered doesn\'t match.');
    }else{
      success_icon.removeClass('is-hidden');
      return_type.status = 1;
    }
    return return_type;

  }

  function updateStatus(){
    $("#availableLoader").fadeIn("fast");
    var userid = $('span#user_id').html();
    userstatus = $('input[name="userstatus"]:checked').val();
    onCallVal = 0;
    atWorkVal = 0;

    if(userstatus == 0){
      onCallVal = 1;
      atWorkVal = 1;
    }
    if(userstatus == 1){
      atWorkVal = 1;
    }
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/updateOnCallStatus',
      type: 'POST',
      data: {'user_id':userid, 'on_status': onCallVal, 'at_status': atWorkVal},
      success: function( result ){
          $('#availableLoader').fadeOut('fast');
          var responseArray = JSON.parse(result);
          if( responseArray['status'] == 1 ){
            $('#userstatus_val').html(userstatus);
            $('.card.block').removeClass('active unactive');
            personalInformationAjax(userid);
            msgshow(responseArray['message'],1,$('#user-status-block'));
          }else{
            msgshow('default',0,$('#user-status-block'));
          }

      },
      error: function( result ){
          $('#availableLoader').fadeOut('fast');
          msgshow('default',0,$('#user-status-block'));
      }
    });
  }

  function updateSubscription(){
      var user_id = $('#user_id').html();
      var email = $('span#user_email').html();
      var exdate = $("#subscription_expiry_date").val();
      $('#subscriptionLoader').fadeIn('fast');

      $.ajax({
          url: '<?php echo BASE_URL; ?>institution/InstitutionHome/updateSubscription',
          type: 'POST',
          data: {'email' : email,'user_id' : user_id,'custom_date' : exdate},
          success: function( result ){
            $('#subscriptionLoader').fadeOut('fast');
            // $('.sub-section').removeClass('active unactive');
            // usersubscriptionexpiryDate(user_id);
            msgshow('Subscription updated successfully.',1,$('#user-expiry-date'));
            setTimeout(function(){
              location.reload();
            },1000);
          },
          error: function( result ){
              $('#subscriptionLoader').fadeOut('fast');
               var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
               $('#customModalLabel').html('Error');
               $('#customModalBody').html('An error has occurred. Please try again');
               $('#customModalFoot').html(buttonHtml);
               $('#customBox').modal('show');
               $("#customBox .loaderImg").hide();

              $(".loading_overlay").hide();
          }
      });
  }

  function setPrivilege(){
    var email = $('#user_email').html();
    var role_id = $('input[name="privilige"]:checked').val();
    $('#priviligeLoader').fadeIn('fast');
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/setUserPrivilege',
      type: "POST",
      data: {'email':email,'role_id':role_id},
      success: function(data) {
        var responseArray = JSON.parse(data);
        $('#priviligeLoader').fadeOut('fast');
        if(responseArray['status'] == 1){
          $('#privilige-val').html(role_id);
          msgshow('Privilege changed successfully.',1,$('#user-privilege-block'));
        }else{
          msgshow('default',0,$('#user-privilege-block'));
        }
      },
      error: function(result){
        msgshow('default',0,$('#user-privilege-block'));
        $('#priviligeLoader').fadeOut('fast');
      }
    });
  }

  function removeUserFinal(){
    var email = $('#user_email').html();
    var userid = $('span#user_id').html();

    $("#customBox .loaderImg").show();

    $.ajax({
      url: '<?php echo BASE_URL; ?>institution/InstitutionHome/removeUser',
      type: 'POST',
      data: {'email': email, 'user_id': userid},
      success: function( result ){
        console.log(result);
        var responseArray = JSON.parse(result);
        if( responseArray['status'] == 1 ){
          $('.remove-error').html(responseArray['message']);
          $('#removeUserBlock .remove-step-2 .btn-info.redBtCustom').hide();
          setTimeout(function(){
            window.location.href = "<?php echo BASE_URL; ?>institution";
          }, 2000);
        }else if(responseArray['status'] == 2){
          msgshow(responseArray['message'],0,$('#removeUserBlock'));
        }else{
          msgshow('default',0,$('#removeUserBlock'));
        }
      },
      error: function( result ){
        msgshow('default',0,$('#removeUserBlock'));
      }
    });
  }

  function msgshow(msg,status,container){
    html = '';
    if(status == 1){
      html = '<div class="card-msg card-success"><span>'+msg+'</span></div>';
      container.find('.card-footer').append(html);
    }else{
      html = '<div class="card-msg card-error"><label>Error!</label> ';
      if(msg == 'default'){
        html += '<span>An error has occurred. Please try again.</span>';
      }else{
        html += '<span>'+msg+'</span>';
      }
      html += '</div>';
      container.find('.card-footer').append(html);
    }
    $('.sub-section').removeClass('active unactive');
    $('.card-msg').show();
    $('.card.block').removeClass('active unactive');
    setTimeout(function(){
      $('.card-msg').remove();
    }, 3000);
  }


  $(function () {
    $("#datepicker").datetimepicker({
        format: 'DD-MM-YYYY',
        useCurrent: false
    });

    $('body').on('focus','#subscription_expiry_date',function(){
      $(this).find('span.input-group-addon').click();
    });

    $("#datepicker").on("dp.change", function (e) {
      console.log(e);
        $('#customSubscriptionPopUp .showDefault').hide();
        $('#customSubscriptionPopUp .showWarning').show();
        $('#customSubscriptionPopUp .showSucess').hide();
    });

    // $('body').on('hover','.eventOff',function(event){
    // // $("#myId").hover(function() {
    //   var subscription_type = $('#usersubscriptionexpiryDate').attr('data-expirytype');
    //   var approved_type = $('#userApprovedStatus').attr('data-status');
    //   var message = '';
    //   if(approved_type != 1){
    //     message = "Please approved user to edit the profile.";
    //   }else if(subscription_type != 1){
    //     message = "Please extend the subscription period of user to edit profile.";
    //   }
    //     $(this).css('cursor','pointer').attr('title', message);
    // }, function() {
    //     $(this).css('cursor','auto');
    // });
    //
    $('body').on('click','.eventOff',function(event){
      event.preventDefault();
      var subscription_type = $('#usersubscriptionexpiryDate').attr('data-expirytype');
      var approved_type = $('#userApprovedStatus').attr('data-status');
      var on_call_access = $('span#user_oncallaccess').html();
      if(approved_type != 1){
        alert("Please approve User to edit the profile.");
      }else if(subscription_type != 1){
        alert("Please extend the subscription period of User to edit profile.");
      }else if(on_call_access == 0){
        alert("Students can't have the Baton Role(s).");
      }
      closeEditMode();
      return false;
    });

    $('body').on('click','.subadmin',function(event){
      event.preventDefault();
      alert("You are not allowed to perform this action.");
      closeEditMode();
      return false;
    });

    // $('.sub-section').on('mouseover', function(){
    //   if($(this).attr('id') != 'device-history' && !$(this).hasClass('notActive')){
    //     $('.sub-section').addClass('disabled');
    //     $(this).removeClass('disabled');
    //   }
    // });
    //
    // $('.sub-section').on('mouseout', function(){
    //   $('.sub-section').addClass('disabled');
    //   $('.sub-section').removeClass('disabled');
    // });

    $('input[name="userstatus"]').on('click', function(){
      var subscription_type = $('#usersubscriptionexpiryDate').attr('data-expirytype');
      // var professionid = $('#professionid').attr('data-profession');
      var on_call_access = $('span#user_oncallaccess').html();
      var totalbaton = $('#total_baton').html();
      var oncallbaton = $('#on_call_baton').html();
      var old_val = $('#userstatus_val').html();
      console.log(old_val);

      $('.card-msg').remove();

      if($('#userstatus_val').html() != $('input[name="userstatus"]:checked').val()){
        if(oncallbaton > 0 && $(this).val() != 0 ){
          setTimeout(function(){
            $('input[name="userstatus"]').prop('checked', false).removeAttr('checked').attr('checked',false);
            $('#userstatus_'+old_val).prop('checked', true);
          },0);
          msgshow('User having active On Call Baton Role(s).',0,$('#user-status-block'));
          return false;
        }else if(totalbaton > 0 && $(this).val() == 2){
          setTimeout(function(){
            $('input[name="userstatus"]').prop('checked', false).removeAttr('checked').attr('checked',false);
            $('#userstatus_'+old_val).prop('checked', true);
          },0);
          msgshow('User having active Baton Role(s).',0,$('#user-status-block'));
          return false;
        }
        if($(this).val() == 0){
          if(subscription_type == 2){
            setTimeout(function(){
              $('input[name="userstatus"]').prop('checked', false).removeAttr('checked').attr('checked',false);
              $('#userstatus_'+old_val).prop('checked', true);
            },0);
            msgshow("You can't go 'On Call' in 'Grace Peroid'.",0,$('#user-status-block'));
            return false;
          }else if(on_call_access == 0){
            setTimeout(function(){
              $('input[name="userstatus"]').prop('checked', false).removeAttr('checked').attr('checked',false);
              $('#userstatus_'+old_val).prop('checked', true);
            },0);
            msgshow("Students can't go 'On Call'.",0,$('#user-status-block'));
            return false;
          }
        }
        $('#status-error span').html("");
        $('#status-error').hide();
      }else{
        $('#status-error span').html("");
        $('#status-error').hide();
      }

    });

    $('input[name="userstatus"]').on('change', function(){
      if($('#userstatus_val').html() != $('input[name="userstatus"]:checked').val()){
        $('.sub-section').removeClass('active').addClass('unactive');
        $(this).parents('.sub-section').addClass('active').removeClass('unactive');
      }else{
        $('.sub-section').removeClass('active unactive');
      }
    });

    $('input[name="privilige"]').on('change', function(){
      if($('#privilige-val').html() != $('input[name="privilige"]:checked').val()){
        $('.sub-section').removeClass('active').addClass('unactive');
        $(this).parents('.sub-section').addClass('active').removeClass('unactive');
      }else{
        $('.sub-section').removeClass('active unactive');
      }
    });

    $('#user-expiry-date .input-group.date .input-group-addon').on('click', function(){
      $('.sub-section').removeClass('active').addClass('unactive');
      $(this).parents('.sub-section').addClass('active').removeClass('unactive');
    });

    $('.card-update i.demo-icon.icon-cancel').on('click', function(){
      $('.sub-section').removeClass('active unactive');
      $('input[name="userstatus"][value="'+$('#userstatus_val').html()+'"]').prop('checked', true);
      $('input[name="privilige"][value="'+$('#privilige-val').html()+'"]').prop('checked', true);
      $('#subscription_expiry_date').val($('#usersubscriptionexpiryDate').attr('data-expirydate'));
    });

    $('.remove-ele-block button').on('click', function(){
      if($('#removeUserBlock').hasClass('eventOff') || $('#removeUserBlock').hasClass('subadmin')){
        $('#removeUserBlock').click();
        return false;
      }
      $('.sub-section').removeClass('active').addClass('unactive');
      $(this).parents('.sub-section').addClass('active').removeClass('unactive');
    });

    $('#cancelRemoveReqBtn').on('click', function(){
      $('.sub-section').removeClass('active unactive');
    });

    $('.sub-section .card-title .edit-link i').on('click', function(){
      $('.sub-section').removeClass('active').addClass('unactive');
      $(this).parents('.sub-section').addClass('active').removeClass('unactive');
    });

    $('.history-dropdown').on('click', function(){
      $(this).parent('.card-title').toggleClass('opened');
    });

  });
</script>
