<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-12 align-self-center col-5">
          <h3 class="text-themecolor m-b-0 m-t-0 col-1 window_history"><img src="<?php echo BASE_URL; ?>institution/img/left-arrow.svg" /></h3>
          <h3 class="text-themecolor m-b-0 m-t-0 col-11" style="float: left;">Export Chat</h3>
            <!-- <h3 class="text-themecolor m-b-0 m-t-0">Export Chat</h3> -->
            <!-- <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php // echo BASE_URL ?>institution/InstitutionHome/index">Home</a></li>
                <li class="breadcrumb-item active">Chat List</li>
            </ol> -->
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">Chat List</li> -->
      </ol>
    </div>


    <!-- ============================================================== -->
  <!-- Start HTML Content -->
  <!-- ============================================================== -->
  <div class="row boaderLines">
     <div class="col-12">
        <div class="card">
           <div class="card-block padLeftRight0 pad0">
              <div class="floatLeftFull padLeftRight14 newListStyle">
                 <div class="tableTop floatLeft marTop10">
                    <span class="card-title">Select <a href="javascript:void(0);"><?php echo isset($name)? $name :'User'; ?>'s</a> chat to export</span>
                    <span id="user_id" style="display: none;"><?php echo isset($id)? $id :'User'; ?></span>
                 </div>
                 <div id="userlisting_filter" class="dataTables_filter">
                    <label><i class="demo-icon icon-sub-lense"></i></label>
                    <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                 </div>
              </div>
              <div class="table-responsive wordBreak" id="ajax_dialogs">
                 <table class="table stylish-table customPading">
                    <thead>
                       <tr>
                          <th>Photo </th>
                          <th>Chat Name</th>
                          <th>Last Message </th>
                          <th>Date</th>
                          <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <td colspan="8" align="center">
                              <font color="green">Please wait while we are loading the list...</font>
                          </td>
                      </tr>
                    </tbody>
                 </table>
              </div>
           </div>
        </div>
     </div>
  </div>
  <!-- ============================================================== -->
  <!-- End HTML Content -->
  <!-- ============================================================== -->

</div>
<script type="text/javascript">
    $(document).ready(function () {
        showUserDialogues();
    });

    $("#sidebarnav>li#menu2").addClass('active').find('a').eq(0).addClass('active');
    $("#sidebarnav>li#menu2").find("li").eq(0).addClass('active').find('a').addClass('active');
    $("li.dropdown .dropdown-menu li a").click(function (e) {
        e.stopPropagation();
    });

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#user_search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(dialogSearch, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function dialogSearch() {
      $search_text = $input.val().trim();
      if($search_text == ''){
          // return false;
      }
       $input.blur();
       $(".loader").fadeIn("fast");
       $('#emptyData').hide();
       $("#ajax_dialogs td.dialog_name").each(function () {
           $(this).parents('tr').show();
           if($(this).html().toLowerCase().indexOf($search_text.toLowerCase()) == -1){
               $(this).parents('tr').hide();
           }
       });

       if($('#ajax_dialogs').find("td.dialog_name:visible").length == 0){
        $('#emptyData').show();
       }
       $(".loader").fadeOut("fast");
    }

    function showUserDialogues(){
        $(".loader").fadeIn("fast");
        var userid = $('span#user_id').html();
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionUser/showDialogues',
            type: "POST",
            data: {'userid':userid},
            success: function(data) {
                $(".loader").fadeOut("fast");
                if(data == ''){
                    $('#ajax_dialogs').html('No chats are available.');
                    $('#exampleInputPassword1').val('');
                }else if( IsJsonString(data) == false ){
                    $('#ajax_dialogs').html(data);
                    $search_text = $input.val().trim();
                    if($search_text != ''){
                        dialogSearch();
                    }
                }else{
                    var responseArray = JSON.parse(data);
                    if(responseArray['status'] == 2){
                        $("#passwordBlock").addClass('has-danger');
                        $("#passwordError").show();
                        $("#emptyPassword").hide();
                        return false;
                    }else{
                        $('#ajax_dialogs').html(responseArray['message']);
                        $("#customBox .loaderImg").hide();
                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Confirmation');
                        $('#customModalBody').html(responseArray['message']);
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');

                        // alert(responseArray['message']);
                    }
                }
            },
            error: function( data ){
                $(".loader").fadeOut("fast");
            }
        });
    }
    function showUserChats($dialogId){
        $(".loader").fadeIn("fast");
        var expires = "";
        var date = new Date();
        date.setTime(date.getTime()+(10 * 60 * 60 * 1000));
        expires = "; expires="+date.toGMTString();

        document.cookie = "user_id="+$('span#user_id').html()+expires+"; path=/";
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionUser/exportForm/" + $dialogId;
    }
    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
</script>
