<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajax_dialogs',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader" style="display: none;"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Export Chat</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>institution/InstitutionHome/index">Home</a></li>
                <li class="breadcrumb-item active">Export Chat</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row boaderLines widgetMain">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <p>To export <a href="javascript:void(0);"><?php echo isset($name)? $name :'User'; ?></a> Chat history, you have to enter your profile password due to security reason.</p>
                    <span id="user_id" style="display: none;"><?php echo isset($id)? $id :'User'; ?></span>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <form>
                                <div class="form-group" id="passwordBlock" >
                                    <label for="exampleInputPassword1">Enter your password</label>
                                    <input type="text" autocorrect="off" autocapitalize="off" autocomplete="off" class="custom_m_Password form-control" id="exampleInputPassword1" placeholder="Password">
                                    <div class="form-control-feedback" id="emptyPassword" style="display:none">Please enter password</div>
                                    <div class="form-control-feedback" id="passwordError" style="display:none">Please enter correct password</div>
                                </div>
                                <div class="form-group" id="dateBlock" >
                                    <label for="example-date-input">Select date range</label>
                                    <div class="col-12 pad0">
                                        <span class="form-control col-5 col-form-label">
                                            <input value="" id="datetimepicker6" type="text" autocorrect="off" autocapitalize="off" autocomplete="off">
                                        </span>
                                        <label for="example-date-input" class="col-1 col-form-label pad0 floatNone txtCenter">To</label>
                                        <span class="form-control col-5 col-form-label">
                                            <input value="" id="datetimepicker7" type="text" autocorrect="off" autocapitalize="off" autocomplete="off">
                                        </span>

                                        <div class="form-control-feedback" id="fromDateError" style="display:none">Please enter from date</div>
                                        <div class="form-control-feedback" id="toDateError" style="display:none">Please enter to date</div>
                                        <div class="form-control-feedback" id="incorrectDate" style="display:none">To date must be greater than from date</div>
                                    </div>
                                </div>
                                <div class="form-group" id="reasonBlock" >
                                    <label>Mention reason to export chat</label>
                                    <textarea class="form-control marBottom10px" rows="5" id="reasonToExport"></textarea>
                                    <div class="form-control-feedback" id="reasonError" style="display:none">Please enter reason to export chat</div>
                                    <div class="col-12 pad0">
                                        <div id="dialogListShow1" class="btn btn-primary" onclick="showUserDialogues()">Proceed</div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <div id="dialogListView" class="row boaderLines" style="display: none;">
        <div class="col-12">
            <div class="card">
                <div class="card-block padTop30 padBottom0">
                    <h4 class="card-title floatLeftFull">Select chat to export</h4>
                    <div id="dialog_filter" class="dataTables_filter">
                        <label>Search:<input id="dialog_search" type="search" class="form-control input-sm" placeholder="" aria-controls="dialogListing"></label>
                    </div>
                    <div class="dropdown-menu2 mailbox">
                        <div id="ajax_dialogs" class="message-center2">
                        </div>
                        <div id="emptyData" style="display: none; text-align: center;">
                            <font color="red">No Record(s) Found!</font>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script>
    $(document).ready(function () {
        $("#sidebarnav>li#menu1").addClass('active').find('a').addClass('active');
        $("li.dropdown .dropdown-menu li a").click(function (e) {
            e.stopPropagation();
        });
        $('#dialogListShow').click(function () {
            $('#dialogListView').show();
            $(this).addClass('disabled');
        });
        $('#dialog_search').val('');
    });
    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#dialog_search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(dialogSearch, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function dialogSearch() {
       $search_text = $input.val().trim();
       if($search_text == ''){
           // return false;
       }
       $input.blur();
       $(".loader").fadeIn("fast");
       $('#emptyData').hide();
       $("#ajax_dialogs a").each(function () {
           $(this).show();
           if($(this).find("h5").html().toLowerCase().indexOf($search_text.toLowerCase()) == -1){
               $(this).hide();
           }
       });
        if($('#ajax_dialogs').find("a:visible").length == 0){
            $('#emptyData').show();
        }
       $(".loader").fadeOut("fast");
    }
</script>

<script type="text/javascript">
    $(function () {

        $('#datetimepicker6').datetimepicker({
            format: 'DD/MM/YYYY',
            defaultDate: new Date(),
        });
        $('#datetimepicker7').datetimepicker({
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#datetimepicker6').data("DateTimePicker").maxDate(new Date());

        $('#datetimepicker7').data("DateTimePicker").maxDate(new Date());

        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>
<script type="text/javascript">
    function showUserDialogues(){
        var fromDate = $('#datetimepicker6').val();
        var toDate = $('#datetimepicker7').val();
        var password = $('#exampleInputPassword1').val().trim();
        var reason = $('#reasonToExport').val().trim();

        if(password==""){
            $("#passwordBlock").addClass('has-danger');
            $("#emptyPassword").show();
            $("#passwordError").hide();
            return false;
        }

        if(fromDate == ''){
            $("#passwordBlock").removeClass('has-danger');
            $("#passwordError").hide();
            $("#emptyPassword").hide();
            $("#dateBlock").addClass('has-danger');
            $("#fromDateError").show();
            $("#toDateError").hide();
            return false;
        }
        if(toDate == ''){
            $("#dateBlock").addClass('has-danger');
            $("#toDateError").show();
            $("#fromDateError").hide();
            return false;
        }

        if(new Date(toDate) < new Date(fromDate)){
            $("#dateBlock").addClass('has-danger');
            $("#incorrectDate").show();
            $("#fromDateError").hide();
            $("#toDateError").hide();
            return false;
        }

        if(reason==""){
            $("#reasonBlock").addClass('has-danger');
            $("#dateBlock").removeClass('has-danger');
            $("#passwordBlock").removeClass('has-danger');
            $("#passwordError").hide();
            $("#emptyPassword").hide();
            $("#toDateError").hide();
            $("#fromDateError").hide();
            $("#reasonError").show();
            $("#fromDateError").hide();
            return false;
        }

        $("#reasonBlock").removeClass('has-danger');
        $("#reasonError").hide();
        $("#passwordBlock").removeClass('has-danger');
        $("#emptyPassword").hide();
        $("#passwordError").hide();
        $("#dateBlock").removeClass('has-danger');
        $("#fromDateError").hide();
        $("#dateBlock").removeClass('has-danger');
        $("#toDateError").hide();

        myDate1=fromDate.split("-");
        myDate1=myDate1[0].split("/");
        var newDate1=myDate1[2]+"/"+myDate1[1]+"/"+myDate1[0]+" 00:00:00";
        fromDate=new Date(newDate1).getTime()/1000;

        myDate2=toDate.split("-");
        myDate2=myDate2[0].split("/");
        var newDate2=myDate2[2]+"/"+myDate2[1]+"/"+myDate2[0]+" 23:59:59";
        toDate=new Date(newDate2).getTime()/1000;

        var userid = $('span#user_id').html();

        $(".loader").fadeIn("fast");
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionUser/showDialogues',
            type: "POST",
            data: {'fromDate': fromDate, 'toDate':toDate, 'userid':userid, 'reason': reason, 'password':password},
            success: function(data) {
                $(".loader").fadeOut("fast");
                if(data == ''){
                    // alert("No chats to export in this date range.");
                    $("#customBox .loaderImg").hide();
                    var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                    $('#customModalLabel').html('Confirmation');
                    $('#customModalBody').html('No chats to export in this date range');
                    $('#customModalFoot').html(buttonHtml);
                    $('#customBox').modal('show');

                    $('#exampleInputPassword1').val('');
                }else if( IsJsonString(data) == false ){
                    $('#exampleInputPassword1').val('');
                    $('#dialogListView').show();
                    $('#ajax_dialogs').html(data);
                }else{
                    var responseArray = JSON.parse(data);
                    if(responseArray['status'] == 2){
                        $("#passwordBlock").addClass('has-danger');
                        $("#passwordError").show();
                        $("#emptyPassword").hide();
                        return false;
                    }else{
                        $("#customBox .loaderImg").hide();
                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Confirmation');
                        $('#customModalBody').html(responseArray['message']);
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');
                        // alert(responseArray['message']);
                    }
                }
            },
            error: function( data ){
                $(".loader").fadeOut("fast");
            }
        });
    }

    function showUserChats($dialog_id) {
        var fromDate = $('#datetimepicker6').val();
        var toDate = $('#datetimepicker7').val();

        myDate1=fromDate.split("-");
        myDate1=myDate1[0].split("/");
        var newDate1=myDate1[2]+"/"+myDate1[1]+"/"+myDate1[0]+" 00:00:00";
        fromDate=new Date(newDate1).getTime()/1000;

        myDate2=toDate.split("-");
        myDate2=myDate2[0].split("/");
        var newDate2=myDate2[2]+"/"+myDate2[1]+"/"+myDate2[0]+" 23:59:59";
        toDate=new Date(newDate2).getTime()/1000;

        var expires = "";
        var date = new Date();
        date.setTime(date.getTime()+(10 * 60 * 60 * 1000));
        expires = "; expires="+date.toGMTString();

        var userid = $('span#user_id').html();

        document.cookie = "from_date="+fromDate+expires+"; path=/";
        document.cookie = "to_date="+toDate+expires+"; path=/";
        document.cookie = "id="+userid+expires+"; path=/";
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionUser/showUserChats/" + $dialog_id;
    }

    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
</script>
