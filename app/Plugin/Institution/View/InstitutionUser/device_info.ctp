<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
      <div class="col-md-12 align-self-center col-5">
          <h3 class="text-themecolor m-b-0 m-t-0 col-1 window_history"><img src="<?php echo BASE_URL; ?>institution/img/left-arrow.svg" /></h3>
          <h3 class="text-themecolor m-b-0 m-t-0 col-11" style="float: left;">User's Login & Logout History</h3>
      </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li> -->
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionAudit/status">Available / OnCall Transactions</a></li> -->
          <!-- <li class="breadcrumb-item active">Transaction History</li> -->
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
      <div class="card">
         <div class="card-block padLeftRight0 pad0">
           <div class="floatLeftFull padLeftRight14 groupCustom newListStyle">
              <div class="tableTop floatLeft marTop10">
                 <!-- <span class="card-title">App Screenshots: <small>Taken by Users </small></span> -->
              </div>
           </div>
            <div class="table-responsive wordBreak" id="ajaxContent">
              <table class="table stylish-table customPading">
                  <thead>
                    <tr>
                        <th style="width:20%; padding-left:20px;">Date</th>
                        <th style="width:20%; text-align:center;">Device Model</th>
                        <th style="width:20%; text-align:center;">Device Version</th>
                        <th style="width:20%; text-align:center;">Platform</th>
                        <th style="width:20%; text-align:center;">Mb Version</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                          if(isset($data['user_list']) && count($data['user_list']) > 0){
                          foreach( $data['user_list'] as $user){
                      ?>
                      <tr>
                          <td><?php echo $user['userProfiles']['first_name'].' '.$user['userProfiles']['last_name']; ?></td>
                          <td><?php echo $user['User']['email']; ?></td>
                          <td><?php echo $user['UserDutyLog']['status'] == 1 ?'OnCall':'Not OnCall'; ?></td>
                          <td><?php echo $user['UserDutyLog']['atwork_status'] == 1 ?'Available':'Not Available'; ?></td>
                          <td><?php echo date("d-m-Y H:i", strtotime($user['UserDutyLog']['modified'])); ?></td>
                          <td>Action</td>
                      </tr>
                      <?php }}elseif(isset($tCount) && $tCount > 0){?>
                      <tr>
                         <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                      </tr>
                      <?php }else{ ?>
                      <tr>
                         <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                      </tr>
                      <?php  } ?>
                  </tbody>
              </table>
              <span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
              <div class="actions marBottom10 paddingSide15 leftFloat98per">
              <?php
                  if(isset($data['user_list']) && count($data['user_list']) > 0){
                      echo $this->element('pagination');
                  }
              ?>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <!-- Row -->
    <!-- User Profile User -->
    <div class="modal fade" id="profileUserPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="profileLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Contact Info</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body pad0">
                    <div class="containerProfile">
                      <div class="otherPImg onCallMain imageUser">
                        <a href="javascript:void(0);">
                          <img class="img-circle userDetails-avatar" src="<?php echo BASE_URL ?>institution/img/ava-single.png" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                          <?php //echo $this->Html->image('institution/img/ava-single.png',array('class'=>'img-circle userDetails-avatar')); ?>
                          <!-- <img class="userDetails-avatar profileUserAvatar" src="images/users/2.jpg" alt="user"> -->
                        </a>
                      </div>
                      <div class="userProfile-info padCustom10-20 borderBottom leftFloatFull">
                        <h3 class="profileUserName"></h3>
                        <span class="onCallTxt otherProfText"></span>
                      </div>
                      <div class="clearFix"></div>
                      <div class="profile_detail">
                        <div class="userProfile-field userProfile-field_email borderBottom">
                          <span class="userDetails-label userDetails-labelEmail">Email:</span>
                          <span class="userDetails-labelEmail-value">
                            <span class="userDetails-field-otheruser-email">
                              <a target="_blank" href=""></a>
                            </span>
                          </span>
                        </div>
                        <div class="userProfile-field userProfile-field_phone">
                          <span class="userDetails-label userDetails-labelPhone">Phone:</span>
                          <span class="userDetails-labelPhone-value">
                            <span class="userDetails-field-otheruser-phone">
                              <span></span>
                            </span>
                          </span>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-success waves-effect">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- Device info of User -->
    <div class="modal fade" id="deviceInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="deviceLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Device Info</h4>
                </div>
                <div class="modal-body pad0">
                    <div class="containerProfile">
                      <div class="userProfile-info padCustom10-20 borderBottom leftFloatFull">
                        <h3 class="loggedInStatus"></h3>
                      </div>
                      <div class="clearFix"></div>
                      <div class="profile_detail">
                        <div class="userProfile-field userProfile-field_email borderBottom customDeviceInfo">
                          <span class="userDetails-label userDetails-labelEmail">Platform:</span>
                          <span class="userDetails-labelEmail-value device_info_platform">-NA-</span>
                        </div>
                        <div class="userProfile-field userProfile-field_email borderBottom customDeviceInfo">
                          <span class="userDetails-label userDetails-labelEmail">Device:</span>
                          <span class="userDetails-labelEmail-value device_info_device">-NA-</span>
                        </div>
                        <div class="userProfile-field userProfile-field_email borderBottom customDeviceInfo">
                          <span class="userDetails-label userDetails-labelEmail">Device Model:</span>
                          <span class="userDetails-labelEmail-value device_info_model">-NA-</span>
                        </div>
                        <div class="userProfile-field userProfile-field_email borderBottom customDeviceInfo">
                          <span class="userDetails-label userDetails-labelEmail">MB Version:</span>
                          <span class="userDetails-labelEmail-value device_info_version">-NA-</span>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-success waves-effect">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>

<script type="text/javascript">
    // $("#sidebarnav>li#menu7").addClass('active').find('a').addClass('active');
    $("#sidebarnav>li#menu1").addClass('active').find('a').addClass('active');

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    // var $input = $('#user_search');

    //on keyup, start the countdown
    // $input.on('keyup', function () {
    //   clearTimeout(typingTimer);
    //   typingTimer = setTimeout(searchUser, doneTypingInterval);
    // });

    //on keydown, clear the countdown
    // $input.on('keydown', function () {
    //   clearTimeout(typingTimer);
    // });

    //user is "finished typing," do something
    // function searchUser() {
    //    $search_text = $input.val().trim();
    //    if($search_text == ''){
    //        // return false;
    //    }
    //    $input.blur();
    //    $(".loader").fadeIn("fast");
    //    $.ajax({
    //        url:'<?php //echo BASE_URL; ?>institution/InstitutionUser/deviceFilter',
    //        type: "POST",
    //        data: {'search_text':$search_text,'user_id': user_id},
    //        success: function(data) {
    //            $('#ajaxContent').html(data);
    //            $('#showTotCount').html($('#totalCount').html());
    //            $(".loader").fadeOut("fast");
    //        }
    //    });
    // }
    // --------- INITIAL FUNCTIONS ------------
    var user_id = "<?php echo $user_id; ?>";
    getList('');
    // $(".loader").fadeOut("fast");

    // --------- INITIAL FUNCTIONS ------------
    function getList($search_text){
       $(".loader").fadeIn("fast");
       $.ajax({
         url:'<?php echo BASE_URL; ?>institution/InstitutionUser/deviceFilter',
         type: "POST",
         data: {'search_text':$search_text,'user_id': user_id},
         success: function(data) {
            console.log(data);
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
           $(".loader").fadeOut("fast");
         },
         error: function( result ){
             $(".loader").fadeOut("fast");
         }
       });
    }

     function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        var goVal=$("#chngPage").val();
        if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
            $(".loader").fadeIn("fast");
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionUser/deviceFilter',
                type: "POST",
                data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'user_id': user_id},
                success: function(data) {
                    $('#ajaxContent').html(data);
                    $('#showTotCount').html($('#totalCount').html());
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
        }

     }
     function chngCount(val){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
      $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionUser/deviceFilter',
          type: "POST",
          data: {'limit':val,'sort':a,'order':order,'user_id': user_id},
          success: function(data) {
              $('#ajaxContent').html(data);
              $('#showTotCount').html($('#totalCount').html());
              $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
              if(a==b){
              $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
              }
              else{
              $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
              }
              $(".loader").fadeOut("fast");
          }
      });
     }

    function abc(val,limit){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
      $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionUser/deviceFilter',
           type: "POST",
           data: {'page_number':val,'limit':limit,'sort':a,'order':order,'user_id': user_id},
           success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
        });
    }

    function getTransaction($user_id){
      window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionUser/getTransaction/" + $user_id;
    }
    $(function () {
      $('#profileUserPopUp').on('show.bs.modal', function (event) {
        $(".modal-content").addClass('loaderBg');
        $('#profileLoader').fadeIn('fast');

        $('.imageUser img').attr('src','<?php echo BASE_URL; ?>institution/img/ava-single.png');
        $('.userProfile-info .profileUserName').html('');
        $('.userDetails-field-otheruser-email a').attr('href','mailto:'+'').html('');
        $('.userDetails-field-otheruser-phone').html('');
        $('.imageUser').removeClass('onCallMain').removeClass('notAtWork');
        $('.onCallTxt.otherProfText').html('');

        var button = $(event.relatedTarget);
        var userid = button.data('user');

        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserProfile',
            type: 'POST',
            data: {'user_id': userid},
            success: function( result ){
                $('#profileLoader').fadeOut('fast');
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){

                    var id = responseArray['data']['id'];
                    var email = responseArray['data']['email'];
                    var phone = responseArray['data']['phone'];
                    var UserName = responseArray['data']['UserName'];
                    var profile_img = responseArray['data']['profile_img'] !=""?responseArray['data']['profile_img']: '<?php echo BASE_URL; ?>institution/img/ava-single.png';
                    var role_status = responseArray['data']['role_status'];
                    var AtWorkStatus = responseArray['data']['AtWorkStatus'];
                    var OnCallStatus = responseArray['data']['OnCallStatus'];
                    var cust_class = responseArray['data']['cust_class'];
                    $('.imageUser').addClass(cust_class);

                    $('.imageUser img').attr('src',profile_img);
                    $('.userProfile-info .profileUserName').html(UserName);
                    $('.userDetails-field-otheruser-email a').attr('href','mailto:'+email).html(email);
                    $('.userDetails-field-otheruser-phone').html(phone);
                    $('.onCallTxt.otherProfText').html(role_status);

                    $(".modal-content").removeClass('loaderBg');
                }
            },
            error: function( result ){
                $('#profileLoader').fadeOut('fast');
            }
        });
      });
    });
</script>
