<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<!-- <div class="loader"></div> -->
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="assignBtnAll addBaton-btn" style="float: right; margin: 15px 0 0 0;">
       <button data-toggle="modal" data-target="#addBatonPopup" class="btn bulk_action_btn pull-right"> <span>+</span> Add Baton Role</button>
    </div>
    <div class="row page-titles">
        <div class="col-md-11 col-11 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Manage Baton Roles</h3>
        </div>
    </div>

    <div style="clear: both;"></div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
     <div id="batonListLoader" class="loader"></div>
      <div class="card">
         <div class="card-block padLeftRight0 pad0">
            <div class="floatLeftFull padLeftRight14 newListStyle">
               <div class="tableTop floatLeft">
                 <div id="list-filter-options">
                     <input type="radio" name="filter_option" value="all" id="filter_option_all">
                     <label for="filter_option_all">All(<span id="showTotCount">0</span>)</label>
                     <input type="radio" name="filter_option" value="Assigned" id="filter_option_assigned">
                     <label for="filter_option_assigned">Assigned(<span>0</span>)</label>
                     <input type="radio" name="filter_option" value="Unassigned" id="filter_option_unassigned">
                     <label for="filter_option_unassigned">Unassigned(<span>0</span>)</label>
                 </div>
                  <!-- <span class="allIconUsers">
                  <img src="<?php echo BASE_URL ?>institution/img/batonRoleIcon.svg" alt=""></span>
                  <span class="card-title">Total Baton Roles:</span>
                  <span class="display-6" id="showTotCount">0</span> -->

               </div>
               <div id="userlisting_filter" class="dataTables_filter">
                  <label><i class="demo-icon icon-sub-lense"></i></label>
                  <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
               </div>
            </div>
            <div class="table-responsive wordBreak" id="ajaxContent">
              <table class="table stylish-table customPading lineHeight36">
                  <thead>
                      <tr>
                          <th>Batton Roles </th>
                          <th>On Call </th>
                          <th>Status</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td colspan="8" align="center">
                            <font color="green">Please wait while we are loading the list...</font>
                        </td>
                    </tr>
                  </tbody>
              </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->




    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- User Privilege -->
    <div class="modal fade" id="addBatonPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="addBatonLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content uploadFileBlock">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Add Baton Role</h4>
                    <p>You can add baton role by creating new role below:</p>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                </div>
                <div class="modal-body">
                    <form class="addBatonForm" id="bulkEmailUpload">
                      <div class="floatLeftFull txtCenter">
                        <textarea id="batonRoleText" placeholder="Write here.."></textarea>
                      </div>
                      <div class="floatLeftFull optionOnCall">
                        <span class="leftFloat"><input id="baton_is_oncall" type="checkbox" /></span>
                        <p> This is On Call Role</p>
                      </div>
                      <div class="row" style="clear: both;">
                          <div class="col-lg-12 col-md-12">
                              <div class="card">
                                  <div class="">
                                    <div class="borderSeparation">
                                      <h4>OR</h4>
                                    </div>
                                      <label for="input-file-now" class="nodefault">Add Multiple Baton Roles</label>
                                      <div class="pull-right">
                                        <span class="sampleTxt">Sample File Formats- </span>
                                        <a target="_blank" class="downloadLink" href="<?php echo BASE_URL; ?>institution/batonrole-sample.csv" title="Sample">Download</a>
                                      </div>
                                      <input type="file" name="bantonfile" id="input-file-now" class="dropify" accept=".csv, .xls, .xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                      <input type="hidden" name="company_id" type="test" value="<?php echo $_COOKIE['adminInstituteId']; ?>" />
                                      <div class="form-control-feedback" id="uploadResult" style="display:none">Upload Result:-</div>
                                      <div class="form-control-feedback" id="uploadSuccess" style="display:none"></div>
                                      <div class="form-control-feedback" id="uploadAlready" style="display:none"></div>
                                      <div class="form-control-feedback" id="uploadError" style="display:none"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <!-- <div style="margin:0 auto;" class="onCallBtMain txtCenter">
                    <button type="button" id="btBatonAccept" class="btn btModelBatonAccept" onclick="changeRequestFinal(20,1,8);">Add</button>
                    <button type="button" id="btBatonCancel" class="btn btModelBatonCancel" data-dismiss="modal">Cancel</button>
                  </div> -->
                  <div class="onCallBtMain txtCenter marBottom30" style="display: none;">
                    <button type="submit" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" id="addBatonbtn2">Upload</button>
                    <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
                  </div>
                    <div style="display:none" class="row showWarning">
                      <div class="col-md-6 pull-left">
                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                        <p>Are you sure you want to add this Baton Role?</p>
                      </div>
                      <div class="col-md-6 txtRight pull-right">
                        <button type="button" class="btn btn-success waves-effect" onclick="addBatonRole();">Add</button>
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                    <div class="row showSucess" style="display:none">
                        <div class="col-md-6 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>Baton Role has been added successfully!</p>
                        </div>
                        <div class="col-md-6 txtRight pull-right">
                          <button type="button" class="btn btn-success waves-effect" onclick="addMoreBatonRole();">Add More</button>
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- Edit Baton Role -->
    <div class="modal fade" id="editBatonPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="editBatonLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Edit Baton Role</h4>
                    <p>You can edit user baton role:</p>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                </div>
                <div class="modal-body">
                    <form class="addBatonForm">
                      <div class="floatLeftFull txtCenter">
                        <textarea id="editBatonRoleText" placeholder="Write here.."></textarea>
                      </div>
                      <div class="floatLeftFull optionOnCall" style="display: none;">
                        <span class="leftFloat"><input id="edit_baton_is_oncall" type="checkbox" disabled="disabled" /></span>
                        <p> This is On Call Role</p>
                      </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <!-- <div style="margin:0 auto;" class="onCallBtMain txtCenter">
                    <button type="button" id="btBatonAccept" class="btn btModelBatonAccept" onclick="changeRequestFinal(20,1,8);">Add</button>
                    <button type="button" id="btBatonCancel" class="btn btModelBatonCancel" data-dismiss="modal">Cancel</button>
                  </div> -->
                    <div style="display:none" class="row showWarning">
                      <div class="col-md-6 pull-left">
                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                        <p>Are you sure you want to update user baton role?</p>
                      </div>
                      <div class="col-md-6 txtRight pull-right">
                        <button type="button" class="btn btn-success waves-effect" onclick="updateBatonRole();">Yes</button>
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                    <div class="row showSucess" style="display:none">
                        <div class="col-md-8 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>Baton Role has been updated successfully!</p>
                        </div>
                        <div class="col-md-4 txtRight pull-right">
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- Unassign Baton Role -->
    <div class="modal fade" id="unassignBatonPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="unassignBatonLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Unassign Baton Role</h4>
                    <p>You can unassign baton role from user:</p>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                </div>
                <div class="modal-body">
                    <form class="addBatonForm">
                      <div class="floatLeftFull txtCenter">
                        <textarea id="unassignBatonRoleText" disabled="disabled" readonly="true"></textarea>
                      </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <!-- <div style="margin:0 auto;" class="onCallBtMain txtCenter">
                    <button type="button" id="btBatonAccept" class="btn btModelBatonAccept" onclick="changeRequestFinal(20,1,8);">Add</button>
                    <button type="button" id="btBatonCancel" class="btn btModelBatonCancel" data-dismiss="modal">Cancel</button>
                  </div> -->
                    <div class="row showWarning">
                      <div class="col-md-6 pull-left">
                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                        <p>Are you sure you want to unassign user baton role?</p>
                      </div>
                      <div class="col-md-6 txtRight pull-right">
                        <button type="button" class="btn btn-success waves-effect" onclick="unassignBatonRoleFinal();">Yes</button>
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal" onclick="closeUnassignedPopup();">Cancel</button>
                      </div>
                    </div>
                    <div class="row showSucess" style="display:none">
                        <div class="col-md-8 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>User baton role has been unassigned successfully!</p>
                        </div>
                        <div class="col-md-4 txtRight pull-right">
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- Edit Baton Role -->
    <div class="modal fade" id="deleteBatonPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="deleteBatonLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Delete Baton Role</h4>
                    <p>You can delete user baton role</p>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                </div>
                <div class="modal-body">
                    <form class="addBatonForm">
                      <div class="floatLeftFull txtCenter">
                        <textarea id="deleteBatonRoleText" disabled="disabled" readonly="true"></textarea>
                      </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <!-- <div style="margin:0 auto;" class="onCallBtMain txtCenter">
                    <button type="button" id="btBatonAccept" class="btn btModelBatonAccept" onclick="changeRequestFinal(20,1,8);">Add</button>
                    <button type="button" id="btBatonCancel" class="btn btModelBatonCancel" data-dismiss="modal">Cancel</button>
                  </div> -->
                    <div class="row showWarning">
                      <div class="col-md-12 txtCenter pull-right">
                        <button type="button" class="btn btn-success waves-effect" onclick="deleteBatonRoleFinal();">Yes</button>
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                    <div class="row showSucess" style="display:none">
                        <div class="col-md-8 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>User baton role has been deleted successfully!</p>
                        </div>
                        <div class="col-md-4 txtRight pull-right">
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- User Assign -->
    <div class="modal fade" id="userAssignPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Assign a Baton Role</h4>
                    <p>You can assign "<span id="assignBatonLabel">....</span>" role to a user</p>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 baton_role_search">
                        <label><i class="demo-icon icon-sub-lense"></i></label>
                        <input type="text" placeholder="Search a user" id="search_unoccupied_role" name="" value="">
                    </div>
                    <div id="batonListing" style="display:block;">
                      <div id="batonUserListLoader" class="loader"></div>
                      <ul id="batonRoleList">
                      </ul>
                    </div>
                </div>
                <div class="modal-footer">
                  <div class="row showWarning" style="display:none">
                    <div class="col-md-6 pull-left">
                      <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                      <p>Are you sure you want to update user baton role?</p>
                    </div>
                    <div class="col-md-6 txtRight pull-right">
                      <button type="button" class="btn btn-success waves-effect" onclick="assignBatonRole();">Yes</button>
                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                  <div class="row showSucess" style="display:none">
                        <div class="col-md-8 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>User baton role has been updated successfully!</p>
                        </div>
                        <div class="col-md-4 txtRight pull-right">
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<button data-toggle="modal" id="editRoleButton" data-target="#editBatonPopup" class="btn bulk_action_btn pull-right hidden-sm-down btn-primary-green" style="display: none;">Edit Baton Roles</button>
<script type="text/javascript">
$("#sidebarnav>li#menu10").addClass('active').find('a').addClass('active');
$("#sidebarnav>li#menu10").find("li").eq(0).addClass('active').find('a').addClass('active');
window.history.pushState('','','<?php echo BASE_URL.'institution/Switchboard/manageBatonRoles'; ?>');

//setup before functions
var typingTimer;                //timer identifier
var UsertypingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#user_search');
var $userInput = $('#search_unoccupied_role');
var $batonInput = $('#batonRoleText');
var $batonInputCheck = $('#baton_is_oncall');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(searchBaton, doneTypingInterval);
});

//on keydown, clear the countdown
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

//on keyup, start the countdown
$userInput.on('keyup', function () {
  clearTimeout(UsertypingTimer);
  UsertypingTimer = setTimeout(searchUserList, doneTypingInterval);
});

//on keydown, clear the countdown
$userInput.on('keydown', function () {
  clearTimeout(UsertypingTimer);
});


$batonInput.on('keyup', function () {
  $text = $batonInput.val().trim();

  $('#addBatonPopup .showSucess').hide();
  if($text == ''){
    $('#addBatonPopup .showWarning').hide();
  }else{
    $('#addBatonPopup .showWarning').show();
  }
});

$(document).ready(function(){
  var list_type = '<?php echo isset($_REQUEST["type"]) ? $_REQUEST["type"] : "all"; ?>';
  console.log(list_type);
  $('input[name="filter_option"]').removeAttr('checked');
  $('input[value="'+list_type+'"]').attr('checked', 'checked');
  getBatonList('1','20',$input.val().trim());
});


$('input[name="filter_option"]').on('change', function(){
  $('input[name="filter_option"]').removeAttr('checked');
  $('input[value="'+$(this).val()+'"]').attr('checked', 'checked');
  searchBaton();
});

var callOnce = true;

function getBatonList(page,limit,$search_text){
  var $filterBy = $('input[name="filter_option"]:checked').val();
  $("#batonListLoader").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
    type: "POST",
    data: {'page_number':page,'limit':limit,'search_text':$search_text,'filter_by':$filterBy},
    success: function(data) {
      $('#ajaxContent').html(data);
      $("#batonListLoader").fadeOut("fast");
      $('#showTotCount').html($('#totalCount').html());
      $('label[for="filter_option_assigned"] span').html($('#tCountAssigned').html());
      $('label[for="filter_option_unassigned"] span').html($('#totalUnassigned').html());
      <?php if(isset($_REQUEST['action']) == 'addBatonRole'){ ?>
        if(callOnce == true){
          $('.assignBtnAll button').trigger('click');
          callOnce = false;
        }
      <?php } ?>
    }
  });
}
function searchBaton() {
  var $filterBy = $('input[name="filter_option"]:checked').val();
   $search_text = $input.val().trim();
   $input.blur();
   $("#batonListLoader").fadeIn("fast");
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
       type: "POST",
       data: {'search_text':$search_text,'filter_by':$filterBy},
       success: function(data) {
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
           $('label[for="filter_option_assigned"] span').html($('#tCountAssigned').html());
           $('label[for="filter_option_unassigned"] span').html($('#totalUnassigned').html());
           $("#batonListLoader").fadeOut("fast");
       }
   });
}
function searchUserList() {
   $search_text = $userInput.val().trim();
   if($search_text == ''){
    $('#batonRoleList').html('');
    $('#userAssignPopUp .showWarning').hide();
    $('#userAssignPopUp .showSucess').hide();
    return false;
   }
   $userInput.blur();
   $("#batonUserListLoader").fadeIn("fast");
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/getUserList',
       type: "POST",
       data: {'search_text':$search_text},
       success: function(data) {
           $('#batonListing').scrollTop(0);
           $('#batonRoleList').html(data);
           $('#userAssignPopUp .showWarning').hide();
           $('#userAssignPopUp .showSucess').hide();
           $("#batonUserListLoader").fadeOut("fast");
       }
   });
}

$('body').on('click','#batonRoleList li.assignUser',function(event){
  event.preventDefault();
  $('#batonRoleList li.assignUser').removeClass('selected');
  $(this).addClass('selected');
  $('#userAssignPopUp .showWarning').show();
  $('#userAssignPopUp .showSucess').hide();
});

function assignBatonRole(){
  var role_id = $('#userAssignPopUp').attr('data-roleid');
  var user_id = $('#batonRoleList li.assignUser.selected').attr('data-user');
  $("#batonUserListLoader").fadeIn("fast");
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/assignUnassignBatonRole',
      type: "POST",
      data: {'user_id':user_id, 'role_id':role_id,'status':1},
      success: function(data) {
        console.log(data);
        $userInput.val('');
        $userInput.attr('disabled',true);
        $('#batonRoleList').empty();
        $('#userAssignPopUp .showWarning').hide();
        $('#userAssignPopUp .showSucess').show();
        $('#userAssignPopUp .showSucess button[data-dismiss="modal"]').trigger('click');
        $("#batonUserListLoader").fadeOut("fast");
      }
  });

}

function addBatonRole(){
  $text = $batonInput.val().trim();
  $check = $batonInputCheck.is(':checked');
  $on_call_val = $check == true ? 1 : 0;
  $("#addBatonLoader").fadeIn("fast");
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/addBatonRole',
      type: "POST",
      data: {'role_name':$text, 'on_call_value':$on_call_val},
      success: function(data) {
        console.log(data);
        var responseArray = JSON.parse(data);
        if(responseArray['status'] == 1){
          $batonInput.val('');
          $batonInputCheck.prop('checked', true);
          $('#batonRoleList').empty()
          $('#addBatonPopup .showWarning').hide();
          $('#addBatonPopup .showSucess').show();
        }else{
          alert(responseArray['message']);
        }
        $("#addBatonLoader").fadeOut("fast");
      }
  });
}

function chngPage(val,limit) {
  var $filterBy = $('input[name="filter_option"]:checked').val();
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
   var goVal=$("#chngPage").val();
   if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
       $("#batonListLoader").fadeIn("fast");
       $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
           type: "POST",
           data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'search_text':$search_text,'filter_by':$filterBy},
           success: function(data) {
               $('#ajaxContent').html(data);
               $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
                   $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
                   $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $("#batonListLoader").fadeOut("fast");
           }
       });
   }

}
function chngCount(val){
  var $filterBy = $('input[name="filter_option"]:checked').val();
   $("#batonListLoader").fadeIn("fast");
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
 $.ajax({
     url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
     type: "POST",
     data: {'limit':val,'sort':a,'order':order,'search_text':$search_text,'filter_by':$filterBy},
     success: function(data) {
         $('#ajaxContent').html(data);
         $('#showTotCount').html($('#totalCount').html());
         $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
         if(a==b){
         $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
         }
         else{
         $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
         }
         $("#batonListLoader").fadeOut("fast");
     }
 });
}

function abc(val,limit){
  var $filterBy = $('input[name="filter_option"]:checked').val();
   $("#batonListLoader").fadeIn("fast");
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   var $search_text = $input.val().trim();
 $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
      type: "POST",
      data: {'page_number':val,'limit':limit,'sort':a,'order':order,'search_text':$search_text,'filter_by':$filterBy},
      success: function(data) {
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
          $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
          if(a==b){
          $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
          }
          else{
          $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
          }
          $("#batonListLoader").fadeOut("fast");
      }
   });
}
</script>
<script type="text/javascript">
$(function () {
  $('#userAssignPopUp').on('hide.bs.modal', function (event) {
    $('#search_unoccupied_role').val('');
    $('#batonRoleList').empty();
    if($('#userAssignPopUp .showSucess').css('display') == 'block'){
      reload();
    }
  });
  $('#userAssignPopUp').on('show.bs.modal', function (event) {
    if (event.namespace === 'bs.modal') {
        var button = $(event.relatedTarget);
        var role_id = button.data('roleid');
        var on_call_value = button.data('oncall');
        $('#userAssignPopUp').attr('data-roleid',role_id);
        $('#userAssignPopUp').attr('data-oncall',on_call_value);

        $('#userAssignPopUp .showWarning').hide();
        $('#userAssignPopUp .showSucess').hide();
        $('#batonUserListLoader').hide();
        $userInput.attr('disabled',false);
    }
  });

  $('#addBatonPopup').on('hide.bs.modal', function (event) {
    if($('#addBatonPopup .showSucess').css('display') == 'block'){
      reload();
    }
  });

  $('#editBatonPopup').on('hide.bs.modal', function (event) {
    if($('#editBatonPopup .showSucess').css('display') == 'block'){
      reload();
    }
  });

  $('#deleteBatonPopup').on('hide.bs.modal', function (event) {
    if($('#deleteBatonPopup .showSucess').css('display') == 'block'){
      reload();
    }
  });

  $('#unassignBatonPopup').on('hide.bs.modal', function (event) {
    if($('#unassignBatonPopup .showSucess').css('display') == 'block'){
      reload();
    }
  });

  $('#addBatonPopup').on('show.bs.modal', function (event) {
    if (event.namespace === 'bs.modal') {
        $batonInput.val('');
        $batonInputCheck.prop('checked', true);
        $('#addBatonLoader').hide();
        $('#addBatonPopup .showWarning').hide();
        $('#addBatonPopup .showSucess').hide();
    }
  });


  // $('#editBatonPopup').on('show.bs.modal', function (event) {
  //   if (event.namespace === 'bs.modal') {
  //       $batonInput.val('');
  //       $batonInputCheck.prop('checked', false);
  //       $('#editBatonLoader').hide();
  //       $('#addBatonPopup .showWarning').hide();
  //       $('#addBatonPopup .showSucess').hide();
  //   }
  // });
});

var rolnameVal = '';
var onCallVal = '';
var b_user_id = '';
function editBatonRole(roleName, roleId, onCall, user_id){
  $('#editBatonRoleText').val(roleName);
  rolnameVal = roleName;
  onCallVal = onCall;
  b_user_id = user_id;
  if(onCall == 'Yes'){
    $('#edit_baton_is_oncall').prop('checked', true);
  }else{
    $('#edit_baton_is_oncall').prop('checked', false);
  }
  $('#editBatonPopup').attr('data-roleid', roleId);
  $('#editBatonRoleText').removeAttr('disabled');
  // $('#edit_baton_is_oncall').removeAttr('disabled');
  $('#editRoleButton').trigger('click');
  $("#batonListLoader").fadeOut("fast");
  $("#editBatonLoader").fadeOut("fast");
  $('#editBatonPopup .showWarning').hide();
  $('#editBatonPopup .showSucess').hide();

  return false;
}

$('#editBatonRoleText').on('keyup', function () {
  editBatonRoleAction();
});

// $('#edit_baton_is_oncall').on('change', function () {
//   editBatonRoleAction();
// });

function editBatonRoleAction(){
  $text = $('#editBatonRoleText').val().trim();
  $onCall = 'No';
  if($('#edit_baton_is_oncall').prop('checked')){
    $onCall = 'Yes';
  }

  $('#editBatonPopup .showSucess').hide();
  if($text == rolnameVal && $onCall == onCallVal){
    $('#editBatonPopup .showWarning').hide();
  }else{
    $('#editBatonPopup .showWarning').show();
  }
}

function updateBatonRole(){
  $text = $('#editBatonRoleText').val().trim();
  $check = $('#edit_baton_is_oncall').is(':checked');
  $roleid = $('#editBatonPopup').attr('data-roleid');
  $on_call_val = $check == true ? 1 : 0;
  user_id = b_user_id;
  $("#editBatonLoader").fadeIn("fast");
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/updateBatonRole',
      type: "POST",
      data: {'role_name':$text, 'on_call_value':$on_call_val, 'role_id':$roleid, 'user_id':user_id},
      success: function(data) {
        console.log(data);
        var responseArray = JSON.parse(data);
        if(responseArray['status'] == 1){
          $('#editBatonRoleText').attr('disabled', 'disabled');
          $('#edit_baton_is_oncall').attr('disabled', 'disabled');
          $('#editBatonPopup .showWarning').hide();
          $('#editBatonPopup .showSucess').show();
        }else{
          alert(responseArray['message']);
        }
        $("#editBatonLoader").fadeOut("fast");
      }
  });
}

function unassignBatonRole(role_name, role_id, user_id){
  $('#unassignBatonPopup').attr('data-rolename', role_name);
  $('#unassignBatonPopup').attr('data-id', role_id);
  $('#unassignBatonPopup').attr('data-userid', user_id);
  $('#unassignBatonRoleText').val(role_name);
  $('#unassignBatonPopup .showWarning').show();
  $('#unassignBatonPopup .showSucess').hide();
  $("#unassignBatonLoader").fadeOut("fast");
  $('.sw-popup-overlay, .sw-userprofile-popup').fadeOut('fast');
}

function unassignBatonRoleFinal(){
  var role_id = $('#unassignBatonPopup').attr('data-id');
  var user_id = $('#unassignBatonPopup').attr('data-userid');
  var notes = '';
  $("#unassignBatonLoader").fadeIn("fast");
  $.ajax({
    // url:'<?php //echo BASE_URL; ?>institution/InstitutionBaton/unassignBatonRoletAdmin',
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/assignUnassignBatonRole',
    type: "POST",
    data: {'user_id':user_id,'status':0,'role_id':role_id,'notes':notes},
    success: function(data) {
      var responseArray = JSON.parse(data);
      $("#unassignBatonLoader").fadeOut("fast");
      if( responseArray['status'] == 1 ){
        $('#unassignBatonPopup .showWarning').hide();
        $('#unassignBatonPopup .showSucess').show();
        //reload();
      }
    }
  });
}

function deleteBatonRole(role_name, role_id, user_id){//deleteBatonPopup
  $("#deleteBatonLoader").fadeIn("fast");
  $('#deleteBatonPopup').attr('data-rolename', role_name);
  $('#deleteBatonPopup').attr('data-roleid', role_id);
  $('#deleteBatonPopup').attr('data-user_id', user_id);
  $('#deleteBatonRoleText').val(role_name);
  $('#deleteBatonPopup .showWarning').show();
  $('#deleteBatonPopup .showSucess').hide();
  $("#deleteBatonLoader").fadeOut("fast");
}

function deleteBatonRoleFinal(){//deleteBatonPopup
  $("#deleteBatonLoader").fadeIn("fast");
  var role_id = $('#deleteBatonPopup').attr('data-roleid');
  var user_id = $('#deleteBatonPopup').attr('data-user_id');
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/deleteBatonRole',
      type: "POST",
      data: {'role_id':role_id,'user_id':user_id},
      success: function(data) {
        console.log(data);
        var responseArray = JSON.parse(data);
        if(responseArray['status'] == 1){
          $batonInput.val('');
          $batonInputCheck.prop('checked', false);
          $('#batonRoleList').empty()
          $('#deleteBatonPopup .showWarning').hide();
          $('#deleteBatonPopup .showSucess').show();
          //reload();
        }else{
          alert(responseArray['message']);
        }
        $("#deleteBatonLoader").fadeOut("fast");
      }
  });
}

function reload(){
    // $('#customModalBody').removeClass('bodyCustom');
    // $('#customModalFoot').css('text-align','center');
    var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
    var limit = $('#Data_Count').val();
    var $search_text = $input.val().trim();
    getBatonList(page,limit,$search_text);
}

function userProfile(obj, id){
  $('#batonListLoader').fadeIn('fast');
  $.ajax({
    //NSDictionary* parameters = @{@"loggedin_user_id":loggedIn_userId,@"user_id":userId,kAPIDeviceId:[UIDevice currentDevice].identifierForVendor.UUIDString};
    url:'<?php echo BASE_URL; ?>institution/Switchboard/getUserProfileDetail',
    //url:'<?php echo BASE_URL; ?>mbapi/mbapiuserwebservices/getOtherUserProfileLite',
    type: "POST",
    // headers: {
    //     //Accept : "text/plain; charset=utf-8",
    //     "Content-Type": "application/json"
    // },
    // data: JSON.stringify({"loggedin_user_id":1,"user_id":6235}),
    data: {"user_id":id},
    success: function(data) {
      obj.parent('td').append(data);
      $('#batonListLoader').fadeOut('fast');
    }
  });
}

function closeUserProfile(){
  $('.sw-popup-overlay').fadeOut('fast').remove();
  $('.sw-userprofile-popup').fadeOut('fast').remove();
}

function closeUnassignedPopup(){
  $('.sw-popup-overlay, .sw-userprofile-popup').fadeIn('fast');
}

function assignBatonPopUpFunc(batonRole){
  $('#assignBatonLabel').html(batonRole);
}

$('#input-file-now').change(function(e){
  var filename = e.target.files[0].name;
  var extension=filename.replace(/^.*\./, '');
  var fileExtension = ['csv', 'xls', 'xlsx'];
  if ($.inArray(extension.toLowerCase(), fileExtension) == -1) {
      $(".dropify-clear").click();
      alert("Only formats are allowed : "+fileExtension.join(', '));
      //$('.dropify-preview').hide();
      //$('#input-file-now').click();
      //window.File.resetPreview();
  }else{
    $("#batonRoleText").attr("disabled", "disabled");
    $(".onCallBtMain.txtCenter").show();

    $("#inputEmail").removeClass('has-danger');
    $("#emptyEmail").hide();
    $("#invalidEmail").hide();
    $batonInputCheck.attr("disabled", "disabled");

    $("#uploadResult").hide();
    $("#uploadSuccess").hide();
    $("#uploadError").hide();

    $("#batonRoleText").val('');
    $('#addBatonPopup .showWarning').hide();
    $('#addBatonPopup .showSucess').hide();
  }

});

$('.onCallBtMain button[type="submit"]').on('click',function(){
    $("form#bulkEmailUpload").submit();
});

$("form#bulkEmailUpload").submit(function(){
    var formData = new FormData(this);
    $('#addBatonLoader').fadeIn('fast');

   $.ajax({
       url: '<?php echo BASE_URL; ?>institution/Switchboard/addBatonRoleFileUpload',
       type: 'POST',
       data: formData,
       success: function (data) {
           console.log(data);
           $('#addBatonLoader').fadeOut('fast');
           var responseArray = JSON.parse(data);
           if( responseArray['status'] == 1 ){
               $(".uploadFileBlock .loaderImg").css("display", "none");
               $("#uploadResult").show();
               $("#uploadSuccess").show();
               $("#uploadError").show();
               $("#uploadAlready").show();
               $(".onCallBtMain.txtCenter").hide();
               $('.dropify-wrapper.has-preview').hide();
               $("#uploadSuccess").html("Success: " + responseArray['data']['addUserData']['addedSuccessfully']);
               $("#uploadAlready").html("Already Exist: " + responseArray['data']['addUserData']['alreadyExist']);
               $("#uploadError").html("Error: " + responseArray['data']['addUserData']['emptyFields']);
               $('#addBatonPopup .showSucess').show();
           }
       },
       cache: false,
       contentType: false,
       processData: false
   });

    return false;
});

$(".addBaton-btn button").click(function(){
    $("#batonRoleText").val('');
    $("#inputEmail").removeClass('has-danger');
    $("#alreadyExist").hide();
    $("#emptyEmail").hide();
    $("#invalidEmail, #uploadAlready").hide();
    $(".dropify-wrapper").removeClass('disable');
    $("#input-file-now").removeClass('disable');
    $("#batonRoleText").attr("disabled", false);
    $batonInputCheck.attr("disabled", false);
    $(".onCallBtMain.txtCenter").hide();
    $(".dropify-wrapper").show();

    $(".dropify-clear").trigger("click");

});

function addMoreBatonRole(){
  $("#batonRoleText").val('');
  $("#inputEmail").removeClass('has-danger');
  $("#alreadyExist, #emptyEmail, #uploadAlready, #invalidEmail").hide();
  $(".dropify-wrapper").removeClass('disable');
  $(".dropify-wrapper").show();
  $("#input-file-now").removeClass('disable');
  $("#batonRoleText").attr("disabled", false);
  $batonInputCheck.attr("disabled", false);
  $(".onCallBtMain.txtCenter").hide();

  $(".dropify-clear").trigger("click");
  $('#addBatonPopup .showSucess').hide();
}
</script>
