<?php
// print_r($data);
// exit;
?>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
#baseLocationLoader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    /* background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459); */
}

.modal-backdrop.fade {
	display: none;
}
.modal-open .modal-backdrop.fade {
	display: block;
}
</style>
<div class="container-fluid">
  <div id="baseLocationLoader"></div>
	 <!-- ============================================================== -->
	 <!-- Bread crumb and right sidebar toggle -->
	 <!-- ============================================================== -->
	 <div class="row page-titles">
	     <div class="col-md-12 col-12 align-self-center">
	         <h3 class="text-themecolor m-b-0 m-t-0">Pending Baton Role Requests</h3>
	     </div>
	 </div>

	 <!-- ============================================================== -->
	 <!-- End Bread crumb and right sidebar toggle -->
	 <!-- ============================================================== -->

  <!-- ============================================================== -->
  <!-- Start HTML Content -->
  <!-- ============================================================== -->
  <div class="row boaderLines">
    <div class="col-12">
       <div id="baseLocation" style="position:relative;">
         <div class="card">
            <div class="card-block padLeftRight0 pad0">
               <div class="floatLeftFull padLeftRight14 newListStyle">
                  <div class="tableTop floatLeft">
                    <div id="list-filter-options">
                      <input type="radio" name="filter_option" value="all" id="filter_option_all" checked="checked">
                      <label for="filter_option_all">All(<span id="showTotCount">0</span>)</label>
                      <input type="radio" name="filter_option" value="Take On" id="filter_option_takeon">
                      <label for="filter_option_takeon">Take On(<span>0</span>)</label>
                      <input type="radio" name="filter_option" value="Transfer" id="filter_option_transfer">
                      <label for="filter_option_transfer">Transfer(<span>0</span>)</label>
                    </div>
                     <!-- <span class="allIconUsers">
                     <img src="/bitbucket/ocr_dev/ocr-web-work/institution/img/batonRoleTransferIcon.svg" alt=""></span>
                     <span class="card-title">Total Requests:</span>
                     <span class="display-6" id="showTotCount">0</span> -->
                  </div>
                  <div id="userlisting_filter" class="dataTables_filter">
                     <label><i class="demo-icon icon-sub-lense"></i></label>
                     <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                  </div>
               </div>
               <div class="table-responsive wordBreak" id="baseLocationContent">
                 <table class="table stylish-table customPading lineHeight36">
                    <thead>
                        <tr>
                            <th style="width:25%;">Name</th>
                            <th style="width:25%;">Baton Role</th>
                            <th style="width:15%; text-align: center;">Request Type</th>
                            <th style="width:20%; text-align: center;">Action</th>
                            <th style="width:15%;">Expiring At(Hrs)</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <td colspan="8" align="center">
                              <font color="green">Please wait while we are loading the list...</font>
                          </td>
                      </tr>
                    </tbody>
                 </table>
               </div>
            </div>
         </div>
       </div>
     </div>
  </div>
</div>

<!-- Baton Status pop up Begin -->

<div class="modal fade" id="batonTransferStatusPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="exampleModalLabel1">Transfer Baton Role Request</h4>
              <p>Are you sure you want to accept the request?</p>
          </div>
          <div class="modal-body batonCustTxt">
              <div class="form-control-feedback leftFloatFull">
                <h6>User’s Message:</h6>
                <p class="user_message">Hi! Please accept my role request, that means you can activate that role when I need to perform privileged actions.</p>
              </div>
          </div>
          <div class="modal-footer">
            <div class="onCallBtMain txtCenter" style="margin:0 auto;">
              <button type="button" id="btBatonTransferAccept" data-value="1" class="btn btModelBatonAccept">Accept</button>
              <button type="button" id="btBatonTransferCancel" data-value="0" class="btn btModelBatonReject">Reject</button>
            </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="batonStatusPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="exampleModalLabel1">Take On Baton Role Request</h4>
              <p>Are you sure you want to accept the request?</p>
          </div>
          <div class="modal-body batonCustTxt" style="display:none;">
              <div class="form-control-feedback leftFloatFull">
                <h6>User’s Message:</h6>
                <p class="user_message">Hi! Please accept my role request, that means you can activate that role when I need to perform privileged actions.</p>
              </div>
          </div>
          <div class="modal-footer">
            <div class="onCallBtMain txtCenter" style="margin:0 auto;">
              <button type="button" id="btBatonAccept" data-value="1" class="btn btModelBatonAccept">Accept</button>
              <button type="button" id="btBatonCancel" data-value="0" class="btn btModelBatonReject">Reject</button>
            </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="batonStatusPopup3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="exampleModalLabel1">Reject Baton Role Request</h4>
              <p>Are you sure you want to reject the request?</p>
          </div>
          <div class="modal-body batonCustTxt">
            <form class="addBatonForm withRejectTxt">
              <div class="floatLeftFull txtCenter">
                <textarea id="reasonTxt" placeholder="Please write reason to reject…" maxlength="150" rows="3"></textarea>
              </div>
              <div class="floatLeftFull optionOnCall">
                <p class="italicTxt">Character Remaining: <span id="leftCount">150</span>/150</p>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="onCallBtMain txtCenter" style="margin:0 auto;">
              <button type="button" id="btBatonRejectAccept" class="btn btModelBatonReject">Yes</button>
              <button type="button" id="btBatonrejectCancel" class="btn btModelBatonCancel">No</button>
            </div>
          </div>
      </div>
  </div>
</div>

<!-- Unassign Baton Role -->
<div class="modal fade" id="unassignBatonPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="loader" id="unassignBatonLoader"></div>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Unassign Baton Role</h4>
                <p>You can unassign baton role from user:</p>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
            </div>
            <div class="modal-body">
                <form class="addBatonForm">
                  <div class="floatLeftFull txtCenter">
                    <textarea id="unassignBatonRoleText" readonly="true"></textarea>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
              <!-- <div style="margin:0 auto;" class="onCallBtMain txtCenter">
                <button type="button" id="btBatonAccept" class="btn btModelBatonAccept" onclick="changeRequestFinal(20,1,8);">Add</button>
                <button type="button" id="btBatonCancel" class="btn btModelBatonCancel" data-dismiss="modal">Cancel</button>
              </div> -->
                <div class="row showWarning">
                  <div class="col-md-6 pull-left">
                    <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                    <p>Are you sure you want to unassign user baton role?</p>
                  </div>
                  <div class="col-md-6 txtRight pull-right">
                    <button type="button" class="btn btn-success waves-effect" onclick="unassignBatonRoleFinal();">Yes</button>
                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal" onclick="closeUnassignedPopup();">Cancel</button>
                  </div>
                </div>
                <div class="row showSucess" style="display:none">
                    <div class="col-md-8 pull-left">
                      <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                      <p>User baton role has been unassigned successfully!</p>
                    </div>
                    <div class="col-md-4 txtRight pull-right">
                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ****************  -->

<!-- Baton Status pop up End -->
<script type="text/javascript">
$("#sidebarnav>li#menu10").addClass('active').find('a').addClass('active');
$("#sidebarnav>li#menu10").find("li").eq(1).addClass('active').find('a').addClass('active');

var textLen = 150;
var typingTimer;
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#user_search');

$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(searchUser, doneTypingInterval);
});
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

$('#reasonTxt').on('keyup',function(){
  var textLen = 150;
  var content = $('#reasonTxt').val();
  var len = content.length;
  var wordsLeft = textLen-len;
  $('#leftCount').html(wordsLeft);
});

function searchUser() {
  var $filterBy = $('input[name="filter_option"]:checked').val();
   $search_text = $input.val().trim();
   if($search_text == ''){
       // return false;
   }
   $input.blur();
   $("#baseLocationLoader").fadeIn("fast");
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/getRequestFilter',
       type: "POST",
       data: {'search_text':$search_text,'filter_by':$filterBy},
       success: function(data) {
         $('#baseLocationContent').html(data);
         $("#baseLocationLoader").fadeOut("fast");
         $('#showTotCount').html($('#totalCount').html());
         $('label[for="filter_option_takeon"] span').html($('#tCountTakeOn').html());
         $('label[for="filter_option_transfer"] span').html($('#tCountTransfer').html());
       }
   });
}

getBatonRequest('1','20',$input.val().trim());

$('input[name="filter_option"]').on('change', function(){
  $('input[name="filter_option"]').removeAttr('checked');
  $('input[value="'+$(this).val()+'"]').attr('checked', 'checked');
  searchUser();
});

function getBatonRequest(page,limit,$search_text){
  var $filterBy = $('input[name="filter_option"]:checked').val();
  $("#baseLocationLoader").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/getRequestFilter',
    type: "POST",
    data: {'page_number':page,'limit':limit,'search_text':$search_text,'filter_by':$filterBy},
    success: function(data) {
      $('#baseLocationContent').html(data);
      $("#baseLocationLoader").fadeOut("fast");
      $('#showTotCount').html($('#totalCount').html());
      $('label[for="filter_option_takeon"] span').html($('#tCountTakeOn').html());
      $('label[for="filter_option_transfer"] span').html($('#tCountTransfer').html());
    }
  });
}
function changeRequest(user_id,role_id){
  var user_note = $('tr[data-id="'+user_id+'"]').find('.user_note').html().trim();
  if(user_note != ''){
    $('#batonStatusPopup .batonCustTxt').show().find('.user_message').html(user_note);
  }else{
    $('#batonStatusPopup .batonCustTxt').hide();
  }
  $('#batonStatusPopup').attr('data-user',user_id);
  $('#batonStatusPopup').attr('data-role',role_id);
  $('#batonStatusPopup').attr('data-type','request');
  $('#batonStatusPopup').modal('toggle');
}

function changeRequestFinal(user_id,status,role_id,notes){
  $("#baseLocationLoader").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/changeBatonRequestStatus',
    type: "POST",
    data: {'user_id':user_id,'status':status,'role_id':role_id,'notes':notes},
    success: function(data) {
      var responseArray = JSON.parse(data);
      $("#baseLocationLoader").fadeOut("fast");
      if( responseArray['status'] == 1 ){
        reload('request');
      }
    }
  });
}

function changeTransferRequest(user_id,role_id){
  var user_note = $('tr[data-id="'+user_id+'"]').find('.user_note').html().trim();
  if(user_note != ''){
    $('#batonTransferStatusPopup .batonCustTxt').show().find('.user_message').html(user_note);
  }else{
    $('#batonTransferStatusPopup .batonCustTxt').hide();
  }
  $('#batonStatusPopup').attr('data-user',user_id);
  $('#batonStatusPopup').attr('data-role',role_id);
  $('#batonStatusPopup').attr('data-type','transfer');
  $('#batonTransferStatusPopup').modal('show');
}

function changeTransferRequestFinal(user_id,status,role_id,notes){
  $("#baseLocationLoader").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/changeBatonTransferRequestStatus',
    type: "POST",
    data: {'user_id':user_id,'status':status,'role_id':role_id,'notes':notes},
    success: function(data) {
      var responseArray = JSON.parse(data);
      $("#baseLocationLoader").fadeOut("fast");
      if( responseArray['status'] == 1 ){
        reload('transfer');
      }
    }
  });
}

$('#btBatonRejectAccept').on('click',function(){
  var type = $('#batonStatusPopup').attr('data-type');
  var user_id = $('#batonStatusPopup').attr('data-user');
  var role_id = $('#batonStatusPopup').attr('data-role');
  var notes = $('#reasonTxt').val();
  if(type == 'request'){
    changeRequestFinal(user_id,0,role_id,notes);
  }else if(type == 'transfer'){
    changeTransferRequestFinal(user_id,0,role_id,notes);
  }
});
$('#btBatonrejectCancel').on('click',function(){
  var textLen = 150;
  $('#reasonTxt').val('');
  $('#leftCount').html(textLen);

  var type = $('#batonStatusPopup').attr('data-type');
  if(type == 'request'){
    $('#batonStatusPopup').show().addClass('show');
  }else if(type == 'transfer'){
    $('#batonTransferStatusPopup').show().addClass('show');
  }
  $('#batonStatusPopup3').hide().removeClass('show');
});

$('#btBatonTransferAccept, #btBatonAccept').on('click',function(){
  var type = $('#batonStatusPopup').attr('data-type');
  var user_id = $('#batonStatusPopup').attr('data-user');
  var role_id = $('#batonStatusPopup').attr('data-role');
  if(type == 'request'){
    changeRequestFinal(user_id,1,role_id,'');
  }else if(type == 'transfer'){
    changeTransferRequestFinal(user_id,1,role_id,'');
  }
});

$('#btBatonCancel, #btBatonTransferCancel').on('click',function(){
  var textLen = 150;
  $('#reasonTxt').val('');
  $('#leftCount').html(textLen);

  var type = $('#batonStatusPopup').attr('data-type');
  if(type == 'request'){
    $('#batonStatusPopup').hide().removeClass('show');
  }else if(type == 'transfer'){
    $('#batonTransferStatusPopup').hide().removeClass('show');
  }
  $('#batonStatusPopup3').show().addClass('show');
});

function chngPage(val,limit) {
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
   var goVal=$("#chngPage").val();
   if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
       $("#batonListLoader").fadeIn("fast");
       $.ajax({
           url:'<?php echo BASE_URL; ?>institution/Switchboard/getRequestFilter',
           type: "POST",
           data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'search_text':$search_text},
           success: function(data) {
               $('#ajaxContent').html(data);
               $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
                   $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
                   $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $("#batonListLoader").fadeOut("fast");
           }
       });
   }

}
function chngCount(val){
   $("#batonListLoader").fadeIn("fast");
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
 $.ajax({
     url:'<?php echo BASE_URL; ?>institution/Switchboard/getRequestFilter',
     type: "POST",
     data: {'limit':val,'sort':a,'order':order,'search_text':$search_text},
     success: function(data) {
         $('#ajaxContent').html(data);
         $('#showTotCount').html($('#totalCount').html());
         $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
         if(a==b){
         $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
         }
         else{
         $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
         }
         $("#batonListLoader").fadeOut("fast");
     }
 });
}

function abc(val,limit){
   $("#batonListLoader").fadeIn("fast");
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
 $.ajax({
      url:'<?php echo BASE_URL; ?>institution/Switchboard/getRequestFilter',
      type: "POST",
      data: {'page_number':val,'limit':limit,'sort':a,'order':order,'search_text':$search_text},
      success: function(data) {
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
          $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
          if(a==b){
          $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
          }
          else{
          $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
          }
          $("#batonListLoader").fadeOut("fast");
      }
   });
}

function reload(type){
    if($('#batonStatusPopup3').hasClass('show')){
      $('#batonStatusPopup3').hide().removeClass('show');
    }

    if(type == 'request'){
      $('#batonStatusPopup').modal('toggle');
    }else if(type == 'transfer'){
      $('#batonTransferStatusPopup').modal('toggle');
    }

    var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
    var limit = $('#Data_Count').val();
    var $search_text = $input.val().trim();
    getBatonRequest(page,limit,$search_text);
    checkUser();
}
//
// function reload(){
//     if($('#batonStatusPopup3').hasClass('show')){
//       $('#batonStatusPopup3').hide().removeClass('show');
//     }
//     if($('#batonStatusPopup').hasClass('show')){
//         $('#batonStatusPopup').modal('toggle');
//     }
//     if($('#batonTransferStatusPopup').hasClass('show')){
//         $('#batonTransferStatusPopup').modal('toggle');
//     }
//     $('.modal-backdrop.fade.show').remove();
//     var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
//     var limit = $('#Data_Count').val();
//     var $search_text = $input.val().trim();
//     getBatonRequest(page,limit,$search_text);
//     checkUser();
// }

function userProfile(obj, id){
  $('#baseLocationLoader').fadeIn('fast');
  $.ajax({
    //NSDictionary* parameters = @{@"loggedin_user_id":loggedIn_userId,@"user_id":userId,kAPIDeviceId:[UIDevice currentDevice].identifierForVendor.UUIDString};
    url:'<?php echo BASE_URL; ?>institution/Switchboard/getUserProfileDetail',
    //url:'<?php echo BASE_URL; ?>mbapi/mbapiuserwebservices/getOtherUserProfileLite',
    type: "POST",
    // headers: {
    //     //Accept : "text/plain; charset=utf-8",
    //     "Content-Type": "application/json"
    // },
    // data: JSON.stringify({"loggedin_user_id":1,"user_id":6235}),
    data: {"user_id":id},
    success: function(data) {
      obj.parent('td').append(data);
      $('#baseLocationLoader').fadeOut('fast');
    }
  });
}

function closeUserProfile(){
  $('.sw-popup-overlay').fadeOut('fast').remove();
  $('.sw-userprofile-popup').fadeOut('fast').remove();
}

function unassignBatonRole(role_name, role_id, user_id){
  $('#unassignBatonPopup').attr('data-rolename', role_name);
  $('#unassignBatonPopup').attr('data-id', role_id);
  $('#unassignBatonPopup').attr('data-userid', user_id);
  $('#unassignBatonRoleText').val(role_name);
  $('#unassignBatonPopup .showWarning').show();
  $('#unassignBatonPopup .showSucess').hide();
  $("#unassignBatonLoader").fadeOut("fast");
  $('.sw-popup-overlay, .sw-userprofile-popup').fadeOut('fast');
}

function unassignBatonRoleFinal(){
  var role_id = $('#unassignBatonPopup').attr('data-id');
  var user_id = $('#unassignBatonPopup').attr('data-userid');
  var notes = '';
  $("#unassignBatonLoader").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/assignUnassignBatonRole',
    type: "POST",
    data: {'user_id':user_id,'status':0,'role_id':role_id,'notes':notes},
    success: function(data) {
      var responseArray = JSON.parse(data);
      //$("#unassignBatonLoader").fadeOut("fast");
      if( responseArray['status'] == 1 ){
        $('#unassignBatonPopup .showWarning').hide();
        $('#unassignBatonPopup .showSucess').show();
        location.reload();
      }
    }
  });
}

function closeUnassignedPopup(){
  $('.sw-popup-overlay, .sw-userprofile-popup').fadeIn('fast');
}
</script>
