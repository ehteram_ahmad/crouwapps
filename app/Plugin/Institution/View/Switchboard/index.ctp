<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<!-- <div class="loader"></div> -->
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
        <!-- Row -->
    <div class="row widgetMain dashCounts sw-dashboard">
        <!-- Column -->
        <div class="col-lg-4 col-md-6">
            <div class="card" id="card-total">
                <div id="loaderTotalBaton" class="loader"></div>
                <div class="card-block" onclick="location.href='<?php echo BASE_URL.'institution/Switchboard/manageBatonRoles'; ?>'">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-5 align-self-center text-center  p-l-0">
                            <?php echo $this->Html->image('Institution.total.svg'); ?>
                        </div>
                        <div class="col-7 text-left">
                            <span class="display-6" id="span_totalBaton">0</span>
                            <h6>Total Baton Roles</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-lg-4 col-md-6">
            <div class="card" id="card-assigned">
                <div id="loaderTotalAssiged" class="loader"></div>
                <div class="card-block" onclick="location.href='<?php echo BASE_URL.'institution/Switchboard/manageBatonRoles?type=Assigned'; ?>'">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-5 align-self-center text-center  p-l-0">
                            <?php echo $this->Html->image('Institution.assigned-baton-icon.svg'); ?>
                        </div>
                        <div class="col-7 text-left">
                            <span class="display-6" id="span_totalAssigned">0</span>
                            <h6>Assigned Baton Roles</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-lg-4 col-md-6">
            <div class="card" id="card-pending">
                <div id="loaderPendingReq" class="loader"></div>
                <div class="card-block" onclick="location.href='<?php echo BASE_URL.'institution/Switchboard/manageBatonRoles?type=Unassigned'; ?>'">
                <!-- <div class="card-block" onclick="location.href='<?php //echo BASE_URL.'institution/Switchboard/getRequest'; ?>'"> -->
                    <!-- Row -->
                    <div class="row">
                        <div class="col-5 align-self-center text-center p-l-0">
                            <?php echo $this->Html->image('Institution.pending.svg'); ?>
                        </div>
                        <div class="col-7 text-left">
                            <span class="display-6" id="span_totalUnassigned">0</span>
                            <h6>Unassigned Baton Roles</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card linkcard">
                <div class="card-block" onclick="location.href='<?php echo BASE_URL.'institution/Switchboard/manageBatonRoles?action=addBatonRole'; ?>'">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-4 align-self-center text-center">
                            <?php echo $this->Html->image('Institution.add-baton-icon.svg'); ?>
                        </div>
                        <div class="col-8 align-self-center">
                            <h6>Add Baton Roles</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card linkcard">
                <div class="card-block" onclick="location.href='<?php echo BASE_URL.'institution/Switchboard/generateQrCode'; ?>'">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-4 align-self-center text-center">
                            <?php echo $this->Html->image('Institution.qr-code.svg'); ?>
                        </div>
                        <div class="col-8 align-self-center">
                            <h6>Generate QR Codes</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-12">
            <div class="card latest-batonrole">
                <div id="loaderLatestBaton" class="loader"></div>
                <div class="card-title">
                    Latest Exchanged Baton Role(s)
                    <span class="nav_arrows">
                      <img class="prev_arrow" onmouseover="hover(this,'prev');" onmouseout="unhover(this,'prev');" src="<?php echo BASE_URL; ?>institution/img/prev_arrow_grey.svg" alt="">
                      <img class="next_arrow" onmouseover="hover(this,'next');" onmouseout="unhover(this,'next');" src="<?php echo BASE_URL; ?>institution/img/next_arrow_grey.svg" alt="">
                    </span>
                </div>
                <div class="card-block" style="padding: 5px 2px 0px 2px;">
                    <!-- Row -->
                    <div class="row">
                      <div class="col-md-12" style="text-align: center;">
                           <font color="green">Please wait while we are fetching the data.</font>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
      </div>
   </div>
</div>

<div class="modal-backdrop fade hide"></div>
<div class="modal fade" id="batonStatusPopup3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="exampleModalLabel1">Reject Baton Role Request</h4>
              <p>Are you sure you want to reject the request?</p>
          </div>
          <div class="modal-body batonCustTxt">
            <form class="addBatonForm withRejectTxt">
              <div class="floatLeftFull txtCenter">
                <textarea id="reasonTxt" placeholder="Please write reason to reject…" maxlength="150" rows="3"></textarea>
              </div>
              <div class="floatLeftFull optionOnCall">
                <p class="italicTxt">Character Remaining: <span id="leftCount">150</span>/150</p>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="onCallBtMain txtCenter" style="margin:0 auto;">
              <button type="button" id="btBatonRejectAccept" class="btn btModelBatonReject">Yes</button>
              <button type="button" id="btBatonrejectCancel" class="btn btModelBatonCancel">No</button>
            </div>
          </div>
      </div>
  </div>
</div>

<!-- Unassign Baton Role -->
<div class="modal fade" id="unassignBatonPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="loader" id="unassignBatonLoader"></div>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Unassign Baton Role</h4>
                <p>You can unassign baton role from user:</p>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
            </div>
            <div class="modal-body">
                <form class="addBatonForm">
                  <div class="floatLeftFull txtCenter">
                    <textarea id="unassignBatonRoleText" disabled="disabled" readonly="true"></textarea>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
              <!-- <div style="margin:0 auto;" class="onCallBtMain txtCenter">
                <button type="button" id="btBatonAccept" class="btn btModelBatonAccept" onclick="changeRequestFinal(20,1,8);">Add</button>
                <button type="button" id="btBatonCancel" class="btn btModelBatonCancel" data-dismiss="modal">Cancel</button>
              </div> -->
                <div class="row showWarning">
                  <div class="col-md-6 pull-left">
                    <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                    <p>Are you sure you want to unassign user baton role?</p>
                  </div>
                  <div class="col-md-6 txtRight pull-right">
                    <button type="button" class="btn btn-success waves-effect" onclick="unassignBatonRoleFinal();">Yes</button>
                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal" onclick="closeUnassignedPopup();">Cancel</button>
                  </div>
                </div>
                <div class="row showSucess" style="display:none">
                    <div class="col-md-8 pull-left">
                      <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                      <p>User baton role has been unassigned successfully!</p>
                    </div>
                    <div class="col-md-4 txtRight pull-right">
                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ****************  -->
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->


</div>
<script type="text/javascript">
$("#sidebarnav>li#menu1").addClass('active').find('a').addClass('active');

$(document).ready(function(){
  // getPendingRequest();
  getBatonroleCount();
  getLatestRequest();
});

function hover(element,tag) {
  element.setAttribute('src', '<?php echo BASE_URL; ?>institution/img/'+tag+'_arrow_blue.svg');
}

function unhover(element,tag) {
  element.setAttribute('src', '<?php echo BASE_URL; ?>institution/img/'+tag+'_arrow_grey.svg');
}

// $('body').on('hover','.next_arrow',function(event) {
//   console.log('sdsd');
//   $(this).attr('src','<?php //echo BASE_URL; ?>institution/img/next_arrow_blue.svg');
// });
// $('body').on('hover','.prev_arrow',function(event) {
//   console.log('sdsd');
//   $(this).attr('src','<?php //echo BASE_URL; ?>institution/img/prev_arrow_blue.svg');
// });
$('body').on('click','.next_arrow',function(event) {
    event.preventDefault();
    $('.custom_divs').animate({scrollLeft:'+=300'},500);
});
$('body').on('click','.prev_arrow',function(event) {
    event.preventDefault();
    $('.custom_divs').animate({scrollLeft:'-=300'},500);
});

function getPendingRequest(){
    $("#loaderPendingReq").fadeIn("fast");
    $.ajax({
    url:'<?php echo BASE_URL; ?>institution/Switchboard/getRequestFilter',
    type: "POST",
    data: {'page':'switchboard'},
    success: function(result) {
      if(result == ''){
        getPendingRequest();
      }else{
        $('#span_pendingRequest').html(result);
        $("#loaderPendingReq").fadeOut("fast");
      }
    }
  });
}
function getBatonroleCount(){
    $("#loaderTotalBaton").fadeIn("fast");
    $("#loaderTotalAssiged").fadeIn("fast");
    $("#loaderPendingReq").fadeIn("fast");
    $.ajax({
    url:'<?php echo BASE_URL; ?>institution/Switchboard/batonFilter',
    type: "POST",
    data: {'page':'switchboard'},
    success: function(result) {
      if(result == ''){
        getBatonroleCount();
      }else{
        var responseArray = JSON.parse(result);
        $('#span_totalBaton').html(responseArray['tCount']);
        $('#span_totalAssigned').html(responseArray['tAssigned']);
        $('#span_totalUnassigned').html(responseArray['tCount']-responseArray['tAssigned']);
        $("#loaderTotalBaton").fadeOut("fast");
        $("#loaderTotalAssiged").fadeOut("fast");
        $("#loaderPendingReq").fadeOut("fast");
      }
    }
  });
}
function getLatestRequest(){
    $("#loaderLatestBaton").fadeIn("fast");
    $.ajax({
    url:'<?php echo BASE_URL; ?>institution/Switchboard/getLatestRequest',
    type: "POST",
    success: function(result) {
      if(result == ''){
        getLatestRequest();
      }else{
        $('.latest-batonrole .card-block').html(result);
        $("#loaderLatestBaton").fadeOut("fast");
      }
    }
  });
}
function changeRequestFinal(user_id,status,role_id,notes){
  notes = $('#lt-bt-notes').html();
  $("#loaderLatestBaton").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/changeBatonRequestStatus',
    type: "POST",
    data: {'user_id':user_id,'status':status,'role_id':role_id,'notes':notes},
    success: function(data) {
      var responseArray = JSON.parse(data);
      //$("#loaderLatestBaton").fadeOut("fast");
      if( responseArray['status'] == 1 ){
        location.reload();
      }
    }
  });
}

function changeTransferRequestFinal(user_id,status,role_id,notes){
  notes = $('#lt-bt-notes').html();
  $("#loaderLatestBaton").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/changeBatonTransferRequestStatus',
    type: "POST",
    data: {'user_id':user_id,'status':status,'role_id':role_id,'notes':notes},
    success: function(data) {
      var responseArray = JSON.parse(data);
      //$("#loaderLatestBaton").fadeOut("fast");
      if( responseArray['status'] == 1 ){
        location.reload();
      }
    }
  });
}


function btBatonReject(){
  var textLen = 150;
  $('#reasonTxt').val('');
  $('#leftCount').html(textLen);

  $('#batonStatusPopup3').show().addClass('show');
  $('.modal-backdrop').removeClass('hide').addClass('show');
}

$('#reasonTxt').on('keyup',function(){
  var textLen = 150;
  var content = $('#reasonTxt').val();
  var len = content.length;
  var wordsLeft = textLen-len;
  $('#leftCount').html(wordsLeft);
});

$('#btBatonRejectAccept').on('click',function(){
  var type = $('#btBatonRejectButton').attr('data-type');
  var user_id = $('#btBatonRejectButton').attr('data-user');
  var role_id = $('#btBatonRejectButton').attr('data-role');
  var notes = $('#reasonTxt').val();

  $('#batonStatusPopup3').hide().removeClass('show');
  $('.modal-backdrop').removeClass('show').addClass('hide');

  if(type == 'request'){
    changeRequestFinal(user_id,0,role_id,notes);
  }else if(type == 'transfer'){
    changeTransferRequestFinal(user_id,0,role_id,notes);
  }
});

$('#btBatonrejectCancel').on('click',function(){
  var textLen = 150;
  $('#reasonTxt').val('');
  $('#leftCount').html(textLen);
  $('#batonStatusPopup3').hide().removeClass('show');
  $('.modal-backdrop').removeClass('show').addClass('hide');
});



function userProfile(obj, id){
  $('#loaderLatestBaton').fadeIn('fast');
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/Switchboard/getUserProfileDetail',
    type: "POST",
    data: {"user_id":id},
    success: function(data) {
      obj.parent('div').append(data);
      $('#loaderLatestBaton').fadeOut('fast');
    }
  });
}

function closeUserProfile(){
  $('.sw-popup-overlay').fadeOut('fast').remove();
  $('.sw-userprofile-popup').fadeOut('fast').remove();
}

function unassignBatonRole(role_name, role_id, user_id){
  $('#unassignBatonPopup').attr('data-rolename', role_name);
  $('#unassignBatonPopup').attr('data-id', role_id);
  $('#unassignBatonPopup').attr('data-userid', user_id);
  $('#unassignBatonRoleText').val(role_name);
  $('#unassignBatonPopup .showWarning').show();
  $('#unassignBatonPopup .showSucess').hide();
  $("#unassignBatonLoader").fadeOut("fast");
  $('.sw-popup-overlay, .sw-userprofile-popup').fadeOut('fast');
}

function unassignBatonRoleFinal(){
  var role_id = $('#unassignBatonPopup').attr('data-id');
  var user_id = $('#unassignBatonPopup').attr('data-userid');
  var notes = '';
  $("#unassignBatonLoader").fadeIn("fast");
  $.ajax({
    // url:'<?php //echo BASE_URL; ?>institution/InstitutionBaton/unassignBatonRoletAdmin',
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/assignUnassignBatonRole',
    type: "POST",
    data: {'user_id':user_id,'status':0,'role_id':role_id,'notes':notes},
    success: function(data) {
      var responseArray = JSON.parse(data);
      $("#unassignBatonLoader").fadeOut("fast");
      if( responseArray['status'] == 1 ){
        $('#unassignBatonPopup .showWarning').hide();
        $('#unassignBatonPopup .showSucess').show();
        location.reload();
      }
    }
  });
}

function closeUnassignedPopup(){
  $('.sw-popup-overlay, .sw-userprofile-popup').fadeIn('fast');
}




$("#batonListLoader").fadeOut("fast");

function getBatonList(page,limit,$search_text){
  $("#batonListLoader").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
    type: "POST",
    data: {'page_number':page,'limit':limit,'search_text':$search_text},
    success: function(data) {
      $('#ajaxContent').html(data);
      $("#batonListLoader").fadeOut("fast");
      $('#showTotCount').html($('#totalCount').html());
    }
  });
}
</script>
<script type="text/javascript">
function reload(){
    // $('#customModalBody').removeClass('bodyCustom');
    // $('#customModalFoot').css('text-align','center');
    var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
    var limit = $('#Data_Count').val();
    var $search_text = $input.val().trim();
    getBatonList(page,limit,$search_text);
}
</script>
