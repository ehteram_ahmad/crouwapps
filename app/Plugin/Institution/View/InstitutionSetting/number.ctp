<?php
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div id="ipLoader" class="loader"></div>
<style type="text/css">
#nhsLoader {
    position: absolute;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.modal-backdrop.fade {
	display: none;
}
.modal-open .modal-backdrop.fade {
	display: block;
}
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Settings: <span style="color:#10a5da;">NHS/MRN Number</span></h3>
          </div>
        </div>
        <span class="is-hidden" id="number_val"></span>
        <div class="row breadcrumb-title">
          <ol class="breadcrumb">
              <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
              <li class="breadcrumb-item active">Screen Shot Transactions</li> -->
          </ol>
        </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
          <div id="nhsNType" style="position:relative;">
            <div id="nhsLoader"></div>
            <div class="card">
              <div class="card-block padLeftRight0 pad0">
               <div class="floatLeftFull padLeftRight40 newListStyle">
                  <div class="tableTop floatLeft">
                     <span class="card-title">Please select a type of number for your institution.</span>
                  </div>
               </div>
               <div class="number_option floatLeftFull tableTop floatLeft padLeftRight40">
                 <div id="list-filter-options">
                   <input type="radio" name="filter_option" value="0" id="filter_option_nhs" checked>
                   <label for="filter_option_nhs">NHS Number</label>
                   <input type="radio" name="filter_option" value="1" id="filter_option_mrn">
                   <label for="filter_option_mrn">MRN Number</label>
                 </div>
               </div>
               <div class="floatLeftFull padLeftRight40 confirm_text is-hidden">
                  <div class="row">
                    <span class="note">Are you sure you want to set NHS Number as default patient number type?</span>
                    <div class="action_btn">
                      <button class="btn updateBtn" type="button" onclick="setType();" name="button">Update</button>
                      <button class="btn cancelBtn" type="button" name="button">Cancel</button>
                    </div>
                  </div>
               </div>
            </div>
             </div>
          </div>

        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
$("#sidebarnav>li#menu4").addClass('active').find('a').eq(0).addClass('active');
$("#sidebarnav>li#menu4").find("li").eq(2).addClass('active').find('a').addClass('active');
$(document).ready(function() {
  getNhsType();
});
function getNhsType(){
  $("#nhsLoader").fadeIn("fast");
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getNhsType',
    type: "POST",
    success: function(data) {
      console.log(data);
      $('#number_val').html(data);
      $('input[name="filter_option"]').prop('checked', false);
      $('input[name="filter_option"][value="'+data+'"]').prop('checked',true);
      // $('#nhsNType').html(data);
      $("#nhsLoader").fadeOut("fast");
    }
  });
}
$('body').on('click','input[name="filter_option"]',function(){
  var nhs_val = $('#number_val').html();
  $('.confirm_text').addClass('is-hidden');
  if($(this).val() != nhs_val){
    $('.confirm_text').removeClass('is-hidden');
  }
});
$('body').on('click','.cancelBtn',function(event){
  event.preventDefault();
  $('input[name="filter_option"]').prop('checked', false);
  var nhs_val = $('#number_val').html().trim().toString();
  console.log(nhs_val);
  $('input[name="filter_option"][value="'+nhs_val+'"]').prop('checked',true);
  $('.confirm_text').addClass('is-hidden');
});
function setType(){
  $("#nhsLoader").fadeIn("fast");
  var type = $('input[name="filter_option"]:checked').val();
  $.ajax({
    url: '<?php echo BASE_URL; ?>institution/InstitutionSetting/updatePatientType',
    type: 'POST',
    data: { type: type },
    success: function( result ){
      $("#nhsLoader").fadeOut("fast");
      $('.confirm_text').addClass('is-hidden');
        var responseArray = JSON.parse(result);
        if( responseArray['status'] == 1 ){
            var buttonHtml = '<button type="button" onClick="getNhsType()" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Confirmation');
            // $('#customModalBody').html('Please wait while a new user added');
            $('#customModalBody').html('Updated successfully');
            $('#customModalFoot').html(buttonHtml);
            $('#customBox').modal('show');

            $("#customBox .loaderImg").hide();
        }else{
          var buttonHtml = '<button type="button" onClick="location.reload()" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Confirmation');
          // $('#customModalBody').html('Please wait while a new user added');
          $('#customModalBody').html(responseArray['message']);
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');

          $("#customBox .loaderImg").hide();

        }
    },
    error: function( result ){
      $('.loader').hide();
      var responseArray = JSON.parse(result);
      var buttonHtml = '<button type="button" onClick="location.reload()" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
      $('#customModalLabel').html('Confirmation');
      // $('#customModalBody').html('Please wait while a new user added');
      $('#customModalBody').html(responseArray['message']);
      $('#customModalFoot').html(buttonHtml);
      $('#customBox').modal('show');

      $("#customBox .loaderImg").hide();
    }
  });
}
</script>
