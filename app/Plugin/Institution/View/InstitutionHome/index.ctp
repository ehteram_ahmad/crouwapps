<?php
/*foreach ($country as  $value) {
    // pr($value['countries']);
}*/
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div id="mainLoader" class="loader"></div>
<style type="text/css">
.loader, .loaderImg {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid" data-type="<?php echo $type; ?>">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-3 align-self-center col-5">
            <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
        </div>
        <div class="buttonTop col-md-9 col-7">
            <button class="btn hidden-sm-down" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" id="addUserButton"><i class="mdi mdi-plus-circle"></i> Add Pre-Approved Email(s)</button>
            <?php if((string)$_COOKIE['adminRoleId'] == '0'){ ?>
            <button class="btn hidden-sm-down greenBt" data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo" id="broadcastMessage"><i class="mdi mdi-email"></i> Broadcast Message</button>
            <?php } ?>
            <!-- <button class="btn hidden-sm-down greenBt" data-toggle="modal" data-target="#exampleModalQr" data-whatever="@mdo" id="requestQrCode"><i class="demo-icon icon-sub-barcode"></i> Request QR Code</button> -->
            <button class="btn hidden-sm-down greenBt" onclick="location.href='<?php echo BASE_URL.'institution/Switchboard/generateQrCode'; ?>'" id="requestQrCode"><i class="demo-icon icon-sub-barcode"></i> Request QR Code</button>
        </div>
    </div>
    <div class="row breadcrumb-title date_range" style="min-height: 15px !important;">
      <div class="floatRight dashboardBtn is-hidden">
        <button class="date-range-selection selected" data-value="0" type="button" name="button">All</button>
        <button class="date-range-selection" type="button" data-value="7" name="button">Week</button>
        <button class="date-range-selection" type="button" data-value="30" name="button">Month</button>
        <button id="dashboard-report-range" type="button" name="button">Period <i class="fa fa-sort-down"></i></button>
        <span class="is-hidden" id="start_date"></span>
        <span class="is-hidden" id="end_date"></span>
      </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div id="mainAjaxContent">
      <div class="row boaderLines widgetMain dashCounts">
          <!-- Column -->
          <div class="col-lg-3 col-md-6">
              <div class="card" data-value="all">
                  <div class="card-block" onclick="getListing('all');">
                      <!-- Row -->
                      <div class="row">
                          <div class="col-12"><span class="display-6"><?php echo isset($data['all_users'])?$data['all_users']:0; ?></span>
                              <h6>All Users</h6>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Column -->
          <div class="col-lg-3 col-md-6">
              <div class="card" data-value="0">
                  <div class="card-block" onclick="getListing(0);">
                      <!-- Row -->
                      <div class="row">
                          <div class="col-12"><span class="display-6"><?php echo isset($data['pending_users'])?$data['pending_users']:0; ?></span>
                              <h6>Awaiting Approval</h6>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Column -->
          <div class="col-lg-3 col-md-6">
              <div class="card" data-value="1">
                  <div class="card-block" onclick="getListing(1);">
                      <!-- Row -->
                      <div class="row">
                          <div class="col-12"><span class="display-6"><?php echo isset($data['approved_users'])?$data['approved_users']:0; ?></span>
                              <h6>Approved Users</h6>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- Column -->
          <div class="col-lg-3 col-md-6">
              <div class="card" data-value="7">
                  <div class="card-block" onclick="getListing(7);">
                      <!-- Row -->
                      <div class="row">
                          <div class="col-12"><span class="display-6"><?php echo isset($data['new_users'])?$data['new_users']:0; ?></span>
                              <h6>Recently Registered Users</h6>
                            </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- Row -->
      <div class="row boaderLines graph-block">
          <div class="col-md-7 ">
            <div class="card">
                <div class="card-block">
                    <div class="col-12 header">
                      <h3>Currently Active Professionals: 0</h3>
                    </div>
                    <div class="col-12">
                      <div id="chartContainer1" style="height: 243px; width: 90%;float:left;">
                        <img class="pie_def" src="<?php echo BASE_URL ?>institution/img/pie.png" alt="">
                      </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="col-md-5 ">
            <div class="card">
              <div class="card-block">
                  <div class="col-12 header">
                    <h3>Currently Signed In Users: 0</h3>
                  </div>
                  <div class="col-12">
                    <div id="chartContainer2" style="height: 243px; width: 98%;float:left;"></div>
                  </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <!-- ============================================================== -->
    <!--                    End PAge Content                            -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
    <!--                    Start Model Contents                        -->
    <!-- ============================================================== -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" style="height: 64%; display:none;" id="adduserLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content uploadFileBlock" id="bulkUploadUser">
            <div class="loaderImg" style="display:none;">
                </div>
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Add Pre-Approved Email(s)</h4>
                    <p>You can add Pre-Approved Email(s) by providing information below:</p>
                </div>
                <div class="modal-body">
                    <form id="bulkEmailUpload">
                        <div id="inputEmail" class="form-group">
                            <input type="email" class="form-control" id="userEmail" name="userEmail" placeholder="Enter user’s email address">
                            <div class="form-control-feedback" id="emptyEmail" style="display:none">Please Enter Email address</div>
                            <div class="form-control-feedback" id="invalidEmail" style="display:none">Please enter a valid email address</div>
                            <div class="form-control-feedback" id="alreadyExist" style="display:none"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="card">
                                    <div class="">
                                      <div class="borderSeparation">
                                        <h4>OR</h4>
                                      </div>
                                        <label for="input-file-now" class="nodefault">Add Multiple Pre-Approved Emails</label>
                                        <div class="pull-right">
                                          <span class="sampleTxt">Sample File Formats- </span>
                                          <a target="_blank" class="downloadLink" href="<?php echo BASE_URL; ?>institution/sample.csv" title="Sample">Download</a>
                                        </div>
                                        <input type="file" name="emailfile" id="input-file-now" class="dropify" accept=".csv, .xls, .xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                        <input type="hidden" name="company_id" type="test" value="<?php echo $_COOKIE['adminInstituteId']; ?>" />
                                        <div class="form-control-feedback" id="uploadResult" style="display:none">Upload Result:-</div>
                                        <div class="form-control-feedback" id="uploadSuccess" style="display:none"></div>
                                        <div class="form-control-feedback" id="uploadError" style="display:none"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="onCallBtMain txtCenter marBottom30">
                  <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" onclick="addUser();" id="addUserbtn1">Add Email</button>
                  <button type="submit" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" id="addUserbtn2" style="display:none;">Upload</button>
                  <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal2" tabindex="2" role="dialog" aria-labelledby="exampleModalLabel2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel2">Broadcast Message</h4>
                    <p>Write a message to broadcast in your institution.</p>
                </div>
                <div class="modal-body">
                    <form>
                        <div id="inputMessage" class="form-group">
                            <textarea class="form-control" id="textMessage" placeholder="Write here…" maxlength="500"></textarea>
                            <p class="txtLeft" id="counter"></p>
                            <div class="form-control-feedback" id="emptyMessage" style="display:none">Please Enter Message</div>
                            <div class="form-control-feedback" id="processingMessage" style="display:none;font-size:12px;color:green;font-style:italic">Please wait while broadcasting messege...</div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <div class="row showWarning" style="display:block">
                    <div class="col-md-6 pull-left">
                      <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                      <p>Are you sure you want to broadcast this message to all users within institution?</p>
                    </div>
                    <div class="col-md-6 txtRight pull-right">
                      <button type="button" class="btn btn-success waves-effect" id="preRequestBtn1">Yes</button>
                      <button type="button" id="postRequestBtn1" style="display: none;" class="btn btn-success waves-effect" data-toggle="modal" data-target="#exampleModal3"></button>
                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                  <div class="row showSucess" style="display:none">
                      <div class="col-md-8 pull-left">
                        <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                        <p>Message Brodacasted successfully to all institute members.</p>
                      </div>
                      <div class="col-md-4 txtRight pull-right">
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal3" tabindex="3" role="dialog" aria-labelledby="exampleModalLabel3">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="height:305px;">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel3">Broadcast Message</h4>
                </div>
                <div class="modal-body">
                   <p>Please confirm your password:</p>
                   <span class="green">* Please use your Medic Bleep password</span>
                    <form>
                        <div id="inputPassword" class="form-group marBottom52px">
                             <input type="text" autocorrect="off" autocapitalize="off" autocomplete="off" class="custom_m_Password form-control" id="messagePassword">
                             <div class="form-control-feedback" id="emptyPassword" style="display:none">Please Enter Password</div>
                             <div class="form-control-feedback" id="incorrectPassword" style="display:none">Password is incorrect</div>
                             <div class="form-control-feedback" id="responseMessage" style="display:none"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Back</button>
                    <button type="button" id="preConfirmBtn2" class="btn btn-success waves-effect" data-dismiss="modal">Confirm Password</button>
                    <button type="button" style="display: none;" id="postConfirmBtn2" class="btn btn-success waves-effect" data-dismiss="modal"></button>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!--                    End Model Content                           -->
    <!-- ============================================================== -->


</div>
<?php
        echo $this->Js->writeBuffer();
     ?>
<script>
    $('.onoffswitch').click(function(){
        var chckOnCall=$('#atwork').is(':checked');
    if(chckOnCall==true){
       console.log('1');
       }else{
           console.log('0');
       }
    });
    $('body').on('click','.date-range-selection',function(){
      var type = $(this).data("value");
      var date = new Date();
      // date.setDate(date.getDate() - parseInt(type));
      var yesterday = moment().subtract(parseInt(type), 'days')
      if(type == '0'){
        getDashboardGraph('all','','');
      }else{
        getDashboardGraph('range',yesterday.toLocaleString(),moment().toLocaleString());
      }
      $('.dashboardBtn button').removeClass("selected");
      $(this).addClass("selected");
      $('#dashboard-report-range').data('daterangepicker').setStartDate(new Date());
      $('#dashboard-report-range').data('daterangepicker').setEndDate(new Date());
    });
    // window.onload = function () {
    	CanvasJS.addColorSet("blueShades",[
                            "#2078a5",
                            "#2688bd",
                            "#3b9fd5",
                            "#53b5e2",
                            "#10a5da",
                            "#2bb5ff",
                            "#47bae2",
                            "#2dccfa",
                            "#76ddfb",
                            "#d2e3f2",
                            "#3db4f4",
                            "#02bbf1",
                            "#8ddaf6",
                            "#dbecf8",
                            "#2c82be",
                            "#53a8e2",
                            "#76ddfb"
                          ]);

    var chart2 = new CanvasJS.Chart("chartContainer2", {
    	animationEnabled: true,
    	theme: "theme2", // "light1", "light2", "dark1", "dark2"
    	dataPointMaxWidth: 80,
    	colorSet: "blueShades",
    	toolTip:{
        animationEnabled: true       //enable here
      },
    	data: [{
    		type: "column",
    		indexLabel:  "{y} user's",
        indexLabelMaxWidth: 50,
        indexLabelWrap: true,
    		indexLabelPlacement: 'inside',
    		indexLabelFontFamily: "'Open Sans', sans-serif",
    		indexLabelFontSize: 13,
    		indexLabelLineDashType: "light",
    		indexLabelFontColor: "white",
    		dataPoints: [
    			{ y: 0, label: "All Users", color: "#9fa9ba" },
    			{ y: 0, label: "Available", color: "#76ddfb" },
    			{ y: 0, label: "On Call", color: "#b4e18b" }
    		]
    	}]
    });
    chart2.render();

    // }
</script>

<script>
    // if (!jQuery().daterangepicker) {
    //     return;
    // }

    $('#dashboard-report-range').daterangepicker({
        "locale": {
            "format": "DD-MM-YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
        },
        "maxDate": new Date()
        //"startDate": "11/08/2015",
        //"endDate": "11/14/2015",
        // opens: (App.isRTL() ? 'right' : 'left'),
    }, function(start, end, label) {
      // 05/08/2019, 10:36:57
      console.log(start.format('DD/MM/YYYY, H:i:s'));
      console.log(end.format('DD/MM/YYYY, H:i:s'));
      $('#start_date').html(start.format('DD/MM/YYYY, H:I:s'));
      $('#end_date').html(end.format('DD/MM/YYYY, H:i:s'));

      $('.dashboardBtn button').removeClass("selected");
      $('#dashboard-report-range').addClass("selected");
      getDashboardGraph('range',$('.left .input-mini').val()+' 00:00:00',$('.right .input-mini').val()+' 23:59:59');
    });
</script>
<script type="text/javascript">
 // --------- INITIAL FUNCTIONS ------------
$(document).ready(function(){
  // getDashboard('1','20',$input.val().trim());
  getDashboardGraph('all','','');
  // $("#mainLoader.loader").fadeOut("fast");
});

 // --------- INITIAL FUNCTIONS ------------
 function getDashboardGraph(type,start_date,end_date){
   $("#mainLoader.loader").fadeIn("fast");
   $.ajax({
     url:'<?php echo BASE_URL; ?>institution/InstitutionHome/dashboardGraphFilter',
     type: "POST",
     data: {'type':type,'start_date':start_date,'end_date':end_date},
     success: function(data) {
       $('#mainAjaxContent').html(data);
       $("#mainLoader.loader").fadeOut("fast");
     }
   });
 };
 function getListing($type){
    if($('.card[data-value="'+$type+'"]').hasClass('selected')){
        return false;
    }
    if($type == 'all'){
        custom_val = '';
    }else{
        custom_val = '/'+$type;
    }
    window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionHome/users" + custom_val;
 }

 function getUserBatonRole(user_id,callBack){
   var returnval = [];
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserBatonRole',
      type: "POST",
      data: {'user_id':user_id},
      success: function(data) {
        console.log(data);
        var responseArray = JSON.parse(data);
        if( responseArray['status'] == 1 ){
          console.log(responseArray['data']);
          returnval = callBack(responseArray['data']);
        }
      }
    });
    return returnval;
 }

$('#input-file-now').change(function(){
    $("#userEmail").attr("disabled", "disabled");
    $("#addUserbtn1").hide();
    $("#addUserbtn2").show();

    $("#inputEmail").removeClass('has-danger');
    $("#emptyEmail").hide();
    $("#invalidEmail").hide();

    $("#uploadResult").hide();
    $("#uploadSuccess").hide();
    $("#uploadError").hide();

});

$("#userEmail").keyup(function() {
    var input = $(this);

    if( input.val() == "" ) {
      $("#addUserbtn1").show();
      $("#addUserbtn2").hide();
      $(".dropify-wrapper").removeClass('disable');
      $("#input-file-now").removeClass('disable');
    }
    else{
        $(".dropify-wrapper").addClass('disable');
        $("#input-file-now").addClass('disable');
        $("#addUserbtn1").show();
        $("#addUserbtn2").hide();
    }
});

$("#addUserButton").click(function(){
    $("#userEmail").val('');
    $("#inputEmail").removeClass('has-danger');
    $("#alreadyExist").hide();
    $("#emptyEmail").hide();
    $("#invalidEmail").hide();
    $(".dropify-wrapper").removeClass('disable');
    $("#input-file-now").removeClass('disable');
    $("#userEmail").attr("disabled", false);

    $(".dropify-clear").trigger("click");

});
$("#broadcastMessage").click(function(){
    $("#textMessage").val('');
    $("#messagePassword").val('');
    $("#inputMessage").removeClass('has-danger');
    $("#inputPassword").removeClass('has-danger');
    $("#emptyMessage").hide();
    $("#incorrectPassword").hide();
    var text_max = 500;
    $('#counter').html(text_max + ' characters remaining');
    $("#processingMessage").hide();
    $("#preRequestBtn1").removeAttr('disabled');
});
$('#preRequestBtn1').click(function() {
 $("#processingMessage").html('');
 var message = $('#textMessage').val().trim();
 if(message==""){
     $("#inputMessage").addClass('has-danger');
     $("#emptyMessage").show();
     return false;
 }else{
     $("#emptyMessage").hide();
     $("#inputPassword").removeClass('has-danger');
     $("#incorrectPassword").hide();
     $("#emptyPassword").hide();
     $("#messagePassword").val('');
     $("#inputMessage").removeClass('has-danger');
 }
 $('#postRequestBtn1').click();
});

$('#preConfirmBtn2').click(function() {
 var email= "<?php echo $_COOKIE['adminEmail']; ?>";
 var trsutId= "<?php echo $_COOKIE['adminInstituteId']; ?>";
 var message = $('#textMessage').val().trim();
 var password = $('#messagePassword').val().trim();
    if(password == ""){
        $("#inputPassword").addClass('has-danger');
        $("#emptyPassword").show();
        $("#incorrectPassword").hide();
        return false;
    }else{
        $("#processingMessage").show();
        $("#processingMessage").css('color','green').html('Please wait while broadcasting message...');
        $("#preRequestBtn1").attr('disabled','disabled');
        $("#preRequestBtn1").css('background-color','grey','border','grey');
        // $('#exampleModal2 .showWarning').hide();
        var subsIds = [];
        var companydetail = {company_id: trsutId};
        var companydetailJson = JSON.stringify(companydetail);
        console.log(password);
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionHome/broadCastMessage',
            type: 'POST',
            data: {'password':password,'message':message},
            success: function( result ){
                $("#customBox .loaderImg").hide();
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                  if($('#preRequestBtn1').attr('disabled')){
                    $('#exampleModal2 #textMessage').attr('readonly',true);
                    $('#exampleModal2 #counter').hide();
                    $('#exampleModal2 .showSucess').show();
                    $('#exampleModal2 .showWarning').hide();
                    // $('#textMessage').val('');
                    $('#counter').html('500 characters remaining');
                    $("#processingMessage").hide();
                  }

                     // $.toast({
                     //    heading: 'Success',
                     //    text: 'Message Sent successfully',
                     //    position: 'top-right',
                     //    loaderBg:'#2c2b2e',
                     //    icon: 'success',
                     //    hideAfter: 3000,
                     //    stack: 6
                     //  });

                     // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                     // $('#customModalLabel').html('Confirmation');
                     // $('#customModalBody').html('Message Sent successfully');
                     // $('#customModalFoot').html(buttonHtml);
                     // $('#customBox').modal('show');


                    $("#preRequestBtn1").removeAttr('disabled');
                    // $("#processingMessage").html("Message Sent.");
                    $("#preRequestBtn1").css('background-color','#10a5da','border','#10a5da');
                  }else if( responseArray['status'] == 2 ){
                    if($('#exampleModal2').hasClass('show')){
                      $('#exampleModal2').modal('hide');
                    }
                    var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                    $('#customModalLabel').html('Alert');
                    $('#customModalBody').html(responseArray['message']);
                    $('#customModalFoot').html(buttonHtml);
                    $('#customBox').modal('show');
                    $("#customBox .loaderImg").hide();
                  }else{
                    $("#processingMessage").css('color','red').html(responseArray['message']).show();
                    $("#preRequestBtn1").removeAttr('disabled');
                    $("#preRequestBtn1").css('background-color','#10a5da','border','#10a5da');
                }
            }
        });
    }
});
 $('#messagePassword').keypress(function(e){
     if(e.which == 13){//Enter key pressed
         $('#preConfirmBtn2').click();
         return false;
     }
 });

 function exportuserChat(userId){
   $("#mainLoader.loader").fadeIn("fast");
    window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionUser/exportChat/" + userId;
 }


 $("form#bulkEmailUpload").submit(function(){
     var formData = new FormData(this);
     $('#adduserLoader').fadeIn('fast');

    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/addUserFileUpload',
        type: 'POST',
        data: formData,
        success: function (data) {
            console.log(data);
            $('#adduserLoader').fadeOut('fast');
            var responseArray = JSON.parse(data);
            if( responseArray['status'] == 1 ){
                $(".uploadFileBlock .loaderImg").css("display", "none");
                $("#uploadResult").show();
                $("#uploadSuccess").show();
                $("#uploadError").show();
                $("#uploadSuccess").html("Success: " + responseArray['data']['addUserData']['valid_email_count']);
                $("#uploadError").html("Error: " + responseArray['data']['addUserData']['invalid_email_count']);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

     return false;
 });

 $('#requestQrCode').on('click',function(){
    $("#number-of-qr-code").val('');
    $("#validity").val(0);
 });

 $('body').on('input change','#number-of-qr-code, #validity',function(){
   $('#exampleModalQr .showWarning').show();
   $('#exampleModalQr .showSucess').hide();
 });

function requestQrCode(){
    var numberOfQrCode = $("#number-of-qr-code").val();
    var validity = $("#validity option:selected").val();
    if(numberOfQrCode == '' || numberOfQrCode == 0){
        alert('Enter a valid number.');
        return false;
    }
    if(numberOfQrCode > 1000){
        alert('Maximum limit is 1000.');
        return false;
    }
    if(validity == '' || validity == 0){
        alert('Select validity.');
        return false;
    }
    $(".loaderImg").show();
    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionQr/requestQrCode',
        type: 'POST',
        data: {'quantity': numberOfQrCode, 'validity': validity},
        success: function( result ){
            $(".loaderImg").hide();
            $("#customBox .loaderImg").hide();
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                // $.toast({
                //     heading: 'Requesting...',
                //     text: 'Please wait while Requesting qr Code',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });

                // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // // $('#customModalBody').html('Please wait while Requesting qr Code');
                // $('#customModalBody').html('QR code requested successfully');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
                $('#exampleModalQr .showWarning').hide();
                $('#exampleModalQr .showSucess').show();
                $("#number-of-qr-code").val('');
                $("#validity").val(0);

                // $('#exampleModalQr').modal('toggle');
            }else{
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $('#exampleModalQr').modal('toggle');
            }
        },
        error: function( result ){
            $(".loaderImg").hide();
            var responseArray = JSON.parse(result);
                $("#inputEmail").addClass('has-danger');
                $("#alreadyExist").show();
                $("#alreadyExist").html('* '+responseArray['values']['message']+'.' );
                $("#emptyEmail").hide();
                $("#invalidEmail").hide();

        }
    });
}

function changeStatus(email, userId, status){
    $("#mainLoader.loader").fadeIn("fast");
    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/changeAssignStatus',
        type: 'POST',
        data: {'email':email, 'user_id': userId, 'status': status},
        success: function( result ){
            $("#mainLoader.loader").fadeOut("fast");
            $("#customBox .loaderImg").hide();

            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                // $.toast({
                //     heading: 'Success',
                //     text: responseArray['message'],
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                // location.reload();
                var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                $('#customModalBody').html(responseArray['message']);
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

            }else if(responseArray['status'] == 2){
              var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
              $('#customModalLabel').html('Error');
              $('#customModalBody').html(responseArray['message']);
              $('#customModalFoot').html(buttonHtml);
              $('#customBox').modal('show');

              $(".loading_overlay1").hide();
            }else{

                // $.toast({
                //     heading: 'Error',
                //     text: 'An error has occurred. Please try again.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'error',
                //     hideAfter: 3000,
                //     stack: 6
                //   });
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $(".loading_overlay1").hide();
            }
        },
        error: function( result ){
            $("#mainLoader.loader").fadeOut("fast");
            $("#customBox .loaderImg").hide();
             // $.toast({
             //        heading: 'Error',
             //        text: 'An error has occurred. Please try again.',
             //        position: 'top-right',
             //        loaderBg:'#2c2b2e',
             //        icon: 'error',
             //        hideAfter: 3000,
             //        stack: 6
             //      });
             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Error');
             $('#customModalBody').html('An error has occurred. Please try again.');
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');

             $(".loading_overlay1").hide();

        }
    });
}

function changeBulkStatus(status) {
    $("#mainLoader.loader").fadeIn("fast");
    var $data = [];
    var $type = $('.card.selected').attr('data-value');
    $('.selectiveCheck input[type="checkbox"]:not(#selectAll):checked').each(function(){
        var details = {};
        var userId = $(this).parents('tr').attr('id');
        var email = $(this).parents('tr').attr('data-email');
        details.user_id = userId;
        details.email = email;
        details.status = status;
        $data.push(details);
    });

    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/changeBulkAssignStatus',
        type: 'POST',
        data: {'user_data' : JSON.stringify($data)},
        success: function( result ){
            $("#mainLoader.loader").fadeOut("fast");
            $("#customBox .loaderImg").hide();

            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){

                var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                $('#customModalBody').html(responseArray['message']);
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                if($type == 0){
                    $('#assign_btn').attr('disabled',true);
                }else{
                    $('#unassign_btn').attr('disabled',true);
                }

                // $.toast({
                //     heading: 'Success',
                //     text: responseArray['message'],
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                // location.reload();
            }else{

                // $.toast({
                //     heading: 'Error',
                //     text: 'An error has occurred. Please try again.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'error',
                //     hideAfter: 3000,
                //     stack: 6
                //   });
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $(".loading_overlay1").hide();
            }
        },
        error: function( result ){
            $("#customBox .loaderImg").hide();
             // $.toast({
             //        heading: 'Error',
             //        text: 'An error has occurred. Please try again.',
             //        position: 'top-right',
             //        loaderBg:'#2c2b2e',
             //        icon: 'error',
             //        hideAfter: 3000,
             //        stack: 6
             //      });
             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
             $(".loading_overlay1").hide();

        }
    });

}

function addUser(){
    var userEmail = $("#userEmail").val().trim();
    var regex = /^[_A-Za-z0-9-']+(\.[_A-Za-z0-9-']+)*@[A-Za-z0-9-]+(\.[a-z0-9-]+)*(\.[A-Za-z]{2,4})$/;
    if(userEmail == '' || userEmail == null){
        $("#inputEmail").addClass('has-danger');
        $("#emptyEmail").show();
        $("#invalidEmail").hide();
    }else if(!regex.test(userEmail)){
         $("#inputEmail").addClass('has-danger');
         $("#invalidEmail").show();
         $("#emptyEmail").hide();
    }else{
        $('#adduserLoader').fadeIn('fast');
        $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/addUser',
        type: 'POST',
        data: { email: userEmail },
        success: function( result ){
            $('#adduserLoader').fadeOut('fast');
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                // $.toast({
                //     heading: 'New User Adding',
                //     text: 'Please wait while a new user added.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                var buttonHtml = '<button type="button"  class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                // $('#customModalBody').html('Please wait while a new user added');
                $('#customModalBody').html('New User added successfully');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $("#customBox .loaderImg").hide();

                $('#exampleModal').modal('toggle');
            }else{
                $("#inputEmail").addClass('has-danger');
                $("#alreadyExist").show();
                $("#alreadyExist").html('* '+responseArray['message']+'.' );
                $("#emptyEmail").hide();
                $("#invalidEmail").hide();

            }
        },
        error: function( result ){
            $('#adduserLoader').fadeOut('fast');
            var responseArray = JSON.parse(result);
                $("#inputEmail").addClass('has-danger');
                $("#alreadyExist").show();
                $("#alreadyExist").html('* '+responseArray['message']+'.' );
                $("#emptyEmail").hide();
                $("#invalidEmail").hide();

        }
    });
    }
}

function removeUser(userEmail, userId){
    var buttonHtml = '<div class="row mainDiv" style="text-align: center;">';
        buttonHtml += '<button type="button" class="btn btn-info waves-effect redBtCustom" id="removeReqBtn">Remove</button>';
        buttonHtml += '<button type="button" onClick="removeClass();" class="btn btn-info waves-effect whiteBtCustom" data-dismiss="modal">Cancel</button>';
        buttonHtml += '</div>';
        buttonHtml += '<div class="row showWarning" style="display:none">';
        buttonHtml += '<div class="col-md-6 pull-left">';
        buttonHtml += '<img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">';
        buttonHtml += '<p>Are you sure you want to Remove User from your Institution?</p>';
        buttonHtml += '</div>';
        buttonHtml += '<div class="col-md-6 txtRight pull-right">';
        buttonHtml += '<button type="button" class="btn btn-success waves-effect" onclick="removeUserFinal(\''+userEmail+'\', '+userId+');">Yes</button>';
        buttonHtml += '<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>';
        buttonHtml += '</div>';
        buttonHtml += '</div>';
        buttonHtml += '<div class="row showSucess" style="display:none">';
        buttonHtml += '<div class="col-md-8 pull-left">';
        buttonHtml += '<img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">';
        buttonHtml += '<p style="margin-top: 7px;">User has been removed successfully.</p>';
        buttonHtml += '</div>';
        buttonHtml += '<div class="col-md-4 txtRight pull-right">';
        buttonHtml += '<button type="button" class="btn btn-info waves-effect" onClick="reload();" data-dismiss="modal">Close</button>';
        buttonHtml += '</div>';
        buttonHtml += '</div>';


    $('#customModalLabel').html('Remove User<p>Remove user from your institution.</p>');
    $('#customModalBody').addClass('bodyCustom').html('<span><img src="<?php echo BASE_URL ?>institution/img/sub-removeUser.svg" alt=""></span><p>It can’t be undone . User will be removed from your institution immediately and would not be able to access the Directory.</p>');
    $('#customModalFoot').css('text-align','left').html(buttonHtml);
    $('#customBox').modal('show');
}

$('body').on('click','#removeReqBtn',function(){
  $('#customBox .row.mainDiv').hide();
  $('#customBox .row.showWarning').show();
  $('#customBox .row.showSucess').hide();
});

function removeClass(){
  $('#customModalBody').removeClass('bodyCustom');
  $('#customModalFoot').css('text-align','center');
}

function removeUserFinal(userEmail, userId){
    // $('#customBox').modal('hide');
    // $('#customModalBody').removeClass('bodyCustom');
    var email = userEmail;
    var userId = userId;
    $("#customBox .loaderImg").show();
    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/removeUser',
        type: 'POST',
        data: {'email': email, 'user_id': userId},
        success: function( result ){
            console.log(result);
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                $("#customBox .loaderImg").hide();
                $('#customBox .row.mainDiv').hide();
                $('#customBox .row.showWarning').hide();
                $('#customBox .row.showSucess').show();
                // var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // $('#customModalBody').html('User removed successfully');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
                // window.location.reload();
            }else if(responseArray['status'] == 2){
              var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
              $('#customModalLabel').html('Error');
              $('#customModalBody').removeClass('bodyCustom').html(responseArray['message']);
              $('#customModalFoot').html(buttonHtml);
              $('#customModalFoot').css('text-align','center');

              $("#customBox .loaderImg").hide();
            }else{
               var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
               $('#customModalLabel').html('Error');
               $('#customModalBody').removeClass('bodyCustom').html('An error has occurred. Please try again');
               $('#customModalFoot').html(buttonHtml);
               $('#customModalFoot').css('text-align','center');
               // $('#customBox').modal('show');

               $("#customBox .loaderImg").hide();
            }
        },
        error: function( result ){
            // $.toast({
            //     heading: 'Error',
            //     text: 'An error has occurred. Please try again.',
            //     position: 'top-right',
            //     loaderBg:'#2c2b2e',
            //     icon: 'error',
            //     hideAfter: 3000,
            //     stack: 6
            // });
            var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Error');
            $('#customModalBody').removeClass('bodyCustom').html('An error has occurred. Please try again');
            $('#customModalFoot').html(buttonHtml);
            $('#customModalFoot').css('text-align','center');
            // $('#customBox').modal('show');

            $("#customBox .loaderImg").hide();
        }
    });

}

function customSubscription(){
    var email = $('#customSubscriptionPopUp').attr('data-userEmail');
    var userId = $('#customSubscriptionPopUp').attr('data-user');
    var exdate = $("#subscription_expiry_date").val();
    var userSubscription = $('#customSubscriptionPopUp').attr('data-subscription');
    $('#mainLoader').fadeIn('fast');

    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/updateSubscription',
        type: 'POST',
        data: {'email' : email,'user_id' : userId,'custom_date' : exdate},
        success: function( result ){
          $('#mainLoader').fadeOut('fast');
            // $('#customSubscriptionPopUp').modal('hide');
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
              $("#usersubscriptionexpiryDate").html('User Subscription Expiry Date: ' + exdate);

                // var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // $('#customModalBody').html('Subscription period is updated');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
                // $("#customBox .loaderImg").hide();
                $('#customSubscriptionPopUp .showDefault').hide();
                $('#customSubscriptionPopUp .showWarning').hide();
                $('#customSubscriptionPopUp .showSucess').show();

                $(".loading_overlay").hide();
                // location.reload();
            }else{
                // $.toast({
                //     heading: 'Error',
                //     text: 'An error has occurred. Please try again.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'error',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $("#customBox .loaderImg").hide();

                $(".loading_overlay").hide();
            }
        },
        error: function( result ){
            // $('#customSubscriptionPopUp').modal('hide');
            $('#mainLoader').fadeOut('fast');
             // $.toast({
             //        heading: 'Error',
             //        text: 'An error has occurred. Please try again.',
             //        position: 'top-right',
             //        loaderBg:'#2c2b2e',
             //        icon: 'error',
             //        hideAfter: 3000,
             //        stack: 6
             //    });
             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Error');
             $('#customModalBody').html('An error has occurred. Please try again');
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');
             $("#customBox .loaderImg").hide();

            $(".loading_overlay").hide();
        }
    });
}

function reload(){
    $('#customModalBody').removeClass('bodyCustom');
    $('#customModalFoot').css('text-align','center');
    var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
    var limit = $('#Data_Count').val();
    var $search_text = $input.val().trim();
    getDashCount();
    getDashboard(page,limit,$search_text);
    notifications();
}

function getDashCount(){
    // widgetMain dashCounts
    var $type = $('.container-fluid').attr('data-type');
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getDashboardCounts',
      type: "POST",
      success: function(data) {
        $('.widgetMain.dashCounts').html(data);
        $('#assign_btn').hide();
        $('#unassign_btn').hide();
        if($type == 0 || $type == 1){
            $('.selectiveCheck').show();
            if($type == 0){
                $('#assign_btn').show();
            }else if($type == 1){
                $('#unassign_btn').show();
            }
        }else{
            $('.selectiveCheck').hide();
        }
        $('.card.selected').removeClass('selected');
        $('.card[data-value="'+$type+'"]').addClass('selected');
      }
    });
}
</script>
