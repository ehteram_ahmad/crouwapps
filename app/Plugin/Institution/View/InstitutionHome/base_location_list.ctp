<?php
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Base Location</h3>
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">Base Location</li>
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
      <div class="card">
         <div class="card-block padLeftRight0">
            <div class="floatLeftFull padLeftRight14">
            <!---Add  Base Location Start[Start]-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="loader" style="height: 64%; display:none;" id="adduserLoader"></div>
                        <div class="modal-dialog" role="document">
                            <div class="modal-content uploadFileBlock" id="bulkUploadUser">
                            <div class="loaderImg" style="display:none;">
                                <?php //echo $this->Html->image('Institution.page-loader.gif', array("class"=> "loading-image" )); ?>

                                </div>
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">Add Base Location</h4>
                                    <p>You can add base location by providing information below:</p>
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                </div>
                                <div class="modal-body">
                                    <form id="bulkEmailUpload">
                                        <div id="inputBaseLocation" class="form-group">
                                            <input type="baseLocation" class="form-control" id="baseLocation" name="baseLocation" placeholder="Enter Base Location">
                                            <div class="form-control-feedback" id="emptyBaseLocation" style="display:none">Please Enter Base Location</div>
                                            <div class="form-control-feedback" id="alreadyBaseLocation" style="display:none"></div>
                                        </div>
                                </div>
                                <div class="onCallBtMain txtCenter marBottom30">
                                  <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" onclick="addBaseLocation();" id="addUserbtn1">Add Base Location</button>
                                  <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
            <!---Add  Base Location Start[END]-->
               <div class="tableTop floatLeft baseLocation">
                  <span class="allIconUsers">
                  <img src="<?php echo BASE_URL ?>institution/img/allUsers.svg" alt=""></span>
                  <span class="card-title">Total Base Locations :</span>
                  <span class="display-6" id="showTotCount"></span>
                  <button class="btn hidden-sm-down" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" id="addUserButton"><i class="mdi mdi-plus-circle"></i> Add Base Location</button>
                  <div class="assignBtnAll">
                     <button id="unassign_btn" disabled="disabled" onclick="changeBulkStatus(0);" style="display: none;" class="btn bulk_action_btn pull-right hidden-sm-down btn-primary"> + Unassign Selected</button>
                     <button id="assign_btn" disabled="disabled" onclick="changeBulkStatus(1);" style="display: none;" class="btn bulk_action_btn pull-right hidden-sm-down btn-primary"> + Assign Selected</button>
                  </div>
               </div>
               <div id="userlisting_filter" class="dataTables_filter baseLocationFilter">
                  <label><i class="demo-icon icon-sub-lense"></i></label>
                  <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
               </div>
            </div>
            <div class="table-responsive wordBreak" id="ajaxContent">
              <table class="table stylish-table customPading">
                  <thead>
                      <tr>
                          <th>Name </th>
                          <th>Status</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                          if(isset($data['base_location_list']) && count($data['base_location_list']) > 0){
                          foreach( $data['base_location_list'] as $baseLocation){
                      ?>
                      <tr>
                          <td><?php echo $baseLocation['RoleTag']['value']; ?></td>
                          <td class="statusBtn" style="width:5%;">
                            <?php if($baseLocation['RoleTag']['status']==1){ ?>
                                      <button onclick="changeStatus('<?php echo $baseLocation['RoleTag']['id']; ?>', 0);" type="button" class="btn btn-primary"> Active </button>
                            <?php }else{ ?>
                                     <button onclick="changeStatus('<?php echo $baseLocation['RoleTag']['id']; ?>', 1);" type="button" class="btn btn-outline-primary"> Inactive </button>
                            <?php }
                              ?>
                          </td>
                      </tr>
                      <?php }}elseif(isset($tCount) && $tCount > 0){?>

                      <tr>
                         <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                      </tr>
                      <?php }else{ ?>
                      <tr>
                         <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                      </tr>
                      <?php  } ?>
                  </tbody>
              </table>
              <span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
              <div class="actions marBottom10 paddingSide15 leftFloat98per">
              <?php
                  if(isset($data['user_list']) && count($data['user_list']) > 0){
                      echo $this->element('pagination');
                  }
              ?>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->




    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- User Privilege -->
    <div class="modal fade" id="privilegePopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="priviligeLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Update User Privilege</h4>
                    <p>You can update user privilege by selecting role type below:</p>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                </div>
                <div class="modal-body">
                    <form>
                        <!-- <span id="subscribtionNote1" style="font-size: 16px;font-weight: normal;">Please select end date of this user’s subscription.</span><br> -->
                        <!-- <label>Select Date: </label> -->
                        <div class="row btCust">
                            <div class="col-md-4">
                                <input type="radio" onclick="setPrivilege(0);" name="privilige" value="0" placeholder="">
                                <label>Admin</label>
                            </div>
                            <div class="col-md-4">
                                <input type="radio" onclick="setPrivilege(1);" name="privilige" value="1" placeholder="">
                                <label>Subadmin</label>
                            </div>
                            <div class="col-md-4">
                                <input type="radio" onclick="setPrivilege(2);" name="privilige" value="2" placeholder="">
                                <label>None</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <div class="row showWarning" style="display:block">
                      <div class="col-md-6 pull-left">
                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                        <p>Are you sure you want to allow this user to access limited features of  Institution Panel?</p>
                      </div>
                      <div class="col-md-6 txtRight pull-right">
                        <button type="button" class="btn btn-success waves-effect" onclick="setPrivilege_final();">Yes</button>
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                    <div class="row showSucess" style="display:none">
                        <div class="col-md-8 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>User Privilege has been updated successfully!</p>
                        </div>
                        <div class="col-md-4 txtRight pull-right">
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
    $("#sidebarnav>li#menu3").addClass('active').find('a').addClass('active');

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#user_search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(searchUser, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function searchUser() {
       $search_text = $input.val().trim();
       if($search_text == ''){
           // return false;
       }
       $input.blur();
       $(".loader").fadeIn("fast");
       $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionHome/baseLocationListFilter',
           type: "POST",
           data: {'search_text':$search_text},
           success: function(data) {
               $('#ajaxContent').html(data);
               $('#showTotCount').html($('#totalCount').html());
               $(".loader").fadeOut("fast");
               $('#assign_btn').hide();
               $('#unassign_btn').hide();
           }
       });
    }
    // --------- INITIAL FUNCTIONS ------------

    getUserList('1','20',$input.val().trim());

    function getUserList(page,limit,$search_text){
       $(".loader").fadeIn("fast");
       $.ajax({
         url:'<?php echo BASE_URL; ?>institution/InstitutionHome/baseLocationListFilter',
         type: "POST",
         data: {'page_number':page,'limit':limit,'search_text':$search_text},
         success: function(data) {
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
           $(".loader").fadeOut("fast");
         }
       });
    }

    // --------- INITIAL FUNCTIONS ------------

    function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        var goVal=$("#chngPage").val();
        $search_text = $input.val().trim();
        if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
            $(".loader").fadeIn("fast");
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionHome/baseLocationListFilter',
                type: "POST",
                data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'search_text':$search_text},
                success: function(data) {
                    $('#ajaxContent').html(data);
                    $('#showTotCount').html($('#totalCount').html());
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
        }

    }
    function chngCount(val){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        $search_text = $input.val().trim();
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/baseLocationListFilter',
            type: "POST",
            data: {'limit':val,'sort':a,'order':order,'search_text':$search_text},
            success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
                $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                }
                else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                }
                $(".loader").fadeOut("fast");
            }
        });
    }

    function abc(val,limit){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        $search_text = $input.val().trim();
      $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionHome/baseLocationListFilter',
           type: "POST",
           data: {'page_number':val,'limit':limit,'sort':a,'order':order,'search_text':$search_text},
           success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
        });
    }
</script>
<script type="text/javascript">
    $(function () {
      $('#privilegePopUp').on('hide.bs.modal', function (event) {
        if($('#privilegePopUp .showSucess').css('display') == 'block'){
          reload();
        }
      });
      $('#privilegePopUp').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var email = button.data('useremail'); // Extract info from data-* attributes
        var userid = button.data('userid'); // Extract info from data-* attributes
        var adminEmail = "<?php echo (string)$_COOKIE['adminEmail']; ?>";
        $('#privilegePopUp .showWarning').hide();
        $('#privilegePopUp .showSucess').hide();

        if(email == adminEmail){
            var buttonHtml = '<button type="button" onClick="closePop();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Error');
            $('#customModalBody').html('You can not perform this action. Please contact to support@medicbleep.com');
            $('#customModalFoot').html(buttonHtml);
            $('#customBox').modal('show');
            $("#customBox .loaderImg").hide();
            return ;
        }
        $('#priviligeLoader').fadeIn('fast');
        $('#privilegePopUp').attr('data-userEmail',email);
        $('#privilegePopUp input[name="privilige"]').prop('checked', false);;
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserPrivilegeStatus',
            type: 'POST',
            data: {'user_email': email, 'user_id': userid},
            success: function( result ){
                $('#priviligeLoader').fadeOut('fast');
                console.log(result);
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                    // if(responseArray['data']['role_id'] != 'null'){
                    $('#privilegePopUp input[name="privilige"][value="'+responseArray['data']['role_id']+'"]').prop('checked', true);
                    $('#privilegePopUp').attr('data-privilige',responseArray['data']['role_id']);
                    // }
                }
            },
            error: function( result ){
                $('#priviligeLoader').fadeOut('fast');
            }
        });
      });
    });

    function closePop(){
        $('#privilegePopUp').modal('hide');
    }

    function setPrivilege(role_id){
        var privilige = $('#privilegePopUp').attr('data-privilige');
        if(privilige == role_id){
            return false;
        }
        $('#privilegePopUp .showWarning').show();
        $('#privilegePopUp .showSucess').hide();
    }

    function setPrivilege_final(){
        var email = $('#privilegePopUp').attr('data-userEmail');
        var privilige = $('#privilegePopUp').attr('data-privilige');
        var role_id = $('#privilegePopUp input[name="privilige"]:checked').val();
        $('#priviligeLoader').fadeIn('fast');
        $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionHome/setUserPrivilege',
          type: "POST",
          data: {'email':email,'role_id':role_id},
          success: function(data) {
            console.log(data);
            var responseArray = JSON.parse(data);
            $('#priviligeLoader').fadeOut('fast');
            // $('#privilegePopUp').modal('toggle');
            // $("#customBox .loaderImg").hide();
            if(responseArray['status'] == 1){
                $('#privilegePopUp .showWarning').hide();
                $('#privilegePopUp .showSucess').show();
                $('#privilegePopUp').attr('data-privilige',role_id);
                // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // // $('#customModalBody').html('Please wait while Requesting qr Code');
                // $('#customModalBody').html('User privilege changed successfully');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
            }else{
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html(responseArray['message']);
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $("#customBox .loaderImg").hide();
            }
          },
            error: function( result ){
                $('#priviligeLoader').fadeOut('fast');
                $("#customBox .loaderImg").hide();
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $("#customBox .loaderImg").hide();
            }
        });
    }

    function reload(){
        var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
        var limit = $('#Data_Count').val();
        var $search_text = $input.val().trim();
        getUserList(page,limit,$search_text);
    }

    function changeStatus(baseLocationId, status){
      alert(baseLocationId);
      alert(status);
    $("#mainLoader.loader").fadeIn("fast");
    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/changeBaseLocationStatus',
        type: 'POST',
        data: {'baseLocationId':baseLocationId, 'status': status},
        success: function( result ){
            $("#mainLoader.loader").fadeOut("fast");
            $("#customBox .loaderImg").hide();

            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                $('#customModalBody').html(responseArray['message']);
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

            }else{
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $(".loading_overlay1").hide();
            }
        },
        error: function( result ){
            $("#mainLoader.loader").fadeOut("fast");
            $("#customBox .loaderImg").hide();
             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Error');
             $('#customModalBody').html('An error has occurred. Please try again.');
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');

             $(".loading_overlay1").hide();

        }
    });
}

function addBaseLocation(){
    var baseLocation = $("#baseLocation").val().trim();
    if(baseLocation == '' || baseLocation == null){
        $("#inputBaseLocation").addClass('has-danger');
        $("#emptyBaseLocation").show();
    }else{
        $('#adduserLoader').fadeIn('fast');
        $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/addBaseLocation',
        type: 'POST',
        data: { value: baseLocation },
        success: function( result ){
            $('#adduserLoader').fadeOut('fast');
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                // $.toast({
                //     heading: 'New User Adding',
                //     text: 'Please wait while a new user added.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                var buttonHtml = '<button type="button"  onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                // $('#customModalBody').html('Please wait while a new user added');
                $('#customModalBody').html('Base Location added successfully');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $("#customBox .loaderImg").hide();

                $('#exampleModal').modal('toggle');
            }else{
                $("#inputBaseLocation").addClass('has-danger');
                $("#alreadyBaseLocation").show();
                $("#alreadyBaseLocation").html('* '+responseArray['message']+'.' );
                $("#emptyBaseLocation").hide();

            }
        },
        error: function( result ){
            $('#adduserLoader').fadeOut('fast');
            var responseArray = JSON.parse(result);
                $("#inputBaseLocation").addClass('has-danger');
                $("#alreadyBaseLocation").show();
                $("#alreadyBaseLocation").html('* '+responseArray['message']+'.' );
                $("#emptyEmail").hide();
                $("#emptyBaseLocation").hide();

        }
    });
    }
}
</script>
