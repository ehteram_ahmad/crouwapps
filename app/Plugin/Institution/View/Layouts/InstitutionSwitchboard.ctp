<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <meta http-equiv="refresh" content="3600;url=<?php echo BASE_URL . 'institution/InstitutionIndex/logout'?>" /> -->
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASE_URL; ?>institution/img/favicon.ico">
    <title>MB Admin</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <?php
        echo $this->Html->css(
            array(
                'Institution.bootstrap.min',
                'Institution.bootstrap-datetimepicker2',
                'Institution.bootstrap-fonts',
                'Institution.jquery.toast',
                'Institution.dropify.min',
                'Institution.style',
                'Institution.default'
            )
        );

        echo $this->Html->script(
            array(
                'ocrNotification',
                'Institution.jquery.min',
                'Institution.tether.min',
                'Institution.bootstrap.min',
                'Institution.jquery.slimscroll',
                'Institution.md5',
                'Institution.waves',
                'Institution.sidebarmenu',
                'Institution.sticky-kit.min',
                'Institution.custom.min',
                'Institution.jquery.toast',
                'Institution.toastr',
                'Institution.dropify.min',
                'Institution.moment',
                'Institution.bootstrap-datetimepicker',
                'Institution.jQuery.style.switcher'
            )
        );
    ?>
</head>

<body style="overflow-anchor:none" class="fix-header card-no-border switchboard-page">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <!-- <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div> -->
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo BASE_URL.'institution/InstitutionHome/index'; ?>">
                        <!-- Logo icon -->
                        <b class="iconLogo">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <?php echo $this->Html->image('Institution.logo-icon.svg',array('class'=>'dark-logo')); ?>
                            <!-- Light Logo icon -->
                            <?php echo $this->Html->image('Institution.logo-icon.svg',array('class'=>'light-logo')); ?>
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="iconTxt">
                         <!-- dark Logo text -->
                         <?php echo $this->Html->image('Institution.logo-text.svg',array('class'=>'dark-logo')); ?>
                         <!-- Light Logo text -->
                         <?php echo $this->Html->image('Institution.logo-text.svg',array('class'=>'light-logo')); ?>
                         </span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  hidden-md-up-->
                        <li class="nav-item"> <a class="nav-link nav-toggler menu-icon text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item">
                          <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)">
                            <!-- <i class="fa fa-align-right" aria-hidden="true"></i> -->
                            <i class="demo-icon icon-sub-menu"></i>
                          </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <?php /* <li class="nav-item dropdown" id="notifContent">
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <!-- <i class="mdi mdi-bell-outline floatLeft"></i> -->
                                <i class="demo-icon icon-sub-notification"></i>
                                <?php
                                    if(isset($count) && $count > 0){
                                ?>
                                    <span class="badge badge-successNew2"> <?php echo $count; ?> </span>
                                <?php
                                    }
                                ?>
                            </a>
                            <div class="dropdown-menu mailbox animated bounceInDown">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                        <?php
                                            if(isset($count) && $count > 0){
                                        ?>
                                        <div class="drop-title allSelect">
                                            <span class="newInput">
                                                <a id="selectAllNotif">All</a>
                                            </span>
                                            <div class="actionBtN floatRight">
                                                <button disabled="disabled" id="assignAllNotif" type="button" class="btn btn-outline-primary">Approve Selected</button>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <?php
                                                if(isset($user) && count($user) > 0){
                                                foreach( $user as $users){
                                            ?>
                                            <!-- Message -->
                                            <a>
                                                <span class="newInputSmall">
                                                    <input class="assignCheck" data-user="<?php echo $users['id']; ?>" data-email="<?php echo $users['email']; ?>" aria-label="Checkbox for following text input" type="checkbox">
                                                </span>
                                                <div class="user-img">
                                                    <span class="round">
                                                        <?php
                                                        if($users['profile_img'] == ''){
                                                            echo substr($users['UserName'],0,1);
                                                        }else{
                                                            echo $this->Html->image($users['profile_img'],array('width'=>'50','height'=>'50'));
                                                        }
                                                        ?>
                                                    </span>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5 style="word-break: break-all;"><?php echo $users['UserName']; ?></h5>
                                                    <span class="mail-desc"><?php echo $users['email']; ?></span>
                                                    <span class="time"><?php echo $users['profession_type']; ?></span>
                                                </div>
                                                <div class="actionBtN">
                                                    <button type="button" onclick="assignUser('<?php echo $users['email']; ?>', '<?php echo $users['id']; ?>',1);" class="btn btn-outline-primary">Approve</button>
                                                </div>
                                            </a>
                                            <?php
                                                }}else{
                                            ?>
                                            <div class="mail-contnet txtCenter">
                                                <?php echo $this->Html->image('Institution.notificationNoRecordFound.png',array('class'=>'verticalCenter')); ?>
                                                <span class="txtCenter txtSmallNofication leftFloatFull">No New Notifications!</span>
                                            </div>
                                            <?php
                                                }
                                            ?>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="<?php echo BASE_URL; ?>/institution/InstitutionHome/index/0"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li> */ ?>

                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="logowestSuffolk" href="#">
                                <?php echo $this->Html->image('Institution.trustlogo/logo_'.$_COOKIE['adminInstituteId'].'.png'); ?>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <div class="dropdown-menu  dropdown-menu-right animated bounceInDown">
                                <span class="dropdown-item"><?php echo $_COOKIE['adminName']; ?></span>
                                <span class="dropdown-item">
                                  <?php echo $_COOKIE['adminEmail'];
                                  echo $_COOKIE['adminRoleId']=='1'?' (Subamdin)':$_COOKIE['adminRoleId']=='3'?' (Switchboard)':' (Admin)'; ?></span>
                                <a class="dropdown-item" href="<?php echo BASE_URL . 'institution/Switchboard/logout'?>"><i class="fa fa-power-off"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <?php echo $this->element('left_menu_switchboard'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <?php echo $this->fetch('content'); ?>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                All Rights Reserved by MedicBleep.com ( Version 1.2.1 )
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!--                        Custom popup [START]                    -->
    <!-- ============================================================== -->
    <style type="text/css" media="screen">
        #customBox .loaderImg{
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: 50% 50% no-repeat rgba(79,74,74,0.459);
        }
        #customBox {
            z-index: 10000 !important;
        }
    </style>
    <div class="modal fade" id="customBox" tabindex="-1" role="dialog" aria-labelledby="customModalLabel">
        <div class="modal-dialog" role="custom">
            <div class="modal-content" id="">
            <div class="loaderImg" style="display:none;"><?php echo $this->Html->image('Institution.page-loader.gif', array("class"=> "loading-image" )); ?></div>
                <div class="modal-header">
                    <h4 class="modal-title" id="customModalLabel">Custom Popup</h4>
                </div>
                <div class="modal-body" id="customModalBody">
                </div>
                <div class="modal-footer" id="customModalFoot">
                   <button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!--                        Custom popup [END]                      -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script>
    $(document).ready(function() {
        $('#customBox').modal({
            backdrop: 'static',
            keyboard: false,
            show: false
        });
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        });
        $(".dropify-clear").click(function(){
            $("#userEmail").attr("disabled", false);
            $("#batonRoleText").attr("disabled", false);
            $batonInputCheck.attr("disabled", false);
            $(".onCallBtMain.txtCenter").hide();
            $("#uploadResult").hide();
            $("#uploadSuccess").hide();
            $("#uploadError").hide();
        });
    });
    </script>
    <script type="text/javascript">
    console.log("<?php echo date_default_timezone_get(); ?>");

        $('#selectAllNotif').on('click',function(){
            if($('.assignCheck[type="checkbox"]:checked').length == $('.assignCheck[type="checkbox"]').length){
                $('.assignCheck[type="checkbox"]').prop('checked', false);
                $('#assignAllNotif').attr('disabled',true);
            }else{
                $('.assignCheck[type="checkbox"]').prop('checked', true);
                $('#assignAllNotif').removeAttr('disabled');
            }
        });
        $('.assignCheck[type="checkbox"]').on('click',function(){
            if($('.assignCheck[type="checkbox"]:checked').length > 0){
                $('#assignAllNotif').removeAttr('disabled');
            }else{
                $('#assignAllNotif').attr('disabled',true);
            }
        });
        $('#assignAllNotif').on('click',function(){
            $(".loader").fadeIn("fast");
            var status = 1;
            var $data = [];
            $('.assignCheck[type="checkbox"]:checked').each(function(){
                var details = {};
                var userId = $(this).attr('data-user');
                var email = $(this).attr('data-email');
                details.user_id = userId;
                details.email = email;
                details.status = status;
                $data.push(details);
            });

            $.ajax({
                url: '<?php echo BASE_URL; ?>institution/InstitutionHome/changeBulkAssignStatus',
                type: 'POST',
                data: {'user_data' : JSON.stringify($data)},
                success: function( result ){
                    $(".loader").fadeOut("fast");
                    $("#customBox .loaderImg").hide();
                    console.log(result);
                    var responseArray = JSON.parse(result);
                    if( responseArray['status'] == 1 ){
                        var buttonHtml = '<button type="button" onClick="location.reload()" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Confirmation');
                        $('#customModalBody').html(responseArray['message']);
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');

                        // $.toast({
                        //     heading: 'Success',
                        //     text: responseArray['message'],
                        //     position: 'top-right',
                        //     loaderBg:'#2c2b2e',
                        //     icon: 'success',
                        //     hideAfter: 3000,
                        //     stack: 6
                        // });
                        // location.reload();
                    }else{
                        // $.toast({
                        //     heading: 'Error',
                        //     text: 'An error has occurred. Please try again.',
                        //     position: 'top-right',
                        //     loaderBg:'#2c2b2e',
                        //     icon: 'error',
                        //     hideAfter: 3000,
                        //     stack: 6
                        //   });
                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Error');
                        $('#customModalBody').html('An error has occurred. Please try again.');
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');

                        $(".loading_overlay1").hide();
                    }
                },
                error: function( result ){
                    $("#customBox .loaderImg").hide();
                     // $.toast({
                     //        heading: 'Error',
                     //        text: 'An error has occurred. Please try again.',
                     //        position: 'top-right',
                     //        loaderBg:'#2c2b2e',
                     //        icon: 'error',
                     //        hideAfter: 3000,
                     //        stack: 6
                     //      });
                     var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                     $('#customModalLabel').html('Error');
                     $('#customModalBody').html('An error has occurred. Please try again.');
                     $('#customModalFoot').html(buttonHtml);
                     $('#customBox').modal('show');

                     $(".loading_overlay1").hide();

                }
            });
        });

        function assignUser(email, userId, status){
            $(".loader").fadeIn("fast");
            $.ajax({
                url: '<?php echo BASE_URL; ?>institution/InstitutionHome/changeAssignStatus',
                type: 'POST',
                data: {'email':email, 'user_id': userId, 'status': status},
                success: function( result ){
                    $(".loader").fadeOut("fast");
                    $("#customBox .loaderImg").hide();
                    var responseArray = JSON.parse(result);
                    if( responseArray['status'] == 1 ){
                        // $.toast({
                        //     heading: 'Success',
                        //     text: responseArray['message'],
                        //     position: 'top-right',
                        //     loaderBg:'#2c2b2e',
                        //     icon: 'success',
                        //     hideAfter: 3000,
                        //     stack: 6
                        // });
                        // location.reload();
                        var buttonHtml = '<button type="button" onClick="location.reload()" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Confirmation');
                        $('#customModalBody').html(responseArray['message']);
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');
                    }else{
                        // $.toast({
                        //     heading: 'Error',
                        //     text: 'An error has occurred. Please try again.',
                        //     position: 'top-right',
                        //     loaderBg:'#2c2b2e',
                        //     icon: 'error',
                        //     hideAfter: 3000,
                        //     stack: 6
                        //   });
                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Error');
                        $('#customModalBody').html('An error has occurred. Please try again.');
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');

                        $(".loading_overlay1").hide();
                    }
                },
                error: function( result ){
                    $("#customBox .loaderImg").hide();
                     // $.toast({
                     //        heading: 'Error',
                     //        text: 'An error has occurred. Please try again.',
                     //        position: 'top-right',
                     //        loaderBg:'#2c2b2e',
                     //        icon: 'error',
                     //        hideAfter: 3000,
                     //        stack: 6
                     //      });
                     var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                     $('#customModalLabel').html('Error');
                     $('#customModalBody').html('An error has occurred. Please try again.');
                     $('#customModalFoot').html(buttonHtml);
                     $('#customBox').modal('show');

                     $(".loading_overlay1").hide();

                }
            });
        }

        setInterval(function(){
            checkUser();
        },20000);

        $('body').on('mousemove',function(){
          var num = 8*60;
          var ca = document.cookie.split(';');
          for(var i=0;i < ca.length;i++) {
              var c = ca[i];
              var date = new Date();
              date.setTime(date.getTime()+(num * 60 * 1000));
              var expires = "; expires="+date.toGMTString();
              var name = c.split('=');
              document.cookie = name[0]+"="+name[1]+expires+"; path=/";
          }
          return null;
        });

        function checkUser(){
            var url = "'<?php echo BASE_URL . 'institution/InstitutionIndex/logout'?>'";
            $.ajax({
                url: '<?php echo BASE_URL; ?>institution/InstitutionHome/getUserPresence',
                type: 'POST',
                success: function( result ){
                    console.log(result);
                    var responseArray = JSON.parse(result);
                    if(responseArray['status'] == 0){
                        $("#customBox .loaderImg").hide();
                        var buttonHtml = '<button type="button" onclick="window.location.href='+url+'" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('<img src="<?php echo BASE_URL ?>institution/img/sub-error.svg" alt=""><span class="msgError">Error!</span>');
                        $('#customModalBody').removeClass('bodyCustom').html(responseArray['message']);
                        $('#customModalFoot').css('text-align','center').html(buttonHtml);
                        $(".loading_overlay1").hide();
                        if(!$('#customBox').hasClass('show')){
                            $('#customBox').modal('show');
                        }
                    }else{
                      // bataon_request_count = responseArray['baton_request_count'];
                      // if(bataon_request_count > 0){
                      //   $('.batonBadgeCount').show();
                      // }else{
                      //   $('.batonBadgeCount').hide();
                      // }
                      // if($('#baton_request_count_latest').html() < bataon_request_count){

                      //   var title = 'Baton Role Requests';
                      //   var body = 'There is a new Baton Role request.';
                      //   var options = {
                      //           body: body,
                      //           onClick: function() {
                      //             window.focus();
                      //             var link_url = "<?php echo BASE_URL.'institution/Switchboard/getRequest'; ?>";
                      //             location.href = link_url;
                      //           },
                      //           timeout: 7,
                      //           closeOnClick: true
                      //         };
                      //   var notify = new OCRNotification(title, options);
                      //   var audio = new Audio('<?php echo BASE_URL; ?>institution/new_message.mp3');

                      //   notify.show();
                      //   audio.play();
                      // }
                      // $('#baton_request_count_latest').html(bataon_request_count);
                    }
                }
            });
        }

        function notifications(){
            // ajaxContent
            $.ajax({
                url: '<?php echo BASE_URL; ?>institution/InstitutionHome/getNotificationCount',
                type: 'POST',
                success: function( result ){
                    $('#notifContent').html(result);
                }
            });
        }

    </script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <?php //echo $this->element('sql_dump'); ?>
</body>

</html>
