<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASE_URL; ?>institution/img/favicon.ico">
    <title>MB Admin</title>
    <?php
        echo $this->Html->css(
            array(
                'Institution.bootstrap.min',
                'Institution.style',
                'Institution.default'
            )
        );
    ?>
    <?php
        echo $this->Html->script(
            array(
                'Institution.jquery.min',
                'Institution.tether.min',
                'Institution.bootstrap.min',
                'Institution.jquery.slimscroll',
                'Institution.waves',
                'Institution.sidebarmenu',
                'Institution.sticky-kit.min',
                'Institution.custom.min',
                'Institution.jQuery.style.switcher'
            )
        );
    ?>
    <!-- <link href="css/colors/default.css" id="theme" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <!-- <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div> -->
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register">
            <div class="login-brand">
                <a class="login-brand-link">
                    <?php echo $this->Html->image('Institution.logomedic.svg'); ?>
                </a>
            </div>
            <div class="login-box card">
            <div class="card-block">
                <?php echo $this->Form->create('login',array('id'=>'loginform',"autocorrect"=>"off","autocapitalize"=>"off","autocomplete"=>"off",'onsubmit'=>"return login();")); ?>
                <!-- <form class="form-horizontal form-material" id="loginform" action="index.html"> -->
                    <h3 class="box-title m-b-20">Sign In</h3>
                    <div class="form-group email-form">
                        <div class="col-xs-12">
                            <?php echo $this->Form->text('userName',array('class'=>'form-control',"autocorrect"=>"off","autocapitalize"=>"off","autocomplete"=>"off",'placeholder'=>'Username','id'=>'login_email')); ?>
                        </div>
                        <div class="form-control-feedback" id="email_error" style="display: none;">
                        </div>
                    </div>
                    <div class="form-group password-form">
                        <div class="col-xs-12">
                            <?php echo $this->Form->text('password',array('class'=>'form-control','placeholder'=>'Password','id'=>'login_password')); ?>
                        </div>
                        <div class="form-control-feedback" id="password_error" style="display: none;">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" action="index.html">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" autocorrect="off" autocapitalize="off" autocomplete="off" required="" placeholder="Email"> </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>

    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
</body>
<script type="text/javascript">

    // Restrict to go back [START]---------------------------------------
    var logedIn = "<?php echo isset($_COOKIE['adminInstituteId']) ? 1 : 0;?>";
    if(logedIn == 0){
      (function (global) {

          if(typeof (global) === "undefined") {
              throw new Error("window is undefined");
          }

          var _hash = "!";
          var noBackPlease = function () {
              global.location.href += "#";

              global.setTimeout(function () {
                  global.location.href += "!";
              }, 50);
          };

          global.onhashchange = function () {
              if (global.location.hash !== _hash) {
                  global.location.hash = _hash;
              }
          };

          global.onload = function () {
              noBackPlease();

              document.body.onkeydown = function (e) {
                  var elm = e.target.nodeName.toLowerCase();
                  if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                      e.preventDefault();
                  }
                  e.stopPropagation();
              };
          }

      })(window);
    }else{
      location.reload();
    }

    // Restrict to go back [END]---------------------------------------

    function login(){

        var userName = $("#login_email").val().trim();
        var password = $("#login_password").val().trim();

        $('#email_error').hide();
        $('#password_error').hide();
        $('.email-form').removeClass('has-danger');
        $('.password-form').removeClass('has-danger');

        if(userName ==''){
            $('.email-form').addClass('has-danger');
            $('#email_error').show().html('Please enter userName.');
            $("#login_email").focus();
            return false;
        }

        if(password ==''){
            $('.password-form').addClass('has-danger');
            $('#password_error').show().html('Please enter password.');
            $("#login_password").focus();
            return false;
        }
        $.ajax({
            url: "<?php echo BASE_URL; ?>institution/InstitutionIndex/login",
            type: 'POST',
            // async: false,
            data: { userName: userName, password: password },
            success: function( result ){
                result = JSON.parse(result);
                if(result['status']==1){
                    location.href = "<?php echo BASE_URL; ?>institution/InstitutionHome/index";
                }else{
                    $('.email-form').addClass('has-danger');
                    $('.password-form').addClass('has-danger');
                    $('#password_error').show().html(result['message']);
                }

            },
            error: function( result ){
                $('#loginMsg').html( result );

            }
        });
        return false;
    }
</script>

</html>
