<?php
// print_r($data);
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader" style="display: none;"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
      <div class="col-md-12 align-self-center col-5">
        <h3 class="text-themecolor m-b-0 m-t-0 col-1 window_history"><img src="<?php echo BASE_URL; ?>institution/img/left-arrow.svg" /></h3>
        <h3 class="text-themecolor m-b-0 m-t-0 col-11" style="float: left;"><?php echo isset($dialogue_name) ? ucwords(strtolower($dialogue_name)) : 'Group'; ?>'s Chat</h3>
      </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionGroup/index">Group chats</a></li>
          <li class="breadcrumb-item"><a href="<?php //echo BASE_URL.'institution/InstitutionGroup/exportForm/'.$dialog_id; ?>">Export Chat</a></li>
          <li class="breadcrumb-item active">Export Chat PDF</li> -->
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row boaderLines widgetMain">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div data-value="<?php echo (isset($data['message_data']) && count($data['message_data']) > 0)?count(isset($data['message_data'])):0; ?>" id="chatHistoryExtraDetails">
                      <p>Hi <?php echo $title; ?></p>
                      <p>Please find below the chat history you requested.</p>
                      <p>Medic Bleep</p>
                      <br>
                      <p>Chat History</p>
                      <p><?php echo date('d-m-Y', $from_date)." to ".date('d-m-Y', $to_date);  ?></p>
                      <br>
                      <p>Chat Subject</p>
                      <p><?php echo $dialogue_name; ?></p>
                      <br>
                      <p>Trust Name</p>
                      <p><?php echo $data['current_company']; ?></p>
                      <br>
                      <?php
                        if(!empty($data['patient_data'])){
                      ?>
                      <p>Patient Detail:</p>
                      <p>Name: <?php echo $data['patient_data']['name'] ?></p>
                      <?php
                        if($data['patient_data']['patient_number_type'] == '0'){
                          ?>
                            <p>NHS Number: <?php echo $data['patient_data']['nhs_number'] ?></p>
                          <?php
                        }else{
                          ?>
                            <p>MRN Number: <?php echo $data['patient_data']['nhs_number'] ?></p>
                          <?php
                        }
                      ?>
                      <?php
                        if($data['patient_data']['patient_dob'] !=''){
                          ?>
                            <p>Born: <?php echo $data['patient_data']['patient_dob'] ?></p>
                          <?php
                        }else{
                          ?>
                            <p>Age: <?php echo $data['patient_data']['age'] ?></p>
                          <?php
                        }
                      ?>
                      <p>Gender: <?php echo $data['patient_data']['gender'] ?></p>
                      <?php
                          if ($data['patient_data']['notes'] !='' && $data['patient_data']['notes'] != 'F5kZgOvvBVW2cIFKUiPEeQ==') { ?>
                            <p>Notes: <?php echo $data['patient_data']['notes'] ?></p>
                      <?php
                          }
                        }
                      ?>
                      <br>
                      <p>Chat Participants:</p>
                      <?php
                          $count = 1;
                          foreach ($data['occupants_data'] as $participants) {
                              echo "<p>".$count.". ".$participants['first_name']." ".$participants['last_name'].", ".$participants['role_status']."</p>";
                              $count++;
                          }
                      ?>
                    </div>
                    <p></p>
                    <p></p>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <form>
                               <table class="exportTable wordBreak" style="border-collapse:collapse;margin-bottom: 15.504pt" cellspacing="0">
                                  <tbody>
                                    <?php
                                      if(isset($data['message_data']) && count($data['message_data']) > 0){
                                        $date_color = '#10A4DA';
                                        $colour1 = '#C6EAFA';
                                        $colour2 = '#EEEEEF';
                                        $showDate = 1;
                                        $row_color = $colour1;
                                        for ($i=0; $i < count($data['message_data']); $i++) {
                                          if($i > 0){

                                            // condition for row color
                                            if($data['message_data'][$i-1]['id'] != $data['message_data'][$i]['id']){
                                              if($row_color == $colour1){
                                                $row_color = $colour2;
                                              }else{
                                                $row_color = $colour1;
                                              }
                                            }

                                            // condition to show date
                                            if($data['message_data'][$i-1]['created_date'] != $data['message_data'][$i]['created_date']){
                                              $showDate = 1;
                                            }else{
                                              $showDate = 0;
                                            }
                                          }

                                          if($showDate == 1){ ?>
                                            <tr style="height:24pt">
                                               <td style="width:579pt;border-bottom-style:solid;border-bottom-width:2pt;border-bottom-color:#FFFFFF" colspan="3" bgcolor="#10A4DA">
                                                  <p class="s2" style="padding-top: 5pt;padding-left: 5pt;text-indent: 0pt;text-align: left;">Date: <?php echo $data['message_data'][$i]['created_date']; ?></p>
                                               </td>
                                            </tr>
                                    <?php }
                                    ?>
                                            <tr style="height:24pt">
                                               <td style="width:101pt;border-top-style:solid;border-top-width:2pt;border-top-color:#FFFFFF;border-bottom-style:solid;border-bottom-width:2pt;border-bottom-color:#FFFFFF;border-right-style:solid;border-right-width:2pt;border-right-color:#FFFFFF" bgcolor="<?php echo $row_color; ?>">
                                                  <p class="s3" style="padding-top: 5pt;padding-left: 5pt;text-indent: 0pt;text-align: left;"><?php echo $data['message_data'][$i]['sender_name']; ?></p>
                                               </td>
                                               <td style="width:377pt;border-top-style:solid;border-top-width:2pt;border-top-color:#FFFFFF;border-left-style:solid;border-left-width:2pt;border-left-color:#FFFFFF;border-bottom-style:solid;border-bottom-width:2pt;border-bottom-color:#FFFFFF;border-right-style:solid;border-right-width:2pt;border-right-color:#FFFFFF" bgcolor="<?php echo $row_color; ?>">
                                                  <p class="s3" style="padding-top: 5pt;padding-left: 4pt;text-indent: 0pt;text-align: left;"><?php echo $data['message_data'][$i]['messages']; ?></p>
                                               </td>
                                               <td style="width:101pt;border-top-style:solid;border-top-width:2pt;border-top-color:#FFFFFF;border-left-style:solid;border-left-width:2pt;border-left-color:#FFFFFF;border-bottom-style:solid;border-bottom-width:2pt;border-bottom-color:#FFFFFF" bgcolor="<?php echo $row_color; ?>">
                                                  <p class="s3" style="padding-top: 5pt;padding-left: 4pt;text-indent: 0pt;text-align: left;"><?php echo $data['message_data'][$i]['timeStamp']; ?></p>
                                               </td>
                                            </tr>
                                    <?php
                                      }}
                                    ?>
                                  </tbody>
                               </table>
                               <div class="form-group">
                                  <div class="col-12 pad0">
                                     <div id="dialogListShow" onclick="exportChat();" class="btn btn-primary">Export Chat</div>
                                     <div id="showExportStatus" style="display:none;"></div>
                                  </div>
                               </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
$("#sidebarnav>li#menu2").addClass('active').find('a').eq(0).addClass('active');
$("#sidebarnav>li#menu2").find("li").eq(1).addClass('active').find('a').addClass('active');
  $(document).ready(function(){
    if($('#chatHistoryExtraDetails').attr('data-value') == 0){
      // alert('No chat to export in this date range.');
      $dialog_id = <?php echo "'".$dialog_id."'";  ?>;
      $("#customBox .loaderImg").hide();
      $(".loader").fadeIn("fast");
      // var href = "<?php //echo BASE_URL; ?>institution/InstitutionGroup/exportForm/"+$dialog_id;
      var buttonHtml = '<button type="button" onclick="window.history.back();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
      $('#customModalLabel').html('Confirmation');
      $('#customModalBody').html('No chat to export in this date range.');
      $('#customModalFoot').html(buttonHtml);
      $('#customBox').modal('show');

      // window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionGroup/exportForm/" + $dialog_id;
    }
  });
  function exportChat(){
      var userId = <?php echo $user_id;  ?>;
      var chatExport = <?php echo json_encode(serialize($data));  ?>;
      var dialogueId = <?php echo "'".$dialog_id."'";  ?>;
      var fromDate = <?php echo $from_date;  ?>;
      var toDate = <?php echo $to_date;  ?>;
      var transaction_id = <?php echo $transaction_id;  ?>;

      $("#showExportStatus").show();
      $("#showExportStatus").html('Please wait...');
      $("#showExportStatus").css( "float", "right");
      $("#showExportStatus").css( "margin-right", "750px");
      $("#showExportStatus").css( "margin-top", "5px");
      $('.loader').fadeIn('fast');
      $.ajax({
          url: '<?php echo BASE_URL; ?>institution/InstitutionUser/chatExport',
          type: 'POST',
          data: {'dialogue_id':dialogueId, 'user_id':userId, 'from_date':fromDate, 'to_date':toDate,'chat_data':chatExport,'transaction_id':transaction_id},
          success: function( result ){
            $('.loader').fadeOut('fast');
            $("#customBox .loaderImg").hide();
              var responseArray = JSON.parse(result);
              if( responseArray['status'] == 1 ){
                  // $.toast({
                  //         heading: 'Success',
                  //         text: 'Chat exported successfully',
                  //         position: 'top-right',
                  //         loaderBg:'#2c2b2e',
                  //         icon: 'success',
                  //         hideAfter: 3000,
                  //         stack: 6
                  //       });
                  var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                  $('#customModalLabel').html('Confirmation');
                  $('#customModalBody').html('Chat exported successfully.<br />Please check your registered email to get pdf.');
                  $('#customModalFoot').html(buttonHtml);
                  $('#customBox').modal('show');

                  $("#showExportStatus").show();
                  $("#showExportStatus").html('Please check your registered email to get pdf');
                  $("#showExportStatus" ).css( "float", "right");
                  $("#showExportStatus" ).css( "margin-right", "520px");
                  $("#showExportStatus" ).css( "margin-top", "5px");
                  setTimeout(function() {
                      $('#showExportStatus').fadeOut('fast');
                  }, 5000);
                  return false;
              }else{
                  // $.toast({
                  //     heading: 'Error',
                  //     text: 'An error has occurred. Please try again.',
                  //     position: 'top-right',
                  //     loaderBg:'#2c2b2e',
                  //     icon: 'error',
                  //     hideAfter: 3000,
                  //     stack: 6
                  //   });
                  var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                  $('#customModalLabel').html('Error');
                  $('#customModalBody').html('An error has occurred. Please try again.');
                  $('#customModalFoot').html(buttonHtml);
                  $('#customBox').modal('show');
                  $('#showExportStatus').fadeOut('fast');
              }
          },
          error: function( result ){
            $('.loader').fadeOut('fast');
            $('#showExportStatus').fadeOut('fast');
            $("#customBox .loaderImg").hide();
               // $.toast({
               //        heading: 'Error',
               //        text: 'An error has occurred. Please try again.',
               //        position: 'top-right',
               //        loaderBg:'#2c2b2e',
               //        icon: 'error',
               //        hideAfter: 3000,
               //        stack: 6
               //      });
               var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
               $('#customModalLabel').html('Error');
               $('#customModalBody').html('An error has occurred. Please try again.');
               $('#customModalFoot').html(buttonHtml);
               $('#customBox').modal('show');
               $(".loading_overlay1").hide();

          }
      });
  }
</script>
