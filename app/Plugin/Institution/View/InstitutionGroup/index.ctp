<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div id="mainLoader" class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

.is-hidden{
  display: none !important;
}

</style>
<div class="container-fluid">
  <div class="row page-titles">
      <div class="col-md-3 align-self-center col-5">
          <h3 class="text-themecolor m-b-0 m-t-0" style="color:#52677a;">Manage: <span style="color:#10a5da;">Groups</span></h3>
      </div>
      <div class="buttonTop col-md-9 col-7">
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- Start HTML Content -->
  <!-- ============================================================== -->
  <div class="row boaderLines marTop20">
     <div class="col-12">
        <div class="card">
           <div class="card-block padLeftRight0 pad0">
              <div class="floatLeftFull padLeftRight14 groupCustom newListStyle">
                 <!-- <div class="tableTop floatLeft marTop10"> -->
                   <div class="tableTop floatLeft">
                     <div id="list-filter-options">
                       <input type="radio" name="filter_option" value="all" id="filter_option_all" checked="checked">
                       <label for="filter_option_all">All (<span id="showTotCount">0</span>)</label>
                       <input type="radio" name="filter_option" value="0" id="filter_option_NG">
                       <label for="filter_option_NG">Normal Groups (<span id="NGCount">0</span>)</label>
                       <input type="radio" name="filter_option" value="1" id="filter_option_PG">
                       <label for="filter_option_PG">Patient Groups (<span id="PGCount">0</span>)</label>
                     </div>
                   </div>
                    <!-- <span class="card-title">Group: <small>Export Chat / Broadcast Messages</small></span> -->
                 <!-- </div> -->
                 <div id="userlisting_filter" class="dataTables_filter">
                    <label><i class="demo-icon icon-sub-lense"></i></label>
                    <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                 </div>
              </div>
              <input class="is-hidden" type="text" id="type_val" name="" value="">
              <div class="table-responsive wordBreak" id="ajax_dialogs">
                 <table class="table stylish-table customPading">
                    <thead>
                       <tr>
                          <th>Photo </th>
                          <th>Group Name</th>
                          <th>Last Message </th>
                          <th>Date</th>
                          <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <td colspan="8" align="center">
                              <font color="green">Please wait while we are loading the list...</font>
                          </td>
                      </tr>
                    </tbody>
                 </table>
              </div>
           </div>
        </div>
     </div>
  </div>
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->

</div>
<div class="modal fade" id="exampleModal2" tabindex="2" role="dialog" aria-labelledby="exampleModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel2">Broadcast Message</h4>
                <p>Write a message to broadcast in group.</p>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
            </div>
            <div class="modal-body">
               <!-- <p>Message:</p> -->
                <form>
                    <div id="inputMessage" class="form-group">
                        <textarea class="form-control" id="textMessage" placeholder="Write here…" maxlength="500"></textarea>
                        <p class="txtLeft" id="counter"></p>
                        <div class="form-control-feedback" id="emptyMessage" style="display:none">Please Enter Message</div>
                        <div class="form-control-feedback" id="processingMessage" style="display:none;font-size:12px;color:green;font-style:italic">Please wait while broadcasting messege...</div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <div class="row showWarning" style="display:block">
                <div class="col-md-6 pull-left">
                  <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                  <p>Are you sure you want to broadcast this message to all users within group?</p>
                </div>
                <div class="col-md-6 txtRight pull-right">
                  <button type="button" class="btn btn-success waves-effect" id="preRequestBtn1">Yes</button>
                  <button type="button" id="postRequestBtn1" style="display: none;" class="btn btn-success waves-effect" data-toggle="modal" data-target="#exampleModal3"></button>
                  <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                </div>
              </div>
              <div class="row showSucess" style="display:none">
                  <div class="col-md-8 pull-left">
                    <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                    <p>Message Brodacasted successfully to all group members.</p>
                  </div>
                  <div class="col-md-4 txtRight pull-right">
                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                  </div>
              </div>
                <!-- <button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>
                <button type="button" id="preRequestBtn1" class="btn btn-success waves-effect">Send Message</button>
                <button type="button" id="postRequestBtn1" style="display: none;" class="btn btn-success waves-effect" data-toggle="modal" data-target="#exampleModal3"></button> -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal3" tabindex="3" role="dialog" aria-labelledby="exampleModalLabel3">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="height:305px;">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon-arrow-left-circle"></i>
                </button> -->
                <h4 class="modal-title" id="exampleModalLabel3">Broadcast Message</h4>
            </div>
            <div class="modal-body">
               <p>Please confirm your password:</p>
               <span class="green">* Please use your Medic Bleep password</span>
                <form>
                    <div id="inputPassword" class="form-group marBottom52px">
                         <input type="text" autocorrect="off" autocapitalize="off" autocomplete="off" class="custom_m_Password form-control" id="messagePassword">
                         <div class="form-control-feedback" id="emptyPassword" style="display:none">Please Enter Password</div>
                         <div class="form-control-feedback" id="incorrectPassword" style="display:none">Password is incorrect</div>
                         <div class="form-control-feedback" id="responseMessage" style="display:none"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Back</button>
                <button type="button" id="preConfirmBtn2" class="btn btn-success waves-effect" data-dismiss="modal">Confirm Password</button>
                <button type="button" style="display: none;" id="postConfirmBtn2" class="btn btn-success waves-effect" data-dismiss="modal"></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
      var val = $('#type_val').val();
      if(val == ''){
        val = 'all';
      }
      $('#type_val').val(val);
      showUserDialogues();
    });
    // $("#sidebarnav>li#menu2").addClass('active').find('a').addClass('active');
    $("#sidebarnav>li#menu2").addClass('active').find('a').eq(0).addClass('active');
    $("#sidebarnav>li#menu2").find("li").eq(1).addClass('active').find('a').addClass('active');

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#user_search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(dialogSearch, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function dialogSearch() {
       $search_text = $input.val().trim();
       if($search_text == ''){
           // return false;
       }
       $input.blur();
       $(".loader").fadeIn("fast");
       $('#emptyData').hide();

        $("#ajax_dialogs tr").removeClass('searchHide');
       $("#ajax_dialogs td.dialog_name").each(function () {
           $(this).parents('tr').show();
           if($(this).html().toLowerCase().indexOf($search_text.toLowerCase()) == -1){
               $(this).parents('tr').hide().addClass('searchHide');
           }
       });

       if($('#ajax_dialogs').find("td.dialog_name:visible").length == 0){
        $('#emptyData').show();
       }

       var patGroup = $("#ajax_dialogs tr.patientMain:not('.searchHide')").length;
       var norGroup = $("#ajax_dialogs tr.normalMain:not('.searchHide')").length;
       var total = patGroup + norGroup;

       console.log(patGroup);
       console.log(norGroup);
       console.log(total);
       $('#NGCount').html(parseInt(norGroup));
       $('#PGCount').html(parseInt(patGroup));
       $('#showTotCount').html(parseInt(total));

       $(".loader").fadeOut("fast");
    }

    $('body').on('click','input[name="filter_option"]',function(){
      $('#type_val').val($(this).val());
      var val = $('#type_val').val();
      $('tr.patientMain').removeClass('is-hidden');
      $('tr.normalMain').removeClass('is-hidden');
       $('#emptyData').hide();
      if(val == 'all'){
        $('tr.patientMain').removeClass('is-hidden');
        $('tr.normalMain').removeClass('is-hidden');
      }
      if(val == '0'){
        $('tr.patientMain').addClass('is-hidden');
      }
      if(val == '1'){
        $('tr.normalMain').addClass('is-hidden');
      }
      if($('#ajax_dialogs').find("td.dialog_name:visible").length == 0){
       $('#emptyData').show();
      }
      // dialogSearch();
      // getDashboard('1','20',$input.val().trim(),$(this).val());
    });

    function showUserDialogues(){
        $(".loader").fadeIn("fast");
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionGroup/showDialogues',
            type: "POST",
            success: function(data) {
                $(".loader").fadeOut("fast");
                if(data == ''){
                    $('#ajax_dialogs').html('No chats are available.');
                    $('#exampleInputPassword1').val('');
                }else if( IsJsonString(data) == false ){
                    $('#ajax_dialogs').html(data);
                    $search_text = $input.val().trim();
                    if($search_text != ''){
                        dialogSearch();
                    }else{
                      var patGroup = $("#ajax_dialogs tr.patientMain:not('.searchHide')").length;
                      var norGroup = $("#ajax_dialogs tr.normalMain:not('.searchHide')").length;
                      $('#showTotCount').html(parseInt(patGroup)+parseInt(patGroup));
                      $('#NGCount').html(parseInt(norGroup));
                      $('#PGCount').html(parseInt(patGroup));
                    }

                }else{
                    var responseArray = JSON.parse(data);
                    if(responseArray['status'] == 2){
                        $("#passwordBlock").addClass('has-danger');
                        $("#passwordError").show();
                        $("#emptyPassword").hide();
                        return false;
                    }else{
                        $('#ajax_dialogs').html(responseArray['message']);

                        $("#customBox .loaderImg").hide();
                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Confirmation');
                        $('#customModalBody').html(responseArray['message']);
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');

                        // alert(responseArray['message']);
                    }
                }
            },
            error: function( data ){
                $(".loader").fadeOut("fast");
            }
        });
    }

    function showUserChats($dialogId){
        $(".loader").fadeIn("fast");
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionGroup/exportForm/" + $dialogId;
    }

    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
</script>
<script type="text/javascript">
var text_max = 500;
$('#counter').html(text_max + ' characters remaining');

$('#textMessage').keyup(function() {
  if($('#counter').css('display') == 'block'){
    $('#exampleModal2 .showSucess').hide();
    var text_length = $('#textMessage').val().length;
    var text_remaining = text_max - text_length;

    $('#counter').html(text_remaining + ' characters remaining');
    if(text_max == text_remaining){
      $('#exampleModal2 .showWarning').hide();
    }else{
      $('#exampleModal2 .showWarning').show();
    }
  }
});
$(function () {
  $('#exampleModal2').on('show.bs.modal', function (event) {
    $('#exampleModal2 .showWarning').hide();
    $('#exampleModal2 .showSucess').hide();
    $('#textMessage').attr('readonly',false);
    $('#counter').show();
    $("#preRequestBtn1").css('background-color','#10a5da','border','#10a5da');
  });
});

$('#preRequestBtn1').click(function() {
 $("#processingMessage").html('');
 var message = $('#textMessage').val().trim();
 if(message==""){
     $("#inputMessage").addClass('has-danger');
     $("#emptyMessage").show();
     return false;
 }
 else
 {
     $("#emptyMessage").hide();
     $("#inputPassword").removeClass('has-danger');
     $("#incorrectPassword").hide();
     $("#emptyPassword").hide();
     $("#messagePassword").val('');
     $("#inputMessage").removeClass('has-danger');
 }
 $('#postRequestBtn1').click();
});

$('#preConfirmBtn2').click(function() {
 var dialogId = $('#exampleModal2').attr('data-dialogue');
 var dialogName = $('#exampleModal2').attr('data-dialoguename');
 var message = $('#textMessage').val().trim();
 var password = $('#messagePassword').val().trim();
    if(password == ""){
        $("#inputPassword").addClass('has-danger');
        $("#emptyPassword").show();
        $("#incorrectPassword").hide();
        return false;
    }else{
        $("#processingMessage").show();
        $("#processingMessage").css('color','green').html('Please wait while broadcasting message...');
        $("#preRequestBtn1").attr('disabled','disabled');
        $("#preRequestBtn1").css('background-color','grey','border','grey');
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionGroup/broadCastMessage',
            type: 'POST',
            data: {'password':password,'message':message,'dialogue_id':dialogId,'dialogue_name':dialogName},
            success: function( result ){
                console.log(result);
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                  if($('#preRequestBtn1').attr('disabled')){
                    $('#exampleModal2 #textMessage').attr('readonly',true);
                    $('#exampleModal2 #counter').hide();
                    $('#exampleModal2 .showSucess').show();
                    $('#exampleModal2 .showWarning').hide();
                    // $('#textMessage').val('');
                    $('#counter').html('500 characters remaining');
                    $("#processingMessage").hide();
                  }
                     // $.toast({
                     //    heading: 'Success',
                     //    text: 'Message Sent successfully',
                     //    position: 'top-right',
                     //    loaderBg:'#2c2b2e',
                     //    icon: 'success',
                     //    hideAfter: 3000,
                     //    stack: 6
                     //  });
                     // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                     // $('#customModalLabel').html('Confirmation');
                     // $('#customModalBody').html('Message Sent successfully');
                     // $('#customModalFoot').html(buttonHtml);
                     // $('#customBox').modal('show');

                    $("#preRequestBtn1").removeAttr('disabled');
                    // $("#processingMessage").html("Message Sent.");
                    // $("#processingMessage").show().css('color','green');
                    $("#preRequestBtn1").css('background-color','#10a5da','border','#10a5da');
                }else if( responseArray['status'] == 2 ){
                  if($('#exampleModal2').hasClass('show')){
                    $('#exampleModal2').modal('hide');
                  }
                  var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                  $('#customModalLabel').html('Alert');
                  $('#customModalBody').html(responseArray['message']);
                  $('#customModalFoot').html(buttonHtml);
                  $('#customBox').modal('show');
                  $("#customBox .loaderImg").hide();
                }else{
                    $("#processingMessage").css('color','red').html(responseArray['message']).show();
                    $("#preRequestBtn1").removeAttr('disabled');
                    $("#preRequestBtn1").css('background-color','#10a5da','border','#10a5da');
                }
            }
        });
    }
});
 $('#messagePassword').keypress(function(e){
     if(e.which == 13){//Enter key pressed
         $('#preConfirmBtn2').click();
         return false;
     }
 });
</script>
