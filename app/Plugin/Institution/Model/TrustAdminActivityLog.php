<?php


App::uses('AppModel', 'Model');

/**
 * Represents model trsut_admin_activity_logs
 */
class TrustAdminActivityLog extends AppModel {

	public function addActivityLog($params=array()){
		$dataObject = array();
		if(!empty($params)){
			if(isset($params['admin_id'])) $dataObject['admin_id'] = $params['admin_id'];
			if(isset($params['user_id'])) $dataObject['user_id'] = $params['user_id'];
			if(isset($params['action'])) $dataObject['action'] = $params['action'];
			if(isset($params['custom_data'])) $dataObject['custom_data'] = $params['custom_data'];
			if(isset($params['company_id'])) $dataObject['company_id'] = $params['company_id'];

			$this->saveAll($dataObject);
		}
	}
}
