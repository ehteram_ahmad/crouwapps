<?php
/*
User model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');

class AdminUser extends AppModel {

	/*
	----------------------------------------------------------------------
	On:
	Desc:
	I/P:
	O/P:
	----------------------------------------------------------------------
	*/
	public function checkAdmin($params = array()){
		$userData = array();
		if(!empty($params)){
			$userData = $this->find('first',
	                    array(
	                    'conditions'=>
	                      array(
	                        'AdminUser.username'=> $params['userName'],
	                        'AdminUser.status'=>1,
	                        ),
	                      // 'fields'=> array("AdminUser.username,AdminUser.company_id,AdminUser.role_id,User.password,AdminUser.status")
	                     )
	                    );
		}
		return $userData;
	}

	/*
	----------------------------------------------------------------------
	On:
	Desc:
	I/P:
	O/P:
	----------------------------------------------------------------------
	*/
	public function checkLogin($params = array()){
		$userData = array();
		if(!empty($params)){
			$userData = $this->find('first',
	                    array(
	                    'joins'=> array(
			                    array(
			                      'table' => 'users',
			                      'alias' => 'User',
			                      'type' => 'left',
			                      'conditions'=> array('User.email = AdminUser.username')
			                  	),
													array(
			                      'table' => 'user_profiles',
			                      'alias' => 'UserProfile',
			                      'type' => 'left',
			                      'conditions'=> array('UserProfile.user_id = User.id')
			                  	)
                    		),
	                    'conditions'=>
	                      array(
	                        'User.email'=> $params['userName'],
	                        'User.password'=> md5( $params['password'] ),
	                        'User.status'=> 1,
	                        'User.approved'=> 1,
	                        // 'AdminUser.status'=>1,
	                        ),
	                      'fields'=> array("AdminUser.username,AdminUser.company_id,AdminUser.role_id,User.id,User.password,UserProfile.country_id,UserProfile.first_name,UserProfile.last_name,AdminUser.status")
	                     )
	                    );
		}
		return $userData;
	}

	/*
	----------------------------------------------------------------------
	On:
	Desc:
	I/P:
	O/P:
	----------------------------------------------------------------------
	*/
	public function checkPresence($params = array()){
		$userData = array();
		if(!empty($params)){
			$userData = $this->find('first',
	                    array(
	                    'conditions'=>
	                      array(
	                        'AdminUser.email'=> $params['userName'],
	                        'AdminUser.status'=> 1,
	                        'AdminUser.role_id'=> $params['role_id'],
													'AdminUser.company_id' => $params['company_id']
	                        ),
	                     )
	                    );
		}
		return $userData;
	}
}
?>
