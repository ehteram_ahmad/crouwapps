<?php

class InstitutionAuditController extends InstitutionAppController {
    public $components = array('RequestHandler','Paginator','Session', 'Institution.InsCommon', 'Institution.InsEmail', 'Image', 'Quickblox', 'Cache');

    public $uses = array('EmailTemplate','Institution.User','Institution.EnterpriseUserList','Institution.UserEmployment','Institution.AdminUser','Institution.CompanyName','Institution.UserSubscriptionLog','Institution.NotificationBroadcastMessage','Institution.UserQbDetail','Institution.UserDutyLog','Institution.UserOneTimeToken','Institution.CacheLastModifiedUser','Institution.ExportChatTransaction','Institution.UserProfile','Institution.QrCodeRequest','Institution.Profession','Institution.RoleTag','Institution.AvailableAndOncallTransaction','Institution.ScreenShotTransaction','Institution.TrustAdminActivityLog');
    public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

    public function screenShot($user_id = NULL){
      $this->checkAdminRole();
        // Admin activity logs [START]
        $activityData = array();
        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
        $activityData['admin_id'] = $_COOKIE['adminUserId'];
        $activityData['action'] = 'Screen Shot Audit Trail';
        $activityData['custom_data'] = 'view';
        $this->TrustAdminActivityLog->addActivityLog($activityData);
        // Admin activity logs [END]
        $this->set(array('tCount'=>1,'user_id'=>$user_id));
    }

    public function screenShotFilter() {

      $user_country_id = $_COOKIE['adminCountryId'];
    	if($user_country_id==226){
          // date_default_timezone_set('Europe/Amsterdam');
          date_default_timezone_set("Europe/London");
      }else if($user_country_id==99){
          date_default_timezone_set('Asia/Kolkata');
      }

    	$companyId = $_COOKIE['adminInstituteId'];
      $user_id = $this->data['user_id'];

    	if(isset($this->data['page_number'])){
    	    $page_number=$this->data['page_number'];
    	}
    	else{
    	    $page_number=1;
    	}

      if(isset($this->data['start_date'])){
					$start_date = date('Y-m-d H:i:s', strtotime($this->data['start_date']));
			}
			else{
					$start_date = date("Y-m-d H:i:s");
			}

			if(isset($this->data['end_date'])){
					$end_date = date('Y-m-d H:i:s', strtotime($this->data['end_date']));
			}
			else{
					$end_date = date("Y-m-d H:i:s");
			}

      if(isset($this->data['type']) && $this->data['type'] != 'all'){
				$date_condition = array(
					'ScreenShotTransaction.created <= ' => $end_date,
					'ScreenShotTransaction.created >= ' => $start_date
				);
			}else{
				$date_condition = array();
			}

      if(isset($this->data['search_text'])){
  		    $text=$this->data['search_text'];
          $search_condition = array(
            'OR'=>array(
              'LOWER(Users.email) LIKE'=>strtolower('%'.$text.'%'),
              'LOWER(ScreenShotTransaction.dialog_name) LIKE'=>strtolower('%'.$text.'%'),
              'LOWER(CONCAT(userProfiles.first_name, " ", userProfiles.last_name)) LIKE'=>strtolower('%'.$text.'%')
            )
          );
  		}
  		else{
  		    $search_condition = array();
  		}

      if(isset($this->data['section'])){
          $section=$this->data['section'];
      }
      else{
          $section='all';
      }

    	if(isset($this->data['limit'])){
    	    $limit=$this->data['limit'];
    	}
    	else{
    	    $limit=20;
    	}

    	$conditions = array(
        'ScreenShotTransaction.institute_id' => $companyId,
        'ScreenShotTransaction.status' => 1,
		  );

      if(isset($user_id) && (int)$user_id > 0){
        $conditions = array_merge($conditions,array('Users.id' => $user_id));
      }
      $conditions = array_merge($conditions,$search_condition,$date_condition);

		$fields = array('ScreenShotTransaction.user_id,ScreenShotTransaction.dialog_name,ScreenShotTransaction.created,userProfiles.first_name,userProfiles.last_name,Users.email');
		$joins = array(
			array(
				'table' => 'users',
				'alias' => 'Users',
				'type' => 'left',
				'conditions'=> array('Users.id = ScreenShotTransaction.user_id')
			),
			array(
				'table' => 'user_profiles',
				'alias' => 'userProfiles',
				'type' => 'left',
				'conditions'=> array('userProfiles.user_id = ScreenShotTransaction.user_id')
			)
		);

    	$options = array(
    		'conditions' => $conditions,
    		'joins' => $joins,
    		'fields' => $fields,
    		'limit' => $limit,
    		'page'=> $page_number,
    		// 'group' => array('ScreenShotTransaction.user_id'),
    		'order' => 'ScreenShotTransaction.created DESC',
    	);

      // print_r($this->data);
      // exit;
    	$this->Paginator->settings = $options;
    	$userList = $this->Paginator->paginate('ScreenShotTransaction');

    	$total = $this->request->params;
    	$tCount = $total['paging']['ScreenShotTransaction']['count'];
      foreach ($userList as $key => $value) {
        // $value['ScreenShotTransaction']['created'] = strtotime
      }
    	$data['user_list'] = $userList;
    	$this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount,'section'=>$section,'user_id'=>$user_id));
		    $this->render('/Elements/audit/screenshot_ajax');
    }

    public function status(){
      $this->checkAdminRole();
        // Admin activity logs [START]
        $activityData = array();
        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
        $activityData['admin_id'] = $_COOKIE['adminUserId'];
        $activityData['action'] = 'Available/OnCall Audit Trail';
        $activityData['custom_data'] = 'view';
        $this->TrustAdminActivityLog->addActivityLog($activityData);
        // Admin activity logs [END]
        $this->set(array('tCount'=>1));
    }

    public function statusFilter() {

        $userData = $this->User->find("first", array("conditions"=> array("User.email"=> $_COOKIE['adminEmail'])));
        $user_country_id = $userData['UserProfile']['country_id'];
        if($user_country_id==226){
            // date_default_timezone_set('Europe/Amsterdam');
            date_default_timezone_set("Europe/London");
        }else if($user_country_id==99){
            date_default_timezone_set('Asia/Kolkata');
        }

        $companyId = $_COOKIE['adminInstituteId'];
        $groupAdmin = $_COOKIE['instituteGroupAdminId'];

        if(isset($this->data['page_number'])){
            $page_number=$this->data['page_number'];
        }
        else{
            $page_number=1;
        }

        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }

        if(isset($this->data['search_text'])){
            $text=$this->data['search_text'];
            $search_condition = array('OR'=>array('LOWER(User.email) LIKE'=>strtolower('%'.$text.'%'),"LOWER(CONCAT(userProfiles.first_name, ' ', userProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%')));
        }else{
            $search_condition = array();
        }
        $custom_condition = array();
        $custom_groupAdmin_condition = array();
        if($groupAdmin != 0 ){
            $custom_groupAdmin_condition = array('AND'=>array('User.id !=' => $groupAdmin));
        }

        if(isset($this->data['type'])){
    			$val = $this->data['type'];
    			if($val == 'all'){
    				$custom_condition = array();
    			}else if($val == 2){
    				$custom_condition = array('UserDutyLog.status'=>0,'UserDutyLog.atwork_status'=>0);
    			}else if($val == 0){
    				$custom_condition = array('UserDutyLog.status'=>1,'UserDutyLog.atwork_status'=>1);
    			}else{
    				$custom_condition = array('UserDutyLog.atwork_status'=>1);
    			}
    		}

        $conditions = array(
            'UserEmployment.company_id' => $companyId,
            'UserEmployment.is_current' => 1,
            'UserEmployment.status' => 1,
            'User.status' => 1,
            'User.approved' => 1,
            'EnterpriseUserList.status' => 1,
        );
        $conditions = array_merge($conditions,$search_condition,$custom_groupAdmin_condition,$custom_condition);
        $fields = array('
                userProfiles.first_name,
                userProfiles.last_name,
                userProfiles.profile_img,
                User.id,
                User.email,
                UserDutyLog.status,
                UserDutyLog.atwork_status,
                UserDutyLog.modified,
                Max(AvailableAndOncallTransaction.created) AS date_cust
            ');
        $joins = array(
            array(
                'table' => 'user_duty_logs',
                'alias' => 'UserDutyLog',
                'type' => 'left',
                'conditions'=> array('UserDutyLog.user_id = User.id')
            ),
            array(
                'table' => 'available_and_oncall_transactions',
                'alias' => 'AvailableAndOncallTransaction',
                'type' => 'left',
                'conditions'=> array('AvailableAndOncallTransaction.user_id = User.id')
            ),
            array(
                'table' => 'user_profiles',
                'alias' => 'userProfiles',
                'type' => 'left',
                'conditions'=> array('userProfiles.user_id = User.id')
            ),
            array(
                'table' => 'user_employments',
                'alias' => 'UserEmployment',
                'type' => 'left',
                'conditions'=> array('UserEmployment.user_id = User.id')
            ),
            array(
                'table' => 'enterprise_user_lists',
                'alias' => 'EnterpriseUserList',
                'type' => 'left',
                'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
            )
        );

        $options = array(
            'conditions' => $conditions,
            'joins' => $joins,
            'fields' => $fields,
            'limit' => $limit,
            'page'=> $page_number,
            'group' => array('User.id'),
            'order' => 'userProfiles.first_name ASC',
            // 'order' => 'LOWER(userProfiles.first_name) ASC',
        );
        $this->Paginator->settings = $options;
        $userList = $this->Paginator->paginate('User');

        $total = $this->request->params;
        $tCount = $total['paging']['User']['count'];

        $data['user_list'] = $userList;
        $this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
        $this->render('/Elements/audit/status_ajax');
    }

    public function getDashboardCounts(){
      $this->autoRender = false;
      if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
        $companyId = $_COOKIE['adminInstituteId'];
        $groupAdmin = $_COOKIE['instituteGroupAdminId'];

        if(isset($this->data['search_text'])){
            $text=$this->data['search_text'];
            $search_condition = array('OR'=>array('LOWER(User.email) LIKE'=>strtolower('%'.$text.'%'),"LOWER(CONCAT(userProfiles.first_name, ' ', userProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%')));
        }else{
            $search_condition = array();
        }
        $custom_groupAdmin_condition = array();
        if($groupAdmin != 0 ){
            $custom_groupAdmin_condition = array('AND'=>array('User.id !=' => $groupAdmin));
        }

        $conditions = array(
            'UserEmployment.company_id' => $companyId,
            'UserEmployment.is_current' => 1,
            'UserEmployment.status' => 1,
            'User.status' => 1,
            'User.approved' => 1,
            'EnterpriseUserList.status' => 1,
        );
        $conditions = array_merge($conditions,$search_condition,$custom_groupAdmin_condition);
        $fields = array('
                userProfiles.first_name,
                userProfiles.last_name,
                userProfiles.profile_img,
                User.id,
                User.email,
                UserDutyLog.status,
                UserDutyLog.atwork_status,
                UserDutyLog.modified,
            ');
        $joins = array(
            array(
                'table' => 'user_duty_logs',
                'alias' => 'UserDutyLog',
                'type' => 'left',
                'conditions'=> array('UserDutyLog.user_id = User.id')
            ),
            array(
                'table' => 'available_and_oncall_transactions',
                'alias' => 'AvailableAndOncallTransaction',
                'type' => 'left',
                'conditions'=> array('AvailableAndOncallTransaction.user_id = User.id')
            ),
            array(
                'table' => 'user_profiles',
                'alias' => 'userProfiles',
                'type' => 'left',
                'conditions'=> array('userProfiles.user_id = User.id')
            ),
            array(
                'table' => 'user_employments',
                'alias' => 'UserEmployment',
                'type' => 'left',
                'conditions'=> array('UserEmployment.user_id = User.id')
            ),
            array(
                'table' => 'enterprise_user_lists',
                'alias' => 'EnterpriseUserList',
                'type' => 'left',
                'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
            )
        );
        $conditions_all = $conditions;
        $conditions_not_available = array_merge($conditions,array('UserDutyLog.status'=>0,'UserDutyLog.atwork_status'=>0));
        $conditions_available = array_merge($conditions,array('UserDutyLog.atwork_status'=>1));
        $conditions_on_call = array_merge($conditions,array('UserDutyLog.status'=>1,'UserDutyLog.atwork_status'=>1));

        $data = array(
          'all_users' => 0,
          'available_users' => 0,
          'on_call_users' => 0
        );

        $data['all_users'] = (int)$this->User->find('count',array(
          'conditions'=> $conditions_all,
          'joins' => $joins,
          'group' => array('User.id')
        ));
        $data['available_users'] = (int)$this->User->find('count',array(
          'conditions'=> $conditions_available,
          'joins' => $joins,
          'group' => array('User.id')
        ));
        $data['not_available_users'] = (int)$this->User->find('count',array(
          'conditions'=> $conditions_not_available,
          'joins' => $joins,
          'group' => array('User.id')
        ));
        $data['on_call_users'] = (int)$this->User->find('count',array(
          'conditions'=> $conditions_on_call,
          'joins' => $joins,
          'group' => array('User.id')
        ));

        return json_encode($data);
      }
    }

    public function getTransaction($user_id = null){
      $this->checkAdminRole();
        if($user_id != null){
            // Admin activity logs [START]
            $activityData = array();
            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
            $activityData['user_id'] = $user_id;
            $activityData['admin_id'] = $_COOKIE['adminUserId'];
            $activityData['action'] = 'Available/OnCall Transaction History';
            $activityData['custom_data'] = 'view';
            $this->TrustAdminActivityLog->addActivityLog($activityData);
            // Admin activity logs [END]
            $this->set(array("user_id"=>$user_id,'tCount'=>1));
        }else{
            $this->redirect(BASE_URL . 'institution/InstitutionAudit/status');
        }
    }

    public function getTransactionFilter(){
        $userData = $this->User->find("first", array("conditions"=> array("User.email"=> $_COOKIE['adminEmail'])));
        $user_country_id = $userData['UserProfile']['country_id'];
        if($user_country_id==226){
            // date_default_timezone_set('Europe/Amsterdam');
            date_default_timezone_set("Europe/London");
        }else if($user_country_id==99){
            date_default_timezone_set('Asia/Kolkata');
        }
        $companyId = $_COOKIE['adminInstituteId'];
        $user_id = $this->data['user_id'];

        if(isset($this->data['section'])){
            $section=$this->data['section'];
        }
        else{
            $section='all';
        }
        if(isset($this->data['page_number'])){
            $page_number=$this->data['page_number'];
        }
        else{
            $page_number=1;
        }

        if(isset($this->data['limit'])){
            $limit=$this->data['limit'];
        }
        else{
            $limit=20;
        }

        $conditions = array(
            'AvailableAndOncallTransaction.user_id' => $user_id,
            'AvailableAndOncallTransaction.company_id' => $companyId,
        );

        $fields = array('AvailableAndOncallTransaction.status,AvailableAndOncallTransaction.created,AvailableAndOncallTransaction.platform,AvailableAndOncallTransaction.device_type,AvailableAndOncallTransaction.type');

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'limit' => $limit,
            'page' => $page_number,
            'order' => 'AvailableAndOncallTransaction.created DESC',
        );
        $this->Paginator->settings = $options;
        $userList = $this->Paginator->paginate('AvailableAndOncallTransaction');

        $total = $this->request->params;
        $tCount = $total['paging']['AvailableAndOncallTransaction']['count'];

        $data['user_list'] = $userList;
        $this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount,'section'=>$section,'user_id'=>$user_id));
        $this->render('/Elements/audit/transaction_history_ajax');
    }

    public function registrationReport(){
      $this->checkAdminRole();
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = 'User Registration Report';
      $activityData['custom_data'] = 'view';
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }

    public function report_filter(){
      $companyId = $_COOKIE['adminInstituteId'];
  		$groupAdmin = $_COOKIE['instituteGroupAdminId'];
      $sortPost=$this->data;

      if(!isset($sortPost['to']) || $sortPost['to']==""){
        $endDate = date("Y-m-d");
      }else{
        $endDate = date('Y-m-d', strtotime($sortPost['to']));
      }

      if(!isset($sortPost['from_date']) || $sortPost['from_date']==""){
        $date = array();
      }else{
        $startDate = date('Y-m-d', strtotime($sortPost['from_date']));
        $date=array('User.registration_date <= ' => $endDate." 23:59:59",'User.registration_date >= ' => $startDate." 00:00:00");
      }

      $conditions = array('User.status' => 1,'User.approved' => 1,'EnterpriseUserList.status' => array(1));
      $company = array('UserEmployment.company_id' => $companyId,'UserEmployment.is_current' => 1,'UserEmployment.status' => 1);
      $tag = array('RoleTag.key' => 'Base Location');

      $conditions1 = array_merge($conditions,$date,$company,$tag);
      $fields1 = array("User.id,User.email,User.registration_date,UserEmployment.company_id,RoleTag.value,Profession.profession_type");

      $options1  =
          array(
          'joins'=>array(
              array(
                  'table' => 'professions',
                  'alias' => 'Profession',
                  'type' => 'left',
                  'conditions'=> array('UserProfile.profession_id = Profession.id')
              ),
              array(
                  'table' => 'user_role_tags',
                  'alias' => 'UserRoleTag',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserRoleTag.user_id')
              ),
              array(
                  'table' => 'role_tags',
                  'alias' => 'RoleTag',
                  'type' => 'left',
                  'conditions'=> array('UserRoleTag.role_tag_id = RoleTag.id')
              ),
              array(
                  'table' => 'user_employments',
                  'alias' => 'UserEmployment',
                  'type' => 'left',
                  'conditions'=> array('User.id = UserEmployment.user_id')
              ),
              array(
                  'table' => 'enterprise_user_lists',
                  'alias' => 'EnterpriseUserList',
                  'type' => 'left',
                  'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
              ),
          ),
          'group' => array('User.id'),
          'fields'=> $fields1,
          'conditions'=> $conditions1,
      );


        $adminUserData = $this->User->find('all',$options1);
        $arr = array();
        $included_ids = array();
        $final_arr = array();

        foreach($adminUserData as $key => $item){
            $included_ids[] = $item['User']['id'];
            $arr[$item['RoleTag']['value']][] = $item;
        }
        array_push($included_ids,(int)$groupAdmin);

        $not_include = array('User.id !=' => $included_ids);

        $conditions2 = array_merge($conditions,$date,$company,$not_include);
        $fields2 = array("User.id,User.email,User.registration_date,UserEmployment.company_id,Profession.profession_type");

        $options2  =
            array(
            'joins'=>array(
                array(
                    'table' => 'professions',
                    'alias' => 'Profession',
                    'type' => 'left',
                    'conditions'=> array('UserProfile.profession_id = Profession.id')
                ),
                array(
                    'table' => 'user_employments',
                    'alias' => 'UserEmployment',
                    'type' => 'left',
                    'conditions'=> array('User.id = UserEmployment.user_id')
                ),
                array(
                    'table' => 'enterprise_user_lists',
                    'alias' => 'EnterpriseUserList',
                    'type' => 'left',
                    'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
                ),
            ),
            'group' => array('User.id'),
            'fields'=> $fields2,
            'conditions'=> $conditions2,
        );
        $otherUserData = $this->User->find('all',$options2);
        $totalCount = count($adminUserData)+count($otherUserData);
        ksort($arr);
        if(!empty($otherUserData)){
          $arr['Others (Users without Base Location)'] = $otherUserData;
        }
        foreach ($arr as $key1 => $value1) {
          $final_arr1 = array();
          foreach($value1 as $key2 => $item2){
             $final_arr1[$item2['Profession']['profession_type']][] = $item2;
          }
          $final_arr[$key1] = $final_arr1;
        }

        $finalData = array();
        $data_list = array();
        foreach ($final_arr as $BL => $value) {
          if(!empty($value)){
            $userList = array();
            $count = 0;
            foreach ($value as $PF => $list) {
              $userList1['profession'] = $PF;
              $userList1['count'] = count($list);
              $count += count($list);
              $userList[] = $userList1;
            }
          }
          $finalData['key'] = $BL;
          $finalData['list'] = $userList;
          $finalData['count'] = $count;
          $data_list[] = $finalData;
        }
        $this->set(array("data"=>$data_list,'condition'=>$sortPost,'totalCount'=>$totalCount));
        $this->render('/Elements/audit/daily_report_ajax');
    }

    public function exportReport(){
      $this->layout = null;
      $this->autoLayout = false;
      $this->autoRender = false;
      set_time_limit(180);
  		ini_set('memory_limit','256M');

  		App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));
  		App::import('Vendor','FPDI',array('file' => 'fpdi/fpdi.php'));
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		  $fpdi = new FPDI();

      $data = unserialize($this->request->data['list']);

      if(!isset($this->request->data['to']) || $this->request->data['to']==""){
        $endDate = date("d-m-Y");
      }else{
        $endDate = date('d-m-Y', strtotime($this->request->data['to']));
      }

      if(!isset($this->request->data['from_date']) || $this->request->data['from_date']==""){
        $date = 'Till '.$endDate;
      }else{
        $startDate = date('d-m-Y', strtotime($this->request->data['from_date']));
        $date = $startDate.' To '.$endDate;
      }

      $textHtml = '<table style="margin:0 auto;" border="0" cellspacing="0" cellpadding="5">';
      $textHtml .= '<tr>';
      $textHtml .= '<td>';
      $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" height="76" alt="">';
      $textHtml .= '</td>';
      $textHtml .= '<td bgcolor="#10A5DA" colspan="4">';
      $textHtml .= '<h4 style="margin-top: 16px;">';
      $textHtml .= '<font color="white">User Registration Report<br />Total : '.$this->request->data['total'].'</font></h4>';
      $textHtml .= '</td>';
      $textHtml .= '<td bgcolor="#10A5DA" colspan="0">';
      $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="149" height="76" alt="">';
      $textHtml .= '</td>';
      $textHtml .= '<td text-align: right; bgcolor="#10A5DA" colspan="4">';
      $textHtml .= '<h4>';
      $textHtml .= '<font color="white">'.$date.'</font></h4>';
      $textHtml .= '</td>';
      $textHtml .= '</tr>';
      $textHtml .= '<tr style="font-size: 12px;">';
      $textHtml .= '<td>';
      $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
      $textHtml .= '</td>';
      $textHtml .= '<td bgcolor=" #F4F4F5";  style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; border-right: 1px solid #E8EBEE;"  colspan="2"><strong>Base Location</strong></td>';
      $textHtml .= '<td bgcolor=" #F4F4F5";  style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; text-align: center; border-right: 1px solid #E8EBEE;"  colspan="2"><strong>Count</strong></td>';
      $textHtml .= '<td bgcolor=" #F4F4F5";  style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; border-right: 1px solid #E8EBEE;"  colspan="2"><strong>Base Loaction</strong></td>';
      $textHtml .= '<td bgcolor=" #F4F4F5";  style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; text-align: center; border-right: 1px solid #E8EBEE;"  colspan="2"><strong>Count</strong></td>';
      $textHtml .= '<td>';
      $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
      $textHtml .= '</td>';
      $textHtml .= '</tr>';
      for ($j=0; $j < count($data);) {
        if($j%2 == 0){
          $textHtml .= '<tr style="font-size: 12px;">';
        }
        $key = '';
        $count_key = '';
        if(isset($data[$j+1])){
          $key = $data[$j+1]['key'];
          $count_key = $data[$j+1]['count'];
        }
        $count = '';
        $textHtml .= '<td>';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
        $textHtml .= '</td>';
        $textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="2"><p>'.$data[$j]['key'].'</p></td>';
        $textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; text-align: center; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="2"><p>'.$data[$j]['count'].'</p></td>';
        $textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="2"><p>'.$key.'</p></td>';
        $textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; text-align: center; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="2"><p>'.$count_key.'</p></td>';
        $textHtml .= '<td>';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
        $textHtml .= '</td>';
        if($j%2 == 0){
          $textHtml .= '</tr>';
        }
        $j += 2;
      }

      for ($i=0; $i < count($data) ; $i++) {
        $textHtml .= '<tr width="100%" height="20px;"><td colspan="8"></td></tr>';

        $textHtml .= '<tr>';
        $textHtml .= '<td>';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" height="76" alt="">';
        $textHtml .= '</td>';
        $textHtml .= '<td bgcolor="#10A5DA" colspan="5">';
        $textHtml .= '<h4 style="margin-top: 16px;">';
        $textHtml .= '<font color="white">'.$data[$i]['key'].'</font></h4>';
        $textHtml .= '</td>';
        $textHtml .= '<td bgcolor="#10A5DA" colspan="0">';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="149" height="76" alt="">';
        $textHtml .= '</td>';
        $textHtml .= '<td bgcolor="#10A5DA" colspan="3">';
        $textHtml .= '<h4>';
        $textHtml .= '<font color="white">Total Count : '.$data[$i]['count'].'</font></h4>';
        $textHtml .= '</td>';
        $textHtml .= '</tr>';
        $textHtml .= '<tr style="font-size: 12px;">';
        $textHtml .= '<td>';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
        $textHtml .= '</td>';
        $textHtml .= '<td bgcolor=" #F4F4F5";  style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; border-right: 1px solid #E8EBEE;"  colspan="4"><strong>Profession</strong></td>';
        $textHtml .= '<td bgcolor=" #F4F4F5";  style="padding: 10px;  border: 1px solid #E8EBEE; outline: 2px solid #E8EBEE; outline-offset: -6px; text-align: center; border-right: 1px solid #E8EBEE;"  colspan="4"><strong>Count</strong></td>';
        $textHtml .= '<td>';
        $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
        $textHtml .= '</td>';
        $textHtml .= '</tr>';

        if(!empty($data[$i]['list'])){
          foreach ($data[$i]['list'] as $list):
            $textHtml .= '<tr style="font-size: 12px;">';
            $textHtml .= '<td>';
            $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
            $textHtml .= '</td>';
            $textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="4"><p>'.$list['profession'].'</p></td>';
            $textHtml .= '<td bgcolor= "#ffffff"; style="padding: 10px; text-align: center; border: 2px solid #E8EBEE; outline: 4px solid #fff; outline-offset: -6px; border-right: 2px solid #E8EBEE; border-left: 2px solid #E8EBEE; border-top: 2px solid #fff;" colspan="4"><p>'.$list['count'].'</p></td>';
            $textHtml .= '<td>';
            $textHtml .= '<img src="'.BASE_URL . 'app/webroot/dailyReport/templates/spacer.gif" width="10" alt="">';
            $textHtml .= '</td>';
            $textHtml .= '</tr>';
          endforeach;
        }else{
          $textHtml .= '<tr>';
          $textHtml .= '<td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>';
          $textHtml .= '</tr>';
        }
      }
      $textHtml .= '</table>';

      // Chat pdf Main Chats [END] ---------------------------

  		$headerTemplate = APP . 'webroot/dailyReport/templates/header.pdf';
  		$files = array($headerTemplate);
  		$pageCount = 0;
  		foreach($files AS $file) {
  		       $pagecount = $fpdi->setSourceFile($file);
  		       for ($i = 1; $i <= $pagecount; $i++) {
  		            $tplidx = $fpdi->ImportPage($i);
  		            // $s = $fpdi->getTemplatesize($tplidx);
                  $fpdi->setPrintHeader(false);
                  $fpdi->setPrintFooter(false);
  		            $fpdi->AddPage('P');
  		            $fpdi->useTemplate($tplidx,null, 0, 0, 0, FALSE);
  		            $fpdi->SetXY(0, 30);
  		            // $fpdi->SetFont('helvetica', '', 11);
  		            $fpdi->writeHTML($textHtml, true, true, true, 0);
  		       }
  		  }

  		$finalFileName = 'Chathistory' . strtotime(date('Y-m-d H:i:s')) . '.pdf';
  		$output = $fpdi->Output(APP . 'webroot/chatHistory/'.$finalFileName, 'F');
      chmod(APP . 'webroot/chatHistory/'.$finalFileName, 0777);
      $path = BASE_URL . 'app/webroot/chatHistory/'.$finalFileName;
      // readfile($path);
      echo $path;
      try{
	    	// Admin activity logs [START]
	    	$activityData = array();
	    	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
	    	$activityData['admin_id'] = $_COOKIE['adminUserId'];
	    	$activityData['action'] = 'User Registration Report Export';
        $activityData['custom_data'] = '{"date_range" :"'.$date.'"}';
	    	$this->TrustAdminActivityLog->addActivityLog($activityData);
	    	// Admin activity logs [END]
	    } catch (Exception $e) {

	    }
    }

}
