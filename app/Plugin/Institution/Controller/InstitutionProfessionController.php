<?php

class InstitutionProfessionController extends InstitutionAppController {
    public $components = array('RequestHandler','Paginator','Session', 'Institution.InsCommon', 'Institution.InsEmail', 'Image', 'Quickblox', 'Cache');

    public $uses = array(
      'EmailTemplate',
      'Institution.User',
      'Institution.EnterpriseUserList',
      'Institution.UserEmployment',
      'Institution.AdminUser',
      'Institution.CompanyName',
      'Institution.UserSubscriptionLog',
      'Institution.NotificationBroadcastMessage',
      'Institution.UserQbDetail',
      'Institution.UserDutyLog',
      'Institution.UserOneTimeToken',
      'Institution.CacheLastModifiedUser',
      'Institution.ExportChatTransaction',
      'Institution.UserProfile',
      'Institution.QrCodeRequest',
      'Institution.Profession',
      'Institution.RoleTag',
      'Institution.AvailableAndOncallTransaction',
      'Institution.ScreenShotTransaction',
      'Institution.TrustAdminActivityLog',
      'Institution.SessionTimeIp',
      'Institution.SessionTimeIpRange',
      'Institution.UserRoleTag',
      'Institution.RoleTagCateogaries',
      'Institution.OncallAccessProfession'
    );
    public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

    public function getCateogaries(){
      $this->autoRender = false;
      $companyId = $_COOKIE['adminInstituteId'];
      $countryId = $_COOKIE['adminCountryId'];
      $profession_name = $this->request->data['profession_name'];
      $existence = $this->Profession->find('count',array('conditions'=>array('LOWER(profession_type)'=>strtolower($profession_name))));
      if($existence == 0){
        $list = $this->RoleTagCateogaries->find('all',array('conditions'=>array('status'=>1)));
        $data = array();
        foreach ($list as $value) {
          $data[] = array(
            'name'=>$value['RoleTagCateogaries']['name'],
            'id'=>$value['RoleTagCateogaries']['id']
          );
        }
        $responseData['data'] = $data;
        $responseData['status'] = 1;
        $responseData['message'] = 'ok.';
      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Profession Already Exist.';
      }
      return json_encode($responseData);
    }

    public function uploadTagData($params = array(), $profId = null, $countryId = null){
      $this->autoRender = false;
      $companyId = $_COOKIE['adminInstituteId'];
      App::import('Vendor','PHPExcel',array('file' => 'PHPExcel/Classes/PHPExcel.php'));
      App::import('Vendor','PHPExcel',array('file' => 'PHPExcel/Classes/IOFactory.php'));
      PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
      if(isset($profId) && isset($profId) && isset($countryId)){

        $tag_key = $params['key'];
        $file = $params['file'];

        $file_name  = $file['name'];
        $file_temp  = $file['tmp_name'];
        $file_size  = $file['size'];
        $file_error = $file['error'];
        $file_ext = explode('.', $file_name);
        $file_ext = strtolower(end($file_ext));
        $allowed = array('csv', 'xls', 'xlsx');

        $file_new_name = uniqid('',true). '.' .$file_ext;
        $file_destination = APP .'/webroot/files/'.$file_new_name;
        if(move_uploaded_file($file_temp, $file_destination)){
          $fileName = $file_destination;
          if(!empty($fileName)){
            $row = 1;
            if (($handle = fopen($fileName , "r")) !== FALSE) {
              if($file_ext == 'xlsx' || $file_ext == 'xls'){
                $objPHPExcel = PHPExcel_IOFactory::load($file_destination);
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                for ($row = 1; $row <= $highestRow; $row++){

                  //  Read a row of data into an array
                  $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                  if($row == 1) continue;
                  $tagName = ucwords($rowData[0][0]);

                  $existence = $this->RoleTag->find('count',array(
                    'conditions'=>array(
                      'key'=>$tag_key,
                      'LOWER(value)'=>strtolower(trim($tagName)),
                      'profession_id' => $profId,
                      'status' => 1,
                      'country_id' => $countryId
                    )
                  ));

                  $tagValArray = array();
                  $tagValArray = array(
                      'key' => $tag_key,
                      'value' => trim($tagName),
                      'profession_id' => $profId,
                      'trust_id' => $companyId,
                      'status' => 1,
                      'country_id' => $countryId
                    );
                  if(!empty($tagValArray) && $existence == 0){
                    $this->RoleTag->saveAll($tagValArray);
                  }

                  if($row % 100 == 0) { sleep(1); }

                }
              }else{
                while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                  $num = count($data);
                  $row++;
                  if($row == 2) continue;
                  $tagName = ucwords($data[0]);

                  $existence = $this->RoleTag->find('count',array(
                    'conditions'=>array(
                      'key'=>$tag_key,
                      'LOWER(value)'=>strtolower(trim($tagName)),
                      'profession_id' => $profId,
                      'status' => 1,
                      'country_id' => $countryId
                    )
                  ));

                  $tagValArray = array();
                  $tagValArray = array(
                      'key' => $tag_key,
                      'value' => trim($tagName),
                      'profession_id' => $profId,
                      'trust_id' => $companyId,
                      'status' => 1,
                      'country_id' => $countryId
                    );

                  if(!empty($tagValArray) && $existence == 0){
                    $this->RoleTag->saveAll($tagValArray);
                  }

                  if($row % 100 == 0) { sleep(1); }
                }
              }
            }
          }
        }
      }
    }

    public function addTagFileUpload(){
      $this->autoRender = false;
      $companyId = $_COOKIE['adminInstituteId'];
      $countryId = $_COOKIE['adminCountryId'];
      $responseData = array();
      $response = array();
      $validEmail = array();
      $invalidEmail = array();
      $createdDate = date("Y-m-d H:i:s");
      if($this->request->is('post')) {
        $paramsInput  = $this->request->params;
        $paramsvalue  = $this->request->data;
        $companyId = $_COOKIE['adminInstituteId'];
        $is_student = isset($paramsvalue['student']) ? $paramsvalue['student'] : 0;

        $profession_name = $paramsvalue['profession_name'];

        if($is_student == 1){
          $this->Profession->saveAll(array('profession_type'=>ucwords($profession_name),'tags'=>'','country_id'=>$countryId,'status'=>1));
        }else{
          $tag_cateogary = isset($paramsvalue['tag_cateogary'])? $paramsvalue['tag_cateogary']:[];
          if(count($tag_cateogary) > 0){
              $tag_txt = 'Base Location'.','.implode(",",$tag_cateogary);
          }else{
            $tag_txt = 'Base Location';
          }

          $this->Profession->saveAll(array('profession_type'=>ucwords($profession_name),'tags'=>$tag_txt,'country_id'=>$countryId,'status'=>1));

          $profId = $this->Profession->getLastInsertId();

          $this->OncallAccessProfession->saveAll(array(
            'profession_id' => $profId,
            'status' => 1
          ));

          $form_data = isset($paramsInput['form']) ? $paramsInput['form'] : [];
          if(!empty($form_data)){
            foreach ($form_data as $key => $value) {
              $tag_upload_array = array();
              $tag_upload_array['key'] = $key;
              $tag_upload_array['file'] = $value;
              if($value['error'] === 0){
                $this->uploadTagData($tag_upload_array,$profId,$countryId);
              }
            }
          }
          $tagValArray = array();
          if(count($tag_cateogary) > 0){
            foreach ($tag_cateogary as $tag_value) {
              if(isset($paramsvalue[$tag_value]) && $paramsvalue[$tag_value] != ''){
                $tagValArray[] = array(
                  'key' => $tag_value,
                  'value' => trim($paramsvalue[$tag_value]),
                  'trust_id' => $companyId,
                  'profession_id' => $profId,
                  'status' => 1,
                  'country_id' => $countryId
                );
              }
            }
          }
          if(!empty($tagValArray)){
            $this->RoleTag->saveAll($tagValArray);
          }
        }

        $responseData['status'] = 1;
        $responseData['message'] = 'OK.';
        // Admin activity logs [START]
          $activityData = array();
          $activityData['company_id'] = $_COOKIE['adminInstituteId'];
          $activityData['admin_id'] = $_COOKIE['adminUserId'];
          $activityData['action'] = 'New Profession Added';
          $activityData['custom_data'] = json_encode(array('file'=>$paramsInput,'form_data'=>$paramsvalue));
          $this->TrustAdminActivityLog->addActivityLog($activityData);
        // Admin activity logs [END]
      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Information not provided.';
      }
      echo json_encode($responseData);
    }

  }
