<?php

/*
 * Email Component
 * 
 * Contains all related function for email sending
 *
 */

App::uses('CakeEmail', 'Network/Email');

class EmailComponent extends Component{ 
	
	/*
	--------------------------------------------------------------------
	On: 
	I/P:
	O/P:
	Desc: Replace variable and put values from email template.
	--------------------------------------------------------------------
	*/
	function strreplacetextreplace(&$t, $d) {
		preg_match_all ( '/{\%(\w*)\%\}/' , $t , $matches );
		foreach($matches[1] as $m){
						$pattern = "/{\%".$m."\%\}/";
						$t = preg_replace( $pattern, $d[$m], $t);
		}
		return $t;
    }

	/*
	------------------------------------------------------------------------
	On: 27-07-17
	I/P: 
	O/P: 
	Desc: Sending mail user when institute approves the user.
	------------------------------------------------------------------------
	*/	

	public function userApproveByInstitute( $params = array()){ 
		if( !empty($params) ){
			try {
				App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
				$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
				$fromName = '';
				$params['subject'] = 'Approval Confirmed';
				$template_name = "MB_Manual_Approval_Confirmed";
				if($params['registration_source'] == "MB"){
					//$template_name = 'Congratulation MB Jan 2017';
					$fromName = 'Medic Bleep';
					//$Email->from(array(NO_REPLY_EMAIL => "Medic Bleep"));
				}else{
					//$template_name = 'Congratulation MB Jan 2017';
					$fromName = 'The On Call Room';
					//$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
				}
			    $template_content = array(
			        array(
			            'name' => $template_name,
			            'content' => 'Approval Confirmed'
			        )
			    );
				$message = array(
				'html' => 'Approval Confirmed',
		        'text' => 'Approval Confirmed',
		        'subject' => $params['subject'],
		        'from_email' => 'support@medicbleep.com',//NO_REPLY_EMAIL
		        'from_name' => $fromName,
		        'to' => array(
		            array(
		                'email' => "ehteram@mediccreations.com",
		                'name' => "Ehteram Ahmad",
		                'type' => 'to'
		            )
		        ),
		        'global_merge_vars' => array(
		            array(
		                'name' => 'NAME',
		                'content' => "Ehteram Ahmad"
		            ),
		            array(
		                'name' => 'SITE_NAME',
		                'content' => 'OCR Team'
		            )
		        )

				);
			    $async = false;
			    $ip_pool = 'Main Pool';
			    $send_at = date('d-m-Y');
			    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
    			//print_r($result);

			}
			catch(Mandrill_Error $e) {
			    // Mandrill errors are thrown as exceptions
			    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			    throw $e;
			}
		}
	}

	/*
	------------------------------------------------------------------------
	On: 27-07-17
	I/P: 
	O/P: 
	Desc: Sending mail user when institute approves the user.
	------------------------------------------------------------------------
	*/	

	public function sendEmailOnQrCodeRequest( $params = array()){ 
		if( !empty($params) ){
			try {
				App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
				$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
				$fromName = '';

				$template = "MB_Order_Received";

			    $template_content = array(
			        array(
			            'name' => $template,
			            'content' => 'MB Order Recieved'
			        )
			    );
				$to[]=array(
					'email'=> $params['to_email'],
					'name' => $params['to_email'],
					'type' => 'to'
				);


				$var[]=array(
					'rcpt' => $params['to_email'],
					'vars' => array(
						array(
							'name' => 'FNAME',
							'content' => $params['fname']
						),
						array(
							'name' => 'QUANTITY',
							'content' => $params['quantity']
						),
						array(
							'name' => 'DELIVERYDATE',
							'content' => $params['delivery_date']
						),
					)
				);	

				$message = array(
				'html' => 'MB Order Recieved',
				'text' => 'MB Order Recieved',
				'subject' => 'MB Order Recieved',
				//'from_email' => 'medicbleep@theoncallroom.com',
				'from_email' => 'support@medicbleep.com',
				'from_name' => 'Medic Bleep',
				'to' => $to,
				"merge_vars" => $var,
				);
				$async = false;
				$ip_pool = 'Main Pool';
				$send_at = date('d-m-Y');
				$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
				$msg = $result;
    			//print_r($result);

			}
			catch(Mandrill_Error $e) {
			    // Mandrill errors are thrown as exceptions
			    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			    throw $e;
			}
		}
	}

}
?>
