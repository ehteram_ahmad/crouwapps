<?php

/*
 Common component to use common methods.
*/

class InsCommonComponent extends Component {

	private $encryptKey = 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd';
	private $ivKey = 'XX.s9GNvnP+zRA^Y';
	/*
	-----------------------------------------------------------------------------------------------
	On: 28-07-2015
	I/P:	$params = array()
			$params['folderpath'] = Path where folder have to create
	Desc: Creating folder dynamically.
	-----------------------------------------------------------------------------------------------
	*/
	public function createFolder($params = array()){
		if(!empty($params)){
			if (!file_exists($params['folderPath'])) {
				mkdir($params['folderPath'], 0755, true);
			}
		}
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 10-08-2015
	I/P: NA
	O/P: random password
	Desc: Generating password randomly and return it
	-----------------------------------------------------------------------------------------------
	*/
	public function randomPassword() {
	    /*$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 6; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string*/
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    // Small,capital,number minimum 6 char
	    $smallAlphabet = "abcdefghijklmnopqrstuwxyz";
	    $capitalAlphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
	    $number = "0123456789";
	    $string = $capitalAlphabet[rand(0, strlen($capitalAlphabet)-1 )].$smallAlphabet[rand(0, strlen($smallAlphabet)-1)].$number[rand(0, strlen($number)-1 )].$capitalAlphabet[rand(0, strlen($capitalAlphabet)-1 )].$number[rand(0, strlen($number)-1 )].$smallAlphabet[rand(0, strlen($smallAlphabet)-1 )];
	    return $string;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 01-10-2015
	I/P:
	O/P:
	Desc: Remove physical file creating for temp operation
	-----------------------------------------------------------------------------------------------
	*/
	public function removeTempFile( $filePath = NULL) {
		$return = false;
		if( !empty($filePath) && file_exists( $filePath )){
			chmod($filePath, 777);
			if(unlink( $filePath ))
					$return = true;
		}
		return $return;
	}

	/*
	_______________________________________________
	On:
	I/P:
	O/P:
	Desc:
	_________________________________________________
	*/

	public function xssCleaner($data)
	{
		// Fix &entity\n;
		$data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
		$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
		$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
		$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

		// Remove any attribute starting with "on" or xmlns
		$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

		// Remove javascript: and vbscript: protocols
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
		$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

		// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
		$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

		// Remove namespaced elements (we do not need them)
		$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

		do
		{
			// Remove really unwanted tags
			$old_data = $data;
			$data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
		}
		while ($old_data !== $data);

		// we are done...
		return $data;
	}

	/*
	_______________________________________________
	On:
	I/P:
	O/P:
	Desc:
	_________________________________________________
	*/
	public function encryptData( $text = NULL ){
		//** Get text with padding
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$pad = $size - (strlen($text) % $size);
    	$text = $text . str_repeat(chr($pad), $pad);
    	//** Get encrypted text
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $this->encryptKey, $text, MCRYPT_MODE_CBC, $this->ivKey ) );
		return $encrypted;
	}

	/*
	_______________________________________________
	On:
	I/P:
	O/P:
	Desc:
	_________________________________________________
	*/
	public function decryptData( $encrypted = NULL ){
		//** Decrypting Data
        // return $encrypted;
        $main_string = $encrypted;
        try{
			$encrypted = base64_decode($encrypted);
			$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $this->encryptKey, $encrypted, MCRYPT_MODE_CBC, $this->ivKey );
		  	//** Remove Padding
		  	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
	        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
	        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
	        if($decrypted == '' & $main_string != 'F5kZgOvvBVW2cIFKUiPEeQ=='){
				return $main_string;
	        }
	        return $decrypted;
        }catch(Exception $e){
        	return $main_string;
        }
	}

	/*
	On:
	I/P:
	O/P:
	Desc: Send notification on event.
	*/
	public function sendGcmMessage( $ids = array(), $data = array(), $params = array() ){
	    //$apiKey = 'AIzaSyAkQx7N0Trq2UE2A1wFvum1_urFHU9NUuM';
	    $apiKey = 'AIzaSyDxbsIp_iqxyKJVjU2bDbgAUd9Fj8mh1ws';
	    $url = 'https://android.googleapis.com/gcm/send';
		$post = array(
	                    'registration_ids'  => $ids,
	                    'data'              => $data,
	                    'notification'=> array('title' => 'The OCR','body'  => !empty($data['message']) ? $data['message'] :'', 'sound'=>'default', 'badge'=> !empty($data['tot_unread_notification']) ? (string) $data['tot_unread_notification'] : (string) 0),
	                    //'content_available' => true,
	                    //'priority'=> 'high'
	                    'content_available' => isset($params['content_available']) ? $params['content_available'] : true,
	                    'priority'=> isset($params['priority']) ? $params['priority'] : 'high',
	                );
	    $headers = array(
	                    'Authorization: key=' . $apiKey,
	                    'Content-Type: application/json'
	                    );
	    $ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );
		$result = curl_exec( $ch );
		$curlError = '';
		if ( curl_errno( $ch ) )
	    {
	        $curlError = curl_error( $ch );
	    }
		curl_close( $ch );
		$response = !empty($curlError) ? $curlError : $result;
		return $response;
	}

	/*
	--------------------------------------------------------------------------------------------
	On: 18-11-2015
	I/P: $content
	O/P: array of hash tags
	Desc: Getting all unique hash tags from any content
	--------------------------------------------------------------------------------------------
	*/
	public function getHashTags( $string ) {
	    $hashtags = FALSE;
	    preg_match_all("/(#\w+)/u", $string, $matches);
	    if ($matches) {
	        $hashtagsArray = array_count_values($matches[0]);
	        $hashtags = array_keys($hashtagsArray);
	    }
	    return $hashtags;
	}

	/*
	----------------------------------------------------------------------------------
	On: 26-01-2016
	I/P:
	O/P:
	Desc: Creating consent pdf files.
	----------------------------------------------------------------------------------
	*/
	public function createConsentPdf( $consentData = array(), $userId = NULL, $fileName = NULL){
		App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));
		App::import('Vendor','FPDI',array('file' => 'fpdi/fpdi.php'));
		// create a PDF object
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$fpdi = new FPDI();
		// add a page
		$pdf->AddPage();
		// set font
		$pdf->SetFont('helvetica', '', 20);
		$pdf->Ln();
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// set bacground image
        $backgroundImg = APP . 'webroot/consent_pdfs/templates/consent2bg.png';
        $pdf->Image($backgroundImg, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		// set auto page breaks
		//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->SetAutoPageBreak(TRUE);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$horizontalRow = "<hr />";
		foreach ($consentData as $consentD) {
			$imgPath = ''; $imgFullPath = ''; $imgHtml = ''; $textHtml = ''; $videoHtml = '';
			if( $consentD["attribute_type"] == 'text' ){
				$textHtml = "<p>" . $consentD["content"] . "</p>";
				$pdf->writeHTML($textHtml, true, 0, true, true);
				$pdf->writeHTML($horizontalRow, true, 0, true, true);
			}
			if( $consentD["attribute_type"] == 'video' ){
				$videoHtml = "<p>" . $consentD["content"] . "</p>";
				$pdf->writeHTML($videoHtml, true, 0, true, true);
				$pdf->writeHTML($horizontalRow, true, 0, true, true);
			}
			if( $consentD["attribute_type"] == 'image' ){
				$imgPath = $consentD["content"];
				$imgFullPath = AMAZON_PATH . $userId . '/ConsentForm/image/' . $imgPath;
				if (@getimagesize($imgFullPath)) {
					if($consentD["is_signature"] == 1){
						$imgHtml = '<p align="right"><img width ="200px;" height ="150px;" align="right" src="'. $imgFullPath .'"></p>';
					}else{
						$imgHtml = '<p align="center"><img align="center" src="'. $imgFullPath .'"></p>';
					}
					$pdf->writeHTML($imgHtml, true, 0, true, true);
					$pdf->writeHTML($horizontalRow, true, 0, true, true);
				}
			}
		}
		//** Create Real pdf file ondisk
		$tempPdfFileName = $userId . strtotime(date('Y-m-d H:i:s')) . '.pdf';
		$tempFile = APP . 'webroot/consent_pdfs/' . $tempPdfFileName;
		$pdf->Output($tempFile , 'F');
		//** Merge Header Template
		// array to hold list of PDF files to be merged
		$headerTemplate = APP . 'webroot/consent_pdfs/templates/consent-optimised.pdf';
		$files = array($headerTemplate, $tempFile);
		$pageCount = 0;
		// iterate through the files
		foreach($files AS $file) {
               $pagecount = $fpdi->setSourceFile($file);
               for ($i = 1; $i <= $pagecount; $i++) {
                    $tplidx = $fpdi->ImportPage($i);
                    $s = $fpdi->getTemplatesize($tplidx);
                    $fpdi->AddPage('P', array($s['w'], $s['h']));
                    $fpdi->useTemplate($tplidx);
               }
          }
		$output = $fpdi->Output(APP . 'webroot/consent_pdfs/' . $fileName, 'F');
		//** Delete temp consent pdf file (after sending mail) from physical directory
		$this->removeTempFile( $tempFile );
	}

	/*
	------------------------------------------------------------------------------------
	On: 17-03-2016
	I/P: $url
	O/P: $rootDomain
	Desc: get root domain of any url
	------------------------------------------------------------------------------------
	*/
	public function getTld($url = "") {
		$rootDomain = "";
		if(!empty($url)){
		$fullUrl = "http://www." . $url;
		$pieces = parse_url($fullUrl);
		if (strpos($pieces['host'], "nhs.uk") !== false) {
			$rootDomain = "nhs.uk";
		}else{
			$domain = isset($pieces['host']) ? $pieces['host'] : '';
				if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $m)) {
					$rootDomain = $m['domain'];
				}
			}
		}
		return $rootDomain;
	}

	/*
	Upload file
	*/
	public function uploadFileToserver($targetPath=NULL, $files=array(), $fileFieldName=NULL, $newFileName=NULL){
		$fileUploadMsg = "";
		$addedFileName = "";
		//$addedFileName = $targetPath . $newFileName;
		$temp = explode(".", basename( $files[$fileFieldName]['name']));
		$newfilename = strtotime( date('Y-m-d H:i:s') ). rand(0,1000).'.' . end($temp);
		$addedFileName = $targetPath . $newfilename;
		try{
				if(move_uploaded_file( $files[$fileFieldName]['tmp_name'], $addedFileName )){
					$fileUploadMsg = "File Uploaded";
				}
		}catch(Exception $e){ $fileUploadMsg = $e->getMessage(); }
		return array($fileUploadMsg, $newfilename);
	}

	public function genrateRandomToken($userId=NULL){
		// return '12345678';
		if(!empty($userId)){
			$userInfo = $userId.rand(1000,100000);
			$token = md5(crypt($userInfo));
		}
		return $token;
	}

	// public function genrateRandomToken($userId=NULL)
	// {
	// 	if(!empty($userId))
	// 	{
	// 		$userInfo = $userId.rand(1000,100000);
	// 		$options = [
	// 		    'cost' => 4,
	// 		];
	// 		$token = md5(password_hash($userInfo, PASSWORD_BCRYPT, $options));
	// 		// $token = md5(crypt($userInfo));
	// 	}
	// 	return $token;
	// }

}
