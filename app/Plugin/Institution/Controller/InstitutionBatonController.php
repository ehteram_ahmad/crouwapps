<?php

class InstitutionBatonController extends InstitutionAppController {
	public $components = array(
		'RequestHandler',
		'Paginator',
		'Session',
		'Institution.InsCommon',
		'Institution.InsEmail',
		'Image',
		'Quickblox',
		'Cache'
	);

	public $uses = array(
		'EmailTemplate',
		'Institution.User',
		'Institution.EnterpriseUserList',
		'Institution.UserEmployment',
		'Institution.AdminUser',
		'Institution.CompanyName',
		'Institution.UserSubscriptionLog',
		'Institution.NotificationBroadcastMessage',
		'Institution.UserQbDetail',
		'Institution.UserDutyLog',
		'Institution.UserOneTimeToken',
		'Institution.CacheLastModifiedUser',
		'Institution.ExportChatTransaction',
		'Institution.UserProfile',
		'Institution.QrCodeRequest',
		'Institution.Profession',
		'Institution.RoleTag',
		'Institution.AvailableAndOncallTransaction',
		'Institution.TrustAdminActivityLog',
		'Institution.UserIpSetting',
		'Institution.UserBatonRole',
		'Institution.DepartmentsBatonRole',
		'Institution.BatonRole'
	);
	public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

	public function index(){
		$this->checkAdminRole();
		try{
				// Admin activity logs [START]
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'Baton Role List';
				$activityData['custom_data'] = 'View';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
		} catch (Exception $e) {

		}
	}

	public function batonFilter(){
		$this->autoRender = false;
		if( null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$companyId = $_COOKIE['adminInstituteId'];
			if(isset($this->data['search_text']) && $this->data['search_text'] != ""){
			    $text=$this->data['search_text'];
			    //$search_condition = array('LOWER(BatonRole.baton_roles) LIKE'=>strtolower('%'.$text.'%'));
					$search_condition = array(
						'OR'=>array(
							"LOWER(CONCAT(userProfiles.first_name, ' ', userProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%'),
							"LOWER(BatonRole.baton_roles) LIKE"=>strtolower('%'.$text.'%')
						));
			}else{
			    $search_condition = array();
			}

			if(isset($this->data['page_number'])){
			    $page_number=$this->data['page_number'];
			}else{
			    $page_number=1;
			}

			if(isset($this->data['limit'])){
			    $limit=$this->data['limit'];
			}else{
			    $limit=20;
			}

			$fields = array('
				BatonRole.baton_roles,
				BatonRole.id,
				DepartmentsBatonRole.timeout,
				DepartmentsBatonRole.on_call_value,
				DepartmentsBatonRole.is_occupied,
				userBatonRoles.from_user,
				userBatonRoles.to_user,
				userBatonRoles.id,
				userBatonRoles.status,
				userProfiles.user_id,
				userProfiles.first_name,
				userProfiles.last_name,
				userProfile.user_id,
				userProfile.first_name,
				userProfile.last_name
				');
			$conditions = array(
				'DepartmentsBatonRole.is_active' => 1,
				'DepartmentsBatonRole.institute_id' => $companyId
				);

			$conditions_finalv1 = array_merge($conditions,$search_condition);
			$conditions_final = array();
			if($this->data['filter_by'] == 'Assigned'){
				$filter_condition = array('DepartmentsBatonRole.is_occupied' => 1);
				$conditions_final = array_merge($conditions_final,array('DepartmentsBatonRole.is_occupied' => 1));
			}elseif($this->data['filter_by'] == 'Unassigned'){
				$filter_condition = array('DepartmentsBatonRole.is_occupied' => 0);
				$conditions_final = array_merge($conditions_final,array('DepartmentsBatonRole.is_occupied' => 0));
			}else{
				$filter_condition = array();
			}

			$conditions_finalv2 = array_merge($conditions_finalv1,$filter_condition);

			$joins = array(
				array(
					'table' => 'baton_roles',
					'alias' => 'BatonRole',
					'type' => 'left',
					'conditions'=> array('BatonRole.id = DepartmentsBatonRole.role_id')
				),
				array(
					'table' => 'user_baton_roles',
					'alias' => 'userBatonRoles',
					'type' => 'left',
					'conditions'=> array(
						'userBatonRoles.role_id = BatonRole.id',
						'userBatonRoles.is_active' => 1,
						'userBatonRoles.status'=> array(1,2,3,4,5),
						// 'userBatonRoles.status'=> array(1,2,5),
						'userBatonRoles.from_user !='=> array(0)
					),
					'order'=>'userBatonRoles.updated_at DESC'
				),
				array(
					'table' => 'user_profiles',
					'alias' => 'userProfiles',
					'type' => 'left',
					'conditions'=> array('userProfiles.user_id = userBatonRoles.from_user')
				),
				array(
					'table' => 'user_profiles',
					'alias' => 'userProfile',
					'type' => 'left',
					'conditions'=> array('userProfile.user_id = userBatonRoles.to_user')
				)

			);

			$options = array(
				'conditions' => $conditions_finalv2,
				'joins' => $joins,
				'fields' => $fields,
				'limit' => $limit,
				'page'=> $page_number,
				'group' => array('DepartmentsBatonRole.role_id'),
				'order' => 'BatonRole.baton_roles ASC'
			);

			$this->Paginator->settings = $options;
			$batonList = $this->Paginator->paginate('DepartmentsBatonRole');

			$total = $this->request->params;
			$tCount = (int)$total['paging']['DepartmentsBatonRole']['count'];
			$baton_array = array();
			// print_r($batonList); die();

			foreach ($batonList as $baton_value) {

				if($baton_value['DepartmentsBatonRole']['is_occupied'] == 1){
					$custom_array = array();
					$custom_array = array(
						'id'=> $baton_value['BatonRole']['id'],
						'name'=> $baton_value['BatonRole']['baton_roles'],
						'is_occupied'=> $baton_value['DepartmentsBatonRole']['is_occupied'],
						'on_call_value'=> $baton_value['DepartmentsBatonRole']['on_call_value'],
						'timeout'=> $baton_value['DepartmentsBatonRole']['timeout'],
						// 'user_name' => $baton_value['userProfiles']['first_name'].' '.$baton_value['userProfiles']['last_name'],
						// 'user_id' => $baton_value['userProfiles']['user_id'],
						'role_status' => $baton_value['userBatonRoles']['status']
					);

					if($baton_value['userBatonRoles']['status'] == '1' || $baton_value['userBatonRoles']['status'] == '2' || $baton_value['userBatonRoles']['status'] == '5'){
						// to user
						// user_profiles

						if($baton_value['userProfiles']['first_name'] == ''){
							$custom_array['active_user_name'] = 'Switchboard';
							$custom_array['active_user_id'] = 0;
						}else{
							$custom_array['active_user_name'] = $baton_value['userProfiles']['first_name'].' '.$baton_value['userProfiles']['last_name'];
							$custom_array['active_user_id'] = $baton_value['userProfiles']['user_id'];
						}

						if($baton_value['userProfile']['first_name'] == ''){
							$custom_array['user_name'] = 'Switchboard';
							$custom_array['user_id'] = 0;
						}else{
							$custom_array['user_name'] = $baton_value['userProfile']['first_name'].' '.$baton_value['userProfile']['last_name'];
							$custom_array['user_id'] = $baton_value['userProfile']['user_id'];
						}

						if($custom_array['active_user_id'] == 0){
							$custom_array['active_user_id'] = $custom_array['user_id'];
						}

					}

					if($baton_value['userBatonRoles']['status'] == '3' || $baton_value['userBatonRoles']['status'] == '4'){
						// from user
						// user_profile

						if($baton_value['userProfiles']['first_name'] == ''){
							$custom_array['user_name'] = 'Switchboard';
							$custom_array['user_id'] = 0;
						}else{
							$custom_array['user_name'] = $baton_value['userProfiles']['first_name'].' '.$baton_value['userProfiles']['last_name'];
							$custom_array['user_id'] = $baton_value['userProfiles']['user_id'];
						}

						if($baton_value['userProfile']['first_name'] == ''){
							$custom_array['active_user_name'] = 'Switchboard';
							$custom_array['active_user_id'] = 0;
						}else{
							$custom_array['active_user_name'] = $baton_value['userProfile']['first_name'].' '.$baton_value['userProfile']['last_name'];
							$custom_array['active_user_id'] = $baton_value['userProfile']['user_id'];
						}

						if($custom_array['active_user_id'] == 0){
							$custom_array['active_user_id'] = $custom_array['user_id'];
						}
					}

					// if($baton_value['userProfile']['first_name'] == ''){
					// 	$custom_array['user_name'] = $baton_value['userProfiles']['first_name'].' '.$baton_value['userProfiles']['last_name'];
					// 	$custom_array['user_id'] = $baton_value['userProfiles']['user_id'];
					// }else{
					// 	$custom_array['user_name'] = $baton_value['userProfile']['first_name'].' '.$baton_value['userProfile']['last_name'];
					// 	$custom_array['user_id'] = $baton_value['userProfile']['user_id'];
					// }
					$baton_array[] = $custom_array;
				}else{
					$baton_array[] = array(
						'id'=> $baton_value['BatonRole']['id'],
						'name'=> $baton_value['BatonRole']['baton_roles'],
						'is_occupied'=> $baton_value['DepartmentsBatonRole']['is_occupied'],
						'on_call_value'=> $baton_value['DepartmentsBatonRole']['on_call_value'],
						'timeout'=> $baton_value['DepartmentsBatonRole']['timeout'],
						'user_name' => '',
						'user_id' => '',
						'role_status' => ''
					);
				}
			}

			$data['baton_list'] = $baton_array;
			/*==Start :: Total Count ==*/
			// print_r($data);die;
			$tCount = $this->DepartmentsBatonRole->find('count',array(
				'conditions'=>array(
					array('DepartmentsBatonRole.is_active' => 1,'DepartmentsBatonRole.institute_id' => $companyId),
					$conditions_finalv1
				),
				'joins' => $joins,
				'fields' => $fields,
				'group' => array('DepartmentsBatonRole.role_id'),
				'order' => 'BatonRole.baton_roles ASC'
				)
			);

			/*==End :: Total Count ==*/

			/*==Start :: Assigned Count ==*/
			$tCountAssigned = $this->DepartmentsBatonRole->find('count',array(
				'conditions'=>array(
					array('DepartmentsBatonRole.is_active' => 1,'DepartmentsBatonRole.institute_id' => $companyId,'DepartmentsBatonRole.is_occupied'=>1
					),
					$conditions_finalv1
				),
				'joins' => $joins,
				'fields' => $fields,
				'group' => array('DepartmentsBatonRole.role_id'),
				'order' => 'BatonRole.baton_roles ASC'
				)
			);

			/*==End :: Assigned Count ==*/

			/*==Start :: Unassigned Count ==*/
			$tCount = (int)$tCount;
			$tCountAssigned = (int)$tCountAssigned;
			$totalUnassigned = $tCount - $tCountAssigned;
			/*==End :: Unassigned Count ==*/

			// $this->Paginator->settings = $options;
			// $batonRole = $this->Paginator->paginate('DepartmentsBatonRole');
			// print_r($data);die;
			$this->set(array("data"=>$data,"limit"=>$limit,'tCount'=>$tCount,'tCountAssigned'=>$tCountAssigned,'totalUnassigned'=>$totalUnassigned));
			$this->render('/Elements/baton/baton_ajax');

		}
	}

	public function getUserList(){
		$companyId = $_COOKIE['adminInstituteId'];
		$groupAdmin = $_COOKIE['instituteGroupAdminId'];
		$type = array(0,1);
		if(isset($this->data['search_text'])){
		    $text=$this->data['search_text'];
		    $search_condition = array('OR'=>array('LOWER(User.email) LIKE'=>strtolower('%'.$text.'%'),"LOWER(CONCAT(userProfiles.first_name, ' ', userProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%')));
		}
		else{
		    $search_condition = array();
		}

		// if(isset($this->data['page_number'])){
		//     $page_number=$this->data['page_number'];
		// }
		// else{
		//     $page_number=1;
		// }
		//
		// if(isset($this->data['limit'])){
		//     $limit=$this->data['limit'];
		// }
		// else{
		//     $limit=20;
		// }

		$custom_condition = array();
		$custom_groupAdmin_condition = array();

		if($groupAdmin != 0 ){
			// $custom_groupAdmin_condition = array('User.id !='+(int)$groupAdmin);
			$custom_groupAdmin_condition = array('AND'=>array('User.id !=' => $groupAdmin));
		}

		$fields = array('
			User.id,
			User.email,
			userProfiles.first_name,
			userProfiles.last_name ,
			userProfiles.profile_img,
			UserDutyLog.status,
			UserDutyLog.atwork_status
		');
		$conditions = array(
				'UserEmployment.company_id' => $companyId,
				'UserEmployment.is_current' => 1,
				'UserEmployment.status' => 1,
				'User.status' => 1,
				'User.approved' => 1,
				'EnterpriseUserList.status' => 1,
			);
		$conditions_final = array_merge($custom_condition,$conditions,$custom_groupAdmin_condition,$search_condition);
		$joins = array(
			array(
				'table' => 'user_employments',
				'alias' => 'UserEmployment',
				'type' => 'left',
				'conditions'=> array('UserEmployment.user_id = User.id')
			),
			array(
				'table' => 'user_qb_details',
				'alias' => 'UserQbDetail',
				'type' => 'left',
				'conditions'=> array('User.id = UserQbDetail.user_id')
			),
			array(
				'table' => 'user_duty_logs',
				'alias' => 'UserDutyLog',
				'type' => 'left',
				'conditions'=> array('User.id = UserDutyLog.user_id')
			),
			array(
				'table' => 'user_subscription_logs',
				'alias' => 'UserSubscriptionLog',
				'type' => 'left',
				'conditions'=> array('User.id = UserSubscriptionLog.user_id')
			),
			array(
				'table' => 'user_profiles',
				'alias' => 'userProfiles',
				'type' => 'left',
				'conditions'=> array('userProfiles.user_id = User.id')
			),
			array(
				'table' => 'enterprise_user_lists',
				'alias' => 'EnterpriseUserList',
				'type' => 'left',
				'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
			)
		);

		$options = array(
			'conditions' => $conditions_final,
			'joins' => $joins,
			'fields' => $fields,
			// 'limit' => $limit,
			// 'page'=> $page_number,
			'group' => array('UserEmployment.user_id'),
			'order' => 'User.registration_date DESC'
		);

		$this->Paginator->settings = $options;
		$userList1 = $this->Paginator->paginate('User');
		$newArr = array();
		foreach ($userList1 AS  $userListdata) {
				$cust_class = 'notAtWork';
				if((string)$userListdata['UserDutyLog']['status'] == '1'){
					$cust_class = 'onCallMain';
				}else if((string)$userListdata['UserDutyLog']['atwork_status'] == '0'){
					$cust_class = 'notAtWork';
				}else{
					$cust_class = '';
				}

		    $newArr[] = array(
		    	'id' =>  $userListdata['User']['id'],
		    	'email' =>  $userListdata['User']['email'],
		    	'UserName' => $userListdata['userProfiles']['first_name'].' '.$userListdata['userProfiles']['last_name'],
		    	'profile_img' => isset($userListdata['userProfiles']['profile_img'])?AMAZON_PATH.$userListdata['User']['id'].'/profile/'.$userListdata['userProfiles']['profile_img']:BASE_URL.'institution/img/ava-single.png',
		    	'AtWorkStatus' =>  $userListdata['UserDutyLog']['atwork_status'],
		    	'OnCallStatus' =>  $userListdata['UserDutyLog']['status'],
					'cust_class' => $cust_class
		    );
		}

		// $this->Paginator->settings = $options;
		// $userList1 = $this->Paginator->paginate('User');
		$userList1 = $this->User->find('all',$options);
		$newArr = array();
		foreach ($userList1 AS  $userListdata) {
				$cust_class = 'notAtWork';
				if((string)$userListdata['UserDutyLog']['status'] == '1'){
					$cust_class = 'onCallMain';
				}else if((string)$userListdata['UserDutyLog']['atwork_status'] == '0'){
					$cust_class = 'notAtWork';
				}else{
					$cust_class = '';
				}

		    $newArr[] = array(
		    	'id' =>  $userListdata['User']['id'],
		    	'email' =>  $userListdata['User']['email'],
		    	'UserName' => $userListdata['userProfiles']['first_name'].' '.$userListdata['userProfiles']['last_name'],
		    	'profile_img' => isset($userListdata['userProfiles']['profile_img'])?AMAZON_PATH.$userListdata['User']['id'].'/profile/'.$userListdata['userProfiles']['profile_img']:BASE_URL.'institution/img/ava-single.png',
		    	'AtWorkStatus' =>  $userListdata['UserDutyLog']['atwork_status'],
		    	'OnCallStatus' =>  $userListdata['UserDutyLog']['status'],
					'cust_class' => $cust_class
		    );
		}

		// $total = $this->request->params;
		// $tCount = $total['paging']['User']['count'];

		$data['user_list'] = $newArr;
		// $this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
		$this->set(array("data"=>$data));
		$this->render('/Elements/baton/baton_user_assign_ajax');
	}

	public function addBatonRole(){
		$this->autoRender = false;
		$responseData = array();
		$companyId = $_COOKIE['adminInstituteId'];
		if($this->request->is('post')) {
				//$batonRoleName = $this->request->data['role_name'];
				$batonRoleNameOriginal = $this->request->data['role_name'];
				$batonRoleNameModified =  strip_tags($batonRoleNameOriginal);
				$batonRoleName = trim(preg_replace("/(\r|\n)/", " ", $batonRoleNameModified));
				$on_call_value = $this->request->data['on_call_value'];
				$createdDate = date("Y-m-d H:i:s");
				if( !empty($batonRoleName) && !empty($companyId) ){
						$roleTagData = $this->BatonRole->find('first',array(
							'conditions'=>array(
								'LOWER(BatonRole.baton_roles)' => strtolower($batonRoleName),
								'BatonRole.is_active' => 1,
								'DepartmentsBatonRole.institute_id' => $companyId
							),
							'joins'=>array(
								array(
				          'table' => 'departments_baton_roles',
				          'alias' => 'DepartmentsBatonRole',
				          'type' => 'left',
				          'conditions'=> array('BatonRole.id = DepartmentsBatonRole.role_id')
				        )
							)
						));
						if( !empty($roleTagData) ){
							$DeptroleTagData = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('DepartmentsBatonRole.role_id'=> $roleTagData['BatonRole']['id'],'DepartmentsBatonRole.is_active'=>1)));
							if($roleTagData['BatonRole']['is_active'] == 1 && $DeptroleTagData['DepartmentsBatonRole']['is_active'] == 1){
								$responseData['status'] = 0;
								$responseData['message'] = 'Already Exists';
								echo json_encode($responseData);
							}else{
								$this->BatonRole->updateAll(array('is_active' => 1),array('id'=> $roleTagData['BatonRole']['id']));
								$this->DepartmentsBatonRole->updateAll(array('is_active' => 1),array('role_id'=> $roleTagData['BatonRole']['id']));
								$responseData['status'] = 1;
								$responseData['message'] = 'Baton Role Added Successfully';
								echo json_encode($responseData);
							}
						}else{
								$this->BatonRole->saveAll(
									array(
										'baton_roles'=> $batonRoleName,
										"is_active"=> 1,
										'created_at' => $createdDate
									)
								);
								$roleId = $this->BatonRole->getLastInsertID();
								$this->DepartmentsBatonRole->saveAll(
									array(
										'department_id' => 1,
										"role_id" => $roleId,
										'is_occupied' => 0,
										'is_active' => 1,
										// 'timeout' => ,
										'institute_id' => $companyId,
										'on_call_value' => $on_call_value,
										'created_at' => $createdDate
									)
								);
								$responseData['status'] = 1;
								$responseData['message'] = 'Baton Role Added Successfully';
								echo json_encode($responseData);
						}
						try{
								// Admin activity logs [START]
								$activityData = array();
								$activityData['company_id'] = $_COOKIE['adminInstituteId'];
								$activityData['admin_id'] = $_COOKIE['adminUserId'];
								$activityData['action'] = $responseData['message'];
								$activityData['custom_data'] = '{"role_name":"'.$batonRoleName.'","on_call_value":"'.$on_call_value.'"}';
								$this->TrustAdminActivityLog->addActivityLog($activityData);
								// Admin activity logs [END]
						} catch (Exception $e) {

						}
				}else{
					$responseData['status'] = 0;
					$responseData['message'] = 'Empty Email Or Company Id.';
					echo json_encode($responseData);
				}
		}else{
			$responseData['status'] = 0;
			$responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
		}
	}

	public function assignUserSearch(){
		$this->checkAdminRole();
		// baton_user_assign_ajax
		if( null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
				$companyId = $_COOKIE['adminInstituteId'];
				$groupAdmin = $_COOKIE['instituteGroupAdminId'];
		}

	}

  public function getRequest(){
		$this->checkAdminRole();
		try{
				// Admin activity logs [START]
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'Baton Role Request List';
				$activityData['custom_data'] = 'View';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
		} catch (Exception $e) {

		}
  }
  public function getRequestFilter(){
    if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$companyId = $_COOKIE['adminInstituteId'];
			$groupAdmin = $_COOKIE['instituteGroupAdminId'];
      $userId = 0;
      if(isset($this->data['search_text']) && $this->data['search_text'] != ''){
  		    $text=$this->data['search_text'];
					//$search_condition = array('OR'=>array("LOWER(CONCAT(UserProfiles.first_name, ' ', UserProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%')));
					$search_condition = array(
						'OR'=>array(
							"LOWER(CONCAT(UserProfiles.first_name, ' ', UserProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%'),
							"LOWER(BatonRole.baton_roles) LIKE"=>strtolower('%'.$text.'%')
						));
					//$search_condition = array('OR'=>array("LOWER(BatonRole.baton_roles) LIKE"=>strtolower('%'.$text.'%')));
  		}
  		else{
  		    $search_condition = array();
  		}
			if(isset($this->data['page_number'])){
			    $page_number=$this->data['page_number'];
			}
			else{
			    $page_number=1;
			}

			if(isset($this->data['limit'])){
			    $limit=$this->data['limit'];
			}
			else{
			    $limit=20;
			}
      $fields = array('
        UserBatonRole.role_id,
        UserBatonRole.from_user,
        UserBatonRole.to_user,
        UserBatonRole.role_id,
        UserBatonRole.status,
        UserBatonRole.notes,
        UserBatonRole.updated_at,
        UserProfile.first_name,
        UserProfile.last_name,
        UserProfile.profile_img,
        UserProfile.thumbnail_img,
        UserProfiles.user_id,
        UserProfiles.first_name,
        UserProfiles.last_name,
        UserProfiles.profile_img,
        UserProfiles.thumbnail_img,
        BatonRole.id,
        BatonRole.baton_roles,
        DepartmentsBatonRole.timeout,
        DepartmentsBatonRole.on_call_value,
				UserDutyLog.atwork_status,
				UserDutyLog.status
      ');

			if($this->data['filter_by'] == 'Take On'){
				$filter = array(2);
			}elseif($this->data['filter_by'] == 'Transfer'){
				$filter = array(4);
			}else{
				$filter = array(2,4);
			}

      $conditions = array(
          "UserBatonRole.from_user"=> $userId,
          "UserBatonRole.is_active"=> 1,
					"DepartmentsBatonRole.institute_id" => $companyId
        );
      $conditionsv1 = array_merge($search_condition,$conditions);
			$filter_condition = array("UserBatonRole.status"=> $filter);
			$conditionsv2 = array_merge($conditionsv1,$filter_condition);
      $joins = array(
        array(
          'table' => 'user_profiles',
          'alias' => 'UserProfile',
          'type' => 'left',
          'conditions'=> array('UserBatonRole.from_user = UserProfile.user_id')
        ),
        array(
          'table' => 'user_profiles',
          'alias' => 'UserProfiles',
          'type' => 'left',
          'conditions'=> array('UserBatonRole.to_user = UserProfiles.user_id')
        ),
        array(
          'table' => 'departments_baton_roles',
          'alias' => 'DepartmentsBatonRole',
          'type' => 'left',
          'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
        ),
        array(
          'table' => 'baton_roles',
          'alias' => 'BatonRole',
          'type' => 'left',
          'conditions'=> array('DepartmentsBatonRole.role_id = BatonRole.id'),
          'group'=> array('UserBatonRole.role_id')
        ),
				array(
					'table' => 'user_duty_logs',
					'alias' => 'UserDutyLog',
					'type' => 'left',
					'conditions'=> array('UserProfiles.user_id = UserDutyLog.user_id')
				)
      );

      $options = array(
        'conditions' => $conditionsv2,
        'joins' => $joins,
        'fields' => $fields,
				'limit' => $limit,
				'page'=> $page_number,
				'group'=> 'UserBatonRole.role_id',
				'order' => array('UserBatonRole.updated_at DESC')
      );

			$this->Paginator->settings = $options;
			$userList = $this->Paginator->paginate('UserBatonRole');

      // $userList = $this->UserBatonRole->find('all',$options);
      $data_array = array();
      $data_array['userList'] = array();
      foreach ($userList as  $batonRoleDetail) {
        $otherUserFullname = $batonRoleDetail['UserProfiles']['first_name']." ".$batonRoleDetail['UserProfiles']['last_name'];

				$updated_at = $batonRoleDetail['UserBatonRole']['updated_at'];
				$createdTimeStamp = strtotime($updated_at);
				$timeoutInSecond = $batonRoleDetail['DepartmentsBatonRole']['timeout']*60;
				$duration = $createdTimeStamp+$timeoutInSecond;

				$cust_class = 'notAtWork';
				if((string)$batonRoleDetail['UserDutyLog']['status'] == '1'){
					$cust_class = 'onCallMain';
				}else if((string)$batonRoleDetail['UserDutyLog']['atwork_status'] == '0'){
					$cust_class = 'notAtWork';
				}else{
					$cust_class = '';
				}

          $data_array['userList'][] = array(
              'other_user_id'=> isset($batonRoleDetail['UserProfiles']['user_id']) ? $batonRoleDetail['UserProfiles']['user_id'] : 0,
              'profile_img'=>	isset($batonRoleDetail['UserProfiles']['profile_img']) ? $batonRoleDetail['UserProfiles']['profile_img'] : "",
              //'thumbnail_img'=> isset($batonRoleDetail['UserProfiles']['thumbnail_img']) ? $batonRoleDetail['UserProfiles']['thumbnail_img'] : "",
							'thumbnail_img' => isset($batonRoleDetail['UserProfiles']['profile_img'])?AMAZON_PATH.$batonRoleDetail['UserProfiles']['user_id'].'/profile/'.$batonRoleDetail['UserProfiles']['profile_img']:'',
              'other_user_name'=> $otherUserFullname,
              'role_name'=> isset($batonRoleDetail['BatonRole']['baton_roles']) ? $batonRoleDetail['BatonRole']['baton_roles'] : "",
              'notes'=> isset($batonRoleDetail['UserBatonRole']['notes']) ? $batonRoleDetail['UserBatonRole']['notes'] : "",
              'role_id'=> isset($batonRoleDetail['UserBatonRole']['role_id']) ? $batonRoleDetail['UserBatonRole']['role_id'] : 0,
              'duration'=> isset($duration) ? $duration : 0,
              'type'=>isset($batonRoleDetail['UserBatonRole']['status']) ? $batonRoleDetail['UserBatonRole']['status'] : 0,
              'on_call_value'=>isset($batonRoleDetail['DepartmentsBatonRole']['on_call_value']) ? $batonRoleDetail['DepartmentsBatonRole']['on_call_value'] : 0,
							'cust_class' => $cust_class
              );
      }



			/*==Start :: Total Count ==*/
			$tCount = $this->UserBatonRole->find('count',array(
				'conditions'=>array(
					array("UserBatonRole.status"=> array(2,4)),
					$conditionsv1
				),
				'joins' => $joins,
        'fields' => $fields,
				'group'=> 'UserBatonRole.role_id'
			));

			/*==End :: Total Count ==*/

			/*==Start :: Take On Count ==*/
			$tCountTakeOn = $this->UserBatonRole->find('count',array(
				'conditions'=>array(
					array("UserBatonRole.status"=> array(2)),
					$conditionsv1
				),
				'joins' => $joins,
        'fields' => $fields,
				'group'=> 'UserBatonRole.role_id'
			));

			/*==End :: Take On Count ==*/

			/*==Start :: Transfer Count ==*/
			$tCount = (int)$tCount;
			$tCountTakeOn = (int)$tCountTakeOn;
			$tCountTransfer = $tCount - $tCountTakeOn;
			//$tCountTransfer = isset($tCountTransfer) ? $tCountTransfer : 0;
			/*==End :: Transfer Count ==*/

			// $this->Paginator->settings = $options;
			// $userList = $this->Paginator->paginate('UserBatonRole');

			$this->set(array("data"=>$data_array,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount,'tCountTakeOn'=>$tCountTakeOn,'tCountTransfer'=>$tCountTransfer));
      $this->render('/Elements/baton/request_ajax');
		}
  }

  public function assignUnassignBatonRole(){
    $this->autoRender = false;
    $responseData = array();
    if($this->request->is('post')) {
      $userId = $this->request->data['user_id'];
      $status = $this->request->data['status'];
      $companyId = $_COOKIE['adminInstituteId'];
      $roleId = $this->request->data['role_id'];
			$role_details = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('role_id'=>$roleId)));
			$oncallValue = $role_details['DepartmentsBatonRole']['on_call_value'];
			$createdDate = date("Y-m-d H:i:s");

      if( !empty($companyId) ){

        if($status == 1 && !empty($userId)){

					// ---- Direct assign
          $this->UserBatonRole->saveAll(array('from_user'=>$userId,'to_user'=>0,'role_id'=>$roleId,'status'=>1,'is_active'=>1,'created_at'=>$createdDate,'updated_at'=>$createdDate,'deleted_at'=>$createdDate));
					// ---- Direct assign

          $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>1),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));

					$paramsVal['user_id'] = $userId;
					$paramsVal['on_call_value'] = $oncallValue;
					$paramsVal['company_id'] = $companyId;
					$this->updateOnCallStatusRoleExchange($paramsVal);
					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_assigned";
					if(!empty($userId) && $userId != 0){
						$params['user_id'] = $userId;
						$params['role_id'] = $roleId;
						$params['from_user_id'] = 0;
						$params['push_on'] = 1;
						$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
					}

        }else{

					$user_data = $this->UserBatonRole->find('first',array('conditions'=>array('UserBatonRole.role_id'=>$roleId,'UserBatonRole.is_active'=>1,'UserBatonRole.status'=>array(1,2,5))));

          $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));
          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.role_id'=>$roleId));
					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_unassign";
					if(!empty($user_data['UserBatonRole']['from_user']) && $user_data['UserBatonRole']['from_user'] != 0){
						$params['user_id'] = $user_data['UserBatonRole']['from_user'];
						$params['from_user_id'] = 0;
						$params['role_id'] = $roleId;
						$params['push_on'] = 0;
						$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
					}

					if(!empty($user_data['UserBatonRole']['from_user']) && ($user_data['UserBatonRole']['status'] == 2 || $user_data['UserBatonRole']['status'] == 5)){
						if(!empty($user_data['UserBatonRole']['to_user']) && $user_data['UserBatonRole']['to_user'] != 0){
							$params['user_id'] = $user_data['UserBatonRole']['to_user'];
							$params['from_user_id'] = 0;
							$params['role_id'] = $roleId;
							$params['push_on'] = 0;
							$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
						}
					}
        }

  			$responseData['status'] = 1;
        $responseData['message'] = 'ok.';
        echo json_encode($responseData);

				if(!empty($userId) && $userId != 0){
					// $params['user_id'] = $userId;
					// $params['from_user_id'] = 0;
					// $params['push_on'] = 0;
					// $sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
					// ----- Update in user profile for directory [START] ----
					$update_params = array();
					$update_params['to_user'] = 0;
					$update_params['from_user'] = $userId;
					$this->getUserBatonRolesData($update_params);
					// ----- Update in user profile for directory [END] ----

					$this->updateCacheOnStatusChange($userId,$companyId);
				}

				try{
						// Admin activity logs [START]
						$activityData = array();
						$activityData['company_id'] = $_COOKIE['adminInstituteId'];
						$activityData['admin_id'] = $_COOKIE['adminUserId'];
						$activityData['user_id'] = $userId;
						$activityData['action'] = $params['request_type'];
						$activityData['custom_data'] = '{"role_id":"'.$roleId.'"}';
						$this->TrustAdminActivityLog->addActivityLog($activityData);
						// Admin activity logs [END]
				} catch (Exception $e) {

				}

      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Email Or Company Id.';
        echo json_encode($responseData);
      }
    }else{
      $responseData['status'] = 0;
      $responseData['message'] = 'Information not provided.';
      echo json_encode($responseData);
    }
  }

  public function changeBatonRequestStatus(){
    $this->autoRender = false;
    $responseData = array();
    if($this->request->is('post')) {
      $userId = $this->request->data['user_id'];
      $status = $this->request->data['status'];
      $companyId = $_COOKIE['adminInstituteId'];
      $roleId = $this->request->data['role_id'];
			$createdDate = date("Y-m-d H:i:s");
      if( !empty($companyId) && !empty($userId) ){
				$role_details = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('role_id'=>$roleId)));
				$oncallValue = $role_details['DepartmentsBatonRole']['on_call_value'];
        if($status == 1){
          // try {
              $this->UserBatonRole->updateAll(array('UserBatonRole.status'=>1,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>3,'UserBatonRole.role_id'=>$roleId));

							$paramsVal['user_id'] = $userId;
							$paramsVal['on_call_value'] = $oncallValue;
							$paramsVal['company_id'] = $companyId;

							$this->updateOnCallStatusRoleExchange($paramsVal);
							$this->updateCacheOnStatusChange($userId,$companyId);

							$params['request_type'] = "swichborad_accept";
            	$params['user_id'] = $userId;
            	$params['from_user_id'] = 0;
							$params['role_id'] = $roleId;
            	$params['push_on'] = 1;

							$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

          // } catch (\Exception $e) {}
          $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>1),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));
        }else{
					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_rejected";
					$params['user_id'] = $userId;
					$params['from_user_id'] = 0;
					$params['role_id'] = $roleId;
					$params['push_on'] = 0;

					$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>3,'UserBatonRole.role_id'=>$roleId));
					$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));
        }

        // try {
          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>0,'UserBatonRole.status'=>2,'UserBatonRole.role_id'=>$roleId));
        // } catch (\Exception $e2) {}

  			$responseData['status'] = 1;
        $responseData['message'] = 'ok.';
        echo json_encode($responseData);
				// ----- Update in user profile for directory [START] ----
				$update_params = array();
				$update_params['to_user'] = 0;
				$update_params['from_user'] = $userId;
				$this->getUserBatonRolesData($update_params);
				// ----- Update in user profile for directory [END] ----

				try{
						// Admin activity logs [START]
						$activityData = array();
						$activityData['company_id'] = $_COOKIE['adminInstituteId'];
						$activityData['admin_id'] = $_COOKIE['adminUserId'];
						$activityData['user_id'] = $userId;
						$activityData['action'] = $params['request_type'];
						$activityData['custom_data'] = '{"role_id":"'.$roleId.'","type":"Take On"}';
						$this->TrustAdminActivityLog->addActivityLog($activityData);
						// Admin activity logs [END]
				} catch (Exception $e) {

				}

      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Email Or Company Id.';
        echo json_encode($responseData);
      }
    }else{
      $responseData['status'] = 0;
      $responseData['message'] = 'Information not provided.';
      echo json_encode($responseData);
    }
  }


  public function changeBatonTransferRequestStatus(){
    $this->autoRender = false;
    $responseData = array();
    if($this->request->is('post')) {
      $userId = $this->request->data['user_id'];
      $status = $this->request->data['status'];
      $companyId = $_COOKIE['adminInstituteId'];
      $roleId = $this->request->data['role_id'];
			$createdDate = date("Y-m-d H:i:s");
      if( !empty($companyId) && !empty($userId) ){
				$role_details = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('role_id'=>$roleId)));
				$oncallValue = $role_details['DepartmentsBatonRole']['on_call_value'];
        if($status == 1){
          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>5,'UserBatonRole.role_id'=>$roleId));
          $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));

					$paramsVal['user_id'] = $userId;
					$paramsVal['on_call_value'] = $oncallValue;
					$paramsVal['company_id'] = $companyId;

					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_transfer_accept";
					$params['user_id'] = $userId;
					$params['role_id'] = $roleId;
					$params['from_user_id'] = 0;
					$params['push_on'] = 0;

					$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

        }else{

          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>1,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>1,'UserBatonRole.role_id'=>$roleId));
					$paramsVal['user_id'] = $userId;
					$paramsVal['on_call_value'] = $oncallValue;
					$paramsVal['company_id'] = $companyId;

					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_transfer_rejected";
					$params['user_id'] = $userId;
					$params['role_id'] = $roleId;
					$params['from_user_id'] = 0;
					$params['push_on'] = 0;

					$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
        }
        $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0),array('UserBatonRole.from_user'=>0,'UserBatonRole.status'=>4,'UserBatonRole.role_id'=>$roleId));

  			$responseData['status'] = 1;
        $responseData['message'] = 'ok.';
        echo json_encode($responseData);

				// ----- Update in user profile for directory [START] ----
				$update_params = array();
				$update_params['to_user'] = 0;
				$update_params['from_user'] = $userId;
				$this->getUserBatonRolesData($update_params);
				// ----- Update in user profile for directory [END] ----

				try{
						// Admin activity logs [START]
						$activityData = array();
						$activityData['company_id'] = $_COOKIE['adminInstituteId'];
						$activityData['admin_id'] = $_COOKIE['adminUserId'];
						$activityData['user_id'] = $userId;
						$activityData['action'] = $params['request_type'];
						$activityData['custom_data'] = '{"role_id":"'.$roleId.'","type":"Transfer"}';
						$this->TrustAdminActivityLog->addActivityLog($activityData);
						// Admin activity logs [END]
				} catch (Exception $e) {

				}
      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Email Or Company Id.';
        echo json_encode($responseData);
      }
    }else{
      $responseData['status'] = 0;
      $responseData['message'] = 'Information not provided.';
      echo json_encode($responseData);
    }
  }

	public function updateCacheOnStatusChange($userId, $companyId){
	    //** update dynamic cache[STATRT]
	    $lastModified = date('Y-m-d H:i:s');
	    if(! empty($userId)){
	        $getDynamicChangedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed")));
	        // return $getDynamicChangedUser;
	        if($getDynamicChangedUser > 0){
	            $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');
	        }else{
	            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }

	        //** Get last modified date dynamic directory[START]
	        $getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
	        if(!empty($getDynamicDirectoryLastModified)){
	            $paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
	            $paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
	            $this->updateDynamicEtagData($paramsDynamicEtagUpdate);
	        }
	        //** Get last modified date dynamic directory[END]
	    }
	    //** update dynamic cache[END]
	}

	public function updateBatonRole(){
		$this->autoRender = false;
		$responseData = array();
		$companyId = $_COOKIE['adminInstituteId'];
		if($this->request->is('post')) {
				$batonRoleName = $this->request->data['role_name'];
				$on_call_value = $this->request->data['on_call_value'];
				$role_id = $this->request->data['role_id'];
				$userId = $this->request->data['user_id'];
				$updatedDate = date("Y-m-d H:i:s");
				if( !empty($batonRoleName) && !empty($companyId) ){

						$this->BatonRole->updateAll(array('baton_roles' => '"'.$batonRoleName.'"'),array('id'=> $role_id));
						$this->DepartmentsBatonRole->updateAll(array('on_call_value' => $on_call_value),array('role_id'=> $role_id,'institute_id'=>$companyId));
						$responseData['status'] = 1;
						$responseData['message'] = 'Baton Role updated Successfully';
						echo json_encode($responseData);

						if(!empty($userId) && $userId != 0){
							// ----- Update in user profile for directory [START] ----
							$update_params = array();
							$update_params['to_user'] = 0;
							$update_params['from_user'] = $userId;
							$this->getUserBatonRolesData($update_params);
							// ----- Update in user profile for directory [END] ----

							$this->updateCacheOnStatusChange($userId,$companyId);
						}

						try{
								// Admin activity logs [START]
								$activityData = array();
								$activityData['company_id'] = $_COOKIE['adminInstituteId'];
								$activityData['admin_id'] = $_COOKIE['adminUserId'];
								$activityData['user_id'] = $userId;
								$activityData['action'] = $responseData['message'];
								$activityData['custom_data'] = '{"role_name":"'.$batonRoleName.'","on_call_value":"'.$on_call_value.'"}';
								$this->TrustAdminActivityLog->addActivityLog($activityData);
								// Admin activity logs [END]
						} catch (Exception $e) {

						}
				}else{
					$responseData['status'] = 0;
					$responseData['message'] = 'Information not provided.';
					echo json_encode($responseData);
				}
		}else{
			$responseData['status'] = 0;
			$responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
		}
	}

	/*---------------------*/
	public function deleteBatonRole(){
		$this->autoRender = false;
		$responseData = array();
		$companyId = $_COOKIE['adminInstituteId'];
		if($this->request->is('post')) {
				$batonRoleId = $this->request->data['role_id'];
				$userId = $this->request->data['user_id'];
				$deletedDate = date("Y-m-d H:i:s");
				if( !empty($batonRoleId) && !empty($companyId) ){
				//	$this->BatonRole->updateAll(array('BatonRole.is_active'=>0,'BatonRole.deleted_at'=>"'" . $deletedDate . "'"),array('BatonRole.id'=>$batonRoleId));
					$this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.deleted_at'=>"'" . $deletedDate . "'"),array('UserBatonRole.role_id'=>$batonRoleId));
					$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_active'=>0,'DepartmentsBatonRole.deleted_at'=>"'" . $deletedDate . "'"),array('DepartmentsBatonRole.role_id'=>$batonRoleId,'DepartmentsBatonRole.institute_id'=>$companyId));

					$responseData['status'] = 1;
	        $responseData['message'] = 'ok.';
	        echo json_encode($responseData);

					if(!empty($userId) && $userId != 0){
						$params['request_type'] = "swichborad_deleted";
						$params['user_id'] = $userId;
						$params['role_id'] = $batonRoleId;
						$params['from_user_id'] = 0;
						$params['push_on'] = 0;
						$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

						// ----- Update in user profile for directory [START] ----
						$update_params = array();
						$update_params['to_user'] = 0;
						$update_params['from_user'] = $userId;
						$this->getUserBatonRolesData($update_params);
						// ----- Update in user profile for directory [END] ----

						$this->updateCacheOnStatusChange($userId,$companyId);
					}

						try{
								// Admin activity logs [START]
								$activityData = array();
								$activityData['company_id'] = $_COOKIE['adminInstituteId'];
								$activityData['admin_id'] = $_COOKIE['adminUserId'];
								$activityData['user_id'] = $userId;
								$activityData['action'] = $params['request_type'];
								$activityData['custom_data'] = '{"role_id":"'.$batonRoleId.'"}';
								$this->TrustAdminActivityLog->addActivityLog($activityData);
								// Admin activity logs [END]
						} catch (Exception $e) {

						}
				}else{
					$responseData['status'] = 0;
					$responseData['message'] = 'Information not provided.';
					echo json_encode($responseData);
				}
		}else{
			$responseData['status'] = 0;
			$responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
		}
	}

}
