<?php

class InstitutionHomeController extends InstitutionAppController {
	public $components = array(
		'RequestHandler',
		'Paginator',
		'Session',
		'Institution.InsCommon',
		'Institution.InsEmail',
		'Image',
		'Quickblox',
		'Cache'
	);

	public $uses = array(
		'EmailTemplate',
		'Institution.User',
		'Institution.EnterpriseUserList',
		'Institution.UserEmployment',
		'Institution.AdminUser',
		'Institution.CompanyName',
		'Institution.UserSubscriptionLog',
		'Institution.NotificationBroadcastMessage',
		'Institution.UserQbDetail',
		'Institution.UserDutyLog',
		'Institution.UserOneTimeToken',
		'Institution.CacheLastModifiedUser',
		'Institution.ExportChatTransaction',
		'Institution.UserProfile',
		'Institution.QrCodeRequest',
		'Institution.Profession',
		'Institution.RoleTag',
		'Institution.AvailableAndOncallTransaction',
		'Institution.TrustAdminActivityLog',
		'Institution.UserIpSetting',
		'Institution.UserLoginTransaction',
		'Institution.UserBatonRole',
		'Institution.DepartmentsBatonRole',
		'Institution.BatonRole'
	);
	public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

	public function index($type=null) {
		$this->checkAdminRole();
		if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			if($type == null || $type == ''){
				$type = 'all';
			}

			// Admin activity logs [START]
			$activityData = array();
			$activityData['company_id'] = $_COOKIE['adminInstituteId'];
			$activityData['admin_id'] = $_COOKIE['adminUserId'];
			$activityData['action'] = 'Admin Dashboard';
			$activityData['custom_data'] = 'View';
			$this->TrustAdminActivityLog->addActivityLog($activityData);
			// Admin activity logs [END]

			$this->set(array('type'=>$type));
			// $this->set(array("data"=>$cust_data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount,'type'=>$type,'expiry'=>$expiryDate));
		}
	}

	public function dashboardGraphFilter(){
		$this->autoRender = false;
		if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$companyId = $_COOKIE['adminInstituteId'];
			$groupAdmin = $_COOKIE['instituteGroupAdminId'];

			$cust_data = array();
			$custom_condition = array();
			$count = 0;
			if($groupAdmin != 0){
				$custom_condition = array('User.id != ' =>(int)$groupAdmin);
			}
			//*********** Active Users & New Users Registred Within last seven days **************//

			$conditions_newUser = array(
			        'UserEmployment.company_id' => $companyId,
			        'User.status' => 1,
			        'User.approved' => 1,
			        'DATE(User.registration_date) > DATE_SUB(NOW(), INTERVAL 7 DAY)'
			    );

			$conditions_final_neUser = array_merge($custom_condition,$conditions_newUser);
			$count = $this->User->find('all',array(
			                'conditions' => $conditions_final_neUser,
			                'fiels' => array('user.id'),
			                'joins'=> array(
			                    array(
			                        'table' => 'user_employments',
			                        'alias' => 'UserEmployment',
			                        'type' => 'left',
			                        'conditions'=> array('UserEmployment.user_id = User.id')
			                    )
			                ),
			            ));
			$cust_data['new_users'] = count($count);

			//*********** Active UserList **************//

			if(isset($this->data['start_date'])){
					$start_date = date('Y-m-d H:i:s', strtotime($this->data['start_date']));
					// $start_date = date('Y-m-d H:i:s', (int)$this->data['start_date']);
			}
			else{
					$start_date = date("Y-m-d H:i:s");
			}

			if(isset($this->data['end_date'])){
					$end_date = date('Y-m-d H:i:s', strtotime($this->data['end_date']));
					// $end_date = date('Y-m-d H:i:s', (int)$this->data['end_date']);
			}
			else{
					$end_date = date("Y-m-d H:i:s");
			}

			if(isset($this->data['type']) && $this->data['type'] != 'all'){
				$date_condition = array(
					'User.registration_date <= ' => $end_date,
					'User.registration_date >= ' => $start_date
				);
			}else{
				$date_condition = array();
			}

			$fields = array('
				User.id,
				User.email,
				EnterpriseUserList.status,
				UserDutyLog.status,
				UserDutyLog.atwork_status,
				Professions.profession_type
			');
			$conditions = array(
					'UserEmployment.company_id' => $companyId,
					'UserEmployment.is_current' => 1,
					'UserEmployment.status' => 1,
					'User.status' => 1,
					'User.approved' => 1,
					'EnterpriseUserList.status' => array(0,1),
				);
			$conditions_final = array_merge($custom_condition,$conditions,$date_condition);
			$joins = array(
				array(
					'table' => 'user_employments',
					'alias' => 'UserEmployment',
					'type' => 'left',
					'conditions'=> array('UserEmployment.user_id = User.id')
				),
				array(
					'table' => 'user_subscription_logs',
					'alias' => 'UserSubscriptionLog',
					'type' => 'left',
					'conditions'=> array('User.id = UserSubscriptionLog.user_id')
				),
				array(
					'table' => 'enterprise_user_lists',
					'alias' => 'EnterpriseUserList',
					'type' => 'left',
					'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
				),
				array(
					'table' => 'user_duty_logs',
					'alias' => 'UserDutyLog',
					'type' => 'left',
					'conditions'=> array('User.id = UserDutyLog.user_id')
				),
				array(
					'table' => 'user_profiles',
					'alias' => 'userProfiles',
					'type' => 'left',
					'conditions'=> array('userProfiles.user_id = User.id')
				),
				array(
					'table' => 'professions',
					'alias' => 'Professions',
					'type' => 'left',
					'conditions'=> array('userProfiles.profession_id = Professions.id')
				)
			);

			$options = array(
				'conditions' => $conditions_final,
				'joins' => $joins,
				'fields' => $fields,
				'group' => array('UserEmployment.user_id'),
				'order' => array('User.registration_date DESC'),
			);

			$userList = $this->User->find('all',$options);
			$subscribedUser = 0;
			$pendingUser = 0;
			foreach ($userList AS  $users) {
		    if($users['EnterpriseUserList']['status'] == 1){
		    	$subscribedUser++;
		    }else if($users['EnterpriseUserList']['status'] == 0){
		    	$pendingUser++;
		    }
			}

			App::import('model','User');
			$UserLoginTransaction = new User();

			$query = 'SELECT
								UserLoginTransaction.user_id,
								User.email,
								UserLoginTransaction.login_status,
								EnterpriseUserList.status,
								UserDutyLog.status,
								UserDutyLog.atwork_status,
								Professions.profession_type
								FROM `user_login_transactions` AS UserLoginTransaction

								LEFT JOIN `user_duty_logs` UserDutyLog ON UserLoginTransaction.user_id = UserDutyLog.user_id
								LEFT JOIN `users` User ON UserLoginTransaction.user_id = User.id
								LEFT JOIN `user_employments` UserEmployment ON UserEmployment.user_id = User.id
								LEFT JOIN `user_subscription_logs` UserSubscriptionLog ON User.id = UserSubscriptionLog.user_id
								LEFT JOIN `enterprise_user_lists` EnterpriseUserList ON User.email = EnterpriseUserList.email AND UserEmployment.company_id = EnterpriseUserList.company_id
								LEFT JOIN `user_profiles` userProfiles ON userProfiles.user_id = User.id
								LEFT JOIN `professions` Professions ON userProfiles.profession_id = Professions.id

								WHERE UserLoginTransaction.id IN (
								    SELECT MAX(id)
								    FROM user_login_transactions
								    GROUP BY `user_id`
								)
								AND User.`status` =1
								AND User.`approved` =1
								AND User.`id` != '.(int)$groupAdmin.'
								AND EnterpriseUserList.`status` IN (0,1)
								AND UserLoginTransaction.`login_status` =1
								AND UserEmployment.`is_current`=1
								AND UserDutyLog.`hospital_id`='.$companyId;

			$userList_new = $UserLoginTransaction->query( $query );

			$oncallUsers = 0;
			$availableUsers = 0;
			$totalUsers = 0;
			$professions = array();
			foreach ($userList_new AS  $userListdata) {
				$is_login = 0;

				if($userListdata['EnterpriseUserList']['status'] == 1){
					if($userListdata['UserLoginTransaction']['login_status'] == 1){
						$is_login = 1;
					}
					if($is_login == 1){
						$totalUsers++;
						if($userListdata['UserDutyLog']['status'] == 1){
							$oncallUsers++;
						}
						if($userListdata['UserDutyLog']['atwork_status'] == 1){
							$availableUsers++;
						}
						if(array_key_exists($userListdata['Professions']['profession_type'],$professions)){
							$professions[$userListdata['Professions']['profession_type']] = (int)$professions[$userListdata['Professions']['profession_type']] + 1;
						}else{
							$professions = array_merge($professions,array($userListdata['Professions']['profession_type']=>1));
						}
					}
				}
			}


			$cust_data['all_users'] = $subscribedUser + $pendingUser;
			$cust_data['total_users'] = $totalUsers;
			$cust_data['pending_users'] = $pendingUser;
			$cust_data['approved_users'] = $subscribedUser;
			$cust_data['available_users'] = $availableUsers;
			$cust_data['oncall_users'] = $oncallUsers;
			$cust_data['professions'] = $professions;

			$tCount=$cust_data['all_users'];
			$this->set(array("data"=>$cust_data,"condition"=>$conditions));
			$this->render('/Elements/home/dashboard_graphs');
		}
	}

	public function getDashboardCounts(){
		$this->autoRender = false;
		if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$companyId = $_COOKIE['adminInstituteId'];
			$groupAdmin = $_COOKIE['instituteGroupAdminId'];
			$cust_data = array();
			$custom_condition = array();
			$count = 0;
			if($groupAdmin != 0){
				$custom_condition = array('User.id != ' =>(int)$groupAdmin);
			}
			if(isset($this->data['search_text'])){
			    $text=$this->data['search_text'];
			    $search_condition = array('OR'=>array('LOWER(User.email) LIKE'=>strtolower('%'.$text.'%'),'LOWER(Professions.profession_type) LIKE'=>strtolower('%'.$text.'%'),"LOWER(CONCAT(userProfiles.first_name, ' ', userProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%')));
			}
			else{
			    $search_condition = array();
			}
			//*********** Active Users & New Users Registred Within last seven days **************//

			$conditions_newUser = array(
			        'UserEmployment.company_id' => $companyId,
			        'User.status' => 1,
			        'User.approved' => 1,
			        'DATE(User.registration_date) > DATE_SUB(NOW(), INTERVAL 7 DAY)'
			    );

			$conditions_final_neUser = array_merge($custom_condition,$conditions_newUser,$search_condition);
			$count = $this->User->find('all',array(
			                'conditions' => $conditions_final_neUser,
			                'fiels' => array('user.id,User.email'),
			                'joins'=> array(
			                    array(
			                        'table' => 'user_employments',
			                        'alias' => 'UserEmployment',
			                        'type' => 'left',
			                        'conditions'=> array('UserEmployment.user_id = User.id')
			                    ),
													array(
														'table' => 'user_profiles',
														'alias' => 'userProfiles',
														'type' => 'left',
														'conditions'=> array('userProfiles.user_id = User.id')
													),
													array(
														'table' => 'professions',
														'alias' => 'Professions',
														'type' => 'left',
														'conditions'=> array('userProfiles.profession_id = Professions.id')
													)
			                )
			            ));
			$cust_data['new_users'] = count($count);

			//*********** Active UserList **************//

			$limit = 20;
			$newArr = array();

			$fields = array('User.id,User.email,EnterpriseUserList.status,Professions.profession_type,userProfiles.first_name,userProfiles.last_name');
			$conditions = array(
					'UserEmployment.company_id' => $companyId,
					'UserEmployment.is_current' => 1,
					'UserEmployment.status' => 1,
					'User.status' => 1,
					'User.approved' => 1,
					'EnterpriseUserList.status' => array(0,1)
				);
			$conditions_final = array_merge($custom_condition,$conditions,$search_condition);
			$joins = array(
				array(
					'table' => 'user_employments',
					'alias' => 'UserEmployment',
					'type' => 'left',
					'conditions'=> array('UserEmployment.user_id = User.id')
				),
				array(
					'table' => 'user_subscription_logs',
					'alias' => 'UserSubscriptionLog',
					'type' => 'left',
					'conditions'=> array('User.id = UserSubscriptionLog.user_id')
				),
				array(
					'table' => 'enterprise_user_lists',
					'alias' => 'EnterpriseUserList',
					'type' => 'left',
					'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
				),
				array(
					'table' => 'user_profiles',
					'alias' => 'userProfiles',
					'type' => 'left',
					'conditions'=> array('userProfiles.user_id = User.id')
				),
				array(
					'table' => 'professions',
					'alias' => 'Professions',
					'type' => 'left',
					'conditions'=> array('userProfiles.profession_id = Professions.id')
				)
			);

			$options = array(
				'conditions' => $conditions_final,
				'joins' => $joins,
				'fields' => $fields,
				'group' => array('UserEmployment.user_id'),
				'order' => array('User.registration_date DESC')
			);

			$userList = $this->User->find('all',$options);
			$subscribedUser = 0;
			$pendingUser = 0;

			foreach ($userList AS  $userListdata) {
			    if($userListdata['EnterpriseUserList']['status'] == 1){
			    	$subscribedUser++;
			    }else if($userListdata['EnterpriseUserList']['status'] == 0){
			    	$pendingUser++;
			    }
			}

			$cust_data['all_users'] = $subscribedUser + $pendingUser;
			$cust_data['pending_users'] = $pendingUser;
			$cust_data['approved_users'] = $subscribedUser;

			// $this->set(array("data"=>$cust_data));
			// $this->render('/Elements/home/dash_count');
			return json_encode($cust_data);
		}
	}

	public function getNotificationCount(){
		if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$fields = array('User.id,User.email,User.registration_date,UserEmployment.company_id,userProfiles.first_name,userProfiles.last_name ,userProfiles.profile_img,Professions.id,Professions.profession_type,EnterpriseUserList.status,UserQbDetail.qb_id,UserDutyLog.status,UserDutyLog.atwork_status,UserSubscriptionLog.subscription_expiry_date,UserSubscriptionLog.subscribe_type');
			$conditions = array(
					'UserEmployment.company_id' => $_COOKIE['adminInstituteId'],
					'UserEmployment.is_current' => 1,
					'UserEmployment.status' => 1,
					'User.status' => 1,
					'User.approved' => 1,
					'EnterpriseUserList.status' => 0
				);

			$joins = array(
				array(
					'table' => 'user_employments',
					'alias' => 'UserEmployment',
					'type' => 'left',
					'conditions'=> array('UserEmployment.user_id = User.id')
				),
				array(
					'table' => 'user_qb_details',
					'alias' => 'UserQbDetail',
					'type' => 'left',
					'conditions'=> array('User.id = UserQbDetail.user_id')
				),
				array(
					'table' => 'user_duty_logs',
					'alias' => 'UserDutyLog',
					'type' => 'left',
					'conditions'=> array('User.id = UserDutyLog.user_id')
				),
				array(
					'table' => 'user_subscription_logs',
					'alias' => 'UserSubscriptionLog',
					'type' => 'left',
					'conditions'=> array('User.id = UserSubscriptionLog.user_id')
				),
				array(
					'table' => 'user_profiles',
					'alias' => 'userProfiles',
					'type' => 'left',
					'conditions'=> array('userProfiles.user_id = User.id')
				),
				array(
					'table' => 'professions',
					'alias' => 'Professions',
					'type' => 'left',
					'conditions'=> array('userProfiles.profession_id = Professions.id')
				),
				array(
					'table' => 'enterprise_user_lists',
					'alias' => 'EnterpriseUserList',
					'type' => 'left',
					'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
				)
			);

			$options = array(
				'conditions' => $conditions,
				'joins' => $joins,
				'fields' => $fields,
				'group' => array('UserEmployment.user_id'),
				'order' => array('User.registration_date DESC'),
			);

			$userList = $this->User->find('all',$options);
			$newArr = array();
			foreach ($userList AS  $userListdata) {

			    $newArr[] = array(
			    	'id' =>  $userListdata['User']['id'],
			    	'email' =>  $userListdata['User']['email'],
			    	'registration_date' => $userListdata['User']['registration_date'],
			    	'company_id' => $userListdata['UserEmployment']['company_id'],
			    	'UserName' => $userListdata['userProfiles']['first_name'].' '.$userListdata['userProfiles']['last_name'],
			    	'profile_img' => isset($userListdata['userProfiles']['profile_img'])?AMAZON_PATH.$userListdata['User']['id'].'/profile/'.$userListdata['userProfiles']['profile_img']:'',
			    	'profession_type' => $userListdata['Professions']['profession_type'],
			    	'ProfessionsId' => $userListdata['Professions']['id'],
			    	'UserQbId' =>  $userListdata['UserQbDetail']['qb_id'],
			    	'AtWorkStatus' =>  $userListdata['UserDutyLog']['atwork_status'],
			    	'OnCallStatus' =>  $userListdata['UserDutyLog']['status'],
			    	'SubscriptionExpiryDate' =>  $userListdata['UserSubscriptionLog']['subscription_expiry_date'],
			    	'SubscriptionType' =>  $userListdata['UserSubscriptionLog']['subscribe_type'],
			    	'status' =>  $userListdata['EnterpriseUserList']['status']
			    );
			}
			$count = count($newArr) < 100 ? count($newArr) : '99+';

			$this->set(array('user'=>$newArr,'count'=>$count));
			$this->render('/Elements/home/notification');
		}
	}

	public function users($type=null){
		$this->checkAdminRole();
		if(isset($_COOKIE['adminInstituteId']) && $_COOKIE['adminInstituteId'] != null){
			if($type == null || $type == ''){
				$type = 'all';
			}
			$companyId = $_COOKIE['adminInstituteId'];
			$groupAdmin = $_COOKIE['instituteGroupAdminId'];
			$expiryDate = strtotime($_COOKIE['institutionExpiryDate'])*1000;
			// Admin activity logs [START]
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'User listing';
				$activityData['custom_data'] = 'view';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
			// Admin activity logs [END]
			$this->set(array('type'=>$type,'expiry'=>$expiryDate));
		}
	}

	public function dashboardFilter() {
		$companyId = $_COOKIE['adminInstituteId'];
		$groupAdmin = $_COOKIE['instituteGroupAdminId'];
		$type = array(0,1);

		if(isset($this->data['sort_order']) && $this->data['sort_order'] != ''){
				$order = $this->data['sort_order'];
		}
		else{
				$order = 'DESC';
		}

		if(isset($this->data['sort_value']) && $this->data['sort_value'] != ''){
			$sort_value = $this->data['sort_value'];
			switch ($sort_value) {
				case 'name':
					$sort = "CONCAT(userProfiles.first_name, ' ', userProfiles.last_name) ".$order;
					break;
				case 'email':
					$sort = 'User.email '.$order;
					break;
				case 'profession':
					$sort = 'Professions.profession_type '.$order;
					break;
				case 'registration_date':
					$sort = 'User.registration_date '.$order;
					break;
				case 'expiry_date':
					$sort = 'UserSubscriptionLog.subscription_expiry_date '.$order;
					break;
				default:
					$sort = 'User.registration_date '.$order;
					break;
			}
		}
		else{
				$sort = 'User.registration_date DESC';
		}

		if(isset($this->data['search_text'])){
		    $text=$this->data['search_text'];
		    $search_condition = array('OR'=>array('LOWER(User.email) LIKE'=>strtolower('%'.$text.'%'),'LOWER(Professions.profession_type) LIKE'=>strtolower('%'.$text.'%'),"LOWER(CONCAT(userProfiles.first_name, ' ', userProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%')));
		}
		else{
		    $search_condition = array();
		}

		if(isset($this->data['page_number'])){
		    $page_number=$this->data['page_number'];
		}
		else{
		    $page_number=1;
		}

		if(isset($this->data['limit'])){
		    $limit=$this->data['limit'];
		}
		else{
		    $limit=20;
		}

		$custom_condition = array();
		$custom_groupAdmin_condition = array();

		if(isset($this->data['type'])){
			$val = $this->data['type'];
			if($val == 'all'){
				$type = array(0,1);
			}else if($val == 7){
				$type = array(0,1);
				$custom_condition = array('DATE(User.registration_date) > DATE_SUB(NOW(), INTERVAL 7 DAY)');
			}else{
				$type = $val;
			}
		}
		if($groupAdmin != 0 ){
			// $custom_groupAdmin_condition = array('User.id !='+(int)$groupAdmin);
			$custom_groupAdmin_condition = array('AND'=>array('User.id !=' => $groupAdmin));
		}

		$fields = array('User.id,User.email,User.registration_date,UserEmployment.company_id,userProfiles.first_name,userProfiles.last_name ,userProfiles.profile_img,Professions.id,Professions.profession_type,EnterpriseUserList.status,UserQbDetail.qb_id,UserDutyLog.status,UserDutyLog.atwork_status,UserSubscriptionLog.subscription_expiry_date,UserSubscriptionLog.subscribe_type');
		$conditions = array(
				'UserEmployment.company_id' => $companyId,
				'UserEmployment.is_current' => 1,
				'UserEmployment.status' => 1,
				'User.status' => 1,
				'User.approved' => 1,
				'EnterpriseUserList.status' => $type,
			);
		$conditions_final = array_merge($custom_condition,$conditions,$custom_groupAdmin_condition,$search_condition);
		$joins = array(
			array(
				'table' => 'user_employments',
				'alias' => 'UserEmployment',
				'type' => 'left',
				'conditions'=> array('UserEmployment.user_id = User.id')
			),
			array(
				'table' => 'user_qb_details',
				'alias' => 'UserQbDetail',
				'type' => 'left',
				'conditions'=> array('User.id = UserQbDetail.user_id')
			),
			array(
				'table' => 'user_duty_logs',
				'alias' => 'UserDutyLog',
				'type' => 'left',
				'conditions'=> array('User.id = UserDutyLog.user_id')
			),
			array(
				'table' => 'user_subscription_logs',
				'alias' => 'UserSubscriptionLog',
				'type' => 'left',
				'conditions'=> array('User.id = UserSubscriptionLog.user_id')
			),
			array(
				'table' => 'user_profiles',
				'alias' => 'userProfiles',
				'type' => 'left',
				'conditions'=> array('userProfiles.user_id = User.id')
			),
			array(
				'table' => 'professions',
				'alias' => 'Professions',
				'type' => 'left',
				'conditions'=> array('userProfiles.profession_id = Professions.id')
			),
			array(
				'table' => 'enterprise_user_lists',
				'alias' => 'EnterpriseUserList',
				'type' => 'left',
				'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
			)
		);

		$options = array(
			'conditions' => $conditions_final,
			'joins' => $joins,
			'fields' => $fields,
			'limit' => $limit,
			'page'=> $page_number,
			'group' => array('UserEmployment.user_id'),
			'order' => $sort,
		);
		// print_r($options);
		// exit;
		$this->Paginator->settings = $options;
		$userList1 = $this->Paginator->paginate('User');
		$newArr = array();
		foreach ($userList1 AS  $userListdata) {
				$cust_class = 'notAtWork';
				if((string)$userListdata['UserDutyLog']['status'] == '1'){
					$cust_class = 'onCallMain';
				}else if((string)$userListdata['UserDutyLog']['atwork_status'] == '0'){
					$cust_class = 'notAtWork';
				}else{
					$cust_class = '';
				}

		    $newArr[] = array(
		    	'id' =>  $userListdata['User']['id'],
		    	'email' =>  $userListdata['User']['email'],
		    	// 'registration_date' => $userListdata['User']['registration_date'],
		    	'registration_date' => date('d-m-Y', strtotime($userListdata['User']['registration_date'])),
		    	'company_id' => $userListdata['UserEmployment']['company_id'],
		    	'UserName' => $userListdata['userProfiles']['first_name'].' '.$userListdata['userProfiles']['last_name'],
		    	'profile_img' => isset($userListdata['userProfiles']['profile_img'])?AMAZON_PATH.$userListdata['User']['id'].'/profile/'.$userListdata['userProfiles']['profile_img']:'',
		    	'profession_type' => $userListdata['Professions']['profession_type'],
		    	'ProfessionsId' => $userListdata['Professions']['id'],
		    	'UserQbId' =>  $userListdata['UserQbDetail']['qb_id'],
		    	'AtWorkStatus' =>  $userListdata['UserDutyLog']['atwork_status'],
		    	'OnCallStatus' =>  $userListdata['UserDutyLog']['status'],
		    	'SubscriptionExpiryDate' =>  $userListdata['UserSubscriptionLog']['subscription_expiry_date'],
		    	'SubscriptionType' =>  $userListdata['UserSubscriptionLog']['subscribe_type'],
		    	'status' =>  $userListdata['EnterpriseUserList']['status'],
					'cust_class' => $cust_class
		    );
		}

		$total = $this->request->params;
		$tCount = $total['paging']['User']['count'];

		$data['user_list'] = $newArr;

		$this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
		$this->render('/Elements/home/dashboard_ajax');

	}

	public function getUserProfile(){
		$this->autoRender = false;
        $responseData = array();
        if($this->request->is('post')) {
        	$userId = $this->request->data['user_id'];
            $options = array(
            	'conditions' => array(
            		'UserDutyLog.user_id' => $userId,
            		'User.status' => 1,
            		'User.approved' =>1
            	),
            	'joins' => array(
            		array(
						'table' => 'user_duty_logs',
						'alias' => 'UserDutyLog',
						'type' => 'left',
						'conditions'=> array('User.id = UserDutyLog.user_id')
					),
            		array(
						'table' => 'user_profiles',
						'alias' => 'userProfiles',
						'type' => 'left',
						'conditions'=> array('userProfiles.user_id = User.id')
					),
					array(
						'table' => 'professions',
						'alias' => 'Professions',
						'type' => 'left',
						'conditions'=> array('userProfiles.profession_id = Professions.id')
					),
            	),
            	'fields' => array('UserDutyLog.atwork_status, UserDutyLog.status,userProfiles.user_id,Professions.profession_type,User.email,userProfiles.first_name,userProfiles.last_name ,userProfiles.profile_img,userProfiles.role_status,userProfiles.profession_id,userProfiles.contact_no'),
            );
            $userData = $this->User->find('first',$options);

            if(!empty($userData)){
	            $status = '';

	            if($userData['UserDutyLog']['status'] == 1){
	            	$status .= 'OnCall, ';
	            }

	            if(trim($userData['userProfiles']['role_status']) == ''){
	            	$status .= $userData['Professions']['profession_type'];
	            }else{
	            	$status .= $userData['userProfiles']['role_status'];
	            }


	            //** Get user role tags[START] // Discuss with Mohit, Uday
	            $roleTagsArr = array();
	            $roleTagsVals = array();
	            $roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $userData['userProfiles']['profession_id'])));

	            if(!empty($roleTags['Profession']['tags'])){
	            	$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
	            }
	            $tagVal = '';
	            if(!empty($roleTagsVals)){
	            	foreach($roleTagsVals as $rt){
	            		$values = array(); $roleTagId = 0;
	            		$paramtrs['user_id'] = $userId;
	            		$paramtrs['key'] = $rt;
	            		$roleTagUser = $this->RoleTag->userRoleTags($paramtrs);
	            		if(!empty($roleTagUser)){
	            			$roleTagId = $roleTagUser[0]['rt']['id'];
	            			$values = array($roleTagUser[0]['rt']['value']);
	            			if(isset($roleTagUser[0]['rt']['value']) && trim($roleTagUser[0]['rt']['value']) != ''){
											$str = explode("$",$roleTagUser[0]['rt']['value']);
											$rolename = $str[0];
	            				$status .= ', '.$rolename;
	            			}
	            		}
	            		$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
	            	}
	            }
	            $userData['role_tags'] = $roleTagsArr;
							$cust_class = 'notAtWork';
							if((string)$userData['UserDutyLog']['status'] == '1'){
								$cust_class = 'onCallMain';
							}else if((string)$userData['UserDutyLog']['atwork_status'] == '0'){
								$cust_class = 'notAtWork';
							}else{
								$cust_class = '';
							}

	            $profile = array(
	            	'id' =>  $userData['userProfiles']['user_id'],
	            	'email' =>  $userData['User']['email'],
	            	'phone' =>  $userData['userProfiles']['contact_no'],
	            	'UserName' => $userData['userProfiles']['first_name'].' '.$userData['userProfiles']['last_name'],
	            	'profile_img' => isset($userData['userProfiles']['profile_img'])?AMAZON_PATH.$userData['userProfiles']['user_id'].'/profile/'.$userData['userProfiles']['profile_img']:'',
	            	'role_status' => $status,
	            	'AtWorkStatus' =>  $userData['UserDutyLog']['atwork_status'],
	            	'OnCallStatus' =>  $userData['UserDutyLog']['status'],
								'cust_class' => $cust_class
	            );
	            $responseData['data'] = $profile;
		    	$responseData['status'] = 1;
	            $responseData['message'] = 'ok.';
	            echo json_encode($responseData);

	            try{
		            // Admin activity logs [START]
		            $activityData = array();
		            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		            $activityData['admin_id'] = $_COOKIE['adminUserId'];
		            $activityData['user_id'] = $userData['userProfiles']['user_id'];
		            $activityData['action'] = 'Profile';
		            $activityData['custom_data'] = 'View';
		            $this->TrustAdminActivityLog->addActivityLog($activityData);
		            // Admin activity logs [END]
	            } catch (Exception $e) {

	            }

            }else{
		    	$responseData['status'] = 0;
	            $responseData['message'] = 'User is not active.';
				echo json_encode($responseData);
            }
        }
	}

	public function getUserAvailableStatus() {
		$this->autoRender = false;
        $responseData = array();
        if($this->request->is('post')) {
            $companyId = $_COOKIE['adminInstituteId'];
            $userId = $this->request->data['user_id'];

            if( !empty($companyId) ){
                $options = array(
                	'conditions' => array(
                		'UserDutyLog.user_id' => $userId,
                		'UserDutyLog.hospital_id' => $companyId
                	),
                	'joins' => array(
                		array(
			    						'table' => 'user_duty_logs',
			    						'alias' => 'UserDutyLog',
			    						'type' => 'left',
			    						'conditions'=> array('User.id = UserDutyLog.user_id')
			    					),
                		array(
											'table' => 'user_profiles',
											'alias' => 'userProfiles',
											'type' => 'left',
											'conditions'=> array('userProfiles.user_id = User.id')
										),
                		array(
											'table' => 'user_dnd_statuses',
											'alias' => 'UserDndStatus',
											'type' => 'left',
											'conditions'=> array('UserDndStatus.user_id = User.id')
										)
                	),
                	'fields' => array('UserDutyLog.atwork_status, UserDutyLog.status,userProfiles.user_id,userProfiles.profession_id,UserDndStatus.is_active'),
                );
                $userAvailableData = $this->User->find('first',$options);

								$batonCount = $this->UserBatonRole->find('all',array(
									'conditions'=>array(
										'UserBatonRole.from_user'=> $userId ,
										'UserBatonRole.status' => array(1,2,5,7),
										'UserBatonRole.is_active' => array(1),
									),
									'joins'=>array(
										array(
									    'table' => 'departments_baton_roles',
									    'alias' => 'DepartmentsBatonRole',
									    'type' => 'left',
									    'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
									  ),
									),
									'fields'=>array('DepartmentsBatonRole.on_call_value,UserBatonRole.role_id,UserBatonRole.status'),
									'group'=> 'UserBatonRole.role_id'
								));

								$onCallBaton = 0;
								foreach ($batonCount as $bat_value) {
									if($bat_value['DepartmentsBatonRole']['on_call_value'] == 1){
										$onCallBaton++;
									}
								}

                $user_data = array(
                	'at_work' => $userAvailableData['UserDutyLog']['atwork_status'],
                	'on_call' => $userAvailableData['UserDutyLog']['status'],
                	'dnd' => isset($userAvailableData['UserDndStatus']['is_active']) ? $userAvailableData['UserDndStatus']['is_active'] : 0,
                	'profession_id' => $userAvailableData['userProfiles']['profession_id'],
									'On_call_batons' => $onCallBaton,
									'total_baton' => count($batonCount)
                );

		    	$responseData['data'] = $user_data;
		    	$responseData['status'] = 1;
	            $responseData['message'] = 'ok.';
				echo json_encode($responseData);

	            try {
	            	// Admin activity logs [START]
	            	$activityData = array();
	            	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
	            	$activityData['admin_id'] = $_COOKIE['adminUserId'];
	            	$activityData['user_id'] = $userId;
	            	$activityData['action'] = 'Available/Oncall Status (View)';
	            	$activityData['custom_data'] = '{"at_work":"'.$userAvailableData['UserDutyLog']['atwork_status'].'","on_call":"'.$userAvailableData['UserDutyLog']['status'].'"}';
	            	$this->TrustAdminActivityLog->addActivityLog($activityData);
	            	// Admin activity logs [END]
	            } catch (Exception $e) {

	            }

            }else{
		    	$responseData['status'] = 0;
	            $responseData['message'] = 'Empty Email Or Company Id.';
				echo json_encode($responseData);
            }
        }else{
	    	$responseData['status'] = 0;
            $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
        }
	}

	public function privilegeList() {
		$this->checkAdminRole();
		$tCount = 1;

		// Admin activity logs [START]
		$activityData = array();
		$activityData['company_id'] = $_COOKIE['adminInstituteId'];
		$activityData['admin_id'] = $_COOKIE['adminUserId'];
		$activityData['action'] = 'Admin Team';
		$activityData['custom_data'] = 'View';
		$this->TrustAdminActivityLog->addActivityLog($activityData);
		// Admin activity logs [END]

		$this->set(array('tCount'=>$tCount));
	}

	public function privilegeListFilter() {
		$companyId = $_COOKIE['adminInstituteId'];

		if(isset($this->data['search_text'])){
		    $text=$this->data['search_text'];
		    // $search_condition = array('OR'=>array('LOWER(AdminUser.email) LIKE'=>strtolower('%'.$text.'%'),'LOWER(AdminUser.username) LIKE'=>strtolower('%'.$text.'%')));

		    $search_condition = array('OR'=>array('LOWER(AdminUser.email) LIKE'=>strtolower('%'.$text.'%'),'LOWER(AdminUser.username) LIKE'=>strtolower('%'.$text.'%'),"LOWER(CONCAT(userProfiles.first_name, ' ', userProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%')));
		}
		else{
		    $search_condition = array();
		}

		if(isset($this->data['page_number'])){
		    $page_number=$this->data['page_number'];
		}
		else{
		    $page_number=1;
		}

		$admin_condition = array();
		if(isset($this->data['type'])){
	    $type = $this->data['type'];
			if($type != 'all' && $type != ''){
				$admin_condition = array(
					'AdminUser.role_id' => $type
				);
			}
		}

		if(isset($this->data['limit'])){
		    $limit=$this->data['limit'];
		}
		else{
		    $limit=20;
		}

		$fields = array('
				AdminUser.email,
				AdminUser.added_date,
				AdminUser.status,
				AdminUser.role_id,
				User.id,
				userProfiles.first_name,
				userProfiles.last_name
			');
		$conditions = array(
				'AdminUser.company_id' => $companyId,
				'AdminUser.status' => 1,
				'User.status' => 1,
				'User.approved' => 1
			);
		$conditions_final = array_merge($conditions,$search_condition,$admin_condition);
		$joins = array(
			array(
				'table' => 'users',
				'alias' => 'User',
				'type' => 'left',
				'conditions'=> array('AdminUser.email = User.email')
			),
			array(
				'table' => 'user_profiles',
				'alias' => 'userProfiles',
				'type' => 'left',
				'conditions'=> array('userProfiles.user_id = User.id')
			),
		);

		$options = array(
			'conditions' => $conditions_final,
			'joins' => $joins,
			'fields' => $fields,
			'limit' => $limit,
			'page'=> $page_number,
			'group' => array('AdminUser.email'),
			'order' => 'userProfiles.first_name ASC',
		);

		$this->Paginator->settings = $options;
		$userList = $this->Paginator->paginate('AdminUser');
		$newArr = array();

		$total = $this->request->params;
		$tCount = $total['paging']['AdminUser']['count'];

		$data['user_list'] = $userList;

		$this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
		$this->render('/Elements/home/admin_ajax');
	}

	public function getAdminCounts(){
		$this->autoRender = false;
		if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){

			$companyId = $_COOKIE['adminInstituteId'];

			if(isset($this->data['search_text'])){
		    $text=$this->data['search_text'];
		    $search_condition = array('OR'=>array('LOWER(AdminUser.email) LIKE'=>strtolower('%'.$text.'%'),'LOWER(AdminUser.username) LIKE'=>strtolower('%'.$text.'%'),"LOWER(CONCAT(userProfiles.first_name, ' ', userProfiles.last_name)) LIKE"=>strtolower('%'.$text.'%')));
			}else{
		    $search_condition = array();
			}

			$conditions = array(
				'AdminUser.company_id' => $companyId,
				'AdminUser.status' => 1,
				'User.status' => 1,
				'User.approved' => 1
			);

			$condition_all_users = array_merge($search_condition, $conditions);
			$condition_all_admins = array_merge($search_condition, $conditions, array('AdminUser.role_id' => 0));
			$condition_all_subadmins = array_merge($search_condition, $conditions, array('AdminUser.role_id' => 1));
			$condition_all_switch_boards = array_merge($search_condition, $conditions, array('AdminUser.role_id' => 3));

			$joins = array(
				array(
					'table' => 'users',
					'alias' => 'User',
					'type' => 'left',
					'conditions'=> array('AdminUser.email = User.email')
				),
				array(
					'table' => 'user_profiles',
					'alias' => 'userProfiles',
					'type' => 'left',
					'conditions'=> array('userProfiles.user_id = User.id')
				)
			);

			$data['all_users'] = (int)$this->AdminUser->find('count',array(
				'conditions' => $condition_all_users,
				'joins' => $joins,
				'group' => array('AdminUser.email')
			));

			$data['all_admins'] = (int)$this->AdminUser->find('count',array(
				'conditions' => $condition_all_admins,
				'joins' => $joins,
				'group' => array('AdminUser.email')
			));
			$data['all_subadmins'] = (int)$this->AdminUser->find('count',array(
				'conditions' => $condition_all_subadmins,
				'joins' => $joins,
				'group' => array('AdminUser.email')
			));
			$data['all_switchboards'] = (int)$this->AdminUser->find('count',array(
				'conditions' => $condition_all_switch_boards,
				'joins' => $joins,
				'group' => array('AdminUser.email')
			));

			return json_encode($data);
		}
	}

	public function getUserPrivilegeStatus() {
		$this->autoRender = false;
        $responseData = array();
        if($this->request->is('post')) {
            $companyId = $_COOKIE['adminInstituteId'];
            $userId = $this->request->data['user_id'];
            $email = $this->request->data['user_email'];

            if( !empty($companyId) ){
              $options = array(
              	'conditions' => array(
              		'AdminUser.email' => $email,
              		'AdminUser.company_id' => $companyId,
              		'AdminUser.status' => 1
              	),
              	'fields' => array('AdminUser.role_id'),
              );
              $userAvailableData = $this->AdminUser->find('first',$options);

              if(!empty($userAvailableData)){
              	$user_data = array(
              		'role_id' => $userAvailableData['AdminUser']['role_id'],
              	);
              }else{
              	$user_data = array(
              		'role_id' => 2,
              	);
              }

			    	$responseData['data'] = $user_data;
			    	$responseData['status'] = 1;
	          $responseData['message'] = 'ok.';
	          echo json_encode($responseData);

	          try{
	            // Admin activity logs [START]
	            $activityData = array();
	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	            $activityData['user_id'] = $userId;
	            $activityData['action'] = 'Privilege Status';
	            $activityData['custom_data'] = 'View';
	            $this->TrustAdminActivityLog->addActivityLog($activityData);
	            // Admin activity logs [END]
	         	} catch (Exception $e) {

	         	}

	        }else{
	    			$responseData['status'] = 0;
	          $responseData['message'] = 'Empty Email Or Company Id.';
						echo json_encode($responseData);
	        }
	      }else{
		    	$responseData['status'] = 0;
	        $responseData['message'] = 'Information not provided.';
					echo json_encode($responseData);
	    }
	}


	public function setUserPrivilege() {
		$this->autoRender = false;
        $responseData = array();
        if($this->request->is('post')) {
            $companyId = $_COOKIE['adminInstituteId'];
            $adminEmail = $_COOKIE['adminEmail'];
            $email = $this->request->data['email'];
            $role = $this->request->data['role_id'];
            if($email == $adminEmail){
            	$responseData['status'] = 0;
	            $responseData['message'] = 'You can not perform this action. Please contact to support@medicbleep.com.';
							echo json_encode($responseData);
            }else if( !empty($companyId) && !empty($email) ){

                $adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $email)));
                $mail_status = 0;
                if(empty($adminUserData)){
                	if((string)$role != '2'){
                		$data = array(
                		    'username' => $email,
                		    'password' => md5(rand()),
                		    'email' => $email,
                		    'status' =>1,
                		    'role_id' => $role,
                		    'company_id' =>$companyId,
                		    'added_date' => date("Y-m-d H:i:s")
                		);
                		$this->AdminUser->saveAll($data);
                	}
                }else{
                	if((string)$role != '2'){
                		$updateData = array(
            		                    'role_id' => $role,
            		                    'status' => 1
            		                );
                	}else{
                		$updateData = array(
                		                    'status' => 0
                		                );
                	}
                	$updateConditions = array("username"=> $email);
                	$updateAdminUserMapping = $this->AdminUser->updateAll($updateData, $updateConditions);
                }

		    	$responseData['status'] = 1;
	            $responseData['message'] = 'ok.';
	            echo json_encode($responseData);

	            try{
		            // Admin activity logs [START]
		            $userData_cust = $this->User->find('first',array('conditions'=>array('email'=>$email)));
		            $activityData = array();
		            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		            $activityData['admin_id'] = $_COOKIE['adminUserId'];
		            $activityData['user_id'] = $userData_cust['User']['id'];
		            $activityData['action'] = 'Privilege Status Change';
		            $activityData['custom_data'] = '{"role_id" : "'.$role.'"}';
		            $this->TrustAdminActivityLog->addActivityLog($activityData);
		            // Admin activity logs [END]
		            $this->roleUpdationMail($email,$role);
	            } catch (Exception $e) {

	            }

            }else{
		    	$responseData['status'] = 0;
	            $responseData['message'] = 'Empty Email Or Company Id.';
				echo json_encode($responseData);
            }
        }else{
	    	$responseData['status'] = 0;
            $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
        }
	}

	public function updateAtWorkStatus() {
		$this->autoRender = false;
        $responseData = array();
        $customData = array();
        $is_import = "true";

        if($this->request->is('post')) {

            $userId = $this->request->data['user_id'];
            $atWorkStatus = $this->request->data['status'];
            $companyId = $_COOKIE['adminInstituteId'];

            $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId, 'User.status'=>1, 'User.approved'=>1)));
            $dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userId)));

            if(!empty($userData['User']['email'])){

            	$oneTimeToken = $this->InsCommon->genrateRandomToken($userId);
            	$oneTimeTokenData = array("user_id"=> $userId, "token"=> $oneTimeToken);
            	$this->UserOneTimeToken->save($oneTimeTokenData);

                $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], $oneTimeToken);

                $token = $tokenDetails->session->token;
                $user_id = $tokenDetails->session->user_id;
                $user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);

                $custom_data_from_api = json_decode($user_details->user->custom_data);

                $customData['userRoleStatus'] = $custom_data_from_api->status;
                $customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
                $customData['isImport'] = $is_import;
                $customData['at_work'] = (isset($atWorkStatus) ? (string) $atWorkStatus : "0");
                $customData['on_call'] = "0";

                if(!empty($token)){
                    $qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id, $customData);

                    $res = json_decode(json_encode($qbResponce));

                    if(!empty($res['errors'])){
                    	$responseData['status'] = 0;
	    	            $responseData['message'] = 'Some technical error occurred. Please try again.';
	    				echo json_encode($responseData);

                        // $responseData = array('method_name'=> 'updateAtWorkStatus', 'status'=>"0", 'response_code'=> "Some technical error occurred. Please try again", 'message'=> 'Some technical error occurred. Please try again', 'system_errors'=>$qbResponce);
                    }else{

                        if($atWorkStatus == 0){
                            if(!empty($dutyLogData)){
                                $this->UserDutyLog->updateAll(array("atwork_status"=> $atWorkStatus, "status"=> $atWorkStatus), array("user_id"=> $userId));
                            }else{
                                $dutyData = array("atwork_status"=> $atWorkStatus, "status"=> $atWorkStatus, "user_id"=> $userId);
                                $this->UserDutyLog->save($dutyData);
                            }
                            $transactionData[] = array('user_id'=>$userId,'company_id'=>$companyId,'type'=>'Available','status'=>$atWorkStatus,'device_type'=>'Institution_pannel','platform'=>'web');
                            $transactionData[] = array('user_id'=>$userId,'company_id'=>$companyId,'type'=>'OnCall','status'=>$atWorkStatus,'device_type'=>'Institution_pannel','platform'=>'web');
                        }else{
                            if(!empty($dutyLogData)){
                                $this->UserDutyLog->updateAll(array("atwork_status"=> $atWorkStatus), array("user_id"=> $userId));
                            }else{
                                $oncallData = array("atwork_status"=> $atWorkStatus, "user_id"=> $userId);
                                $this->UserDutyLog->save($oncallData);
                            }
                            $transactionData = array('user_id'=>$userId,'company_id'=>$companyId,'type'=>'Available','status'=>$atWorkStatus,'device_type'=>'Institution_pannel','platform'=>'web');
                        }
                        $this->AvailableAndOncallTransaction->saveAll($transactionData);

                        $userData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userId)));


                        //***************** Update DynamicDirectoy Cache[START] ****************//
                        $updateCachevalue = $this->updateCacheOnStatusChange($userId, $companyId);
                        //***************** Update DynamicDirectoy Cache[END] ****************//

                        $user_data = array(
                        	'at_work' => $userData['UserDutyLog']['atwork_status'],
                        	'on_call' => $userData['UserDutyLog']['status'],
                        );

        		    	$responseData['data'] = $user_data;

                        $responseData['status'] = 1;
	    	            $responseData['message'] = 'Status changed successfully.';
	    	            echo json_encode($responseData);
	    	            try{
		    	            // Admin activity logs [START]
		    	            $activityData = array();
		    	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		    	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
		    	            $activityData['user_id'] = $userId;
		    	            $activityData['action'] = 'Available Status Update';
		    	            $activityData['custom_data'] = json_encode($user_data);
		    	            $this->TrustAdminActivityLog->addActivityLog($activityData);
		    	            // Admin activity logs [END]
		    	        } catch (Exception $e) {

		    	        }

                       // $responseData = array('method_name'=> 'updateAtWorkStatus', 'status'=>"1", 'Status changed successfully'=> "200", 'message'=> 'Status changed successfully', 'data' => $userData, 'check'=>$updateCachevalue);
                    }

                }else{
                	$responseData['status'] = 0;
    	            $responseData['message'] = 'Some technical error occurred. Please try again.';
    				echo json_encode($responseData);

                    // $responseData = array('method_name'=> 'updateAtWorkStatus', 'status'=>"0", 'response_code'=> "Some technical error occurred. Please try again", 'message'=> 'Some technical error occurred. Please try again', 'system_errors'=>$qbResponce);
                }

            }else{
            	$responseData['status'] = 0;
	            $responseData['message'] = 'User is Inactive.';
				echo json_encode($responseData);

                // $responseData = array('method_name'=> 'updateAtWorkStatus', 'status'=>"0", 'response_code'=> "Login Credentials Blank", 'message'=> 'Login Credentials Blank');
            }
        }else{
        	$responseData['status'] = 0;
            $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);

            // $responseData = array('method_name'=> 'updateAtWorkStatus', 'status'=>"0", 'response_code'=> "Information not provided by app", 'message'=> 'Information not provided by app');
        }
	}

	public function updateOnCallStatus() {
		$this->autoRender = false;
    $responseData = array();
    $customData = array();
    $is_import = "true";
    $sendPush = 0;
    if($this->request->is('post')) {

        $companyId = $_COOKIE['adminInstituteId'];
        $userId = $this->request->data['user_id'];
        $onCallStatus = $this->request->data['on_status'];
        $atWorkStatus = $this->request->data['at_status'];

        $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId, 'User.status'=>1, 'User.approved'=>1)));
        $dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userId)));
        if(!empty($userData['User']['email'])){

        	$oneTimeToken = $this->InsCommon->genrateRandomToken($userId);
        	$oneTimeTokenData = array("user_id"=> $userId, "token"=> $oneTimeToken);
        	$this->UserOneTimeToken->save($oneTimeTokenData);

            $qbTokenRequest = $this->Quickblox->quickLogin($userData['User']['email'], $oneTimeToken);
            if(!empty($qbTokenRequest)){
                $token = $qbTokenRequest->session->token;
                $qbUserId = $qbTokenRequest->session->user_id;
                $qbUserDetailRequest = $this->Quickblox->getuserDetailsbyId($token,$qbUserId);

                $qbUserDetailResponce = json_decode(json_encode($qbUserDetailRequest));
                $res = 1;
                if($res == 1){
                    $custom_data_from_api = json_decode($qbUserDetailResponce->user->custom_data);
                    $customData['userRoleStatus'] = $custom_data_from_api->status;
                    $customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
                    $customData['isImport'] = $is_import;
                    // $customData['at_work'] = $custom_data_from_api->at_work;
										$customData['dnd_status'] = isset($custom_data_from_api->dnd_status) ? $custom_data_from_api->dnd_status : [];
                    $customData['at_work'] = (isset($atWorkStatus) ? (string) $atWorkStatus : $custom_data_from_api->at_work);
                    $customData['on_call'] = (isset($onCallStatus) ? (string) $onCallStatus : "0");
										if(isset($custom_data_from_api->dnd_status))
										{
											if(isset($atWorkStatus) && $atWorkStatus == '0'){
												$params['user_id'] = $userId;
												$this->dissmissDndOnOcrAndQb($params,'DND dismiss by ADMIN: User Status Changed');
											}else{
													$customData['is_dnd_active'] = 1;
											}
										}
                    $qbUpdateRequest = $this->Quickblox->updateCustomDataOnQbLite($token, $qbUserId,$customData);
                    $qbUpdateResponce = json_decode(json_encode($qbUpdateRequest));
                    if(empty($qbUpdateResponce['errors'])){

                    	if(!empty($dutyLogData)){
                    	    $this->UserDutyLog->updateAll(array("atwork_status"=> $atWorkStatus), array("user_id"=> $userId));
                    	}else{
                    	    $oncallData = array("atwork_status"=> $atWorkStatus, "user_id"=> $userId);
                    	    $this->UserDutyLog->save($oncallData);
                    	}

											$this->UserDutyLog->updateAll(array("modified"=> "'".date('Y-m-d H:i:s')."'", "status"=> $onCallStatus), array("user_id"=> $userId, "hospital_id"=>$companyId));
											$transactionData = array();
                    	if((string)$custom_data_from_api->on_call != (string)$onCallStatus){
													$transactionData[] = array('user_id'=>$userId,'company_id'=>$companyId,'type'=>'OnCall','status'=>$onCallStatus,'device_type'=>'Institution_pannel','platform'=>'web');
											}
                    	if((string)$custom_data_from_api->at_work != (string)$atWorkStatus){
                    		$sendPush = "1";
                    			$transactionData[] = array('user_id'=>$userId,'company_id'=>$companyId,'type'=>'Available','status'=>$atWorkStatus,'device_type'=>'Institution_pannel','platform'=>'web');
                    	}

											if(!empty($transactionData)){
												$this->AvailableAndOncallTransaction->saveAll($transactionData);
											}



                        $userData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userId)));

                        //***************** Update DynamicDirectoy Cache[START] ****************//
                        $updateCachevalue = $this->updateCacheOnStatusChange($userId, $companyId);
                        //***************** Update DynamicDirectoy Cache[END] ****************//

                        $user_data = array(
                        	'at_work' => $userData['UserDutyLog']['atwork_status'],
                        	'on_call' => $userData['UserDutyLog']['status'],
                        );

        		    				$responseData['data'] = $user_data;

                        $responseData['status'] = 1;
    	    	            $responseData['message'] = 'Status changed successfully.';
    	    	            echo json_encode($responseData);

												try{
	    	    	            // Admin activity logs [START]
	    	    	            $activityData = array();
	    	    	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	    	    	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	    	    	            $activityData['user_id'] = $userId;
	    	    	            $activityData['action'] = 'User Status Updated';
	    	    	            $activityData['custom_data'] = json_encode($user_data);
	    	    	            $this->TrustAdminActivityLog->addActivityLog($activityData);
	    	    	            // Admin activity logs [END]
    	    	            } catch (Exception $e) {

    	    	            }
  	    	            	if($sendPush == '1'){

													if($atWorkStatus == 0){
														$params['user_id'] = $userId;
														$params['is_dnd_active'] = $atWorkStatus;
														$params['push_on'] = $atWorkStatus;
														$params['dnd_data'] = array('User'=>array('dnd_name' => "",'dnd_status_id' => "",'end_time' => "",'icon_selected' => ""));
														$sendVoipDNDPushNotification = $this->sendVoipPushNotification($params);
													}

													$push_params['user_id'] = $userId;
													$push_params['at_work_status'] = $atWorkStatus;
													$push_params['push_on'] = $atWorkStatus;

													$sendVoipPushNotification = $this->sendAtWorkPushNotification($push_params);
												}
                    }else{
                    	$responseData['status'] = 0;
					            $responseData['message'] = 'Some technical error occurred. Please try again.';
											echo json_encode($responseData);
                    }
                }else{
	            	$responseData['status'] = 0;
		            $responseData['message'] = 'Some technical error occurred. Please try again.';
								echo json_encode($responseData);
                }
            }else{
            	$responseData['status'] = 0;
	            $responseData['message'] = 'Some technical error occurred. Please try again.';
							echo json_encode($responseData);
            }
        }else{
        	$responseData['status'] = 0;
          $responseData['message'] = 'User is Inactive.';
					echo json_encode($responseData);
        }

    }else{
    	$responseData['status'] = 0;
      $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
    }
	}

	public function updateSubscription() {
		$this->autoRender = false;
        $responseData = array();
        if($this->request->is('post')) {

            $companyId = $_COOKIE['adminInstituteId'];

            $userEmail = $this->request->data['email'];
            $userId = $this->request->data['user_id'];
            $customDate = $this->request->data['custom_date'];

            if( !empty($userEmail) && !empty($companyId) ){
                $userData = $this->EnterpriseUserList->find('first',array('conditions'=>array('email'=> $userEmail, 'company_id'=> $companyId)));
                if(!empty($userData))
                {
                    $createdDate = date("Y-m-d H:i:s");
                    $getUserDeatail = $this->User->find('first',
                    array('conditions'=>
                      array(
                        'email'=> $userEmail
                        )
                    ));
                    if(!empty($getUserDeatail))
                    {
                        $responseUserEmployment = $this->getUserEmployment($userId,$companyId);
                        if($responseUserEmployment == 1)
                        {
                            $this->EnterpriseUserList->updateAll(array("status"=> 1), array("company_id"=> $companyId, "email"=> $userEmail));
                            $result = $this->updateUserCustomSubscription($userId,$companyId,$customDate);
                            if($result == 1){
                	        	$responseData['status'] = 1;
                	            $responseData['message'] = "User's Subscription Updated Successfully.";
                	            echo json_encode($responseData);

                	            try{
		            	            // Admin activity logs [START]
		            	            $activityData = array();
		            	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		            	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
		            	            $activityData['user_id'] = $userId;
		            	            $activityData['action'] = 'User Subscription Update';
		            	            $activityData['custom_data'] = '{"subscription_expiry_date" : "'.$customDate.'"}';
		            	            $this->TrustAdminActivityLog->addActivityLog($activityData);
		            	            // Admin activity logs [END]
		            	        } catch (Exception $e) {

		            	        }

                            }else{
                	        	$responseData['status'] = 0;
                	            $responseData['message'] = 'An error has occurred. Please try again.';
                				echo json_encode($responseData);
                            }
                        }else{
	        	        	$responseData['status'] = 0;
	        	            $responseData['message'] = 'An error has occurred. Please try again.';
	        				echo json_encode($responseData);
                        }
                    }else{
        	        	$responseData['status'] = 0;
        	            $responseData['message'] = 'An error has occurred. Please try again.';
        				echo json_encode($responseData);
                    }
                }else{
                    $this->EnterpriseUserList->save(array('email'=> $userEmail, "company_id"=> $companyId, 'status'=> 1, 'created' => $createdDate));
    	        	$responseData['status'] = 1;
    	            $responseData['message'] = "User's Subscription Updated Successfully.";
    				echo json_encode($responseData);
                }
            }else{
	        	$responseData['status'] = 0;
	            $responseData['message'] = 'Empty Email Or Company Id.';
				echo json_encode($responseData);
            }
        }else{
        	$responseData['status'] = 0;
            $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
        }
	}

	public function updateUserCustomSubscription($userId, $companyId, $customDate){
	    $result = 0;
	    $getCompanyDetail = $this->CompanyName->find('first',
			                array('conditions'=>
			                  array(
			                    'id'=> $companyId
			                    )
			                ));
	    if(!empty($getCompanyDetail)){
	      $customFormatedDate = date("Y-m-d", strtotime($customDate));
	      $newSubscriptionPeriod = $customFormatedDate." "."23:59:59";
	      $this->UserSubscriptionLog->updateAll(array("subscription_expiry_date"=>"'".$newSubscriptionPeriod."'", "subscribe_type"=> 1,"company_id"=>$companyId),array("user_id"=> $userId));
	      $result = 1;
	    }

	    return $result;
	}

	public function changeAssignStatus() {
		$this->autoRender = false;
		$responseData = array();
		$companyId = $_COOKIE['adminInstituteId'];
		$userEmail = $this->request->data['email'];
		$userId = $this->request->data['user_id'];
		$status = $this->request->data['status'];
		if(!empty($userEmail) && !empty($companyId)){
			$userData = $this->EnterpriseUserList->find('first',array('conditions'=>array('email'=> $userEmail, 'company_id'=> $companyId)));
			if(!empty($userData)){
				$createdDate = date("Y-m-d H:i:s");

				$getUserDeatail = $this->User->find('first',array('conditions'=> array('email'=> $userEmail)));

				if(!empty($getUserDeatail)){
				    $responseUserEmployment = $this->getUserEmployment($userId,$companyId);
				    if($responseUserEmployment == 1){
							$batonRoles = $this->UserBatonRole->find('count',array('conditions'=>array('from_user'=>$userId,'status'=>array(1,2,5,7),'is_active' => array(1))));
				        if($batonRoles == 0){
									$this->EnterpriseUserList->updateAll(array("status"=> $status), array("company_id"=> $companyId, "email"=> $userEmail));

					        $result = $this->updateUserSubscription($userId,$companyId,$status);
					        $updateCachevalue = $this->updateLastModifiedUserCache($userId, $status, $companyId);
					        if($result == 1){
					        	$adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $userEmail,'AdminUser.status'=>1)));
					        	if(!empty($adminUserData) && $status != 1){
					        	    $updateData = array(
					        	                    'status' => 0
					        	                );
					        	    $updateConditions = array("username"=> $userEmail);
					        	    $updateAdminUserMapping = $this->AdminUser->updateAll($updateData, $updateConditions);
					        	    $this->roleUpdationMail($userEmail,2);
					        	}
					        	if($status == 1){
					        		$responseData['message'] = 'User Approved Successfully';
					        	}else{
					        		$responseData['message'] = 'User Unapproved Successfully';
					        	}
	    				    	$responseData['status'] = 1;
	    	    				echo json_encode($responseData);

										try {
											if($status == 0){
												$params['user_id'] = $userId;
												$this->dissmissDndOnOcrAndQb($params,'DND dismiss by ADMIN: '.$responseData['message']);
											}
										} catch (Exception $eDND) {

										}

	    	    				try{
		    				    	// Admin activity logs [START]
		    				    	$activityData = array();
		    				    	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
		    				    	$activityData['admin_id'] = $_COOKIE['adminUserId'];
		    				    	$activityData['user_id'] = $userId;
		    				    	$activityData['action'] = $responseData['message'];
		    				    	$this->TrustAdminActivityLog->addActivityLog($activityData);
		    				    	// Admin activity logs [END]
	    				    	} catch (Exception $e) {

	    				    	}
					        }else{
	    				    	$responseData['status'] = 0;
	    	    	            $responseData['message'] = 'An error has occurred. Please try again.';
	    	    				echo json_encode($responseData);
					        }
								}else{
									$responseData['status'] = 2;
												$responseData['message'] = "Can't change the Approved Status as user already having active Baton Role(s).";
									echo json_encode($responseData);
								}
				    }else{
				    	$responseData['status'] = 0;
	    	            $responseData['message'] = 'An error has occurred. Please try again.';
	    				echo json_encode($responseData);
				    }
				}else{
					$responseData['status'] = 0;
    	            $responseData['message'] = 'An error has occurred. Please try again.';
    				echo json_encode($responseData);
				}
			}
		}
	}

	public function changeBulkAssignStatus() {
		$this->autoRender = false;
		$responseData = array();
		$companyId = $_COOKIE['adminInstituteId'];
		$user_data = json_decode($this->request->data['user_data']);
		foreach ($user_data as $user) {
		    $userEmail = $user->email;
		    $userId = $user->user_id;
		    $status = $user->status;
    		if(!empty($userEmail) && !empty($companyId)){
    			$userData = $this->EnterpriseUserList->find('first',array('conditions'=>array('email'=> $userEmail, 'company_id'=> $companyId)));
    			if(!empty($userData)){
    				$createdDate = date("Y-m-d H:i:s");
    				$getUserDeatail = $this->User->find('first',
    				array('conditions'=>
    				  array(
    				    'email'=> $userEmail
    				    )
    				));
    				if(!empty($getUserDeatail))
    				{
    				    $responseUserEmployment = $this->getUserEmployment($userId,$companyId);
    				    if($responseUserEmployment == 1)
    				    {
    				        $this->EnterpriseUserList->updateAll(array("status"=> $status), array("company_id"=> $companyId, "email"=> $userEmail));

    				        $result = $this->updateUserSubscription($userId,$companyId,$status);
    				        $updateCachevalue = $this->updateLastModifiedUserCache($userId, $status, $companyId);
    				        if($result == 1){
    				        	$adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $userEmail,'AdminUser.status'=>1)));
    				        	if(!empty($adminUserData) && $status != 1){
    				        	    $updateData = array(
    				        	                    'status' => 0
    				        	                );
    				        	    $updateConditions = array("username"=> $userEmail);
    				        	    $updateAdminUserMapping = $this->AdminUser->updateAll($updateData, $updateConditions);
    				        	    $this->roleUpdationMail($userEmail,2);
    				        	}
    				        	if($status == 1){
    				        		$responseData['message'] = 'User Approved Successfully';
    				        	}else{
    				        		$responseData['message'] = 'User Unapproved Successfully';
    				        	}

											try {
												if($status == 0){
													$params['user_id'] = $userId;
													$this->dissmissDndOnOcrAndQb($params,'DND dismiss by ADMIN: '.$responseData['message']);
												}
											} catch (Exception $eDND) {

											}

    				        	try{
	    				        	// Admin activity logs [START]
	    				        	$activityData = array();
	    				        	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
	    				        	$activityData['admin_id'] = $_COOKIE['adminUserId'];
	    				        	$activityData['user_id'] = $userId;
	    				        	$activityData['action'] = $responseData['message'];
	    				        	$activityData['custom_data'] = 'Bulk Update';
	    				        	$this->TrustAdminActivityLog->addActivityLog($activityData);
	    				        	// Admin activity logs [END]
    				        	} catch (Exception $e) {

    				        	}

        				    	$responseData['status'] = 1;
    				        }else{
        				    	$responseData['status'] = 0;
        	    	            $responseData['message'] = 'An error has occurred. Please try again.';
    				        }
    				    }else{
    				    	$responseData['status'] = 0;
    	    	            $responseData['message'] = 'An error has occurred. Please try again.';
    				    }
    				}else{
    					$responseData['status'] = 0;
        	            $responseData['message'] = 'An error has occurred. Please try again.';
    				}
    			}
    		}
		}
		echo json_encode($responseData);
	}

	public function getUserEmployment($userId, $companyId){
	    $result = 0;
	    $getUserEmploymentData = $this->UserEmployment->find('first',
	                array('conditions'=>
	                  array(
	                    'user_id'=> $userId,
	                    'company_id' => $companyId,
	                    'is_current' => 1
	                    )
	                ));
	    if(!empty($getUserEmploymentData)){
	      $result = 1;
	    }
	    return $result;
	}

	public function updateUserSubscription($userId, $companyId, $status){
	    $result = 0;
	    $getCompanyDetail = $this->CompanyName->find('first',
	                array('conditions'=>
	                  array(
	                    'id'=> $companyId
	                    )
	                ));
	    if(!empty($getCompanyDetail)){

	    	if($status == 1){
	    		$newSubscriptionPeriod = date('Y-m-d H:i:s', strtotime($getCompanyDetail['CompanyName']['subscription_expiry_date']));
	        }else{
	        	// $newSubscriptionPeriod = date("Y-m-d", strtotime("+1 month"));
	        	$newSubscriptionPeriod = date("Y-m-d", strtotime("0 day"));
	        	$status == 3;
	        }
	      	$this->UserSubscriptionLog->updateAll(array("subscription_expiry_date"=>"'".$newSubscriptionPeriod."'", "subscribe_type"=>$status,"company_id"=>$companyId),array("user_id"=> $userId));
	      	$result = 1;
	    }

	    return $result;
	}

	public function updateLastModifiedUserCache($userId, $deletedStatus, $companyId){
	    $modifiedType = "deleted_user";
	    $lastModified = date('Y-m-d H:i:s');
	    $deletedStatus = ($deletedStatus == 0)?1:0;
	    if(!empty($userId)){
	        $getCacheLastModifiedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType)));
	        if($getCacheLastModifiedUser > 0){
	            $this->CacheLastModifiedUser->updateDeletedUser($userId, $companyId, $modifiedType, $deletedStatus);
	        }else{
	            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType, "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }

	        //** Update static cache[START]
	        $this->CacheLastModifiedUser->updateLastModifiedDate(0, $companyId, 'static_directory_changed');
	        //** Update static cache[END]

	        //** Set eTag values for static directory[START]
	        $paramsStaticEtagUpdate['key'] = 'staticDirectoryEtag:'.$companyId;
	        $this->updateStaticEtagData($paramsStaticEtagUpdate);
	        //** Set eTag values for static directory[END]

	        //** update dynamic cache[STATRT]
	        if($deletedStatus == 0){
	            $getDynamicChangedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed")));
	            if($getDynamicChangedUser > 0){
	                $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');
	            }else{
	                $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>$lastModified);
	                $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	            }

	            //** Get last modified date dynamic directory[START]
	            $getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
	            if(!empty($getDynamicDirectoryLastModified)){
	                $paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
	                $paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
	                $this->updateDynamicEtagData($paramsDynamicEtagUpdate);
	            }
	            //** Get last modified date dynamic directory[END]
	        }
	        //** update dynamic cache[END]

	        //******* Delete Cache in case of Delete, Activate, Deactivate[START] ********//
	        $cacObj = $this->Cache->redisConn();
	        if(($cacObj['connection']) && empty($cacObj['errors'])){
	            if($cacObj['robj']->exists('institutionDirectory:'.$companyId)){
	                $key = 'institutionDirectory:'.$companyId;
	                $cacObj = $this->Cache->delRedisKey($key);
	            }
	        }
	        //******* Delete Cache in case of Delete, Activate, Deactivate[END] ********//
	  }
	}

	public function broadCastMessage() {
		$this->autoRender = false;
		$responseData = array();
		$companyId = $_COOKIE['adminInstituteId'];
		$email = $_COOKIE['adminEmail'];
		$password = $this->request->data['password'];
		$message = $this->request->data['message'];
		$serviceId = 'MG0490a00ed855239165604a73dda63967';

		$params = array();
		$params['userName'] = strtolower($email);
    $params['password'] = $password;
    $userData = $this->AdminUser->checkLogin($params);
		if(!empty($userData)){
			$conditions = array(
				'UserEmployment.company_id' => $companyId,
				'UserEmployment.is_current' => 1,
				'UserEmployment.status' => 1,
				'User.status' => 1,
				'User.approved' => 1,
				'EnterpriseUserList.status' => array(1)
			);

			$joins = array(
				array(
					'table' => 'user_employments',
					'alias' => 'UserEmployment',
					'type' => 'left',
					'conditions'=> array('UserEmployment.user_id = User.id')
				),
				array(
					'table' => 'user_qb_details',
					'alias' => 'UserQbDetail',
					'type' => 'left',
					'conditions'=> array('User.id = UserQbDetail.user_id')
				),
				array(
					'table' => 'user_profiles',
					'alias' => 'userProfiles',
					'type' => 'left',
					'conditions'=> array('userProfiles.user_id = User.id')
				),
				array(
					'table' => 'professions',
					'alias' => 'Professions',
					'type' => 'left',
					'conditions'=> array('userProfiles.profession_id = Professions.id')
				),
				array(
					'table' => 'enterprise_user_lists',
					'alias' => 'EnterpriseUserList',
					'type' => 'left',
					'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
				),
				array(
						'table' => 'voip_push_notifications',
						'alias' => 'VoipPushNotification',
						'type' => 'left',
						'conditions'=> array('User.id = VoipPushNotification.user_id')
				)
			);

			$options = array(
				'conditions' => $conditions,
				'joins' => $joins,
				'fields' => array('User.id','UserQbDetail.qb_id','VoipPushNotification.device_token','VoipPushNotification.fcm_voip_token'),
				'group' => array('UserEmployment.user_id'),
				// 'order' => array('UserLoginTransaction.id' => 'DESC')
			);

			$id_data = $this->User->find('all',$options);
			$broadcastIds = array();
			$broadcastUsers = array();
			$smsIds = array();
			if(empty($id_data)){
				$responseData['status'] = 2;
				$responseData['message'] = 'No members to send Broadcast message.';
				echo json_encode($responseData);
			}
			foreach ($id_data AS  $ids) {
				if(!empty($ids['VoipPushNotification']) && isset($ids['VoipPushNotification']['fcm_voip_token'])){
					if($ids['UserQbDetail']['qb_id'] != "" && $ids['UserQbDetail']['qb_id'] != null){
							$broadcastIds[] = $ids['UserQbDetail']['qb_id'];
							$broadcastUsers[] = $ids['User']['id'];
							$token_data[] = $ids['VoipPushNotification'];
					}
				}else{
						$smsIds[] = $ids['User']['id'];
				}
			}

			if(count($broadcastUsers) > 0){
				foreach ($token_data as $value) {
					$params = array();

					$params['notificationMessage'] = $message;
					$params['voip_tokens']['VoipPushNotification'] = $value;
					$this->sendCustomVoipPush($params);

				}
				$responseData['status'] = 1;
				$responseData['message'] = 'OK';

				$notificationSendResponse = array(
					"sender"=>$email,
					"send_to"=>serialize($broadcastUsers),
					"trust_id"=>$companyId,
					"sent_messages"=>$message,
					"type"=>"broadcast",
					"sent_from"=>"Dashboard",
					"response"=>'Broadcast sent'
				);

				try{
				 $this->NotificationBroadcastMessage->saveAll($notificationSendResponse);
				}catch(Exception $e){}
			}
// ------ Response when SMS service are not available --------
      else{
        $responseData['status'] = 2;
        $responseData['message'] = 'No members to send Broadcast message.';
        echo json_encode($responseData);
      }
// ------ Response when SMS service are not available --------

			if(count($smsIds) > 1){
			    $numbers = $this->UserProfile->find('all',array('conditions'=>array('UserProfile.user_id' => $smsIds),'fields'=>array('UserProfile.contact_no','UserProfile.user_id')));

					$deliver_status['success'] = 0;
					$deliver_status['success_users'] = array();
					$deliver_status['failure'] = 0;
					$deliver_status['failure_users'] = array();

			    foreach ($numbers as $value) {
						if($value['UserProfile']['contact_no'] != ''){
								$smsUsers[] = $value['UserProfile']['user_id'];
								$data = json_decode(json_decode($this->sendSms($value['UserProfile']['contact_no'], $message, $serviceId)));
								if($data->status == 'accepted'){
										$response = json_decode(json_decode($this->getSmsResponse($data->sid)));
										if($response->status != 'failed'){
											$deliver_status['success'] += 1;
											$deliver_status['success_users'][] = $value['UserProfile']['user_id'];
										}else{
											$deliver_status['failure'] += 1;
											$deliver_status['failure_users'][] = $value['UserProfile']['user_id'];
										}
								}else{
										$deliver_status['failure'] += 1;
										$deliver_status['failure_users'][] = $value['UserProfile']['user_id'];
								}
						}
			    }

					$messageResponse = array("sender"=>$email,"send_to"=>serialize($smsUsers),"trust_id"=>$companyId,"sent_messages"=>$message,"type"=>"message","sent_from"=>"Dashboard","deliver_status"=>json_encode($deliver_status),"response"=>"Message sent");
			    try{
			     $this->NotificationBroadcastMessage->saveAll($messageResponse);
			    }catch(Exception $e){}

			    $responseData['status'] = 1;
			    $responseData['message'] = 'OK';
			}
			echo json_encode($responseData);
			try{
				// Admin activity logs [START]
	            $activityData = array();
	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	            $activityData['action'] = 'Broadcast Message';
	            $activityData['custom_data'] = '{"company_id" :"'.$companyId.'","message":"'.$message.'"}';
	            $this->TrustAdminActivityLog->addActivityLog($activityData);
	            // Admin activity logs [END]
	        } catch (Exception $e) {

	        }

		}else{
            $responseData['status'] = 0;
            $responseData['message'] = 'Invalid Credentials.';
			echo json_encode($responseData);
		}

	}


	public function removeUser() {
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$companyId = $_COOKIE['adminInstituteId'];
		    $userEmail = $this->request->data['email'];
		    if( !empty($userEmail) && !empty($companyId) ){
		        $getUserDeatail = $this->User->find('first',
						            array('conditions'=>
						              	array(
						                	'email'=> $userEmail
						                )
					            	));
		        if(!empty($getUserDeatail)){
		            $responseUserEmployment = $this->getUserEmployment($getUserDeatail['User']['id'],$companyId);
		            if($responseUserEmployment == 1){
									$batonRoles = $this->UserBatonRole->find('count',array('conditions'=>array('from_user'=>$getUserDeatail['User']['id'],'status'=>array(1,2,5,7),'is_active'=>array(1))));
									if($batonRoles == 0){
										$this->EnterpriseUserList->updateAll(array("status"=> 2), array("company_id"=> $companyId, "email"=> $userEmail));
		                $result = $this->removeUserSubscription($getUserDeatail['User']['id'],$companyId);
		                $updateCachevalue = $this->updateLastModifiedUserCache($getUserDeatail['User']['id'], 0, $companyId);
		                if($result == 1){

		                	$adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $userEmail,'AdminUser.status'=>1)));
		                	if(!empty($adminUserData)){
		                	    $updateData = array(
		                	                    'status' => 0
		                	                );
		                	    $updateConditions = array("username"=> $userEmail);
		                	    $updateAdminUserMapping = $this->AdminUser->updateAll($updateData, $updateConditions);
		                	    $this->roleUpdationMail($userEmail,2);
		                	}

		                    $this->UserEmployment->updateAll(array("company_id"=> -1), array("is_current"=> 1, "status"=> 1, "user_id" => $getUserDeatail['User']['id']));
                            $responseData['status'] = 1;
                            $responseData['message'] = 'User removed successfully.';
                			echo json_encode($responseData);

	            			try{
		            			// Admin activity logs [START]
		                        $activityData = array();
		                        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		                        $activityData['admin_id'] = $_COOKIE['adminUserId'];
		                        $activityData['user_id'] = $getUserDeatail['User']['id'];
		                        $activityData['action'] = 'Remove User';
		                        $this->TrustAdminActivityLog->addActivityLog($activityData);
		                        // Admin activity logs [END]
		                    } catch (Exception $e) {

		                    }
		                    try {
												    $params['user_id'] = $getUserDeatail['User']['id'];
												    $this->dissmissDndOnOcrAndQb($params,'DND dismiss by ADMIN: '.$responseData['message']);

												} catch (Exception $eDND) {

												}
		                }else{
		                    $this->EnterpriseUserList->updateAll(array("status"=> 2), array("company_id"=> $companyId, "email"=> $userEmail));
		                    $responseData['status'] = 1;
                            $responseData['message'] = 'User removed successfully.';
                			echo json_encode($responseData);

                			try{
	                			// Admin activity logs [START]
	                            $activityData = array();
	                            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	                            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	                            $activityData['action'] = 'Remove User';
	                            $activityData['custom_data'] = '{"email" :"'.$userEmail.'"}';
	                            $this->TrustAdminActivityLog->addActivityLog($activityData);
	                            // Admin activity logs [END]
	                        } catch (Exception $e) {

	                        }
	                        try {
													    $params['user_id'] = $getUserDeatail['User']['id'];
													    $this->dissmissDndOnOcrAndQb($params,'DND dismiss by ADMIN: '.$responseData['message']);

													} catch (Exception $eDND) {

													}
		                }
									}else{
										$responseData['status'] = 2;
										$responseData['message'] = "Can't remove as user already having active Baton Role(s).";
										echo json_encode($responseData);
									}
		            }else{
		                $this->EnterpriseUserList->updateAll(array("status"=> 2), array("company_id"=> $companyId, "email"=> $userEmail));
		                $responseData['status'] = 1;
                        $responseData['message'] = 'User removed successfully.';
            			echo json_encode($responseData);

            			try{
	            			// Admin activity logs [START]
	                        $activityData = array();
	                        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	                        $activityData['admin_id'] = $_COOKIE['adminUserId'];
	                        $activityData['action'] = 'Remove User';
	                        $activityData['custom_data'] = '{"email" :"'.$userEmail.'"}';
	                        $this->TrustAdminActivityLog->addActivityLog($activityData);
	                        // Admin activity logs [END]
	                    } catch (Exception $e) {

	                    }
	                    try {
											    $params['user_id'] = $getUserDeatail['User']['id'];
											    $this->dissmissDndOnOcrAndQb($params,'DND dismiss by ADMIN: '.$responseData['message']);

											} catch (Exception $eDND) {

											}
		            }
		        }else{
		            $this->EnterpriseUserList->updateAll(array("status"=> 2), array("company_id"=> $companyId, "email"=> $userEmail));
		            $responseData['status'] = 1;
                    $responseData['message'] = 'User removed successfully.';
        			echo json_encode($responseData);

                    try{
	                    // Admin activity logs [START]
	                    $activityData = array();
	                    $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	                    $activityData['admin_id'] = $_COOKIE['adminUserId'];
	                    $activityData['action'] = 'Remove User';
	                    $activityData['custom_data'] = '{"email" :"'.$userEmail.'"}';
	                    $this->TrustAdminActivityLog->addActivityLog($activityData);
	                    // Admin activity logs [END]
	                } catch (Exception $e) {

	                }
		        }
		    }else{
		    	$responseData['status'] = 0;
                $responseData['message'] = 'An error has occurred. Please try again.';
    			echo json_encode($responseData);
		    }
		}else{
	    	$responseData['status'] = 0;
            $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
		}
	}

	public function addUser(){
	    $this->autoRender = false;
		$responseData = array();
		$companyId = $_COOKIE['adminInstituteId'];

	    if($this->request->is('post')) {
	        $userEmail = strtolower($this->request->data['email']);

	        if( !empty($userEmail) && !empty($companyId) ){
	            $userData = $this->EnterpriseUserList->find('all',array('conditions'=>array('LOWER(email)'=> $userEmail, 'company_id'=>$companyId)));
	            if( !empty($userData) ){
    	            $responseData['status'] = 0;
    	            $responseData['message'] = 'Email Already Exists';
    				echo json_encode($responseData);
	            }else{
	                $createdDate = date("Y-m-d H:i:s");
	                $getUserDeatail = $this->User->find('first',
						                array('conditions'=>
						                  	array(
						                  	  	'email'=> $userEmail
						                    )
					                	));
	                if(!empty($getUserDeatail)){
	                    $responseUserEmployment = $this->getUserEmployment($getUserDeatail['User']['id'],$companyId);
	                    if($responseUserEmployment == 1){
	                        $this->EnterpriseUserList->save(array('email'=> $userEmail, "company_id"=> $companyId, 'status'=> 1, 'source'=>1, 'created' => $createdDate));
	                        $result = $this->updateUserSubscription($getUserDeatail['User']['id'],$companyId,1);
	                        if($result == 1){
                	            $responseData['status'] = 1;
                	            $responseData['message'] = 'User Added successfully.';
                				echo json_encode($responseData);

                	            try{
	                	            // Admin activity logs [START]
	                	            $activityData = array();
	                	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	                	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	                	            $activityData['action'] = 'Add User';
	                	            $activityData['custom_data'] = '{"email" :"'.$userEmail.'"}';
	                	            $this->TrustAdminActivityLog->addActivityLog($activityData);
	                	            // Admin activity logs [END]
	                	        } catch (Exception $e) {

	                	        }
	                        }else{
	                        	$responseData['status'] = 0;
                	            $responseData['message'] = 'Some Error Occured While Adding User.';
                				echo json_encode($responseData);
	                        }
	                    }else{
	                        $this->EnterpriseUserList->save(array('email'=> $userEmail, "company_id"=> $companyId, 'status'=> 1, 'source' => 1, 'created' => $createdDate));
                        	$responseData['status'] = 1;
            	            $responseData['message'] = 'User Added successfully.';
            				echo json_encode($responseData);

            	            try{
	            	            // Admin activity logs [START]
	            	            $activityData = array();
	            	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	            	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	            	            $activityData['action'] = 'Add User';
	            	            $activityData['custom_data'] = '{"email" :"'.$userEmail.'"}';
	            	            $this->TrustAdminActivityLog->addActivityLog($activityData);
	            	            // Admin activity logs [END]
	        	            } catch (Exception $e) {

	        	            }
	                    }

	                }else{
	                    $this->EnterpriseUserList->save(array('email'=> $userEmail, "company_id"=> $companyId, 'status'=> 1, 'source' => 1, 'created' => $createdDate));
                    	$responseData['status'] = 1;
        	            $responseData['message'] = 'User Added successfully.';
        				echo json_encode($responseData);

        	            try{
	        	            // Admin activity logs [START]
	        	            $activityData = array();
	        	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	        	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	        	            $activityData['action'] = 'Add User';
	        	            $activityData['custom_data'] = '{"email" :"'.$userEmail.'"}';
	        	            $this->TrustAdminActivityLog->addActivityLog($activityData);
	        	            // Admin activity logs [END]
	        	        } catch (Exception $e) {

	        	        }
	                }
	            }
	        }
	        else{
	        	$responseData['status'] = 0;
	            $responseData['message'] = 'Empty Email Or Company Id.';
				echo json_encode($responseData);
	        }

	    }
	    else{
	    	$responseData['status'] = 0;
            $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
	    }

	}

	public function addUserFileUpload(){
				App::import('Vendor','PHPExcel',array('file' => 'PHPExcel/Classes/PHPExcel.php'));
				App::import('Vendor','PHPExcel',array('file' => 'PHPExcel/Classes/IOFactory.php'));
				PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $this->autoRender = false;
        $responseData = array();
        $response = array();
        $validEmail = array();
        $invalidEmail = array();
        $createdDate = date("Y-m-d H:i:s");
        if($this->request->is('post')) {
            $paramsInput  = $this->request->params;
            $companyId = $_COOKIE['adminInstituteId'];
            $file_name  = $paramsInput['form']['emailfile']['name'];
            $file_temp  = $paramsInput['form']['emailfile']['tmp_name'];
            $file_size  = $paramsInput['form']['emailfile']['size'];
            $file_error = $paramsInput['form']['emailfile']['error'];
            $file_ext = explode('.', $file_name);
            $file_ext = strtolower(end($file_ext));
            $allowed = array('csv', 'xls', 'xlsx');
            if(in_array($file_ext, $allowed)){
                if($file_error === 0){
                    $file_new_name = uniqid('',true). '.' .$file_ext;
                    $file_destination = APP .'/webroot/files/'.$file_new_name;
                    if(move_uploaded_file($file_temp, $file_destination)){
                       $fileName = $file_destination;
                       if(!empty($fileName)){
                            $row = 1;
                            if (($handle = fopen($fileName , "r")) !== FALSE) {

															// xlsx and xls process
															if($file_ext == 'xlsx' || $file_ext == 'xls'){
																$objPHPExcel = PHPExcel_IOFactory::load($file_destination);
																$sheet = $objPHPExcel->getSheet(0);
																$highestRow = $sheet->getHighestRow();
																$highestColumn = $sheet->getHighestColumn();
																for ($row = 1; $row <= $highestRow; $row++){

																	//  Read a row of data into an array
																	$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

																	if($row == 1) continue;
																	$userEmail = strtolower($rowData[0][0]);

																	//check email exist

																	if( !empty($userEmail) && !empty($companyId) ){
																			$userData = $this->EnterpriseUserList->find('all',array('conditions'=>array('LOWER(email)'=> $userEmail, 'company_id'=>$companyId)));

																			// print_r($userData);
																			if( !empty($userData) ){
																					$invalidEmail[] = $userData[0]['EnterpriseUserList']['email'];
																			}else if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$userEmail)){
																					$invalidEmail[] = $userEmail;
																			}else{
																					//************* Here check to look user is already registered ******************//
																					//************* if registered then add to enterprise user list and update subscrition day ******************//
																					$getUserDeatail = $this->User->find('first',
																					array('conditions'=>
																						array(
																							'email'=> $userEmail
																							)
																					));
																					if(!empty($getUserDeatail)){
																							$responseUserEmployment = $this->getUserEmployment($getUserDeatail['User']['id'],$companyId);
																							if($responseUserEmployment == 1){
																									$this->EnterpriseUserList->saveAll(array('email'=> $userEmail, "company_id"=> $companyId, 'source' => 1 ,'status'=> 1, 'created' => $createdDate));
																									// $result = $this->updateUserSubscription($getUserDeatail['User']['id'],$companyId);
																							}else{
																									$this->EnterpriseUserList->saveAll(array('email'=> $userEmail, "company_id"=> $companyId, 'source' => 1 ,'status'=> 1, 'created' => $createdDate));
																							}

																					}
																					//************* if not registered then add to enterprise user list ******************//
																					else{
																							$this->EnterpriseUserList->saveAll(array('email'=> $userEmail, "company_id"=> $companyId, 'source' => 1, 'status'=> 1, 'created' => $createdDate));
																					}
																					$validEmail[] = $userEmail;
																			}
																	}
																	if($row % 100 == 0) { sleep(1); }

																}
															}else{
                                while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                                    $num = count($data);
                                    $row++;
                                    if($row == 2) continue;
                                    $userEmail = $data[0];
                                    //check email exist


                                    if( !empty($userEmail) && !empty($companyId) ){
                                        $userData = $this->EnterpriseUserList->find('all',array('conditions'=>array('email'=> $userEmail, 'company_id'=>$companyId)));

																				// print_r($userData);
                                        if( !empty($userData) ){
                                            $invalidEmail[] = $userData[0]['EnterpriseUserList']['email'];
                                        }else if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$userEmail)){
                                            $invalidEmail[] = $userEmail;
                                        }else{
                                            //************* Here check to look user is already registered ******************//
                                            //************* if registered then add to enterprise user list and update subscrition day ******************//
                                            $getUserDeatail = $this->User->find('first',
                                            array('conditions'=>
                                              array(
                                                'email'=> $userEmail
                                                )
                                            ));
                                            if(!empty($getUserDeatail)){
                                                $responseUserEmployment = $this->getUserEmployment($getUserDeatail['User']['id'],$companyId);
                                                if($responseUserEmployment == 1){
                                                    $this->EnterpriseUserList->saveAll(array('email'=> $userEmail, "company_id"=> $companyId, 'source' => 1 ,'status'=> 1, 'created' => $createdDate));
                                                    // $result = $this->updateUserSubscription($getUserDeatail['User']['id'],$companyId);
                                                }else{
                                                    $this->EnterpriseUserList->saveAll(array('email'=> $userEmail, "company_id"=> $companyId, 'source' => 1 ,'status'=> 1, 'created' => $createdDate));
                                                }

                                            }
                                            //************* if not registered then add to enterprise user list ******************//
                                            else{
                                                $this->EnterpriseUserList->saveAll(array('email'=> $userEmail, "company_id"=> $companyId, 'source' => 1, 'status'=> 1, 'created' => $createdDate));
                                            }
                                            $validEmail[] = $userEmail;
                                        }
                                    }
                                    if($row % 100 == 0) { sleep(1); }
                                	}
																}
                                fclose($handle);
                                $response['success'] = "File Uploaded Successfully";
                                //$response['valid_email'] = $validEmail;
                                $response['valid_email_count'] = count($validEmail);
                                //$response['invalid_email'] = $invalidEmail;
                                $response['invalid_email_count'] = count($invalidEmail);

                    	    	$responseData['status'] = 1;
                                $responseData['message'] = 'File Uploaded Successfully.';
                                $responseData['data'] = array('addUserData'=> $response);
                    			echo json_encode($responseData);

                                try{
	                                // Admin activity logs [START]
	                	            $activityData = array();
	                	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	                	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	                	            $activityData['action'] = 'Add User (Bulk)';
	                	            $activityData['custom_data'] = '{"valid_email" :"'.json_encode($validEmail).'","invalid_email" :"'.json_encode($invalidEmail).'"}';
	                	            $this->TrustAdminActivityLog->addActivityLog($activityData);
	                	            // Admin activity logs [END]
	                	        } catch (Exception $e) {

	                	        }
                            }
                       }else{
               		    	$responseData['status'] = 0;
               	            $responseData['message'] = 'Please Upload Valid File.';
               	            echo json_encode($responseData);
                       }

											 if( !empty($file_destination) && file_exists( $file_destination )){
													 chmod($file_destination, 777);
													 unlink( $file_destination );
													 $return = true;
											 }

                    }else{
        		    	$responseData['status'] = 0;
        	            $responseData['message'] = 'Some Error Occured In Uploading File. Please Try Again.';
        	            echo json_encode($responseData);
                    }
                }else{
                	$responseData['status'] = 0;
    	            $responseData['message'] = 'Please Upload Valid File.';
    	            echo json_encode($responseData);
                }
            }else{
	        	$responseData['status'] = 0;
	            $responseData['message'] = 'Please Upload Valid File.';
	            echo json_encode($responseData);
            }
        }else{
        	$responseData['status'] = 0;
            $responseData['message'] = 'Information not provided.';
            echo json_encode($responseData);
        }
    }

	public function sendNotification($params = array()){
		$messageArr = array(
		                "message"=> $params['notificationMessage'],
		                "aps"=> array("alert"=> $params['notificationMessage'], "sound"=> "notification_sound.mp3"),
		                "is_broadcast"=> 1,
		                "show_alert"=>1,
		                "tap_to_reply" => 0,
		            );
		$messageEncoded = base64_encode(json_encode($messageArr));
		$postData=json_encode(array("event"=> array("notification_type"=> "push", "environment"=> (ENVIRONMENT == "live" ? "production" : "development"), "user"=> $params['recipients'], "message"=> $messageEncoded)));
		$curl = curl_init();
	   curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'events.json');
	        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
	        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	                'Content-Type: application/json',
	                'QuickBlox-REST-API-Version: 0.1.0',
	                'QB-Token: ' . $params['qbtoken']
	            ));
	    $response = curl_exec($curl);
        if (!empty($response)) {
            return $response;
        } else {
            return false;
        }
        curl_close($curl);
	    return $response;
	}

	public function removeUserSubscription($userId, $companyId){
	    $result = 0;
	    $newSubscriptionPeriod = date("Y-m-d", strtotime("+15 days"));
	    $this->UserSubscriptionLog->updateAll(array("subscription_expiry_date"=>"'".$newSubscriptionPeriod."'", "subscribe_type"=> 2,"company_id"=>-1),array("user_id"=> $userId));
	    $result = 1;
	    return $result;
	}

	public function updateCacheOnStatusChange($userId, $companyId){
	    //** update dynamic cache[STATRT]
	    $lastModified = date('Y-m-d H:i:s');
	    if(! empty($userId)){
	        $getDynamicChangedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed")));
	        // return $getDynamicChangedUser;
	        if($getDynamicChangedUser > 0){
	            $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');
	        }else{
	            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }

	        //** Get last modified date dynamic directory[START]
	        $getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
	        if(!empty($getDynamicDirectoryLastModified)){
	            $paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
	            $paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
	            $this->updateDynamicEtagData($paramsDynamicEtagUpdate);
	        }
	        //** Get last modified date dynamic directory[END]
	    }
	    //** update dynamic cache[END]
	}

	public function sendSms($to, $message, $serviceId) {

		$fields = array(
			'Body' => urlencode($message),
			'MessagingServiceSid' => urlencode($serviceId),
			'To' => urlencode($to),
		);
		$fields_string ='';

		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/".Configure::read("twilio_details.account_sid")."/Messages.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_USERPWD, Configure::read("twilio_details.account_sid") . ":" . Configure::read("twilio_details.auth_token"));

		$result = curl_exec($ch);
		if($result){
			return json_encode($result);
		}
		if (curl_errno($ch)) {
		    return 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

	}

	public function sessionRead(){
		print_r($_SESSION);
		// echo $_COOKIE['adminRoleId'];
		// echo '<br>';
		// echo $_COOKIE['adminInstituteId'];
		// echo '<br>';
		// echo $_COOKIE['adminEmail'];
		// echo '<br>';
		exit;
	}

	public function getUserPresence(){
		$this->autoRender = false;
        $responseData = array();
		if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$roleId = $_COOKIE['adminRoleId'];
			$companyId = $_COOKIE['adminInstituteId'];
			$adminEmail = $_COOKIE['adminEmail'];
			$byPass = (isset($_COOKIE['adminAccessId']) && $_COOKIE['adminAccessId'] == 0)? 1: 0;

			$params = array();
			$params['userName'] = strtolower($adminEmail);
			$params['role_id'] = $roleId;
			$params['company_id'] = $companyId;
			$userData = $this->AdminUser->checkPresence($params);
			$batonRequestCount = $this->UserBatonRole->find('count',array(
				'conditions'=>array(
					"UserBatonRole.from_user"=> 0,
					"UserBatonRole.status"=> array(2,4),
					"UserBatonRole.is_active"=> 1,
					"DepartmentsBatonRole.institute_id" => $companyId
				),
				'joins' => array(
					array(
	          'table' => 'departments_baton_roles',
	          'alias' => 'DepartmentsBatonRole',
	          'type' => 'left',
	          'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
	        )
				),
				'group'=> 'UserBatonRole.role_id'
			));
			if(empty($userData) && $byPass == 0){
				$responseData['status'] = 0;
				$responseData['message'] = 'Sorry, your privileges has been changed by the  Trust Admin. Please re-login OR contact at support@medicbleep.com';
				echo json_encode($responseData);
			}else{
				$responseData['baton_request_count'] = $batonRequestCount;
				$responseData['status'] = 1;
				$responseData['message'] = 'ok.';
				echo json_encode($responseData);
			}
		}else{
			$responseData['status'] = 0;
		    $responseData['message'] = 'Session Time Out. Please login again to continue.';
		    echo json_encode($responseData);
		}
	}

	public function getUserSessionTimeout() {
		$this->autoRender = false;
	    $responseData = array();
	    if($this->request->is('post')) {
	        $companyId = $_COOKIE['adminInstituteId'];
	        $userId = $this->request->data['user_id'];
	        $email = $this->request->data['user_email'];

	        if( !empty($companyId) ){
	            $options = array(
	            	'conditions' => array(
	            		'UserIpSetting.user_id' => $userId,
	            		'UserIpSetting.company_id' => $companyId,
	            		'UserIpSetting.status' => 1
	            	),
	            	'fields' => array('UserIpSetting.ip,UserIpSetting.time_span'),
	            );
	            $userSessionData = $this->UserIpSetting->find('all',$options);

				    	$responseData['data'] = $userSessionData;
				    	$responseData['status'] = 1;
	            $responseData['message'] = 'ok.';
	            echo json_encode($responseData);

	            try{
		            // Admin activity logs [START]
		            $activityData = array();
		            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		            $activityData['admin_id'] = $_COOKIE['adminUserId'];
		            $activityData['user_id'] = $userId;
		            $activityData['action'] = 'Session Timeout Status';
		            $activityData['custom_data'] = 'View';
		            $this->TrustAdminActivityLog->addActivityLog($activityData);
		            // Admin activity logs [END]
	           	} catch (Exception $e) {

	           	}


	        }else{
		    	$responseData['status'] = 0;
	            $responseData['message'] = 'Empty Email Or Company Id.';
				echo json_encode($responseData);
	        }
	    }else{
	    	$responseData['status'] = 0;
	        $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
	    }
	}

	public function setSessionTimeout(){
		$this->autoRender = false;
        $responseData = array();
        if($this->request->is('post')) {
            $companyId = $_COOKIE['adminInstituteId'];
            $adminEmail = $_COOKIE['adminEmail'];
            $json_array = json_decode($this->request->data['ip_time_data'],true);
            $userId = $this->request->data['user_id'];
            if( !empty($companyId) && !empty($userId) ){

            	$this->UserIpSetting->deleteAll(array('UserIpSetting.company_id'=> $companyId, 'UserIpSetting.user_id' => $userId));
							if(!empty($json_array)){
								foreach ($json_array as $value) {
	            		$data[] = array(
	            		    'user_id' => $userId,
	            		    'company_id' => $companyId,
	            		    'ip' => $value['ip'],
	            		    'time_span' =>$value['time'],
	            		    'status' => 1,
	            		);
	            	}
	            	$this->UserIpSetting->saveAll($data);
							}

		    			$responseData['status'] = 1;
	            $responseData['message'] = 'ok.';
	            echo json_encode($responseData);

	            try{
		            // Admin activity logs [START]
		            $activityData = array();
		            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		            $activityData['admin_id'] = $_COOKIE['adminUserId'];
		            $activityData['user_id'] = $userId;
		            $activityData['action'] = 'Session Timeout Status Change';
		            $activityData['custom_data'] = $this->request->data['ip_time_data'];
		            $this->TrustAdminActivityLog->addActivityLog($activityData);
		            // Admin activity logs [END]
	            } catch (Exception $e) {

	            }

            }else{
		    	$responseData['status'] = 0;
	            $responseData['message'] = 'Empty Email Or Company Id.';
				echo json_encode($responseData);
            }
        }else{
	    	$responseData['status'] = 0;
            $responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
        }
	}

	public function roleUpdationMail($email,$role){
		$role = (string)$role;
		$companyId = $_COOKIE['adminInstituteId'];
		if($role == '2'){
			$adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $email)));
			// $role_name = $adminUserData['AdminUser']['role_id'] == '0'?'Admin':'Subadmin';
			if($adminUserData['AdminUser']['role_id'] == '1'){
				$role_name = 'Subadmin';
			}else if($adminUserData['AdminUser']['role_id'] == '3'){
				$role_name = 'Switchboard';
			}else{
				$role_name = 'Admin';
			}
			// $role_name = $adminUserData['AdminUser']['role_id'] == '1'?'Subadmin':$adminUserData['AdminUser']['role_id'] == '3'?'Switchboard':'Admin';
			$status = 0;
		}else{
			if($role == '1'){
				$role_name = 'Subadmin';
			}else if($role == '3'){
				$role_name = 'Switchboard';
			}else{
				$role_name = 'Admin';
			}
			// $role_name = $role == '1'?'Subadmin':$role == '3'?'Switchboard':'Admin';
			// $role_name = $role == '0'?'Admin':'Subadmin';
			$status = 1;
		}
		try {
			$userData_cust = $this->User->find('first',array('conditions'=>array('email'=>$email)));
			$company_data = $this->CompanyName->find('first',array('conditions'=>array('CompanyName.id'=>$companyId)));

			$params['trust_name'] = "'".$company_data['CompanyName']['company_name']."'";
			$params['status'] = $status;
			$params['f_name'] = $userData_cust['UserProfile']['first_name'];
			$params['role'] = $role_name;
			$params['toMail'] = $email;
			// $params['toMail'] = 'deepakkumar@mediccreations.com';


			$mailSend = $this->InsEmail->assignRole( $params );
		} catch (Exception $e) {}
	}

	public function getUserBatonRole() {
		$this->autoRender = false;
    $responseData = array();
    if($this->request->is('post')) {
        $companyId = $_COOKIE['adminInstituteId'];
        $userId = $this->request->data['user_id'];
        if( !empty($companyId) && !empty($userId) ){
// =====================================================
					$fields = array('
					UserBatonRole.role_id,
					UserBatonRole.from_user,
					UserBatonRole.to_user,
					UserBatonRole.role_id,
					UserBatonRole.status,
					UserProfile.first_name,
					UserProfile.last_name,
					UserProfile.profile_img,
					UserProfile.thumbnail_img,
					UserProfiles.user_id,
					UserProfiles.first_name,
					UserProfiles.last_name,
					UserProfiles.profile_img,
					UserProfiles.thumbnail_img,
					BatonRole.id,
					BatonRole.baton_roles,
					DepartmentsBatonRole.timeout,
					DepartmentsBatonRole.on_call_value
					');
					$conditions = array(
							"UserBatonRole.from_user"=> $userId ,
							"UserBatonRole.is_active"=> 1,
							// 'UserBatonRole.status' => array(1,2,5,7),
							'UserBatonRole.is_active' => array(1)
						);

					$joins = array(
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfile',
							'type' => 'left',
							'conditions'=> array('UserBatonRole.from_user = UserProfile.user_id')
						),
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfiles',
							'type' => 'left',
							'conditions'=> array('UserBatonRole.to_user = UserProfiles.user_id')
						),
						array(
							'table' => 'departments_baton_roles',
							'alias' => 'DepartmentsBatonRole',
							'type' => 'left',
							'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
						),
						array(
							'table' => 'baton_roles',
							'alias' => 'BatonRole',
							'type' => 'left',
							'conditions'=> array('DepartmentsBatonRole.role_id = BatonRole.id')
						)
					);

					$options = array(
						'conditions' => $conditions,
						'joins' => $joins,
						'fields' => $fields,
						'group'=> 'UserBatonRole.role_id'
					);

					$userList = $this->UserBatonRole->find('all',$options);
					$data_array = array();
					foreach ($userList as  $batonRoleDetail) {
						$otherUserFullname = $batonRoleDetail['UserProfiles']['first_name']." ".$batonRoleDetail['UserProfiles']['last_name'];
						$data_array[] = array(
								'other_user_id'=> isset($batonRoleDetail['UserProfiles']['user_id']) ? $batonRoleDetail['UserProfiles']['user_id'] : 0,
								'profile_img'=>	isset($batonRoleDetail['UserProfiles']['profile_img']) ? $batonRoleDetail['UserProfiles']['profile_img'] : "",
								'thumbnail_img'=> isset($batonRoleDetail['UserProfiles']['thumbnail_img']) ? $batonRoleDetail['UserProfiles']['thumbnail_img'] : "",
								'other_user_name'=> $otherUserFullname,
								'role_name'=> isset($batonRoleDetail['BatonRole']['baton_roles']) ? $batonRoleDetail['BatonRole']['baton_roles'] : "",
								'role_id'=> isset($batonRoleDetail['UserBatonRole']['role_id']) ? $batonRoleDetail['UserBatonRole']['role_id'] : 0,
								'duration'=> isset($batonRoleDetail['DepartmentsBatonRole']['timeout']) ? $batonRoleDetail['DepartmentsBatonRole']['timeout'] : 0,
								'type'=>isset($batonRoleDetail['UserBatonRole']['status']) ? $batonRoleDetail['UserBatonRole']['status'] : 0,
								'on_call_value'=>isset($batonRoleDetail['DepartmentsBatonRole']['on_call_value']) ? $batonRoleDetail['DepartmentsBatonRole']['on_call_value'] : 0,
								);
					}
// =====================================================
    			$responseData['data'] = $data_array;
    			$responseData['status'] = 1;
          $responseData['message'] = 'ok.';
          echo json_encode($responseData);
        }else{
					$responseData['status'] = 0;
          $responseData['message'] = 'Empty Email Or Company Id.';
					echo json_encode($responseData);
        }
    }else{
  		$responseData['status'] = 0;
			$responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
    }
	}

	public function getUnoccupiedBatonRole() {
		$this->autoRender = false;
    $responseData = array();
    if($this->request->is('post')) {
        $companyId = $_COOKIE['adminInstituteId'];
				$text = $this->data['search_text'];
				// $text = '';
        if( !empty($companyId)){

// =====================================================
					$fields = array('
					BatonRole.baton_roles,
					DepartmentsBatonRole.role_id,
					DepartmentsBatonRole.on_call_value
					');
					$conditions = array(
							'BatonRole.is_active' => 1,
							'DepartmentsBatonRole.is_active' => 1,
							'DepartmentsBatonRole.is_occupied' => 0,
							'DepartmentsBatonRole.institute_id' => $companyId,
							'LOWER(BatonRole.baton_roles) LIKE'=>strtolower('%'.$text.'%')
						);

					$joins = array(
						array(
							'table' => 'baton_roles',
							'alias' => 'BatonRole',
							'type' => 'left',
							'conditions'=> array('DepartmentsBatonRole.role_id = BatonRole.id')
						)
					);

					$options = array(
						'conditions' => $conditions,
						'joins' => $joins,
						'fields' => $fields,
						'group' => array('DepartmentsBatonRole.role_id'),
						'order' => array('BatonRole.baton_roles'=>'ASC'),
					);

					$batonRoles = $this->DepartmentsBatonRole->find('all',$options);
					$data_array = array();
					foreach ($batonRoles as  $batonRoleDetail) {
						$data_array[] = array(
								'name'=> isset($batonRoleDetail['BatonRole']['baton_roles']) ? $batonRoleDetail['BatonRole']['baton_roles'] : '',
								'id'=> isset($batonRoleDetail['DepartmentsBatonRole']['role_id']) ? $batonRoleDetail['DepartmentsBatonRole']['role_id'] : 0,
								'on_call_value'=>isset($batonRoleDetail['DepartmentsBatonRole']['on_call_value']) ? $batonRoleDetail['DepartmentsBatonRole']['on_call_value'] : 0,
								);
					}
// =====================================================
    			$responseData['data'] = $data_array;
    			$responseData['status'] = 1;
          $responseData['message'] = 'ok.';
          echo json_encode($responseData);
        }else{
					$responseData['status'] = 0;
          $responseData['message'] = 'Empty Email Or Company Id.';
					echo json_encode($responseData);
        }
    }else{
  		$responseData['status'] = 0;
			$responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
    }
	}
}
