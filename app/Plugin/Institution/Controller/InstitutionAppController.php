<?php
class InstitutionAppController extends AppController {
	public $components = array('Session','Cache');
	public $uses = array(
		'EmailTemplate',
		'Institution.User',
		'Institution.EnterpriseUserList',
		'Institution.UserEmployment',
		'Institution.AdminUser',
		'Institution.CompanyName',
		'Institution.UserSubscriptionLog',
		'Institution.NotificationBroadcastMessage',
		'Institution.UserQbDetail',
		'Institution.UserDutyLog',
		'Institution.UserOneTimeToken',
		'Institution.CacheLastModifiedUser',
		'Institution.ExportChatTransaction',
		'Institution.UserProfile',
		'Institution.QrCodeRequest',
		'Institution.Profession',
		'Institution.RoleTag',
		'Institution.TrustAdminActivityLog',
		'Institution.UserBatonRole'
	);

	public function beforeFilter(){
		if(isset($_COOKIE['adminEmail']) && isset($_COOKIE['adminInstituteId'])){
			$admin_email = $_COOKIE['adminEmail'];
			$companyId = $_COOKIE['adminInstituteId'];

			if($this->request->params['controller'] == 'Switchboard'){
				$this->layout = 'InstitutionSwitchboard';
			}else

			if($_COOKIE['adminRoleId'] == 3){
				$this->layout = 'InstitutionSwitchboard';
			}else{
				$this->layout = 'InstitutionDefault';
			}

			$batonRequestCount = $this->UserBatonRole->find('count',array(
				'conditions'=>array(
					"UserBatonRole.from_user"=> 0,
					"UserBatonRole.status"=> array(2,4),
					"UserBatonRole.is_active"=> 1,
					"DepartmentsBatonRole.institute_id" => $companyId
				),
				'joins' => array(
					array(
	          'table' => 'departments_baton_roles',
	          'alias' => 'DepartmentsBatonRole',
	          'type' => 'left',
	          'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
	        )
				),
				'group'=> 'UserBatonRole.role_id'
			));

			$this->set(array('baton_count'=>$batonRequestCount));
		}else{
			$this->layout = 'InstitutionLogin';
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function checkAdminRole(){
		if(isset($_COOKIE['adminEmail']) && isset($_COOKIE['adminInstituteId']) &&  null !== $_COOKIE['adminEmail'] && $_COOKIE['adminEmail'] !='' && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			if($_COOKIE['adminRoleId'] == 3){
				$this->layout = 'InstitutionSwitchboard';
				$this->redirect('/institution/Switchboard/index');
			}
		}else{
			$this->layout = 'InstitutionLogin';
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function updateStaticEtagData($params = array()){
		$cacObjEtag = $this->Cache->redisConn();
		if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
				$etagLastModified = strtotime(date('Y-m-d H:i:s'));
				$eTagNewVal = md5($params['key'] . rand(1,10000) . $etagLastModified);
				$cacObjEtag['robj']->hset($params['key'],'eTag' , $eTagNewVal);
				$cacObjEtag['robj']->hset($params['key'],'lastmodified' , $etagLastModified);
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function updateDynamicEtagData($params = array()){
		$cacObjEtag = $this->Cache->redisConn();
		if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
				//$dynamicPreviousModifiedDate = $cacObjEtag['robj']->hget($params['key'],'lastmodified');
				//$cacObjEtag['robj']->hset($params['key'],'previousLastmodified' , $dynamicPreviousModifiedDate);
				$etagLastModified = strtotime($params['lastmodified']);
				$eTagNewVal = md5($params['key'] . rand(1,10000) . $etagLastModified);
				$cacObjEtag['robj']->hset($params['key'],'eTag' , $eTagNewVal);
				$cacObjEtag['robj']->hset($params['key'],'lastmodified' , $etagLastModified);
		}
	}

}
