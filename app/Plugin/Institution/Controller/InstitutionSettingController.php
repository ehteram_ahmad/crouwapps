<?php

class InstitutionSettingController extends InstitutionAppController {
    public $components = array('RequestHandler','Paginator','Session', 'Institution.InsCommon', 'Institution.InsEmail', 'Image', 'Quickblox', 'Cache');

    public $uses = array(
      'EmailTemplate',
      'Institution.User',
      'Institution.EnterpriseUserList',
      'Institution.UserEmployment',
      'Institution.AdminUser',
      'Institution.CompanyName',
      'Institution.UserSubscriptionLog',
      'Institution.NotificationBroadcastMessage',
      'Institution.UserQbDetail',
      'Institution.UserDutyLog',
      'Institution.UserOneTimeToken',
      'Institution.CacheLastModifiedUser',
      'Institution.ExportChatTransaction',
      'Institution.UserProfile',
      'Institution.QrCodeRequest',
      'Institution.Profession',
      'Institution.RoleTag',
      'Institution.AvailableAndOncallTransaction',
      'Institution.ScreenShotTransaction',
      'Institution.TrustAdminActivityLog',
      'Institution.SessionTimeIp',
      'Institution.SessionTimeIpRange',
      'Institution.UserRoleTag',
      'Institution.UserPermanentRole'
    );
    public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

    public function index(){
      $this->checkAdminRole();
    	if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
          // Admin activity logs [START]
          $activityData = array();
          $activityData['company_id'] = $_COOKIE['adminInstituteId'];
          $activityData['admin_id'] = $_COOKIE['adminUserId'];
          $activityData['action'] = 'Settings Section';
          $activityData['custom_data'] = 'View';
          $this->TrustAdminActivityLog->addActivityLog($activityData);
          // Admin activity logs [END]
  		}
    }

    public function session(){
      $this->checkAdminRole();
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = 'Session List';
      $activityData['custom_data'] = 'View';
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }


    public function getIpTimeout(){
      $this->autoRender = false;
	    $responseData = array();
	    $ip_range_ids = array();
	    $ip_ids = array();
	    $ip_range_time = 0;
	    $ip_time = 0;
	    if($this->request->is('post')) {
	        $companyId = $_COOKIE['adminInstituteId'];
          $normal_ips = $this->SessionTimeIp->find('all',array('conditions'=>array('SessionTimeIp.company_id'=> $companyId)));
          $ip_range = $this->SessionTimeIpRange->find('all',array('conditions'=>array('SessionTimeIpRange.company_id'=> $companyId)));

          if(!empty($ip_range)){
            foreach ($ip_range as $value) {
              $ip_data = array();
              $ip_data['from'] = $value['SessionTimeIpRange']['ip_from'];
              $ip_data['to'] = $value['SessionTimeIpRange']['ip_to'];
              $ip_data['time'] = $value['SessionTimeIpRange']['time_span'];
              $ip_range_time = $value['SessionTimeIpRange']['time_span'];
              $ip_range_ids[] = $ip_data;
            }
          }

          if(!empty($normal_ips)){
            foreach ($normal_ips as $value) {
              $ip_data = array();
              $ip_data['ip'] = $value['SessionTimeIp']['ip'];
              $ip_data['time'] = $value['SessionTimeIp']['time_span'];
              $ip_time = $value['SessionTimeIp']['time_span'];
              $ip_ids[] = $ip_data;
            }
          }

          $userSessionData['ip_range'] = $ip_range_ids;
          $userSessionData['ip_list'] = $ip_ids;
          $userSessionData['time'] = $ip_time;

          $responseData['data'] = $userSessionData;
		    	$responseData['status'] = 1;
          $responseData['message'] = 'ok.';
          echo json_encode($responseData);
      }else{
          $responseData['status'] = 0;
          $responseData['message'] = 'Information not provided.';
          echo json_encode($responseData);
      }

    }

    public function setIpTimeout(){
      $this->autoRender = false;
      $responseData = array();
      if($this->request->is('post')) {
          $companyId = $_COOKIE['adminInstituteId'];
          $ip_range = json_decode($this->request->data['ip_range'],true);
          $ips = json_decode($this->request->data['ips'],true);
          $time = $this->request->data['time'];

          $this->SessionTimeIp->deleteAll(array('SessionTimeIp.company_id'=> $companyId));
          $this->SessionTimeIpRange->deleteAll(array('SessionTimeIpRange.company_id'=> $companyId));

          if(!empty($ip_range)){
            $data = array();
            foreach ($ip_range as $value) {
              $data[] = array(
                  'company_id' => $companyId,
                  'ip_from' => $value['from'],
                  'ip_to' => $value['to'],
                  'time_span' => $time,
                  'status' => 1
              );
            }
            $this->SessionTimeIpRange->saveAll($data);
          }

          if(!empty($ips)){
            $data = array();
            foreach ($ips as $value) {
              $data[] = array(
                  'company_id' => $companyId,
                  'ip' => $value,
                  'time_span' => $time,
                  'status' => 1
              );
            }
            $this->SessionTimeIp->saveAll($data);
          }
          $responseData['status'] = 1;
          $responseData['message'] = 'ok.';
          echo json_encode($responseData);
          try{
              // Admin activity logs [START]
              $activityData = array();
              $activityData['company_id'] = $_COOKIE['adminInstituteId'];
              $activityData['admin_id'] = $_COOKIE['adminUserId'];
              $activityData['action'] = 'Update Session Time out';
              $activityData['custom_data'] = json_encode($this->request->data);
              $this->TrustAdminActivityLog->addActivityLog($activityData);
              // Admin activity logs [END]
          } catch (Exception $e) {

          }
      }else{
          $responseData['status'] = 0;
          $responseData['message'] = 'Information not provided.';
          echo json_encode($responseData);
      }

    }
    public function number(){
      $this->checkAdminRole();
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = 'NHS/MRN Section';
      $activityData['custom_data'] = 'View';
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }
    public function getNhsType(){
      if(isset($_COOKIE['adminInstituteId']) && $_COOKIE['adminInstituteId'] != null){
        $this->autoRender = false;
        $companyId = $_COOKIE['adminInstituteId'];
        $nhsNum = 0;

        $fields = array('CompanyName.patient_number_type');
        $conditions = array(
            'CompanyName.id' => $companyId,
            'CompanyName.status' => 1
          );

        $options = array(
          'conditions' => $conditions,
          'fields' => $fields
        );

        $data = $this->CompanyName->find('first',$options);
        if(isset($data['CompanyName']['patient_number_type'])){
          $nhsNum = $data['CompanyName']['patient_number_type'];
        }
        return $nhsNum;
      }
    }
    public function updatePatientType(){
      $this->autoRender = false;
      $responseData = array();
      if($this->request->is('post')) {
        $companyId = $_COOKIE['adminInstituteId'];
        $type = $this->request->data['type'];

        if( !empty($companyId)){

          $updateData = array(
            'patient_number_type' => $type
          );
          $updateConditions = array("id"=> $companyId);
          $updateAdminUserMapping = $this->CompanyName->updateAll($updateData, $updateConditions);

          $responseData['status'] = 1;
          $responseData['message'] = 'ok.';
          echo json_encode($responseData);

          try{
            // Admin activity logs [START]
            $activityData = array();
            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
            $activityData['admin_id'] = $_COOKIE['adminUserId'];
            $activityData['action'] = 'Update Patient Number Type';
            $activityData['custom_data'] = '{"patient_number_type" :"'.$type.'"}';
            $this->TrustAdminActivityLog->addActivityLog($activityData);
            // Admin activity logs [END]
          } catch (Exception $e) {

          }
        }else{
          $responseData['status'] = 0;
          $responseData['message'] = 'Empty Company Id.';
          echo json_encode($responseData);
        }
      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Information not provided.';
        echo json_encode($responseData);
      }
    }

    public function baseLocation(){
      $this->checkAdminRole();
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = 'Base Location List';
      $activityData['custom_data'] = 'View';
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }
    public function getBaseLocation(){
      $companyId = $_COOKIE['adminInstituteId'];
  		if(isset($this->data['search_text']) && $this->data['search_text'] != ''){
  		    $text=$this->data['search_text'];
  		    $search_condition = array('OR'=>array('LOWER(RoleTag.value) LIKE'=>strtolower('%'.$text.'%'),'LOWER(RoleTag.value) LIKE'=>strtolower('%'.$text.'%')));
  		}else{
  		    $search_condition = array();
  		}

  		if(isset($this->data['page_number'])){
  		    $page_number=$this->data['page_number'];
  		}else{
  		    $page_number=1;
  		}

      $custom_condition = array('RoleTag.status'=>array(0,1));
      if(isset($this->data['type'])){
        $type = $this->data['type'];
        if($type != 'all'){
          $custom_condition = array('RoleTag.status'=>$type);
        }
  		}

  		if(isset($this->data['limit'])){
  		    $limit=$this->data['limit'];
  		}else{
  		    $limit=20;
  		}
  		$fields = array('
  				RoleTag.id,
  				RoleTag.key,
  				RoleTag.value,
  				RoleTag.trust_id,
  				RoleTag.status
  			');
  		$conditions = array(
  				'RoleTag.trust_id' => array($companyId, 0),
  				'RoleTag.key' => "Base Location"
  			);
  		$conditions_final = array_merge($conditions,$search_condition,$custom_condition);
  		$options = array(
  			'conditions' => $conditions_final,
  			'fields' => $fields,
  			'limit' => $limit,
  			'page'=> $page_number,
  			'group' => array('RoleTag.id'),
  			'order' => 'RoleTag.value ASC'
  		);

  		$this->Paginator->settings = $options;
  		$baselocationList = $this->Paginator->paginate('RoleTag');
  		$newArr = array();

  		$total = $this->request->params;
  		$tCount = $total['paging']['RoleTag']['count'];

  		$data['base_location_list'] = $baselocationList;

  		$this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
  		$this->render('/Elements/home/base_location_ajax');
    }

    public function getDashboardCounts(){
      $this->autoRender = false;
      $companyId = $_COOKIE['adminInstituteId'];
  		if(isset($this->data['search_text']) && $this->data['search_text'] != ''){
  		    $text=$this->data['search_text'];
  		    $search_condition = array('OR'=>array('LOWER(RoleTag.value) LIKE'=>strtolower('%'.$text.'%'),'LOWER(RoleTag.value) LIKE'=>strtolower('%'.$text.'%')));
  		}else{
  		    $search_condition = array();
  		}
      $fields = array('
  				RoleTag.status
  			');
  		$conditions = array(
  				'RoleTag.trust_id' => array($companyId, 0),
  				'RoleTag.key' => "Base Location"
  			);
  		$conditions_final = array_merge($conditions,$search_condition);
  		$options = array(
  			'conditions' => $conditions_final,
  			'fields' => $fields,
  			'group' => array('RoleTag.id')
  		);

      $data['active'] = (int)$this->RoleTag->find('count',array(
                    'conditions' => array_merge($conditions_final,array('RoleTag.status'=>1)),
                		'fields' => $fields,
                		'group' => array('RoleTag.id')
                  ));

      $data['inactive'] = (int)$this->RoleTag->find('count',array(
                    'conditions' => array_merge($conditions_final,array('RoleTag.status'=>0)),
                		'fields' => $fields,
                		'group' => array('RoleTag.id')
                  ));
      $data['all'] = $data['active'] + $data['inactive'];
      return json_encode($data);
    }

    public function changeBaseLocationStatus() {
  		$this->autoRender = false;
  		$responseData = array();
  		$companyId = $_COOKIE['adminInstituteId'];
  		$baseLocationId = $this->request->data['baseLocationId'];
  		$status = $this->request->data['status'];
      // $existence = $this->UserRoleTag->find('count',array('conditions'=>array('UserRoleTag.role_tag_id'=>$baseLocationId,'UserRoleTag.status'=>1)));
      $existence = $this->checkBaseLocationSelected($baseLocationId);
  		if(!empty($baseLocationId) && !empty($companyId)){
        if($existence > 0 && $status == 0){
          $responseData['message'] = 'Base Location is already assigned to a user.';
          $responseData['status'] = 0;
        }else{
          $this->RoleTag->updateAll(array("status"=> $status), array("id"=> $baseLocationId));
        	if($status == 1){
        		$responseData['message'] = 'Base Location has been activated successfully';
            $responseData['status'] = 1;
        	}else{
        		$responseData['message'] = 'Base Location has been deactivated successfully';
            $responseData['status'] = 1;
        	}
          try{
  		    	// Admin activity logs [START]
  		    	$activityData = array();
  		    	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
  		    	$activityData['admin_id'] = $_COOKIE['adminUserId'];
  		    	$activityData['action'] = $responseData['message'];
  		    	$activityData['custom_data'] = json_encode($this->request->data);
  		    	$this->TrustAdminActivityLog->addActivityLog($activityData);
  		    	// Admin activity logs [END]
  	    	} catch (Exception $e) {

  	    	}
        }
        echo json_encode($responseData);
  		}else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Email Or Company Id.';
        echo json_encode($responseData);
      }
  	}

    public function updateBaseLocation() {
  		$this->autoRender = false;
  		$responseData = array();

      $companyId = $_COOKIE['adminInstituteId'];

      $baseLocationUpdateType = $this->request->data['type'];
      $baseLocationValue = $this->request->data['value'];
  		$baseLocationId = $this->request->data['baseLocationId'];

      $baseLocationValue = ucwords($baseLocationValue);
      $createdDate = date("Y-m-d H:i:s");

      $existence = $this->checkBaseLocationSelected($baseLocationId);
  		if(!empty($baseLocationId) && !empty($baseLocationValue) && !empty($companyId)){
        if($existence > 0){
          $responseData['message'] = 'Base Location is already assigned to a user.';
          $responseData['status'] = 0;
        }else{
          if($baseLocationUpdateType == 'update'){
            $roleTagData = $this->RoleTag->find('first',array(
              'conditions'=>array(
                'LOWER(RoleTag.value)'=> strtolower($baseLocationValue),
                'RoleTag.trust_id' => $companyId,
                'RoleTag.status !=' => 2
              )
            ));
            if(!empty($roleTagData)){
              $responseData['status'] = 0;
              $responseData['message'] = 'Base Location Already Exists';
            }else{
              $this->RoleTag->updateAll(array("value"=> '"'.ucwords($baseLocationValue).'"'), array("id"=> $baseLocationId));
              $responseData['message'] = 'Base Location has been Updated successfully';
              $responseData['status'] = 1;
            }
          }else if($baseLocationUpdateType == 'delete'){
            $this->RoleTag->updateAll(array("status"=> 2), array("id"=> $baseLocationId));
            $responseData['message'] = 'Base Location has been Deleted successfully';
            $responseData['status'] = 1;
          }
          try{
  		    	// Admin activity logs [START]
  		    	$activityData = array();
  		    	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
  		    	$activityData['admin_id'] = $_COOKIE['adminUserId'];
  		    	$activityData['action'] = $responseData['message'];
  		    	$activityData['custom_data'] = json_encode($this->request->data);
  		    	$this->TrustAdminActivityLog->addActivityLog($activityData);
  		    	// Admin activity logs [END]
  	    	} catch (Exception $e) {

  	    	}
        }
        echo json_encode($responseData);
  		}else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Value Or Company Id.';
        echo json_encode($responseData);
      }
  	}

    public function checkBaseLocationSelected($roleId = null) {
      $this->autoRender = false;
      if($roleId != null){
        $companyId = $_COOKIE['adminInstituteId'];
        $existence = $this->RoleTag->find('first',array('conditions'=>array('RoleTag.id'=>$roleId)));
        $tag_name = $existence['RoleTag']['value'];
        $condition = array(
          'RoleTag.value' => $tag_name,
          'UserEmployment.company_id' => $companyId,
          'User.status' => 1,
          'User.approved' => 1
        );
        $joins = array(
          array(
              'table' => 'user_employments',
              'alias' => 'UserEmployment',
              'type' => 'left',
              'conditions'=> array('UserEmployment.user_id = User.id')
          ),
          array(
              'table' => 'user_role_tags',
              'alias' => 'UserRoleTag',
              'type' => 'left',
              'conditions'=> array('UserRoleTag.user_id = User.id')
          ),
          array(
              'table' => 'role_tags',
              'alias' => 'RoleTag',
              'type' => 'left',
              'conditions'=> array('RoleTag.id = UserRoleTag.role_tag_id')
          )
        );

        $exist = $this->User->find('count',array(
  			                'conditions' => $condition,
  			                'fiels' => array('user.id,RoleTag.id,UserRoleTag.role_tag_id,RoleTag.value'),
  			                'joins'=> $joins
  			            ));
        return $exist;
      }
    }

    public function addBaseLocation(){
      $this->autoRender = false;
  		$responseData = array();
  		$companyId = $_COOKIE['adminInstituteId'];

	    if($this->request->is('post')) {
	        $baseLocationValue = $this->request->data['value'];
	        $baseLocationValue = ucwords($baseLocationValue);
          $createdDate = date("Y-m-d H:i:s");
          // RoleTag.trust_id
	        if( !empty($baseLocationValue) && !empty($companyId) ){
	            $roleTagData = $this->RoleTag->find('first',array(
                'conditions'=>array(
                  'LOWER(RoleTag.value)'=> strtolower($baseLocationValue),
                  'RoleTag.trust_id' => $companyId
                )
              ));
	            if( !empty($roleTagData) ){
                if($roleTagData['RoleTag']['status'] == 2){
                  $this->RoleTag->updateAll(array("status"=> 1), array("id"=> $roleTagData['RoleTag']['id']));
                  // $this->RoleTag->updateAll(array('key'=> 'Base Location', "trust_id"=>$companyId , "value"=> $baseLocationValue, 'status'=> 1, 'created' => $createdDate));
	                $responseData['status'] = 1;
    	            $responseData['message'] = 'Base Location Added Successfully';
    				      echo json_encode($responseData);
                }else{
                  $responseData['status'] = 0;
                  $responseData['message'] = 'Base Location Already Exists';
                  echo json_encode($responseData);
                }
	            }else{
	                $this->RoleTag->save(array('key'=> 'Base Location', "trust_id"=>$companyId , "value"=> $baseLocationValue, 'status'=> 1, 'created' => $createdDate));
	                $responseData['status'] = 1;
    	            $responseData['message'] = 'Base Location Added Successfully';
    				      echo json_encode($responseData);
	            }
              try{
                  // Admin activity logs [START]
                  $activityData = array();
                  $activityData['company_id'] = $_COOKIE['adminInstituteId'];
                  $activityData['admin_id'] = $_COOKIE['adminUserId'];
                  $activityData['action'] = $responseData['message'];
                  $activityData['custom_data'] = '{"base_location":"'.$baseLocationValue.'"}';
                  $this->TrustAdminActivityLog->addActivityLog($activityData);
                  // Admin activity logs [END]
              } catch (Exception $e) {

              }
	        }
	        else{
	        	$responseData['status'] = 0;
            $responseData['message'] = 'Empty Email Or Company Id.';
            echo json_encode($responseData);
	        }

	    }
	    else{
	    	$responseData['status'] = 0;
        $responseData['message'] = 'Information not provided.';
        echo json_encode($responseData);
	    }
  	}

    public function profession(){
      $this->checkAdminRole();
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = 'Profession List';
      $activityData['custom_data'] = 'View';
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }

    public function getProfession(){
      $companyId = $_COOKIE['adminInstituteId'];
      $user_country_id = $_COOKIE['adminCountryId'];

      if(isset($this->data['search_text'])){
  		    $text=$this->data['search_text'];
  		    $search_condition = array('OR'=>array('LOWER(Profession.profession_type) LIKE'=>strtolower('%'.$text.'%')));
  		}
  		else{
  		    $search_condition = array();
  		}
      if(isset($this->data['page_number'])){
  		    $page_number=$this->data['page_number'];
  		}
  		else{
  		    $page_number=1;
  		}

  		if(isset($this->data['limit'])){
  		    $limit=$this->data['limit'];
  		}
  		else{
  		    $limit=20;
  		}

      $type = array(0,1);
  		if(isset($this->data['type'])){
  			$val = $this->data['type'];
  			if($val != 'all'){
  				$type = array($val);
  			}
  		}
      $custom_condition = array('Profession.status'=>$type);

      $condition = array_merge(
        array('Profession.country_id'=>$user_country_id),
        $custom_condition,
        $search_condition
      );

      $fields = array('
        Profession.id,
        Profession.profession_type,
        Profession.status,
        Profession.tags
      ');

      $options = array(
  			'conditions' => $condition,
  			'fields' => $fields,
  			'limit' => $limit,
  			'page'=> $page_number,
  			'group' => array('Profession.id'),
  			'order' => 'Profession.profession_type ASC'
  		);

      // print_r($options);
      // exit;

      $this->Paginator->settings = $options;
  		$profession = $this->Paginator->paginate('Profession');

      $total = $this->request->params;
  		$tCount = $total['paging']['Profession']['count'];

      $final_list = array();
      foreach ($profession as $prof) {
        $tag = $prof['Profession']['tags'];
        $id = $prof['Profession']['id'];
        $status = $prof['Profession']['status'];
        $name = $prof['Profession']['profession_type'];

        $final_list_tags = array();
        if(isset($tag) && $tag != ''){
          $tag_array = explode(",",$tag);
          foreach ($tag_array as $tag_value) {
            if($tag_value != 'Base Location'){
              $tag_data = $this->RoleTag->find('all',array('conditions'=>array('RoleTag.key'=>$tag_value,'RoleTag.trust_id'=>$companyId,'RoleTag.status'=>1,'RoleTag.profession_id'=>array($id))));
              $tag_data_array = array();
              foreach ($tag_data as $value) {
                $tag_data_array[] = $value['RoleTag']['value'];
              }
              $final_list_tags[$tag_value] = $tag_data_array;
            }
          }
        }
        $final_list[$name]['status'] = $status;
        $final_list[$name]['id'] = $id;
        $final_list[$name]['tags'] = $final_list_tags;
      }
      // $this->set(array("data"=>$final_list));
      $this->set(array("data"=>$final_list,"condition"=>$condition,"limit"=>$limit,'tCount'=>$tCount));
			$this->render('/Elements/settings/profession_ajax');
    }

    public function getProfDashCount(){
      $this->autoRender = false;
      $user_country_id = $_COOKIE['adminCountryId'];
      $companyId = $_COOKIE['adminInstituteId'];
  		if(isset($this->data['search_text']) && $this->data['search_text'] != ''){
  		    $text=$this->data['search_text'];
  		    $search_condition = array('OR'=>array('LOWER(Profession.profession_type) LIKE'=>strtolower('%'.$text.'%')));
  		}else{
  		    $search_condition = array();
  		}

  		$conditions = array('Profession.country_id'=>$user_country_id);

  		$conditions_final = array_merge($conditions,$search_condition);

      $data['active'] = (int)$this->Profession->find('count',array(
                    'conditions' => array_merge($conditions_final,array('Profession.status'=>1)),
                		'group' => array('Profession.id')
                  ));

      $data['inactive'] = (int)$this->Profession->find('count',array(
                    'conditions' => array_merge($conditions_final,array('Profession.status'=>0)),
                		'group' => array('Profession.id')
                  ));
      $data['all'] = $data['active'] + $data['inactive'];
      return json_encode($data);
    }

    public function updateProfState(){
      $this->autoRender = false;
  		$responseData = array();
      $companyId = $_COOKIE['adminInstituteId'];
      $user_country_id = $_COOKIE['adminCountryId'];

      $professionUpdateType = $this->request->data['request_type'];
      $professionStatus = (int)$this->request->data['status'];
  		$professionId = $this->request->data['id'];
      $professionValue = $this->request->data['value'];

      $existence = $this->UserProfile->find('count',array('conditions'=>array('UserProfile.profession_id'=>$professionId)));

  		if(isset($professionId) && isset($professionStatus) && isset($companyId)){
        if($existence > 0){
          $responseData['message'] = 'Profession is already assigned to a user.';
          $responseData['status'] = 0;
        }else{
          if($professionUpdateType == 'update'){
            $profData = $this->Profession->find('first',array(
              'conditions'=>array(
                'LOWER(Profession.profession_type)'=> strtolower($professionValue),
                'Profession.country_id' => $user_country_id,
                'Profession.status !=' => 2
              )
            ));
            if(!empty($profData)){
              $responseData['status'] = 0;
              $responseData['message'] = 'Profession Already Exists';
            }else{
              $this->Profession->updateAll(array("profession_type"=> '"'.ucwords($professionValue).'"'), array("id"=> $professionId));
              $responseData['message'] = 'Profession has been Updated successfully';
              $responseData['status'] = 1;
            }
          }else if($professionUpdateType == 'status'){
            $this->Profession->updateAll(array("status"=> $professionStatus), array("id"=> $professionId));
            $responseData['message'] = 'Profession has been Updated successfully';
            $responseData['status'] = 1;
          }else if($professionUpdateType == 'delete'){
            $this->Profession->updateAll(array("status"=> $professionStatus), array("id"=> $professionId));
            $responseData['message'] = 'Profession has been Deleted successfully';
            $responseData['status'] = 1;
          }
          try{
  		    	// Admin activity logs [START]
  		    	$activityData = array();
  		    	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
  		    	$activityData['admin_id'] = $_COOKIE['adminUserId'];
  		    	$activityData['action'] = $responseData['message'];
  		    	$activityData['custom_data'] = json_encode($this->request->data);
  		    	$this->TrustAdminActivityLog->addActivityLog($activityData);
  		    	// Admin activity logs [END]
  	    	} catch (Exception $e) {

  	    	}
        }
        echo json_encode($responseData);
  		}else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Value Or Company Id.';
        echo json_encode($responseData);
      }
    }

    public function getTagData(){
      $id = $this->request->data['id'];
      $data = $this->professionDetail($id, 'request');
      return $data;
    }
    public function professionDetail($prof_id = null, $type = null){
      $this->checkAdminRole();
      if($type == 'request'){
        $this->autoRender = false;
      }
      if(isset($_COOKIE['adminInstituteId']) && isset($prof_id)){
        $profession = $this->Profession->find('first',array('conditions'=>array('Profession.id'=>$prof_id)));
        $companyId = $_COOKIE['adminInstituteId'];

        $tag = $profession['Profession']['tags'];
        $id = $profession['Profession']['id'];
        $status = $profession['Profession']['status'];
        $name = $profession['Profession']['profession_type'];

        $final_list_tags = array();
        if(isset($tag) && $tag != ''){
          $tag_array = explode(",",$tag);
          foreach ($tag_array as $tag_value) {
            if($tag_value != 'Base Location'){
              $tag_data = $this->RoleTag->find('count',array('conditions'=>array('RoleTag.key'=>$tag_value,'RoleTag.trust_id'=>$companyId,'RoleTag.status'=>1,'RoleTag.profession_id'=>array($id))));
              $final_list_tags[$tag_value] = $tag_data;
            }
          }
        }

        $final_list['name'] = $name;
        $final_list['id'] = $id;
        $final_list['tags'] = $final_list_tags;
        if($type == 'request'){
          return json_encode($final_list);
        }else{
          $this->set(array('data'=>$final_list));
        }
        // Admin activity logs [START]
        $activityData = array();
        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
        $activityData['admin_id'] = $_COOKIE['adminUserId'];
        $activityData['action'] = 'Profession Details';
        $activityData['custom_data'] = json_encode(array('profession_id'=>$profId));
        $this->TrustAdminActivityLog->addActivityLog($activityData);
        // Admin activity logs [END]
      }
    }

    public function getTagDataList(){
      $this->autoRender = false;
      $tagKey = $this->request->data['key'];
      $profId = $this->request->data['prof_id'];
      $companyId = $_COOKIE['adminInstituteId'];
      $list = $this->RoleTag->find('all',array(
        'conditions'=>array(
          'RoleTag.profession_id'=>$profId,
          'RoleTag.trust_id'=>$companyId,
          'RoleTag.key'=>$tagKey
        ),
        'group'=>array('RoleTag.id'),
        'order'=>'RoleTag.value'
      ));
      $tagList = array();
      foreach ($list as $value) {
        $tagList[] = array(
          'name' => $value['RoleTag']['value'],
          'id' => $value['RoleTag']['id'],
          'status' => $value['RoleTag']['status']
        );
      }
      return json_encode($tagList);
    }

    public function addRoleTag(){
      $this->autoRender = false;
      $responseData = array();
      $companyId = $_COOKIE['adminInstituteId'];
      $key = $this->request->data['key'];
      $name = $this->request->data['name'];
      $professionId = $this->request->data['profession_id'];
      if(isset($key) && isset($name) && isset($professionId) && isset($companyId)){
        $existence = $this->RoleTag->saveAll(array('key'=>$key,'value'=>ucwords($name),'trust_id'=>$companyId,'profession_id'=>$professionId,'status'=>1));
        $responseData['status'] = 1;
        $responseData['message'] = $key.' Successfully added.';
      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Value Or Company Id.';
      }
      echo json_encode($responseData);
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = $responseData['message'];
      $activityData['custom_data'] = json_encode($this->request->data);
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }

    public function changeTagStatus(){
      $this->autoRender = false;
      $companyId = $_COOKIE['adminInstituteId'];
      $tagId = $this->request->data['tag_id'];
      $status = $this->request->data['status'];
      $key = $this->request->data['tag_key'];
      $existence = $this->getTagExistence($key,$tagId);
      if(!empty($tagId) && !empty($companyId)){
        if($existence > 0 && $status == 0){
          $responseData['message'] = $key.' already assigned to a user.';
          $responseData['status'] = 0;
        }else{
          $this->RoleTag->updateAll(array("status"=> $status), array("id"=> $tagId));
        	if($status == 1){
        		$responseData['message'] = $key.' has been activated successfully.';
            $responseData['status'] = 1;
        	}else{
        		$responseData['message'] = $key.' has been deactivated successfully.';
            $responseData['status'] = 1;
        	}
        }
        echo json_encode($responseData);
  		}else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Email Or Company Id.';
        echo json_encode($responseData);
      }
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = $responseData['message'];
      $activityData['custom_data'] = json_encode($this->request->data);
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }

    public function getTagExistence($key = null, $tagId = null){
      if(isset($key) && isset($tagId)){
        $companyId = $_COOKIE['adminInstituteId'];
        $tagData = $this->RoleTag->find('first',array('conditions'=>array('RoleTag.id'=>$tagId)));
        $tagName = $tagData['RoleTag']['value'];
        $checkString = '{"tag":"'.$key.'","values":["'.$tagName.'"]}';
        $count = $this->UserPermanentRole->find('count',array(
          'conditions'=>array(
            'LOWER(UserPermanentRole.role) LIKE' => strtolower('%'.$checkString.'%'),
            'UserPermanentRole.institute_id' => $companyId
          )
        ));
        return $count;
      }
    }

    public function updateRoleTag(){
      $this->autoRender = false;
      $responseData = array();
      $companyId = $_COOKIE['adminInstituteId'];
      $key = $this->request->data['key'];
      $name = $this->request->data['name'];
      $tagId = $this->request->data['tag_id'];
      $existence = $this->getTagExistence($key,$tagId);
      if(isset($key) && isset($name) && isset($tagId) && isset($companyId)){
        $roleTagData = $this->RoleTag->find('first',array(
          'conditions'=>array(
            'LOWER(RoleTag.value)'=> strtolower($name),
            'RoleTag.trust_id' => $companyId,
            'RoleTag.status !=' => 2
          )
        ));
        if($existence > 0){
          $responseData['message'] = $key.' already assigned to a user.';
          $responseData['status'] = 0;
        }else if(!empty($roleTagData)){
          $responseData['status'] = 0;
          $responseData['message'] = $key.' Already Exists';
        }else{
          $this->RoleTag->updateAll(array("value"=> '"'.ucwords($name).'"'), array("id"=> $tagId));
          $responseData['message'] = $key.' has been Updated successfully';
          $responseData['status'] = 1;
        }
      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Value Or Company Id.';
      }
      echo json_encode($responseData);
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = $responseData['message'];
      $activityData['custom_data'] = json_encode($this->request->data);
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }

    public function addProfession(){
      $this->checkAdminRole();
      // Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = 'Add New Profession';
      $activityData['custom_data'] = 'View';
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
    }
}
