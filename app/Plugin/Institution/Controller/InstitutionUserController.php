<?php

class InstitutionUserController extends InstitutionAppController {
	public $components = array('RequestHandler','Paginator','Session', 'Institution.InsCommon', 'Institution.InsEmail', 'Image', 'Quickblox', 'Cache');

	public $uses = array(
		'AdminUser',
		'EmailTemplate',
		'Institution.User',
		'Institution.EnterpriseUserList',
		'Institution.UserEmployment',
		'Institution.AdminUser',
		'Institution.CompanyName',
		'Institution.UserSubscriptionLog',
		'Institution.NotificationBroadcastMessage',
		'Institution.UserQbDetail',
		'Institution.UserDutyLog',
		'Institution.UserOneTimeToken',
		'Institution.CacheLastModifiedUser',
		'Institution.ExportChatTransaction',
		'Institution.UserProfile',
		'Institution.QrCodeRequest',
		'Institution.TrustAdminActivityLog',
		'Institution.UserLoginTransaction',
		'Institution.userPermanentRole',
		'Institution.UserBatonRole',
		'Institution.Countries',
		'Institution.OncallAccessProfession'
	);
	public $helpers = array('Form','Html', 'Js', 'Time','Paginator');


	public function index($type=null) {
		$this->checkAdminRole();
		if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$companyId = $_COOKIE['adminInstituteId'];
			$groupAdmin = $_COOKIE['instituteGroupAdminId'];
			$cust_data = array();
			$custom_condition = array();
			$count = 0;
			if($groupAdmin != 0){
				$custom_condition = array('User.id != ' =>(int)$groupAdmin);
			}
			//*********** Active Users & New Users Registred Within last seven days **************//

			$conditions_newUser = array(
			        'UserEmployment.company_id' => $companyId,
			        'User.status' => 1,
			        'User.approved' => 1,
			        'DATE(User.registration_date) > DATE_SUB(NOW(), INTERVAL 7 DAY)'
			    );

			$conditions_final_neUser = array_merge($custom_condition,$conditions_newUser);
			$count = $this->User->find('all',array(
			                'conditions' => $conditions_final_neUser,
			                'fiels' => array('user.id'),
			                'joins'=> array(
			                    array(
			                        'table' => 'user_employments',
			                        'alias' => 'UserEmployment',
			                        'type' => 'left',
			                        'conditions'=> array('UserEmployment.user_id = User.id')
			                    )
			                ),
			            ));
			$cust_data['new_users'] = count($count);

			//*********** Active UserList **************//

			$limit = 20;
			$newArr = array();

			$fields = array('User.id,User.email,EnterpriseUserList.status');
			$conditions = array(
					'UserEmployment.company_id' => $companyId,
					'UserEmployment.is_current' => 1,
					'UserEmployment.status' => 1,
					'User.status' => 1,
					'User.approved' => 1,
					'EnterpriseUserList.status' => array(0,1),
				);

			$conditions_final = array_merge($custom_condition,$conditions);
			$joins = array(
				array(
					'table' => 'user_employments',
					'alias' => 'UserEmployment',
					'type' => 'left',
					'conditions'=> array('UserEmployment.user_id = User.id')
				),
				array(
					'table' => 'user_subscription_logs',
					'alias' => 'UserSubscriptionLog',
					'type' => 'left',
					'conditions'=> array('User.id = UserSubscriptionLog.user_id')
				),
				array(
					'table' => 'enterprise_user_lists',
					'alias' => 'EnterpriseUserList',
					'type' => 'left',
					'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
				)
			);

			$options = array(
				'conditions' => $conditions_final,
				'joins' => $joins,
				'fields' => $fields,
				'group' => array('UserEmployment.user_id'),
				'order' => array('User.registration_date DESC'),
			);

			$userList = $this->User->find('all',$options);
			$subscribedUser = 0;
			$pendingUser = 0;

			foreach ($userList AS  $userListdata) {
			    if($userListdata['EnterpriseUserList']['status'] == 1){
			    	$subscribedUser++;
			    }else if($userListdata['EnterpriseUserList']['status'] == 0){
			    	$pendingUser++;
			    }
			}

			$cust_data['all_users'] = $subscribedUser + $pendingUser;
			$cust_data['pending_users'] = $pendingUser;
			$cust_data['approved_users'] = $subscribedUser;

			$tCount=$cust_data['all_users'];
			if($type == null || $type == ''){
				$type = 'all';
			}

			// Admin activity logs [START]
			$activityData = array();
			$activityData['company_id'] = $_COOKIE['adminInstituteId'];
			$activityData['admin_id'] = $_COOKIE['adminUserId'];
			$activityData['action'] = 'Pre-approved Emails';
			$activityData['custom_data'] = 'View';
			$this->TrustAdminActivityLog->addActivityLog($activityData);
			// Admin activity logs [END]

			$this->set(array("data"=>$cust_data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount,'type'=>$type));
		}
	}

	public function userListFilter(){
		if(isset($_COOKIE['adminInstituteId']) && $_COOKIE['adminInstituteId'] != null){
			$companyId = $_COOKIE['adminInstituteId'];

			if(isset($this->data['search_text'])){
			    $text=$this->data['search_text'];
			    $search_condition = array(
			    	'LOWER(EnterpriseUserList.email) LIKE'=>strtolower('%'.$text.'%')
			    );
			}
			else{
			    $search_condition = array();
			}

			if(isset($this->data['page_number'])){
			    $page_number=$this->data['page_number'];
			}
			else{
			    $page_number=1;
			}

			if(isset($this->data['type'])){
				$val = $this->data['type'];
				if($val == 'all'){
					$custom_condition = array();
				}else if($val == 1){
					$custom_condition = array('Users.status !='=>'');
				}else{
					$custom_condition = array('Users.status'=>'');
				}
			}else{
				$custom_condition = array();
			}

			if(isset($this->data['limit'])){
			    $limit=$this->data['limit'];
			}
			else{
			    $limit=20;
			}

			$conditions = array(
					'EnterpriseUserList.company_id' => $companyId,
					'EnterpriseUserList.source' => 1,
				);
			$conditions_final = array_merge($conditions,$search_condition,$custom_condition);
			$joins = array(
				array(
					'table' => 'users',
					'alias' => 'Users',
					'type' => 'left',
					'conditions'=> array('Users.email = EnterpriseUserList.email')
				)
			);
			$options = array(
				'conditions' => $conditions_final,
				'limit' => $limit,
				'joins' => $joins,
				'page'=> $page_number,
				'fields' => array('Users.email,Users.status,EnterpriseUserList.email,EnterpriseUserList.created,EnterpriseUserList.status'),
				'group' => array('EnterpriseUserList.email'),
				'order' => array('EnterpriseUserList.updated DESC'),
			);

			$this->Paginator->settings = $options;
			$userList = $this->Paginator->paginate('EnterpriseUserList');
			$newArr = array();
			foreach ($userList AS  $userListdata) {
				$signed = 0;
				if(isset($userListdata['Users']['email'])){
					$signed = 1;
				}
		    $newArr[] = array(
		    	'email' =>  $userListdata['EnterpriseUserList']['email'],
		    	'registration_date' => $userListdata['EnterpriseUserList']['created'],
		    	'status' =>  $userListdata['EnterpriseUserList']['status'],
		    	'registered' =>  $signed
		    );
			}

			$total = $this->request->params;
			$tCount = $total['paging']['EnterpriseUserList']['count'];

			$data['user_list'] = $newArr;

			$this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount));
			$this->render('/Elements/user/user_list');
		}
	}

	public function getDashCount(){
		$this->autoRender = false;
		if(isset($_COOKIE['adminInstituteId']) && $_COOKIE['adminInstituteId'] != null){
			$companyId = $_COOKIE['adminInstituteId'];

			if(isset($this->data['search_text'])){
			    $text=$this->data['search_text'];
			    $search_condition = array(
			    	'LOWER(EnterpriseUserList.email) LIKE'=>strtolower('%'.$text.'%')
			    );
			}
			else{
			    $search_condition = array();
			}
			$conditions = array(
					'EnterpriseUserList.company_id' => $companyId,
					'EnterpriseUserList.source' => 1,
				);
			$conditions_final = array_merge($conditions,$search_condition);
			$joins = array(
				array(
					'table' => 'users',
					'alias' => 'Users',
					'type' => 'left',
					'conditions'=> array('Users.email = EnterpriseUserList.email')
				)
			);
			$signedUsers = 0;
			$total_data = $this->EnterpriseUserList->find('all',array(
				'conditions' => $conditions_final,
				'joins' => $joins,
				'fields' => array('Users.email,EnterpriseUserList.email'),
				'group' => array('EnterpriseUserList.email'),
			));
			foreach ($total_data as $value) {
				if(isset($value['Users']['email'])){
					$signedUsers++;
				}
			}
			$data['signed_in'] = $signedUsers;
			$data['pending_in'] = count($total_data) - $signedUsers;
			$data['total'] = count($total_data);

			return json_encode($data);
		}
	}

	public function changeAssignStatus() {
		$this->autoRender = false;
		$responseData = array();
		$userEmail = $this->request->data['email'];
		$status = $this->request->data['status'];
		if(!empty($userEmail) && isset($_COOKIE['adminInstituteId'])){
			$companyId = $_COOKIE['adminInstituteId'];
			$userData = $this->EnterpriseUserList->find('first',array('conditions'=>array('email'=> $userEmail, 'company_id'=> $companyId)));
			if(!empty($userData)){
				$createdDate = date("Y-m-d H:i:s");
				$getUserDeatail = $this->User->find('first',array('conditions'=> array('email'=> $userEmail)));

				if(!empty($getUserDeatail)){
					$userId = $getUserDeatail['User']['id'];
					$params['user_id'] = $userId;
				    $responseUserEmployment = $this->getUserEmployment($userId,$companyId);
				    if($responseUserEmployment == 1){
				        $this->EnterpriseUserList->updateAll(array("status"=> $status), array("company_id"=> $companyId, "email"=> $userEmail));

				        $result = $this->updateUserSubscription($userId,$companyId,$status);
				        $updateCachevalue = $this->updateLastModifiedUserCache($userId, $status, $companyId);
				        if($result == 1){
				        	$adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $userEmail,'AdminUser.status'=>1)));
				        	if(!empty($adminUserData) && $status != 1){
				        	    $updateData = array(
				        	                    'status' => 0
				        	                );
				        	    $updateConditions = array("username"=> $userEmail);
				        	    $updateAdminUserMapping = $this->AdminUser->updateAll($updateData, $updateConditions);
				        	    $this->roleUpdationMail($userEmail,2);
				        	}
				        	if($status == 1){
				        		$responseData['message'] = 'User Approved Successfully';
				        	}else{
				        		$responseData['message'] = 'User Unapproved Successfully';
				        	}
    				    	$responseData['status'] = 1;
    	    				echo json_encode($responseData);

									try {
										if($status == 0){
											$this->dissmissDndOnOcrAndQb($params,'DND dismiss by ADMIN: '.$responseData['message']);
										}
									} catch (Exception $eDND) {

									}

    				    	try{
	    				    	// Admin activity logs [START]
	    				    	$activityData = array();
	    				    	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
	    				    	$activityData['admin_id'] = $_COOKIE['adminUserId'];
	    				    	$activityData['user_id'] = $userId;
	    				    	$activityData['action'] = $responseData['message'];
	    				    	$this->TrustAdminActivityLog->addActivityLog($activityData);
	    				    	// Admin activity logs [END]
	    				    } catch (Exception $e) {

	    				    }
				        }else{
    				    	$responseData['status'] = 0;
    	    	            $responseData['message'] = 'An error has occurred. Please try again.';
    	    				echo json_encode($responseData);
				        }
				    }else{
				    	$this->EnterpriseUserList->updateAll(array("status"=> $status), array("company_id"=> $companyId, "email"=> $userEmail));
				    	if($status == 1){
			        		$responseData['message'] = 'User Approved Successfully';
			        	}else{
			        		$responseData['message'] = 'User Unapproved Successfully';
			        	}
				    	$responseData['status'] = 1;
	    				echo json_encode($responseData);

							try {
								if($status == 0){
									$params['email'] = $userEmail;
									$this->dissmissDndOnOcrAndQb($params,'DND dismiss by ADMIN: '.$responseData['message']);
								}
							} catch (Exception $eDND) {

							}

				    	try{
					    	// Admin activity logs [START]
					    	$activityData = array();
					    	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
					    	$activityData['admin_id'] = $_COOKIE['adminUserId'];
					    	// $activityData['user_id'] = $userId;
					    	$activityData['action'] = $responseData['message'];
					    	$activityData['custom_data'] = '{"email" :"'.$userEmail.'"}';
					    	$this->TrustAdminActivityLog->addActivityLog($activityData);
					    	// Admin activity logs [END]
					    } catch (Exception $e) {

					    }
				    }
				}else{
					$this->EnterpriseUserList->updateAll(array("status"=> $status), array("company_id"=> $companyId, "email"=> $userEmail));

					if($status == 1){
		        		$responseData['message'] = 'User Approved Successfully';
		        	}else{
		        		$responseData['message'] = 'User Unapproved Successfully';
		        	}
			    	$responseData['status'] = 1;
    				echo json_encode($responseData);

			    	try{
				    	// Admin activity logs [START]
				    	$activityData = array();
				    	$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				    	$activityData['admin_id'] = $_COOKIE['adminUserId'];
				    	// $activityData['user_id'] = $userId;
				    	$activityData['action'] = $responseData['message'];
				    	$activityData['custom_data'] = '{"email" :"'.$userEmail.'"}';
				    	$this->TrustAdminActivityLog->addActivityLog($activityData);
				    	// Admin activity logs [END]
				    } catch (Exception $e) {

				    }
				}
			}
		}
	}

	public function exportChat($user_id) {
		$this->checkAdminRole();
		if($user_id != null){
		$userData = $this->User->find('first',
						array(
							'conditions' => array('User.id'=>$user_id,'User.status'=>1,'User.approved'=>1),
							'joins' => array(
		                		array(
									'table' => 'user_profiles',
									'alias' => 'userProfiles',
									'type' => 'left',
									'conditions'=> array('userProfiles.user_id = User.id')
								)
							),
						)
					);
		if(!empty($userData)){
			$user_name = $userData['UserProfile']['first_name'];
			$user_id = $userData['UserProfile']['user_id'];
		}

		// Admin activity logs [START]
		$activityData = array();
		$activityData['company_id'] = $_COOKIE['adminInstituteId'];
		$activityData['admin_id'] = $_COOKIE['adminUserId'];
		$activityData['user_id'] = $user_id;
		$activityData['action'] = 'Dialog List';
		$activityData['custom_data'] = 'View';
		$this->TrustAdminActivityLog->addActivityLog($activityData);
		// Admin activity logs [END]

		$this->set(array("name"=>$user_name,"id"=>$user_id));
		}
	}

	public function showDialogues() {
        $this->autoRender = false;
        $responseData = array();
        $customData = array();
        $is_import = "true";
        if($this->request->is('post')) {

            $companyId = $_COOKIE['adminInstituteId'];
            $userId = $this->request->data['userid'];

            // $inputFromDate = $this->request->data['fromDate'];
            // $inputToDate = $this->request->data['toDate'];
            // $reason = $this->request->data['reason'];

            $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));

            if(!empty($userData['User']['email'])){

                $oneTimeToken = $this->InsCommon->genrateRandomToken($userId);
                $oneTimeTokenData = array("user_id"=> $userId, "token"=> $oneTimeToken);
                $this->UserOneTimeToken->saveAll($oneTimeTokenData);

                $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], $oneTimeToken);
                // $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], '12345678');
                $token = $tokenDetails->session->token;
                $user_id = $tokenDetails->session->user_id;

                $dialogs = $this->Quickblox->quickGetDialogLite($token, '', '');
                $dialog_data = array();
                foreach ($dialogs->items AS  $dialog) {
									if($dialog->last_message != ''){
										$custom_class = '';
                    $image_url = '';
                    $is_image = '';

                    if(isset($dialog->photo) && (strpos($dialog->photo, 'http') !== false)){
                        $image_url = $dialog->photo;
                        $is_image = 'image';
                    }
                    if($dialog->type == '2'){
                        if(!empty($dialog->data) && $dialog->data != null && isset($dialog->data->Patient_name)){
                            $custom_class = 'patientMain';
                        }
                        if(isset($dialog->photo) && (strpos($dialog->photo, 'http') !== false)){
                            $image_url = $dialog->photo;
                            $is_image = 'image';
                        }else{
                            if($custom_class == 'patientMain'){
                                $image_url = BASE_URL.'institution/img/patientDefaultIcon.svg';
                                $is_image = 'image';
                            }else{
                                $image_url = BASE_URL.'institution/img/ava-group.png';
                                $is_image = 'image';
                            }
                        }
                    }
                    $dialog_data[] = array(
                        'dialog_name' => $dialog->name,
                        'image' => $image_url,
                        'custom_img_class' => $is_image,
                        'last_message' => $this->InsCommon->decryptData($dialog->last_message),
                        // 'last_message' => $dialog->last_message,
                        'message_time' => $dialog->last_message_date_sent,
                        'occupants' => (string)join(',',$dialog->occupants_ids),
                        'dialog_id' => (string)$dialog->_id,
                        'created_by' => (string)$dialog->user_id,
                        'pat_class' => $custom_class,
                    );
									}
                }
                $this->set(array("data"=>$dialog_data));
                $this->render('/Elements/user/dialog_list');


            }else{
                $responseData['status'] = 0;
                $responseData['message'] = 'User is not active.';
                echo json_encode($responseData);

            }
        }else{
            $responseData['status'] = 0;
            $responseData['message'] = 'Some technical error occurred. Please try again.';
            echo json_encode($responseData);

        }
    }

    public function exportForm($dialog_id){
			$this->checkAdminRole();
    	$user_id = $_COOKIE['user_id'];

    	// Admin activity logs [START]
        $activityData = array();
        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
        $activityData['admin_id'] = $_COOKIE['adminUserId'];
        $activityData['action'] = 'Dialog click to export (Export form)';
        $activityData['custom_data'] = '{"dialog_id" :"'.$dialog_id.'"}';
        $this->TrustAdminActivityLog->addActivityLog($activityData);
        // Admin activity logs [END]

        $this->set(array('dialogue_id'=>$dialog_id,'user_id'=>$user_id));
    }

	public function showUserChats($dialog_id) {
		$this->checkAdminRole();
		if(isset($dialog_id) && $_COOKIE['id']){
			$inputFromDate = $_COOKIE['from_date'];
			$inputToDate = $_COOKIE['to_date'];
			$reason = (string)$_COOKIE['reason'];
			$companyId = $_COOKIE['adminInstituteId'];
			$adminId = $_COOKIE['instituteGroupAdminId'];
            $adminEmail = $_COOKIE['adminEmail'];
            $userId = $_COOKIE['id'];
            $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));

            if(!empty($userData['User']['email'])){
            	$userDetails = $this->User->find("first", array("conditions"=> array("User.email"=> $adminEmail)));

            	$title = $userDetails['UserProfile']['first_name'];
            	$chat_user_id = $userData['UserProfile']['user_id'];
            	// $user_country_id = $userData['UserProfile']['country_id'];
							$user_country_id = $_COOKIE['adminCountryId'];


            	$oneTimeToken = $this->InsCommon->genrateRandomToken($chat_user_id);
            	$oneTimeTokenData = array("user_id"=> $chat_user_id, "token"=> $oneTimeToken);
            	$this->UserOneTimeToken->saveAll($oneTimeTokenData);

                $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], $oneTimeToken);
                // $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], '12345678');
                $token = $tokenDetails->session->token;
                $user_id = $tokenDetails->session->user_id;

                $dialogs = $this->Quickblox->quickGetDialogData($token,$dialog_id);
                $dialog_data = array();
                $occupantsId = '';
                $chatCreatedBy = '';
                $dialogue_cust_data = [];
                foreach ($dialogs->items AS  $dialog) {
                	$dialogue_name = $dialog->name;
                	$occupantsId = $dialog->occupants_ids;
            		$chatCreatedBy = (string)$dialog->user_id;
            		if(isset($dialog->data)){
            			$dialogue_cust_data = $dialog->data;
            		}
                }

                $patient_details = $this->formatPatientdata($dialogue_cust_data);

                $response_data = $this->getuserMessage($chatCreatedBy,$occupantsId,$token,$dialog_id,$inputFromDate, $inputToDate,$patient_details,$user_country_id);

                $transactionData = array(
                	'user_id_from'=>$adminId,
                	'user_id_to'=>$userId,
                	'company_id'=>$companyId,
                	'from_date'=>date('y-m-d H:i:s',$inputFromDate),
                    'to_date'=>date('y-m-d H:i:s',$inputToDate),
                	'dialog_id'=>$dialog_id,
                	'export_type'=>0,
                	'reason'=>$reason,
                	'mail_sent'=>0,
                );
                $transaction = $this->ExportChatTransaction->saveAll($transactionData);
                $transaction_id = $this->ExportChatTransaction->getLastInsertId();

                // Admin activity logs [START]
                $activityData = array();
                $activityData['company_id'] = $_COOKIE['adminInstituteId'];
                $activityData['admin_id'] = $_COOKIE['adminUserId'];
                $activityData['action'] = 'Chat messages display';
                $activityData['custom_data'] = '{"dialog_id" :"'.$dialog_id.'","from" :"'.date('y-m-d H:i:s',$inputFromDate).'","to" :"'.date('y-m-d H:i:s',$inputToDate).'"}';
                $this->TrustAdminActivityLog->addActivityLog($activityData);
                // Admin activity logs [END]

        		$this->set(array("data"=>$response_data,'title'=>$title,'user_id'=>$chat_user_id,'from_date' =>$inputFromDate,'to_date' => $inputToDate,'dialogue_name' =>$dialogue_name,'dialog_id'=>$dialog_id,'transaction_id'=>$transaction_id));
            }else{
				$this->redirect(BASE_URL . 'institution/InstitutionHome/index');
            }
		}else{
			$this->redirect(BASE_URL . 'institution/InstitutionHome/index');
		}
	}

	public function chatExport() {
		$this->autoRender = false;
        $responseData = array();
        if($this->request->is('post')) {
            $companyId = $_COOKIE['adminInstituteId'];
            $instituteAdmin = $_COOKIE['adminEmail'];
            $userId = (string)$this->request->data['user_id'];
            $dialog_id = (string)$this->request->data['dialogue_id'];
            $transaction_id = $this->request->data['transaction_id'];

            $from_date = (string)$this->request->data['from_date'];
            $to_date = (string)$this->request->data['to_date'];
            $chat_message = unserialize($this->request->data['chat_data']);

            $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
            if(!empty($userData['User']['email'])){
            	$title = $userData['UserProfile']['first_name'];
            	$chat_user_id = $userData['UserProfile']['user_id'];

        		$oneTimeToken = $this->InsCommon->genrateRandomToken($chat_user_id);
        		$oneTimeTokenData = array("user_id"=> $chat_user_id, "token"=> $oneTimeToken);
        		$this->UserOneTimeToken->saveAll($oneTimeTokenData);

        	    $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], $oneTimeToken);
        	    // $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], '12345678');
        	    $token = $tokenDetails->session->token;
        	    $user_id = $tokenDetails->session->user_id;

        	    $dialogs = $this->Quickblox->quickGetDialogData($token,$dialog_id);
        	    $dialog_data = array();
        	    $occupantsId = '';
        	    $chatCreatedBy = '';
        	    foreach ($dialogs->items AS  $dialog) {
        	    	$dialogue_name = $dialog->name;
        	    	$occupantsId = $dialog->occupants_ids;
        			$chatCreatedBy = (string)$dialog->user_id;
        	    }
        	    $userDetails = $this->User->find("first", array("conditions"=> array("User.email"=> $instituteAdmin)));

            	$details["user_id"] = $userDetails['UserProfile']['user_id'];
            	$details["dialog_name"] = $dialogue_name;
            	$details["email"] = $userDetails['User']['email'];
            	$details["first_name"] = $userDetails['UserProfile']['first_name'];
            	$details["country_id"] = $userDetails['UserProfile']['country_id'];
            	$details["created_by"] = $chatCreatedBy;
            	$details["occupants_id"] = $occupantsId;
            	$details["from_date"] = $from_date;
            	$details["to_date"] = $to_date;

            	$createPdf = $this->createPdf($chat_message,$details);

            	if(!empty($createPdf[0])){
            	    if(!empty($userDetails)){
            	    	$email = $userDetails['User']['email'];
            	    	$name = $userDetails['UserProfile']['first_name'];
            	    	$chatHistoryParams['toMail']= $email;
            	    	$chatHistoryParams['name']= $name;
            	    	$chatHistoryParams['chatHistoryFileName'] = $createPdf[0];
            	    	chmod(APP . 'webroot/chatHistory/'.$createPdf[0], 0777);
            	    	$chatHistoryParams['chatHistoryFilePath']= base64_encode(file_get_contents(BASE_URL.'/chatHistory/'.$createPdf[0]));
            	    	$chatHistoryParams['conversationName'] = $dialogue_name;
            	    	$chatHistoryParams['fromDate'] = date('d-m-Y', $from_date);
            	    	$chatHistoryParams['toDate'] = date('d-m-Y', $to_date);
            	    	$chatHistoryParams['emailSubject'] = "Chat history from Medic Bleep";
            	    	$sendChatHistoryMail = $this->InsEmail->sendChatHistory($chatHistoryParams);
            	    	$this->ExportChatTransaction->updateAll(array("mail_sent"=> 1), array('id'=>$transaction_id));
            	    }
            	    // $responseData = array('method_name'=> 'chatHistoryPdfSend', 'status'=>"1", 'response_code'=> '200', 'message'=> 'success');
            	    //$this->ChatHistoryScheduler->updateAll(array("mail_response"=> "'".$sendChatHistoryMail."'"), array("id"=>$tableId));
            	    //** Remove pdf file after sending mail
            	    unlink( APP . 'webroot/chatHistory/'.$createPdf[0] );
            	}

		    	$responseData['status'] = 1;
	            $responseData['message'] = 'ok.';
				echo json_encode($responseData);

	            try{
		            // Admin activity logs [START]
		            $activityData = array();
		            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		            $activityData['admin_id'] = $_COOKIE['adminUserId'];
		            $activityData['action'] = 'Chat Export PDF';
		            $activityData['custom_data'] = '{"dialog_id" :"'.$dialog_id.'","from" :"'.date('y-m-d H:i:s',$from_date).'","to" :"'.date('y-m-d H:i:s',$to_date).'"}';
		            $this->TrustAdminActivityLog->addActivityLog($activityData);
		            // Admin activity logs [END]
		        } catch (Exception $e) {

		        }
            }
        }
	}

	public function createPdf($chatData, $details){
		set_time_limit(180);
		ini_set('memory_limit','256M');

		App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));
		App::import('Vendor','FPDI',array('file' => 'fpdi/fpdi.php'));
		// create a PDF object
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$fpdi = new FPDI();

		$name = $details["first_name"];
		$loggedinUserCountryId = $details["country_id"];
		$conversationName = $details['dialog_name'];
		$fromDate = $details['from_date'];
		$toDate = $details['to_date'];
		$chatCreatedBy = $details['created_by'];
		$occupantsId = $details['occupants_id'];

		if($loggedinUserCountryId==226){
		    // date_default_timezone_set('Europe/Amsterdam');
		    date_default_timezone_set("Europe/London");
		}else if($loggedinUserCountryId==99){
		    date_default_timezone_set('Asia/Kolkata');
		}

		//** Chat Created By Id[START]
		$userByQbId = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $chatCreatedBy)));
		    if(!empty($userByQbId)){
		        $chatCreatedByUserId = $userByQbId["UserQbDetail"]['user_id'];
		    }
		//** Chat Created By Id[END]

		// Chat pdf header details [START] ---------------------------

		$textHtml = '<table cellspacing="0" cellpadding="2" color="#1B3B6A">';
		$textHtml .= '<tr><td colspan=3 width="575px;">Hi '.$name.'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Please find below the chat history you requested.</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Medic Bleep</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Chat History:</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">'.date('d-m-Y', $fromDate).' to '.date('d-m-Y', $toDate).'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Chat Subject:</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">'.$conversationName.'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Trust Name:</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">'.$chatData['current_company'].'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';

		if(!empty($chatData['patient_data'])){
			$textHtml .= '<tr><td colspan=3 width="575px;">Patient Detail:</td></tr>';

		    $textHtml .= '<tr><td colspan=3 width="575px;"><br />
		                    Name: '. $chatData['patient_data']['name'] .'<br />';
		    if((string)$chatData['patient_data']['patient_number_type'] == '0'){
		    	$textHtml .= 'NHS Number: '.$chatData['patient_data']['nhs_number'].'<br />';
		    }else{
		    	$textHtml .= 'MRN Number: '.$chatData['patient_data']['nhs_number'].'<br />';
		    }

		    if($chatData['patient_data']['patient_dob'] !== ''){
		    	$textHtml .= 'Born: '.$chatData['patient_data']['patient_dob'].'<br />';
		    }else{
		    	$textHtml .= 'Age: '.$chatData['patient_data']['age'].'<br />';
		    }
		    $textHtml .= 'Gender: '.$chatData['patient_data']['gender'];
		    if($chatData['patient_data']['notes'] != ''){
		    	$textHtml .= '<br />Notes: '.nl2br($chatData['patient_data']['notes']);
		    }
		    $textHtml .= '</td></tr>';

			$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		}

		$textHtml .= '<tr><td colspan=3 width="575px;">Chat Participants:</td></tr>';
		$roleStatus = '';
		$count = 1;

		foreach ($chatData['occupants_data'] as $participants) {
			$textHtml .= '<tr><td colspan=3 width="575px;">'.$count.". ".$participants['first_name']." ".$participants['last_name'].", ".$participants['role_status'].'</td></tr>';
		    $count++;
		}
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		$textHtml .= '</table>';

		// Chat pdf header details [END] ---------------------------
		// Chat pdf Main Chats [START] ---------------------------

		$textHtml .= '<table cellspacing="2" cellpadding="5">';
		if(isset($chatData['message_data']) && count($chatData['message_data']) > 0){
		  	$date_color = '#10A4DA';
		  	$colour1 = '#C6EAFA';
		  	$colour2 = '#EEEEEF';
		  	$showDate = 1;
		  	$row_color = $colour1;

		  	for ($i=0; $i < count($chatData['message_data']); $i++) {
		    	if($i > 0){

			      	// condition for row color
			      	if($chatData['message_data'][$i-1]['id'] != $chatData['message_data'][$i]['id']){
			        	if($row_color == $colour1){
			          	$row_color = $colour2;
			        	}else{
			          	$row_color = $colour1;
			        	}
			      	}

			      	// condition to show date
			      	if($chatData['message_data'][$i-1]['created_date'] != $chatData['message_data'][$i]['created_date']){
			        	$showDate = 1;
			      	}else{
			        	$showDate = 0;
			      	}
		    	}

		    	if($showDate == 1){
		    		$textHtml .= '<tr bgcolor="#10A5DA">';
		    		$textHtml .= '<td colspan="3" width="575px;"><font color="white"><strong>' . 'Date: '.$chatData['message_data'][$i]['created_date']. '</strong></font></td>';
		    		$textHtml .= '</tr>';
		    	}

		    	$textHtml .= '<tr bgcolor="'.$row_color.'">';
		    	$textHtml .= '<td width="100px;">' .$chatData['message_data'][$i]['sender_name'] . '</td>';
		    	$textHtml .= '<td width="375px;">' . htmlspecialchars_decode($chatData['message_data'][$i]['messages']) . '</td>';
		    	$textHtml .= '<td width="100px;">' . $chatData['message_data'][$i]['timeStamp'] .'</td>';
		    	$textHtml .= '</tr>';

		    }
		}
		$textHtml .= '</table>';

		// Chat pdf Main Chats [END] ---------------------------
		$headerTemplate = APP . 'webroot/chatHistory/templates/header-single-v3.pdf';
		$files = array($headerTemplate);
		$pageCount = 0;
		//$pagesCount = 1;
		foreach($files AS $file) {
		       $pagecount = $fpdi->setSourceFile($file);
		       for ($i = 1; $i <= $pagecount; $i++) {
		            $tplidx = $fpdi->ImportPage($i);
		            $s = $fpdi->getTemplatesize($tplidx);
		            $fpdi->AddPage('P');
		            $fpdi->useTemplate($tplidx,null, 0, 0, 0, FALSE);
		            $fpdi->SetXY(3, 90);
		            $fpdi->SetFont('helvetica', '', 11);
		            $fpdi->writeHTML($textHtml, true, true, true, 0);
		            //** Write Footer

		            //$fpdi->writeHTML('<p align="center">Copyright 2017 Medic Creations. All Rights Reserved</p>', true, true, true, 0);
		       }
		  }

		$finalFileName = 'Chathistory' . strtotime(date('Y-m-d H:i:s')) . '.pdf';
		//$finalFileName = 'chatHistory.pdf';
		$output = $fpdi->Output(APP . 'webroot/chatHistory/'.$finalFileName, 'F');
		// sleep(60);
		// chmod(APP . 'webroot/chatHistory/'.$finalFileName, 0777);
		// unlink( $finalFileName );
		return array($finalFileName);

	}

	public function getuserMessage($chatCreatedBy,$occupantsId,$token,$dialogue_id,$inputFromDate, $inputToDate,$patient_details,$loggedinUserCountryId){
	    //** Chat Created By Id[START]
	    $companyId = $_COOKIE['adminInstituteId'];
		$groupAdmin = $_COOKIE['instituteGroupAdminId'];
	    $userByQbId = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $chatCreatedBy)));
	        if(!empty($userByQbId)){
	            $chatCreatedByUserId = $userByQbId["UserQbDetail"]['user_id'];
	        }
	    //** Chat Created By Id[END]
	        if($loggedinUserCountryId==226){
	            // date_default_timezone_set('Europe/Amsterdam');
	            date_default_timezone_set("Europe/London");
	        }else if($loggedinUserCountryId==99){
	            date_default_timezone_set('Asia/Kolkata');
	        }
	    //** Chat Participants details[START]
	    $participantIdsData = $this->UserQbDetail->find("list", array("fields"=>array("user_id"),"conditions"=> array("qb_id"=>$occupantsId)));

	    // return $participantIdsData;

	    $participantIds = array_unique($participantIdsData);

	    //** Created By Institution[START]
	    $userCurrentCompanyName = "";
	    if($companyId == -1){
	        $userCurrentCompanyName = "None";
	    }else if(!empty($companyId)){
	        $userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $companyId, "status"=> 1, "created_by"=> 0)));
	        if(!empty($userCompanyDetails)){
	            $userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
	        }
	    }
	    //** Created By Institution[END]

	    $roleStatus = '';
	    $count = 1;
	    $participantIdsArr = [];
	    $newArr = array();
	    foreach ($participantIds AS  $participantId) {
	    	if($groupAdmin != $participantId){
	    		$chatParticipantsDetails = $this->getParticipentDetail($participantId);
	    		if(!empty($chatParticipantsDetails)){
	    		    if(!empty($chatParticipantsDetails['UserProfile']['role_status']))
	    		    {
	    		        $roleStatus = $chatParticipantsDetails['UserProfile']['role_status'];
	    		    }
	    		    else if(!empty($chatParticipantsDetails['Profession']['profession_type']))
	    		    {
	    		        $roleStatus = $chatParticipantsDetails['Profession']['profession_type'];
	    		    }
	    		    else
	    		    {
	    		        $roleStatus = "NA";
	    		    }
	    		}
	    		$newArr[] = array('first_name'=> $chatParticipantsDetails['UserProfile']['first_name'], 'last_name'=> $chatParticipantsDetails['UserProfile']['last_name'],'role_status'=>$roleStatus);
	    	}
	    }

        $chatMessageDetail = $this->Quickblox->quickGetMessageLite($token, $dialogue_id, $inputFromDate, $inputToDate);
        $messageResult = json_decode(json_encode($chatMessageDetail));
        $messageArr = array();

        foreach ($messageResult->items as  $message) {
            // $created_at = date("d-m-Y", strtotime(str_replace('/','-',substr(trim($message->created_at),0,10))));
						date_default_timezone_set('UTC');
            $created_at = strtotime($message->created_at);
            if($loggedinUserCountryId==226){
                // date_default_timezone_set('Europe/Amsterdam');
                date_default_timezone_set("Europe/London");
            }else if($loggedinUserCountryId==99){
                date_default_timezone_set('Asia/Kolkata');
            }
            $created_at = date("d-m-Y", $created_at);

            $senderName = "";
            $userDetailsSender = $this->userDetailsByQbId($message->sender_id);
            if(!empty($userDetailsSender)){
                $senderName = $userDetailsSender["UserProfile"]['first_name'];
            }

            $messages = "";
            if( !empty($message->message)){
                if(!empty($message->notification_type)){
                    $messages = $this->formatNotification($message);
                }else{
                	$messages = $this->formatMessage($message);
                }
                $timeStamp = date("H:i", $message->date_sent);

                $messageArr[] = array('id'=>$message->sender_id,'created_date'=> $created_at, 'sender_name'=> $senderName,'messages'=>$messages,'timeStamp'=>$timeStamp);
            }
        }

        $responseData = array('message_data'=> $messageArr,'occupants_data'=>$newArr, 'current_company'=>$userCurrentCompanyName,'patient_data'=>$patient_details);
	    return $responseData;
	}

	public function userDetailsByQbId($qbId=NULL){
        $userDetails = array();
        if(!empty($qbId)){
            $userByQbId = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $qbId)));
            if(!empty($userByQbId['UserQbDetail']['user_id'])){
                $userDetails = $this->User->userDetailsById($userByQbId['UserQbDetail']['user_id']);
            }
        }
        return $userDetails;
    }

    public function formatPatientdata($cust_data){
    	$patientData = array();
    	if(!empty($cust_data) && $cust_data != null && isset($cust_data->Patient_name)){
    		$patientData = array(
    			'name' => $this->InsCommon->decryptData($cust_data->Patient_name),
    			'nhs_number' => $this->InsCommon->decryptData($cust_data->Patient_nhs_number),
    			'age' => $this->InsCommon->decryptData($cust_data->Patient_age),
    			'gender' => $this->InsCommon->decryptData($cust_data->Patient_gender),
    			'notes' => isset($cust_data->Patient_condition)?$this->InsCommon->decryptData($cust_data->Patient_condition):'',
    			'patient_number_type' => isset($cust_data->Patient_number_type)?$cust_data->Patient_number_type:0,
    			'patient_dob' => isset($cust_data->Patient_dob)?$this->InsCommon->decryptData($cust_data->Patient_dob):''

    			// 'name' => $this->InsCommon->decryptData($cust_data->Patient_name),
    			// 'nhs_number' => $this->InsCommon->decryptData($cust_data->Patient_nhs_number),
    			// 'age' => $this->InsCommon->decryptData($cust_data->Patient_age),
    			// 'gender' => $this->InsCommon->decryptData($cust_data->Patient_gender),
    			// 'notes' => isset($cust_data->Patient_condition)?$this->InsCommon->decryptData($cust_data->Patient_condition):''

    			// 'name' => $cust_data->Patient_name,
    			// 'nhs_number' => $cust_data->Patient_nhs_number,
    			// 'age' => $cust_data->Patient_age,
    			// 'gender' => $cust_data->Patient_gender,
    			// 'notes' => isset($cust_data->Patient_condition)?$cust_data->Patient_condition:''
    		);
    	}
    	return $patientData;
    }

    public function formatNotification($message=NULL){
        $returnMsg = "Notification Message";
        $contact_custom_msg = '';
        $notifyType = $message->notification_type;
        if(isset($message->contact_custom_msg) && $message->contact_custom_msg != ''){
        	$contact_custom_msg = $message->contact_custom_msg;
        }
        if(!empty($notifyType)){
            switch($notifyType){
                case 1:
                        $returnMsg = "Group Created";
                        break;
                case 2:
                        $returnMsg = "Group Updated";
                        break;
                case 4:
                        $returnMsg = "Contact Request<br />".$contact_custom_msg;
                        break;
                case 5:
                        $returnMsg = "Contact Request Accepted";
                        break;
                case 6:
                        $returnMsg = "Contact Request Rejected";
                        break;
                case 7:
                        $returnMsg = "Contact Request Removed";
                        break;
                case 101:
                        $returnMsg = "Missed Call";
                        break;
                case 102:
                        $returnMsg = "Call Not Responded";
                        break;
                case 103:
                        $returnMsg = "Call Rejected";
                        break;
                case 104:
                        $returnMsg = "Missed Audio Call";
                        break;
                case 105:
                        $returnMsg = "Audio Call Not Responded";
                        break;
                case 106:
                        $returnMsg = "Missed Video Call";
                        break;
                case 107:
                        $returnMsg = "Video Call Not Responded";
                        break;
                case 108:
                        $returnMsg = "Took a screenshot of this chat.";
                        break;
                default:
                        $returnMsg = "Notification Message";
                        break;
            }
        }
        return $returnMsg;
    }

    public function formatMessage($message = null) {
    	$forward_message_data = array();
    	$reply_message_data = array();
    	$senderName = '';
    	$forward_user = '';
    	$forward_attac = '';
    	$messages = "";

    	$userDetailsSender = $this->userDetailsByQbId($message->sender_id);
    	if(!empty($userDetailsSender)){
            $senderName = $userDetailsSender["UserProfile"]['first_name'];
        }

    	if(isset($message->is_forwarded_msg) && $message->is_forwarded_msg == 'Yes'){
    		$forward_message_data = json_decode($message->forwarded_msg_user_info,true);
    	}

    	if(isset($message->is_reply_msg) && $message->is_reply_msg == 'Yes'){
    		$reply_message_data = json_decode($message->reply_msg_user_info,true);
    		if(isset($reply_message_data['reply_attachment']) && gettype($reply_message_data['reply_attachment'])=='string'){
    			$reply_message_data['reply_attachment'] = json_decode($reply_message_data['reply_attachment'],true);
    		}
    	}

    	if(!empty($forward_message_data)){
    		if((string)$message->sender_id == (string)$forward_message_data['forward_msg_user_id']){
    			$forward_user = 'own';
    		}else{
		        $forward_user = $forward_message_data['forward_msg_sender_name']."'s";
    		}

    		if(count($message->attachments) > 0){
    			$forward_attac_type = $this->getAttachmentType($message->attachments[0]->type);
    			$forward_attac_url = QB_API_ENDPOINT.'blobs/'.$message->attachments[0]->id;
    			$forward_message = '<a target="_blank" href="'.$forward_attac_url.'">'.$forward_attac_type.'</a>';
    		}else{
    			$forward_message = $this->InsCommon->decryptData($forward_message_data['forward_msg_message']);
    			// $forward_message = $forward_message_data['forward_msg_message'];
    		}

    		if(isset($message->isEncrypted) && $message->isEncrypted=='Yes'){
    		    $main_message = $this->InsCommon->decryptData($message->message);
    		    // $main_message = $message->message;
    		}else{
    		    $main_message = $message->message;
    		}

    		$messages = $senderName.' forwarded '.$forward_user." message<br>".$forward_message."<br><br>".$senderName."'s Message: ".$main_message;
    	}else if(!empty($reply_message_data)){
    		if((string)$message->sender_id == (string)$reply_message_data['reply_msg_user_id']){
    			$reply_user = 'own';
    		}else{
		        $reply_user = $reply_message_data['reply_msg_sender_name']."'s";
    		}

    		if(count($message->attachments) > 0){
    			$reply_main_attac_type = $this->getAttachmentType($message->attachments[0]->type);
    			$reply_main_attac_url = QB_API_ENDPOINT.'blobs/'.$message->attachments[0]->id;
    			$main_message = '<a target="_blank" href="'.$reply_main_attac_url.'">'.$reply_main_attac_type.'</a>';
			}else{
    			if(isset($message->isEncrypted) && $message->isEncrypted=='Yes'){
    			    $main_message = $this->InsCommon->decryptData($message->message);
    			    // $main_message = $message->message;
    			}else{
    			    $main_message = $message->message;
    			}
    		}

    		if(isset($reply_message_data['reply_attachment']) && $reply_message_data['reply_is_media'] == 'Yes'){
    			$reply_attac_type = $this->getAttachmentType($reply_message_data['reply_attachment']['type']);
    			$reply_attac_url = QB_API_ENDPOINT.'blobs/'.$reply_message_data['reply_attachment']['id'];
    			$reply_message = '<a target="_blank" href="'.$reply_attac_url.'">'.$reply_attac_type.'</a>';
    			// $reply_message = '';
    		}else{
    			$reply_message = $this->InsCommon->decryptData($reply_message_data['reply_msg_message']);
    			// $reply_message = $reply_message_data['reply_msg_message'];
    		}

    		$messages = $senderName.' replied '.$reply_user." message<br>".$reply_message."<br><br>".$senderName."'s Reply: ".$main_message;
    	}else{
    		if(count($message->attachments) > 0){
    			$attac_type = $this->getAttachmentType($message->attachments[0]->type);
    			$attac_url = QB_API_ENDPOINT.'blobs/'.$message->attachments[0]->id;
    			$messages = '<a target="_blank" href="'.$attac_url.'">'.$attac_type.'</a>';
    		}else{
    			if(isset($message->isEncrypted) && $message->isEncrypted=='Yes'){
    			    $messages = $this->InsCommon->decryptData($message->message);
    			    // $messages = $message->message;
    			}else{
    			    $messages = $message->message;
    			}
    		}
    	}
    	return $messages;
    }

    public function getAttachmentType($attachment_type){

    	$attachment_type = strtolower($attachment_type);

	    if($attachment_type=="video"){
    	    $attachmentName = "Video Attachment";
    	}else if($attachment_type=="mp4"){
    	    $attachmentName = "Video Attachment";
    	}else if($attachment_type=="aac"){
    	    $attachmentName = "Audio Attachment";
    	}else if($attachment_type=="mp3"){
    	    $attachmentName = "Audio Attachment";
    	}else if($attachment_type=="audio"){
    	    $attachmentName = "Audio Attachment";
    	}else if($attachment_type=="photo"){
    	    $attachmentName = "Image Attachment";
    	}else if($attachment_type=="image"){
    	    $attachmentName = "Image Attachment";
    	}else if($attachment_type=="attachment image"){
    	    $attachmentName = "Image Attachment";
    	}else if(in_array($attachment_type,array("pdf","doc","docx","xls","xlsx","ppt","pptx"))){
    	    $attachmentName = "Document Attachment";
    	}else{
    		$attachmentName = "Attachment";
    	}
    	return $attachmentName;
    }

    public function getParticipentDetail($participantId){
        $conditions = "UserProfile.user_id = $participantId";
        $userLists = $this->UserProfile->find('first',
                            array(
                                'joins'=>
                                array(
                                    array(
                                        'table' => 'professions',
                                        'alias' => 'Profession',
                                        'type' => 'left',
                                        'conditions'=> array('UserProfile.profession_id = Profession.id')
                                    ),
                                ),
                                'conditions' => $conditions,
                                'fields' => array('UserProfile.first_name', 'UserProfile.last_name','UserProfile.role_status', 'Profession.profession_type')
                            )
                        );
        return  $userLists;
    }

	public function getUserEmployment($userId, $companyId){
	    $result = 0;
	    $getUserEmploymentData = $this->UserEmployment->find('first',
	                array('conditions'=>
	                  array(
	                    'user_id'=> $userId,
	                    'company_id' => $companyId,
	                    'is_current' => 1
	                    )
	                ));
	    if(!empty($getUserEmploymentData)){
	      $result = 1;
	    }
	    return $result;
	}

	public function updateUserSubscription($userId, $companyId, $status){
	    $result = 0;
	    $getCompanyDetail = $this->CompanyName->find('first',
	                array('conditions'=>
	                  array(
	                    'id'=> $companyId
	                    )
	                ));
	    if(!empty($getCompanyDetail)){

	    	if($status == 1){
	    		$newSubscriptionPeriod = date('Y-m-d H:i:s', strtotime($getCompanyDetail['CompanyName']['subscription_expiry_date']));
	        }else{
	        	// $newSubscriptionPeriod = date("Y-m-d", strtotime("+1 month"));
	        	$newSubscriptionPeriod = date("Y-m-d", strtotime("0 day"));
	        	$status == 3;
	        }
	      	$this->UserSubscriptionLog->updateAll(array("subscription_expiry_date"=>"'".$newSubscriptionPeriod."'", "subscribe_type"=>$status,"company_id"=>$companyId),array("user_id"=> $userId));
	      	$result = 1;
	    }

	    return $result;
	}

	public function updateLastModifiedUserCache($userId, $deletedStatus, $companyId){
	    $modifiedType = "deleted_user";
	    $lastModified = date('Y-m-d H:i:s');
	    $deletedStatus = ($deletedStatus == 0)?1:0;
	    if(!empty($userId)){
	        $getCacheLastModifiedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType)));
	        if($getCacheLastModifiedUser > 0){
	            $this->CacheLastModifiedUser->updateDeletedUser($userId, $companyId, $modifiedType, $deletedStatus);
	        }else{
	            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType, "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }

	        //** Update static cache[START]
	        $this->CacheLastModifiedUser->updateLastModifiedDate(0, $companyId, 'static_directory_changed');
	        //** Update static cache[END]

	        //** Set eTag values for static directory[START]
	        $paramsStaticEtagUpdate['key'] = 'staticDirectoryEtag:'.$companyId;
	        $this->updateStaticEtagData($paramsStaticEtagUpdate);
	        //** Set eTag values for static directory[END]

	        //** update dynamic cache[STATRT]
	        if($deletedStatus == 0){
	            $getDynamicChangedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed")));
	            if($getDynamicChangedUser > 0){
	                $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');
	            }else{
	                $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>$lastModified);
	                $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	            }

	            //** Get last modified date dynamic directory[START]
	            $getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
	            if(!empty($getDynamicDirectoryLastModified)){
	                $paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
	                $paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
	                $this->updateDynamicEtagData($paramsDynamicEtagUpdate);
	            }
	            //** Get last modified date dynamic directory[END]
	        }
	        //** update dynamic cache[END]

	        //******* Delete Cache in case of Delete, Activate, Deactivate[START] ********//
	        $cacObj = $this->Cache->redisConn();
	        if(($cacObj['connection']) && empty($cacObj['errors'])){
	            if($cacObj['robj']->exists('institutionDirectory:'.$companyId)){
	                $key = 'institutionDirectory:'.$companyId;
	                $cacObj = $this->Cache->delRedisKey($key);
	            }
	        }
	        //******* Delete Cache in case of Delete, Activate, Deactivate[END] ********//
	  }
	}

	public function deviceInfo($user_id = null){
		if(isset($_COOKIE['adminInstituteId'])){
			$this->checkAdminRole();
			// Admin activity logs [START]
			$activityData = array();
			$activityData['company_id'] = $_COOKIE['adminInstituteId'];
			$activityData['admin_id'] = $_COOKIE['adminUserId'];
			$activityData['action'] = 'Users Device Info';
			$activityData['custom_data'] = 'View';
			$this->TrustAdminActivityLog->addActivityLog($activityData);
			// Admin activity logs [END]

			$this->set(array('tCount'=>1,'user_id'=>$user_id));
		}
	}

	public function deviceFilter_view(){
		if(isset($this->data['user_id'])){
		    $user_id=$this->data['user_id'];
		}
		else{
		    $user_id=0;
		}

		$conditions = array(
		    'UserLoginTransaction.user_id' => $user_id
		);

		$device_data = array();
		$device_data['Web'] = $this->UserLoginTransaction->find('first',array('conditions'=>array('UserLoginTransaction.user_id'=>$user_id,'UserLoginTransaction.application_type'=>'MB-WEB'),'order'=>'UserLoginTransaction.id DESC'));
		$device_data['App'] = $this->UserLoginTransaction->find('first',array('conditions'=>array('UserLoginTransaction.user_id'=>$user_id,'UserLoginTransaction.application_type'=>'MB'),'order'=>'UserLoginTransaction.id DESC'));
		$device_array = array();
		if(isset($device_data)){
			foreach ($device_data as $device) {
				$device_all_info = array();
				if(isset($device['UserLoginTransaction']['device_info'])){
					$device_info = json_decode($device['UserLoginTransaction']['device_info'],true);
					if(!empty($device_info)){
						$device_info = array_change_key_case($device_info, CASE_LOWER);
						if(strtolower($device_info['device_type']) == 'mbweb'){
							$device_all_info =  $this->getBrowser($device_info['manufacturer']);
							$device_all_info['type'] = 'Web';
							$device_all_info['mb_version'] = $device_info['version'];
						}else{
							if(strtolower($device_info['device_type']) == 'android'){
								$device_all_info['type'] = 'Android';
								$device_all_info['device'] = $device_info['model'];
								$device_all_info['platform'] = 'Android '.$device_info['os_version'];
								$device_all_info['mb_version'] = $device_info['version'];
							}else{
								$device_all_info['type'] = 'iOS';
								$device_all_info['device'] = $device_info['device'];
								$device_all_info['platform'] = 'iOS '.$device_info['os_version'];
								$device_all_info['mb_version'] = $device_info['version'];
							}
						}
					}
					$device_all_info['status'] = $device['UserLoginTransaction']['login_status'];
					$device_all_info['login_date'] = $device['UserLoginTransaction']['login_date'];
				}
				$device_array[] = $device_all_info;
			}
		}
		$userList_final['UserLoginTransaction'] = $device_array;

		$data['user_list'] = $userList_final;
		$this->set(array("data"=>$data,'user_id'=>$user_id));
		$this->render('/Elements/user/device_view');
	}

	public function deviceFilter(){

		if(isset($this->data['user_id'])){
		    $user_id=$this->data['user_id'];
		}
		else{
		    $user_id=0;
		}

		if(isset($this->data['page_number'])){
		    $page_number=$this->data['page_number'];
		}
		else{
		    $page_number=1;
		}

		if(isset($this->data['limit'])){
		    $limit=$this->data['limit'];
		}
		else{
		    $limit=20;
		}

		if(isset($this->data['section'])){
				$section=$this->data['section'];
		}
		else{
				$section='all';
		}

		$conditions = array(
		    'UserLoginTransaction.user_id' => $user_id
		);

		$options = array(
		    'conditions' => $conditions,
		    'limit' => $limit,
		    'page'=> $page_number,
		    'order' => 'UserLoginTransaction.id DESC',
		);
		$this->Paginator->settings = $options;
		$device_data = $this->Paginator->paginate('UserLoginTransaction');

		$device_array = array();
		if(isset($device_data)){
			foreach ($device_data as $device) {
				$device_all_info = array();
				if(isset($device['UserLoginTransaction']['device_info'])){
					$device_info = json_decode($device['UserLoginTransaction']['device_info'],true);
					if(!empty($device_info)){
						$device_info = array_change_key_case($device_info, CASE_LOWER);
						if(strtolower($device_info['device_type']) == 'mbweb'){
							$device_all_info =  $this->getBrowser($device_info['manufacturer']);
							$device_all_info['type'] = 'Web';
							$device_all_info['mb_version'] = $device_info['version'];
						}else{
							if(strtolower($device_info['device_type']) == 'android'){
								$device_all_info['type'] = 'Android';
								$device_all_info['device'] = $device_info['model'];
								$device_all_info['platform'] = 'Android '.$device_info['os_version'];
								$device_all_info['mb_version'] = $device_info['version'];
							}else{
								$device_all_info['type'] = 'iOS';
								$device_all_info['device'] = $device_info['device'];
								$device_all_info['platform'] = 'iOS '.$device_info['os_version'];
								$device_all_info['mb_version'] = $device_info['version'];
							}
						}
					}
					$device_all_info['status'] = $device['UserLoginTransaction']['login_status'];
					$device_all_info['login_date'] = $device['UserLoginTransaction']['login_date'];
				}
				$device_array[] = $device_all_info;
			}
		}
		$userList_final['UserLoginTransaction'] = $device_array;

		$total = $this->request->params;
		$tCount = $total['paging']['UserLoginTransaction']['count'];

		$data['user_list'] = $userList_final;
		$this->set(array("data"=>$data,"condition"=>$conditions,"limit"=>$limit,'tCount'=>$tCount,'section'=>$section,'user_id'=>$user_id));
		$this->render('/Elements/user/device_filter');
	}

	public function getBrowser($u_agent = null){
	    if($u_agent !== null){
	    	$bname = 'Unknown';
	    	$platform = 'Unknown';
	    	$version= "";
	    	$p_version= "";

	    	// Next get the name of the useragent yes seperately and for good reason
	    	if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
	    	{
	    	    $bname = 'Internet Explorer';
	    	    $ub = "MSIE";
	    	}
	    	elseif(preg_match('/Edge/i',$u_agent))
	    	{
	    	    $bname = 'Microsoft Edge';
	    	    $ub = "Edge";
	    	}
	    	elseif(preg_match('/Firefox/i',$u_agent))
	    	{
	    	    $bname = 'Mozilla Firefox';
	    	    $ub = "Firefox";
	    	}
	    	elseif(preg_match('/Chrome/i',$u_agent))
	    	{
	    	    $bname = 'Google Chrome';
	    	    $ub = "Chrome";
	    	}
	    	elseif(preg_match('/Safari/i',$u_agent))
	    	{
	    	    $bname = 'Apple Safari';
	    	    $ub = "Safari";
	    	}
	    	elseif(preg_match('/Opera/i',$u_agent))
	    	{
	    	    $bname = 'Opera';
	    	    $ub = "Opera";
	    	}
	    	elseif(preg_match('/Netscape/i',$u_agent))
	    	{
	    	    $bname = 'Netscape';
	    	    $ub = "Netscape";
	    	}

	    	// finally get the correct version number
	    	$known = array('Version', $ub, 'other');
	    	$pattern = '#(?<browser>' . join('|', $known) .
	    	')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	    	if (!preg_match_all($pattern, $u_agent, $matches)) {
	    	    // we have no matching number just continue
	    	}

	    	// see how many we have
	    	$i = count($matches['browser']);
	    	if ($i != 1) {
	    	    //we will have two since we are not using 'other' argument yet
	    	    //see if version is before or after the name
	    	    if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
	    	        $version= $matches['version'][0];
	    	    }
	    	    else {
	    	        $version= $matches['version'][1];
	    	    }
	    	}
	    	else {
	    	    $version= $matches['version'][0];
	    	}

				if (preg_match('/linux/i', $u_agent)) {
				    $platform = 'linux';
				    $pb = 'linux';
				}
				elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
				    $platform = 'Mac OS';
				    $pb = 'macintosh|mac os x';
				}
				elseif (preg_match('/windows|win32|win64/i', $u_agent)) {
				    $platform = 'Windows';
				    $pb = 'windows|win32';
				}

				$known1 = array('platform', $pb, 'other');
				$pattern1 = '#(?<platform>' . join('|', $known1) .
				')[ ]+(?<version>[0-9.|a-zA-Z._ ]*)#';
				if (!preg_match_all($pattern1, strtolower($u_agent), $matches1)) {
				    // we have no matching number just continue
				}
				if($platform != 'Mac OS'){
				  if($matches1[0][0] == strtolower('Windows 95')|| $matches1[0][0] == strtolower('Win95')){
				    $p_version =  "Windows 95";
				  }
				  if($matches1[0][0] == strtolower('Windows 98')|| $matches1[0][0] == strtolower('Win98')){
				    $p_version =  "Windows 98 & 98 SE";
				  }
				  if($matches1[0][0] == strtolower('Windows CE')){
				    $p_version =  "Windows CE";
				  }
				  if($matches1[0][0] == strtolower('Windows 9x 4.90')){
				    $p_version =  "Windows ME";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 4.0')){
				    $p_version =  "Windows NT 4.0";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 5.0')){
				    $p_version =  "Windows 2000";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 5.1')){
				    $p_version =  "Windows XP";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 5.2')){
				    $p_version =  "Windows Server 2003 and XP x64 edition";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 6.0')){
				    $p_version =  "Windows Vista";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 6.1')){
				    $p_version =  "Windows 7";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 6.2')){
				    $p_version =  "Windows 8";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 6.3')){
				    $p_version =  "Windows 8.1";
				  }
				  if($matches1[0][0] == strtolower('Windows NT 10.0')){
				    $p_version =  "Windows 10";
				  }
				}else{
				  $j = count($matches1['platform']);
				  if ($j != 1) {
				      //we will have two since we are not using 'other' argument yet
				      //see if version is before or after the name
				      if (strripos($u_agent,"Version") < strripos($u_agent,$pb)){
				          $p_version= str_replace("_",".",$matches1['version'][0]);
				      }
				      else {
				          $p_version= str_replace("_",".",$matches1['version'][1]);
				      }
				  }
				  else {
				      $p_version= $p_version= str_replace("_",".",$matches1['version'][0]);
				  }
				}

	    	// check if we have a number
	    	if ($version==null || $version=="") {$version="?";}
	    	if ($p_version==null || $p_version=="") {$p_version="?";}

				return array(
	    	    'device'   => $bname." (".$version.")",
	    	    'platform'  => $platform." (".$p_version.")",
	    	);
	    }else{
	    	return array();
	    }
	}

	public function roleUpdationMail($email,$role){
		$role = (string)$role;
		$companyId = $_COOKIE['adminInstituteId'];
		if($role == '2'){
			$adminUserData = $this->AdminUser->find('first', array('conditions'=> array('AdminUser.company_id'=> $companyId, 'AdminUser.username' => $email)));
			// $role_name = $adminUserData['AdminUser']['role_id'] == '0'?'Admin':$adminUserData['AdminUser']['role_id'] == '3'?'Switchboard':'Subadmin';
			if($adminUserData['AdminUser']['role_id'] == '1'){
				$role_name = 'Subadmin';
			}else if($adminUserData['AdminUser']['role_id'] == '3'){
				$role_name = 'Switchboard';
			}else{
				$role_name = 'Admin';
			}
			$status = 0;
		}else{
			if($role == '1'){
				$role_name = 'Subadmin';
			}else if($role == '3'){
				$role_name = 'Switchboard';
			}else{
				$role_name = 'Admin';
			}
			// $role_name = $role == '0'?'Admin':$role == '3'?'Switchboard':'Subadmin';
			$status = 1;
		}
		try {
			$userData_cust = $this->User->find('first',array('conditions'=>array('email'=>$email)));
			$company_data = $this->CompanyName->find('first',array('conditions'=>array('CompanyName.id'=>$companyId)));

			$params['trust_name'] = "'".$company_data['CompanyName']['company_name']."'";
			$params['status'] = $status;
			$params['f_name'] = $userData_cust['UserProfile']['first_name'];
			$params['role'] = $role_name;
			$params['toMail'] = $email;
			// $params['toMail'] = 'deepakkumar@mediccreations.com';


			$mailSend = $this->InsEmail->assignRole( $params );
		} catch (Exception $e) {}
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 15-07-2019
	I/P: --
	O/P: User View (render)
	Desc:
	------------------------------------------------------------------------------------------------
	*/

	public function userInfo($userId) {
		$this->checkAdminRole();
		if(isset($_COOKIE['adminInstituteId']) && isset($userId) && $userId != ''){
			$userData = $this->User->find('first',array('conditions'=>array('User.id'=>$userId)));
			$oncall_access = $this->OncallAccessProfession->find('count',array('conditions'=>array(
				'OncallAccessProfession.profession_id'=>$userData['UserProfile']['profession_id'],
				'OncallAccessProfession.status'=>1
			)));
			$countryId = $_COOKIE['adminCountryId'];
			$country_data = $this->Countries->findById($countryId);
			$country_code = $country_data['Countries']['country_code'];

			$onCall_access_key = ($oncall_access > 0) ? 1 : 0;
			try{
				// Admin activity logs [START]
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['user_id'] = $userId;
				$activityData['action'] = 'Profile View';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
			} catch (Exception $e) {

			}
			$this->set(array('user_id'=>$userId, 'userName'=>$userData['UserProfile']['first_name']."'s",'user_email'=>$userData['User']['email'],'oncall_access'=>$onCall_access_key,'country_code'=>$country_code));
		}
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 16-07-2019
	I/P: User id of user
	O/P: User Personal Information (render)
	Desc:	Gives the imformation like: name, email, phone, profession
	------------------------------------------------------------------------------------------------
	*/

	public function getUserPersonalInfo(){
		if(isset($this->data['user_id']) && isset($_COOKIE['adminInstituteId'])){
			$user_id = $this->data['user_id'];
			$companyId = $_COOKIE['adminInstituteId'];
			$fields = array('
					User.id,
					User.email,
					UserProfile.first_name,
					UserProfile.last_name,
					UserProfile.profile_img,
					UserProfile.gmcnumber,
					UserProfile.contact_no,
					UserProfile.custom_role,
					UserDutyLog.status,
					UserDutyLog.atwork_status,
					UserDndStatus.is_active,
					UserDndStatus.dnd_status_id,
					DndStatusOption.dnd_name,
					Profession.profession_type,
					EnterpriseUserList.status
			');
			// UserRoleTag.user_id,
			// UserRoleTag.role_tag_id,
			// RoleTag.key,
			// RoleTag.value
			$conditions = array(
				'User.id' => $user_id,
				'User.status' => 1,
				'User.approved' => 1,
				'UserEmployment.company_id' => $companyId,
				'UserEmployment.is_current' => 1,
				'UserEmployment.status' => 1
			);

			$joins = array(
				array(
						'table' => 'professions',
						'alias' => 'Profession',
						'type' => 'left',
						'conditions'=> array('UserProfile.profession_id = Profession.id')
				),
				array(
		        'table' => 'user_duty_logs',
		        'alias' => 'UserDutyLog',
		        'type' => 'left',
		        'conditions'=> array('UserDutyLog.user_id = User.id')
		    ),
				array(
					'table' => 'user_dnd_statuses',
					'alias' => 'UserDndStatus',
					'type' => 'left',
					'conditions'=> array('UserDndStatus.user_id = User.id')
				),
				array(
					'table' => 'dnd_status_options',
					'alias' => 'DndStatusOption',
					'type' => 'left',
					'conditions'=> array('DndStatusOption.id = UserDndStatus.dnd_status_id')
				),
				array(
					'table' => 'user_employments',
					'alias' => 'UserEmployment',
					'type' => 'left',
					'conditions'=> array('UserEmployment.user_id = User.id')
				),
				array(
					'table' => 'enterprise_user_lists',
					'alias' => 'EnterpriseUserList',
					'type' => 'left',
					'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
				)
				// array(
				// 	'table' => 'user_role_tags',
				// 	'alias' => 'UserRoleTag',
				// 	'type' => 'left',
				// 	'conditions'=> array('User.id = UserRoleTag.user_id')
				// ),
				// array(
				// 	'table' => 'role_tags',
				// 	'alias' => 'RoleTag',
				// 	'type' => 'left',
				// 	'conditions'=> array('UserRoleTag.role_tag_id = RoleTag.id','RoleTag.key' => 'Base Location')
				// )
			);

			$options = array(
				'conditions' => $conditions,
				'joins' => $joins,
				'fields' => $fields
			);

			$userData = $this->User->find('first',$options);
			if(!empty($userData)){
				$duty_status = '';
				if($userData['UserDutyLog']['status'] == 1){
					$duty_status = 'onCallMain';
				}else if($userData['UserDutyLog']['atwork_status'] == 0){
					$duty_status = 'notAtWork';
				}
				$user_id = $userData['User']['id'];
				$user_email = $userData['User']['email'];
				$user_first_name = $userData['UserProfile']['first_name'];
				$user_last_name = $userData['UserProfile']['last_name'];
				$user_img = isset($userData['UserProfile']['profile_img'])?AMAZON_PATH.$userData['User']['id'].'/profile/'.$userData['UserProfile']['profile_img']:'';
				$gmc_number = isset($userData['UserProfile']['gmcnumber']) ? $userData['UserProfile']['gmcnumber'] : '';
				$profession = $userData['Profession']['profession_type'];
				$phone = $userData['UserProfile']['contact_no'];
				$dnd_active = $userData['UserDndStatus']['is_active'];
				$status = $userData['EnterpriseUserList']['status'];
				$dnd_name = $userData['DndStatusOption']['dnd_name'];
				// $baselocation = isset($userData['RoleTag']['value']) ? $userData['RoleTag']['value'] : 'Not Selected';
				$roleTagData = json_decode($userData['UserProfile']['custom_role'],true);
				$baselocation = 'Not Selected';
				if(isset($roleTagData) && !empty($roleTagData)){
					foreach ($roleTagData as $value) {
						if($value['key'] == 'Base Location' && count($value['value']) > 0){
							$baselocation = $value['value'][0];
						}
					}
				}

				$this->set(
					array(
						"user_id" => $user_id,
						"user_email" => $user_email,
						"user_first_name" => $user_first_name,
						"user_last_name" => $user_last_name,
						"user_img" => $user_img,
						"gmc_number" => $gmc_number,
						"profession" => $profession,
						"phone" => $phone,
						"user_active" => true,
						"duty_status" => $duty_status,
						"dnd_active" => $dnd_active,
						"status" => $status,
						"baselocation" => $baselocation,
						"dnd_name" => $dnd_name
					)
				);
			}else{
				$this->set(array("user_active" => false));
			}
			$this->render('/Elements/user/user_personal_info');
		}
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 17-07-2019
	I/P: user id of user
	O/P: Provides the subscription of the user in the particular institution
	Desc:
	------------------------------------------------------------------------------------------------
	*/

	public function usersubscriptionexpiryDate(){
		$this->autoRender = false;
    $responseData = array();
    $customData = array();
    $is_import = "true";
    if($this->request->is('post')) {
      $user_id = $this->request->data['user_id'];
			$userData = $this->User->find('first',array(
				'conditions' => array('User.id'=>$user_id,'User.status'=>1,'User.approved'=>1),
				'joins' => array(
					array(
						'table' => 'user_subscription_logs',
						'alias' => 'UserSubscriptionLog',
						'type' => 'left',
						'conditions'=> array('User.id = UserSubscriptionLog.user_id')
					),
					array(
						'table' => 'company_names',
						'alias' => 'CompanyName',
						'type' => 'left',
						'conditions'=> array('CompanyName.id = UserSubscriptionLog.company_id')
					)
				),
				'fields' => array('UserSubscriptionLog.subscription_expiry_date,UserSubscriptionLog.subscribe_type,CompanyName.subscription_expiry_date')
			));

			if(!empty($userData)){
				$responseData['expiry_date'] = date('d-m-Y', strtotime($userData['UserSubscriptionLog']['subscription_expiry_date']));
				$responseData['company_expiry_date'] = date('d-m-Y', strtotime($userData['CompanyName']['subscription_expiry_date']));
				if(strtotime(date('d-m-Y')) > strtotime($responseData['expiry_date'])){
					$responseData['subscribe_type'] = 3;
				}else{
					$responseData['subscribe_type'] = $userData['UserSubscriptionLog']['subscribe_type'];
				}
			}
			echo json_encode($responseData);
	  }
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 19-07-2019
	I/P: User id of user and email
	O/P: --
	Desc: Updates the email on the OCR Servers
	------------------------------------------------------------------------------------------------
	*/

	public function userDataUpdate(){
		$this->autoRender = false;
		if(isset($_COOKIE['adminInstituteId'])){
			$userId = $this->request->data['user_id'];
			$email = $this->request->data['user_email'];
			$companyId = $_COOKIE['adminInstituteId'];
			$data = json_decode($this->request->data['return_data'],true);
			$update_array = array();
			$return_data = array();
			$return_data['status'] = 0;
			$return_data['message'] = '';
			$emailExist = 0;
			if($data['type'] == 'password'){
				$updateArray = array("User.password"=> "'". md5($data['value']) ."'" );
				$changePass = $this->User->updateAll( $updateArray, array("User.id"=> $userId) );

				$return_data['status'] = 1;
				$return_data['message'] = SUCCESS_200;
			}else{
				if($data['type'] == 'email'){
					$emailExist = $this->User->find('count',array('conditions'=>array('User.email'=>strtolower($data['value']),'User.status'=>1)));
				}

				$user_data = $this->UserQbDetail->find('first',array('conditions'=>array('UserQbDetail.user_id'=>$userId)));
				if(!empty($user_data)){
					if($emailExist > 0){
						$return_data['message'] = 'Email has already been taken.';
					}else{
						$params = array('user_id'=>$userId,'email'=>$email,'qb_id'=>$user_data['UserQbDetail']['qb_id']);
						$QBupdate = $this->updateOnQB($params, $data);
						if(!empty($QBupdate)){
							if(isset($QBupdate['errors'])){
								foreach ($QBupdate['errors'] as $key => $value) {
									$return_data['message'] = ucfirst($key).' '.$value[0];
								}
							}else{
								if($data['type'] == 'name'){
									$update_array = array(
										'first_name' => '"'.$data['first_name'].'"',
										'last_name' => '"'.$data['last_name'].'"'
									);
									$this->UserProfile->updateAll($update_array,array('UserProfile.user_id'=>$userId));
								}else if($data['type'] == 'email'){
									$old_data = $this->User->find('first',array('conditions'=>array('User.id'=>$userId,'User.status'=>1)));
									$update_array = array(
										'email' => '"'.$data['value'].'"'
									);
									$update_admin_array = array(
										'username' => '"'.$data['value'].'"',
										'email' => '"'.$data['value'].'"'
									);
									$this->User->updateAll($update_array,array('User.id'=>$userId));
									$this->EnterpriseUserList->updateAll($update_array,array('EnterpriseUserList.email'=>$old_data['User']['email']));
									$this->AdminUser->updateAll($update_admin_array,array('AdminUser.email'=>$old_data['User']['email']));

								}else if($data['type'] == 'phone'){
									$update_array = array(
										'contact_no' => '"'.$data['value'].'"'
									);
									$this->UserProfile->updateAll($update_array,array('UserProfile.user_id'=>$userId));
								}
								$return_data['status'] = 1;
								$return_data['message'] = SUCCESS_200;

								$updateCachevalue = $this->updateLastModifiedUserCache($userId, 1, $companyId);
							}
						}else{
							$return_data['message'] = ERROR_615;
						}
					}
				}else{
					$return_data['message'] = 'User not Exist';
				}
			}
			// Admin activity logs [START]
      $activityData = array();
      $activityData['company_id'] = $_COOKIE['adminInstituteId'];
      $activityData['admin_id'] = $_COOKIE['adminUserId'];
      $activityData['action'] = 'User Profile Update';
      $activityData['custom_data'] = json_encode($this->request->data);
      $this->TrustAdminActivityLog->addActivityLog($activityData);
      // Admin activity logs [END]
			return json_encode($return_data);
		}
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 19-07-2019
	I/P: User id of user and email
	O/P: --
	Desc: Updates the email on the QB servers
	------------------------------------------------------------------------------------------------
	*/

	public function updateOnQB($params,$data){
		$userId = $params['user_id'];
		$userEMail = $params['email'];
		$oneTimeToken = $this->InsCommon->genrateRandomToken($userId);
		$oneTimeTokenData = array("user_id"=> $userId, "token"=> $oneTimeToken);
		$this->UserOneTimeToken->saveAll($oneTimeTokenData);
		$tokenDetails = $this->Quickblox->quickLogin($userEMail, $oneTimeToken);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$Qdata = $this->Quickblox->updateParamOnQuickBlox($token, $params['qb_id'], $data);
		return json_decode($Qdata,true);
		exit();
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 22-07-2019
	I/P: User id of user
	O/P: --
	Desc: Provides the list of User's Permanent Role(s)
	------------------------------------------------------------------------------------------------
	*/

	public function getPermanentRoles(){
		$this->autoRender = false;
		if(isset($_COOKIE['adminInstituteId'])){
			$companyId = $_COOKIE['adminInstituteId'];
			$userId = $this->request->data['user_id'];

			$permanent_data = $this->userPermanentRole->find('all',array(
				'conditions'=>array(
					'userPermanentRole.user_id'=>$userId,
					'userPermanentRole.institute_id'=>$companyId,
					'userPermanentRole.active'=>1
				),
				'fields'=>array('
					userPermanentRole.role,
					userPermanentRole.id
				')
			));
			$perm_list = array();
			if(!empty($permanent_data)){
				foreach ($permanent_data as $value) {
					$perm_list[] = array(
						'name' => $value['userPermanentRole']['role'],
						'role_id' => $value['userPermanentRole']['id']
					);
				}
			}

			$this->set(array('perm_list'=>$perm_list));
			$this->render('/Elements/user/user_perm_list');
			// print_r($perm_list);
			// exit;
		}
	}

}
