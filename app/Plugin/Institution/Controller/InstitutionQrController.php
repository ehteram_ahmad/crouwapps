<?php

class InstitutionQrController extends InstitutionAppController {
	public $components = array('RequestHandler','Paginator','Session', 'Institution.InsCommon', 'Institution.InsEmail', 'Image', 'Quickblox', 'Cache');

	public $uses = array('EmailTemplate','Institution.User','Institution.EnterpriseUserList','Institution.UserEmployment','Institution.AdminUser','Institution.CompanyName','Institution.UserSubscriptionLog','Institution.NotificationBroadcastMessage','Institution.UserQbDetail','Institution.UserDutyLog','Institution.UserOneTimeToken','Institution.CacheLastModifiedUser','Institution.ExportChatTransaction','Institution.UserProfile','Institution.QrCodeRequest','Institution.TrustAdminActivityLog');
	public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

	public function requestQrCode() {
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$companyId = $_COOKIE['adminInstituteId'];
			$userEmail = $_COOKIE['adminEmail'];
			$numberOfQrCode = $this->request->data['quantity'];
			$validity = $this->request->data['validity'];
			if(!empty($companyId) ){
			    $user = $this->User->find('first',array('conditions'=>array('email'=>$userEmail)));
			    if(!empty($user)){
			        $this->QrCodeRequest->save(array('institute_id'=> $companyId,'user_id'=>$user['User']['id'] , "number_of_qr_codes"=> $numberOfQrCode, 'validity'=> $validity, 'status' => "0"));

			        try{
			            $client_params['to_email'] = $userEmail;
			            $client_params['fname'] = $user['UserProfile']['first_name'];
			            $client_params['quantity'] = $numberOfQrCode;
			            $client_params['delivery_date'] = date('d M Y', strtotime("+5 days"));;

			            $mailSend = $this->InsEmail->sendEmailOnQrCodeRequest( $client_params );

				// -------------MAIL TO ADMIN -------------------
			            $company_data = $this->CompanyName->find('first',array('conditions'=>array('CompanyName.id'=>$companyId)));
			            $admin_params['to_email'] = 'deepakkumar@mediccreations.com';
			            $admin_params['fname'] = 'Admin';
			            $admin_params['quantity'] = $numberOfQrCode;
			            $admin_params['date'] = date('d M Y');
			            $admin_params['trust'] = "'".$company_data['CompanyName']['company_name']."'";

			            $mailSend = $this->InsEmail->sendEmailOnQrCodeRequestToAdmin( $admin_params );
			        }catch(Exception $e){

			        }

			    	$responseData['status'] = 1;
    	            $responseData['message'] = 'Request Sent Successfully';
    				echo json_encode($responseData);

                    try{
	                    // Admin activity logs [START]
	    	            $activityData = array();
	    	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
	    	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
	    	            $activityData['action'] = 'QR Code Requested';
	    	            $activityData['custom_data'] = '{"quantity" :"'.$numberOfQrCode.'","validity" :"'.$validity.'"}';
	    	            $this->TrustAdminActivityLog->addActivityLog($activityData);
	    	            // Admin activity logs [END]
	    	        } catch (Exception $e) {

	    	        }

			    }
			}else{
		    	$responseData['status'] = 0;
	            $responseData['message'] = 'Empty Company Id';
				echo json_encode($responseData);
			}
		}
	}

}
