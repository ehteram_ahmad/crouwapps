<?php


App::uses('Model', 'Model');

/**
 * Represents model EnterpriseUserList
 */
class EnterpriseUserList extends Model {

	/*
	--------------------------------------------------------------------------------
	ON: 29-05-2017
	I/P:
	O/P:
	Desc: 
	--------------------------------------------------------------------------------
	*/
	public function getUserSubscriptionDetails($dataParams=array()){
		$returnData = array();
		//if(!empty($dataParams['email'])){
		$companyId = $dataParams['company_id'];
			$conditions = array("EnterpriseUserList.email"=> $dataParams['email'],"EnterpriseUserList.company_id"=> $companyId, "EnterpriseUserList.status"=> 1,"CompanyName.is_subscribed"=>1, "CompanyName.subscription_expiry_date >= "=> date('Y-m-d 23:59:59'));
			$returnData = $this->find('first', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'company_names',
										'alias' => 'CompanyName',
										'type' => 'left',
										'conditions'=> array('CompanyName.id = EnterpriseUserList.company_id')
									)
								),
								'fields'=> array("CompanyName.id","CompanyName.subscription_date","CompanyName.subscription_expiry_date"),
								'conditions' => $conditions,
							)
						);
			// ** If company subscribed and not check to user email for enterprise list
			/*if(empty($returnData)){
				$querData = $this->query("SELECT CompanyName.id, CompanyName.subscription_date, CompanyName.subscription_expiry_date FROM company_names CompanyName WHERE CompanyName.id = $companyId AND CompanyName.is_auto_paid_user = 1");
				if(!empty($querData)){
					$returnData['CompanyName']['id'] = $querData[0]['CompanyName']['id'];
					$returnData['CompanyName']['subscription_date'] = $querData[0]['CompanyName']['subscription_date'];
					$returnData['CompanyName']['subscription_expiry_date'] = $querData[0]['CompanyName']['subscription_expiry_date'];
				}
			}*/
		//}
		return $returnData;
	}

	/*
	--------------------------------------------------------------------------------
	ON: 15-12-2017
	I/P:
	O/P:
	Desc: 
	--------------------------------------------------------------------------------
	*/

	public function getEnterpriseUserStatus($dataParams=array())
	{
		$email = $dataParams['email'];
		$companyId = $dataParams['company_id'];
		if(!empty($email)){
			$userCurrentstatus = $this->find("first", array("conditions"=> array("email"=> $email, "company_id"=> $companyId)));
    	}
    	return $userCurrentstatus;
	}
	
}