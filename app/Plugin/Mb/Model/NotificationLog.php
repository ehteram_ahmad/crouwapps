<?php 
/*
 Represents model for Notification Logs.
*/
App::uses('AppModel', 'Model');

class NotificationLog extends AppModel {
	public $name = 'NotificationLog';

	/*
	----------------------------------------------------------------------------------------
	On: 09-11-2015
	I/P: 
	O/P: 
	Desc: List of user notifications
	----------------------------------------------------------------------------------------
	*/
	public function notificationListByUser( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1, $params = array(), $sortBy = 'latest' ){
		$notificationList = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		if( !empty($params) ){
			$conditions = '';
			if( !empty($params['user_id']) ){
				$conditions = array('NotificationLog.to_user_id'=> $params['user_id'], 'NotificationLog.by_user_id <> '=> $params['user_id']);
			}
			//** If notification id set fetch all notifications > $params['notification_id']
			if( !empty($params['user_id']) && !empty($params['notification_id']) ){
				if( isset($sortBy) && $sortBy == 'previous'){
					$conditions = array('NotificationLog.id < '=> $params['notification_id'], 'NotificationLog.to_user_id'=> $params['user_id'], 'NotificationLog.by_user_id <> '=> $params['user_id']);
				}else{
					$conditions = array('NotificationLog.id > '=> $params['notification_id'], 'NotificationLog.to_user_id'=> $params['user_id'], 'NotificationLog.by_user_id <> '=> $params['user_id']);
				}
			}
			$options =array(             
				'joins' =>
					array(
						array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'inner',
							'conditions'=> array('User.id = NotificationLog.by_user_id')
						),
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfile',
							'type' => 'left',
							'foreignKey' => 'user_id',
							'conditions'=> array('User.id = UserProfile.user_id')
						),
					),
				'fields'=> array('NotificationLog.id, NotificationLog.by_user_id, NotificationLog.to_user_id, NotificationLog.notify_type, NotificationLog.item_id, NotificationLog.read_status, NotificationLog.created,  User.id, User.status, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
				'conditions'=> $conditions,
				'order'=> array('NotificationLog.created DESC'),
				'limit'=> $pageSize,
				'offset'=> $offsetVal  
			);
			$notificationList = $this->find('all', $options);
			return $notificationList;
		}
	}

	/*
	----------------------------------------------------------------------------------------
	On: 29-03-2016
	I/P: 
	O/P: 
	Desc: Unread notification count
	----------------------------------------------------------------------------------------
	*/
	public function unreadNotificationCount( $userId = NULL ){
		$unreadNotifications = 0;
		if(!empty($userId)){
			$conditions = array("to_user_id"=> $userId, "send_status"=> (int) 1, "read_status"=> (int) 0 , "by_user_id <> "=> $userId);
			$unreadNotifications = $this->find("count", array("conditions"=> $conditions));
		}
		return $unreadNotifications;
	}

}
?>