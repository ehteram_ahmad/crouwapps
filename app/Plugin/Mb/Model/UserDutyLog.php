<?php


App::uses('Model', 'Model');

/**
 * Represents model User Duty Log
 */
class UserDutyLog extends Model {

	public function userDutyStatus($params = array()){
		$dutyStatusDetails = array();
		if(!empty($params)){
			$dutyStatusDetails = $this->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "hospital_id"=>$params['company_id']), 'order'=> array('created DESC')));
		}
		return $dutyStatusDetails;
	}

	public function getStatusInfo($userId){
		$userLists = array();
			$quer = "SELECT 
			 `UserDutyLog`.`id`,`UserDutyLog`.`hospital_id`,`UserDutyLog`.`status`,`UserDutyLog`.`modified`,`UserDutyLog`.`created`,`UserDutyLog`.`atwork_status`   
			  FROM user_duty_logs UserDutyLog 
			  WHERE  `UserDutyLog`.`user_id`= $userId ORDER BY id DESC";
			  $userLists = $this->query($quer);
		return $userLists;
	}
	
}
