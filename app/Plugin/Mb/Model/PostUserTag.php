<?php 
/*
 Represents model for post user tags.
*/
App::uses('AppModel', 'Model');

class PostUserTag extends AppModel {
	public $name = 'PostUserTag';

	/*
	---------------------------------------------------------------------------------------------
	On: 
	I/P:
	O/P:
	Desc: Getting information of tagged users with post(beat)
	---------------------------------------------------------------------------------------------
	*/
	public function postTaggedUsers( $pageSize = DEFAULT_PAGE_SIZE, $pageNumber = 1, $postId = NULL ){
		$taggedUsers = array();
		if( !empty($postId) ){
			$offsetVal = ( $pageNumber - 1 ) * $pageSize;
			$conditions = array('PostUserTag.post_id'=> $postId, 'User.status'=> 1, 'User.approved'=> 1);
			$options =array(             
				'joins' =>
					array(
						array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'inner',
							'conditions'=> array('User.id = PostUserTag.user_id')
							),
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfile',
							'type' => 'left',
							'foreignKey' => 'user_id',
							'conditions'=> array('PostUserTag.user_id = UserProfile.user_id')
						),
					),
				'fields'=> array('User.id, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
				'conditions'=> $conditions,
				'limit'=> $pageSize,
				'offset'=> $offsetVal
				);
			$taggedUsers = $this->find('all', $options);
		}
		return $taggedUsers;
	}

	/*
	---------------------------------------------------------------------------------------------
	On: 
	I/P:
	O/P:
	Desc: Getting information of tagged users with post(beat)
	---------------------------------------------------------------------------------------------
	*/
	public function postTaggedUsersCount( $postId = NULL ){
		$taggedUsersCount = 0;
		if( !empty($postId) ){
			$conditions = array('PostUserTag.post_id'=> $postId, 'User.status'=> 1, 'User.approved'=> 1);
			$options =array(             
				'joins' =>
					array(
						array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'inner',
							'conditions'=> array('User.id = PostUserTag.user_id')
							),
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfile',
							'type' => 'left',
							'foreignKey' => 'user_id',
							'conditions'=> array('PostUserTag.user_id = UserProfile.user_id')
						),
					),
				//'fields'=> array('User.id, User.status, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
				'conditions'=> $conditions
				);
			$taggedUsersCount = $this->find('count', $options);
		}
		return $taggedUsersCount;
	}
	
}
?>