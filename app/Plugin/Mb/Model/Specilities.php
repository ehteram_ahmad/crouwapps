<?php 
App::uses('AppModel', 'Model');

class Specilities extends AppModel {
	public $name = 'Specilities';
	/*
	--------------------------------------------------------------------
	On: 28-07-2015
	I/P: $params = array()
	O/P: array()
	Desc: Fetches all active specilities.
	--------------------------------------------------------------------
	*/	
	public function allSpecilities($params = array(), $allRows = '' ){
		$data = array();
		if( $allRows == 'all' ){
			$data = $this->find('all', array('conditions'=> array('status'=> 1), 'order'=> array('name')));
		}else{
			$offsetVal = ( $params['page_number'] - 1 ) * $params['size'];
			$data = $this->find('all', array(
								'conditions'=> array('status'=> 1), 
								'order'=> array('name'),
								'limit'=> $params['size'],
								'offset'=> $offsetVal));
		}
		return $data;
	}

	
}
?>