<?php 
	//echo !empty($about['Content']['content_text']) ? $about['Content']['content_text'] : '';
?>
<section class="inner-content about">
	<div class="container">
<div class="col-sm-12">
<?php if(isset($pageTitle) && $pageTitle != ''){?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>

<?php /* ?><p><?php echo isset($emailVerifyMsg) ? $emailVerifyMsg : '' ; ?></p><?php */ ?>
<?php /* ?><p>Thank you for verifying your account! Please be patience while our administrator reviews the details that you have provided for our records. You will get a confirmation email to your registered email address once your account has been approved.</p>
</div>
</div>
</section><?php */ ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin-top: 0px !important; padding-top: 0px !important">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
html, body {
	margin-top: 0px !important;
	padding-top: 0px !important;
}
body {
	background-color: #FFFFFF;
	margin-top: 0px !important;
	padding-top: 0px !important;
	font-family: sans-serif;
}
table {
	margin-top: 0px !important;
	padding-top: 0px !important;
}
</style>
<style type="text/css">
a img {
	color: #000001 !important;
}
.wysiwyg-text-align-right {
	text-align: right;
}
.wysiwyg-text-align-center {
	text-align: center;
}
.wysiwyg-text-align-left {
	text-align: left;
}
.wysiwyg-text-align-justify {
	text-align: justify;
}
body {
	text-shadow: none;
	padding-top: 0;
	padding-right: 0;
	padding-bottom: 0;
	padding-left: 0;
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 0;
	margin-left: 0;
	color: #000000!important;
	font-style: normal;
	font-family: Arial;
	font-size: 14px;
	line-height: 24px;
}
h1, #email-299411 h1 {
	text-shadow: none;
	padding-top: 0;
	padding-right: 0;
	padding-bottom: 0;
	padding-left: 0;
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 0;
	margin-left: 0;
	color: #000000!important;
	font-weight: 400;
	font-style: normal;
	font-family: Arial;
	font-size: 36px;
	line-height: 44px;
}
h2, #email-299411 h2 {
	text-shadow: none;
	padding-top: 0;
	padding-right: 0;
	padding-bottom: 0;
	padding-left: 0;
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 0;
	margin-left: 0;
	color: #000000!important;
	font-weight: 400;
	font-style: normal;
	font-family: Arial;
	font-size: 24px;
	line-height: 32px;
}
h3, #email-299411 h3 {
	text-shadow: none;
	padding-top: 0;
	padding-right: 0;
	padding-bottom: 0;
	padding-left: 0;
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 0;
	margin-left: 0;
	color: #000000!important;
	font-weight: 400;
	font-style: normal;
	font-family: Arial;
	font-size: 15px;
	line-height: 21px;
}
p, #email-299411 p {
	text-shadow: none;
	padding-top: 0;
	padding-right: 0;
	padding-bottom: 0;
	padding-left: 0;
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 0;
	margin-left: 0;
	color: #000000!important;
	font-style: normal;
	font-family: Arial;
	font-size: 14px;
	line-height: 24px;
}
a, #email-299411 a {
	text-shadow: none;
	padding-top: 0;
	padding-right: 0;
	padding-bottom: 0;
	padding-left: 0;
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 0;
	margin-left: 0;
	color: #1122CC!important;
	text-decoration: underline;
}
.h1_color_span_wrapper {
	color: #000000;
}
.h2_color_span_wrapper {
	color: #000000;
}
.h3_color_span_wrapper {
	color: #000000;
}
.p_color_span_wrapper {
	color: #000000;
}
.a_color_span_wrapper {
	color: #1122CC;
}
.mi-all {
	display: block;
}
.mi-desktop {
	display: block;
}
.mi-mobile {
	display: none;
	font-size: 0;
	max-height: 0;
	line-height: 0;
	padding: 0;
	float: left;
	overflow: hidden;
	mso-hide: all; /* hide elements in Outlook 2007-2013 */
}
</style>
<style type="text/css">

	div, p, a, li, td { -webkit-text-size-adjust:none; }
	
	@media only screen and (max-device-width: 480px), screen and (max-width: 480px), screen and (orientation: landscape) and (max-width: 630px) {
		
		/* very important! all except 'all' and this current type get a display:none; */
		.mi-desktop{ display: none !important; }

		/* then show the mobile one */
		.mi-mobile{ 
			display: block !important;
			font-size: 12px !important;
			max-height: none !important;
			line-height: 1.5 !important;
			float: none !important;
			overflow: visible !important;
		}
	}

</style>
</head>
<body id="email-299411" style="background: #FFFFFF; color: #000000 !important; font-family: Arial; font-size: 14px; font-style: normal; line-height: 24px; margin: 0px 0 0 0px; padding: 0px 0 0; text-shadow: none" bgcolor="#FFFFFF">
<style type="text/css">
body {
margin-top: 0px !important; padding-top: 0px !important;
}
body {
background-color: #FFFFFF; margin-top: 0px !important; padding-top: 0px !important; font-family: sans-serif;
}
body {
text-shadow: none; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; color: #000000 !important; font-style: normal; font-family: Arial; font-size: 14px; line-height: 24px;
}
</style>
<img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/activate-o.gif" width="1" height="1">

<table align="center" cellpadding="0" cellspacing="0" width="100%" style="background: #FFFFFF; border-collapse: collapse; border-spacing: 0; border: 0; margin: 0px 0 0; padding: 0px 0 0" bgcolor="#FFFFFF">
  <tbody>
    <tr align="center" style="border-collapse: collapse; border-spacing: 0; border: 0">
      <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; border: 0"><div class="mi-all" style="display: block">
          <table width="750" class="mi-all" align="center" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; border: 0; display: block; margin: 0px 0 0; min-width: 750px; padding: 0px 0 0">
            <tbody>
              <tr align="left" style="border-collapse: collapse; border-spacing: 0; border: 0">
                <td><table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; border: 0; margin-top: 0px !important; min-width: 750px; padding-top: 0px !important">
                    <tbody>
                      <tr style="border-collapse: collapse; border-spacing: 0; border: 0">
                        <td align="left" valign="top"><table align="center" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; border: 0; margin: 0px 0 0; min-width: 750px; padding: 0px 0 0">
                            <tbody>
                              <tr align="left" style="border-collapse: collapse; border-spacing: 0; border: 0">
                                <td><table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; border: 0; margin-top: 0px !important; min-width: 750px; padding-top: 0px !important">
                                    <tbody>
                                      <tr style="border-collapse: collapse; border-spacing: 0; border: 0">
                                        <td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly"><a href="https://theoncallroom.com/"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/activate-1.jpg" style="border: 0; display: block; line-height: 0px"></a></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                              </tr>
                              <tr align="left" style="border-collapse: collapse; border-spacing: 0; border: 0">
                                <td><table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; border: 0; margin-top: 0px !important; min-width: 750px; padding-top: 0px !important">
                                    <tbody>
                                      <tr style="border-collapse: collapse; border-spacing: 0; border: 0">
                                        <td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/activate01-2.jpg" style="border: 0; display: block; line-height: 0px"></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                              </tr>
                              <tr align="left" style="border-collapse: collapse; border-spacing: 0; border: 0">
                                <td><table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; border: 0; margin-top: 0px !important; min-width: 750px; padding-top: 0px !important">
                                    <tbody>
                                      <tr style="border-collapse: collapse; border-spacing: 0; border: 0">
                                        <td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly">
                                        	<img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/activate-3.jpg" style="border: 0; display: block; line-height: 0px">
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                              </tr>
                            </tbody>
                          </table></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table>
        </div></td>
    </tr>
  </tbody>
</table>
<div style="display: none; font: 15px courier; white-space: nowrap"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>