<?php
/*
-----------------------------------------------------------------------------------------------
	Desc: Controller used to do some chat history tasks.
-----------------------------------------------------------------------------------------------
*/

App::uses('AppController', 'Controller');

class ChathistoryController extends AppController {
	public $uses = array('Mb.User', 'Mb.UserProfile','Mb.UserQbDetail','Mb.ChatHistoryScheduler','Mb.UserEmployment','Mb.CompanyName','Mb.Profession');
	public $components = array('Mb.MbCommon', 'Mb.MbEmail');

	/*
	-----------------------------------------------------------------------------------------------
	ON:  
	I/P: 
	O/P: 
	Desc: 
	-----------------------------------------------------------------------------------------------
	*/
	public function chatHistory(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** Create PDF
				if(!empty($dataInput['chat_history'])){
					$details['user_id'] = $dataInput['user_id'];
					$details['from_date'] = $dataInput['from_date'];
					$details['to_date'] = $dataInput['to_date'];
					$details['dialog_name'] = $dataInput['dialog_name'];
					$responseData = array('method_name'=> 'chatHistory', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
					//** Add on table[START]
					$chatHistoryData = 
						array(
							"user_id"=>$dataInput['user_id'],
							"chat_history_data"=>json_encode($dataInput['chat_history']),
							"user_details"=>json_encode($details)

						);
					$this->ChatHistoryScheduler->save($chatHistoryData);
					//** Add on table[END]
					
					
				}else{
				$responseData = array('method_name'=> 'chatHistory', 'status'=>"0", 'response_code'=> "646", 'message'=> ERROR_646);
			}
				
			}else{
				$responseData = array('method_name'=> 'chatHistory', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'chatHistory', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	ON:  
	I/P: 
	O/P: 
	Desc: 
	-----------------------------------------------------------------------------------------------
	*/
	public function createPdf($chatData = array(), $details = array(), $patientInfo = array()){
		set_time_limit(180);
		ini_set('memory_limit','256M');

		App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));
		App::import('Vendor','FPDI',array('file' => 'fpdi/fpdi.php'));
		// create a PDF object
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$fpdi = new FPDI();

		$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $details['user_id'])));
		$name = $userDetails['UserProfile']['first_name'];
		$loggedinUserCountryId = $userDetails['UserProfile']['country_id'];
		$conversationName = $details['dialog_name'];
		$fromDate = $details['from_date'];
		$toDate = $details['to_date'];
		$chatCreatedBy = $details['created_by'];
		$occupantsId = $details['occupants_id'];
		$createdDate = "";

		//** Chat Created By Id[START]
		$userByQbId = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $chatCreatedBy)));
			if(!empty($userByQbId)){
				$chatCreatedByUserId = $userByQbId["UserQbDetail"]['user_id'];
			}
		//** Chat Created By Id[END]

		//** Chat Participants details[START]
		$participantIdsData = $this->UserQbDetail->find("list", array("fields"=>array("user_id"),"conditions"=> array("qb_id"=>$occupantsId)));
		$participantIds = array_unique($participantIdsData);
		$chatParticipants = array();
		// foreach($participantIds as $participantId){
		// 	$chatParticipantsDetails = $this->UserProfile->find("first", array("conditions"=>array("user_id"=> $participantId)));
		// 	//echo "<pre>"; print_r($chatParticipantsDetails);die;
		// 	if(!empty($chatParticipantsDetails)){
		// 		$chatParticipants[] = $chatParticipantsDetails['UserProfile']['first_name'];
		// 	}
		// }
		//** Chat Participants details[END]

		//** Created By Institution[START]
		$userCurrentCompanyName = "";
		$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $chatCreatedByUserId, "is_current"=> 1)));
		$companyId = $userCurrentCompany['UserEmployment']['company_id'];
		if($companyId == -1){
			$userCurrentCompanyName = "None";
		}else if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
			$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
			if(!empty($userCompanyDetails)){
				$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
			}
		}
		//** Created By Institution[END]
		$rowCnt = 0;
		$textHtml = '<table cellspacing="0" cellpadding="2" color="#1B3B6A">';
		$textHtml .= '<tr><td colspan=3 width="575px;">Hi '.$name.'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Please find below the chat history you requested.</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Medic Bleep</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Chat History:</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">'.substr(str_replace("\\","",$fromDate),0,16).' to '.substr(str_replace("\\","",$toDate),0,16).'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Chat Subject:</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">'.$conversationName.'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">Trust Name:</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;">'.$userCurrentCompanyName.'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		if(!empty($patientInfo)){
		$textHtml .= '<tr><td colspan=3 width="575px;">Patient Detail:</td></tr>';
		if(isset($patientInfo['isEncrypted']) && $patientInfo['isEncrypted']=='Yes'){
		$textHtml .= '<tr><td colspan=3 width="575px;">
							Name:'. $this->MbCommon->decryptData($patientInfo['Patient_name']) .'<br />
							NHS Number:'.$this->MbCommon->decryptData($patientInfo['Patient_nhs_number']).'<br />
							Age:'.$this->MbCommon->decryptData($patientInfo['Patient_age']).'<br />
							Gender:'.$this->MbCommon->decryptData($patientInfo['Patient_gender']).'<br />
							Notes:'.nl2br($this->MbCommon->decryptData($patientInfo['Patient_condition'])).'
							</td></tr>';
		}else{
			$textHtml .= '<tr><td colspan=3 width="575px;">
							Name:'. $patientInfo['Patient_name'] .'<br />
							NHS Number:'.$patientInfo['Patient_nhs_number'].'<br />
							Age:'.$patientInfo['Patient_age'].'<br />
							Gender:'.$patientInfo['Patient_gender'].'<br />
							Notes:'.nl2br($patientInfo['Patient_condition']).'
							</td></tr>';
		}
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		}
		$textHtml .= '<tr><td colspan=3 width="575px;">Chat Participants:</td></tr>';
		$roleStatus = '';
		$count = 1;
		foreach($participantIds as $participantId){
				// $chatParticipantsDetails = $this->UserProfile->find("first", array("conditions"=>array("user_id"=> $participantId)));
			$chatParticipantsDetails = $this->getParticipentDetail($participantId);
			if(!empty($chatParticipantsDetails)){
				if(!empty($chatParticipantsDetails['UserProfile']['role_status']))
				{
					$roleStatus = ", ".$chatParticipantsDetails['UserProfile']['role_status'];
				}
				else if(!empty($chatParticipantsDetails['Profession']['profession_type']))
				{
					$roleStatus = ", ".$chatParticipantsDetails['Profession']['profession_type'];
				}
				else
				{
					$roleStatus = ", NA";
				}
				$textHtml .= '<tr><td colspan=3 width="575px;">'.$count.". ".$chatParticipantsDetails['UserProfile']['first_name']." ".$chatParticipantsDetails['UserProfile']['last_name'].$roleStatus.'</td></tr>';
			}
			$count++;
		}
		//$textHtml .= '<tr><td colspan=3 width="575px;">'.implode(", ", $chatParticipants).'</td></tr>';
		$textHtml .= '<tr><td colspan=3 width="575px;"></td></tr>';
		$textHtml .= '</table>';

		if(!empty($chatData["items"])){
		$textHtml .= '<table cellspacing="2" cellpadding="5">';
		foreach ($chatData["items"] as $chatD) {

			$created_at = str_replace('/','-',substr(trim($chatD["created_at"]),0,10));
			if($createdDate !=$created_at){
				$createdDate = $created_at;
				$textHtml .= '<tr bgcolor="#10A5DA">';
				$textHtml .= '<td colspan="3" width="575px;"><font color="white"><strong>' . 'Date: '.$created_at . '</strong></font></td>';
				$textHtml .= '</tr>';
			}
			$senderName = "";
			$userDetailsSender = $this->userDetailsByQbId($chatD['sender_id']);
			if(!empty($userDetailsSender)){
				$senderName = $userDetailsSender["UserProfile"]['first_name'];
			}
			if($rowCnt%2==0){
				$rowBgColor = "#C7EAFB";
			}else{
				$rowBgColor = "#EFEFF0";
			}
				$timeStamp = "";
				if($loggedinUserCountryId==226){
					date_default_timezone_set('Europe/Amsterdam');
				}else if($loggedinUserCountryId==99){
					date_default_timezone_set('Asia/Kolkata');
				}
				$message = "";
				if( !empty($chatD["message"])){
					if(!empty($chatD["notification_type"])){
						$message = $this->formatNotification($chatD["notification_type"]);
					}else{
						if($chatD["message"]=='Audio' || $chatD["message"]=='Video' || $chatD["message"]=='Notification message' || $chatD["message"]=='Contact request' || $chatD["message"]=='Location' || $chatD["message"]=='Call Notification' || $chatD["message"]=='Attachment image' || $chatD["message"]=='Attachment' || $chatD["message"]=='aac' || $chatD["message"]=='mp4' || $chatD["message"]=='mp3' || $chatD["message"]=='photo' || $chatD["message"]=='image'){
							/*if($chatD["isEncrypted"]=='Yes'){
								$message = $this->MbCommon->decryptData($chatD["message"]);
							}else{
								$message = $chatD["message"];
							}*/
							$message = $chatD["message"];
						}
						else if($chatD["isEncrypted"]=='Yes'){
							$message = $this->MbCommon->decryptData($chatD["message"]);
						}else{
							$message = $chatD["message"];
						}
					}
					$timeStamp = date("H:i", $chatD['date_sent']);
					if(empty($chatD["attachments"])){
						$textHtml .= '<tr bgcolor="'.$rowBgColor.'">';
						$textHtml .= '<td width="100px;" >' .$senderName . '</td>';
						$textHtml .= '<td width="375px;" >' . $message . '</td>';
						$textHtml .= '<td width="100px;" >' . $timeStamp .'</td>';
						$textHtml .= '</tr>';
					}
					
				}
				if( !empty($chatD["attachments"])){
					$messageUrl = "";
					$attachmentName = "";
					/*if($message=="Video"){ $attachmentName = "Video Attachment"; }
					else if($message=="mp4"){ $attachmentName = "Video Attachment"; }
					else if($message=="aac"){ $attachmentName = "Audio Attachment"; }
					else if($message=="mp3"){ $attachmentName = "Audio Attachment"; }
					else if($message=="Audio"){ $attachmentName = "Audio Attachment"; }
					else if($message=="photo"){ $attachmentName = "Image Attachment"; }
					else if($message=="image"){ $attachmentName = "Image Attachment"; }
					else if($message=="Attachment image"){ $attachmentName = "Image Attachment"; }*/
					foreach($chatD["attachments"] as $attachment){
						//** Check Attachment type[START]
							if($attachment["type"]=="Video"){ $attachmentName = "Video Attachment"; }
							else if($attachment["type"]=="mp4"){ $attachmentName = "Video Attachment"; }
							else if($attachment["type"]=="aac"){ $attachmentName = "Audio Attachment"; }
							else if($attachment["type"]=="mp3"){ $attachmentName = "Audio Attachment"; }
							else if($attachment["type"]=="Audio"){ $attachmentName = "Audio Attachment"; }
							else if($attachment["type"]=="photo"){ $attachmentName = "Image Attachment"; }
							else if($attachment["type"]=="image"){ $attachmentName = "Image Attachment"; }
							else if($attachment["type"]=="Attachment image"){ $attachmentName = "Image Attachment"; }
							else if(in_array($attachment["type"],array("pdf","doc","docx","xls","xlsx","ppt","pptx"))){ $attachmentName = "Document Attachment"; }
						//** Check Attachment type[END]
						//$attachment["url"] = preg_replace('/\s+/', '', $attachment["url"]);
						$messageUrl = trim($attachment["url"]);//strstr(trim($attachment["url"]), '?', true) ? strstr(trim($attachment["url"]), '?', true) : trim($attachment["url"]);
						$timeStamp = date("H:i", $chatD['date_sent']);
						$textHtml .= '<tr bgcolor="'.$rowBgColor.'">';
						$textHtml .= '<td width="100px;">' .$senderName . '</td>'; 
						$textHtml .= '<td width="375px;"><a href="'.$messageUrl.'">' . $attachmentName . '</a></td>';
						$textHtml .= '<td width="100px;">' . $timeStamp .'</td>';
						$textHtml .= '</tr>'; 
					}
				}
			$rowCnt++;
			empty($chatD);
		}
	}
		$textHtml .= '</table>';
		// array to hold list of PDF files to be merged
		$headerTemplate = APP . 'webroot/chatHistory/templates/header-single-v3.pdf';
		$files = array($headerTemplate);
		$pageCount = 0;
		//$pagesCount = 1;
		foreach($files AS $file) {
               $pagecount = $fpdi->setSourceFile($file);
               for ($i = 1; $i <= $pagecount; $i++) {
                    $tplidx = $fpdi->ImportPage($i);
                    $s = $fpdi->getTemplatesize($tplidx);
                    $fpdi->AddPage('P');
                	$fpdi->useTemplate($tplidx,null, 0, 0, 0, FALSE);
                	$fpdi->SetXY(3, 90);
                	$fpdi->SetFont('helvetica', '', 11); 
                	$fpdi->writeHTML($textHtml, true, true, true, 0); 
                	//** Write Footer
                	
					//$fpdi->writeHTML('<p align="center">Copyright 2017 Medic Creations. All Rights Reserved</p>', true, true, true, 0);
               }
          }
          
        $finalFileName = 'Chathistory' . strtotime(date('Y-m-d H:i:s')) . '.pdf';
        //$finalFileName = 'chatHistory.pdf';
        $output = $fpdi->Output(APP . 'webroot/chatHistory/'.$finalFileName, 'F');
        sleep(60);
        chmod(APP . 'webroot/chatHistory/'.$finalFileName, 0777);
        //unlink( $tempFile );
		return array($finalFileName);
	}

	/*
	-----------------------------------------------------------------------------------------------
	ON:  
	I/P: 
	O/P: 
	Desc:
	-----------------------------------------------------------------------------------------------
	*/
	public function userDetailsByQbId($qbId=NULL){
		$userDetails = array();
		if(!empty($qbId)){
			$userByQbId = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $qbId)));
			if(!empty($userByQbId['UserQbDetail']['user_id'])){
				$userDetails = $this->User->userDetailsById($userByQbId['UserQbDetail']['user_id']);
			}
		}
		return $userDetails;
	}

	/*
	-----------------------------------------------------------------------------------------------
	ON:  
	I/P: 
	O/P: 
	Desc:
	-----------------------------------------------------------------------------------------------
	*/
	public function chatHistoryPdfSend(){
		$chatHistoryData = $this->ChatHistoryScheduler->find("first", array("conditions"=> array("mail_sent_status"=> 0), 'order'=> 'id DESC', 'limit'=> 10));
		foreach($chatHistoryData as $chatHistory){
			//echo "<pre>"; print_r($chatHistory);die;
			$chatData = json_decode($chatHistory['chat_history_data'], true);
			$details = json_decode($chatHistory['user_details'], true);
			$tableId = $chatHistory['id'];
			$this->ChatHistoryScheduler->updateAll(array("mail_sent_status"=> 1), array("id"=>$tableId));

			$createPdf = $this->createPdf($chatData,$details);
			if(!empty($createPdf[0])){
				$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $details['user_id'])));
				$email = $userDetails['User']['email'];
				$name = $userDetails['UserProfile']['first_name'];
				$chatHistoryParams['toMail']= $email;
				$chatHistoryParams['name']= $name;
				$chatHistoryParams['chatHistoryFileName'] = $createPdf[0];
				chmod(APP . 'webroot/chatHistory/'.$createPdf[0], 0777);
				$chatHistoryParams['chatHistoryFilePath']= base64_encode(file_get_contents(BASE_URL.'/chatHistory/'.$createPdf[0]));
				$chatHistoryParams['conversationName'] = $details['dialog_name'];
				$chatHistoryParams['fromDate'] = $details['from_date'];
				$chatHistoryParams['toDate'] = $details['to_date'];
				$chatHistoryParams['emailSubject'] = "Chat with ".substr($details['dialog_name'],0,10). " from: ".substr($details['from_date'],0,10)." to: ".substr($details['to_date'],0,10);
				$sendChatHistoryMail = $this->MbEmail->sendChatHistory($chatHistoryParams);
				$this->ChatHistoryScheduler->updateAll(array("mail_response"=> "'".$sendChatHistoryMail."'"), array("id"=>$tableId));
				//** Remove pdf file after sending mail
				unlink( APP . 'webroot/chatHistory/'.$createPdf[0] ); 
			}
		}
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	ON: 22-04-2017 
	I/P: 
	O/P: 
	Desc: Chat History data with file upload
	-----------------------------------------------------------------------------------------------
	*/
	public function chatHistoryFileUpload(){
		if($this->request->is('post')) {
			$this->autoRender = false;
			$responseData = array();
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = json_decode($_REQUEST['data'], true);
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0 ){ 
					//if( isset($_FILES) && !empty($_FILES) ){
					$targetPath = WWW_ROOT . 'chatHistory/uploader_tmp/';
					$fileUpload = $this->MbCommon->uploadFileToserver($targetPath, $_FILES, 'fileName', $dataInput['fileName']);
					//** Add on table[START]
					$details["user_id"] = $dataInput['user_id'];
					$details["from_date"] = $dataInput['from_date'];
					$details["to_date"] = $dataInput['to_date'];
					$details["dialog_id"] = $dataInput['dialog_id'];
					$details["dialog_name"] = $dataInput['dialog_name'];
					$details["fileName"] = $dataInput['fileName'];
					$details["created_by"] = $dataInput['created_by'];
					//$details["occupants_id"] = json_encode($dataInput['occupants_id']);
					$details["occupants_id"] = $dataInput['occupants_id'];
					$chatHistoryData = json_encode(file_get_contents(BASE_URL.'/chatHistory/uploader_tmp/'.$fileUpload[1]), JSON_UNESCAPED_SLASHES);
					
					$chatHistoryData = 
					array(
					"user_id"=>$dataInput['user_id'],
					"chat_history_data"=>$chatHistoryData,
					"user_details"=>json_encode($details),
					"file_name"=> $fileUpload[1]//$dataInput['fileName']
					);
					$this->ChatHistoryScheduler->save($chatHistoryData);
					//** Add on table[END]
					$responseData = array('method_name'=> 'chatHistoryFileUpload', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_609);
					//}
				}
				else{
					$responseData = array('method_name'=> 'chatHistoryFileUpload', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'chatHistoryFileUpload', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'chatHistoryFileUpload', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	ON: 22-04-2017 
	I/P: 
	O/P: 
	Desc: Chat History send with pdf file creation.
	-----------------------------------------------------------------------------------------------
	*/
	public function chatHistoryPdfSendUsingFile(){
		$chatHistoryData = $this->ChatHistoryScheduler->find("first", array("conditions"=> array("mail_sent_status"=> 0), 'order'=> 'id DESC', 'limit'=> 10));
		foreach($chatHistoryData as $chatHistory){ 
			$chatData = json_decode(file_get_contents(BASE_URL.'/chatHistory/uploader_tmp/'.$chatHistory['file_name']), true);//json_decode($chatHistory['chat_history_data'], true);//json_decode($chatHistory['chat_history_data'], true);
			$patientInfo = !empty($chatData['patient_info']) ? $chatData['patient_info'] : array();
			$details = json_decode($chatHistory['user_details'], true);
			$tableId = $chatHistory['id'];
			$this->ChatHistoryScheduler->updateAll(array("mail_sent_status"=> 1), array("id"=>$tableId));
			//echo "<pre>"; print_r($chatData['chat_history']);die;
			$createPdf = $this->createPdf($chatData['chat_history'],$details,$patientInfo);
			if(!empty($createPdf[0])){
				$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $details['user_id'])));
				$email = $userDetails['User']['email'];
				$name = $userDetails['UserProfile']['first_name'];
				$chatHistoryParams['toMail']= $email;
				$chatHistoryParams['name']= $name;
				$chatHistoryParams['chatHistoryFileName'] = $createPdf[0];
				chmod(APP . 'webroot/chatHistory/'.$createPdf[0], 0777);
				$chatHistoryParams['chatHistoryFilePath']= base64_encode(file_get_contents(BASE_URL.'/chatHistory/'.$createPdf[0]));
				$chatHistoryParams['conversationName'] = $details['dialog_name'];
				$chatHistoryParams['fromDate'] = $details['from_date'];
				$chatHistoryParams['toDate'] = $details['to_date'];
				$chatHistoryParams['emailSubject'] = "Chat history from Medic Bleep";//"Chat with ".substr($details['dialog_name'],0,10). " from: ".substr($details['from_date'],0,10)." to: ".substr($details['to_date'],0,10);
				$sendChatHistoryMail = $this->MbEmail->sendChatHistory($chatHistoryParams);
				$this->ChatHistoryScheduler->updateAll(array("mail_response"=> "'".$sendChatHistoryMail."'"), array("id"=>$tableId));
				//** Remove pdf file after sending mail
				unlink( APP . 'webroot/chatHistory/'.$createPdf[0] ); 
			}
		}
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	ON: 24-04-2017 
	I/P: 
	O/P: 
	Desc: Format/decrypt message, Notification etc.
	-----------------------------------------------------------------------------------------------
	*/
	public function formatNotification($notifyType=NULL){
		$returnMsg = "Notification Message";
		if(!empty($notifyType)){
			switch($notifyType){
				case 1: 
						$returnMsg = "Group Created";
						break;
				case 2: 
						$returnMsg = "Group Updated";
						break;
				case 4: 
						$returnMsg = "Contact Request";
						break;
				case 5: 
						$returnMsg = "Contact Request Accepted";
						break;
				case 6: 
						$returnMsg = "Contact Request Rejected";
						break;
				case 7: 
						$returnMsg = "Contact Request Removed";
						break;
				case 101: 
						$returnMsg = "Missed Call";
						break;
				case 102: 
						$returnMsg = "Call Not Responded";
						break;
				case 103: 
						$returnMsg = "Call Rejected";
						break;
				case 104: 
						$returnMsg = "Missed Audio Call";
						break;
				case 105: 
						$returnMsg = "Audio Call Not Responded";
						break;
				case 106: 
						$returnMsg = "Missed Video Call";
						break;
				case 107: 
						$returnMsg = "Video Call Not Responded";
						break;
				case 108: 
						$returnMsg = "Took a screenshot of this chat.";
						break;
				default:
						$returnMsg = "Notification Message";
						break;
			}
		}
		return $returnMsg;
	}

	public function getParticipentDetail($participantId)
	{
		$conditions = "UserProfile.user_id = $participantId";
        $userLists = $this->UserProfile->find('first', 
                            array(
                                'joins'=>
                                array(
                                    array(
                                        'table' => 'professions',
                                        'alias' => 'Profession',
                                        'type' => 'left',
                                        'conditions'=> array('UserProfile.profession_id = Profession.id')
                                    ),
                                ),
                                'conditions' => $conditions,
                                'fields' => array('UserProfile.first_name', 'UserProfile.last_name','UserProfile.role_status', 'Profession.profession_type')
                            )
                        );
        return  $userLists;
	}

}
