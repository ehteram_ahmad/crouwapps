<?php 
/*
Represent model for institute name.
*/
App::uses('AppModel', 'Model');

class InstituteName extends AppModel {
	public $name = 'InstituteName';
	
	/*
	--------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: Method to get on call institute list
	--------------------------------------------------------------------------
	*/

	public function instituteWiseOnCallUser($textKey){
		$onCallInstituteList = array();
		//$onCallInstituteList = $this->find('all');
		//echo "<pre>"; print_r($onCallInstituteList);die;
		//$conditions = "userEmployments.is_current = 1 AND User.id > $textKey";
		$conditions = "InstituteName.id > $textKey";
		$onCallInstituteList = $this->find('all', 
						array(
							'joins'=>
							array(
								array(
									'table' => 'user_duty_logs',
									'alias' => 'userDutyLogs',
									'type' => 'left',
									'conditions'=> array('userDutyLogs.hospital_id = InstituteName.id AND userDutyLogs.status = "1" ')
								),	
							),
							'conditions' => $conditions,
							'fields' => array('InstituteName.institute_name', 'InstituteName.id', 'COUNT(userDutyLogs.status) AS onCallStatus'),
							'group'=> array('InstituteName.institute_name')
						)
					);
	//echo "<pre>"; print_r($onCallInstituteList);die;
		return $onCallInstituteList;

	}

}
?>