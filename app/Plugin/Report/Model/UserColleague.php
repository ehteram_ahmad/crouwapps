<?php 
/*
UserColleague model to represent colleague users.
*/
App::uses('AppModel', 'Model');

class UserColleague extends AppModel {


	public function getCollegueCount($userId)
	{
		$collegueLists = array();
		$conditions = "(UserColleague.user_id = $userId OR UserColleague.colleague_user_id = $userId) AND UserColleague.status = 1";
		$collegueLists = $this->find('all', 
						array(
							'conditions' => $conditions
						)
					);
		$collegueCount = sizeof($collegueLists);
		return $collegueCount;
	}
	
}

?>