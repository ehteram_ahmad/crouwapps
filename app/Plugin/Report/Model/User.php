<?php 
/*
User model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');

class User extends AppModel {

	public function userDetailsByInstitute($textKey){
		$userLists = array();
		//$userLists = $this->find('all');
		//echo "<pre>"; print_r($userLists);die;
		//$conditions = "userEmployments.is_current = 1 AND User.id > $textKey";
		$conditions = "User.id > $textKey";
		$userLists = $this->find('all', 
						array(
							'joins'=>
							array(
								array(
									'table' => 'user_profiles',
									'alias' => 'userProfiles',
									'type' => 'left',
									'conditions'=> array('User.id = userProfiles.user_id')
								),
								array(
									'table'=> 'user_employments',
									'alias' => 'userEmployments',
									'type' => 'left',
									'conditions'=> array('User.id = userEmployments.user_id')
								),
								array(
									'table'=> 'institute_names',
									'alias' => 'instituteNames',
									'type' => 'left',
									'conditions'=> array('userEmployments.company_id = instituteNames.id')
								),
								
							),
							'conditions' => $conditions,
							'fields' => array('User.id', 'User.email', "CONCAT(userProfiles.first_name, ' ' , userProfiles.last_name) AS UserName", 'instituteNames.institute_name', 'instituteNames.status'),
							'group'=> array('User.id'),
							'order'=> 'User.id ASC'
						)
					);
	//echo "<pre>"; print_r($userLists);die;
		return $userLists;

	}


	public function userStatusDetail($textKey){
		$userLists = array();
		//$userLists = $this->find('all');
		//echo "<pre>"; print_r($userLists);die;
		//$conditions = "userEmployments.is_current = 1 AND User.id > $textKey";
		$conditions = "User.id > $textKey";
		$userLists = $this->find('all', 
						array(
							'joins'=>
							array(
								array(
									'table' => 'user_profiles',
									'alias' => 'userProfiles',
									'type' => 'left',
									'conditions'=> array('User.id = userProfiles.user_id')
								),
								array(
									'table'=> 'user_duty_logs',
									'alias' => 'userDutyLogs',
									'type' => 'left',
									'conditions'=> array('User.id = userDutyLogs.user_id')
								),
								array(
									'table'=> 'company_names',
									'alias' => 'companyNames',
									'type' => 'left',
									'conditions'=> array('userDutyLogs.hospital_id = companyNames.id')
								),
								array(
									'table'=> 'user_devices',
									'alias' => 'userDevices',
									'type' => 'left',
									'conditions'=> array('User.id = userDevices.user_id')
								),
								
							),
							'conditions' => $conditions,
							'fields' => array('User.id', 'User.email', "CONCAT(userProfiles.first_name, ' ' , userProfiles.last_name) AS UserName", 'userDutyLogs.status AS onCall', 'companyNames.company_name', 'userDevices.status AS signedIn'),
							'group'=> array('User.id'),
							'order'=> 'User.id ASC, userDutyLogs.user_id DESC, userDevices.user_id DESC'
						)
					);
	//echo "<pre>"; print_r($userLists);die;
		return $userLists;

	}

}
?>