<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>The OCR</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL The OCR STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>favicon.ico" /> </head>
    <!-- END HEAD -->
    <?php
    echo $this->Html->css(array(
        'Signup.font-awesome.min',
        'Signup.simple-line-icons.min',
        'Signup.bootstrap.min',
        'Signup.uniform.default',
        'Signup.bootstrap-switch.min',
        'Signup.select2.min',
        'Signup.select2-bootstrap.min',
        'Signup.dropzone.min',
        'Signup.basic.min',
        'Signup.components',
        'Signup.plugins.min',
        'Signup.login-5',
        ));

    ?>
    <style type="text/css">
    .loaderId {
			    position: fixed;
			    left: 0px;
			    top: 0px;
			    width: 100%;
			    height: 100%;
			    z-index: 9999;
	        	background: url('<?php echo BASE_URL.'signup/images/page-loader.gif'; ?>') 50% 50% no-repeat rgba(79,74,74,0.459);
    	    }
    </style>
    <body class=" login">
    <span class="loaderId" style="display:none"></span>
        <!-- BEGIN : LOGIN PAGE 5-2 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 login-container bs-reset">
                    <img onclick="location.href='<?php echo BASE_URL; ?>';" class="login-logo login-6" src="<?php echo BASE_URL.'signup/images/logoBig.svg'; ?>" alt="" />
                    <div class="login-content">
                        <h1>To get started, you need to create an account. This is simple and won’t take long.
</h1>
                        <p class="step1">Thanks ! What’s your E-mail ID ?</p>
                        <div class="mainStepGroup">
							<div class="multiStep step1" id="multiStep1-1">
								<div class="multiStepLeft">1</div>
								<div class="multiStepRight">
									<div class="multiStepTxt">Let's proceed with your E-mail ID </div>
									<div class="multiStepLine"></div>
								</div>
							</div>
							<div class="multiStep marTop20px" id="multiStep1-2">
								<div class="multiStepLeft">1</div>
								<div class="multiStepRight">
									<div class="multiStepTxt">Let's proceed with your E-mail ID </div>
									<div class="multiStepLine"></div>
								</div>
							</div>
						</div>
                        <form action="javascript:;" class="login-form paddingLeft23label marTop80px" method="post" id="stepForm1">
                            <div class="alert alert-danger display-hide error1">
	                            <button class="close" data-close="alert"></button>
	                            <span id="error1">PLEASE ENTER E-MAIL ADDRESS </span>
	                          </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                   	<div class="form-group">
										<label>E-MAIL ADDRESS*</label>
										<textarea class="hidden" id="preApproved" ></textarea>
										<input class="form-control marBottom40px" type="text" autocomplete="off" placeholder="name@domain.com" id="textEmail" name="email" required />
									</div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                    <button class="btn blue marTop23px" type="submit" id="btnProceed">PROCEED</button>
                                    <button class="btn blue marTop23px updateProceed step2" type="submit">UPDATE</button>
                                </div>
                            </div>
                        </form>
                        
                        <div class="mainStepGroup">
							<div class="multiStep gray step1" id="multiStep2-1">
								<div class="multiStepLeft">2</div>
								<div class="multiStepRight">
									<div class="multiStepTxt">Let's see if you already have a profile with us</div>
									<div class="multiStepLine"></div>
								</div>
							</div>
							<div class="multiStep marTop20px multiStep1" id="multiStep2-2">
								<div class="multiStepLeft">2</div>
								<div class="multiStepRight">
									<div class="multiStepTxt">Let's see if you already have a profile with us</div>
									<div class="multiStepLine"></div>
								</div>
							</div>
							<div class="multiStep" id="multiStep2-3">
								<div class="multiStepLeft">2</div>
								<div class="multiStepRight">
									<div class="multiStepTxt">Let's see if you already have a profile with us</div>
									<div class="multiStepLine"></div>
								</div>
							</div>
						</div>
                        <form action="javascript:;" class="login-form paddingLeft23label marTop80px" method="post" id="stepForm2" style="display: none;">
                            <div class="alert alert-danger display-hide error2">
	                            <button class="close" data-close="alert"></button>
	                            <span id="error2">PLEASE ENTER FIRST NAME AND LAST NAME. </span>
	                          </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                   	<div class="form-group">
                                    	<label>FIRST NAME*</label>
                                    	<input class="form-control" type="text" autocomplete="off" placeholder="Enter Your First Name" id="textFirstName" name="first" required /> 
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                   	<div class="form-group">
                                    	<label>LAST NAME*</label>
                                    	<input class="form-control" type="text" autocomplete="off" placeholder="Enter Your Last Name" id="textLastName" name="last" required /> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                   	<div class="form-group">
                                    	<label>PROFESSION*</label>
                                    	<select class="form-control select3me" id="profession" name="profession" required>
                                        	<option>PLEASE SELECT</option>
                                        	<option>Option 1</option>
                                        	<option>Option 2</option>
                                        	<option>Option 3</option>
                                        	<option>Option 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                    <button class="btn blue marTop23px" type="submit" id="btnSubmit">SUBMIT</button>
                                    <button class="btn blue marTop23px updateSubmit step2" type="submit">UPDATE</button>
                                </div>
                            </div>
                        </form>
                        
                        <div class="multiStep gray marTop20px" id="multiStep3">
							<div class="multiStepLeft">3</div>
							<div class="multiStepRight">
								<div class="multiStepTxt">Claim your profile or Register Manually</div>
								<div class="multiStepLine"></div>
							</div>
						</div>
						<textarea class="hidden" id="country_code"></textarea>
						<textarea class="hidden" id="country_name"></textarea>
						<textarea class="hidden" id="county"></textarea>
                       	<!-- BEGIN CLAIM SECTION -->
						<div class="mainClaimSection" id="stepForm3">
							<div class="topClaimSection">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12 marAuto">
										<button class="btn blue marBottom30px btManual" type="button">REGISTER MANUALLY</button>
									</div>
								</div>
								<div class="row claimF">
									<div class="with100Center marAuto">
										<img src="<?php echo BASE_URL.'signup/images/claimDivider.svg'; ?>" alt="" />
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-xs-12">
										<div class="claimList claimF">
											<ul id="gmcData">
												<li>
													<div class="claimSingle">
														<div class="imgClaimSingle">
															<div class="imgPlaceholder">Denis Shepovalov</div>
														</div>
														<div class="nameClaimSingle">Denis Shepovalov</div>
														<div class="numClaimSingle">0544894</div>
														<div class="countryClaimSingle">United Kingdom</div>
													</div>
												</li>
											</ul>
										</div>
										<div class="userClaim claimF claimL">
											<div class="alert alert-danger display-hide error3">
											  <button class="close" data-close="alert"></button>
											  <span id="error3">PLEASE ENTER PASSWORD. </span>
											</div>
											<div class="userClaimTop">
												<button type="button" class="close btClose" data-dismiss="modal" aria-hidden="true"></button>
												<textarea class="hidden" id="Ref"></textarea>
												<textarea class="hidden" id="Fname"></textarea>
												<textarea class="hidden" id="Lname"></textarea>
												<h4 id="gmcUserName">Welcome Denis Shepovalov</h4>
											</div>
											<div class="userClaimBottom">
											<form action="javascript:;" class="claim-form" method="post">
												<p class="claimL">Please complete your profile by entering your password</p>
												<div class="row">
													<div class="col-xs-12 col-md-6 col-sm-6">
														<div class="form-group">
															<label>E-MAIL ADDRESS*</label>
															<input id="gmcEmail" class="form-control" type="text" autocomplete="off" maxlength="50" disabled="" placeholder="name@domain.com" name="email" required /> 
														</div>
													</div>
													<div class="col-xs-12 col-md-6 col-sm-6">
														<div class="form-group">
															<label>PASSWORD*</label>
															<input id="gmcPwd" class="form-control" type="password" autocomplete="off" maxlength="50" placeholder="PASSWORD" name="password" required /> 
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-8">
														<div class="privacyTxt">
															<input id="gmcCheck" type="checkbox" class="rem-checkbox" />
															<span class="text">I have read and agree to the <strong><a href="<?php echo BASE_URL . 'terms-and-conditions'; ?>" target="_blank">T &amp; C</a></strong> and <strong><a href="<?php echo BASE_URL . 'privacy-policy'; ?>" target="_blank">Privacy Policy</a></strong></span>
                                    					</div>
													</div>
												<div class="col-sm-4">
													<button id="gmcSubmit" class="btn blue marBottom30px" type="submit">SUBMIT</button>
												</div>
												</div>
												</form>
											</div>
										</div>
										<div class="userClaim manulF">
											<div class="alert alert-danger display-hide error4">
											  <button class="close" data-close="alert"></button>
											  <span id="error4">PLEASE ENTER PASSWORD. </span>
											</div>
											<div class="userClaimBottom">
											<div class="claim-form">
												<div class="row">
													<div class="col-xs-12 col-md-6 col-sm-6">
														<div class="form-group">
															<label>E-MAIL ADDRESS*</label>
															<input class="form-control" type="text" autocomplete="off" id="mannualEmail" disabled="" placeholder="name@domain.com" name="email" required /> 
														</div>
													</div>
													<div class="col-xs-12 col-md-6 col-sm-6">
														<div class="form-group">
															<label>PASSWORD*</label>
															<input id="mannualPswrd" class="form-control" type="password" autocomplete="off" placeholder="PASSWORD" name="password" required /> 
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12">
														<p class="txtGray2 marLeft18px manulF">UPLOAD YOUR PROFESSIONAL REGISTRATION CERTIFICATE</p>
														<form action="<?php echo BASE_URL."signup/signup/upload"; ?>" class="dropzone dropzone-file-area tooltips" data-original-title="Please provide us with one of the following: - Registration certificate from GMC, NPI, MCI, NMC and GDC- Photo of your Employment / Student / Professional ID Card." style="width: 100%; margin-top: 10px;" id="my-dropzone"></form>
														<p class="txtBlue2 manulF">All the information provided will be kept confidential on our secure servers and is for internal use only.</p>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-8">
														<div class="privacyTxt">
															<input id="mannualChck" type="checkbox" class="rem-checkbox" />
															<span class="text">I have read and agree to the <strong><a href="<?php echo BASE_URL . 'terms-and-conditions'; ?>" target="_blank">T &amp; C</a></strong> and <strong><a href="<?php echo BASE_URL . 'privacy-policy'; ?>" target="_blank">Privacy Policy</a></strong></span>
                                    					</div>
													</div>
												<div class="col-sm-4">
													<button id="mannualSubmit" class="btn blue marBottom30px" type="submit">SUBMIT</button>
												</div>
												</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                       	<!-- END CLAIM SECTION -->
                       	
                       
                    </div>
                </div>
                <div class="col-md-6 bs-reset">
                    <div class="login-bg">
                    	<!--<img src="images/welcome.svg" class="welcomeImg">-->
                    	<div class="step1" id="step1Callout">
							<div class="callOut top18 right9"><p>WELCOME TO <br>THE OCR</p></div>
						</div>
                   		<div class="step2 claimF" style="display: none;">
							<div class="callOut top17 right32"><p>Claim Your <br>Profile</p></div>
							<div class="callOut top17 right11 flipH"><p class="flipH">Or Register <br>Manually</p></div>
						</div>
                   		<div class="manulF" style="display: none;">
							<div class="callOut top22 right20 flipH"><p class="flipH">JuST TAKE! <br>FEW MINUTES</p></div>
						</div>
                   		<div class="claimL" style="display: none;">
							<div class="callOut top20 right24"><p class="marTop38px">congrats!</p></div>
							<div class="callOut top18 right4 flipH"><p class="flipH">let’s <br>begin</p></div>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : LOGIN PAGE 5-2 -->
        <!--[if lt IE 9]>
			<script src="js/respond.min.js"></script>
			<script src="js/excanvas.min.js"></script> 
		<![endif]-->
        
		<?php 
		echo $this->Html->script(array(
		    'Signup.jquery.min',
		    'Signup.bootstrap.min',
		    'Signup.js.cookie.min',
		    'Signup.bootstrap-hover-dropdown.min',
		    'Signup.jquery.slimscroll.min',
		    'Signup.jquery.blockui.min',
		    'Signup.jquery.uniform.min',
		    'Signup.bootstrap-switch.min',
		    'Signup.jquery.validate',
		    'Signup.additional-methods.min',
		    'Signup.select2.full.min',
		    'Signup.jquery.backstretch',
		    'Signup.dropzone',
		    'Signup.form-dropzone.min',
		    'Signup.app',
		    'Signup.login-5',
		    'Signup.jquery.uploadfile',
		    'Signup.randomColor',
		    'Signup.material-avatar',
		    'Signup.mbSignup',
		    ));
		?>
		<script>
			MaterialAvatar(document.getElementsByClassName('imgPlaceholder'), {shape: 'circle', randomColor: {hue: 'monochrome'}});
		</script>
		<script>
				$(document).ready(function()
				{
					// checkOpeningUser();

				var settings = {
					url: "upload.php",
					method: "POST",
					allowedTypes:"jpg,png,jpeg",
					fileName: "myfile",
					multiple: true,
					onSuccess:function(files,data,xhr)
					{
						$("#status").html("<font color='green'></font>");

					},
					onError: function(files,status,errMsg)
					{		
						$("#status").html("<font color='red'>Upload is Failed</font>");
					}
				}
				$("#mulitplefileuploader").uploadFile(settings);
				
				$('#stepForm2, #multiStep3').fadeOut();
				
				$('.mainClaimSection').fadeOut();
				
				$('#btnProceed, .updateProceed').click(function() {
					$('#profession').html('');
					$('#select2-profession-container').html('PLEASE SELECT');
					var x = $('#textEmail').val();
					var atpos = x.indexOf("@");
					var dotpos = x.lastIndexOf(".");
					// var chkReg="/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i";
					var chkReg=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

					// var letters = "^[A-Za-z]{1,50}$";  
					// if(!fname.match(letters)){  
					if(x ==""){
					  // alert("Enter a valid e-mail address");
					  $('.error1').css('display','block');
					  $("#error1").html("Please enter email.");
					  $('#textEmail').focus();
					    return false;
					}
					if (!chkReg.test(x)) {  
					// if (!chkReg.test(x)) {
					    // alert("Not a valid e-mail address");
					    $('.error1').css('display','block');
					    $("#error1").html("Please enter valid email.");
					    $('#textEmail').focus();
					    return false;
					}
					var chck=checkMail(x);
					var arr = chck.split('~');
					getProfessions();
					$('#preApproved').html(arr[1]);
					if(arr[0]!="0"){
						$('.error1').css('display','block');
						$("#error1").html("Email already exist.");
						$('#textEmail').focus();
						return false;
					}
					$('.error1').css('display','none');
					$( "p.step1, #multiStep1-1" ).hide();
					$('#multiStep1-2').removeClass('multiStep1');
					$('#multiStep1-1').addClass('multiStep1');
					$('#stepForm2, #multiStep1-2').css({"display":"block"});
					$('#multiStep2-1').removeClass('gray');
					$('#multiStep3').css({"display":"block"});
					$("html, body").animate({
						scrollTop: 300
					}, 300);
					setTimeout(function(){
					  $('#stepForm1').hide();
					  $('.login-logo, .login-content h1, .login-content p').hide();
					  $('.user-login-5 .login-container > .login-content').css("marginTop", '0');
					}, 100);
					return false;
				});
					
				$('.updateProceed').click(function() {
					$('#multiStep1-2').removeClass('passed');
					$( "#btnSubmit" ).show();
					$('.updateSubmit').css({"display":"none"});
					if ($('#btnSubmit').hasClass('passed')) {
						$('#btnSubmit').css({"display":"none"});
						$('.updateSubmit').css({"display":"block"});
					} else {
						$('#btnSubmit').css({"display":"block"});
						$('.updateSubmit').css({"display":"none"});
					}
					$('#multiStep2-2').css({"display":"block"});
					$('#multiStep2-3').css({"display":"none"});
					$('#multiStep2-2').addClass('multiStep1');
					$('.multiStep1').addClass('passed');
				});
				
				$('#multiStep1-2').click(function() {
					$('#multiStep2-2').css({"display":"none"});
					$('#multiStep2-3').css({"display":"block"});
					$(this).addClass('passed');
					$('#multiStep2-2').removeClass('multiStep1');
					$('#stepForm1').show();
					$('#stepForm2, #multiStep3').fadeOut();
					$('#stepForm3, #btnProceed, .claimF, .manulF, .claimL').css({"display":"none"});
					$('.updateProceed, #step1Callout').css({"display":"block"});
					if ($('#btnSubmit').hasClass('passed')) {
						$('#btnSubmit').css({"display":"none"});
						$('.updateSubmit').css({"display":"block"});
					} else {
						$('#btnSubmit').css({"display":"block"});
						$('.updateSubmit').css({"display":"none"});
					}
				});
					
				$('#multiStep2-2').click(function() {
					$('#multiStep3').css({"display":"block"});
				});
				
				$('#btnSubmit, .updateSubmit').click(function() {
					var fname=$('#textFirstName').val();
					var lname=$('#textLastName').val();
					var prof=$('#profession').val();
					if(fname==""){
					  $('.error2').css('display','block');
					  $("#error2").html("Please enter first name.");
					  $('#textFirstName').focus();
					  return false;
					}
					var letters = "^[A-Za-z]{1,50}$";  
					if(!fname.match(letters)){  
					   $('.error2').css('display','block');
					   $("#error2").html("Numeric and Special characters are not allowed in NAME. Please enter ... (A-Z, a-z) upto 50 max.");
					   $('#textFirstName').focus();
					  return false;  
					}  
					if(lname==""){
					  $('.error2').css('display','block');
					  $("#error2").html("Please enter last name.");
					  $('#textLastName').focus();
					  return false;
					}
					if(!lname.match(letters)){  
					  $('.error2').css('display','block');
					   $("#error2").html("Numeric and Special characters are not allowed in NAME. Please enter ... (A-Z, a-z) upto 50 max.");
					  $('#textLastName').focus();
					  return false;  
					}
					if(prof==null){
					  $('.error2').css('display','block');
					  $("#error2").html("Please select profession.");
					  return false;
					}
					var country=getCountry();
					var arr = country.split('~');
					var countryCode=arr['0'];
					$('#country_code').html(arr['0']);
					$('#country_name').html(arr['1']);
					$('#county').html(arr['2']);
					getGmc(prof,countryCode,fname,lname);
					$('.error2').css('display','none');  
					$(this).addClass('passed');
					$( ".step1" ).hide();
					$('#multiStep').hide();
					//$('div.multiStep1').attr('id', 'multiStep1');
					$('button.step1').addClass('multiStepBtn');
					$('.multiStep').removeClass("gray");
					$('.step2').fadeIn();
					$('.step2 .callOut').fadeIn();
					$('.mainClaimSection').fadeIn();
					$('.mainClaimSection').css({"visibility": "visible"});
					$('#multiStep2-2, #stepForm3, .claimF, .btManual').css({"display":"block"});
					$('.claimL, .manulF').css({"display":"none"});
					$("html, body").animate({
						scrollTop: 620
					}, 300);
					//$('.step2').fadeIn();
					setTimeout(function(){
					  $('.login-form').fadeOut();
					  $('.login-logo, .login-content h1, .login-content p').fadeOut();
					  $('.user-login-5 .login-container > .login-content').css("marginTop", '0');
					}, 300);
					return false;
				});
				
				$('.multiStep1').click(function() {
					$(this).addClass('passed');
					$('#btnSubmit').hide();
					$('.mainClaimSection').fadeOut();
					$( ".step1, .updateSubmit" ).show();
					$('#multiStep').show();
					$('.login-form').fadeIn();
					$( "button.step1.multiStepBtn, .claimF, .manulF, .claimL" ).hide();
					$('button.step1').removeClass('multiStepBtn');
					//$('div.multiStep1').removeAttr('id', 'multiStep1');
					$('.multiStep1, #stepForm1, #multiStep2-1, p.step1').css({"display":"none"});
					$('#multiStep2-2').css({"display":"block"});
				});
				
				$('.btManual').click(function() {
					$('#mannualEmail').val($('#textEmail').val());
					$(this).css({"display":"none"});
					$('.claimF').css({"display":"none"});
					$('.manulF').fadeIn();
					$('.manulF').css({"display":"block"});
				});
				
				// $('.updateSubmit').click(function() {
				// 	$('.multiStep1').removeClass('passed');
				// 	$('#multiStep2-2').removeClass('passed');
				// 	$( ".step1" ).hide();
				// 	$('#multiStep').hide();
				// 	//$('div.multiStep1').attr('id', 'multiStep1');
				// 	$('.step2').fadeIn();
				// 	$('.step2 .callOut').fadeIn();
				// 	$('.mainClaimSection').fadeIn();
				// 	$('.mainClaimSection').css({"visibility": "visible"});
				// 	$('.claimF, .btManual').css({"display":"block"});
				// 	$('.claimL, .manulF').css({"display":"none"});
				// 	$("html, body").animate({
				// 		scrollTop: 420
				// 	}, 300);
				// 	//$('.step2').fadeIn();
				// 	setTimeout(function(){
				// 	  $('.login-form').fadeOut();
				// 	  $('.login-logo, .login-content h1, .login-content p').fadeOut();
				// 	  $('.user-login-5 .login-container > .login-content').css("marginTop", '0');
				// 	}, 600);
				// 	return false;
				// });
				$('.claimList li').click(function() {
					$('.claimF').css({"display":"none"});
					$('.btManual').css({"display":"none"});
					$('.claimL').fadeIn();
					$('.manulF').css({"display":"none"});
				});
				
				$('.btClose').click(function() {
					$('.claimL').css({"display":"none"});
					$('.step2').fadeIn();
					$('.step2 .callOut').fadeIn();
					$('.claimF').fadeIn();
					$('.btManual').fadeIn();
					$('.userClaim').css({"display":"none"});
				});
				/*$(window).scroll(function () {
					if ($(this).scrollTop() > 600) {
						$('.claimList').fadeIn();
					} else {
						$('.login-form').fadeOut();
					}
				});*/
				$('#gmcSubmit').click(function(){
					var pwd=$('#gmcPwd').val();
					// var passReg ="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,20}$";
					var passReg ="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,20}$";
					if(pwd==""){
					  $('.error3').css('display','block');
					  $("#error3").html("PLEASE ENTER PASSWORD.");
					  $('#gmcPwd').focus();
					  return false;
					}
					if(!pwd.match(passReg)){ 
					  $('.error3').css('display','block');
					  $("#error3").html("PASSWORD REQUIRE MINIMUM 6 CHARACTERS AND MAXIMUM 20 CHARACTERS WITH 1 UPPERCASE,1 LOWERCASE,1 NUMBER."); 
					  $('#gmcPwd').focus();
					  return false;  
					}
					if($('#gmcCheck').prop("checked") == false){
					  $('.error3').css('display','block');
					  $("#error3").html("PLEASE ACCEPT TERMS AND CONDITIONS.");
					  return false;
					}
					$('.error3').css('display','none');
					gmcFinalSubmit(); 
				});
				$('#mannualSubmit').click(function(){
					var pwd=$('#mannualPswrd').val();
					// var passReg ="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,20}$";
					var passReg ="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,20}$";
					if(pwd==""){
					  $('.error4').css('display','block');
					  $("#error4").html("Please enter password.");
					  $('#mannualPswrd').focus();
					  return false;
					}
					if(!pwd.match(passReg)){ 
					  $('.error4').css('display','block');
					  $("#error4").html("Password require minimum 6 characters, maximum 20 characters long, 1 upper case, 1 lower case and 1 number."); 
					  $('#mannualPswrd').focus();
					  return false;  
					}
					if($('#preApproved').html()=="1"){
					}else{
						if($('.dz-success .dz-image').length == 0) {
						 	$('.error4').css('display','block');
						 	$("#error4").html("Please select your professional registration certificate.");
						 	return false;
						}
						if($('.dz-success .dz-error-message span').html()!=""){
							$('.error4').css('display','block');
							$("#error4").html("Only jpg, jpeg and png images are allowed with max size 10 MB.");
							return false;
						}
					}
					if($('#mannualChck').prop("checked") == false){
					  $('.error4').css('display','block');
					  $("#error4").html("Please accept our Terms & Conditions and Privacy Policy.");
					  return false;
					}
					$('.error4').css('display','none');
					mannualSignup();
				});

			});
		</script>
		
    </body>

</html>