function checkOpeningUser(){
    $.ajax({
           type: 'POST',
           async:false,
           url:'<?php echo BASE_URL; ?>signup/signup/checkCountryOfOpeningUser',
           data:{user_id: 1},
           success: function(data) {
              if(data==1){
               return true;
              }else{
               window.location.href="othercountry.html"
              }
           }
           
   }); 
return true;
}

function checkMail(email){
     var returnVal = 0;
     $.ajax({
            type: 'POST',
            async:false,
            url:'<?php echo BASE_URL; ?>signup/signup/checkMail',
            data:{email: email},
            success: function(data) {
              
                returnVal=data;
            }
            
    }); 

     return returnVal;
}

function getProfessions(){
     $.ajax({
            type: 'POST',
            async:false,
            url:'<?php echo BASE_URL; ?>signup/signup/getProfessions',
            success: function(data) {
                $('#profession').html(data);
            }    
    }); 
}

function getCountry(){
    var returnVal = '';
     $.ajax({
            type: 'POST',
            async:false,
            url:'<?php echo BASE_URL; ?>signup/signup/getCountry',
            success: function(data) {
                returnVal=data;
            } 
    });
    return returnVal; 
}

function getGmc(prof,country,fname,lname){
     $.ajax({
            type: 'POST',
            async:false,
            url:'<?php echo BASE_URL; ?>signup/signup/getGmcData',
            data:{profession: prof, country: country, fname: fname, lname: lname},
            success: function(data) {
                $('#gmcData').html(data);
            } 
    });
}

function gmcRegister(ref){
    $('.claimF').css({"display":"none"});
    $('.btManual').css({"display":"none"});
    $('.claimL').fadeIn();
    $('.manulF').css({"display":"none"});
    $('#Ref').html(ref);
    $('#Fname').html($('#'+ref+' #gmcFname').html());
    $('#Lname').html($('#'+ref+' #gmcLname').html());
    $('#gmcEmail').val($('#textEmail').val());
    $('#gmcUserName').html('Welcome '+$('#'+ref+' .nameClaimSingle').html());
}
function gmcFinalSubmit(){
    var email=$('#textEmail').val();
    var fname=$('#Fname').val();
    var lname=$('#Lname').val();
    var prof=$('#profession').val();
    var refNo=$('#Ref').val();
    var pswrd=$('#gmcPwd').val();
    var countryCode=$('#country_code').html();
    var countryName=$('#country_name').html();
    var county=$('#county').html();
     $.ajax({
            type: 'POST',
            global: false,
            cache: false,
            url:'<?php echo BASE_URL; ?>signup/signup/GmcSignup',
            beforeSend:function (){ 
               $(".loaderId").css("display","inline");
            },
            data:{email: email, fname: fname, lname: lname, prof: prof, refNo: refNo, pswrd: pswrd, countryCode: countryCode, countryName: countryName, county: county},
            success: function(data) {
              $(".loaderId").css("display","none");
              if(data=="ok"){
              window.location.href="https://www.surveymonkey.co.uk/r/X27M5LV";
              }
            } 
    });
}

function mannualSignup(){
  // if($('#preApproved').html()=="1"){
  if($('.dz-success .dz-image').length == 0) {
    img="";
    ext="";
  }else{
    img=$('.dz-success .dz-image img').attr('src');
    ext=$('.dz-success .dz-filename span').html();
    
    img=img.split("base64,");
    img=img[1];
    
    ext=ext.split(".");
    ext=ext[(ext.length)-1];
  }
  var preAprovd=$('#preApproved').html();
  var email=$('#textEmail').val();
  var fname=$('#textFirstName').val();
  var lname=$('#textLastName').val();
  var prof=$('#profession').val();
  var pswrd=$('#mannualPswrd').val();
  var countryCode=$('#country_code').html();
  var countryName=$('#country_name').html();
  var county=$('#county').html();
   $.ajax({
          type: 'POST',
          global: false,
          cache: false,
          url:'<?php echo BASE_URL; ?>signup/signup/mannualSignup',
          beforeSend:function (){ 
             $(".loaderId").css("display","inline");
          },
          data:{img: img, ext: ext, email: email, fname: fname, lname: lname, prof: prof, pswrd: pswrd, countryCode: countryCode, countryName: countryName, county: county},
          success: function(data) {
            $(".loaderId").css("display","none");
            if(data=="ok"){
              if(preAprovd!=1){
              window.location.href='<?php echo BASE_URL."signup/signup/thank/'+fname+'" ?>';
              }else{
                window.location.href="https://www.surveymonkey.co.uk/r/X27M5LV";
              }
            }
          } 
  });
}







