<?php 
/*
Model represents gmc_users table
*/

App::uses('AppModel', 'Model');

class GmcUkUser extends AppModel {
	
	/*
	On: 22-07-2015
	I/P: $params = array(), $returnType = json/array
	O/P: Gmc users info
	Desc: Fetch all Gmc users info.
	*/
	public function gmcUserSearch( $params = array(), $returnType = 'array' ){
		$gmcUsers = array();
		if( !empty($params) ){ 
			$limit = empty($params['size'])?PAGE_SIZE:$params['size'];
			$conditions = " is_registered = 0 ";
			if(!empty($params['first_name'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND LOWER(GivenName) LIKE LOWER("'.$params['first_name'].'%") ';
				}else{
					$conditions .= ' LOWER(GivenName) LIKE LOWER("'.$params['first_name'].'%") ';
				}
			}
			if(!empty($params['last_name'])){
				if(strlen($conditions) >0){
						$conditions .= ' AND LOWER(Surname) LIKE LOWER("'.$params['last_name'].'%") ';
					}else{
						$conditions .= ' LOWER(Surname) LIKE LOWER("'.$params['last_name'].'%") ';
					}
			}
			/*if(!empty($params['country'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND placeofqualificationcountry LIKE "'.$params['country'].'%" ';
				}else{
					$conditions .= ' placeofqualificationcountry LIKE "'.$params['country'].'%" ';
				}
			}*/
			if(!empty($params['specilities'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND Specialty1 LIKE "'.$params['specilities'].'%" ';
				}else{
					$conditions .= ' Specialty1 LIKE "'.$params['specilities'].'%" ';
				}
			}
			$gmcUsers = $this->find('all', 
							array(
								'conditions' => $conditions,
								'limit' => $limit,
								'order'=> array('GivenName'=> 'ASC', 'Surname'=> 'ASC')
							)
						);
		}
		return $gmcUsers; 
	}

	/*
	On: 05-08-2015
	I/P: 
	O/P:
	Desc: 
	*/
	public function updateFields( $fields = array(), $conditions = array() ){
		$return = 0;
		if( !empty($fields) && !empty($conditions) ){
			$this->updateAll( $fields, $conditions );

			if( $this->getAffectedRows() ){
				$return = 1;
			}
		}
		return $return;
	}
	
	
}
?>