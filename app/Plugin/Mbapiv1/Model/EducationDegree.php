<?php 
/*
Represent model for education degree.
*/
App::uses('AppModel', 'Model');

class EducationDegree extends AppModel {
	
	/*
	--------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: Displays all company names.
	--------------------------------------------------------------------------
	*/
	public function educationDegreeLists( $params = array() ){
		$educationDegreeLists = array();
		if( !empty($params) ){
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			$conditions = " status = 1 AND created_by = 0 ";
			if(!empty($params['searchText'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND LOWER(degree_name) LIKE LOWER("'.$params['searchText'].'%") ';
				}
			}
			$educationDegreeLists = $this->find("all", array(
										"conditions"=> $conditions,
										"order"=> array("degree_name"),
										'limit'=> $pageSize,
										'offset'=> $offsetVal
										)
									);
		}
		return $educationDegreeLists;
	}

}
?>