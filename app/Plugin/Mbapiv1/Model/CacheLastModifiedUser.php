<?php


App::uses('Model', 'Model');

/**
 * Represents model CacheLastModifiedUser
 */
class CacheLastModifiedUser extends Model {

	/*
	------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: 
	------------------------------------------------------------------------------
	*/
	public function updateLastModifiedDate($userId=NULL, $companyId=NULL, $modifiedType=NULL){
		if(!empty($companyId)){
			$quer = "UPDATE cache_last_modified_users SET last_modified=now() WHERE user_id=$userId AND company_id = $companyId AND type='$modifiedType'";
			$data = $this->query($quer);
    	}
	}


	public function updateLastModifiedDateDynamicDirectory($userId=NULL, $companyId=NULL, $modifiedType=NULL, $pkid){
		if(!empty($companyId)){
			$quer = "UPDATE cache_last_modified_users SET last_modified=now() WHERE user_id=$userId AND id= $pkid AND company_id = $companyId AND type='$modifiedType'";
			$data = $this->query($quer);
    	}
	}

	/*
	------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: 
	------------------------------------------------------------------------------
	*/
	public function getStaticLastModifiedDate($params=array()){
		$data =array();
		if(!empty($params)){
			$companyId = $params['company_id'];
			$eTagDate = $params['eTagData'];
			$quer = "SELECT last_modified FROM cache_last_modified_users WHERE company_id=$companyId AND type='static_directory_changed'";
			$data = $this->query($quer);
    	}
    	return $data;
	}

	/*
	------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: 
	------------------------------------------------------------------------------
	*/
	public function getDeletedLastModifiedData($params=array()){
		$data =array();
		if(!empty($params)){
			$companyId = $params['company_id'];
			$eTagDate = $params['eTagData'];
			$userId = $params['user_id'];
			$quer = "SELECT last_modified FROM cache_last_modified_users WHERE company_id=$companyId AND type='deleted_user'";
			$data = $this->query($quer);
    	}
    	return $data;
	}

	/*
	------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: 
	------------------------------------------------------------------------------
	*/

	public function updateDeletedUser($userId=NULL, $companyId=NULL, $modifiedType=NULL, $deletedStatus){
		if(!empty($userId)){
			$quer = "UPDATE cache_last_modified_users SET last_modified=now(), status=$deletedStatus WHERE user_id=$userId AND company_id = $companyId AND type='$modifiedType'";
			$data = $this->query($quer);
    	}
	}

	/*
	------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: 
	------------------------------------------------------------------------------
	*/

	public function updateDeletedUserCache($companyId=NULL){
		if(!empty($companyId)){
			$quer = "UPDATE cache_last_modified_users SET last_modified=now(), status=0 WHERE company_id = $companyId AND type='deleted_user'";
			$data = $this->query($quer);
    	}
	}

	/*
	------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: 
	------------------------------------------------------------------------------
	*/
	public function getLastModifiedDateDynamicDirectory($companyId=NULL){
		$data =array();
		if(!empty($companyId)){
			$quer = "SELECT last_modified FROM cache_last_modified_users WHERE company_id=$companyId AND type='dynamic_directory_changed' AND status=1 ORDER BY last_modified DESC LIMIT 1";
			$data = $this->query($quer);
		}
		return $data;
	}

	
}
