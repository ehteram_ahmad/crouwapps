<?php
/*
-----------------------------------------------------------------------------------------------
	Desc: Controller used to do some scheduled tasks.
-----------------------------------------------------------------------------------------------
*/

App::uses('AppController', 'Controller');

class MbcronController extends AppController {
	public $uses = array('Mb.User', 'Mb.UserProfile', 'Mb.UserColleague', 'Mb.SyncContact', 'Mb.UserFollow','Mb.EmailSmsLog','Mb.UserSubscriptionLog','Mb.CompanyName','Mbapiv1.VoipPushNotification', 'Mbapiv1.UserQbDetail', 'Mbapiv1.UserOneTimeToken', 'Mbapiv1.ChatDeleteTransaction','Mbapiv1.VoipPushNotification','Mbapiv1.UserBatonRole','Mbapiv1.DepartmentsBatonRole','Mbapiv1.AppStatsTransaction','Mbapiv1.VoipPushNotificationDetail');
	public $components = array('Mb.MbCommon', 'Mb.MedicBleep','Mb.MbEmail','Quickblox');
	/*
	-----------------------------------------------------------------------------------------------
	On: 23-05-2016
	I/P: 
	O/P: 
	Desc: Auto colleague
	-----------------------------------------------------------------------------------------------
	*/
	public function setColleagueFromSyncListOld(){
		ini_set('max_execution_time', 300);
		$syncData = $this->SyncContact->find("all", array("conditions"=> array("is_sync_done"=> 0), "order"=> "id DESC"));
		foreach($syncData as $sData){
			$contactNo = ""; $email = ""; $userIdSyncBy = "";
			$contactNo = !empty($sData['SyncContact']['mobile_no']) ? $sData['SyncContact']['mobile_no'] : "";
			$email = !empty($sData['SyncContact']['email']) ? $sData['SyncContact']['email'] : "";
			$userIdSyncBy = $sData['SyncContact']['user_id'];
			$userSyncByDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdSyncBy)));
			//** Conditions
			$userData = array();
			//** Check User by mobile/email
			if(!empty($contactNo)){
				$conditions = array(
								"UserProfile.contact_no"=> $contactNo
								);
				$userData = $this->User->find("first", array("conditions"=> $conditions));
			}else if(!empty($email)){
				$conditions = array(
								"User.email"=> $email
								);
				$userData = $this->User->find("first", array("conditions"=> $conditions));
			}
			//echo "<pre>";print_r($userData);die;
			//** If user found from sync list then make colleague
			if(!empty($userData)){
				$userIdToBeSync = $userData['User']['id'];
				$isSyncDone = false;
				if($userData['UserProfile']['country_id'] == $userSyncByDetails['UserProfile']['country_id']){
					//** Set Colleague
					$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync)));
					$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy)));
					if($userIdSyncBy != $userIdToBeSync){ //** Don't Make colleague to self user
						if($myColleague == 0 && $meColleague == 0){
							$colleagueData = array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync, "status"=> 2);
							$this->UserColleague->saveAll($colleagueData);
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}else{
							/*$colleagueCondition = array();
							if($myColleague > 0){
								$colleagueCondition = array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync);
								$checkColleagueStatus = $this->UserColleague->find("first", array("conditions"=> $colleagueCondition));
								if($checkColleagueStatus['UserColleague']['status'] == 0){
									$inviteColleague = $this->UserColleague->updateAll(array("status"=> 2), $colleagueData );
								}
							}*/
							/*else{
								$colleagueCondition = array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy);
								$checkColleagueStatus = $this->UserColleague->find("first", array("conditions"=> $colleagueCondition));
								if($checkColleagueStatus['UserColleague']['status'] == 0){
									$inviteColleague = $this->UserColleague->updateAll( array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy,"status"=> 2), array("id"=> $checkColleagueStatus['UserColleague']['id']) );
								}
							}*/
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}

						//** If any user send colleague request also add user follow[START]
							try{
								$conditions = array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync);
								$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
								if($userFollowCheck == 0 ){
									$this->UserFollow->save( array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync, "follow_type"=> 1, "status"=> 1) );
								}else{
									$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
								}
							}catch( Exception $e ){}
						//** If any user send colleague request also add user follow[END]
					}
					//** Set SyncContact Model value to true
					$isSyncDone = true;
				}
				//** Update SyncContact Model
				if($isSyncDone){
					$this->SyncContact->updateAll(array('is_sync_done' => 1 ), array("id"=> $sData['SyncContact']['id']));
				}
					/* Accept/Remove friend  request Quickblox start */
						/*$senderDetails = $this->User->findById($userIdSyncBy );
						$receiverDetails = $this->User->findById($userIdToBeSync);
						try{ 
							$this->MedicBleep->quickAddRemoveDialog($senderDetails, $receiverDetails, 1);
						}catch( Exception $e ){}*/
					/* Accept/Remove friend request Quickblox end */
			}
		}
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 29-07-2016
	I/P: 
	O/P: 
	Desc: Auto colleague
	-----------------------------------------------------------------------------------------------
	*/
	public function setColleagueFromSyncList(){
		ini_set('max_execution_time', 300);
		ini_set('memory_limit', '256M');
		//** Fetch all sync contacts with email matching[START]
		$conditions = array(
							"SyncContact.is_sync_done"=> 0,
							"User.status"=> 1,
							"User.approved"=> 1,
						);
		$fields = array("User.id, User.email, SyncContact.id, SyncContact.user_id, SyncContact.email, SyncContact.is_sync_done");
		//$totCountFields = array("count(*) AS totRecord");
		$options = array(
								'joins'=>
								array(
									array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'INNER',
										'conditions'=> array(
											'User.email = SyncContact.email',
											"SyncContact.user_id <> User.id",
											)
									),
								),
								'conditions' => $conditions,
								'fields'=> $fields,
								//'limit'=> $pageSize,
								//'offset'=> $offsetVal,
								'group'=> array('User.id, SyncContact.user_id'), 
								'order'=> 'SyncContact.id DESC'
							);
		$userDataEmailSync = $this->SyncContact->find('all', $options);
		foreach($userDataEmailSync as $sData){

			$userIdSyncBy = ""; $userIdToBeSync = "";
			$userIdSyncBy = $sData['SyncContact']['user_id'];
			$userSyncByDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdSyncBy)));
			//** User to be sync details
			$userData = array();
			$userIdToBeSync = $sData['User']['id'];
			$userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdToBeSync)));
			//** If user found from sync list then make colleague
			if(!empty($userData)){
				$isSyncDone = false;
				if($userData['UserProfile']['country_id'] == $userSyncByDetails['UserProfile']['country_id']){
					//** Set Colleague
					$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync)));
					$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy)));
					if($userIdSyncBy != $userIdToBeSync){ //** Don't Make colleague to self user
						if($myColleague == 0 && $meColleague == 0){
							$colleagueData = array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync, "status"=> 2);
							$this->UserColleague->saveAll($colleagueData);
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}else{
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}

						//** If any user send colleague request also add user follow[START]
							try{
								$conditions = array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync);
								$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
								if($userFollowCheck == 0 ){
									$this->UserFollow->save( array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync, "follow_type"=> 1, "status"=> 1) );
								}else{
									$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
								}
							}catch( Exception $e ){}
						//** If any user send colleague request also add user follow[END]
					}
					//** Set SyncContact Model value to true
					$isSyncDone = true;
				}
				//** Update SyncContact Model
				if($isSyncDone){
					$this->SyncContact->updateAll(array('is_sync_done' => 1 ), array("id"=> $sData['SyncContact']['id']));
				}
			}
		}
		//echo "<pre>"; print_r($userDataEmailSync);
		//** Fetch all sync contacts with email matching[END]									
		

		//** Fetch all sync contacts with Mobile matching[START]
		$conditionsMobileSync = array(
							"SyncContact.is_sync_done"=> 0,
						);
		$fieldsMobileSync = array("UserProfile.user_id, SyncContact.id, SyncContact.user_id, SyncContact.email, SyncContact.is_sync_done");
		$optionsMobileSync = array(
								'joins'=>
								array(
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfile',
										'type' => 'INNER',
										'conditions'=> array(
											'UserProfile.contact_no = SyncContact.mobile_no',
											"SyncContact.user_id <> UserProfile.user_id",
											)
									),
								),
								'conditions' => $conditionsMobileSync,
								'fields'=> $fieldsMobileSync,
								//'limit'=> $pageSize,
								//'offset'=> $offsetVal,
								'group'=> array('UserProfile.user_id, SyncContact.user_id'), 
								'order'=> 'SyncContact.id DESC'
							);
		$userDataMobileSync = $this->SyncContact->find('all', $optionsMobileSync);
		//echo "<pre>"; print_r($userDataMobileSync);
		foreach($userDataMobileSync as $sData){

			$userIdSyncBy = ""; $userIdToBeSync = ""; $userSyncByDetails = array();
			$userIdSyncBy = $sData['SyncContact']['user_id'];
			$userSyncByDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdSyncBy)));
			//** User to be sync details
			$userData = array();
			$userIdToBeSync = $sData['UserProfile']['user_id'];
			$userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdToBeSync)));
			//** If user found from sync list then make colleague
			if(!empty($userData)){
				$isSyncDone = false;
				if($userData['UserProfile']['country_id'] == $userSyncByDetails['UserProfile']['country_id']){
					//** Set Colleague
					$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync)));
					$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy)));
					if($userIdSyncBy != $userIdToBeSync){ //** Don't Make colleague to self user
						if($myColleague == 0 && $meColleague == 0){
							$colleagueData = array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync, "status"=> 2);
							$this->UserColleague->saveAll($colleagueData);
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}else{
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}

						//** If any user send colleague request also add user follow[START]
							try{
								$conditions = array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync);
								$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
								if($userFollowCheck == 0 ){
									$this->UserFollow->save( array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync, "follow_type"=> 1, "status"=> 1) );
								}else{
									$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
								}
							}catch( Exception $e ){}
						//** If any user send colleague request also add user follow[END]
					}
					//** Set SyncContact Model value to true
					$isSyncDone = true;
				}
				//** Update SyncContact Model
				if($isSyncDone){
					$this->SyncContact->updateAll(array('is_sync_done' => 1 ), array("id"=> $sData['SyncContact']['id']));
				}
			}
		}
		//echo "<pre>"; print_r($userDataMobileSync);die;
		//** Fetch all sync contacts with mobile matching[END]
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 14-09-2017
	I/P: 
	O/P: 
	Desc: Send reminder mail to free trail user
	-----------------------------------------------------------------------------------------------
	*/

	public function freeTrailReminderMail()
	{
		$freeTrialUser = $this->UserSubscriptionLog->find("all", array("conditions"=> array("subscribe_type"=> 0)));
		foreach ($freeTrialUser AS $user) {
			// $userEmail = $this->User->find("first", array("conditions"=> array("user_id"=> $user['UserSubscriptionLog']['user_id'])));
			$userLists = $this->User->getUserDetail( $user['UserSubscriptionLog']['user_id'] );
			$subscriptionExpiryDate = $user['UserSubscriptionLog']['subscription_expiry_date'];
			$currentDate = date('Y-m-d H:i:s');
			//$subscriptionPerriod = ceil(abs(strtotime($subscriptionExpiryDate) - strtotime($currentDate)) / 86400);
			$subscriptionPerriod = floor((strtotime($subscriptionExpiryDate) - strtotime($currentDate)) / 86400);
			if($subscriptionPerriod == 0 || $subscriptionPerriod == 1 || $subscriptionPerriod == 2 || $subscriptionPerriod == 7 || $subscriptionPerriod ==15 )
			{
				if(isset($userLists[0]['User']['email']))
				{
					try{
						$mailSend = $this->MbEmail->sendFreeSubscriptionMail( $subscriptionPerriod, $userLists[0]['User']['email'], $userLists[0][0]['usrName'] );
					}catch(Exception $e){
						$mailSend = $e->getMessage();
					}
					$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $user['UserSubscriptionLog']['user_id'], "notify_type"=> "Free Trial Subscription Mail") );
				}
			}
		}
		echo "Done";exit();
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 13-12-2017
	I/P: 
	O/P: 
	Desc: Check institute and user subscription
	-----------------------------------------------------------------------------------------------
	*/

	public function checkUserAndInstituteSubscription()
	{
		$userLists = $this->User->find("all", array("conditions"=> array("status"=> 1, "approved"=>1)));
		foreach ($userLists as $users) {

			$userCurrentCompany = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $users['User']['id']), "order"=> array("id DESC")));
			if(! empty($userCurrentCompany))
			{
				//********Check user subscription expiry date ********//
				if(strtotime($userCurrentCompany['UserSubscriptionLog']['subscription_expiry_date']) <= strtotime(date('Y-m-d 23:59:59')))
				{
					$conditions = array("user_id"=> $userCurrentCompany['UserSubscriptionLog']['user_id'], "company_id"=> $userCurrentCompany['UserSubscriptionLog']['company_id']);
					$this->UserSubscriptionLog->updateAll( array("subscribe_type"=> 3), $conditions );
				}

				//********Check Institute subscription expiry date ********//
				else if( ($userCurrentCompany['UserSubscriptionLog']['company_id'] != -1) || ($userCurrentCompany['UserSubscriptionLog']['company_id'] != 0) )
				{
					$dataParams['company_id'] = $userCurrentCompany['UserSubscriptionLog']['company_id'];
					if($dataParams['company_id'] == 0)
					{

					}
					else
					{
						$checkInstituteSubscription = $this->CompanyName->getComapanySubscriptionDetailsForCron($dataParams);
						if($checkInstituteSubscription == 1)
						{
							$conditions = array("user_id"=> $userCurrentCompany['UserSubscriptionLog']['user_id'], "company_id"=> $dataParams['company_id']);
							$this->UserSubscriptionLog->updateAll( array("subscribe_type"=> 3), $conditions );
						}
					}
				}
			}
		}
		echo "Done";
		exit();
		
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 03-10-2018
	I/P: 
	O/P: 
	Desc: Check institute and user subscription
	-----------------------------------------------------------------------------------------------
	*/

	public function voipPushNotification()
	{
		$responseData = array();
		$dtoken = '';
		if($this->request->is('post')){
			// if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$dataUserDevice['user_id'] = isset($dataInput['user_id']) ? $dataInput['user_id']:0;
				$dataUserDevice['device_id'] = isset($dataInput['device_id']) ? $dataInput['device_id']:'';
				$dataUserDevice['device_type'] = isset($dataInput['device_type']) ? $dataInput['device_type']:'';
				$dataUserDevice['application_type'] = isset($dataInput['application_type']) ? $dataInput['application_type']:'MB';
				$dataUserDevice['voip_notification'] = isset($dataInput['voip_notification']) ? $dataInput['voip_notification']:0;
				$dataUserDevice['device_token'] = isset($dataInput['device_token']) ? $dataInput['device_token']:'';
				$dataUserDevice['fcm_voip_token'] = isset($dataInput['fcm_voip_token']) ? $dataInput['fcm_voip_token']:'';

				$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataUserDevice['user_id'])));

				if(!empty($dataUserDevice['device_token']) && !empty($dataUserDevice['fcm_voip_token']))
				{
					$device_token = $dataUserDevice['device_token'];
					$fcm_voip_token = $dataUserDevice['fcm_voip_token'];
				}
				else
				{
					$userDetails['User']['id'] = $dataUserDevice['user_id'];
					$oneTimeToken = $this->genrateRandomToken($userDetails['User']['id']);
					$oneTimeTokenData = array("user_id"=> $userDetails['User']['id'], "token"=> $oneTimeToken);
					$this->UserOneTimeToken->saveAll($oneTimeTokenData);

					$email = $userDetails['User']['email'];
					$tokenDetails = $this->Quickblox->quickLogin($email, $oneTimeToken);
					// echo "<pre>";print_r($tokenDetails);exit();
					$token = $tokenDetails->session->token;
					$user_id = $tokenDetails->session->user_id;
					$user_details = $this->Quickblox->getUserSubscription($token);
					// echo "<pre>";print_r($user_details);exit();
					foreach ($user_details as $key => $value) {
						if($value->subscription->device->udid == $dataUserDevice['device_id'] )
						{
							if($value->subscription->notification_channel->name == 'apns_voip')
							{
								$fcm_voip_token = $value->subscription->device->client_identification_sequence;
							}
							if($value->subscription->notification_channel->name == 'gcm')
							{
								$fcm_voip_token = $value->subscription->device->client_identification_sequence;
							}
						}
					}
				}
				// $user_devices_subscription = end($user_details);
				// $device_token = $user_devices_subscription->subscription->device->client_identification_sequence;
				// echo $token."<br />";
				// echo "<pre>";print_r($user_details);exit();


				if(! empty($dataUserDevice))
				{
					$getDeviceDetail = $this->VoipPushNotification->find("first", array("conditions"=> array("device_id"=> $dataUserDevice['device_id'])));
					if( !empty($getDeviceDetail))
					{
						$currentDateFormate = date('Y-m-d H:i:s');
						$userDeviceId = $dataUserDevice['device_id'];
						// echo "<pre>";print_r($userDeviceId );exit();
						if(!empty($fcm_voip_token))
						{
							$this->VoipPushNotification->updateAll(array("date_created"=> "'".$currentDateFormate."'" , "push_notification_status"=> $dataUserDevice['voip_notification'], "device_token"=> "'".$device_token."'", "fcm_voip_token"=> "'".$fcm_voip_token."'", "user_id"=>$dataUserDevice['user_id']), array("device_id"=> $userDeviceId));
						}
						else
						{
							$this->VoipPushNotification->updateAll(array("date_created"=> "'".$currentDateFormate."'" , "push_notification_status"=> $dataUserDevice['voip_notification'], "user_id"=>$dataUserDevice['user_id']), array("device_id"=> $userDeviceId));
						}
					}
					else
					{
						$insertUserdeviceDetail = array("user_id"=>$dataUserDevice['user_id'], "device_id"=> $dataInput['device_id'], "device_type"=> $dataInput['device_type'], "device_token"=>$device_token, "fcm_voip_token"=>$fcm_voip_token, "application_type"=> $dataInput['application_type'], "push_notification_status"=> $dataUserDevice['voip_notification'], "status"=>1, "date_created"=> date('Y-m-d H:i:s'));
						$this->VoipPushNotification->save($insertUserdeviceDetail);
					}
				}
				//To do else response message
				if($dataUserDevice['voip_notification'] == "2")
				{
					$insertChatDeleteTransaction = array("user_id"=>$dataUserDevice['user_id'], "device_id"=> $dataInput['device_id'], "device_type"=> $dataInput['device_type'], "application_type"=> $dataInput['application_type'], "status"=>1);
					// echo "<pre>";print_r($insertChatDeleteTransaction);exit;
					$this->ChatDeleteTransaction->saveAll($insertChatDeleteTransaction);
				}
				try{
					$appStats = array("user_id"=>$dataUserDevice['user_id'], "device_id"=> $dataInput['device_id'], "device_type"=> $dataInput['device_type'], "device_token"=>$device_token, "fcm_voip_token"=>$fcm_voip_token, "application_type"=> $dataInput['application_type'], "push_notification_status"=> $dataUserDevice['voip_notification'], "status"=>1, "date_created"=> date('Y-m-d H:i:s'));
						$this->VoipPushNotificationDetail->save($appStats);
				}catch(Exception $e){}
				//App Stats Transactions[END]
				$responseData = array('method_name'=> 'voipPushNotification','status'=>'1','response_code'=>'200','message'=> ERROR_200, 'data'=>$fcm_voip_token, 'detail'=>$getDeviceDetail);

			// }else{
			// $responseData = array('method_name'=> 'getInstitutesMasterData', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
		$responseData = array('method_name'=> 'voipPushNotification','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	echo json_encode($responseData);
    	exit();
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 03-10-2018
	I/P: 
	O/P: 
	Desc: Send Push notification
	-----------------------------------------------------------------------------------------------
	*/

	// public function sendNotificationData()
	// {

	// 	$conditions = array(
	// 			'VoipPushNotification.push_notification_status' => 0
	// 		);
	// 	$joins = array(
	// 			array(
	// 				'table' => 'user_qb_details',
	// 				'alias' => 'UserQbDetail',
	// 				'type' => 'left',
	// 				'conditions'=> array('VoipPushNotification.user_id = UserQbDetail.user_id')
	// 			)
	// 		);
	// 	$options = array(
	// 			'conditions' => $conditions,
	// 			'joins' => $joins,
	// 			'fields' => array('UserQbDetail.qb_id'),
	// 		);
	// 	$id_data = $this->VoipPushNotification->find('all',$options);
	// 	$all_qb_ids = array();
	// 	foreach ($id_data AS  $ids) {
	// 		if($ids['UserQbDetail']['qb_id'] != "" && $ids['UserQbDetail']['qb_id'] != null){
	// 			$all_qb_ids[] = $ids['UserQbDetail']['qb_id'];
	// 		}
	// 	}

	// 	$userDetails['User']['id'] = 1;
	// 	$oneTimeToken = $this->genrateRandomToken($userDetails['User']['id']);
	// 	$oneTimeTokenData = array("user_id"=> $userDetails['User']['id'], "token"=> $oneTimeToken);
	// 	$this->UserOneTimeToken->saveAll($oneTimeTokenData);

	// 	$email = "ehteram@mediccreations.com";
	// 	$tokenDetails = $this->Quickblox->quickLogin($email, $oneTimeToken);
	// 	echo "<pre>";print_r($tokenDetails);exit();
	// 	if(isset($tokenDetails->session)){
	// 		$token = $tokenDetails->session->token;
	// 		$message = "Test";
	// 		$params['qbtoken'] = $token;
	// 	    $params['recipients'] = array( "ids"=> $all_qb_ids);
	// 	    $params['notificationMessage'] = $message;
	// 	    $sendNotification = $this->sendNotification($params);
	// 	    $sendNotificationArr = json_decode(json_encode($sendNotification));
	// 	    echo "<pre>";print_r($sendNotificationArr);exit();

	// 	}else{
 //            $responseData['status'] = 0;
 //            $responseData['message'] = 'We are facing some technical issue.';
	// 		echo json_encode($responseData);
	// 	}
	// 	echo "<pre>";print_r($all_qb_ids);exit();
		
	// }

	// public function sendNotification($params = array()){
	//   $messageArr = array(
	//                         "message"=> $params['notificationMessage'],
	//                         "aps"=> array("alert"=> $params['notificationMessage'], "sound"=> "notification_sound.mp3"),
	//                         "is_broadcast"=> 1,
	//                         "show_alert"=>1,
	//                         "tap_to_reply" => 0,
	//                     );
	//   $messageEncoded = base64_encode(json_encode($messageArr));
	//   $postData=json_encode(array("event"=> array("notification_type"=> "push", "environment"=> (ENVIRONMENT == "development"), "user"=> $params['recipients'], "message"=> $messageEncoded)));
	//   $curl = curl_init();
	//    curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'events.json');
	//         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
	//         curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
	//         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	//         curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	//         curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	//                 'Content-Type: application/json',
	//                 'QuickBlox-REST-API-Version: 0.1.0',
	//                 'QB-Token: ' . $params['qbtoken']
	//             ));
	//     $response = curl_exec($curl);
	//                 //echo "<pre>"; print_r($response);die;
	//                 if (!empty($response)) {
	//                         return $response;
	//                 } else {
	//                         return false;
	//                 }
	//                 curl_close($curl);
	//     return $response;
	// }

	public function genrateRandomToken($userId=NULL)
	{
		if(!empty($userId))
		{
			$userInfo = $userId.rand(1000,100000);
			$options = [
			    'cost' => 4,
			];
			$token = md5(password_hash($userInfo, PASSWORD_BCRYPT, $options));
			// $token = md5(crypt($userInfo));	
		}
		return $token;
	}

	public function revokeRoleExchangeAction()
    {
        $expiredUserDetails = $this->UserBatonRole->getUserForExpiration();
        if (!empty($expiredUserDetails)) {
            foreach ($expiredUserDetails as $userDetail) {
                $params['from_user'] = $userDetail['UserBatonRole']['from_user'];
                $params['to_user'] = $userDetail['UserBatonRole']['to_user'];
				$params['role_id'] = $userDetail['UserBatonRole']['role_id'];
				$params['first_name'] = '';
                $params['last_name'] = '';
                switch ($userDetail['UserBatonRole']['status']) {
                    case 3:
                        // $this->UserBatonRole->deleteAll(array("UserBatonRole.id" => $userDetail['id']), false);
                        $this->UserBatonRole->updateAll(array("UserBatonRole.is_active" => 0), array("UserBatonRole.from_user" => $userDetail['UserBatonRole']['from_user'],
                        "UserBatonRole.to_user" => $userDetail['UserBatonRole']['to_user'], "UserBatonRole.role_id" => $userDetail['UserBatonRole']['role_id']));
                        $this->UserBatonRole->updateAll(array("UserBatonRole.status" => 1), array("UserBatonRole.from_user" => $userDetail['UserBatonRole']['to_user'],
                        "UserBatonRole.to_user" => $userDetail['UserBatonRole']['from_user'], "UserBatonRole.role_id" => $userDetail['UserBatonRole']['role_id']));
                        $params['request_type'] = 'timeout';
                        $params['user_id'] = $userDetail['UserBatonRole']['from_user'];
                        $sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
                        $params['user_id'] = $userDetail['UserBatonRole']['to_user'];
                        $sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
                        //Release Role when request timout
                        if($params['to_user'] == 0){
							$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$params['role_id']));
						}
                        break;
                    case 5:
                        $this->UserBatonRole->updateAll(array("UserBatonRole.is_active" => 0), array("UserBatonRole.from_user" => $userDetail['UserBatonRole']['to_user'],
                        "UserBatonRole.to_user" => $userDetail['UserBatonRole']['from_user'], "UserBatonRole.role_id" => $userDetail['UserBatonRole']['role_id']));
                        $this->UserBatonRole->updateAll(array("UserBatonRole.status" => 1), array("UserBatonRole.from_user" => $userDetail['UserBatonRole']['from_user'],
                        "UserBatonRole.to_user" => $userDetail['UserBatonRole']['to_user'], "UserBatonRole.role_id" => $userDetail['UserBatonRole']['role_id']));
                        $params['request_type'] = 'timeout';
                        $params['user_id'] = $userDetail['UserBatonRole']['from_user'];
                        $sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
                        $params['user_id'] = $userDetail['UserBatonRole']['to_user'];
                        $sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
                        if($params['from_user'] == 0){
							$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$params['role_id']));
						}
						break;
                }
            }
        }
        echo "Done";exit();
    }


    public function updateExpiredUserBatonRole()
    {
    	$getUserBatonDetails = $this->UserBatonRole->find('all',array('conditions'=>array('from_user'=>588)));
    	foreach($getUserBatonDetails AS $getUseDetails)
    	{
    		$checkInstituteSubscription = $this->UserSubscriptionLog->find('first',array('conditions'=>array('user_id'=>$getUseDetails['UserBatonRole']['from_user'])));
    		$this->UserBatonRole->updateAll(array("status"=> -1, "is_active" =>0), array('from_user'=>588));
    		$this->UserBatonRole->updateAll(array("status"=> -1, "is_active" =>0), array('to_user'=>588));
    	}

    	echo "Done";exit();

    }

}
