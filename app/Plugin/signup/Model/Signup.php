<?php 
/*
Add, update, List user interests.
*/
App::uses('AppModel', 'Model');

class Signup extends SignupAppModel {
	
	/*
    ________________________________________________
    Method Name:Main=>loginCurl
    Purpose:
    Detail: 
    created by :Developer
    Created date:4-7-16
    Params:Via post
    _________________________________________________
    */
    function loginCurl($url, $data){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $headers = array();
            //$headers[] = 'access_key: KgGcxuClGLh6wSLlXi82KSotz6viIgohR/eyRnMn/mfKCcgK04obx9JU67+wH+to';
            $headers[] = 'access_key: '.ACCESS_KEY;
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Accept: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $server_output = curl_exec ($ch);

            return $server_output;
            curl_close ($ch);
    }
}
?>