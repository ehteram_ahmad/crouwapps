<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Medic Bleep App</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL The OCR STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <?php
            echo $this->Html->css(array(
                'Signup.font-awesome.min',
                'Signup.simple-line-icons.min',
                'Signup.bootstrap.min',
                'Signup.uniform.default',
                'Signup.bootstrap-switch.min',
                'Signup.select2.min',
                'Signup.select2-bootstrap.min',
                'Signup.dropzone.min',
                'Signup.basic.min',
                'Signup.components',
                'Signup.plugins.min',
                'Signup.login-5',
                ));

            ?>
        <!-- END PAGE LEVEL STYLES -->
        
        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN : LOGIN PAGE 5-2 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 login-container bs-reset">
                    <img onclick="location.href='<?php echo BASE_URL; ?>';" class="login-logo login-6" src="<?php echo BASE_URL.'signup/images/logoBig.svg'; ?>" alt="" />
                    <div class="login-content">
                        <h1 id="nameTxt">Thank you <?php echo $name; ?></h1>
						<p>You are almost there!</p>
						<p>We take security of our members very seriously and it's important for us ensure that we provide only the best possible experience.</p>
						<p>Please allow us some time to verify your credentials. A member of our team will be in touch with you shortly.</p>
                   		<div class="row">
							<div class="col-sm-6 col-md-6 col-xs-12 marAuto">
								<button class="btn blue marBottom30px btManual" type="button">SIGN IN</button>
							</div>
						</div>
                    </div>
                </div>
                <div class="col-md-6 bs-reset">
                    <div class="login-bg2">
                    	<!--<img src="images/welcome.svg" class="welcomeImg">-->
                    	<div class="step1">
							<div class="callOut top31 right19 flipH"><p class="flipH">thank <br>you</p></div>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : LOGIN PAGE 5-2 -->
        <!--[if lt IE 9]>
			<script src="js/respond.min.js"></script>
			<script src="js/excanvas.min.js"></script> 
		<![endif]-->
        <?php 
                echo $this->Html->script(array(
                    'Signup.jquery.min',
                    'Signup.bootstrap.min',
                    'Signup.js.cookie.min',
                    'Signup.bootstrap-hover-dropdown.min',
                    'Signup.jquery.slimscroll.min',
                    'Signup.jquery.blockui.min',
                    'Signup.jquery.uniform.min',
                    'Signup.bootstrap-switch.min',
                    'Signup.jquery.validate',
                    'Signup.additional-methods.min',
                    'Signup.select2.full.min',
                    'Signup.jquery.backstretch',
                    'Signup.dropzone',
                    'Signup.form-dropzone.min',
                    'Signup.app',
                    'Signup.login-5',
                    'Signup.jquery.uploadfile',
                    'Signup.randomColor',
                    'Signup.material-avatar',
                    'Signup.mbSignup',
                    ));
                ?>
		
    </body>

</html>