      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Doctors & Physicians',     11],
          ['Dentists',      2],
          ['Nurses',  2],
          ['Pharmacists', 2],
		  ['Students',  2],
          ['Institutions', 2],
          ['Others',    7]
        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }