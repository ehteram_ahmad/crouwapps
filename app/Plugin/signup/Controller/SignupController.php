<?php
// -----------------
define('ACCESS_KEY','HIMDTrD5DvajrHc3/2lCA5ugNfGmL4R0l8A4ltlDqUXw1bu7Cpt5ZKBsYI5xYnJO');
define('DEVICE_ID','');
define('DEVICE_TYPE','OCRWEB');
define('APPLICATION_TYPE','OCR');

// BASE path
define('OCR_PATH','https://theoncallroom.com/');//Live
//define('OCR_PATH','http://52.24.50.79/ocr1/ocr-web/');// DEV
//define('OCR_PATH','http://52.24.50.79/ocr1freez/ocr-web/');//QA
// define('OCR_PATH','http://52.35.228.67/mb-ftp/qa/web-chat/dist/');//Test
// define('OCR_PATH','http://localhost/qa/web-chat/dist/');//Test
// API PATHs
define('OCR_EXISTEMAIL','userwebservices/preExistEmail');
define('OCR_PROFESSION','userwebservices/allProfessions');
define('OCR_GMC','gmcuserwebservices/gmcUsersSearch');
define('OCR_SIGNUP','userwebservices/userSignup');
// -----------------
class SignupController extends SignupAppController {
	public $uses = array('User','UserSpecilitie','AdminUserSentMail','AdminUser','EmailTemplate');
	public $components = array('RequestHandler','Paginator','Session', 'Common');
	public $helpers = array('Js','Html', 'Paginator');

	private $encryptKey = 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd';
	private $ivKey = 'XX.s9GNvnP+zRA^Y'; 


	/*
	________________________________________________
	Method Name:Main=>OCR Signup
	Purpose:
	Detail: 
	created by :Developer
	Params:NA
	_________________________________________________
	*/


	public function index(){
		$this->layout=null;

	}

	public function thank($name){
		$this->layout=null;
		$this->set('name',$name);
	}


	public function curl($url,$data){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  //Post Fields
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$headers = array();
		$headers[] = 'access_key:KgGcxuClGLh6wSLlXi82KSotz6viIgohR/eyRnMn/mfKCcgK04obx9JU67+wH+to';
		// $headers[] = 'access_key: '.ACCESS_KEY;
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Accept: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$server_output = curl_exec ($ch);

		return $server_output;
		curl_close ($ch);
	}

	/*
	________________________________________________
	Method Name:Main=>checkMail
	Purpose:check pre-existing email
	Detail: 
	created by :Developer
	Created date:10-01-2017
	Params:email
	_________________________________________________
	*/ 
	public function checkMail(){
		$data=$this->data;
		$email=$data['email'];
	    $dataJson = json_encode(array('email'=>$email,'device_type'=>DEVICE_TYPE,'version'=>'' ) );
	    // Prepare the data
	    $postData = array(
	              'values'=>$this->encryptData($dataJson), 
	     ); 
	    $postData = json_encode($postData);
	    $url = OCR_PATH.OCR_EXISTEMAIL;
	    $value = $this->curl($url, $postData);
	    //echo "<pre>"; print_r($value);exit;
	    $content = json_decode($value,true);
	    $resultData = $this->decryptData($content['values']);
	    $result = json_decode($resultData,true);
	    // return $result;
	    $result=$result['data'];
	    $check['exist']=$result['email_exists'];
	    $check['preQualified']=$result['pre_qualified_email'];
	    $exist=0;
	    $approved=0;
	    if($check['exist']!=0){
	     	  	$exist=1;
	 	}
	 	if($check['preQualified']!=0){
	 		$approved=1;
	 	}
	     	  echo $exist."~".$approved;
	    // return $check;
	    exit();
	}
	/*
	________________________________________________
	Method Name:Main=>get Profession
	Purpose:check pre-existing email
	Detail: 
	created by :Developer
	Created date:10-01-2017
	Params:email
	_________________________________________________
	*/ 
	public function getProfessions(){
	    $dataJson = json_encode(array() );
	    // Prepare the data
	    $postData = array(
	              'values'=>$this->encryptData($dataJson), 
	     ); 
	    $postData = json_encode($postData); 
	    $url = OCR_PATH.OCR_PROFESSION;
	    $value = $this->curl($url, $postData);
	    $content = json_decode($value,true);
	    $resultData = $this->decryptData($content['values']);
	    $result = json_decode($resultData,true);
	    $result=$result['data'];
	    $result=$result['Profession'];
	    // return $result;
	    echo "<option value='0' selected disabled >PLEASE SELECT</option>";
	    foreach ($result as $key ) {
	    	echo "<option value=".$key['id']." >".$key['profession_type']."</option>";
	    }
	    exit();
	}

	/*
	________________________________________________
	Method Name:Main=>get GMC Data
	Purpose:get GMC Data
	Detail: 
	created by :Developer
	Created date:11-01-2017
	Params:email
	_________________________________________________
	*/ 
	public function getGmcData(){
		$prof=$this->data['profession'];
		$country=$this->data['country'];
		$fname=$this->data['fname'];
		$lname=$this->data['lname'];
	  if($prof==8 && $country=='GB'){
	    $dataJson = json_encode(array('profession_id'=>'8', 'first_name'=>$fname,'last_name'=>$lname,'country'=>'uk'));
	    // Prepare the data
	    $postData = array(
	              'values'=>$this->encryptData($dataJson), 
	     ); 
	    $postData = json_encode($postData);
	    $url = OCR_PATH.OCR_GMC;
	    $value = $this->curl($url, $postData);
	    $content = json_decode($value,true);
	    $resultData = $this->decryptData($content['values']);
	    $result = json_decode($resultData,true);
	    $result=$result['data'];
	    $result=$result['GmcUser'];
	  }else{
	    $result= "none";
	  }
	  if($result=="none" || empty($result)){
	  	echo "Sorry! <br>It looks like your profile is not a part of our records. Please manually register with us to proceed.";
	  }else{
	  	foreach ($result as $value) {
	  		echo "<li id=".$value['GMCRefNo']." onclick='gmcRegister(".$value['GMCRefNo'].");' ><div class='claimSingle'><div class='imgClaimSingle'><div class='imgPlaceholder'>".$value['GivenName']." ".$value['Surname']."</div></div><div class='nameClaimSingle'>".$value['GivenName']." ".$value['Surname']."</div><div class='numClaimSingle'>".$value['GMCRefNo']."</div><div class='countryClaimSingle'>".$value['PlaceofQualificationCountry']."</div></div><textarea class='hidden' id='gmcRef'>".$value['GMCRefNo']."</textarea><textarea class='hidden' id='gmcFname'>".$value['GivenName']."</textarea><textarea class='hidden' id='gmcLname'>".$value['Surname']."</textarea></li>";
	  	}
	  }
	    exit();
	}

	/*
	________________________________________________
	Method Name:Main=>GMC Signup
	Purpose:GMC Signup
	Detail: 
	created by :Developer
	Created date:11-01-2017
	Params:email
	_________________________________________________
	*/ 
	public function gmcSignup(){
		$email=$this->data['email'];
		$fname=$this->data['fname'];
		$lname=$this->data['lname'];
		$prof=$this->data['prof'];
		$refNo=$this->data['refNo'];
		$pswrd=$this->data['pswrd'];
		$countryCode=$this->data['countryCode'];
		$countryName=$this->data['countryName'];
		$county=$this->data['county'];

	    $dataJson = json_encode(array('email'=>$email,'password'=>$pswrd,'first_name'=>$fname,'last_name'=>$lname,'country_code'=>$countryCode,'country'=>$countryName,'county'=>$county,'profession_id'=>$prof,'gmcnumber'=>$refNo));
	    // Prepare the data
	    $postData = array(
	              'values'=>$this->encryptData($dataJson), 
	     ); 
	    $postData = json_encode($postData);
	    $url = OCR_PATH.OCR_SIGNUP;
	    $value = $this->curl($url, $postData);
	    $content = json_decode($value,true);
	    $resultData = $this->decryptData($content['values']);
	    $result = json_decode($resultData,true);
	    if($result['status']=='1'){
	    	    	echo "ok";
	    		}
	    exit();
	}

	/*
	________________________________________________
	Method Name:Main=>Mannual Signup
	Purpose:Mannual Signup
	Detail: 
	created by :Developer
	Created date:12-01-2017
	Params:email
	_________________________________________________
	*/ 
	public function mannualSignup(){
		$img=$this->data['img'];
		$ext=$this->data['ext'];
		$email=$this->data['email'];
		$fname=$this->data['fname'];
		$lname=$this->data['lname'];
		$prof=$this->data['prof'];
		$pswrd=$this->data['pswrd'];
		$countryCode=$this->data['countryCode'];
		$countryName=$this->data['countryName'];
		$county=$this->data['county'];

	    $dataJson = json_encode(array('email'=>$email,'password'=>$pswrd,'first_name'=>$fname,'last_name'=>$lname,'country_code'=>$countryCode,'country'=>$countryName,'county'=>$county,'profession_id'=>$prof,'registration_img'=>$img,'img_ext'=>$ext));
	    // Prepare the data
	    $postData = array(
	              'values'=>$this->encryptData($dataJson), 
	     ); 
	    $postData = json_encode($postData);
	    $url = OCR_PATH.OCR_SIGNUP;
	    $value = $this->curl($url, $postData);
	    
	    $content = json_decode($value,true);
	    $resultData = $this->decryptData($content['values']);
	    $result = json_decode($resultData,true);

	    // $result=$result['data'];
	    // $result=$result['GmcUser'];
	    // echo "<pre>"; print_r($result);
	    if($result['status']=='1'){
	    	echo "ok";
		}
	    exit();
	}

	/*
	________________________________________________
	Method Name:Main=>encryptData
	Purpose:Make encrypt the comming data
	Detail: 
	created by :Developer
	Created date:12-07-2016
	Params:$text = NULL
	_________________________________________________
	*/
	public function encryptData( $text = NULL ){
	        //** Get text with padding
	        $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
	        $pad = $size - (strlen($text) % $size);
	        $text = $text . str_repeat(chr($pad), $pad);
	        //** Get encrypted text
	        $encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $this->encryptKey, $text, MCRYPT_MODE_CBC, $this->ivKey ) );
	        return $encrypted;
	    } 
	/*
	________________________________________________
	Method Name:Main=>decryptData
	Purpose:Make decrypt the comming data
	Detail: 
	created by :Developer
	Created date:12-07-2016
	Params:$text = NULL
	_________________________________________________
	*/
	public function decryptData( $encrypted = NULL ){
	        //** Decrypting Data
	        $encrypted = base64_decode($encrypted);
	        $decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $this->encryptKey, $encrypted, MCRYPT_MODE_CBC, $this->ivKey );
	        //** Remove Padding
	        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
	        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
	        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
	        return $decrypted;
	    }
	/*
	________________________________________________
	Method Name:Main=>getRealUserIp
	Purpose:get the IP, country, country code 
	Detail: 
	created by :Developer
	Created date:12-07-2016
	Params:$text = NULL
	_________________________________________________
	*/     
	public function getRealUserIpCountry(){
	    $strIpCount = '';
	    $ip = '';
	    switch(true){
	      case (!empty($_SERVER['HTTP_X_REAL_IP'])) : $ip = $_SERVER['HTTP_X_REAL_IP'];
	      case (!empty($_SERVER['HTTP_CLIENT_IP'])) : $ip =  $_SERVER['HTTP_CLIENT_IP'];
	      case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : $ip =  $_SERVER['HTTP_X_FORWARDED_FOR'];
	      default : $ip = $_SERVER['REMOTE_ADDR'];
	    }
	    
	    //$ip='203.122.23.162';
	    if($_SERVER['HTTP_HOST']=='localhost'){ $ip='203.122.23.162'; }
	    return $ip;
	 }

	 public function getCountry(){
	 	$ipOfUser = $this->getRealUserIpCountry();
	 	 $allData=$this->getCountryDetailOfOpeningUser($ipOfUser);
	 	//$allData=$this->getCountryDetailOfOpeningUser('194.80.131.5');
	 	// echo $allData;
	 	echo $allData['country_code']."~".$allData['country_name']."~".$allData['region_name'];
	 }

	/*
	________________________________________________
	Method Name:Main=>getCountryDetailOfOpeningUser
	Purpose:get country code  
	Detail: 
	created by :Developer
	Created date:27-10-2016
	Params:$text = NULL
	_________________________________________________
	*/  
	public function getCountryDetailOfOpeningUser($ip=NULL){
	   
	    $json_url = "http://freegeoip.net/json/".$ip;
	    $json = file_get_contents($json_url);
	    $data = json_decode($json, TRUE);
	    echo $data['country_code']."~".$data['country_name']."~".$data['region_name'];
	    exit;
	}
	public function checkCountryOfOpeningUser(){
		$ipOfUser = $this->getRealUserIpCountry();
		$allData = $this->getCountryDetailOfOpeningUser($ipOfUser);
		// 72.229.28.185
		//$allData = $controller->getCountryDetailOfOpeningUser('72.229.28.185');
		if($allData['country_code']=='GB' || $allData['country_code']=='IN' || $allData['country_code']=='NP'){
			echo 1;
		}else{
			echo 0;
		}


	}

	public function upload(){
		$output_dir = WWW_ROOT . 'img/uploader_tmp/';

		if(isset($_FILES["myfile"]))
		{
			$ret = array();

			$error =$_FILES["myfile"]["error"];
		   {
		    
		    	if(!is_array($_FILES["myfile"]['name'])) //single file
		    	{
		       	 	$fileName = $_FILES["myfile"]["name"];
		       	 	move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $_FILES["myfile"]["name"]);
		       	 	 //echo "<br> Error: ".$_FILES["myfile"]["error"];
		       	 	 
			       	 	 $ret[$fileName]= $output_dir.$fileName;
		    	}
		    	else
		    	{
		    	    	$fileCount = count($_FILES["myfile"]['name']);
		    		  for($i=0; $i < $fileCount; $i++)
		    		  {
		    		  	$fileName = $_FILES["myfile"]["name"][$i];
			       	 	 $ret[$fileName]= $output_dir.$fileName;
		    		    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName );
		    		  }
		    	
		    	}
		    }
		    echo json_encode($ret);
		}
		exit;
	}


}
?>