<?php 
/*
 Represents model for comments by users for any Post(Article).
*/
App::uses('AppModel', 'Model');

class UserPostComment extends AppModel {
	public $name = 'UserPostComment';

	/*
	On: 21-08-2015
	I/P: $postId, $pageSize, $pageNum
	O/P: array of post comments
	Desc: Post comments according to size if size not provided default 15 comments
	*/
	public function postComments( $postId = NULL , $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 ){
		$postComment = array();
		if( !empty($postId) ){
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			$options =array(             
				'joins' =>
					array(
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfile',
							'type' => 'inner',
							'conditions'=> array('UserProfile.user_id = UserPostComment.user_id')
						),
						
					),
				'conditions'=> array('UserPostComment.status'=> 1, 'UserPostComment.user_post_id'=> $postId),
				'fields'=> array('UserProfile.id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, UserPostComment.id, UserPostComment.user_id, UserPostComment.user_post_id, UserPostComment.content, UserPostComment.created'), 
				'order'=> array('UserPostComment.id DESC'),
				'limit'=> $pageSize,
				'offset'=> $offsetVal
			);
			$postComment = $this->find('all', $options);
		}
		return $postComment;
	}
}
?>