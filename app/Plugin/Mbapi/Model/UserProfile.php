<?php 
/*
Add, update, Validate user profile.
*/
App::uses('AppModel', 'Model');

class UserProfile extends AppModel { 

/*public $validate = array(		
        'first_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message'  => 'First name is Required.'
            ),
			'characters' => array(
				'rule' => array('custom', '/^[a-z0-9. ]*$/i'),
				'message'  => 'Alphanumeric characters with spaces only'
			)
        ),
        'last_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message'  => 'Last name is Required.'
            ),
			'characters' => array(
				'rule' => array('custom', '/^[a-z0-9 ]*$/i'),
				'message'  => 'Alphanumeric characters with spaces only'
			)
        ),
    );*/

/*
    On: 06-08-2015
    I/P: $fields = array(), $conditions = array()
    O/P: true/false
    Desc: 
    */
    public function updateFields( $fields = array(), $conditions = array() ){
        $return = 0;
        if( !empty($fields) && !empty($conditions) ){
            $this->updateAll( $fields, $conditions );

            if( $this->getAffectedRows() ){
                $return = 1;
            }
        }
         return $return;
    }		
	
    public function getParticipentDetail($participantId)
    {
        $conditions = "UserProfile.user_id = $participantId";
        $userLists = $this->find('first', 
                            array(
                                'joins'=>
                                array(
                                    array(
                                        'table' => 'professions',
                                        'alias' => 'Professions',
                                        'type' => 'left',
                                        'conditions'=> array('UserProfile.profession_id = Professions.id')
                                    ),
                                ),
                                'conditions' => $conditions,
                                'fields' => array('UserProfile.first_name', 'UserProfile.last_name','UserProfile.role_status', 'Professions.profession_type')
                            )
                        );
        return  $userLists;
    }

    /*
    --------------------------------------------------------------
    On: 08-12-2017
    I/P: 
    O/P: 
    Desc: Fetches User Country.
    --------------------------------------------------------------
    */

    public function getUserCountry($userId)
    {
        if(!empty($userId)){
            $userCountry = $this->find("first", array("conditions"=> array("user_id"=> $userId), "order"=> array("id DESC")));
        }
        return $userCountry;
    }
}
?>