<?php
/*
Desc: API data get/post to Webservices.
*/

//App::uses('AppController', 'Controller');

class MbapiuserwebservicesController extends AppController {

	public $uses = array('Mbapi.User','Mbapi.UserProfile', 'Mbapi.GmcUkUser','Mbapi.Specilities','Mbapi.Profession', 'Mbapi.Country', 'Mbapi.UserSpecilities', 'Mbapi.UserInterest', 'Mbapi.UserFollow', 'Mbapi.EmailSmsLog', 'Mbapi.PreQualifiedDomain', 'Mbapi.UserColleague', 'Mbapi.UserNotificationSetting', 'EmailTemplate', 'Mbapi.InstituteName', 'Mbapi.CompanyName', 'Mbapi.EducationDegree', 'Mbapi.Designation', 'Mbapi.UserInstitution', 'Mbapi.UserEmployment', 'Mbapi.NotificationUser', 'Mbapi.InstitutionInformation', 'Mbapi.MedicBleepAppUrl', 'Mbapi.ActivityLog', 'UserQbDetail', 'Mbapi.TempRegistration', 'NpiUsaUser','Mbapi.UserVisibilitySetting', 'Mbapi.UserDutyLog','Mbapi.CipOnOff', 'Mbapi.ForceLogoutSetting', 'Mbapi.ForgotPasswordLinkExpire', 'Mbapi.UserOneTimeToken', 'Mbapi.EnterpriseUserList', 'Mbapi.UserSubscriptionLog', 'Mbapi.ApiRequestResponseTrack', "Mbapi.PaidSubscriptionDomain", "Mbapi.AdminActivityLog", "Mbapi.CompanyGeofenceParameter","Mbapi.RoleTag","Mbapi.UserRoleTag","Mbapi.TourGuideCompletionVisit","Mbapi.OncallAccessProfessions","Mbapi.UserDevice","Mbapi.CacheLastModifiedUser","Mbapi.AvailableAndOncallTransaction","Mbapi.AppDeviceKey","Mbapi.AppServerKey", "Mbapi.LoginAttemptTransaction", "Mbapi.CompanyBranchName", "Mbapi.QrCodeDetail","Mbapi.InstituteSelectionTransaction", "Mbapi.VoipPushNotification","Mbapi.UserLoginTransaction","Mbapi.UserDeviceInfo",'Mbapi.NotificationBroadcastMessage','Mbapi.UserDndStatus','Mbapi.DndStatusOption','Mbapi.BatonRole','Mbapi.UserBatonRole','Mbapi.DepartmentsBatonRole');
	public $components = array('Common', 'Image', 'Mbapi.MbEmail','Quickblox','Cache');

	/*
	-------------------------------------------------------------------------------------
	On: 12-05-2016
	I/P: Optional {user_id, size, page_number}
	O/P: All Specilities.
	Desc: Displays all specilities JSON data.
	-------------------------------------------------------------------------------------
	*/
	public function allSpecilities(){
		$this->autoRender=false;
		$response = array();
		if($this->accesskeyCheck()){
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
			try{
				$params = array();
				$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
				$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
				$sortType = isset($dataInput['order_by']) ? $dataInput['order_by'] : '';
				$data = $this->Specilities->allSpecilities( $params, $sortType );
				if(!empty($data)){
					foreach($data as $dt){
						// ** Get user Interest and Specilities
						if( isset($dataInput['user_id']) && !empty($dataInput['user_id']) ){
							if( isset($dataInput['type']) && $dataInput['type'] == 'interest' ){
								$isSelected = 0;
								if($this->UserInterest->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "interest_id"=> $dt['Specilities']['id'], "status"=> 1)))>0){
									$isSelected = 1;
								}
								$specData[] = array('id'=> $dt['Specilities']['id'], 'name'=> $dt['Specilities']['name'], 'img_path'=> SPECILITY_ICON_PATH.$dt['Specilities']['img_name'], "is_selected"=> (int) $isSelected);
							}
							elseif( isset($dataInput['type']) && $dataInput['type'] == 'specilities' ){
								$isSelected = 0;
								if($this->UserSpecilities->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "specilities_id"=> $dt['Specilities']['id'], "status"=> 1)))>0){
									$isSelected = 1;
								}
								$specData[] = array('id'=> $dt['Specilities']['id'], 'name'=> $dt['Specilities']['name'], 'img_path'=> SPECILITY_ICON_PATH.$dt['Specilities']['img_name'], "is_selected"=> (int) $isSelected);
							}else{
								$specData[] = array('id'=> $dt['Specilities']['id'], 'name'=> $dt['Specilities']['name'], 'img_path'=> SPECILITY_ICON_PATH.$dt['Specilities']['img_name']);
							}
						}else{
							$specData[] = array('id'=> $dt['Specilities']['id'], 'name'=> $dt['Specilities']['name'], 'img_path'=> SPECILITY_ICON_PATH.$dt['Specilities']['img_name']);
						}
					}
					$response = array('method_name'=> 'allSpecilities', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('Specilities'=> $specData));
				}else{
					$response = array('method_name'=> 'allSpecilities', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}catch(Exception $e){
				$response = array('method_name'=> 'allSpecilities', 'status'=>"0", 'response_code'=> '615', 'message'=> ERROR_615);
			}
		}else{
			// $getUserStatusResponse = $this->getUserStatus($dataInput['user_id'], $dataInput['user_id']);
			// $response = array('method_name'=> 'allSpecilities', 'status'=>"0", 'response_code'=> $getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
			$response = array('method_name'=> 'allSpecilities', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);
		}
		//echo json_encode($response);
		$encryptedData = $this->Common->encryptData(json_encode($response));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 28-07-2015
	I/P: None
	O/P: All Professions
	-------------------------------------------------------------------------------------
	*/
	public function allProfessions(){
		$this->autoRender = false;
			$response = array();
			if($this->accesskeyCheck()){
			$response = array();
				try{
						$params = array();
						$data = $this->Profession->allProfessionss();
						if(!empty($data)){
							foreach($data as $dt){
								$specData[] = array('id'=> $dt['Profession']['id'], 'profession_type'=> $dt['Profession']['profession_type']);
							}
							$response = array('method_name'=> 'allProfessions', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('Profession'=> $specData));
						}else{
							$response = array('method_name'=> 'allProfessions', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
						}
					}catch(Exception $e){
						$response = array('method_name'=> 'allProfessions', 'status'=>"0", 'response_code'=> '615', 'message'=> ERROR_615);
					}
			}else{
				$response = array('method_name'=> 'allProfessions', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);
			}
			//echo json_encode($response);
			$encryptedData = $this->Common->encryptData(json_encode($response));
			echo json_encode(array("values"=> $encryptedData));
			exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 04-08-2015
	I/P: Json post
	O/P: json response
	Desc: Response of user signup
	-------------------------------------------------------------------------------------
	*/
	public function userSignup(){ 
		$this->autoRender = false;
		$title = SUCCESS_612;
		$messageVal = SUCCESS_615;
		$responseUserSignup = array();
		if($this->request->is('post')) { 
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if($this->validateAccessKey()){// User Details
				if(!empty($dataInput['country']) ){
				 if(!empty($dataInput['profession_id']) ){
				 	//$dataInput['email'] = addslashes($dataInput['email']);
					$this->request->data['User']['email'] = isset($dataInput['email']) ? $dataInput['email'] : '';
					$this->request->data['User']['password'] = isset($dataInput['password'])?md5($dataInput['password']):'';
					$this->request->data['User']['registration_date'] = date('Y-m-d H:i:s');
					$this->request->data['User']['activation_key'] = strtotime(date('Y-m-d H:i:s')) . rand(1000,100000);
					$this->request->data['User']['registration_source'] = "MB"; 
					// User Profile Details
					//$dataInput['first_name'] = addslashes($dataInput['first_name']); 
					$this->request->data['UserProfile']['first_name'] = isset($dataInput['first_name'])?$dataInput['first_name']:'';
					//$dataInput['last_name'] = addslashes($dataInput['last_name']);
					$this->request->data['UserProfile']['last_name'] = isset($dataInput['last_name'])?$dataInput['last_name']:'';
					$this->request->data['UserProfile']['profession_id'] = isset($dataInput['profession_id'])?$dataInput['profession_id']:'';
					$this->request->data['UserProfile']['gender'] = isset($dataInput['gender'])?$dataInput['gender']:'M';
					$this->request->data['UserProfile']['county'] = isset($dataInput['county'])?$dataInput['county']:'';
					//** Get country id
					$countryData = array();
					$countryCode = isset($dataInput['country_code'])?$dataInput['country_code']:'';
					$countryName = isset($dataInput['country'])?$dataInput['country']:'';
						$countryData = $this->Country->countryDetails( array('id'), array('country_code'=> strtoupper($countryCode)));
						if(empty($countryData)){
							$countryData = $this->Country->countryDetails( array('id'), array('country_name'=> ucwords($countryName)));
						}
						if( !empty($countryData) ){
							$countryId = $countryData[0]['Country']['id'];
						}else{
							$this->Country->save(array('country_name'=> ucwords($dataInput['country']), "country_code"=> strtoupper($countryCode)));
							$countryId = $this->Country->getLastInsertId();
						}
						$this->request->data['UserProfile']['country_id'] = $countryId;	
					//** Check GMC Number
					if(isset($dataInput['gmcnumber']) && !empty($dataInput['gmcnumber'])){
						$this->request->data['UserProfile']['gmcnumber'] = $dataInput['gmcnumber'];
						//** Set auto active so can be send admin approval mail directely
						$autoActive = 1;
						$this->request->data['User']['approved'] = 1;
						//** Update GMC User table as registered
						try{	
							if(in_array(strtolower($dataInput['country']), array('united kingdom','uk','gb'))){ //** If user is from uk check uk GMC	
								$this->GmcUkUser->updateFields( array('is_registered'=> 1), array('GMCRefNo'=> $dataInput['gmcnumber']));
							}elseif(in_array(strtolower($dataInput['country']), array('united states','us','usa'))){
								$this->NpiUsaUser->useDbConfig = 'gmc';
								$this->NpiUsaUser->updateFields( array('is_registered'=> 1), array('npi'=> $dataInput['gmcnumber']));
							}
						}
						catch( Exception $e){
						}
					}	
					//** Check if user register with pre approved domain email	
					if(isset($dataInput['email']) && !empty($dataInput['email'])){
						$domainNameArr = explode("@", $dataInput['email']);//** Extract Domain name from email
						$domainName = end($domainNameArr);	//** Domain Name
						if($domainName == 'wsh.nhs.uk' || $domainName == 'mediccreations.com')
						{
							$autoActiveAccount = 1;
							$title = SUCCESS_613;
							$messageVal = SUCCESS_604;
							$this->request->data['User']['status'] = 1;
							$this->request->data['User']['approved'] = 1;
						}
						$rootDomain = $this->Common->getTld($domainName);
						$preQualifiedEmail = $this->PreQualifiedDomain->find("count", array("conditions"=> array("domain_name"=> trim($rootDomain), "status"=>1)));
						if($preQualifiedEmail > 0){
							//** Set auto active so can be send admin approval mail directely
							$autoActive = 1;
							$this->request->data['User']['approved'] = 1;

							if($domainName == 'wsh.nhs.uk' || $domainName == 'mediccreations.com')
							{
								$autoActiveAccount = 1;
								$title = SUCCESS_613;
								$messageVal = SUCCESS_604;
								$this->request->data['User']['status'] = 1;
								$this->request->data['User']['approved'] = 1;
							}
						}
					}	


					//Added On 17 Dec 2108[START]
					$checkEnterpriseUser = $this->EnterpriseUserList->find("first", array("conditions"=> array("email"=> $dataInput['email'])));
					if(!empty($checkEnterpriseUser))
					{
						$autoActiveAccount = 1;
						$messageVal = SUCCESS_604;
						$title = SUCCESS_613;
					}
					//Added On 17 Dec 2108[END]
					if($this->User->checkValueExists('email', $this->request->data['User']['email'])){ //** Check email already exists
						$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "609", 'message'=> ERROR_609);
					}

					//** check password policy is matched
					// $uppercase = preg_match('@[A-Z]@', $dataInput['password']);
					// $lowercase = preg_match('@[a-z]@', $dataInput['password']);
					// $number    = preg_match('@[0-9]@', $dataInput['password']);
					// $specialChars = preg_match('@[^\w]@', $dataInput['password']);

					// if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($dataInput['password']) < 8){
					// 	$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "661", 'message'=> ERROR_661);
					// }

					else{
							$this->User->set($this->request->data);
							if($this->User->saveAll($this->request->data, array('validate'=>'only'))) { 
								try{	
										$this->User->saveAll($this->request->data); //** Save Data
										$userId = $this->User->getLastInsertID();
											// Send activation link to email id
											if($autoActive){
												//$params['activationLink'] = BASE_URL.'Index/verifyMailPreApproved/'. $this->request->data['User']['activation_key'] . '/' . base64_encode($userId);
												$params['activationLink'] = MEDICBLEEP_BASE_URL . 'Index/verifyMailPreApproved.php?activation_key='. $this->request->data['User']['activation_key'] . '&user_id=' . base64_encode($userId);
											}else{
												//$params['activationLink'] = BASE_URL.'Index/verifyMail/'. $this->request->data['User']['activation_key'] . '/' . base64_encode($userId);
												$params['activationLink'] = MEDICBLEEP_BASE_URL . 'Index/verifyMail.php?activation_key='. $this->request->data['User']['activation_key'] . '&user_id=' . base64_encode($userId);
											}
											$params['toMail'] = $this->request->data['User']['email'];
											$params['from'] = NO_REPLY_EMAIL;
											$params['name'] = $this->request->data['UserProfile']['first_name'];
											$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'MedicBleep Email Verify')));
											$params['temp'] = $templateContent['EmailTemplate']['content'];
											
											try{
												if($autoActiveAccount)
												{

												}
												else
												{
													$mailSend = $this->MbEmail->verifyEmail( $params );
												}
											}catch(Exception $e){
												$mailSend = $e->getMessage();
											}
											if($autoActiveAccount)
											{

											}
											else
											{
												$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "signup") );
											}


											// try{
											// 	$mailSend = $this->MbEmail->verifyEmail( $params );
											// }catch(Exception $e){
											// 	$mailSend = $e->getMessage();
											// }
											// $this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "signup") );
										//** Registration Image (Like DL, Passport image upload)
										if(isset($dataInput['registration_img']) && !empty($dataInput['registration_img']) ){ //** Registration Image
											//** Create Image from string and move to amazon
											try{
												$regImgName = $this->Image->createimage($dataInput['registration_img'],WWW_ROOT . 'img/uploader_tmp/', md5( 'certificate_'.$userId ), $dataInput['img_ext']);
												if( !empty($regImgName) ){
													// $this->Image->moveImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$regImgName, 'img_name'=> $userId.'/certificate/'.md5( 'certificate_'.$userId ).'.'.$dataInput['img_ext']) );

													$this->Image->pushImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$regImgName, 'img_name'=> $userId.'/certificate/'.md5( 'certificate_'.$userId ).'.'.$dataInput['img_ext']), $contentType = 'image/png',  $imageName);
													
													$registrationImgName = $regImgName;
												}else{
													$registrationImgName = '';
												}
											}catch(Exception $e){
												$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
											}
											//** Update Registration image
											$this->UserProfile->updateFields( array('registration_image'=> "'".$registrationImgName."'"), array('user_id'=> $userId) );
											//** Remove physiacal file from Temp Directory
											if( file_exists( WWW_ROOT . 'img/uploader_tmp/'.$regImgName )){
												chmod(WWW_ROOT . 'img/uploader_tmp/'.$regImgName, 777);
												unlink( WWW_ROOT . 'img/uploader_tmp/'.$regImgName );
											}
										}
									//// add data in Quickblox database [start]
									try{
									 	$this->request->data['User']['email'] = stripslashes($this->request->data['User']['email']);
										$securePass = $this->Common->randomPassword();
										$details = $this->Quickblox->quickAdduserFromOcr(stripslashes($this->request->data['User']['email']), $securePass, stripslashes($this->request->data['User']['email']),$userId,stripslashes($this->request->data['UserProfile']['first_name']).' '.stripslashes($this->request->data['UserProfile']['last_name']));
										//echo "<pre>";print_r($details);exit();

										$detailsArr = json_decode($details, true);
										if(!empty($detailsArr) && is_array($detailsArr)){
											$this->addUserQBid($detailsArr, $userId);
										}
										//** Set profession as status on QB[START]
										$oneTimeToken = $this->genrateRandomToken($userId);
										$oneTimeTokenData = array("user_id"=>$userId, "token"=> $oneTimeToken);
										$this->UserOneTimeToken->saveAll($oneTimeTokenData);
											$professionData = $this->Profession->findById($this->request->data['UserProfile']['profession_id']);
											$profession = !empty($professionData['Profession']['profession_type']) ? $professionData['Profession']['profession_type'] : "";
											$userCustomQbData = array("userEmail"=> $this->request->data['User']['email'], "userPassword"=> $oneTimeToken, "userRoleStatus"=> $profession);
											$customDataQb = $this->Quickblox->updateCustomData($userCustomQbData);
										//** Set profession as status on QB[END] 
									}catch(Exception $e){ $details = $e->getMessage(); }
									
									//// add data in Quickblox database [end]

									//** check email associated with partner institution [start]
									$domainNameArr = explode("@", $dataInput['email']);
									$domainName = end($domainNameArr);
									 $preAssignedEmail = $this->PaidSubscriptionDomain->find("first", array("conditions"=> array("domain_name"=> trim($domainName), "status"=>1)));
									 $checkEnterpriseUser = $this->EnterpriseUserList->find("first", array("conditions"=> array("email"=> $dataInput['email'], "status"=>1)));
									 $approvedCompanyId = 0;
									 if(isset($preAssignedEmail['PaidSubscriptionDomain'])){
									 	$approvedCompanyId=$preAssignedEmail['PaidSubscriptionDomain']['company_id'];
									 }elseif(isset($checkEnterpriseUser['EnterpriseUserList'])){
									 	$approvedCompanyId= $checkEnterpriseUser['EnterpriseUserList']['company_id'];
									 }
									 	//** get company subscription details
									 	if(!empty($approvedCompanyId)){
									 	$companySubscriptionData=$this->CompanyName->find("first", array("conditions"=> array("id"=>$approvedCompanyId, "status"=>1,"is_subscribed"=>1)));
									 	if(isset($companySubscriptionData['CompanyName'])){
									 		$subscriptionDate=$companySubscriptionData['CompanyName']['subscription_date'];
									 		$subscriptionExpiryDate=$companySubscriptionData['CompanyName']['subscription_expiry_date'];

									 		//** Set auto company_id
									 		$this->UserEmployment->saveAll(array('user_id'=>$userId,'company_id'=>$approvedCompanyId,'is_current'=>1,'status'=>1));

											//** Set email in user enterprise list
											$checkEnterpriseUser = $this->EnterpriseUserList->find("count", array("conditions"=> array('email'=>$this->request->data['User']['email'],'company_id'=>$approvedCompanyId)));
											if($checkEnterpriseUser == 0){
									 			$this->EnterpriseUserList->saveAll(array('email'=>$this->request->data['User']['email'],'company_id'=>$approvedCompanyId,'status'=>1));
									 		}else{
									 			$this->EnterpriseUserList->updateAll(array("status"=> 1), array('email'=>$this->request->data['User']['email'],'company_id'=>$approvedCompanyId));

									 			//Added on 17 Dec 2018
									 			$this->User->updateAll(array("status"=> 1, "approved"=>1), array('email'=>$this->request->data['User']['email']));
									 			
									 		}
									 		//** Set subscription expiry date in subscription log
									 		$this->UserSubscriptionLog->saveAll(array('user_id'=>$userId,'company_id'=>$approvedCompanyId,'subscription_date'=>$subscriptionDate,'subscription_expiry_date'=>$subscriptionExpiryDate,'subscribe_type'=>1));
									 	}
									 }
									 //}
									 
									//** check email associated with partner institution [end]

									//** add default on duty/at work[START]
									$dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userId)));
									if(!empty($dutyLogData)){
											$this->UserDutyLog->updateAll(array("hospital_id"=>$approvedCompanyId, "atwork_status"=> 0, "status"=> 0), array("user_id"=> $userId));
									}else{
										$dutyData = array("hospital_id"=>$approvedCompanyId, "atwork_status"=> 0, "status"=> 0, "user_id"=> $userId);
										$this->UserDutyLog->save($dutyData);
									}
									//** add default on duty/at work[END]

									$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"1", 'response_code'=> "200", 'message'=> $messageVal, 'title'=>$title);
									//** By default follw user 'The On Call Doctor'
									try{
										$email = "ocr@theoncallroom.com";
										$userDetails = $this->User->find("first", array("conditions"=> array("email"=> $email)));
										$followedToUserId = $userDetails['User']['id'];
										$followData = array("followed_by"=> $userId, "followed_to"=> $followedToUserId, "follow_type"=> 1, "status"=> 1);
										$this->UserFollow->save( $followData );
									}catch(Exception $e){}
								}catch(Exception $e){
									$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
								}
							}else{
									/*$errors = $this->User->validationErrors;
									if( !empty($errors) ){
										$errorMsg = "All field(s) are blank.";	
									}
									$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "615", 'message'=> $errorMsg);	
								*/
							}		
					}
					}else{
						$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "634", 'message'=> ERROR_634);
					}
				}else{
					$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "634", 'message'=> ERROR_639);
				}
				//******* Delete Cache on user signup[START] ********//
				// $userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $userId, "is_current"=> 1), "order"=> array("id DESC")));
				// $cacObj = $this->Cache->redisConn();
				// if(($cacObj['connection']) && empty($cacObj['errors'])){
				// 	if($cacObj['robj']->exists('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id']))
				// 	{
				// 		$key = 'institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'];
				// 		$cacObj = $this->Cache->delRedisKey($key);
				// 	}
				// }
				//******* Delete Cache on user signup[END] ********//
			} else{
				$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}	
		}else{
			$responseUserSignup = array('method_name'=> 'userSignup', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseUserSignup);
		exit();
	}

	/*
	-------------------------------------------------------------------------------------
	On: 04-08-2015
	I/P: $activationKey
	O/P: Key activation Message
	-------------------------------------------------------------------------------------
	*/
	public function verifyMail( $activationKey = NULL , $encryptedUserId = NULL ){
		$msg = 'Wrong Key';
		if( !empty( $activationKey ) ){
			$activationStatus = $this->User->activationKeyStatus( $activationKey );
			if( !empty($activationStatus) ){
				if( $activationStatus['User']['approved'] == 1 ){
					$msg = 'Email Already Verified.';
				}else {
					$keyUpdate = $this->User->activateKey( $activationKey );
					if($keyUpdate){
						$msg = "Email Verified";
						//** Send welcome mail
						if( !empty($encryptedUserId) ){
							$userId = (int) base64_decode($encryptedUserId);
							try{
								$userDetails = $this->User->find('first', array('fields'=> array('User.id, User.email, UserProfile.first_name, UserProfile.last_name'), 'conditions'=> array('User.id'=> $userId)));
								//echo "<pre>";print_r($userDetails);die;
							}catch(Exception $e){
								//echo $e->getMessage();die;
							}
							$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Welcome')));
							$params['temp'] = $templateContent['EmailTemplate']['content'];
							$params['toMail'] = $userDetails['User']['email'];
							$params['from'] = NO_REPLY_EMAIL;
							$params['name'] = $userDetails['UserProfile']['first_name'] . ' ' . $userDetails['UserProfile']['last_name'];
							$params['regards'] = "Dr. Sandeep Bansal";
							$mailSend = $this->MbEmail->signupWelcomeEmail( $params );
							$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "Signup Welcome") );
						}
					}
				}
			}
		}
		echo $msg;
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 04-08-2015
	I/P: $userId, $password
	O/P: User profile JSON format
	Desc: User authenticate using userName and password. If success return user profile.
	-------------------------------------------------------------------------------------
	*/
	public function login(){
		$this->autoRender = false;
		$responseData = array();
		$number_of_attempts = 0;
		$attempt_time = 0;
		$updatePassword = 0;
		$reasonToupdatePassword = '';
		$userRemoteAddress = $_SERVER['REMOTE_ADDR'];
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			// echo "<pre>";print_r($dataInput);exit();
			// if($this->accesskeyCheck()){
			if($this->validateAccessKey()){
				if( !empty($dataInput['email']) && !empty($dataInput['password']) ){

					$uppercase = preg_match('@[A-Z]@', $dataInput['password']);
					$lowercase = preg_match('@[a-z]@', $dataInput['password']);
					$number    = preg_match('@[0-9]@', $dataInput['password']);
					$specialChars = preg_match('@[^\w]@', $dataInput['password']);

					if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($dataInput['password']) < 8) {
					  $updatePassword = 1;
					  $reasonToupdatePassword = MESSAGE_001;
					}

					// $reasonToupdatePassword = !empty(string($reasonToupdatePassword))?string($reasonToupdatePassword):string($reasonToupdatePassword);

					$dataInput['email'] = strtolower($dataInput['email']);
					$params = array('email'=> $dataInput['email']);
					$userDetails = $this->User->userDetails( $params );
					// echo "<pre>";print_r($userDetails);exit();

					$userLoginTransaction = $this->LoginAttemptTransaction->find("first", array("conditions"=> array("email"=> $dataInput['email'])));
					$number_of_attempts = !empty($userLoginTransaction['LoginAttemptTransaction']['attempt']) ? $userLoginTransaction['LoginAttemptTransaction']['attempt'] : 0;
					if(! empty($userLoginTransaction['LoginAttemptTransaction']['last_login']))
					{
						date_default_timezone_set('Asia/Kolkata');
						$last_login_time = strtotime($userLoginTransaction['LoginAttemptTransaction']['last_login']);
						$current_time = strtotime(date('Y-m-d H:i:s'));
						$difference = ($current_time - $last_login_time);
						$attempt_time = intval($difference/60);
					}

					if($attempt_time >= 10){
						if(!empty($userLoginTransaction)){
							$this->LoginAttemptTransaction->updateAll(array("attempt"=>0 , "status"=> 1, "last_login"=>'"'.date("Y-m-d H:i:s").'"'), array("email"=> $dataInput['email']));
							}else{
								$userLoginTransactionToInsert = array("email"=>$dataInput['email'], "attempt"=> $userLoginAttempt+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
								$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
							}
					}
					$userLoginTransaction = $this->LoginAttemptTransaction->find("first", array("conditions"=> array("email"=> $dataInput['email'])));
					$number_of_attempts = !empty($userLoginTransaction['LoginAttemptTransaction']['attempt']) ? $userLoginTransaction['LoginAttemptTransaction']['attempt'] : 0;
					if(! empty($userLoginTransaction['LoginAttemptTransaction']['last_login']))
					{
						date_default_timezone_set('Asia/Kolkata');
						$last_login_time = strtotime($userLoginTransaction['LoginAttemptTransaction']['last_login']);
						$current_time = strtotime(date('Y-m-d H:i:s'));
						$difference = ($current_time - $last_login_time);
						$attempt_time = intval($difference/60);
					}
					if($attempt_time <=10){
						if($number_of_attempts  <= 10){
							if(  $userDetails['User']['email'] == $dataInput['email'] && $userDetails['User']['password'] == md5($dataInput['password']) ){
								// echo "<pre>";print_r("Bye");exit();
								if( $userDetails['User']['status'] != 2){
									if( $userDetails['User']['status'] == 1 ){

										$dataUserDevice['device_type'] = isset($dataInput['device_type']) ? $dataInput['device_type']:'0';

										$admin_approval = array('is_admin_approved' => 1, 'message'=> '' );

										if($userDetails['User']['approved'] == 0 && $dataUserDevice['device_type'] == 'MBWEB'){
											$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "633", 'message'=> ERROR_633);

										}else{
											if($userDetails['User']['approved'] == 0){
												$admin_approval = array('is_admin_approved' => 0, 'message'=> ERROR_666 );
											}
											$userData = $this->userFields( $userDetails );
											$userData['admin_approval'] = $admin_approval;
											// Update Last Logged in Date
											$this->User->updateFields( array('User.last_loggedin_date'=> "'".date('Y-m-d H:i:s')."'"), array('User.id'=> $userDetails['User']['id']));
											// User Device data save
											$dataUserDevice['user_id'] = $userDetails['User']['id'];
											$dataUserDevice['device_id'] = isset($dataInput['device_id']) ? $dataInput['device_id']:'';
											$dataUserDevice['token'] = md5(strtotime(date('Y-m-d H:i:s')) . $userDetails['User']['id'] . rand(1000,100000));
											// Used to distinguish the application type Shashi 07-06-16
											$dataUserDevice['application_type'] = isset($dataInput['application_type']) ? $dataInput['application_type']:'MB';
											//** Logout from preveous devices[START]
											$this->logoutFromOtherDevices($dataUserDevice);
											//** Logout from preveous devices[END]
											//** Check User and device
											$userDeviceCheck = $this->UserDevice->find("count", array("conditions"=> array("user_id"=> $userDetails['User']['id'], "device_id"=> $dataInput['device_id'], 'application_type'=> 'MB')));
											//echo json_encode(array('value'=>$userDeviceCheck));exit();
											if($userDeviceCheck == 0){
												$this->UserDevice->save( $dataUserDevice );

												//** Inactive all other users device id
												$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataInput['device_id'], "user_id !="=> $userDetails['User']['id'], 'application_type'=> 'MB'));
											}else{
												$this->UserDevice->updateAll(array("status"=> 1), array("user_id"=> $userDetails['User']['id'], "device_id"=> $dataInput['device_id'], 'application_type'=> 'MB'));
												//** Inactive all other users device id
												$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataInput['device_id'], "user_id !="=> $userDetails['User']['id'], 'application_type'=> 'MB'));
											}
											// Get Token
											$tokenParams = array(
																'user_id'=>  $userDetails['User']['id'],
																'device_id'=> $dataInput['device_id'],
																'status'=> 1
																);
											$tokenData = $this->UserDevice->tokenDetails( $tokenParams );
											$userData['token'] = !empty($tokenData['UserDevice']['token']) ? $tokenData['UserDevice']['token'] : "";
											//** For user current company paid then add in enterprise list if not[START]
											$checkEnterprise = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']), "company_id"=> $userData['Company']['company_id'])));
											if($checkEnterprise==0){
												$this->EnterpriseUserList->save(array("email"=> stripslashes($userDetails['User']['email']), "company_id"=> $userData['Company']['company_id'], "status"=> 0));
											}
											//** For user current company paid then add in enterprise list if not[END]

											//** Get Settings Values like CIP ON/OFF, force logout values[END]
											//** Get user Enterprise data[START]
											$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userData['user_id'])));
											$dataParams['email'] = stripslashes($userDetails['User']['email']);
											$dataParams['company_id'] = $userData['Company']['company_id'];
											$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);

											$checkUserSubscribedExistingUser = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userData['user_id'])));
											//** For existing user prior to enterprise feature add enterprise data first time[START]
											if(empty($checkUserSubscribedExistingUser)){
												if(empty($checkUserForPaid)){
													$subscriptionStartDate = date('Y-m-d 23:59:59');
													$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_TRIAL_PERIOD.' days'));
												}else{
													$subscriptionStartDate = $checkUserForPaid['CompanyName']['subscription_date'];
													$subscriptionExpireDate = $checkUserForPaid['CompanyName']['subscription_expiry_date'];
												}
													$subscriptionDataVals = array("user_id"=> $userData['user_id'],"company_id"=> $userData['Company']['company_id'], "subscription_date"=> $subscriptionStartDate, "subscription_expiry_date"=> $subscriptionExpireDate);
													$this->UserSubscriptionLog->save($subscriptionDataVals);
											}
											//** For existing user prior to enterprise feature add enterprise data first time[END]
											$status = 0;$expiryDate = 0;
											$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
											//** When user comes after trust expiry date update grace period[START]
												$currentDate = date('Y-m-d 23:59:59');
												$gracePeriod = date('Y-m-d 23:59:59', strtotime($currentDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
												$trustExpiryDate = $checkUserSubscribed['UserSubscriptionLog']['subscription_expiry_date'];
												if(strtotime($currentDate) > strtotime($trustExpiryDate) && !empty($checkUserForPaid) && ($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] != 2)){
													$this->UserSubscriptionLog->updateAll(array("company_id"=> $userData['Company']['company_id'], "subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $userDetails['User']['id']));
												}elseif($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] == 1 && empty($checkUserForPaid)){
													$this->UserSubscriptionLog->updateAll(array("company_id"=> $userData['Company']['company_id'],"subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $userDetails['User']['id']));
												}
											//** When user comes after trust expiry date update grace period[END]
											//** Update user expiration[START]
											$checkUserSubscribedForexpire = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
											if(!empty($checkUserSubscribedForexpire) ){
												$paramData['subscription_expiry_date'] = $checkUserSubscribedForexpire['UserSubscriptionLog']['subscription_expiry_date'];//$checkUserForPaid['CompanyName']['subscription_expiry_date'];
												$isExpired = $this->UserSubscriptionLog->checkSusbcriptionExpired($paramData);
												if($isExpired){
													$this->UserSubscriptionLog->updateAll(array("company_id"=>$userData['Company']['company_id'], "subscribe_type"=> 3),array("user_id"=> $userDetails['User']['id']));
												}
											}
											//** Update user expiration[END]
											//** Get user SUBSCRIPTION status
											$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
											if(!empty($checkUserSubscribedFinal)){
												//if(!empty($checkUserForPaid)){ $status = 1; }
												if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
												$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
												if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
												//** Check if expired
												if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
											}
											//** In Case of grace period oncall should be off[START]
											if($status==2){
												$this->UserDutyLog->updateAll(array("status"=> 0), array("user_id"=> $userData['user_id'], "hospital_id"=> $userData['Company']['company_id']));
											}
											//** In Case of user expired update cache[START]

											if($status==3){
												$userDataParams['company_id'] = isset($userCurrentCompany['UserEmployment']['company_id']) ? $userCurrentCompany['UserEmployment']['company_id'] : 0;
												$userDataParams['user_id'] = isset($userData['user_id']) ? $userData['user_id']: 0;
												$updateStaticCache = $this->saveStaticData($userDataParams);
											}

											//** In Case of user expired update cache[END]
											//** In Case of grace period oncall should be off[END]
											$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate) > 0) ? strtotime($expiryDate):0);
											//** Get user Enterprise data[END]
											$userData['settings'] = array($subscriptionData);

											//** Get User Tour Guide Status
											$tourValue="0";
											$tourGuideData = $this->TourGuideCompletionVisit->find('count',array('conditions'=>array('user_id'=>$userData['user_id'],'device_type'=>$dataUserDevice['device_type'],'status'=>1)));
											if($tourGuideData > 0){
												$tourValue="1";
											}
											$userData['tourGuide'] = array("value"=>$tourValue);

											//** Get user role tags[START] // Discuss with Mohit, Uday
											$roleTagsArr = array(); $roleTagsVals = array();
											$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
											if(!empty($roleTags['Profession']['tags'])){
												$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
											}
											if(!empty($roleTagsVals)){
												foreach($roleTagsVals as $rt){
													$values = array(); $roleTagId = 0;
													$paramtrs['user_id'] = $userDetails['User']['id'];
													$paramtrs['key'] = $rt;
													$roleTagUser = $this->RoleTag->userRoleTags($paramtrs);
													if(!empty($roleTagUser)){
														$roleTagId = $roleTagUser[0]['rt']['id'];
														$values = array($roleTagUser[0]['rt']['value']);
													}
													$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
												}
											}
											$userData['role_tags'] = $roleTagsArr;
											$chatMessagedata = array("encryptionKey"=> 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd', "encryptionIv"=>'XX.s9GNvnP+zRA^Y');

											$updatePasswordPolicy = array("should_change_password"=> $updatePassword, "reason"=>$reasonToupdatePassword);
											//** Get user role tags[END]

											$responseData = array('method_name'=> 'login', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('User'=> $userData, 'MessageKey'=>$chatMessagedata, 'updatePasswordPolicy'=> $updatePasswordPolicy));

											//** Add custom data on QB [START]
											$atwork = "0"; $oncall = "0";
											$dutyData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userData['user_id'])));
											if(!empty($dutyData)){
												$atwork = $dutyData['UserDutyLog']['atwork_status'];
												$oncall = $dutyData['UserDutyLog']['status'];
											}
											try{
												//** One time token to validate for QB[START]
												$oneTimeToken = $this->genrateRandomToken($userDetails['User']['id']);
												$oneTimeTokenData = array("user_id"=> $userDetails['User']['id'], "token"=> $oneTimeToken);
												$this->UserOneTimeToken->save($oneTimeTokenData);
												//** One time token to validate for QB[END]
												$tokenDetails = $this->Quickblox->quickLogin($dataInput['email'], $oneTimeToken);
												$token = $tokenDetails->session->token;
												$user_id = $tokenDetails->session->user_id;
												$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
												$custom_data_from_api = json_decode($user_details->user->custom_data);
												$customData['userRoleStatus'] = $custom_data_from_api->status;
												if(empty($customData['userRoleStatus']))
												{
													$customData['userRoleStatus'] = $userData['Profession']['profession_type'];
												}
												$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
												$customData['isImport'] = "true";
												$customData['at_work'] = (string) $atwork;
												$customData['on_call'] = (string) $oncall;
												$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
											}catch(Exception $e){}
											//** Add custom data on QB [END]


											if(!empty($userLoginTransaction)){
											$this->LoginAttemptTransaction->updateAll(array("attempt"=>0 , "status"=> 1, "last_login"=>'"'.date("Y-m-d H:i:s").'"'), array("email"=> $dataInput['email']));
											}else{
												$userLoginTransactionToInsert = array("email"=>$dataInput['email'], "attempt"=> $number_of_attempts+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
												$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
											}
										}
									}else{
										$userInformation =  $this->AdminActivityLog->find('first', array('conditions' =>
				               				 array('AdminActivityLog.user_id' => $userDetails['User']['id']),'order'=>array('AdminActivityLog.id DESC')
				              				  ,'limit'=> 1));
										if($userInformation['AdminActivityLog']['message'] == "Deactivated"){
												$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
										}else{
												$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "605", 'message'=> ERROR_605);
										}
									}
								}else{
									$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
								}
							}else{
								if(!empty($userLoginTransaction)){
								$this->LoginAttemptTransaction->updateAll(array("attempt"=>$number_of_attempts+1 , "status"=> 1), array("email"=> $dataInput['email']));
								}else{
									$userLoginTransactionToInsert = array("email"=>$dataInput['email'], "attempt"=> $number_of_attempts+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
									$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
								}
								$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "604", 'message'=> ERROR_604, "noa"=>$number_of_attempts);
							}
						}else{
							$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "660", 'message'=> ERROR_660);
						}
					}else{
						$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "660", 'message'=> ERROR_660);
					}
				}else{
					$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "603", 'message'=> ERROR_603);
				}
			}else{
				$response = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 04-08-2015
	I/P:
	O/p:
	Desc:
	-------------------------------------------------------------------------------------
	*/
	public function userFields( $ud = array(), $userType = NULL ){
		$userData = array();
		if( !empty($ud) ){
				//** Get User Country[START]
			$ud['User']['email'] = stripslashes($ud['User']['email']);
				$userCountry = ''; $userCountryCode = '';
				if( !empty($ud['UserProfile']['country_id']) ){
					$countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
					$userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';

					$userCountryCode = !empty($countryData['Country']['country_code'])?$countryData['Country']['country_code']:'';
				}
				//** Get User Country[END]
				//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
					//** If QB id not found add qb id of user[START]
				$checkUserExistsOnUserQbDetail = $this->UserQbDetail->find("count", array("conditions"=> array("user_id"=> $ud['User']['id'])));
					if(empty($qbId)){
						$responseData = $this->checkQBUser($ud['User']['email']);
						$qbDetailsData = json_decode($responseData, true);
						if(empty($qbDetailsData)){
							//** One time token to validate for QB[START]
							$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenData = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
							$this->UserOneTimeToken->save($oneTimeTokenData);
							//** One time token to validate for QB[END]
							$qbDetails = $this->Quickblox->quickAdduserFromOcr($ud['User']['email'], $oneTimeToken, $ud['User']['email'], $ud['User']['id'], $ud['UserProfile']['first_name'].' '.$ud['UserProfile']['last_name']);
							$responseData = $this->checkQBUser($ud['User']['email']);
							$qbDetailsData = json_decode($responseData, true);
							//** Add QB id to our DB
							if($checkUserExistsOnUserQbDetail == 0){
								$addQbDetailsData = array("user_id"=> $ud['User']['id'], "qb_id"=> $qbDetailsData['user']['id']);
								$this->UserQbDetail->save($addQbDetailsData);
							}else{
								$this->UserQbDetail->updateAll(array("qb_id"=> $qbDetailsData['user']['id']), array("user_id"=> $ud['User']['id']));
							}
						}else{
							//** Add QB id to our DB
							if($checkUserExistsOnUserQbDetail == 0){
								$addQbDetailsData = array("user_id"=> $ud['User']['id'], "qb_id"=> $qbDetailsData['user']['id']);
								$this->UserQbDetail->save($addQbDetailsData);
							}else{
								$this->UserQbDetail->updateAll(array("qb_id"=> $qbDetailsData['user']['id']), array("user_id"=> $ud['User']['id']));
							}
						}
						$qbId = !empty($qbDetailsData['user']['id']) ? $qbDetailsData['user']['id'] : 0;
					}
					//** If QB id not found add qb id of user[END]
				$qbInfo = array("id"=> $qbId);
				//** Get QB details [END]
				//** Get role status [START]
				$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				//** Get role status [END]
				//** User Profession[START]
			$userProfession = '';
			if( !empty($ud['UserProfile']['profession_id']) ){
				$professionData = $this->Profession->findById( $ud['UserProfile']['profession_id'] );
				$userProfession  = !empty($professionData['Profession']['profession_type'])?$professionData['Profession']['profession_type']:'';
			}
			//** User Profession[END]
			//** Get Current Employment[START]
				$companyId = 0; $userCurrentCompanyName = ""; $isCurrent = 1;
				$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "is_current"=> 1)));
				$companyId = $userCurrentCompany['UserEmployment']['company_id'];
				if($companyId == -1){
					$userCurrentCompanyName = "None";
				}else if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
					$companyId = $userCurrentCompany['UserEmployment']['company_id'];
					$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
					if(!empty($userCompanyDetails)){
						$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
					}
				}
			//** Get Current Employment[END]

			//** On call Access profession wise[START]
				$oncallAccess = 0;
				$professionIds = array(1,2,3,8);
				if(in_array($ud['UserProfile']['profession_id'], $professionIds)){
					$oncallAccess = 1;
				}
			//** On call Access profession wise[END]
			//** Check user on duty[START]
				$onDutyVal = 0;
				$atWorkVal = 0;
				// $dutyCheck = $this->UserDutyLog->find("all", array("conditions"=> array("user_id"=> $ud['User']['id']), 'order'=> 'id DESC'));
				$dutyCheck = $this->UserDutyLog->getStatusInfo($ud['User']['id']);
				// if( !empty($dutyCheck['UserDutyLog']['status'])){
				// 	$onDutyVal = 1;
				// }
				// if( !empty($dutyCheck['UserDutyLog']['atwork_status'])){
				// 	$atWorkVal = $dutyCheck['UserDutyLog']['atwork_status'];
				// }
				foreach ($dutyCheck as $value) {
					$atWorkVal = $value['UserDutyLog']['atwork_status'];
					$onDutyVal = $value['UserDutyLog']['status'];
				}
			//** Check user on duty[START]
				$userData = array(
									'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
									'first_time_loggedin'=> ($ud['User']['last_loggedin_date'] == '0000-00-00 00:00:00') ? "Y":"N",
									'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
									'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
									'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
									'country'=> !empty($userCountry) ? $userCountry : '',
									'country_code'=> !empty($userCountryCode) ? $userCountryCode : '',
									'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
									'role_id'=> (!empty($ud['User']['role_id']) && $ud['User']['role_id'] == 1) ? $ud['User']['role_id'] : 0,
									'phone'=> array('mobile_no'=> !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'', 'is_mobile_verified'=> !empty($ud['UserProfile']['contact_no_is_verified']) ? $ud['UserProfile']['contact_no_is_verified']: 0),
									'qb_details'=> $qbInfo,
									'Profession'=> array('id'=>!empty($ud['UserProfile']['profession_id'])?$ud['UserProfile']['profession_id']:"", 'profession_type'=> !empty($userProfession) ? $userProfession:''),
									'current_employment'=> $userCurrentCompanyName,
									"role_status"=> !empty($userRoleStatusData['UserProfile']['role_status']) ? $userRoleStatusData['UserProfile']['role_status']:'',
									"oncall_access"=> $oncallAccess,
									"on_duty"=> $onDutyVal,
									"at_work"=> $atWorkVal,
									//"dutyArray"=> $dutyCheck,
									"Company"=> array("company_id"=> !empty($companyId)?$companyId:0,"company_name"=>$userCurrentCompanyName, "is_current"=> $isCurrent)
								);
			}
		return $userData;
	}


	/*
	-------------------------------------------------------------------------------------
	On: 10-08-2015
	I/P:
	O/P:
	Desc: Saving specilities to user.
	-------------------------------------------------------------------------------------
	*/
	public function addUserSpecilities(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** At first um mark all specilities of user
				$updateData = $this->UserSpecilities->updateAll( array('status'=> 0), array('user_id'=>$dataInput['user_id']) );
				//** Update One by One specility
				foreach($dataInput['specilitiy_ids'] as $specilitiesId){
					$specilitiesData = array('user_id'=>$dataInput['user_id'] , 'specilities_id'=> $specilitiesId);
					if( $this->UserSpecilities->find("count", array("conditions"=> array(array('user_id'=>$dataInput['user_id'] , 'specilities_id'=> $specilitiesId)))) == 0 ){
						$saveData = $this->UserSpecilities->saveAll( $specilitiesData );
					}else{
						$updateData = $this->UserSpecilities->updateAll( array('status'=> 1), array('user_id'=>$dataInput['user_id'] , 'specilities_id'=> $specilitiesId) );
					}
				}
				//** Add all specilities as interest
					//** update all Interests
						$this->UserInterest->updateAll(array("status"=>0), array('user_id'=> $dataInput['user_id']));
						//** add or update interest one by one according to request
						foreach( $dataInput['specilitiy_ids'] as $interestId ){	//echo "<pre>";print_r($interestId);die;
							$interestData = array('user_id'=> $dataInput['user_id'],'interest_id'=> $interestId, 'status'=> 1 );
							if($this->UserInterest->find("count", array("conditions"=>array('user_id'=> $dataInput['user_id'],'interest_id'=> $interestId))) ==0 ){
								$this->UserInterest->saveAll( $interestData );
							}else{
								$this->UserInterest->updateAll(array("status"=> 1), array('user_id'=> $dataInput['user_id'],'interest_id'=> $interestId));
							}
						}

				$responseData = array('method_name'=> 'addUserSpecilities', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
			}else{
				// $getUserStatusResponse = $this->getUserStatus($dataInput['user_id']);
				// $responseData = array('method_name'=> 'addUserSpecilities', 'status'=>"0", 'response_code'=> $getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
				$response = array('method_name'=> 'addUserSpecilities', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addUserSpecilities', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 24-08-2015
	I/P: JSON data {"token":""}
	O/P: JSON data with error code
	Desc:
	-------------------------------------------------------------------------------------
	*/
	public function logout(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() && $this->validateAccessKey() ){
					$getHeaderData = $this->getHeaderValues();
					if( !empty( $getHeaderData ) ){
						// $decryptedToken = $this->Common->decryptData($getHeaderData['token']);
						$decryptedToken = $getHeaderData['token'];
						try{
							$logout = $this->UserDevice->updateAll(array('status'=>0), array('token'=> $decryptedToken));
		     			if( $logout ){
		     				//** If logout successful change status of GCM notiifcation
		     				try{
		     					$notificationData = array("user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : '', "device_id"=> isset($dataInput['device_id']) ? $dataInput['device_id'] : '');
		     					if( $this->NotificationUser->find("count", array("conditions"=> $notificationData)) > 0 ){
		     						$this->NotificationUser->updateAll( array("status"=> 0), $notificationData );
		     					}
		     				}catch(Exception $e){}
							try {
								$checkToken = $this->UserLoginTransaction->find("first", array("conditions"=> array("token"=> $decryptedToken)));
								$dataParams['user_id'] = $checkToken['UserLoginTransaction']['user_id'];
								$dataParams['device_id'] = $checkToken['UserLoginTransaction']['device_id'];
								$dataParams['token'] = $checkToken['UserLoginTransaction']['token'];
								$dataParams['device_type'] = $checkToken['UserLoginTransaction']['device_type'];
								$dataParams['application_type'] = $checkToken['UserLoginTransaction']['application_type'];
								$dataParams['login_status'] = "0";
								$setLoginTransaction = $this->userLoginLogoutTransaction($dataParams);
							} catch (Exception $e) {

							}

		     				$responseData = array('method_name'=> 'logout','status'=>'1','response_code'=>'200','message'=> ERROR_200);
		    			}else{
		    				$responseData = array('method_name'=> 'logout','status'=>'0','response_code'=>'617','message'=> ERROR_617);
		    			}
		    		}catch( Exception $e ){
		    			$responseData = array('method_name'=> 'logout','status'=>'0','response_code'=> '615', 'system_errors'=> $e->getMessage() );
		    		}
		    	}else{
						$responseData = array('method_name'=> 'logout','status'=>'0','response_code'=>'617','message'=> ERROR_617);
					}
		    }else{
		    	// $getUserStatusResponse = $this->getUserStatus($dataInput['user_id']);
		     //     $responseData = array('method_name'=> 'logout','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
    		$response = array('method_name'=> 'logout', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);
       }
		  }else{
				$responseData = array('method_name'=> 'logout','status'=>'0','response_code'=>'611','message'=> ERROR_611);
			}
    	echo json_encode($responseData);
    	exit();
	}

	/*
	-------------------------------------------------------------------------------------
	On: 08-09-2015
	I/P: HEADER: (access_key, token) JSON: {"user_id":""}
	O/P: JSON data
	Desc: Get user profile for any particular user
	-------------------------------------------------------------------------------------
	*/
	public function getUserProfile(){
		$responseData = array();
		$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$t1 = explode(" ",microtime());
		$milisec1 = str_replace('.', '', substr((string)$t1[0],1,4));
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$userInformation =  $this->AdminActivityLog->find('first', array('conditions' =>
		                array('AdminActivityLog.user_id' => $dataInput['loggedin_user_id']),'order'=>array('AdminActivityLog.id DESC')
		                ,'limit'=> 1));
			if($userInformation['AdminActivityLog']['message'] != "Deactivated")
		    {
				if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1){
					if( $this->tokenValidate() && $this->accesskeyCheck() ){
							if( $this->User->findById( $dataInput['user_id'] ) ){
								$params['User.id'] = $dataInput['user_id'];
								$this->User->recursive = 2;
								$userDataList = $this->User->userDetails( $params );
								$loggedinUserId = $dataInput['loggedin_user_id'];
								$userData = $this->filterUserFields($userDataList, $loggedinUserId);
								//** Check user follow
								$isFollow = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $dataInput['loggedin_user_id'], "followed_to"=> $dataInput['user_id'], "follow_type"=> 1, "status"=> 1)));
								$userData['is_following'] = ( $isFollow > 0 ) ? 1 : 0;
								//** Check user Colleague
									$colleagueStatus = 0; $requestType = '';
									$myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $dataInput['loggedin_user_id'], "colleague_user_id"=> $dataInput['user_id'])));
									$meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['loggedin_user_id'])));
									if( !empty($myColleague) ){
										$colleagueStatus = $myColleague['UserColleague']['status'];
										$requestType = "from_me";
									}elseif( !empty($meColleague) ){
										$colleagueStatus = $meColleague['UserColleague']['status'];
										$requestType = "to_me";
									}
									$userData['Colleague'] = array('status' => (int) $colleagueStatus, 'request_by' => $requestType);
									$userData['UserVisibilitySetting'] = $this->userVisibilitySettingForMobileEmail($dataInput['user_id']);
								// Check condition whethr Colleague or not
									if($dataInput['loggedin_user_id']!=$dataInput['user_id']){
									     $userData['isYourColleague'] = $this->isYourColleague($dataInput['loggedin_user_id'],$dataInput['user_id']);
								     }
								//** Check User enterprise subscription details[START]
									$status = 0;$expiryDate = 0;
									$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"])));
									if(!empty($checkUserSubscribedFinal)){
										if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
										$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
										if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
										//** Check if expired
										if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
									}
									$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate)>0) ? strtotime($expiryDate):0);
									$settingData = array($subscriptionData);
									//** Check User enterprise subscription details[END]
									$responseData = array('method_name'=> 'getUserProfile','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> array('User'=> $userData, 'settings'=> $settingData));
					    	}
					    	else{
					    		$responseData = array('method_name'=> 'getUserProfile','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
					    	}
					    }else{
					         $responseData = array('method_name'=> 'getUserProfile','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
				         }
			        }else{
				    		$getUserStatusResponse = $this->getUserProfileStatus($dataInput['user_id'],$dataInput['loggedin_user_id']);
				    		$responseData = array('method_name'=> 'getUserProfile','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
				    	}
				}else{
					$responseData = array('method_name'=> 'getUserProfile', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
				}
	       }else{
			$responseData = array('method_name'=> 'getUserProfile','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	//echo json_encode($responseData);
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		//** Track API Request, Response[START]
		$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$t2 = explode(" ",microtime());
		$milisec2 = str_replace('.', '', substr((string)$t2[0],1,4));
		$headerVals = $this->getHeaderValues();
		$trackData = array(
							"user_id"=> isset($dataInput['loggedin_user_id']) ? $dataInput['loggedin_user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> ($milisec2 - $milisec1),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		//** Track API Request, Response[END]
    	exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 08-09-2015
	I/P: array()
	O/P: array()
	Desc: Get all user profile fields
	-------------------------------------------------------------------------------------
	*/
	public function filterUserFields( $ud = array(), $loggedinUserId = NULL ){
		$userData = array();
		if( !empty($ud) ){
			//** Get User Country[START]
			$userCountry = ''; $userCountryCode = '';
			if( !empty($ud['UserProfile']['country_id']) ){
				$countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
				$userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';
				$userCountryCode = !empty($countryData['Country']['country_code'])?$countryData['Country']['country_code']:'';
			}
			//** Get User Country[END]
			//** User Profession[START]
			$userProfession = '';
			if( !empty($ud['UserProfile']['profession_id']) ){
				$professionData = $this->Profession->findById( $ud['UserProfile']['profession_id'] );
				$userProfession  = !empty($professionData['Profession']['profession_type'])?$professionData['Profession']['profession_type']:'';
			}
			//** User Profession[END]

			if( $ud['User']['id'] == $loggedinUserId ){ //** Personal Information only self user.
				$userEmail = !empty($ud['User']['email']) ? $ud['User']['email']:'';
				$contactNum = !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'';
				$address = !empty($ud['UserProfile']['address']) ? $ud['UserProfile']['address']:'';
				$dob = (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'';
			}else{
				//** Check Email visibility settings[START]
				$emailVisibilityCheck = $this->UserVisibilitySetting->find("first", array("conditions"=> array("visibility_setting_type"=> "Email", "user_id"=> $ud['User']['id'])));
				if(!empty($emailVisibilityCheck) && $emailVisibilityCheck['UserVisibilitySetting']['visibility_setting_value']==3){
					$userEmail = "Private";
				}else{
					$userEmail = !empty($ud['User']['email']) ? $ud['User']['email']:'';
				}
				//** Check Email visibility settings[END]

				//** Check Mobile visibility settings[START]
				$mobileVisibilityCheck = $this->UserVisibilitySetting->find("first", array("conditions"=> array("visibility_setting_type"=> "Mobile", "user_id"=> $ud['User']['id'])));
				if(!empty($mobileVisibilityCheck) && $mobileVisibilityCheck['UserVisibilitySetting']['visibility_setting_value']==3){
					$contactNum = 'Private';
				}else{
					$contactNum = !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'';
				}
				//** Check Mobile visibility settings[END]
				$address = !empty($ud['UserProfile']['address']) ? $ud['UserProfile']['address']:'';
				$dob = (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'';
			}
			//** If user is colleague show DOB[START]
			$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'], "status"=> 1)));
			$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId, "status"=> 1)));
			if($myColleague > 0 || $meColleague > 0){
				$dob = (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'';
			}
			//** If user is colleague show DOB[END]
			//** User employment history[START]
				$userEmploymentData = array(); $designationDetails = array();
				$userEmployment = $this->UserEmployment->getUserEmployment( $ud['User']['id'] );
				foreach( $userEmployment as $employment){
					$designationDetails = array("designation_id"=> !empty($employment['Designation']['id']) ? $employment['Designation']['id'] : (int) 0, "designation_name"=> !empty($employment['Designation']['designation_name']) ? $employment['Designation']['designation_name'] : "");
					$dutyStatusData = $this->UserDutyLog->userDutyStatus(array("user_id"=>$ud['User']['id'] , "company_id"=> $employment['UserEmployment']['company_id']));
					$dutyStatus = isset($dutyStatusData['UserDutyLog']['status']) ? $dutyStatusData['UserDutyLog']['status'] : 0;
					$companyIdOfuser = $employment['UserEmployment']['company_id'];
					if($companyIdOfuser == -1){
						$companyId = -1;
					}else{
						$companyId = !empty($employment['Company']['id']) ? $employment['Company']['id'] : (int) 0;
					}
					if($companyIdOfuser == -1){
						$companyName = "None";
					}else{
						$companyName = !empty($employment['Company']['company_name']) ? $employment['Company']['company_name'] : '';
					}
					$isCompanyAssignPending = 0;
					if($companyId != -1){
						if($employment['Company']['is_subscribed'] == 1 && $employment['Company']['status']==1){
							$isCompanyAssignCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $ud['User']['email'], "company_id"=> $companyId, "status"=> 0)));
						}
					}
					if($isCompanyAssignCheck > 0){ $isCompanyAssignPending = 1; }
					$userEmploymentData[] = array("id"=> !empty($employment['UserEmployment']['id']) ? (int) $employment['UserEmployment']['id'] : (int) 0, "company_id"=> $companyId ,"company_name"=> $companyName, "from_date"=> $employment['UserEmployment']['from_date'], "to_date"=> $employment['UserEmployment']['to_date'],"location"=> $employment['UserEmployment']['location'], "description"=> $employment['UserEmployment']['description'],"is_current"=> $employment['UserEmployment']['is_current'],"duty_status"=> $dutyStatus, "Designation"=> $designationDetails, "is_company_approval_pending"=> $isCompanyAssignPending);
				}
			//** User employment history[END]
			//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
				$qbInfo = array("id"=> $qbId);
			//** Get QB details [END]
			//** Get role status [START]
			$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
			//** Get role status [END]
			//** Get Current Employment[START]
				$userCurrentCompanyName = "";
				$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "is_current"=> 1)));
				$companyId = $userCurrentCompany['UserEmployment']['company_id'];
				if($companyId == -1){
					$userCurrentCompanyName = "None";
				}else if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
					$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
					if(!empty($userCompanyDetails)){
						$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
					}
				}
				//** Get Current Employment[END]
				//** On call Access profession wise[START]
				$oncallAccess = 0;
				$professionIds = array(1,2,3,8);
				if(in_array($ud['UserProfile']['profession_id'], $professionIds)){
					$oncallAccess = 1;
				}
				$atWorkStatus = 0;
				$dutyCheck = $this->UserDutyLog->getStatusInfo($ud['User']['id']);
				foreach ($dutyCheck AS $value) {
					$atWorkStatus = $value['UserDutyLog']['atwork_status'];
				}
				//** On call Access profession wise[END]
			if(!empty($qbId)){
			$userData = array(
								'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
								'role_id'=> (!empty($ud['User']['role_id']) && $ud['User']['role_id'] == 1) ? $ud['User']['role_id'] : 0,
								'email'=>	stripslashes($userEmail),
								'last_loggedin_date'=> !empty($ud['User']['last_loggedin_date']) ? $ud['User']['last_loggedin_date']:'',
								'status' => !empty($ud['User']['status']) ? (string) $ud['User']['status']:'',
								'registration_date'=> !empty($ud['User']['registration_date']) ? (string) $ud['User']['registration_date']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'country'=> !empty($userCountry) ? $userCountry : '',
								'country_code'=> !empty($userCountryCode) ? $userCountryCode : '',
								'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
								'city'=> !empty($ud['UserProfile']['city']) ? $ud['UserProfile']['city']:'',
								'Profession'=> array('id'=>!empty($ud['UserProfile']['profession_id'])?$ud['UserProfile']['profession_id']:"", 'profession_type'=> !empty($userProfession) ? $userProfession:''),
								'gmcnumber'=> !empty($ud['UserProfile']['gmcnumber']) ? $ud['UserProfile']['gmcnumber']:'',
								'phone'=> array('mobile_no'=> $contactNum, 'is_mobile_verified'=> !empty($ud['UserProfile']['contact_no_is_verified']) ? $ud['UserProfile']['contact_no_is_verified']: 0),
								'address'=> $address,
								'dob'=> $dob,
								'gender'=> !empty($ud['UserProfile']['gender']) ? $ud['UserProfile']['gender']:'',
								'current_employment'=> $userCurrentCompanyName,
								'Company'=> !empty($userEmploymentData) ? $userEmploymentData : array(),
								"qb_details"=> $qbInfo,
								"role_status"=> !empty($userRoleStatusData['UserProfile']['role_status']) ? $userRoleStatusData['UserProfile']['role_status']:'',
								"oncall_access"=> $oncallAccess,
								"at_work"=> $atWorkStatus
							);
			}
		}
		return $userData;
	}

	/*
	On: 12-05-2016
	I/P:
	O/P:
	Desc: User Profile update.
	*/
	public function updateProfile(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] );
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->find( "count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0 ){
					try{
						$userInfo = $this->User->find( "first", array("conditions"=> array("User.id"=>$dataInput['user_id'])));
						//** One time token to validate for QB[START]
						$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
						$oneTimeTokenData = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
						$this->UserOneTimeToken->save($oneTimeTokenData);
						//** One time token to validate for QB[END]
						// ** Profile update goes here
						if(isset($dataInput['edit_details']) && $dataInput['edit_details'] == 'contact'){
							$profileData = array("UserProfile.contact_no"=> "'".$dataInput['contact_no']."'", "UserProfile.contact_no_is_verified"=> $dataInput['is_mobile_verified']);
							//$contactResult = $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $dataInput['user_id']));
							//$qbResponce = $this->updateContactNoOnQb($userInfo['User']['email'],$oneTimeToken, $dataInput['contact_no']);
							$contactResult = $this->UserProfile->updateAll($profileData, array("UserProfile.user_id"=> $dataInput['user_id']));
							$userProfile = $this->getUserInfo($dataInput['user_id'], $dataInput['user_id']);
							$userData['User'] = $userProfile;

							if($contactResult)
							{
								$responseData = array('method_name'=> 'updateProfile', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_610, 'data'=> $userData);
							}
							else
							{
								$responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_648);
							}
						}
						if(isset($dataInput['edit_details']) && $dataInput['edit_details'] == 'role_status'){
							$profileData = array("UserProfile.role_status"=> "'". addslashes($dataInput['role_status_text']) ."'");
							$qbResponce = $this->updateRoleStatusOnQb($userInfo['User']['email'],$oneTimeToken, $dataInput['role_status_text']);
							$roleStatusResult = $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $dataInput['user_id']));
							$userProfile = $this->getUserInfo($dataInput['user_id'], $dataInput['user_id']);
							$userData['User'] = $userProfile;

							if($roleStatusResult)
							{
								$responseData = array('method_name'=> 'updateProfile', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601, 'data'=> $userData);
							}
							else
							{
								$responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
							}
						}
						if (isset($dataInput['edit_details']) && $dataInput['edit_details'] == 'user_img') {
							$imageName = $this->Image->createimage($dataInput['profile_img'],WWW_ROOT . 'img/uploader_tmp/', md5($dataInput['user_id'] .'_' . strtotime(date('Y-m-d H:i:s')) ), $dataInput['img_ext']);
							if( !empty($imageName) ){
								$this->Image->moveImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$imageName, 'img_name'=> $dataInput['user_id'].'/profile/'.$imageName) );
							}
							//** Update avatar url on qb
							$imgVal = AMAZON_PATH.$dataInput['user_id'].'/profile/'.$imageName;
							$qbResponce = $this->updateAvatarUrlOnQb($userInfo['User']['email'],$oneTimeToken, $imgVal);
							if($qbResponce['qbsuccess'] == 1)
							{
								//** Update Image name on table
								$profileData = array("UserProfile.profile_img"=> "'".$imageName."'");
								$imageUploadResult = $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $dataInput['user_id']));
								//** Remove physical file from Temp Directory
								$this->Common->removeTempFile( WWW_ROOT . 'img/uploader_tmp/'.$imageName );
								$userProfile = $this->getUserInfo($dataInput['user_id'], $dataInput['user_id']);
								$userData['User'] = $userProfile;
								if($imageUploadResult)
								{
									$responseData = array('method_name'=> 'updateProfile', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601, 'data'=> $userData);
								}
								else
								{
									$responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
								}
							}
							else
							{
							$responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618, "system_errors"=> $qbResponce);
							}
						}
						//** Get User Details
						//$userProfile = $this->getUserInfo($dataInput['user_id'], $dataInput['user_id']);
						//$userData['User'] = $userProfile;
						//$responseData = array('method_name'=> 'updateProfile', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601, 'data'=> $userData);
						//** Update Activity Logs[START]
						$localServerInfo = $this->Common->getFrontServerInfo();
							$activityLogData = array(
									"user_id"=> $dataInput['user_id'],
									"item_id"=> 0,
									"activity_type"=> "editProfile",
									"message"=> "",
									"old_info"=> $this->Common->arrayToJson($userInfo),
									"log_info"=> $localServerInfo,
								);
							$activityLog = $this->ActivityLog->save($activityLogData);
						//** Update Activity Logs[END]
					}catch(Exception $e){
						$responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				// $getUserStatusResponse = $this->getUserStatus($dataInput['user_id']);
				// $responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> $getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
				$responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'updateProfile', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	On:
	I/P:
	O/P:
	Desc: Check if email already exists and also belongs to pre existing domain
	*/
	public function preExistEmail(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateAccessKey() ){
					$emailExists = $this->User->find("count", array("conditions"=> array("User.email"=> $dataInput['email'])));
					$domainNameArr = explode("@", $dataInput['email']);//** Extract Domain name from email
					$domainName = end($domainNameArr);	//** Domain Name
					$rootDomain = $this->Common->getTld($domainName);
					$preQualifiedEmail = $this->PreQualifiedDomain->find("count", array("conditions"=> array("domain_name"=> trim($rootDomain), "status"=>1)));
					$preQualifiedData = array("email_exists"=> $emailExists, "pre_qualified_email"=> $preQualifiedEmail);
					if($dataInput['msg_type'] == 1){
						$successCode = SUCCESS_602;
					}elseif($dataInput['msg_type'] == 2){
						$successCode = SUCCESS_603;
					}else{
						$successCode = ERROR_200;
					}
					$responseData = array('method_name'=> 'preExistEmail','status'=>'1','response_code'=>'200', 'message'=> $successCode, 'data'=> $preQualifiedData);
			    }else{
			         $responseData = array('method_name'=> 'preExistEmail','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
		         //** Save Temp registration data[START]
		         try{
		         	$tempRegisterData = array(
		         							"email"=> $dataInput['email'],
		         							"user_info"=> $this->Common->getFrontServerInfo(),
		         							"registration_source"=> !empty($dataInput['registration_source']) ? $dataInput['registration_source']: "MB",
		         							"device_type"=> !empty($dataInput['device_type']) ? $dataInput['device_type']: 0,
		         							"version"=> !empty($dataInput['version']) ? $dataInput['version']: 0,
		         						);
		         	$saveTempData = $this->TempRegistration->save($tempRegisterData);
		         }catch(Exception $e){}
		         //** Save Temp registration data[END]
	       }else{
			$responseData = array('method_name'=> 'preExistEmail','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	echo json_encode($responseData);
    	exit();
	}

	/*
	--------------------------------------------------------------------------
	On: 03-11-2015
	I/P: JSON
	O/P: JSON, user lists
	Desc: Fetching user list by pagination with search
	--------------------------------------------------------------------------
	*/
	public function userLists(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
					$params['first_name'] = isset($dataInput['first_name']) ? stripslashes($dataInput['first_name']) : '';
					$params['last_name'] = isset($dataInput['last_name']) ? stripslashes($dataInput['last_name']) : '';
					$params['email'] = isset($dataInput['email']) ? $dataInput['email'] : '';
					$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
					$userDetails = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $loggedinUserId)));
					$params['loggedin_user_countryid'] = $userDetails['UserProfile']['country_id'];
					$userLists = $this->User->userSearch( $params, $loggedinUserId );
					$userData['UserProfile'] = $this->userFieldsFormat( $userLists , $loggedinUserId );
					$responseData = array('method_name'=> 'userLists','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData);
			    }else{
			    	// $getUserStatusResponse = $this->getUserStatus($dataInput['user_id']);
			     //     $responseData = array('method_name'=> 'userLists','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
			    	$responseData = array('method_name'=> 'userLists','status'=>'0','response_code'=>'602','message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'userLists','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	--------------------------------------------------------------------------
	On: 03-11-2015
	I/P: JSON
	O/P: JSON, user lists
	Desc: Fetching user list by pagination with search
	--------------------------------------------------------------------------
	*/
	public function userFieldsFormat( $userData = array(), $loggedinUserId = NULL ){
		$userDataList = array();
		if( !empty($userData) ){
		//** User Data
			foreach( $userData as $ud ){
				//** Find user colleague with logged in user
				$colleagueStatus = 0; $requestType = '';
				$myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'])));
				$meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId)));
				if( !empty($myColleague) ){
					$colleagueStatus = $myColleague['UserColleague']['status'];
					$requestType = "from_me";
				}elseif( !empty($meColleague) ){
					$colleagueStatus = $meColleague['UserColleague']['status'];
					$requestType = "to_me";
				}
				//** Check loggedin user follows or not
				$isFollow = 0;
				$conditions = array("followed_by"=> $loggedinUserId, "followed_to"=> $ud['User']['id'], "follow_type"=> 1, "status"=> 1);
				$followCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
				if( $followCheck > 0) { $isFollow = 1; }
				//** Get Country name
				$countryDetails = $this->Country->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['country_id'])));
				//** Get Profession
				$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['profession_id'])));
				//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
				$qbInfo = array("id"=> $qbId);
				//** Get QB details [END]
				//** Get role status [START]
				$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				//** Get role status [END]

				//** Get Current Employment[START]
				$userCurrentCompanyName = "";
				$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "is_current"=> 1)));
				$companyId = $userCurrentCompany['UserEmployment']['company_id'];
				if($companyId == -1){
					$userCurrentCompanyName = "None";
				}else if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
					$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
					if(!empty($userCompanyDetails)){
						$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
					}
				}
				//** Get Current Employment[END]
				//** On call Access profession wise[START]
				$oncallAccess = 0;
				$professionIds = array(1,2,3,8);
				if(in_array($ud['UserProfile']['profession_id'], $professionIds)){
					$oncallAccess = 1;
				}
				//** On call Access profession wise[END]
				if(!empty($qbId)){
				$userDataList[] = array(
								'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
								'email'=>	!empty($ud['User']['email']) ? stripslashes($ud['User']['email']):'',
								'last_loggedin_date'=> !empty($ud['User']['last_loggedin_date']) ? $ud['User']['last_loggedin_date']:'',
								'status' => !empty($ud['User']['status']) ? (string) $ud['User']['status']:'',
								'registration_date'=> !empty($ud['User']['registration_date']) ? (string) $ud['User']['registration_date']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'country'=> !empty($countryDetails['Country']['country_name']) ? $countryDetails['Country']['country_name']:'',
								'country_code'=> !empty($countryDetails['Country']['country_code']) ? $countryDetails['Country']['country_code']:'',
								'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
								'city'=> !empty($ud['UserProfile']['city']) ? $ud['UserProfile']['city']:'',
								'profession'=> !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type']:'',
								'gmcnumber'=> !empty($ud['UserProfile']['gmcnumber']) ? $ud['UserProfile']['gmcnumber']:'',
								'contact_no'=> !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'',
								'address'=> !empty($ud['UserProfile']['address']) ? $ud['UserProfile']['address']:'',
								'dob'=> (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'',
								'year_of_graduation'=> !empty($ud['UserProfile']['year_of_graduation']) ? $ud['UserProfile']['year_of_graduation']:'',
								'gender'=> !empty($ud['UserProfile']['gender']) ? $ud['UserProfile']['gender']:'',
								//'current_employment'=> !empty($ud['UserProfile']['current_employment']) ? $ud['UserProfile']['current_employment']:'',
								'current_employment'=> $userCurrentCompanyName,
								'institution_name'=> !empty($ud['UserProfile']['institution_name']) ? $ud['UserProfile']['institution_name']:'',
								'Colleague'=> array("status"=> $colleagueStatus, "request_by"=> $requestType),
								'is_follow'=> $isFollow,
								'qb_details'=> $qbInfo,
								//'role_status'=> !empty($ud['UserProfile']['role_status']) ? $ud['UserProfile']['role_status']:'',
								"role_status"=> !empty($userRoleStatusData['UserProfile']['role_status']) ? $userRoleStatusData['UserProfile']['role_status']:'',
								"oncall_access"=> $oncallAccess
								);
				}
			}
		}
		return $userDataList;
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 06-11-2015
	I/P: JSON (user_id, old_password, new_password)
	O/P: JSON (Success/Fail)
	Desc: User can Reset password
	-----------------------------------------------------------------------------------------
	*/
	/*public function resetPassword(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] );
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->find( "count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0 ){
					//** Reset Password
					$userData = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
					if( $userData['User']['password'] == md5($dataInput['old_password']) ){
						try{
							$changePass = $this->User->updateAll( array("User.password"=> "'". md5($dataInput['new_password']) ."'" ), array("User.id"=> $dataInput['user_id']) );
							if( $changePass ){
								$responseData = array('method_name'=> 'resetPassword', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
							}else{
								$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
							}
						}catch( Exception $e ){
							$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
						}
					}else{
						$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "606", 'message'=> ERROR_606);
					}
				}else{
					$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}*/

	/*
	-----------------------------------------------------------------
	On: 31-12-2015
	I/P:
	O/P:
	Desc: fetches all master institution names.
	-----------------------------------------------------------------
	*/
	public function getInstitutesMasterData(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] );
				$dataInput = json_decode($encryptedData, true);
				$params["searchText"] = isset($dataInput['searchText']) ? $dataInput['searchText'] : '';
				$params["size"] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
				$params["page_number"] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
				$instituteLists = $this->InstituteName->instituteLists( $params );
				if( !empty($instituteLists) ){
					$instituteData = array();
					foreach( $instituteLists as $institutes ){
						$instituteData[] = array("id"=> $institutes['InstituteName']['id'], "institute_name"=> !empty($institutes['InstituteName']['institute_name']) ? $institutes['InstituteName']['institute_name'] : '');
					}
					$institutesList['Institutes'] = $instituteData;
					$responseData = array('method_name'=> 'getInstitutesMasterData', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $institutesList);
				}else{
					$responseData = array('method_name'=> 'getInstitutesMasterData', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'getInstitutesMasterData', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'getInstitutesMasterData', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-----------------------------------------------------------------
	On: 12-05-2016
	I/P:
	O/P:
	Desc: fetches all master company names.
	-----------------------------------------------------------------
	*/
	public function getCompaniesMasterData(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$userStatusDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$userCountryDetail = $this->UserProfile->find("first", array("conditions"=> array("UserProfile.user_id"=> $dataInput['user_id'])));
			if($userStatusDetails['User']['status']==1 && $userStatusDetails['User']['approved']==1)
			{
				if( $this->validateToken() && $this->validateAccessKey() ){
					$params["searchText"] = isset($dataInput['searchText']) ? $dataInput['searchText'] : '';
					$params["size"] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$params["page_number"] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
					$params["userCountry"] = isset($userCountryDetail['UserProfile']['country_id']) ? $userCountryDetail['UserProfile']['country_id'] : '';
					$companyLists = $this->CompanyName->companyLists( $params );
					if( !empty($companyLists) ){
						foreach( $companyLists as $company ){
						//** Check if user subscribed for comapny[START]
						$isUserSubscribed = 0;
						$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
						$dataParams['email'] = $userDetails['User']['email'];
						$dataParams['company_id'] = $company['CompanyName']['id'];
						//$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);
						if($company['CompanyName']['is_subscribed']==1 && $company['CompanyName']['status']==1){ $isUserSubscribed = 1; }
						if(!empty($checkUserForPaid)){ $isUserSubscribed = 1; }
						//** Check if user subscribed for comapny[END]
							$companyData[] = array("id"=> $company['CompanyName']['id'], "company_name"=> $company['CompanyName']['company_name'],"is_partner"=> !empty($isUserSubscribed)?1:0);
						}
						$companies['Company'] = $companyData;
						$responseData = array('method_name'=> 'getCompaniesMasterData', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $companies);
					}else{
						$responseData = array('method_name'=> 'getCompaniesMasterData', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
					}
				}else{
					$responseData = array('method_name'=> 'getCompaniesMasterData', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
				}
			}
			else
			{
				$statusResponse = $this->getUserStatus($dataInput['user_id'],$dataInput['user_id']);
				$responseData = array('method_name'=> 'getCompaniesMasterData','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'getCompaniesMasterData', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-----------------------------------------------------------------
	On: 12-05-2016
	I/P:
	O/P:
	Desc: fetches all master education degree names.
	-----------------------------------------------------------------
	*/
	public function getDegreeMasterData(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] );
				$dataInput = json_decode($encryptedData, true);
				$params["searchText"] = isset($dataInput['searchText']) ? $dataInput['searchText'] : '';
				$params["size"] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
				$params["page_number"] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
				$degreeLists = $this->EducationDegree->educationDegreeLists( $params );
				if( !empty($degreeLists) ){
					foreach( $degreeLists as $degree ){
						$degreeData[] = array("id"=> $degree['EducationDegree']['id'], "degree_name"=> $degree['EducationDegree']['degree_name']);
					}
					$companies['Degree'] = $degreeData;
					$responseData = array('method_name'=> 'getDegreeMasterData', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $companies);
				}else{
					$responseData = array('method_name'=> 'getDegreeMasterData', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'getDegreeMasterData', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'getDegreeMasterData', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-----------------------------------------------------------------
	On: 12-05-2016
	I/P:
	O/P:
	Desc: fetches all master designation names.
	-----------------------------------------------------------------
	*/
	public function getDesignationMasterData(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] );
				$dataInput = json_decode($encryptedData, true);
				$params["searchText"] = isset($dataInput['searchText']) ? $dataInput['searchText'] : '';
				$params["size"] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
				$params["page_number"] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
				$designationLists = $this->Designation->designationLists( $params );
				if( !empty($designationLists) ){
					foreach( $designationLists as $designation ){
						$designationData[] = array("id"=> $designation['Designation']['id'], "designation_name"=> $designation['Designation']['designation_name']);
					}
					$designation['Designation'] = $designationData;
					$responseData = array('method_name'=> 'getDesignationMasterData', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $designation);
				}else{
					$responseData = array('method_name'=> 'getDesignationMasterData', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'getDesignationMasterData', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'getDesignationMasterData', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	---------------------------------------------------------------
	On: 12-05-2016
	I/P:
	O/P:
	Desc: Fetches user info by userid
	---------------------------------------------------------------
	*/
	public function getUserInfo($userId = NULL, $loggedinUserId = NULL){
		$userData = array();
		if(!empty($userId)){
			$params['User.id'] = $userId;
			$this->User->recursive = 2;
			$userDataList = $this->User->userDetails( $params );
			$userData = $this->filterUserFields($userDataList, $loggedinUserId);
			//** Check user follow
			//$isFollow = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $loggedinUserId, "followed_to"=> $userId, "follow_type"=> 1, "status"=> 1)));
			//$userData['is_following'] = ( $isFollow > 0 ) ? 1 : 0;
			//** Check user Colleague
				$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userId, "colleague_user_id"=> $loggedinUserId)));
				$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $userId)));
				$colleagueStatus = (int) 0;
				if( $myColleague > 0 && $meColleague > 0 ){ //** Check if already colleague
					if( $myColleague > 0 ){
						$myColleagueData = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $userId, "colleague_user_id"=> $loggedinUserId)));
						$colleagueStatus = $myColleagueData['UserColleague']['status'];
					}else{
						$meColleagueData = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $userId)));
						$colleagueStatus = $meColleagueData['UserColleague']['status'];
					}
				}
				$userData['is_colleague'] = (int) $colleagueStatus;

		}
		return $userData;
	}

	/*
	----------------------------------------------------------------------------
	On: 12-05-2016
	I/P:
	O/P:
	Desc: Fetch user details by user id
	----------------------------------------------------------------------------
	*/
	public function userDetailsById($userId = NULL, $loggedinUserId = NULL){
		$userLists = array();
		if(!empty($userId)){
			$userData = $this->User->userDetailsByUserId($userId, $loggedinUserId);
			$userLists = $this->userFieldData($userData, $loggedinUserId);
		}
		return $userLists;
	}

	/*
	--------------------------------------------------------------------------
	On: 12-05-2016
	I/P: JSON
	O/P: JSON, user lists
	Desc: Fetching user list by pagination with search
	--------------------------------------------------------------------------
	*/
	public function userFieldData( $ud = array(), $loggedinUserId = NULL ){
		$userDataList = array();
		if( !empty($ud) ){
		//** User Data

				//** Find user colleague with logged in user
				$colleagueStatus = 0; $requestType = '';
				$myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'])));
				$meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId)));
				if( !empty($myColleague) ){
					$colleagueStatus = $myColleague['UserColleague']['status'];
					$requestType = "from_me";
				}elseif( !empty($meColleague) ){
					$colleagueStatus = $meColleague['UserColleague']['status'];
					$requestType = "to_me";
				}
				//** Check loggedin user follows or not
				$isFollow = 0;
				$conditions = array("followed_by"=> $loggedinUserId, "followed_to"=> $ud['User']['id'], "follow_type"=> 1, "status"=> 1);
				$followCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
				if( $followCheck > 0) { $isFollow = 1; }
				//** Get Country name
				$countryDetails = $this->Country->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['country_id'])));
				//** Get Profession
				if(!empty($ud['UserProfile']['profession_id'])){
					$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['profession_id'])));
				}
				//** Get Current Employment[START]
				$userCurrentCompanyName = "";
				$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "is_current"=> 1)));
				$companyId = $userCurrentCompany['UserEmployment']['company_id'];
				if($companyId == -1){
					$userCurrentCompanyName = "None";
				}else if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
					$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
					if(!empty($userCompanyDetails)){
						$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
					}
				}
				//** Get Current Employment[END]
				$userDataList = array(
								'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
								'email'=>	!empty($ud['User']['email']) ? stripslashes($ud['User']['email']):'',
								'last_loggedin_date'=> !empty($ud['User']['last_loggedin_date']) ? $ud['User']['last_loggedin_date']:'',
								'status' => !empty($ud['User']['status']) ? (string) $ud['User']['status']:'',
								'registration_date'=> !empty($ud['User']['registration_date']) ? (string) $ud['User']['registration_date']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'country'=> !empty($countryDetails['Country']['country_name']) ? $countryDetails['Country']['country_name']:'',
								'country_code'=> !empty($countryDetails['Country']['country_code']) ? $countryDetails['Country']['country_code']:'',
								'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
								'city'=> !empty($ud['UserProfile']['city']) ? $ud['UserProfile']['city']:'',
								'profession'=> !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type']:'',
								'gmcnumber'=> !empty($ud['UserProfile']['gmcnumber']) ? $ud['UserProfile']['gmcnumber']:'',
								'contact_no'=> !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'',
								'address'=> !empty($ud['UserProfile']['address']) ? $ud['UserProfile']['address']:'',
								'dob'=> (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'',
								'year_of_graduation'=> !empty($ud['UserProfile']['year_of_graduation']) ? $ud['UserProfile']['year_of_graduation']:'',
								'gender'=> !empty($ud['UserProfile']['gender']) ? $ud['UserProfile']['gender']:'',
								//'current_employment'=> !empty($ud['UserProfile']['current_employment']) ? $ud['UserProfile']['current_employment']:'',
								'current_employment'=> $userCurrentCompanyName,
								'institution_name'=> !empty($ud['UserProfile']['institution_name']) ? $ud['UserProfile']['institution_name']:'',
								'Colleague'=> array("status"=> $colleagueStatus, "request_by"=> $requestType),
								'is_follow'=> $isFollow
								);

		}
		return $userDataList;
	}

	/*
	--------------------------------------------------------------------------
	On: 01-06-2016
	I/P: JSON
	O/P: JSON
	Desc: Checking user's current location and profile location
	--------------------------------------------------------------------------
	*/
	public function userGeoFencingCheck(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					$this->User->recursive = 0;
					$userData = $this->User->findById( $dataInput['user_id']);
					if(!empty($userData['UserProfile']['country_id'])){
						$countryId =  $userData['UserProfile']['country_id'];
						$countryData = $this->Country->find("first", array("conditions"=> array("id"=> $countryId)));
						//** Check user geo fencing
						if( $countryData['Country']['country_code'] == $dataInput['country_code']){
							$responseData = array('method_name'=> 'userGeoFencingCheck','status'=>'1','response_code'=>'200', 'message'=> ERROR_200);
						}else{
							$responseData = array('method_name'=> 'userGeoFencingCheck','status'=>'0','response_code'=>'636', 'message'=> ERROR_636);
						}
					}else{
			    		$responseData = array('method_name'=> 'userGeoFencingCheck','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'userGeoFencingCheck','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'userGeoFencingCheck','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	------------------------------------------------------------------------------------
	On: 29-06-2016
	I/P:
	O/P:
	Desc: add quick blox id for any user during add user
	------------------------------------------------------------------------------------
	*/
	public function addUserQBid($detailsArr = array(), $userId = NULL){
		if(!empty($detailsArr) && !empty($userId)){
			$qbId = !empty($detailsArr['user']['id']) ? (int) $detailsArr['user']['id'] : '';
			$qbData = array("user_id"=> $userId, "qb_id"=> $qbId);
			$addQBdetails = $this->UserQbDetail->save($qbData);
		}
	}

	/*
	------------------------------------------------------------------------------------
	On: 12-07-2016
	I/P:
	O/P:
	Desc: check QB user exists
	------------------------------------------------------------------------------------
	*/
	public function checkQBUser($userEmail = NULL){
		$token = $this->Quickblox->quickAuth();
		$userEmail = stripslashes($userEmail);
		$curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/by_email.json?email=' . $userEmail);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);

		        if (!empty($response)) {
		                return $response;
		        } else {
		                return false;
		        }
		        curl_close($curl);

		 die;
	}
	/*
	--------------------------------------------------------------------------
	On: 22-07-2016
	I/P: JSON
	O/P: JSON
	Desc: QB user id
	--------------------------------------------------------------------------
	*/
	public function userIdByQBid(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					$this->User->recursive = 0;
					$userData = $this->UserQbDetail->find('all', array('fields'=>array('user_id','qb_id'),'conditions'=>array('qb_id'=>$dataInput['qb_user_id'] )) );// $dataInput['qb_user_id']);
					if(!empty($userData[0]['UserQbDetail']['user_id'])){

						$responseData = array('method_name'=> 'userIdByQBid','status'=>'1','response_code'=>'200', 'data'=>$userData[0]['UserQbDetail']['user_id'], 'message'=> ERROR_200);

					}else{
			    		$responseData = array('method_name'=> 'userIdByQBid','status'=>'0','response_code'=>'638', 'message'=> ERROR_638);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'userIdByQBid','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'userIdByQBid','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}
/*
-------------------------------------------------------------------------
Method Name:userVisibilitySetting
Developer:Shashi
ON: 03-01-2017
I/P: JSON (user_id, user_email_visible_setting,user_phone_visible_setting)
O/P: JSON (success/fail)
Desc: Used to set the visibility of the user to other user by mean of email and phone.
-------------------------------------------------------------------------
*/
// public function userVisibilitySetting(){
// 		$responseData = array();
// 		if($this->request->is('post')){
// 			$dataInput = $this->request->input('json_decode', true) ;
// 				if( $this->validateToken() && $this->validateAccessKey() ){

// 					if($dataInput['visibility_setting_type']=="Mobile"){
// 						//get count if exist //
// 						$condUserExist = array('user_id'=>$dataInput['user_id'],'visibility_setting_type'=>'Mobile');
// 						$dataExist = $this->UserVisibilitySetting->find('count',array('conditions'=>$condUserExist));
// 						if($dataExist > 0){ // Update

// 							$userVisibilityInfo = $this->UserVisibilitySetting->find( "first", array("conditions"=> $condUserExist ));

// 							//** Update Activity Logs[START]
// 						$localServerInfo = $this->Common->getFrontServerInfo();
// 							$activityLogData = array(
// 									"user_id"=> $dataInput['user_id'],
// 									"item_id"=> 0,
// 									"activity_type"=> "UserVisibilitySetting",
// 									"message"=> "",
// 									"old_info"=> $this->Common->arrayToJson($userVisibilityInfo),
// 									"log_info"=> $localServerInfo,
// 								);
// 							$activityLog = $this->ActivityLog->save($activityLogData);



// 							$this->UserVisibilitySetting->updateAll(array('visibility_setting_value'=>$dataInput['visibility_setting_value'],'updated_date'=>'"'.date("Y-m-d H:i:s").'"'),$condUserExist);
// 						}else{ // Insert

// 							$this->UserVisibilitySetting->save(array(
// 									'id'=>'',
// 									'user_id'=>$dataInput['user_id'],
// 									'visibility_setting_type'=>$dataInput['visibility_setting_type'],
// 									'visibility_setting_value'=>$dataInput['visibility_setting_value'],
// 									'created_date'=>date("Y-m-d H:i:s"),
// 									'updated_date'=>date("Y-m-d H:i:s")

// 								));

// 						}
// 					 $dateResponse = $this->UserVisibilitySetting->find('all',array('conditions'=>array('user_id'=>$dataInput['user_id'],'visibility_setting_type'=>'Mobile')));

// 					 $resultData = array('visibility_setting_type'=>'Mobile','visibility_setting_value'=>$dateResponse[0]['UserVisibilitySetting']['visibility_setting_value']);

// 					 $responseData = array('method_name'=> 'userVisibilitySetting','status'=>'1','response_code'=>'200', 'data'=>$resultData, 'message'=> SUCCESS_607);

// 					}else if($dataInput['visibility_setting_type']=="Email"){
// 						//get count if exist //
// 						$condUserExist = array('user_id'=>$dataInput['user_id'],'visibility_setting_type'=>'Email');
// 						$dataExist = $this->UserVisibilitySetting->find('count',array('conditions'=>$condUserExist));
// 						if($dataExist > 0){ // Update

// 							$userVisibilityInfo = $this->UserVisibilitySetting->find( "first", array("conditions"=> $condUserExist ));

// 							//** Update Activity Logs[START]
// 						$localServerInfo = $this->Common->getFrontServerInfo();
// 							$activityLogData = array(
// 									"user_id"=> $dataInput['user_id'],
// 									"item_id"=> 0,
// 									"activity_type"=> "UserVisibilitySetting",
// 									"message"=> "",
// 									"old_info"=> $this->Common->arrayToJson($userVisibilityInfo),
// 									"log_info"=> $localServerInfo,
// 								);
// 							$activityLog = $this->ActivityLog->save($activityLogData);



// 							$this->UserVisibilitySetting->updateAll(array('visibility_setting_value'=>$dataInput['visibility_setting_value'],'updated_date'=>'"'.date("Y-m-d H:i:s").'"'),$condUserExist);
// 						}else{ // Insert



// 							$this->UserVisibilitySetting->save(array(
// 									'id'=>'',
// 									'user_id'=>$dataInput['user_id'],
// 									'visibility_setting_type'=>$dataInput['visibility_setting_type'],
// 									'visibility_setting_value'=>$dataInput['visibility_setting_value'],
// 									'created_date'=>date("Y-m-d H:i:s"),
// 									'updated_date'=>date("Y-m-d H:i:s")

// 								));
// 						}
// 					 $dateResponse = $this->UserVisibilitySetting->find('all',array('conditions'=>array('user_id'=>$dataInput['user_id'],'visibility_setting_type'=>'Email')));

// 					 $resultData = array('visibility_setting_type'=>'Email','visibility_setting_value'=>$dateResponse[0]['UserVisibilitySetting']['visibility_setting_value']);

// 					 $responseData = array('method_name'=> 'userVisibilitySetting','status'=>'1','response_code'=>'200', 'data'=>$resultData, 'message'=> SUCCESS_607);

// 					}else{
// 					  $resultData = array('visibility_setting_type'=>'Mobile','visibility_setting_value'=>array());
// 					  $responseData = array('method_name'=> 'userVisibilitySetting','status'=>'1','response_code'=>'200', 'data'=>$resultData, 'message'=> SUCCESS_607);
// 					}

// 			    }else{
// 			    	// $getUserStatusResponse = $this->getUserStatus($dataInput['user_id']);
// 			     //     $responseData = array('method_name'=> 'userVisibilitySetting','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
// 			    	$responseData = array('method_name'=> 'userVisibilitySetting','status'=>'0','response_code'=>'602','message'=> ERROR_602);
// 		         }
// 	       }else{
// 			$responseData = array('method_name'=> 'userVisibilitySetting','status'=>'0','response_code'=>'611','message'=> ERROR_611);
// 	    }
// 	    echo json_encode($responseData);
//     	exit;
// 	}

/*
-------------------------------------------------------------------------
Method Name:userVisibilitySetting
Developer:Shashi
ON: 03-01-2017
I/P: JSON (user_id, user_email_visible_setting,user_phone_visible_setting)
O/P: JSON (success/fail)
Desc: Used to set the visibility of the user to other user by mean of email and phone.
-------------------------------------------------------------------------
*/
	public function userVisibilitySetting(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() && $this->validateAccessKey() ){

					if($dataInput['visibility_setting_type']=="Mobile"){
						//get count if exist //
						$condUserExist = array('user_id'=>$dataInput['user_id'],'visibility_setting_type'=>'Mobile');
						$dataExist = $this->UserVisibilitySetting->find('count',array('conditions'=>$condUserExist));
						if($dataExist > 0){ // Update

							$userVisibilityInfo = $this->UserVisibilitySetting->find( "first", array("conditions"=> $condUserExist ));

							//** Update Activity Logs[START]
						$localServerInfo = $this->Common->getFrontServerInfo();
							$activityLogData = array(
									"user_id"=> $dataInput['user_id'],
									"item_id"=> 0,
									"activity_type"=> "UserVisibilitySetting",
									"message"=> "",
									"old_info"=> $this->Common->arrayToJson($userVisibilityInfo),
									"log_info"=> $localServerInfo,
								);
							$activityLog = $this->ActivityLog->save($activityLogData);



							$this->UserVisibilitySetting->updateAll(array('visibility_setting_value'=>$dataInput['visibility_setting_value'],'updated_date'=>'"'.date("Y-m-d H:i:s").'"'),$condUserExist);
						}else{ // Insert

							$this->UserVisibilitySetting->save(array(
									'id'=>'',
									'user_id'=>$dataInput['user_id'],
									'visibility_setting_type'=>$dataInput['visibility_setting_type'],
									'visibility_setting_value'=>$dataInput['visibility_setting_value'],
									'created_date'=>date("Y-m-d H:i:s"),
									'updated_date'=>date("Y-m-d H:i:s")

								));

						}
					 $dateResponse = $this->UserVisibilitySetting->find('all',array('conditions'=>array('user_id'=>$dataInput['user_id'],'visibility_setting_type'=>'Mobile')));

					 $resultData = array('visibility_setting_type'=>'Mobile','visibility_setting_value'=>$dateResponse[0]['UserVisibilitySetting']['visibility_setting_value']);

					 $responseData = array('method_name'=> 'userVisibilitySetting','status'=>'1','response_code'=>'200', 'data'=>$resultData, 'message'=> SUCCESS_607);

					}else if($dataInput['visibility_setting_type']=="Email"){
						//get count if exist //
						$condUserExist = array('user_id'=>$dataInput['user_id'],'visibility_setting_type'=>'Email');
						$dataExist = $this->UserVisibilitySetting->find('count',array('conditions'=>$condUserExist));
						if($dataExist > 0){ // Update

							$userVisibilityInfo = $this->UserVisibilitySetting->find( "first", array("conditions"=> $condUserExist ));

							//** Update Activity Logs[START]
						$localServerInfo = $this->Common->getFrontServerInfo();
							$activityLogData = array(
									"user_id"=> $dataInput['user_id'],
									"item_id"=> 0,
									"activity_type"=> "UserVisibilitySetting",
									"message"=> "",
									"old_info"=> $this->Common->arrayToJson($userVisibilityInfo),
									"log_info"=> $localServerInfo,
								);
							$activityLog = $this->ActivityLog->save($activityLogData);



							$this->UserVisibilitySetting->updateAll(array('visibility_setting_value'=>$dataInput['visibility_setting_value'],'updated_date'=>'"'.date("Y-m-d H:i:s").'"'),$condUserExist);
						}else{ // Insert



							$this->UserVisibilitySetting->save(array(
									'id'=>'',
									'user_id'=>$dataInput['user_id'],
									'visibility_setting_type'=>$dataInput['visibility_setting_type'],
									'visibility_setting_value'=>$dataInput['visibility_setting_value'],
									'created_date'=>date("Y-m-d H:i:s"),
									'updated_date'=>date("Y-m-d H:i:s")

								));
						}
					 $dateResponse = $this->UserVisibilitySetting->find('all',array('conditions'=>array('user_id'=>$dataInput['user_id'],'visibility_setting_type'=>'Email')));

					 $resultData = array('visibility_setting_type'=>'Email','visibility_setting_value'=>$dateResponse[0]['UserVisibilitySetting']['visibility_setting_value']);

					 $responseData = array('method_name'=> 'userVisibilitySetting','status'=>'1','response_code'=>'200', 'data'=>$resultData, 'message'=> SUCCESS_607);

					}else{
					  $resultData = array('visibility_setting_type'=>'Mobile','visibility_setting_value'=>array());
					  $responseData = array('method_name'=> 'userVisibilitySetting','status'=>'1','response_code'=>'200', 'data'=>$resultData, 'message'=> SUCCESS_607);
					}

			    }else{
			    	// $getUserStatusResponse = $this->getUserStatus($dataInput['user_id']);
			     //     $responseData = array('method_name'=> 'userVisibilitySetting','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
			    	$responseData = array('method_name'=> 'userVisibilitySetting','status'=>'0','response_code'=>'602','message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'userVisibilitySetting','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
	    echo json_encode($responseData);
    	exit;
	}
/*
-------------------------------------------------------------------------
Method Name:userVisibilitySetting
Developer:Shashi
ON: 03-01-2017
I/P: JSON (user_id, user_email_visible_setting,user_phone_visible_setting)
O/P: JSON (success/fail)
Desc: Used to set the visibility of the user to other user by mean of email and phone.
-------------------------------------------------------------------------
*/
public function userVisibilitySettingForMobileEmail($user_id=NULL){
	$returnArray=array();
	// Email
	$userEmail = $this->UserVisibilitySetting->find('all',array('conditions'=>array('user_id'=>$user_id , 'visibility_setting_type'=>'Email')));
	if(!empty($userEmail)){
		$returnArray['Email']=$userEmail[0]['UserVisibilitySetting']['visibility_setting_value'];
	}else{
		$returnArray['Email']='2';
	}
	$userMobile = $this->UserVisibilitySetting->find('all',array('conditions'=>array('user_id'=>$user_id , 'visibility_setting_type'=>'Mobile')));
	// Mobile
	if(!empty($userMobile)){
		$returnArray['Mobile']=$userMobile[0]['UserVisibilitySetting']['visibility_setting_value'];
	}else{
		$returnArray['Mobile']='2';
	}
	return $returnArray;
}
/*
-------------------------------------------------------------------------
Method Name:isYourColleague
Developer:Shashi
ON: 03-01-2017
I/P: JSON (user_id, user_email_visible_setting,user_phone_visible_setting)
O/P: JSON (success/fail)
Desc: Used to set the visibility of the user to other user by mean of email and phone.
-------------------------------------------------------------------------
*/
public function isYourColleague($loggedin_user_id,$user_id){
	$returnArray=array();
	// Check condition for colleauge
	$first = 0;
	$second = 0;
	$UserColleague1 = $this->UserColleague->find('all',array('conditions'=>array('user_id'=>$loggedin_user_id , 'colleague_user_id'=>$user_id, 'status'=>'1')));
	if(!empty($UserColleague1)){
		$first=1;
	}
	$UserColleague2 = $this->UserColleague->find('all',array('conditions'=>array('user_id'=>$user_id , 'colleague_user_id'=>$loggedin_user_id, 'status'=>'1')));
	if(!empty($UserColleague2)){
		$second=1;
	}

	$third = $first+$second;
	if($third > 0){
		$colleaugeValue =  'yes';
	}else{
		$colleaugeValue =  'no';
	}

	return $colleaugeValue;
}

/*
		Test Login for QB
	*/
		public function loginToQb(){
		$this->autoRender = false;
		$responseData = array();
		$requestEmail = str_replace('â€', "'", strtolower($_REQUEST['email']));
		$userDetails = array();
		$params = array();
		if( $this->UserDevice->find("count", array("conditions"=> array("token"=> $_REQUEST['password'], "status"=>1))) >0 ){
			$params = array('email'=> $requestEmail);
		}
		else if( $this->UserOneTimeToken->find("count", array("conditions"=> array("token"=> $_REQUEST['password'], "status"=> 1))) >0 ){
			$params = array('email'=> $requestEmail);
			//** Inactive token after used[START]
			$this->UserOneTimeToken->updateAll(array("status"=> 0),array("token"=> $_REQUEST['password']));
			//** Inactive token after used[END]
		}
		if(!empty($params)){
			$userDetails = $this->User->userDetails( $params );
		}

		if(!empty($userDetails)){
		$data = array(
				'id'=>(int) $userDetails['User']['id'],
			    'external_user_id'=>(int) $userDetails['User']['id'],
				'login'=> stripslashes($userDetails['User']['email']),
				'email'=> stripslashes($userDetails['User']['email']),
				'full_name'=> stripslashes($userDetails['UserProfile']['first_name']).' '.stripslashes($userDetails['UserProfile']['last_name'])

			);
		$responseData['user'] = $data;
		//header("Status: 200");
		http_response_code(200);
	     }else{
	     	//$data = array();
			//$responseData['user'] = $data;
			$responseData = "Invalid token";
			//header("Status: 422");
			http_response_code(422);
	     }

		echo json_encode($responseData);

		//** Store Request on temp table[START]
		    $requestVal = json_encode(array('email'=> $requestEmail,'password'=> $_REQUEST['password']));
		    $responseVal = json_encode($responseData);
			App::import('model','User');
			$check = new User();
			$addRequestQuer = "INSERT INTO test_cip_logins (request, response) VALUES ('".addslashes($requestVal)."', '".addslashes($responseVal)."') ";
			$addRequestResponse = $check->query($addRequestQuer);
		//** Store Request on temp table[END]
		exit;
	}

	/*
	------------------------------------------------------------------------------------
	On: 14-02-2017
	I/P:
	O/P:
	Desc: Add user Institution(Almost same as profile edit on OCR employment edit)
	------------------------------------------------------------------------------------
	*/
	public function addUserInstitution(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
			$userStatusDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			if($userStatusDetails['User']['status']==1 && $userStatusDetails['User']['approved']==1)
			{
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					//** Add User Institution[START]
					if(!empty($dataInput['Company'])){
						foreach( $dataInput['Company'] as $company ){
							$profileData = array();
							if(!empty($company['company_id'])){
								//** Remove all is_current company before edit[START]
								try{
									$this->UserEmployment->updateAll( array("is_current"=> 0) , array("user_id"=> $dataInput['user_id']));
								}catch(Exception $e){}
								//** Remove all is_current company before edit[END]
								$company['designation_id'] = !empty($company['Designation']['designation_id']) ? $company['Designation']['designation_id'] : 0 ;
								if( empty($company['id']) ){
									$profileData = array("user_id"=> $dataInput['user_id'], "company_id"=> $company['company_id'], "from_date"=> isset($company['from_date']) ? date('Y-m-d', strtotime($company['from_date']))  : '0000-00-00', "to_date"=> isset($company['to_date']) ? date('Y-m-d', strtotime($company['to_date']))  : '0000-00-00', "location"=> isset($company['location']) ? $company['location']  : '', "description"=> isset($company['description']) ? $company['description']  :'', "is_current"=> 1,"designation_id"=> $company['designation_id']);
									$this->UserEmployment->saveAll( $profileData );
								}else{
									$fromDate = "0000-00-00"; $toDate = "0000-00-00"; $location = ""; $description = "";
									$profileData = array("user_id"=> $dataInput['user_id'], "company_id"=> $company['company_id'], "from_date"=> isset($company['from_date']) ? "'". date('Y-m-d', strtotime($company['from_date'])) ."'"  : "'".$fromDate."'", "to_date"=> isset($company['to_date']) ? "'". date('Y-m-d', strtotime($company['to_date'])) ."'"  : "'".$toDate."'", "location"=> isset($company['location']) ? "'". $company['location']. "'"  : "'". $location ."'", "description"=> isset($company['description']) ? "'". $company['description']. "'"  : "'". $description ."'", "is_current"=> 1,"designation_id"=> isset($company['designation_id']) ? $company['designation_id'] : 0);
									$this->UserEmployment->updateAll( $profileData , array("id"=> $company['id']));
								}
								//** Add/update ON duty[START]
								if($company['is_current'] == 1){ //** If current company add duty value (ON/OFF)
										$previousCompanyId = !empty($company['previous_company_id']) ? $company['previous_company_id'] :0;
										$currentCompanyId = !empty($company['company_id']) ? $company['company_id']: 0;
										if( $this->UserDutyLog->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id']))) == 0){
											$dutyData = array("user_id"=> $dataInput['user_id'], "hospital_id"=> $company['company_id'], "status"=> $company['duty_status']);
											$this->UserDutyLog->saveAll($dutyData);
										}else{
											$this->UserDutyLog->updateAll(array("hospital_id"=> $company['company_id'], "modified"=> "'".date('Y-m-d H:i:s')."'", "status"=> $company['duty_status']), array("user_id"=> $dataInput['user_id']));
										}

								}
							}
						}
						//** Update free/enterprise user date[START]
						$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
						$companySubscriptionData=$this->CompanyName->find("first", array("conditions"=> array("id"=>$currentCompanyId, "status"=>1,"is_subscribed"=>1)));
							if(!empty($companySubscriptionData)){
							//** Set email in user enterprise list
							$checkUserAdded = $this->EnterpriseUserList->find("count", array("conditions"=> array('email'=> stripslashes($userDetails['User']['email']),'company_id'=>$currentCompanyId)));
							if($checkUserAdded == 0){
								$this->EnterpriseUserList->save(array('email'=> stripslashes($userDetails['User']['email']),'company_id'=>$currentCompanyId, 'status'=> 0));
							}
						}
						//** Previous company paid check[START]
						$dataParamsPrevious['company_id'] = isset($previousCompanyId) ? $previousCompanyId : 0;
						$dataParamsPrevious['email'] = $userDetails['User']['email'];
						$checkUserForPaidPreviousCompany = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParamsPrevious);
						//** Previous company paid check[END]

						//** Current company paid check[START]
						$dataParams['company_id'] = $currentCompanyId;
						$dataParams['email'] = $userDetails['User']['email'];
						$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);
						//** Current Company paid check[END]
						$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						if(!empty($checkUserForPaid)){
							if(!empty($checkUserSubscribed)){
								$this->UserSubscriptionLog->updateAll(array("subscribe_type"=>1,"company_id"=> $currentCompanyId, "subscription_date"=> "'".$checkUserForPaid['CompanyName']['subscription_date']."'", "subscription_expiry_date"=> "'".$checkUserForPaid['CompanyName']['subscription_expiry_date']."'"), array("user_id"=> $dataInput['user_id']));
							}else{
								$subscriptionData = array("subscribe_type"=>1,"user_id"=> $dataInput['user_id'],"company_id"=> $currentCompanyId, "subscription_date"=> "'".$checkUserForPaid['CompanyName']['subscription_date']."'", "subscription_expiry_date"=> $checkUserForPaid['CompanyName']['subscription_expiry_date']);
								$this->UserSubscriptionLog->save($subscriptionData);
							}
						}else{
							//** For new user setting first time institution[START]
								$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
								if(empty($checkUserSubscribed)){
									$subscriptionStartDate = date('Y-m-d 23:59:59');
									$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_TRIAL_PERIOD.' days'));
									$subscriptionDataVals = array("user_id"=> $userDetails['User']['id'],"company_id"=> 0, "subscription_date"=> $subscriptionStartDate, "subscription_expiry_date"=> $subscriptionExpireDate);
									$this->UserSubscriptionLog->save($subscriptionDataVals);
								}
							//** For new user setting first time institution[END]

							//** User edit institution paid to free update grace period[START]
							if(!empty($checkUserForPaidPreviousCompany) && empty($checkUserForPaid)){
								$subscriptionStartDate = date('Y-m-d 23:59:59');
								$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
								if(!empty($checkUserSubscribed)){
									$this->UserSubscriptionLog->updateAll(array("company_id"=>$currentCompanyId, "subscribe_type"=>2, "subscription_date"=> "'".$subscriptionStartDate."'", "subscription_expiry_date"=> "'".$subscriptionExpireDate."'"), array("user_id"=> $dataInput['user_id']));
								}else{
									$subscriptionData = array("user_id"=> $dataInput['user_id'],"company_id"=> $currentCompanyId,"subscribe_type"=>2, "subscription_date"=> "'".$subscriptionStartDate."'", "subscription_expiry_date"=> $subscriptionExpireDate);
									$this->UserSubscriptionLog->save($subscriptionData);
								}
							}
							//** User edit institution paid to free update grace period[END]
						}
						//** Update free/enterprise user date[END]
						//** Get user Enterprise data[START]
						$status = 0;$expiryDate = 0;
						$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						//** When user comes after trust expiry date update grace period[START]
						$currentDate = date('Y-m-d 23:59:59');
						$gracePeriod = date('Y-m-d 23:59:59', strtotime($currentDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
						$trustExpiryDate = $checkUserSubscribed['UserSubscriptionLog']['subscription_expiry_date'];
						if(strtotime($currentDate) > strtotime($trustExpiryDate) && !empty($checkUserForPaid) && ($checkUserSubscribed['UserSubscriptionLog']['subscribe_type']==2)){
							$this->UserSubscriptionLog->updateAll(array("subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $dataInput['user_id']));
						}
						//** When user comes after trust expiry date update grace period[END]
						//** Get user SUBSCRIPTION status
						$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						if(!empty($checkUserSubscribedFinal)){
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
							$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
						}
						//** In Case of grace period oncall should be off[START]
						if($status==2){
							$this->UserDutyLog->updateAll(array("status"=> 0), array("user_id"=> $dataInput['user_id'], "hospital_id"=> $currentCompanyId));
						}
						//** In Case of grace period oncall should be off[END]
						$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate) > 0) ? strtotime($expiryDate):0);
						//** Get user Enterprise data[END]
						//** Add company details[START]
						$companyName = "";
						$employment = $this->UserEmployment->find("first", array("conditions"=> array("company_id"=> $currentCompanyId, "is_current"=> 1)));
						$companyNameData = $this->CompanyName->find("first", array("conditions"=> array("id"=> $currentCompanyId)));
						if(!empty($companyNameData)) {$companyName = $companyNameData['CompanyName']['company_name']; }
						$isCompanyAssignPending = 0;
						if($currentCompanyId !=1){
							if($companyNameData['CompanyName']['is_subscribed'] == 1 && $companyNameData['CompanyName']['status']==1){
								$isCompanyAssignCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $userDetails['User']['email'], "company_id"=> $currentCompanyId, "status"=> 0)));
							}
						}
						if($isCompanyAssignCheck > 0){ $isCompanyAssignPending = 1; }
						$userEmploymentData = array("id"=> !empty($employment['UserEmployment']['id']) ? (int) $employment['UserEmployment']['id'] : (int) 0, "company_id"=> $currentCompanyId ,"company_name"=> $companyName,"is_current"=> $employment['UserEmployment']['is_current'],"is_company_approval_pending"=> $isCompanyAssignPending);
						//** Add company details[END]
						//$settingData['settings'] = array($subscriptionData);
						$data = array('settings'=> array($subscriptionData), 'Company'=> array($userEmploymentData));
						$responseData = array('method_name'=> 'addUserInstitution', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $data);

						//** For first time free user send mail to sales[START]
						$checkFinalUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						$chkAlreadySent = $this->EmailSmsLog->find("count", array("conditions"=>array("type"=> "email", "user_id"=> $dataInput['user_id'], "notify_type"=> "Free Trial User Mail to sales")));
						if(($chkAlreadySent == 0) && ($checkFinalUserSubscribed['UserSubscriptionLog']['subscribe_type'] == 0)){
							$mailParams = array();
							$mailParams['name'] = stripslashes($userDetails['UserProfile']['first_name']).' '.stripslashes($userDetails['UserProfile']['last_name']);
							$professionData = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
							$mailParams['profession'] = !empty($professionData['Profession']['profession_type']) ? $professionData['Profession']['profession_type'] :'N/A';
							$companyData = $this->CompanyName->find("first", array("conditions"=> array("id"=> $currentCompanyId)));
							$mailParams['institution_name'] = !empty($companyData['CompanyName']['company_name']) ? $companyData['CompanyName']['company_name'] : 'N/A';
							$mailParams['user_email'] = $userDetails['User']['email'];
							$mailParams['user_phone_number'] = !empty($userDetails['UserProfile']['contact_no']) ? $userDetails['UserProfile']['contact_no'] :'N/A';
							$mailParams['registration_date'] = substr($userDetails['User']['registration_date'],0,10);
							$mailToSales = $this->MbEmail->sendMailToSalesFreeEnterpriseUser($mailParams);
							$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailToSales, "user_id"=> $dataInput['user_id'], "notify_type"=> "Free Trial User Mail to sales") );
						}
						//** For first time free user send mail to sales[END]

					}
					//** Add User Institution[END]

				}else{
					$responseData = array('method_name'=> 'addUserInstitution', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
				}
			}
			else
			{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
				$responseData = array('method_name'=> 'settings','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'addUserInstitution', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------------
	On: 16-02-2017
	I/P:
	O/P:
	Desc: User list of same hospital(institution, organization) and ON/OFF duty
	------------------------------------------------------------------------------------
	*/
	public function oncallUserList(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
					$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
					$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId)));
					$params['loggedin_user_countryid'] = $userDetails['UserProfile']['country_id'];
					$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
					if($loggedinUserId == 2){ //** just to access only for sunnys@theoncallroom.com a/c to wsh for sometime
						$params['user_current_company_id'] = 336;
					}else{
						$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
					}
					if($userDetails['User']['approved'] == 2){
						$responseData = array('method_name'=> 'oncallUserList','status'=>'0','response_code'=>'655','message'=> ERROR_655);
					}else{
					//** On Duty Users
					$userLists = array();
					if($params['user_current_company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
						$userLists = $this->User->oncallUserLists( $params, $loggedinUserId );
					}
					$formattedUsers = $this->sameOrganizationUserFieldsFormat( $userLists , $loggedinUserId );
					//echo "<pre>"; print_r($userLists);die;
					$onDuty = array(); $offDuty = array(); $offDutyAtwork = array(); $offDutyNotAtwork = array();
					$offdutyarray = array();
					foreach($formattedUsers as $ul){
						if($ul['on_duty'] == 1){
							// test is_current is 0 or not if is current is 0 then not put in list Shashi 1-3-17
							$isCurrent = $this->UserEmployment->find('all',array('conditions'=>array('UserEmployment.user_id'=>$ul['user_id'],'UserEmployment.is_current'=>'1')));
							if(!empty($isCurrent)){
								$onDuty[] = $ul;
							}
						}else{
							// test is_current is 0 or not if is current is 0 then not put in list Shashi 1-3-17

							$isCurrent = $this->UserEmployment->find('all',array('conditions'=>array('UserEmployment.user_id'=>$ul['user_id'],'UserEmployment.is_current'=>'1')));
							if(empty($isCurrent)){

							}else{
								if($ul['at_work'] == 1)
								{
									$offDutyAtwork[] = $ul;
								}
								else
								{
									$offDutyNotAtwork[] = $ul;
								}
							}

						}
					}
					$userData['on_duty_users'] = $onDuty;
					if(!empty($offDutyAtwork) && !empty($offDutyNotAtwork))
					{
						$offdutyarray = array_merge($offDutyAtwork,$offDutyNotAtwork);
					}
					elseif(!empty($offDutyAtwork)){
					    $offdutyarray = $offDutyAtwork;
					}
					else
					{
					    $offdutyarray = $offDutyNotAtwork;
					}
					$userData['off_duty_users'] = $offdutyarray;
					$responseData = array('method_name'=> 'oncallUserList','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData);
			   	}
			    }else{
			    	$getUserStatusResponse = $this->getUserStatus($dataInput['user_id']);
			         $responseData = array('method_name'=> 'oncallUserList','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
		         }
	       }else{
			$responseData = array('method_name'=> 'oncallUserList','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	--------------------------------------------------------------------------
	On: 20-02-2017
	I/P: JSON
	O/P: JSON, user lists
	Desc: Fetching user list by pagination with search
	--------------------------------------------------------------------------
	*/
	public function sameOrganizationUserFieldsFormat( $userData = array(), $loggedinUserId = NULL ){
		$userDataList = array();
		if( !empty($userData) ){
		//** User Data
			foreach( $userData as $ud ){
				//** Find user colleague with logged in user
				$colleagueStatus = 0; $requestType = '';
				$myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'])));
				$meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId)));
				if( !empty($myColleague) ){
					$colleagueStatus = $myColleague['UserColleague']['status'];
					$requestType = "from_me";
				}elseif( !empty($meColleague) ){
					$colleagueStatus = $meColleague['UserColleague']['status'];
					$requestType = "to_me";
				}
				//** Check loggedin user follows or not
				$isFollow = 0;
				$conditions = array("followed_by"=> $loggedinUserId, "followed_to"=> $ud['User']['id'], "follow_type"=> 1, "status"=> 1);
				$followCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
				if( $followCheck > 0) { $isFollow = 1; }
				//** Get Country name
				$countryDetails = $this->Country->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['country_id'])));
				//** Get Profession
				$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['profession_id'])));
				//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
				$qbInfo = array("id"=> $qbId);
				//** Get QB details [END]
				//** Get role status [START]
				$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				//** Get role status [END]
				//** Get Current Employment[START]
				$userCurrentCompanyName = "";
				$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "is_current"=> 1)));
				$companyId = $userCurrentCompany['UserEmployment']['company_id'];
				if($companyId == -1){
					$userCurrentCompanyName = "None";
				}else if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
					$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
					if(!empty($userCompanyDetails)){
						$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
					}
				}
				//** Get Current Employment[END]
				//** On call Access profession wise[START]
				$oncallAccess = 0;
				$professionIds = array(1,2,3,8);
				if(in_array($ud['UserProfile']['profession_id'], $professionIds)){
					$oncallAccess = 1;
				}
				//** On call Access profession wise[END]
				if(!empty($qbId)){
				$userDataList[] = array(
								'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
								'email'=>	!empty($ud['User']['email']) ? $ud['User']['email']:'',
								'last_loggedin_date'=> !empty($ud['User']['last_loggedin_date']) ? $ud['User']['last_loggedin_date']:'',
								'status' => !empty($ud['User']['status']) ? (string) $ud['User']['status']:'',
								'registration_date'=> !empty($ud['User']['registration_date']) ? (string) $ud['User']['registration_date']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'country'=> !empty($countryDetails['Country']['country_name']) ? $countryDetails['Country']['country_name']:'',
								'country_code'=> !empty($countryDetails['Country']['country_code']) ? $countryDetails['Country']['country_code']:'',
								'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
								'city'=> !empty($ud['UserProfile']['city']) ? $ud['UserProfile']['city']:'',
								'profession'=> !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type']:'',
								'gmcnumber'=> !empty($ud['UserProfile']['gmcnumber']) ? $ud['UserProfile']['gmcnumber']:'',
								'contact_no'=> !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'',
								'address'=> !empty($ud['UserProfile']['address']) ? $ud['UserProfile']['address']:'',
								'dob'=> (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'',
								'year_of_graduation'=> !empty($ud['UserProfile']['year_of_graduation']) ? $ud['UserProfile']['year_of_graduation']:'',
								'gender'=> !empty($ud['UserProfile']['gender']) ? $ud['UserProfile']['gender']:'',
								//'current_employment'=> !empty($ud['UserProfile']['current_employment']) ? $ud['UserProfile']['current_employment']:'',
								'current_employment'=> $userCurrentCompanyName,
								'institution_name'=> !empty($ud['UserProfile']['institution_name']) ? $ud['UserProfile']['institution_name']:'',
								'Colleague'=> array("status"=> $colleagueStatus, "request_by"=> $requestType),
								'is_follow'=> $isFollow,
								'qb_details'=> $qbInfo,
								"role_status"=> !empty($userRoleStatusData['UserProfile']['role_status']) ? $userRoleStatusData['UserProfile']['role_status']:'',
								"on_duty"=> !empty($ud['UserDutyLog']['status']) ? $ud['UserDutyLog']['status'] : 0,
								"at_work"=> !empty($ud['UserDutyLog']['atwork_status']) ? $ud['UserDutyLog']['atwork_status'] : 0,
								"oncall_access"=> $oncallAccess
								);
				}
			}
		}
		return $userDataList;
	}

	/*
	-------------------------------------------------------------------------------
	On: 28-03-2017
	I/P: JSON
	O/P: JSON
	Desc: Setting values like CIP on/off
	-------------------------------------------------------------------------------
	*/
	public function settings(){
		$this->autoRender = false;
		$responseData = array();
		$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$t1 = explode(" ",microtime());
		$milisec1 = str_replace('.', '', substr((string)$t1[0],1,4));
		$response_message = SUCCESS_611;
		$response_code = "611";
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$userStatusDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			// if($userStatusDetails['User']['status']==1 && $userStatusDetails['User']['approved']==1)
			if($userStatusDetails['User']['status']==1)
			{
				if($this->validateAccessKey()){
					if($this->User->findById( $dataInput['user_id'] ) )
					{
						$headerVals = $this->getHeaderValues();
						$dataInput["token"] = $headerVals['token'];
						//** Check alraedy logout from particular device[START]
						$forceLogoutSettingVal = "1";
						if(!empty($dataInput["token"])){
							$checkToken = $this->UserDevice->find("count", array("conditions"=> array("user_id"=> $dataInput["user_id"],"token"=> $dataInput["token"], "status"=> 1)));
							if($checkToken > 0){
								$forceLogoutSettingVal = "0";
							}
						}
						$forceLogoutSettingValues = array("status"=> $forceLogoutSettingVal, 'type'=>'FORCE_LOGOUT');
						//** Check alraedy logout from particular device[END]

						//** Get user Enterprise data[START]
						$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput["user_id"])));
						$dataParams['email'] = stripslashes($userDetails['User']['email']);
						$currentCompanyId = 0;
						$currentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"], "is_current"=> 1, "status"=> 1)));
						if(!empty($currentCompany)){ $currentCompanyId = $currentCompany['UserEmployment']['company_id']; }
						$dataParams['company_id'] = $currentCompanyId;
						$dataParams['email'] = stripslashes($dataParams['email']);
						$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);
						$status = 0;$expiryDate = 0;
						$checkUserSubscribedExistingUser = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"])));
						//** For existing user prior to enterprise feature add enterprise data first time[START]
						if(empty($checkUserSubscribedExistingUser)){
							if(empty($checkUserForPaid)){
								$subscriptionStartDate = date('Y-m-d 23:59:59');
								$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_TRIAL_PERIOD.' days'));
							}else{
								$subscriptionStartDate = $checkUserForPaid['CompanyName']['subscription_date'];
								$subscriptionExpireDate = $checkUserForPaid['CompanyName']['subscription_expiry_date'];
							}
							$subscriptionDataVals = array("user_id"=> $dataInput["user_id"],"company_id"=> $currentCompanyId, "subscription_date"=> $subscriptionStartDate, "subscription_expiry_date"=> $subscriptionExpireDate);
							$this->UserSubscriptionLog->save($subscriptionDataVals);
						}
						//** For existing user prior to enterprise feature add enterprise data first time[END]

//------ DEEPAK ---- START (commented to restrict grace)
						//** Get updated user enterprise values[START]
						// $checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"])));
						// //** Get updated user enterprise values[START]
						// //** When user comes after trust expiry date update grace period[START]
						// $currentDate = date('Y-m-d 23:59:59');
						// $gracePeriod = date('Y-m-d 23:59:59', strtotime($currentDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
						// $trustExpiryDate = $checkUserSubscribed['UserSubscriptionLog']['subscription_expiry_date'];
						// if(strtotime($currentDate) > strtotime($trustExpiryDate) && !empty($checkUserForPaid) && ($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] != 2)){
						// 	$this->UserSubscriptionLog->updateAll(array("company_id"=>$userData['Company']['company_id'], "subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $dataInput["user_id"]));
						// }elseif($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] == 1 && empty($checkUserForPaid)){
						// 	$this->UserSubscriptionLog->updateAll(array("company_id"=>$userData['Company']['company_id'], "subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $dataInput["user_id"]));
						// }
//------ DEEPAK ---- END
						//** When user comes after trust expiry date update grace period[END]
						//** Update user expiration[START]
						$checkUserSubscribedExpire = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"])));
						if(!empty($checkUserSubscribedExpire)){
							$paramData['subscription_expiry_date'] = $checkUserSubscribedExpire['UserSubscriptionLog']['subscription_expiry_date'];//$checkUserForPaid['CompanyName']['subscription_expiry_date'];
							$isExpired = $this->UserSubscriptionLog->checkSusbcriptionExpired($paramData);
							if($isExpired){
								$this->UserSubscriptionLog->updateAll(array("company_id"=>$userData['Company']['company_id'],  "subscribe_type"=> 3),array("user_id"=> $userDetails['User']['id']));
							}
						// DEEPAK --- change enterprise user list status in case of non paid user
							if($checkUserSubscribedExpire['UserSubscriptionLog']['subscribe_type'] != 1){
								$this->EnterpriseUserList->updateAll(array("status"=> 0), array('email'=>stripslashes($dataParams['email']),'company_id'=>$userData['Company']['company_id']));
							}
						}
						//** Update user expiration[END]
						//** Get user SUBSCRIPTION status
						$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"])));
						if(!empty($checkUserSubscribedFinal)){
							//if(!empty($checkUserForPaid)) { $status = 1; }
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
							$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
							//** Check if expired
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
						}
							//** In Case of grace period oncall should be off[START]
						if($status==2){
							$this->UserDutyLog->updateAll(array("status"=> 0), array("user_id"=> $dataInput["user_id"], "hospital_id"=> $currentCompanyId));
						}
						//** In Case of user expired update cache[START]
						if($status==3){
							$forceLogoutSettingValues = array("status"=> '1', 'type'=>'FORCE_LOGOUT');
							$response_message = ERROR_665;
							$response_code = "665";
							$userDataParams['company_id'] = isset($currentCompanyId) ? $currentCompanyId : 0;
							$userDataParams['user_id'] = isset($dataInput["user_id"]) ? $dataInput["user_id"]: 0;
							$updateStaticCache = $this->saveSettingStaticData($userDataParams);
						}

						//** In Case of user expired update cache[END]

						//** In Case of grace period oncall should be off[END]
						$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate)>0) ? strtotime($expiryDate):0);
						$settingData = array($forceLogoutSettingValues, $subscriptionData);
						//** Get user Enterprise data[END]


						//** Add company details[START]
						$companyName = "";
						$companyNameData = $this->CompanyName->find("first", array("conditions"=> array("id"=> $currentCompanyId)));
						if(!empty($companyNameData)) {
							$companyName = $companyNameData['CompanyName']['company_name'];
							$patientNumberType = $companyNameData['CompanyName']['patient_number_type'];
						}
						$isCompanyAssignPending = 0;
						$isCompanyAssignCheck = 0;
						if($currentCompanyId !=1){
							if($companyNameData['CompanyName']['is_subscribed'] == 1 && $companyNameData['CompanyName']['status']==1){
								$isCompanyAssignCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $userDetails['User']['email'], "company_id"=> $currentCompanyId, "status"=> 0)));
							}
						}
						if($isCompanyAssignCheck > 0){ $isCompanyAssignPending = 1; }
						$userEmploymentData = array("id"=> !empty($currentCompany['UserEmployment']['id']) ? (int) $currentCompany['UserEmployment']['id'] : (int) 0, "company_id"=> $currentCompanyId ,"company_name"=> $companyName, "patient_number_type"=>$patientNumberType,"is_current"=> $currentCompany['UserEmployment']['is_current'],"is_company_approval_pending"=> $isCompanyAssignPending);
						$userDndParams['user_id'] = $dataInput['user_id']; 
						$getUserDndStatus = $this->UserDndStatus->getUserDndStatus($userDndParams);
						$dndStatusOptions = $this->DndStatusOption->getDndStatusOptions($getUserDndStatus['UserDndStatus']['dnd_status_id']);
						$isDndActive = isset($getUserDndStatus['UserDndStatus']['is_active']) ? $getUserDndStatus['UserDndStatus']['is_active'] : 0;
						if(!empty($getUserDndStatus))
						{
							if($getUserDndStatus['UserDndStatus']['is_active'])
							{

								$userDndDetail = array('dnd_status_id'=>$getUserDndStatus['UserDndStatus']['dnd_status_id'], 'dnd_name'=>$dndStatusOptions['DndStatusOption']['dnd_name'],
									'icon_selected'=>$dndStatusOptions['DndStatusOption']['icon_selected'], 'end_time'=>$getUserDndStatus['UserDndStatus']['end_time']);
							}
							else
							{
								$userDndDetail = array();
							}
						}
						// ------ User Baton Count ------ [START]
						$userBatonRole_active = 0;
						$userBatonRole_request = 0;
						$batonCountVal = array();
						$userBatonRole_active = $this->UserBatonRole->find("count", array("conditions"=> array("from_user"=> $dataInput['user_id'], "is_active"=>1, "status"=>1), "group"=>array('UserBatonRole.role_id')));
						$userBatonRole_request = $this->UserBatonRole->find("count", array("conditions"=> array("from_user"=> $dataInput['user_id'], "is_active"=>1, "status !="=>1), "group"=>array('UserBatonRole.role_id')));

						$batonCountVal['active']  = ($userBatonRole_active > 0) ? $userBatonRole_active : 0;
						$batonCountVal['request']  = ($userBatonRole_request > 0) ? $userBatonRole_request : 0;
						// ------ User Baton Count ------ [END]
						// ------ User Permanent Count ------ [STARt]
						$userPermRole_active = 0;
						$userPermRole_request = 0;
						$permCountVal = array();
						$userPermRole_active = $this->UserPermanentRole->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "active"=>1, "status"=>1, "institute_id" => $currentCompanyId), "group"=>array('UserPermanentRole.id')));
						$userPermRole_request = $this->UserPermanentRole->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "active"=>1, "status !="=>1, "institute_id" => $currentCompanyId), "group"=>array('UserPermanentRole.id')));

						$permCountVal['active']  = ($userPermRole_active > 0) ? $userPermRole_active : 0;
						$permCountVal['request']  = ($userPermRole_request > 0) ? $userPermRole_request : 0;
						// ------ User Permanent Count ------ [END]



						// ------ User At Work Status ------ [START]
						$at_work_status = '0';
						$user_duty_data = $this->UserDutyLog->find('first',array('conditions'=>array("user_id"=> $dataInput["user_id"], "hospital_id"=> $currentCompanyId)));
						if(!empty($user_duty_data)){
							$at_work_status = $user_duty_data['UserDutyLog']['atwork_status'];
						}
						// ------ User At Work Status ------ [END]

						//** Add company details[END]

						// $responseData = array('method_name'=> 'settings', 'status'=>"1", 'response_code'=> "611", 'message'=> SUCCESS_611, 'data'=> array('settings'=> $settingData, 'Company'=>$userEmploymentData));
						// $checkToken = $this->UserLoginTransaction->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"],"token"=> $dataInput["token"])));
						$responseData = array(
							'method_name'=> 'settings',
							'status'=>"1",
							'response_code'=> $response_code,
							'message'=> $response_message,
							'data'=> array(
								'settings'=> $settingData,
								'Company'=>$userEmploymentData,
								"checkToken"=>$checkToken,
								'is_dnd_active'=>$isDndActive,
								'dnd_status' =>$userDndDetail,
								"user_baton_role_count"=>$batonCountVal,
								"user_perm_role_count"=>$permCountVal,
								"at_work_status" => $at_work_status
							)
						);
						// try{
						// 	if($forceLogoutSettingVal == "1")
						// 	{
						// 		$checkToken = $this->UserLoginTransaction->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"],"token"=> $dataInput["token"])));
						// 	}
						// 	$updateLoginTransaction = $this->userLoginLogoutTransaction($checkToken);
						// }catch(Exception $e){
						// 	$updateLoginTransaction = $e->getMessage();
						// }
					}
					else
					{
						$responseData = array('method_name'=> 'settings','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
					}

				}else{
					$responseData = array('method_name'=> 'settings','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
				}
			}
			else
			{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
				$responseData = array('method_name'=> 'settings','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'settings', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		//** Track API Request, Response[START]
		$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$t2 = explode(" ",microtime());
		$milisec2 = str_replace('.', '', substr((string)$t2[0],1,4));
		$trackData = array(
							"user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> ($milisec2 - $milisec1),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		//** Track API Request, Response[END]
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 29-03-2017
	I/P: JSON (user_id, old_password, new_password)
	O/P: JSON (Success/Fail)
	Desc: User can Reset password
	-----------------------------------------------------------------------------------------
	*/
	public function resetPassword(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if( $this->validateToken() && $this->validateAccessKey() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				if( $this->User->find( "count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0 ){
					//** Reset Password
					$userData = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
					if( $userData['User']['password'] == md5($dataInput['old_password']) ){
						try{

							$uppercase = preg_match('@[A-Z]@', $dataInput['new_password']);
							$lowercase = preg_match('@[a-z]@', $dataInput['new_password']);
							$number    = preg_match('@[0-9]@', $dataInput['new_password']);
							$specialChars = preg_match('@[^\w]@', $dataInput['new_password']);

							if($uppercase && $lowercase && $number && $specialChars && strlen($dataInput['new_password']) >= 8){
								$changePass = $this->User->updateAll( array("User.password"=> "'". md5($dataInput['new_password']) ."'" ), array("User.id"=> $dataInput['user_id']) );
								if( $changePass ){
									$responseData = array('method_name'=> 'resetPassword', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_608);
									$resetPassMailParams['toMail'] = $userData['User']['email'];
									$resetPassMailParams['name'] = stripslashes($userData['UserProfile']['first_name']).' '.stripslashes($userData['UserProfile']['last_name']);
									$this->MbEmail->resetPassword($resetPassMailParams);
									$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $changePass, "user_id"=> $userData['User']['id'], "notify_type"=> "MB Reset Password") );
								}else{
									$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
								}
							}else
							{
								$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "661", 'message'=> ERROR_661);
							}
						}catch( Exception $e ){
							$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
						}
					}else{
						$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "606", 'message'=> ERROR_606);
					}
				}else{
					$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$getUserStatusResponse = $this->getUserStatus($dataInput['user_id']);
				$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> $getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'resetPassword', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 29-03-2017
	I/P: $email
	O/P: Message Mail Sent
	Desc: Send mail with to reset password by the user.
	-------------------------------------------------------------------------------------
	*/
	public function forgotPassword( ){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if($this->validateAccessKey()){
				//** Get User Email
				$userEmail = strtolower($dataInput['email']);
				$result = $this->User->find('first', array('fields'=> array('User.id, User.email, User.status, User.approved, User.activation_key, UserProfile.first_name, UserProfile.last_name,UserProfile.contact_no'), 'conditions'=> array('User.email'=> $userEmail)));
				if($result['User']['email'] == $userEmail){
					$userActive = $result['User']['status'];
					$userApproved = $result['User']['approved'];
					if($userActive == 1 && $userApproved == 1 ){
					//** Set mail params
					$tokenToValidate = strtotime(date('Y-m-d H:i:s')) . rand(1000,100000);
					$params['name'] = stripslashes($result['UserProfile']['first_name']) . ' ' . stripslashes($result['UserProfile']['last_name']);
					$params['toMail'] = $userEmail;
					$params['forgotPasswordLink'] = MEDICBLEEP_BASE_URL.'resetPassword.php?user_id='.urlencode($result['User']['id']).'&activation_key='.urlencode($tokenToValidate);
					try{
						$mailSendCheck = $this->MbEmail->forgotPasswordMb( $params );
						if($mailSendCheck){
							$responseData = array('method_name'=> 'forgotPassword', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_606);
							//** Add Values to link expiration Model
							$linkExpireData = array("user_id"=>$result['User']['id'], "project"=>"MB","verification_token"=> $tokenToValidate, "user_local_info"=> $this->Common->getFrontServerInfo());
							$this->ForgotPasswordLinkExpire->save($linkExpireData);
						}else{
							$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "607", 'message'=> ERROR_607);
						}
						$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSendCheck, "user_id"=> $result['User']['id'], "notify_type"=> "MB Forgot Password") );
						// $this->sendSmsLinkForgotPassword($params['forgotPasswordLink'],$result);
					}catch(Exception $e){
						$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
					}
					}else{
					$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "647", 'message'=> ERROR_647);
				}
				}else{
					$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "610", 'message'=> ERROR_610);
				}
			}else{
				$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 19-02-2019
	I/P: $user_id
	O/P: Message Mail Sent
	Desc: Send mail with to reset password by the user from Reset password screen medicbleep.com.
	-------------------------------------------------------------------------------------
	*/
	public function ResendForgotPassword( ){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			//** Get User Email
			$userId = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : "";
			// $userId = $dataInput['user_id'];
			$result = $this->User->find('first', array('fields'=> array('User.id, User.email, User.status, User.approved, User.activation_key, UserProfile.first_name, UserProfile.last_name,UserProfile.contact_no'), 'conditions'=> array('User.id'=> $userId)));
			if($result['User']['id'] == $userId){
				$userActive = $result['User']['status'];
				$userEmail = $result['User']['email'];
				$userApproved = $result['User']['approved'];
				if($userActive == 1 && $userApproved == 1 ){
					//** Set mail params
					$tokenToValidate = strtotime(date('Y-m-d H:i:s')) . rand(1000,100000);
					$params['name'] = stripslashes($result['UserProfile']['first_name']) . ' ' . stripslashes($result['UserProfile']['last_name']);
					$params['toMail'] = $userEmail;
					$params['forgotPasswordLink'] = MEDICBLEEP_BASE_URL.'resetPassword.php?user_id='.urlencode($result['User']['id']).'&activation_key='.urlencode($tokenToValidate);
					try{
						$mailSendCheck = $this->MbEmail->forgotPasswordMb( $params );
						if($mailSendCheck){
							$responseData = array('method_name'=> 'forgotPassword', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_606);
							//** Add Values to link expiration Model
							$linkExpireData = array("user_id"=>$result['User']['id'], "project"=>"MB","verification_token"=> $tokenToValidate, "user_local_info"=> $this->Common->getFrontServerInfo());
							$this->ForgotPasswordLinkExpire->save($linkExpireData);
						}else{
							$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "607", 'message'=> ERROR_607);
						}
						$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSendCheck, "user_id"=> $result['User']['id'], "notify_type"=> "MB Forgot Password") );
						// $this->sendSmsLinkForgotPassword($params['forgotPasswordLink'],$result);
					}catch(Exception $e){
						$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "647", 'message'=> ERROR_647);
				}
			}else{
				$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "610", 'message'=> ERROR_610);
			}
		}else{
			$responseData = array('method_name'=> 'forgotPassword', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	--------------------------------------------------------------------------------
	On: 22-05-2017
	I/P:
	O/P:
	Desc: Logout from all device
	--------------------------------------------------------------------------------
	*/
	public function logoutFromOtherDevices($dataUserDevice = array()){
		if(!empty($dataUserDevice)){
			App::import('model','UserDevice');
			$UserDevice = new UserDevice();
			$userId = $dataUserDevice['user_id'];
			if(in_array(strtolower($dataUserDevice['device_type']), array("ios","android"))){
				$mobileTokenCount = $UserDevice->query("SELECT count(*) tokenCount FROM user_devices WHERE user_id = $userId AND (LOWER(device_type) = 'ios' OR LOWER(device_type) = 'android')");
				if($mobileTokenCount[0][0]['tokenCount'] > 0){
					$mobileTokenLogout = $UserDevice->query("UPDATE user_devices SET status = 0 WHERE user_id = $userId AND LOWER(application_type) = 'mb' AND  (LOWER(device_type) = 'ios' OR LOWER(device_type) = 'android')");
				}
			}else if(in_array(strtolower($dataUserDevice['device_type']), array("mbweb","web","0"))){
				$webTokenCount = $UserDevice->query("SELECT count(*) tokenCount FROM user_devices WHERE user_id = $userId AND (LOWER(device_type) = 'mbweb' OR LOWER(device_type) = 'web' OR LOWER(device_type) = '0')");
				if($webTokenCount[0][0]['tokenCount'] > 0){
					$UserDevice->query("UPDATE user_devices SET status = 0 WHERE user_id = $userId AND LOWER(application_type) = 'mb-web' AND  (LOWER(device_type) = 'web' OR LOWER(device_type) = 'mbweb' OR LOWER(device_type) = '0')");
				}
			}
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 20-06-2017
	I/P:
	O/P:
	Desc: At work status
	--------------------------------------------------------------------------------
	*/

	public function updateAtWorkStatus(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** Add User Institution[START]
				if(isset($dataInput['atwork_status'])){
					$dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
					if($dataInput['atwork_status'] == 0)
					{
						if(!empty($dutyLogData)){
							$this->UserDutyLog->updateAll(array("atwork_status"=> $dataInput['atwork_status'], "status"=> $dataInput['atwork_status']), array("user_id"=> $dataInput['user_id']));
						}else{
							$dutyData = array("atwork_status"=> $dataInput['atwork_status'], "status"=> $dataInput['atwork_status'], "user_id"=> $dataInput['user_id']);
							$this->UserDutyLog->save($dutyData);
						}
					}
					else
					{
						if(!empty($dutyLogData)){
							$this->UserDutyLog->updateAll(array("atwork_status"=> $dataInput['atwork_status']), array("user_id"=> $dataInput['user_id']));
						}else{
							$oncallData = array("atwork_status"=> $dataInput['atwork_status'], "user_id"=> $dataInput['user_id']);
							$this->UserDutyLog->save($oncallData);
						}
					}
					$responseData = array('method_name'=> 'updateAtWorkStatus', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
				}
				//** Add User Institution[END]

			}else{
				$responseData = array('method_name'=> 'updateAtWorkStatus', 'status'=>"2", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'updateAtWorkStatus', 'status'=>"3", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------------
	On: 13-07-2017
	I/P:
	O/P:
	Desc: User list of same hospital(institution, organization) and ON/OFF duty (used for lesser data)
	------------------------------------------------------------------------------------
	*/
	public function oncallUserListLight(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
				if($this->User->findById( $dataInput['user_id'] ) )
				{
					$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));

					if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1)
					{
						if( $this->tokenValidate() && $this->accesskeyCheck() ){
							$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
							$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
							$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
							$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId)));
							$params['loggedin_user_countryid'] = $userDetails['UserProfile']['country_id'];
							$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
							$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
							$enterpriseUserCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']),"company_id"=> $userCurrentCompany['UserEmployment']['company_id'],"status"=> 1)));
							$companySubscriptionCheck=$this->CompanyName->find("count", array("conditions"=> array("id"=>$userCurrentCompany['UserEmployment']['company_id'], "status"=>1,"is_subscribed"=>1)));
							//** Get Pending Colleagues[START]
							$pendingColleagueParams['loggedin_user_id'] = $loggedinUserId;
							$pendingColleagues = $this->UserColleague->getPendingColleague($pendingColleagueParams);
							$formattedPendingColleagues = $this->sameOrganizationUserFieldsFormatLight( $pendingColleagues , $loggedinUserId );
							$userData['pending_contacts'] = $formattedPendingColleagues;
							//** Get Pending Colleagues[END]
							if($companySubscriptionCheck ==1 && $enterpriseUserCheck ==0){
								/*$responseData = array('method_name'=> 'oncallUserListLight','status'=>'0','response_code'=>'655','message'=> ERROR_655);*/
								$userData['on_duty_users'] = array();
								$userData['off_duty_users'] = array();
								$userData['diretory_access'] = 655;
								$userData['message'] = ERROR_655;
								$responseData = array('method_name'=> 'oncallUserListLight','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData);
							}else{
								//** On Duty Users
								$userLists = array();
								if($params['user_current_company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
									$userLists = $this->User->oncallUserListsLight( $params, $loggedinUserId );
								}
								$formattedUsers = $this->sameOrganizationUserFieldsFormatLight( $userLists , $loggedinUserId );
								//echo "<pre>"; print_r($userLists);die;
								$onDuty = array(); $offDuty = array(); $offDutyAtwork = array(); $offDutyNotAtwork = array();
								$offdutyarray = array();
								//** for paid company check to show only approved user
								foreach($formattedUsers as $ul){
									$isApproveByInstitutionCheck = 1;
									$userEnterpriseCheck = $this->EnterpriseUserList->find("first", array("conditions"=> array("company_id"=> $params['user_current_company_id'], "email"=> $ul['email'], "status"=> 1)));
									if($companySubscriptionCheck ==1 && empty($userEnterpriseCheck)){$isApproveByInstitutionCheck = 0;}
									if($isApproveByInstitutionCheck){
									if($ul['on_duty'] == 1){
										// test is_current is 0 or not if is current is 0 then not put in list Shashi 1-3-17
										$isCurrent = $this->UserEmployment->find('all',array('conditions'=>array('UserEmployment.user_id'=>$ul['user_id'],'UserEmployment.is_current'=>'1')));
										if(!empty($isCurrent)){
											$onDuty[] = $ul;
										}else{
											$offDuty[] = $ul;
										}
									}else{
										// test is_current is 0 or not if is current is 0 then not put in list Shashi 1-3-17
										$offDuty[] = $ul;
										$isCurrent = $this->UserEmployment->find('all',array('conditions'=>array('UserEmployment.user_id'=>$ul['user_id'],'UserEmployment.is_current'=>'1')));
										if(empty($isCurrent)){

										}else{
											if($ul['at_work'] == 1)
											{
												$offDutyAtwork[] = $ul;
											}
											else
											{
												$offDutyNotAtwork[] = $ul;
											}
										}

									}
								}
							}
								$userData['on_duty_users'] = $onDuty;
								if(!empty($offDutyAtwork) && !empty($offDutyNotAtwork))
								{
									$offdutyarray = array_merge($offDutyAtwork,$offDutyNotAtwork);
								}
								elseif(!empty($offDutyAtwork)){
								    $offdutyarray = $offDutyAtwork;
								}
								else
								{
								    $offdutyarray = $offDutyNotAtwork;
								}
								$userData['off_duty_users'] = $offdutyarray;
								$responseData = array('method_name'=> 'oncallUserListLight','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData);
							}

						}else{
					        $responseData = array('method_name'=> 'oncallUserListLight','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
				         }

					}
					else
					{
						$isCurrentUser = 1;
						$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
						$responseData = array('method_name'=> 'oncallUserListLight','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
					}
				}
			else
			{
				$responseData = array('method_name'=> 'oncallUserListLight','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			}
	       }else{
				$responseData = array('method_name'=> 'oncallUserListLight','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
	    $encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	--------------------------------------------------------------------------
	On: 20-02-2017
	I/P: JSON
	O/P: JSON, user lists
	Desc: Fetching user list by pagination with search (used for lesser data)
	--------------------------------------------------------------------------
	*/
	public function sameOrganizationUserFieldsFormatLight( $userData = array(), $loggedinUserId = NULL ){
		$userDataList = array();
		if( !empty($userData) ){
		//** User Data
			foreach( $userData as $ud ){
				//** Find user colleague with logged in user
				$colleagueStatus = 0; $requestType = '';
				$myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'])));
				$meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId)));
				if( !empty($myColleague) ){
					$colleagueStatus = $myColleague['UserColleague']['status'];
					$requestType = "from_me";
				}elseif( !empty($meColleague) ){
					$colleagueStatus = $meColleague['UserColleague']['status'];
					$requestType = "to_me";
				}
				//** Get Profession
				$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['profession_id'])));
				//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
				$qbInfo = array("id"=> $qbId);
				//** Get QB details [END]
				//** Get role status [START]
				$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				//** Get role status [END]

				if(!empty($qbId)){
				$userDataList[] = array(
								'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
								'email'=>	!empty($ud['User']['email']) ? stripslashes($ud['User']['email']):'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'profession'=> !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type']:'',
								'Colleague'=> array("status"=> $colleagueStatus, "request_by"=> $requestType),
								'qb_details'=> $qbInfo,
								"role_status"=> !empty($userRoleStatusData['UserProfile']['role_status']) ? $userRoleStatusData['UserProfile']['role_status']:'',
								"on_duty"=> !empty($ud['UserDutyLog']['status']) ? $ud['UserDutyLog']['status'] : 0,
								"at_work"=> !empty($ud['UserDutyLog']['atwork_status']) ? $ud['UserDutyLog']['atwork_status'] : 0,
								);
				}
			}
		}
		return $userDataList;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 24-08-2017
	I/P: JSON
	O/P: JSON
	Desc: Update at work status for user
	----------------------------------------------------------------------------------------------
	*/
	public function updateAtWorkStatusLite(){
		$this->autoRender = false;
		$responseData = array();
		$customData = array();
		$is_import = "true";
		$startTime = microtime(true) * 1000; //** Used to track API request response
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			$dataInput = json_decode($encryptedData, true);
			//** One time token to validate for QB[START]
			$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
			$oneTimeTokenData = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
			$this->UserOneTimeToken->save($oneTimeTokenData);
			//** One time token to validate for QB[END]
			// if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$userData = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
				$dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
				if(!empty($userData['User']['email'])){
					$tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], $oneTimeToken);
					$token = $tokenDetails->session->token;
					$user_id = $tokenDetails->session->user_id;
					$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
					$custom_data_from_api = json_decode($user_details->user->custom_data);
					$customData['userRoleStatus'] = $custom_data_from_api->status;
					$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
					$customData['isImport'] = $is_import;
					$customData['at_work'] = (isset($dataInput['atwork_status']) ? (string) $dataInput['atwork_status'] : "0");
					$customData['on_call'] = "0";
					if(!empty($token)){
						$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
						$res = json_decode(json_encode($qbResponce));
						if(!empty($res['errors'])){
		                    $responseData = array('method_name'=> 'updateAtWorkStatusLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=>$qbResponce);
		                }else{

		                	if($dataInput['atwork_status'] == 0)
							{
								if(!empty($dutyLogData)){
									$this->UserDutyLog->updateAll(array("atwork_status"=> $dataInput['atwork_status'], "status"=> $dataInput['atwork_status']), array("user_id"=> $dataInput['user_id']));
								}else{
									$dutyData = array("atwork_status"=> $dataInput['atwork_status'], "status"=> $dataInput['atwork_status'], "user_id"=> $dataInput['user_id']);
									$this->UserDutyLog->save($dutyData);
								}
							}
							else
							{
								if(!empty($dutyLogData)){
									$this->UserDutyLog->updateAll(array("atwork_status"=> $dataInput['atwork_status']), array("user_id"=> $dataInput['user_id']));
								}else{
									$oncallData = array("atwork_status"=> $dataInput['atwork_status'], "user_id"=> $dataInput['user_id']);
									$this->UserDutyLog->save($oncallData);
								}
							}
							//** Get user current company geo fence[START]
								$params['user_id'] = $dataInput['user_id'];
								$usergeoFenceData = $this->getUserGeoData($params);
							//** Get user current company geo fence[END]
		                   $responseData = array('method_name'=> 'updateAtWorkStatusLite', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $usergeoFenceData);
		                }

					}else{
						$responseData = array('method_name'=> 'updateAtWorkStatusLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=>$qbResponce);
					}

				}else{
					$responseData = array('method_name'=> 'updateAtWorkStatusLite', 'status'=>"0", 'response_code'=> "603", 'message'=> ERROR_603);
				}

			// }else{
			// 	$responseData = array('method_name'=> 'updateAtWorkStatusLite', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'updateAtWorkStatusLite', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		$endTime = microtime(true) * 1000;//** Used to track API request response
		$headerVals = $this->getHeaderValues();
		$trackData = array(
							"user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> (int) ($endTime - $startTime),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		exit;
	}


	/*
	------------------------------------------------------------------------------------
	On: 25-08-2017
	I/P:
	O/P:
	Desc: Add user InstitutionLite(Almost same as profile edit on OCR employment edit)
	------------------------------------------------------------------------------------
	*/
	public function addUserInstitutionLite(){
		$this->autoRender = false;
		$responseData = array();
		$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$t1 = explode(" ",microtime());
		$milisec1 = str_replace('.', '', substr((string)$t1[0],1,4));
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$userStatusDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			// if($userStatusDetails['User']['status']==1 && $userStatusDetails['User']['approved']==1)
			if($userStatusDetails['User']['status']==1)
			{
				//** One time token to validate for QB[START]
				$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
				$oneTimeTokenData = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
				$this->UserOneTimeToken->saveAll($oneTimeTokenData);
				//** One time token to validate for QB[END]
				if( $this->validateToken() && $this->validateAccessKey() ){
					//** Add User Institution[START]
					if(!empty($dataInput['Company'])){
						foreach( $dataInput['Company'] as $company ){
							$profileData = array();
							if(!empty($company['company_id'])){
								//** Remove all is_current company before edit[START]
								try{
									$this->UserEmployment->updateAll( array("is_current"=> 0) , array("user_id"=> $dataInput['user_id']));
								}catch(Exception $e){}
								//** Remove all is_current company before edit[END]
								$company['designation_id'] = !empty($company['Designation']['designation_id']) ? $company['Designation']['designation_id'] : 0 ;
								if( empty($company['id']) ){
									$profileData = array("user_id"=> $dataInput['user_id'], "company_id"=> $company['company_id'], "from_date"=> isset($company['from_date']) ? date('Y-m-d', strtotime($company['from_date']))  : '0000-00-00', "to_date"=> isset($company['to_date']) ? date('Y-m-d', strtotime($company['to_date']))  : '0000-00-00', "location"=> isset($company['location']) ? $company['location']  : '', "description"=> isset($company['description']) ? $company['description']  :'', "is_current"=> 1,"designation_id"=> $company['designation_id']);
									$this->UserEmployment->saveAll( $profileData );
								}else{
									$fromDate = "0000-00-00"; $toDate = "0000-00-00"; $location = ""; $description = "";
									$profileData = array("user_id"=> $dataInput['user_id'], "company_id"=> $company['company_id'], "from_date"=> isset($company['from_date']) ? "'". date('Y-m-d', strtotime($company['from_date'])) ."'"  : "'".$fromDate."'", "to_date"=> isset($company['to_date']) ? "'". date('Y-m-d', strtotime($company['to_date'])) ."'"  : "'".$toDate."'", "location"=> isset($company['location']) ? "'". $company['location']. "'"  : "'". $location ."'", "description"=> isset($company['description']) ? "'". $company['description']. "'"  : "'". $description ."'", "is_current"=> 1,"designation_id"=> isset($company['designation_id']) ? $company['designation_id'] : 0);
									$this->UserEmployment->updateAll( $profileData , array("id"=> $company['id']));
								}
								//** Add/update ON duty[START]
								if($company['is_current'] == 1){ //** If current company add duty value (ON/OFF)
										$previousCompanyId = !empty($company['previous_company_id']) ? $company['previous_company_id'] :0;
										$currentCompanyId = !empty($company['company_id']) ? $company['company_id']: 0;
										$qbResponce = $this->updateOnCallStatusLite($userStatusDetails['User']['email'],$oneTimeToken, $company['duty_status']);
										// $encryptedData = $this->Common->encryptData(json_encode($responseData));
										// echo json_encode(array("values"=> $encryptedData));
										// exit();
										if($qbResponce['qbsuccess'] == 1)
										{
											if( $this->UserDutyLog->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id']))) == 0){
												$dutyData = array("user_id"=> $dataInput['user_id'], "hospital_id"=> $company['company_id'], "status"=> $company['duty_status']);
												$this->UserDutyLog->saveAll($dutyData);
											}else{
												$this->UserDutyLog->updateAll(array("hospital_id"=> $company['company_id'], "modified"=> "'".date('Y-m-d H:i:s')."'", "status"=> $company['duty_status']), array("user_id"=> $dataInput['user_id']));
											}
										}
										else{
											$responseData = array('method_name'=> 'addUserInstitutionLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $qbResponce);
										}

								}
							}
						}
						//** Update free/enterprise user date[START]
						$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
						$companySubscriptionData=$this->CompanyName->find("first", array("conditions"=> array("id"=>$currentCompanyId, "status"=>1,"is_subscribed"=>1)));
							if(!empty($companySubscriptionData)){
							//** Set email in user enterprise list
							$checkUserAdded = $this->EnterpriseUserList->find("first", array("conditions"=> array('email'=> stripslashes($userDetails['User']['email']),'company_id'=>$currentCompanyId))); //previously count changed to first in query
							if(empty($checkUserAdded)){ // previously $checkUserAdded == 0
								$this->EnterpriseUserList->save(array('email'=> stripslashes($userDetails['User']['email']),'company_id'=>$currentCompanyId, 'status'=> 0));
							}
							//****Update user as unassign for the institute if removed by trust admin
							else if($checkUserAdded['EnterpriseUserList']['status'] == 2)
							{
								$this->EnterpriseUserList->updateAll(array("status"=> 0), array("email"=> $userDetails['User']['email'], "company_id"=>$currentCompanyId));
							}
						}
						//** Previous company paid check[START]
						$dataParamsPrevious['company_id'] = isset($previousCompanyId) ? $previousCompanyId : 0;
						$dataParamsPrevious['email'] = $userDetails['User']['email'];
						$checkUserForPaidPreviousCompany = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParamsPrevious);
						//** Previous company paid check[END]

						//** Current company paid check[START]
						$dataParams['company_id'] = $currentCompanyId;
						$dataParams['email'] = $userDetails['User']['email'];
						$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);
						//** Current Company paid check[END]
						$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						if(!empty($checkUserForPaid)){
							if(!empty($checkUserSubscribed)){
								$this->UserSubscriptionLog->updateAll(array("subscribe_type"=>1,"company_id"=> $currentCompanyId, "subscription_date"=> "'".$checkUserForPaid['CompanyName']['subscription_date']."'", "subscription_expiry_date"=> "'".$checkUserForPaid['CompanyName']['subscription_expiry_date']."'"), array("user_id"=> $dataInput['user_id']));
							}else{
								$subscriptionData = array("subscribe_type"=>1,"user_id"=> $dataInput['user_id'],"company_id"=> $currentCompanyId, "subscription_date"=> "'".$checkUserForPaid['CompanyName']['subscription_date']."'", "subscription_expiry_date"=> $checkUserForPaid['CompanyName']['subscription_expiry_date']);
								$this->UserSubscriptionLog->save($subscriptionData);
							}
						}else{
							//** For new user setting first time institution[START]
								$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
								if(empty($checkUserSubscribed)){
									$subscriptionStartDate = date('Y-m-d 23:59:59');
									$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_TRIAL_PERIOD.' days'));
									$subscriptionDataVals = array("user_id"=> $userDetails['User']['id'],"company_id"=> 0, "subscription_date"=> $subscriptionStartDate, "subscription_expiry_date"=> $subscriptionExpireDate);
									$this->UserSubscriptionLog->save($subscriptionDataVals);
								}
							//** For new user setting first time institution[END]

							//** User edit institution paid to free update grace period[START]
							if(!empty($checkUserForPaidPreviousCompany) && empty($checkUserForPaid)){
								$subscriptionStartDate = date('Y-m-d 23:59:59');
								$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
								if(!empty($checkUserSubscribed)){
									$this->UserSubscriptionLog->updateAll(array("company_id"=>$currentCompanyId, "subscribe_type"=>2, "subscription_date"=> "'".$subscriptionStartDate."'", "subscription_expiry_date"=> "'".$subscriptionExpireDate."'"), array("user_id"=> $dataInput['user_id']));
								}else{
									$subscriptionData = array("user_id"=> $dataInput['user_id'],"company_id"=> $currentCompanyId,"subscribe_type"=>2, "subscription_date"=> "'".$subscriptionStartDate."'", "subscription_expiry_date"=> $subscriptionExpireDate);
									$this->UserSubscriptionLog->save($subscriptionData);
								}
							}
							//** User edit institution paid to free update grace period[END]
						}
						//** Update free/enterprise user date[END]
						//** Get user Enterprise data[START]
						$status = 0;$expiryDate = 0;
						$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						//** When user comes after trust expiry date update grace period[START]
						$currentDate = date('Y-m-d 23:59:59');
						$gracePeriod = date('Y-m-d 23:59:59', strtotime($currentDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
						$trustExpiryDate = $checkUserSubscribed['UserSubscriptionLog']['subscription_expiry_date'];
						if(strtotime($currentDate) > strtotime($trustExpiryDate) && !empty($checkUserForPaid) && ($checkUserSubscribed['UserSubscriptionLog']['subscribe_type']==2)){
							$this->UserSubscriptionLog->updateAll(array("subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $dataInput['user_id']));
						}
						//** When user comes after trust expiry date update grace period[END]
						//** Get user SUBSCRIPTION status
						$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						if(!empty($checkUserSubscribedFinal)){
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
							$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
						}
						//** In Case of grace period oncall should be off[START]
						if($status==2){
							//** One time token to validate for QB[START]
							$oneTimeToken = "";
							$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenData = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
							$this->UserOneTimeToken->saveAll($oneTimeTokenData);
							//** One time token to validate for QB[END]
							$qbResponce = $this->updateOnCallStatusLite($userStatusDetails['User']['email'],$oneTimeToken, 0);
							if($qbResponce['qbsuccess'] == 1){
								$this->UserDutyLog->updateAll(array("status"=> 0), array("user_id"=> $dataInput['user_id'], "hospital_id"=> $currentCompanyId));
							}
						}
						//** In Case of grace period oncall should be off[END]
						$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate) > 0) ? strtotime($expiryDate):0);
						//** Get user Enterprise data[END]
						//** Add company details[START]
						$companyName = "";
						$employment = $this->UserEmployment->find("first", array("conditions"=> array("company_id"=> $currentCompanyId, "is_current"=> 1)));
						$companyNameData = $this->CompanyName->find("first", array("conditions"=> array("id"=> $currentCompanyId)));
						if(!empty($companyNameData)) {
							$companyName = $companyNameData['CompanyName']['company_name'];
							$patientNumberType = $companyNameData['CompanyName']['patient_number_type'];
						}
						$isCompanyAssignPending = 0;
						$isCompanyAssignCheck = 0;
						if($currentCompanyId !=1){
							if($companyNameData['CompanyName']['is_subscribed'] == 1 && $companyNameData['CompanyName']['status']==1){
								$isCompanyAssignCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $userDetails['User']['email'], "company_id"=> $currentCompanyId, "status"=> 0)));
							}
						}
						if($isCompanyAssignCheck > 0){ $isCompanyAssignPending = 1; }
						$userEmploymentData = array("id"=> !empty($employment['UserEmployment']['id']) ? (int) $employment['UserEmployment']['id'] : (int) 0, "company_id"=> $currentCompanyId ,"company_name"=> $companyName, "patient_number_type"=>$patientNumberType, "is_current"=> $employment['UserEmployment']['is_current'],"is_company_approval_pending"=> $isCompanyAssignPending);
						//** Add company details[END]
						//$settingData['settings'] = array($subscriptionData);




						$data = array('settings'=> array($subscriptionData), 'Company'=> array($userEmploymentData));


						// Add transaction for QR code selection [START]

						if(!empty($dataInput['Company'])){
							$this->InstituteSelectionTransaction->updateAll(
							    array('InstituteSelectionTransaction.status'=>0),
							    array('InstituteSelectionTransaction.user_id'=>$dataInput['user_id'])
							);
							foreach( $dataInput['Company'] as $company ){
								$transactionData = array(
									'user_id' => $dataInput['user_id'],
									'in_intitute'=> $company['company_id'],
									'out_institute'=> $company['previous_company_id'],
									'mode'=> 'mannual',
									'status'=> 1
								);
							}
								
						}
				
						// Add transaction for QR code selection [END]
						//Update Dnd Status
						$params['user_id'] = $dataInput['user_id'];
						$params['email'] = $userDetails['User']['email'];
						$oneTimeTokenForDnd = $this->dissmissDndOnOcrAndQb($params);

						$responseData = array('method_name'=> 'addUserInstitutionLite', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $data);

						//** For first time free user send mail to sales[START]
						$checkFinalUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						$chkAlreadySent = $this->EmailSmsLog->find("count", array("conditions"=>array("type"=> "email", "user_id"=> $dataInput['user_id'], "notify_type"=> "Free Trial User Mail to sales")));
						if(($chkAlreadySent == 0) && ($checkFinalUserSubscribed['UserSubscriptionLog']['subscribe_type'] == 0)){
							$mailParams = array();
							$mailParams['name'] = stripslashes($userDetails['UserProfile']['first_name']).' '.stripslashes($userDetails['UserProfile']['last_name']);
							$professionData = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
							$mailParams['profession'] = !empty($professionData['Profession']['profession_type']) ? $professionData['Profession']['profession_type'] :'N/A';
							$companyData = $this->CompanyName->find("first", array("conditions"=> array("id"=> $currentCompanyId)));
							$mailParams['institution_name'] = !empty($companyData['CompanyName']['company_name']) ? $companyData['CompanyName']['company_name'] : 'N/A';
							$mailParams['user_email'] = $userDetails['User']['email'];
							$mailParams['user_phone_number'] = !empty($userDetails['UserProfile']['contact_no']) ? $userDetails['UserProfile']['contact_no'] :'N/A';
							$mailParams['registration_date'] = substr($userDetails['User']['registration_date'],0,10);
							$mailToSales = $this->MbEmail->sendMailToSalesFreeEnterpriseUser($mailParams);
							$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailToSales, "user_id"=> $dataInput['user_id'], "notify_type"=> "Free Trial User Mail to sales") );
						}
						//** For first time free user send mail to sales[END]

						//**************** Code For cache Delete[START] ****************//
						$updateCache = $this->cacheDeleteOnInstitutionChange($dataInput['user_id'], $currentCompanyId,$previousCompanyId);
						//**************** Code For cache Delete[END]   ****************//


					}
					//** Add User Institution[END]

				}else{
					$responseData = array('method_name'=> 'addUserInstitutionLite', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
				}
			}
			else
			{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
				$responseData = array('method_name'=> 'settings','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'addUserInstitutionLite', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		//** Track API Request, Response[START]
		$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$t2 = explode(" ",microtime());
		$milisec2 = str_replace('.', '', substr((string)$t2[0],1,4));
		$headerVals = $this->getHeaderValues();
		$trackData = array(
							"user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> ($milisec2 - $milisec1),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		//** Track API Request, Response[END]
		exit;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 25-08-2017
	I/P: JSON
	O/P: JSON
	Desc: Update at work status for user on qb
	----------------------------------------------------------------------------------------------
	*/

	public function updateOnCallStatusLite($userEmail, $password, $dutystatus)
	{
		$response = array();
		$customData = array();
		$is_import = "true";
		$tokenDetails = $this->Quickblox->quickLogin($userEmail, $password);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
		$custom_data_from_api = json_decode($user_details->user->custom_data);
		$customData['userRoleStatus'] = $custom_data_from_api->status;
		$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
		$customData['isImport'] = $is_import;
		$customData['at_work'] = $custom_data_from_api->at_work;
		$customData['on_call'] = (string) $dutystatus;
		$customData['dnd_status'] = $custom_data_from_api->dnd_status;
		if(!empty($customData['dnd_status']))
		{
			$customData['is_dnd_active'] = 1;
		}
		if(!empty($token)){
			$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
			$res = json_decode(json_encode($qbResponce));
			if(!empty($res['errors'])){
				$response['qberror'] = $qbResponce;
			}
			else
			{
				$response['qbsuccess'] = 1;
			}
		}
		else{
			$response['qberror'] = $tokenDetails;
		}
		return $response;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 25-08-2017
	I/P: JSON
	O/P: JSON
	Desc: Update at work status for user on qb
	----------------------------------------------------------------------------------------------
	*/

	public function updateDndStatusLite($params = array())
	{
		$response = array();
		$customData = array();
		$is_import = "true";
		$dndArray = array('dnd_name'=>$params['dnd_name'], 'end_time'=>(string)$params['end_time']);
		$tokenDetails = $this->Quickblox->quickLogin($params['user_email'], $params['one_time_token']);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
		$custom_data_from_api = json_decode($user_details->user->custom_data);
		$customData['userRoleStatus'] = $custom_data_from_api->status;
		$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
		$customData['isImport'] = $is_import;
		$customData['at_work'] = $custom_data_from_api->at_work;
		$customData['on_call'] = $custom_data_from_api->on_call;
		$customData['is_dnd_active'] = $params['is_dnd_active'];
		if($params['is_dnd_active'] == 1)
		{
			$customData['dnd_status'] = $dndArray;
		}
		if(!empty($token)){
			$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
			$res = json_decode(json_encode($qbResponce));
			if(!empty($res['errors'])){
				$response['qberror'] = $qbResponce;
			}
			else
			{
				$response['qbsuccess'] = 1;
			}
		}
		else{
			$response['qberror'] = $tokenDetails;
		}
		return $response;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 28-08-2017
	I/P: JSON
	O/P: JSON
	Desc: get users geo fence data for latest company
	-------------------------------------------------------------------------------------
	*/
	public function getUserGeoData($params=array()){
		$userGeoData = array();
		if(!empty($params)){
			$userCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "is_current"=> 1)));
			$companyGeoData = $this->CompanyGeofenceParameter->find("first", array("conditions"=> array("company_id"=> $userCompany['UserEmployment']['company_id'])));
			if(!empty($companyGeoData)){
				$userGeoData['GeoData'] = array("company_id"=> $companyGeoData['CompanyGeofenceParameter']['company_id'], "company_address"=> $companyGeoData['CompanyGeofenceParameter']['company_address'], "latitude"=> $companyGeoData['CompanyGeofenceParameter']['latitude'], "longitude"=> $companyGeoData['CompanyGeofenceParameter']['longitude'], "radius"=> $companyGeoData['CompanyGeofenceParameter']['radius']);
			}else{
				$userGeoData = (object) array();
			}
		}
		return $userGeoData;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 15-09-2017
	I/P: JSON
	O/P: JSON
	Desc: update user avatar url on qb
	-------------------------------------------------------------------------------------
	*/

	public function updateAvatarUrlOnQb($userEmail, $password, $profileImg)
	{
		$response = array();
		$customData = array();
		$is_import = "true";
		$tokenDetails = $this->Quickblox->quickLogin($userEmail, $password);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
		$custom_data_from_api = json_decode($user_details->user->custom_data);
		$customData['userRoleStatus'] = $custom_data_from_api->status;
		$customData['userProfileImgPath'] = $profileImg;
		$customData['isImport'] = $is_import;
		$customData['at_work'] = $custom_data_from_api->at_work;
		$customData['on_call'] = $custom_data_from_api->on_call;
		$customData['dnd_status'] = $custom_data_from_api->dnd_status;
		if(!empty($customData['dnd_status']))
		{
			$customData['is_dnd_active'] = 1;
		}
		if(!empty($token)){
			$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
			$res = json_decode(json_encode($qbResponce));
			if(!empty($res['errors'])){
				$response['qberror'] = $qbResponce;
			}
			else
			{
				$response['qbsuccess'] = 1;
			}
		}
		else{
			$response['qberror'] = $tokenDetails;
		}
		return $response;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 15-09-2017
	I/P: JSON
	O/P: JSON
	Desc: update role status on qb
	-------------------------------------------------------------------------------------
	*/

	public function updateRoleStatusOnQb($userEmail, $password, $roleStatus)
	{
		$response = array();
		$customData = array();
		$is_import = "true";
		$tokenDetails = $this->Quickblox->quickLogin($userEmail, $password);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
		$custom_data_from_api = json_decode($user_details->user->custom_data);
		$customData['userRoleStatus'] = $roleStatus;
		$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
		$customData['isImport'] = $is_import;
		$customData['at_work'] = $custom_data_from_api->at_work;
		$customData['on_call'] = $custom_data_from_api->on_call;
		$customData['dnd_status'] = $custom_data_from_api->dnd_status;
		if(!empty($customData['dnd_status']))
		{
			$customData['is_dnd_active'] = 1;
		}
		if(!empty($token)){
			$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
			$res = json_decode(json_encode($qbResponce));
			if(!empty($res['errors'])){
				$response['qberror'] = $qbResponce;
			}
			else
			{
				$response['qbsuccess'] = 1;
			}
		}
		else{
			$response['qberror'] = $tokenDetails;
		}
		return $response;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 19-09-2017
	I/P: JSON
	O/P: JSON
	Desc: update phone number on qb
	-------------------------------------------------------------------------------------
	*/

	public function updateContactNoOnQb($userEmail, $password, $contact_no)
	{
		$response = array();
		$tokenDetails = $this->Quickblox->quickLogin($userEmail, $password);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		if(!empty($token))
		{
			$qbResponce = $this->Quickblox->updatePhoneOnQuickBlox($token, $user_id, $contact_no);
			$res = json_decode(json_encode($qbResponce));
			if(!empty($res['errors'])){
				$response['qberror'] = $qbResponce;
			}
			else
			{
				$response['qbsuccess'] = 1;
			}
		}
		else
		{
			$response['qberror'] = $tokenDetails;
		}
		return $response;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 09-10-2017
	I/P: JSON
	O/P: JSON
	Desc: genrate Random Token
	-------------------------------------------------------------------------------------
	*/

	// public function genrateRandomToken($userId=NULL)
	// {
	// 	if(!empty($userId))
	// 	{
	// 		$userInfo = $userId.rand(1000,100000);
	// 		$options = [
	// 		    'cost' => 4,
	// 		];
	// 		$token = md5(password_hash($userInfo, PASSWORD_BCRYPT, $options));
	// 		$token = md5(crypt($userInfo));
	// 	}
	// 	return $token;
	// }

	public function genrateRandomToken($userId=NULL)
	{
		if(!empty($userId))
		{
			$userInfo = $userId.rand(1000,100000);
			$options = [
			    'cost' => 4,
			];
			$token = md5(password_hash($userInfo, PASSWORD_BCRYPT, $options));
			// $token = md5(crypt($userInfo));
		}
		return $token;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 24-10-2017
	I/P: JSON
	O/P: JSON
	Desc: Update profile new
	-------------------------------------------------------------------------------------
	*/
	public function updateProfileLite(){
		$responseData = array();
		$isProfileisUpdated = 0;
		$isCacheModified = 0;
		$modifiedText = 0;
		if($this->request->is('post')) {
				$dataInput = $this->request->input ( 'json_decode', true) ;
				if( $this->User->find( "count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0 ){

					try{
						$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'], "is_current"=> 1), "order"=> array("id DESC")));

						$userInformation =  $this->AdminActivityLog->find('first', array('conditions' =>
		                array('AdminActivityLog.user_id' => $dataInput['user_id']),'order'=>array('AdminActivityLog.id DESC')
		                ,'limit'=> 1));
						if($userInformation['AdminActivityLog']['message'] != "Deactivated"){
						$userInfo = $this->User->find( "first", array("conditions"=> array("User.id"=>$dataInput['user_id'])));
						// if($userInfo['User']['status']==1 && $userInfo['User']['approved']==1){
						if($userInfo['User']['status']==1){
						//if( $this->tokenValidate() && $this->accesskeyCheck() ){
						//** Update Role Status[START]
						if(isset($dataInput['role_status'])){

							//** One time token to validate for QB[START]
							$oneTimeTokenForRoleStatus = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenDataRoleStatus = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeTokenForRoleStatus);
							$this->UserOneTimeToken->saveAll($oneTimeTokenDataRoleStatus);
							//** One time token to validate for QB[START]

							$profileData = array("UserProfile.role_status"=> "'". addslashes($dataInput['role_status']) ."'");
							$qbResponceRoleStatus = $this->updateRoleStatusOnQb($userInfo['User']['email'],$oneTimeTokenForRoleStatus, $dataInput['role_status']);
							$roleStatusResult = $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $dataInput['user_id']));

							if($qbResponceRoleStatus['qbsuccess'] == 1)
							{
								$isCacheModified = 1;
								$modifiedText = 2;
								$roleStatusResult = $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $dataInput['user_id']));
								$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601);
							}
							else
							{
								$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $qbResponceRoleStatus);
							}

						}
						//** Update Role Status[END]

						//** Update Available Status[START]

						if(isset($dataInput['at_work'])){
							$dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
							//** One time token to validate for QB[START]
							$oneTimeTokenForAvailable = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenDataAvailableStatus = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeTokenForAvailable);
							$this->UserOneTimeToken->saveAll($oneTimeTokenDataAvailableStatus);
							//** One time token to validate for QB[START]

							if(!empty($userInfo['User']['email'])){
								$tokenDetails = $this->Quickblox->quickLogin($userInfo['User']['email'], $oneTimeTokenForAvailable);
								$token = $tokenDetails->session->token;
								$user_id = $tokenDetails->session->user_id;
								$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
								$custom_data_from_api = json_decode($user_details->user->custom_data);
								$customData['userRoleStatus'] = $custom_data_from_api->status;
								$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
								$customData['isImport'] = $is_import;
								$customData['at_work'] = (isset($dataInput['at_work']) ? (string) $dataInput['at_work'] : "0");
								$customData['on_call'] = "0";
								$customData['dnd_status'] = $custom_data_from_api->dnd_status;
								if(!empty($customData['dnd_status']))
								{
									$customData['is_dnd_active'] = 1;
								}
								if(!empty($token)){
									$qbResponceAvailableStatus = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
									$res = json_decode(json_encode($qbResponce2));
									if(!empty($res['errors'])){
					                    $responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=>$qbResponceAvailableStatus);
					                }else{
					                	$isCacheModified = 1;
					                	$modifiedText = 2;
					                	if($dataInput['at_work'] == 0)
										{
											if(!empty($dutyLogData)){
												$this->UserDutyLog->updateAll(array("atwork_status"=> $dataInput['at_work'], "status"=> $dataInput['at_work']), array("user_id"=> $dataInput['user_id']));
											}else{
												$dutyData = array("atwork_status"=> $dataInput['at_work'], "status"=> $dataInput['at_work'], "user_id"=> $dataInput['user_id']);
												$this->UserDutyLog->save($dutyData);
											}
										}
										else
										{
											if(!empty($dutyLogData)){
												$this->UserDutyLog->updateAll(array("atwork_status"=> $dataInput['at_work']), array("user_id"=> $dataInput['user_id']));
											}else{
												$oncallData = array("atwork_status"=> $dataInput['at_work'], "user_id"=> $dataInput['user_id']);
												$this->UserDutyLog->save($oncallData);
											}
										}
										//********** Save available Transaction[START]
										try
										{
											$deviceType =  $this->getDeviceType();
											$transactionParams['user_id'] = $dataInput['user_id'];
											$transactionParams['company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
											$transactionParams['type'] = "Available";
											$transactionParams['status'] = $dataInput['at_work'];
											$transactionParams['device_type'] = $deviceType;
											$transactionParams['platform'] = "app";
											$saveAvailableTransaction = $this->saveAvailableAndOncallTransactions($transactionParams);

											if($deviceType == "MBWEB" || $deviceType == "mbweb")
											{
												$params['user_id'] = $dataInput['user_id'];
												$params['at_work_status'] = $dataInput['at_work'];
												$params['push_on'] = $dataInput['at_work'];
												$sendAtWorkPushNotification = $this->sendAtWorkPushNotification($params);
											}
											
										}catch(Exception $e){
											$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
										}
										//********** Save available Transaction[END]
					                   $responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601);
					                }

								}else{
									$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=>$qbResponceAvailableStatus);
								}

							}else{
								$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "603", 'message'=> ERROR_603);
							}
						}
						//** Update Available Status[END]

						//** Update On Call Status[START]
						if(isset($dataInput['duty_status'])){
							$dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
							//** One time token to validate for QB[START]
							$oneTimeTokenForOnCall = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenDataOnCall = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeTokenForOnCall);
							$this->UserOneTimeToken->saveAll($oneTimeTokenDataOnCall);
							//** One time token to validate for QB[END]

							$qbResponceOnCall = $this->updateOnCallStatusLite($userInfo['User']['email'],$oneTimeTokenForOnCall, $dataInput['duty_status']);
							if($qbResponceOnCall['qbsuccess'] == 1)
							{
								$isCacheModified = 1;
								$modifiedText = 2;
								if(! empty($dutyLogData))
								{
									$this->UserDutyLog->updateAll(array("status"=> $dataInput['duty_status']), array("user_id"=> $dataInput['user_id']));
								}
								//********** Save OnCall Transaction[START]
								try
								{
									$deviceType =  $this->getDeviceType();
									$transactionParams['user_id'] = $dataInput['user_id'];
									$transactionParams['company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
									$transactionParams['type'] = "OnCall";
									$transactionParams['status'] = $dataInput['duty_status'];
									$transactionParams['device_type'] = $deviceType;
									$transactionParams['platform'] = "app";
									$saveOnCallTransaction = $this->saveAvailableAndOncallTransactions($transactionParams);
								}catch(Exception $e){
									$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
								}
								//********** Save OnCall Transaction[END]
								$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601);
							}
							else{
								$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $qbResponceOnCall);
							}
						}
						//** Update On Call Status[END]

						//** Update Contact Number[START]

						if(isset($dataInput['phone'])){
							$profileData = array("UserProfile.contact_no"=> "'".$dataInput['phone'][0]['mobile_no']."'", "UserProfile.contact_no_is_verified"=> $dataInput['phone'][0]['is_mobile_verified']);

							//** One time token to validate for QB[START]
							$oneTimeTokenForContact = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenDataContact = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeTokenForContact);
							$this->UserOneTimeToken->saveAll($oneTimeTokenDataContact);
							//** One time token to validate for QB[END]

							$qbResponceContact = $this->updateContactNoOnQb($userInfo['User']['email'],$oneTimeTokenForContact, $dataInput['phone'][0]['mobile_no']);
							if($qbResponceContact['qbsuccess'] == 1)
							{
								$isProfileisUpdated = 1;
								$modifiedText = 1;
								$contactResult = $this->UserProfile->updateAll($profileData, array("UserProfile.user_id"=> $dataInput['user_id']));

								if($contactResult)
								{
									$userProfile = $this->UserProfile->find("first", array("conditions"=> array("user_id"=>$dataInput['user_id'])));
								$userData['User'] = array('profile_img'=> !empty($userProfile['UserProfile']['profile_img']) ? AMAZON_PATH . $dataInput['user_id']. '/profile/' . $userProfile['UserProfile']['profile_img']:'');
									$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_610, 'data'=> $userData);
								}
								else
								{
									$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_648);
								}

							}
							else
							{
								$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $qbResponceContact);
							}
						}

						//** Update Contact Number[END]

						//** Update Role Tag[START]
						if(isset($dataInput['role_tags'])){
							$isCacheModified = 1;
							$modifiedText = 2;
							foreach($dataInput['role_tags'] as $roleTags){
								$userRoleTag = $roleTags['tag'];
								$userRoleTagValue = $roleTags['values'];
								foreach ($userRoleTagValue as $tagValue) {

									$roleTagData = $this->RoleTag->find("first", array("conditions"=> array("key"=> $userRoleTag, "value"=> $tagValue)));
									if($userRoleTag == "Grade")
									{
										$getUserRoleTagData = $this->UserRoleTag->getUserRoleTagData($dataInput['user_id'],$userRoleTag);
										if(! empty($getUserRoleTagData))
										{
											$updateUserRoleTag = $this->UserRoleTag->updateAll(array("role_tag_id"=> $roleTagData['RoleTag']['id']), array("UserRoleTag.id"=> $getUserRoleTagData[0]['UserRoleTag']['id']));
										}
										else
										{
											$this->UserRoleTag->saveAll(array('user_id'=>$dataInput['user_id'],'role_tag_id'=>$roleTagData['RoleTag']['id'],'created'=>date("Y-m-d H:i:s")));
										}
									}
									if($userRoleTag == "Speciality")
									{
										$getUserRoleTagData = $this->UserRoleTag->getUserRoleTagData($dataInput['user_id'],$userRoleTag);
										if(! empty($getUserRoleTagData))
										{
											$updateUserRoleTag = $this->UserRoleTag->updateAll(array("role_tag_id"=> $roleTagData['RoleTag']['id']), array("UserRoleTag.id"=> $getUserRoleTagData[0]['UserRoleTag']['id']));
										}
										else
										{
											$this->UserRoleTag->saveAll(array('user_id'=>$dataInput['user_id'],'role_tag_id'=>$roleTagData['RoleTag']['id'],'created'=>date("Y-m-d H:i:s")));
										}
									}
									if($userRoleTag == "Base Location")
									{
										$getUserRoleTagData = $this->UserRoleTag->getUserRoleTagData($dataInput['user_id'],$userRoleTag);
										if(! empty($getUserRoleTagData))
										{
											$updateUserRoleTag = $this->UserRoleTag->updateAll(array("role_tag_id"=> $roleTagData['RoleTag']['id']), array("UserRoleTag.id"=> $getUserRoleTagData[0]['UserRoleTag']['id']));
										}
										else
										{
											$this->UserRoleTag->saveAll(array('user_id'=>$dataInput['user_id'],'role_tag_id'=>$roleTagData['RoleTag']['id'],'created'=>date("Y-m-d H:i:s")));
										}
									}
									if($userRoleTag == "Band")
									{
										$getUserRoleTagData = $this->UserRoleTag->getUserRoleTagData($dataInput['user_id'],$userRoleTag);
										if(! empty($getUserRoleTagData))
										{
											$updateUserRoleTag = $this->UserRoleTag->updateAll(array("role_tag_id"=> $roleTagData['RoleTag']['id']), array("UserRoleTag.id"=> $getUserRoleTagData[0]['UserRoleTag']['id']));
										}
										else
										{
											$this->UserRoleTag->saveAll(array('user_id'=>$dataInput['user_id'],'role_tag_id'=>$roleTagData['RoleTag']['id'],'created'=>date("Y-m-d H:i:s")));
										}
									}

								}
							}

							$roleTagsArr = array(); $roleTagsVals = array();
							$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $userInfo['UserProfile']['profession_id'])));
							if(!empty($roleTags['Profession']['tags'])){
								$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
							}
							if(!empty($roleTagsVals)){
								foreach($roleTagsVals as $rt){
									$values = array(); $roleTagId = 0;
									$params['user_id'] = $dataInput["user_id"];
									$params['key'] = $rt;
									$roleTagUser = $this->RoleTag->userRoleTags($params);
									if(!empty($roleTagUser)){
										$roleTagId = $roleTagUser[0]['rt']['id'];
										$values = array($roleTagUser[0]['rt']['value']);
									}
									$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
								}
							}
							$customRoleTagData = array("UserProfile.custom_role"=> "'". json_encode($roleTagsArr) ."'");
							$updateCustomRoleTag = $this->UserProfile->updateFields($customRoleTagData, array("UserProfile.user_id"=> $dataInput['user_id']));
							$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
						}
						//** Update Role Tag[END]

						//** Update Profile Image[END]
						if (isset($dataInput['user_img'])) {
							$imageName = $this->Image->createimage($dataInput['user_img'][0]['profile_img'],WWW_ROOT . 'img/uploader_tmp/', md5($dataInput['user_id'] .'_' . strtotime(date('Y-m-d H:i:s')) ), $dataInput['user_img'][0]['img_ext']);
							if( !empty($imageName) ){
								// $this->Image->moveImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$imageName, 'img_name'=> $dataInput['user_id'].'/profile/'.$imageName) );

								$this->Image->pushImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$imageName, 'img_name'=> $dataInput['user_id'].'/profile/'.$imageName), $contentType = 'image/png',  $imageName);
							}
							//** Update avatar url on qb
							//** One time token to validate for QB[START]
							$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenDataProfileImg = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
							$this->UserOneTimeToken->saveAll($oneTimeTokenDataProfileImg);
							//** One time token to validate for QB[START]
							// $imgVal = AMAZON_PATH.$dataInput['user_id'].'/profile/'.$imageName;
							$imgVal = PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH.$dataInput['user_id'].'/profile/'.$imageName;
							$qbResponce = $this->updateAvatarUrlOnQb($userInfo['User']['email'],$oneTimeToken, $imgVal);
							if($qbResponce['qbsuccess'] == 1)
							{
								//** Update Image name on table
								$isProfileisUpdated = 1;
								$modifiedText = 1;
								$profileData = array("UserProfile.profile_img"=> "'".$imageName."'", "UserProfile.thumbnail_img"=> "'".$imageName."'");
								$imageUploadResult = $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $dataInput['user_id']));
								//** Remove physical file from Temp Directory
								$this->Common->removeTempFile( WWW_ROOT . 'img/uploader_tmp/'.$imageName );
								$ud = $this->UserProfile->find("first", array("conditions"=> array("user_id"=>$dataInput['user_id'])));
								// $userData['User'] = array('profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $dataInput['user_id']. '/profile/' . $ud['UserProfile']['profile_img']:'');
								$userData['User'] = array('profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $dataInput['user_id']. '/profile/' . $ud['UserProfile']['profile_img']:'', 'thumbnail_img'=> !empty($ud['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $dataInput['user_id']. '/profile/' . $ud['UserProfile']['thumbnail_img']:'');
								if($imageUploadResult)
								{
									$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601, 'data'=> $userData);
								}
								else
								{
									$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
								}
							}
							else
							{
							$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618, "system_errors"=> $qbResponce);
							}
						}


						if (isset($dataInput['dnd_status'])) {
							$params['user_id'] = $dataInput['user_id'];
							$params['institute_id'] = $userCurrentCompany['UserEmployment']['company_id'];
							$params['user_email'] = $userInfo['User']['email'];
							$params['dnd_status_id'] = $dataInput['dnd_status']['dnd_status_id'];
							$dndStatusOptions = $this->DndStatusOption->getDndStatusOptions($params);
							$params['dnd_name'] = $dndStatusOptions['DndStatusOption']['dnd_name'];
							$params['icon_selected'] = $dndStatusOptions['DndStatusOption']['icon_selected'];
							$getUserDndStatus = $this->UserDndStatus->getUserDndStatus_pre($params);
							// echo "<pre>";print_r($getUserDndStatus);exit();
							$params['is_dnd_active'] = isset($dataInput['dnd_status']['dnd_active']) ? $dataInput['dnd_status']['dnd_active']: 0;
							$startTime = time();
							$duration = $dataInput['dnd_status']['duration'];
							$endTime = $startTime + $duration*60;
							$params['end_time'] = ($dataInput['dnd_status']['dnd_active'] == 1) ? $endTime : "";
							$params['dnd_name'] = ($dataInput['dnd_status']['dnd_active'] == 1) ? $params['dnd_name'] : "";
							$params['dnd_status_id'] = ($dataInput['dnd_status']['dnd_active'] == 1) ? $params['dnd_status_id'] : "";
							$params['icon_selected'] = ($dataInput['dnd_status']['dnd_active'] == 1) ? $params['icon_selected'] : "";


							$oneTimeTokenForDnd = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenDataDnd = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeTokenForDnd);
							$this->UserOneTimeToken->saveAll($oneTimeTokenDataDnd);
							$params['one_time_token'] = $oneTimeTokenForDnd;

							$qbResponceDnd = $this->updateDndStatusLite($params);
							if($qbResponceDnd['qbsuccess'] == 1)
							{
								$isCacheModified = 1;
								$modifiedText = 2;
								try{
									if(!empty($getUserDndStatus))
									{
										$updateUserRoleTag = $this->UserDndStatus->updateAll(array("dnd_status_id"=>$dataInput['dnd_status']['dnd_status_id'], "start_time"=> $startTime, "end_time"=>$endTime,"duration"=>$duration,"is_active"=>$params['is_dnd_active']), array("UserDndStatus.user_id"=> $dataInput['user_id']));
									}
									else
									{
										$this->UserDndStatus->saveAll(array('user_id'=>$dataInput['user_id'],'institute_id'=>$userCurrentCompany['UserEmployment']['company_id'],"dnd_status_id"=>$dataInput['dnd_status']['dnd_status_id'],"start_time"=>$startTime,"end_time"=>$endTime,"duration"=>$duration,"is_active"=>$dataInput['dnd_status']['dnd_active'],'created'=>date("Y-m-d H:i:s")));
									}
									$dndData['User'] = array('dnd_status_id'=>$params['dnd_status_id'],'dnd_name'=>$params['dnd_name'],'icon_selected'=>$params['icon_selected'], 'end_time'=>(string)$params['end_time']);
									// $dndJsonData = $dndData;
									// try{
									// 	if($dataInput['device_type'] == 'mbweb' || $dataInput['device_type'] == 'MBWEB')
									// 	{
									// 		$sendPushNotificationOnDevice = $this->sendVoipPushNotification($params);
									// 	}

									// }catch(Exception $e){
									// 	$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
									// }
									$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601, 'data'=>$dndData);
									try
									{
										$deviceType =  $this->getDeviceType();
										$deviceType['device_type'] = $deviceType;
										if($deviceType == "MBWEB" || $deviceType == "mbweb")
										{
											$params['user_id'] = $params['user_id'];
											$params['is_dnd_active'] = $params['is_dnd_active'];
											if($params['is_dnd_active'] == 1)
											{
												$params['push_on'] = 0;
											}
											else{
												if(isset($dataInput['at_work']))
													{
														$params['push_on'] = 0;
													}
													else
													{
														$params['push_on'] = 1;
													}
											}
											$sendVoipPushNotification = $this->sendVoipPushNotification($params);
										}
									}catch(Exception $e){
										$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
									}

								}catch(Exception $e){
									$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
								}
							}
							else{
								$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $qbResponceOnCall);
							}
						}

						//******* Delete Cache if profile is updated[START] ********//
						if($isProfileisUpdated == 1)
						{
							$this->CacheLastModifiedUser->updateDeletedUserCache($userCurrentCompany['UserEmployment']['company_id']);

							/*$modifiedType = "static_directory_changed";
							$userIdVal = 0;
							$conditionArr = array('company_id'=>$userCurrentCompany['UserEmployment']['company_id'],'type'=>$modifiedType);
							$chkStaticModifiedDateExists = $this->CacheLastModifiedUser->find("count", array("conditions"=> $conditionArr));
							//echo "<pre>";print_r($chkStaticModifiedDateExists1);exit();
							if($chkStaticModifiedDateExists > 0){
								$this->CacheLastModifiedUser->updateLastModifiedDate($userIdVal, $userCurrentCompany['UserEmployment']['company_id'], $modifiedType);

							}*/

							$cacObj = $this->Cache->redisConn();
							if(($cacObj['connection']) && empty($cacObj['errors'])){
								if($cacObj['robj']->exists('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id']))
								{
									$key = 'institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'];
									$cacObj = $this->Cache->delRedisKey($key);
								}
							}

							//** Set eTag values for static directory[START]
							$paramsStaticEtagUpdate['key'] = 'staticDirectoryEtag:'.$userCurrentCompany['UserEmployment']['company_id'];
							$this->updateStaticEtagData($paramsStaticEtagUpdate);
							//** Set eTag values for static directory[END]

						}
						//******* Delete Cache if profile is updated[END] ********//

						//******* Cache last modified user[START] ********//
						if($isCacheModified == 1)
						{
							// if($modifiedText == 1){
							// 	$modifiedType = "static_directory_changed";
							// 	$userIdforCache = 0;
							// }
							// else if($modifiedText == 2)
							// {
							// 	$modifiedType = "dynamic_directory_changed";
							// 	$userIdforCache = $dataInput['user_id'];
							// }
							// $getCacheLastModifierUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userIdforCache, "company_id"=> $userCurrentCompany['UserEmployment']['company_id'],"type"=>"dynamic_directory_changed")));

							$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

							if(!empty($isDynamicDataExist))
							{
								// $this->CacheLastModifiedUser->updateLastModifiedDate($dataInput['user_id'], $userCurrentCompany['UserEmployment']['company_id'], "dynamic_directory_changed");

								$this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($userId, $companyId, 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
							}
							else
							{
								$saveCacheLastModifiedData = array("user_id"=> $dataInput['user_id'], "company_id"=> $userCurrentCompany['UserEmployment']['company_id'], "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>date('Y-m-d H:i:s'));
								$this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
							}
							//** Get last modified date dynamic directory[START]
							$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($userCurrentCompany['UserEmployment']['company_id']);
							if(!empty($getDynamicDirectoryLastModified)){
								$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
								$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$userCurrentCompany['UserEmployment']['company_id'];
								$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
							}
							//** Get last modified date dynamic directory[END]
						}
						//******* Cache last modified user[END] ********//

						// }else{
					 //        $responseData = array('method_name'=> 'updateProfileLite','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
				  //        }
						//** Update Profile Image[END]
						}else{
				    		$getUserStatusResponse = $this->getUserProfileStatus($dataInput['user_id'], $dataInput['user_id']);
				    		$responseData = array('method_name'=> 'updateProfileLite','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
				    	}
				    	}else{
							$responseData = array('method_name'=> 'getUserProfile', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
						}
					}catch(Exception $e){
						$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
		}else{
			$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 08-02-2019
	I/P: $userInfo, $params
	O/P: updateValue
	Desc: Internal function updateUserRoleStatus
	-------------------------------------------------------------------------------------
	*/

	public function updateUserRoleStatus($userInfo, $params)
	{
		if(!empty($params))
		{
			//** One time token to validate for QB[START]
			$oneTimeTokenForRoleStatus = $this->genrateRandomToken($params['user_id']);
			$oneTimeTokenDataRoleStatus = array("user_id"=> $params['user_id'], "token"=> $oneTimeTokenForRoleStatus);
			$this->UserOneTimeToken->saveAll($oneTimeTokenDataRoleStatus);
			//** One time token to validate for QB[START]

			$profileData = array("UserProfile.role_status"=> "'". addslashes($params['role_status']) ."'");
			$qbResponceRoleStatus = $this->updateRoleStatusOnQb($userInfo['User']['email'],$oneTimeTokenForRoleStatus, $params['role_status']);
			if($qbResponceRoleStatus['qbsuccess'] == 1)
			{
				$isCacheModified = 1;
				$modifiedText = 2;
				$roleStatusResult = $this->UserProfile->updateFields($profileData, array("UserProfile.user_id"=> $params['user_id']));
				$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601);
			}
			else
			{
				$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $qbResponceRoleStatus);
			}
			return $responseData;
		}
	}

	/*
	-------------------------------------------------------------------------------------
	On: 08-02-2019
	I/P: $userInfo, $params
	O/P: updateValue
	Desc: Internal function updateUserAvailableStatus
	-------------------------------------------------------------------------------------
	*/


	public function updateUserAvailableStatus($userInfo, $params)
	{
		if(!empty($params))
		{
				$dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $params['user_id'])));
				//** One time token to validate for QB[START]
				$oneTimeTokenForAvailable = $this->genrateRandomToken($params['user_id']);
				$oneTimeTokenDataAvailableStatus = array("user_id"=> $params['user_id'], "token"=> $oneTimeTokenForAvailable);
				$this->UserOneTimeToken->saveAll($oneTimeTokenDataAvailableStatus);
				if(!empty($userInfo['User']['email'])){
				$tokenDetails = $this->Quickblox->quickLogin($userInfo['User']['email'], $oneTimeTokenForAvailable);
				$token = $tokenDetails->session->token;
				$user_id = $tokenDetails->session->user_id;
				$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
				$custom_data_from_api = json_decode($user_details->user->custom_data);
				$customData['userRoleStatus'] = $custom_data_from_api->status;
				$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
				$customData['isImport'] = $is_import;
				$customData['at_work'] = (isset($params['at_work']) ? (string) $params['at_work'] : "0");
				$customData['on_call'] = "0";
				if(!empty($token)){
					$qbResponceAvailableStatus = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
					$res = json_decode(json_encode($qbResponce2));
					if(!empty($res['errors'])){
	                    $responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=>$qbResponceAvailableStatus);
	                }else{
	                	$isCacheModified = 1;
	                	$modifiedText = 2;
	                	if($params['at_work'] == 0)
						{
							if(!empty($dutyLogData)){
								$this->UserDutyLog->updateAll(array("atwork_status"=> $params['at_work'], "status"=> $params['at_work']), array("user_id"=> $params['user_id']));
							}else{
								$dutyData = array("atwork_status"=> $params['at_work'], "status"=> $params['at_work'], "user_id"=> $params['user_id']);
								$this->UserDutyLog->save($dutyData);
							}
						}
						else
						{
							if(!empty($dutyLogData)){
								$this->UserDutyLog->updateAll(array("atwork_status"=> $params['at_work']), array("user_id"=> $params['user_id']));
							}else{
								$oncallData = array("atwork_status"=> $params['at_work'], "user_id"=> $params['user_id']);
								$this->UserDutyLog->save($oncallData);
							}
						}
						//********** Save available Transaction[START]
						try
						{
							$deviceType =  $this->getDeviceType();
							$transactionParams['user_id'] = $params['user_id'];
							$transactionParams['company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
							$transactionParams['type'] = "Available";
							$transactionParams['status'] = $params['at_work'];
							$transactionParams['device_type'] = $deviceType;
							$transactionParams['platform'] = "app";
							$saveAvailableTransaction = $this->saveAvailableAndOncallTransactions($transactionParams);
						}catch(Exception $e){
							$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
						}
						//********** Save available Transaction[END]
	                   $responseData = array('method_name'=> 'updateProfileLite', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_601);
	                }

				}else{
					$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=>$qbResponceAvailableStatus);
				}

			}else{
				$responseData = array('method_name'=> 'updateProfileLite', 'status'=>"0", 'response_code'=> "603", 'message'=> ERROR_603);
			}
		}
		return $responseData;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 25-10-2017
	I/P: JSON
	O/P: JSON
	Desc: Get Profile with role tags
	-------------------------------------------------------------------------------------
	*/
	public function getProfileLite()
	{
		if($this->request->is('post')){
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$deviceId = $header['device_id'];
			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$userInformation['AdminActivityLog']['message'] = "";
			$userInformation =  $this->AdminActivityLog->find('first', array('conditions' =>
		                array('AdminActivityLog.user_id' => $dataInput['loggedin_user_id']),'order'=>array('AdminActivityLog.id DESC')
		                ,'limit'=> 1));
			if($userInformation['AdminActivityLog']['message'] != "Deactivated"){
				if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1){
					if( $this->validateToken() && $this->validateAccessKey() ){
							if( $this->User->findById( $dataInput['user_id'] ) ){
								$params['User.id'] = $dataInput['user_id'];
								$this->User->recursive = 2;
								$userDataList = $this->User->userDetails( $params );
								$loggedinUserId = $dataInput['loggedin_user_id'];
								$userData = $this->filterUserFieldsLite($userDataList, $loggedinUserId);
								//** Check User enterprise subscription details[START]
									$status = 0;$expiryDate = 0;
									$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput["user_id"])));
									if(!empty($checkUserSubscribedFinal)){
										if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
										$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
										if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
										//** Check if expired
										if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
									}
									$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate)>0) ? strtotime($expiryDate):0);
									$settingData = array($subscriptionData);
									//** Check User enterprise subscription details[END]
									//** Get user role tags[START]
									$roleTagsArr = array(); $roleTagsVals = array();
									$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
									if(!empty($roleTags['Profession']['tags'])){
										$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
									}
									if(!empty($roleTagsVals)){
										foreach($roleTagsVals as $rt){
											$values = array(); $roleTagId = 0;
											$params['user_id'] = $dataInput["user_id"];
											$params['key'] = $rt;
											$roleTagUser = $this->RoleTag->userRoleTags($params);
											if(!empty($roleTagUser)){
												$roleTagId = $roleTagUser[0]['rt']['id'];
												$values = array($roleTagUser[0]['rt']['value']);
											}
											$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
										}
									}
									$userData['role_tags'] = $roleTagsArr;
									$userbatonRolestatus = array(1,2,5,7);
									$userBatonRole = $this->UserBatonRole->find("count", array("conditions"=> array("from_user"=> $dataInput['user_id'], "status"=>$userbatonRolestatus , "is_active"=>1), "group"=>array('UserBatonRole.role_id')));
									$userData['user_baton_role_count'] = $userBatonRole;
									$getUserActiveBatonRoles = $this->getUserActiveBatonRoles($dataInput['loggedin_user_id']);
									$userData['baton_roles'] = $getUserActiveBatonRoles;

									$responseData = array('method_name'=> 'getProfileLite','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> array('User'=> $userData, 'settings'=> $settingData));
					    	}
					    	else{
					    		$responseData = array('method_name'=> 'getProfileLite','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
					    	}
					    }else{
					         $responseData = array('method_name'=> 'getProfileLite','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
				         }
			        }else{
				    		$getUserStatusResponse = $this->getUserProfileStatus($dataInput['user_id'],$dataInput['loggedin_user_id']);
				    		$responseData = array('method_name'=> 'getProfileLite','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
				    	}
				}else{
					$responseData = array('method_name'=> 'getProfileLite', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
				}
	       }else{
			$responseData = array('method_name'=> 'getProfileLite','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
  //   	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		// echo json_encode(array("values"=> $encryptedData));
		// exit;
		// $encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 26-10-2016
	I/P: array()
	O/P: array()
	Desc: Get all user profile fields
	-------------------------------------------------------------------------------------
	*/
	public function filterUserFieldsLite( $ud = array(), $loggedinUserId = NULL ){
		$userData = array();
		if( !empty($ud) ){
			//** Get User Country[START]
			$userCountry = ''; $userCountryCode = '';
			if( !empty($ud['UserProfile']['country_id']) ){
				$countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
				$userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';
				$userCountryCode = !empty($countryData['Country']['country_code'])?$countryData['Country']['country_code']:'';
			}
			//** Get User Country[END]
			//** User Profession[START]
			$userProfession = '';
			if( !empty($ud['UserProfile']['profession_id']) ){
				$professionData = $this->Profession->findById( $ud['UserProfile']['profession_id'] );
				$userProfession  = !empty($professionData['Profession']['profession_type'])?$professionData['Profession']['profession_type']:'';
			}
			//** User Profession[END]

			if( $ud['User']['id'] == $loggedinUserId ){ //** Personal Information only self user.
				$userEmail = !empty($ud['User']['email']) ? $ud['User']['email']:'';
				$contactNum = !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'';
				$address = !empty($ud['UserProfile']['address']) ? $ud['UserProfile']['address']:'';
				$dob = (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'';
			}
			//** User employment history[START]
				$userEmploymentData = array(); $designationDetails = array();
				$userEmployment = $this->UserEmployment->getUserEmployment( $ud['User']['id'] );
				foreach( $userEmployment as $employment){
					$dutyStatusData = $this->UserDutyLog->userDutyStatus(array("user_id"=>$ud['User']['id'] , "company_id"=> $employment['UserEmployment']['company_id']));
					$dutyStatus = isset($dutyStatusData['UserDutyLog']['status']) ? $dutyStatusData['UserDutyLog']['status'] : 0;
					$companyIdOfuser = $employment['UserEmployment']['company_id'];
					if($companyIdOfuser == -1){
						$companyId = -1;
						$patientNumberType = $employment['Company']['patient_number_type'];
					}else{
						$companyId = !empty($employment['Company']['id']) ? $employment['Company']['id'] : (int) 0;
						$patientNumberType = $employment['Company']['patient_number_type'];
					}
					if($companyIdOfuser == -1){
						$companyName = "None";
						$patientNumberType = $employment['Company']['patient_number_type'];
					}else{
						$companyName = !empty($employment['Company']['company_name']) ? $employment['Company']['company_name'] : '';
						$patientNumberType = $employment['Company']['patient_number_type'];
					}
					$isCompanyAssignPending = 0;
					if($companyId != -1){
						if($employment['Company']['is_subscribed'] == 1 && $employment['Company']['status']==1){
							$isCompanyAssignCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $ud['User']['email'], "company_id"=> $companyId, "status"=> 0)));
						}
					}
					if($isCompanyAssignCheck > 0){ $isCompanyAssignPending = 1; }
					$userEmploymentData[] = array("id"=> !empty($employment['UserEmployment']['id']) ? (int) $employment['UserEmployment']['id'] : (int) 0, "company_id"=> $companyId ,"patient_number_type"=>$patientNumberType, "company_name"=> $companyName,"is_current"=> !empty($employment['UserEmployment']['is_current'])? (int) $employment['UserEmployment']['is_current']:0,"duty_status"=> $dutyStatus, "is_company_approval_pending"=> (int) $isCompanyAssignPending);
				}
			//** User employment history[END]
			//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
				$qbInfo = array("id"=> (int) $qbId);
			//** Get QB details [END]
			//** Get role status [START]
			$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
			//** Get role status [END]
			//** Get Current Employment[START]
				$userCurrentCompanyName = "";
				$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "is_current"=> 1)));
				$companyId = $userCurrentCompany['UserEmployment']['company_id'];
				if($companyId == -1){
					$userCurrentCompanyName = "None";
				}else if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
					$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
					if(!empty($userCompanyDetails)){
						$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
					}
				}
				//** Get Current Employment[END]
				//** On call Access profession wise[START]
				$oncallAccess = 0;
				/*$professionIds = array(1,2,3,8,12,13,14,19);
				if(in_array($ud['UserProfile']['profession_id'], $professionIds)){
					$oncallAccess = 1;
				}*/
				$oncallAccessCount = $this->OncallAccessProfessions->find("count", array("conditions"=> array("profession_id"=> $ud['UserProfile']['profession_id'], "status"=> 1)));
				if($oncallAccessCount>0){
					$oncallAccess = 1;
				}
				$atWorkStatus = 0;
				$dutyCheck = $this->UserDutyLog->getStatusInfo($ud['User']['id']);
				foreach ($dutyCheck AS $value) {
					$atWorkStatus = $value['UserDutyLog']['atwork_status'];
				}
				$userDndParams['user_id'] = $ud['User']['id'];
				$getUserDndStatus = $this->UserDndStatus->getUserDndStatus_pre($userDndParams);
				$dndStatusOptions = $this->DndStatusOption->getDndStatusOptions($getUserDndStatus['UserDndStatus']['dnd_status_id']);
				if(!empty($getUserDndStatus))
				{
					if($getUserDndStatus['UserDndStatus']['is_active'])
					{

						$userDndDetail['dnd_status'] = array('dnd_status_id'=>$getUserDndStatus['UserDndStatus']['dnd_status_id'], 'dnd_name'=>$dndStatusOptions['DndStatusOption']['dnd_name'],
							'icon_selected'=>$dndStatusOptions['DndStatusOption']['icon_selected'], 'end_time'=>$getUserDndStatus['UserDndStatus']['end_time']);

					}
				}
				//** On call Access profession wise[END]
			if(!empty($qbId)){
			$userData = array(
								'user_id'=> !empty($ud['User']['id']) ? (int) $ud['User']['id']:'',
								'email'=>	stripslashes($userEmail),
								'status' => !empty($ud['User']['status']) ? (int) $ud['User']['status']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'thumbnail_img'=> !empty($ud['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['thumbnail_img']:'',
								'country_code'=> !empty($userCountryCode) ? $userCountryCode : '',
								'Profession'=> array('id'=>!empty($ud['UserProfile']['profession_id'])?$ud['UserProfile']['profession_id']:"", 'profession_type'=> !empty($userProfession) ? $userProfession:''),
								'phone'=> array('mobile_no'=> $contactNum, 'is_mobile_verified'=> !empty($ud['UserProfile']['contact_no_is_verified']) ? (int) $ud['UserProfile']['contact_no_is_verified']: 0),
								'current_employment'=> $userCurrentCompanyName,
								'Company'=> !empty($userEmploymentData) ? $userEmploymentData : array(),
								"qb_details"=> $qbInfo,
								"role_status"=> !empty($userRoleStatusData['UserProfile']['role_status']) ? $userRoleStatusData['UserProfile']['role_status']:'',
								"oncall_access"=> (int) $oncallAccess,
								"at_work"=> $atWorkStatus,
								"is_dnd_active"=>isset($getUserDndStatus['UserDndStatus']['is_active']) ? $getUserDndStatus['UserDndStatus']['is_active'] : 0,
							);
			if($getUserDndStatus['UserDndStatus']['is_active'])
				$userData = array_merge($userData, $userDndDetail);
			}
		}
		return $userData;
	}

	/*
	-------------------------------------------------------------------------------------------------------
	ON:
	I/P:
	O/P:
	Desc:
	-------------------------------------------------------------------------------------------------------
	*/
	public function getUserPrivacySettings(){
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$userInformation =  $this->AdminActivityLog->find('first', array('conditions' =>
		                array('AdminActivityLog.user_id' => $dataInput['user_id']),'order'=>array('AdminActivityLog.id DESC')
		                ,'limit'=> 1));
			if($userInformation['AdminActivityLog']['message'] != "Deactivated"){
				if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1){
					if( $this->validateToken() && $this->validateAccessKey() ){
							if( $this->User->findById( $dataInput['user_id'] ) ){
			//** Check Email visibility settings[START]
				$userEmail = 2;
				$emailVisibilityCheck = $this->UserVisibilitySetting->find("first", array("conditions"=> array("visibility_setting_type"=> "Email", "user_id"=> $dataInput['user_id'])));
				if(!empty($emailVisibilityCheck)){
					$userEmail = $emailVisibilityCheck['UserVisibilitySetting']['visibility_setting_value'];
				}

				//***********This check is for directory access(Contact request removal)[START]***********//
				if($userEmail == 1)
				{
					$userEmail = 2;
				}
				//***********This check is for directory access(Contact request removal)[END]***********//
				if($emailVisibilityCheck['UserVisibilitySetting']['visibility_setting_value'] == NULL)
				{
					$userEmail = 2;
				}
			//** Check Email visibility settings[END]

			//** Check Mobile visibility settings[START]
				$contactNum = 3;
				$mobileVisibilityCheck = $this->UserVisibilitySetting->find("first", array("conditions"=> array("visibility_setting_type"=> "Mobile", "user_id"=> $userDetails['User']['id'])));
				if(!empty($mobileVisibilityCheck)){
					$contactNum = $mobileVisibilityCheck['UserVisibilitySetting']['visibility_setting_value'];
				}
				//***********This check is for directory access(Contact request removal)[START]***********//
				if($contactNum == 1)
				{
					$contactNum = 2;
				}
				//***********This check is for directory access(Contact request removal)[END]***********//
				$visibilityData = array("phone"=> $contactNum, "email"=> $userEmail);
				$responseData = array('method_name'=> 'getUserPrivacySettings','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, "data"=> $visibilityData);
			//** Check Mobile visibility settings[END]
		}
					    }else{
					         $responseData = array('method_name'=> 'getUserPrivacySettings','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
				         }
			        }else{
				    		$getUserStatusResponse = $this->getUserProfileStatus($dataInput['user_id'],$dataInput['user_id']);
				    		$responseData = array('method_name'=> 'getUserPrivacySettings','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
				    	}
				}else{
					$responseData = array('method_name'=> 'getUserPrivacySettings', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
				}
	       }else{
			$responseData = array('method_name'=> 'getUserPrivacySettings','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 31-10-2017
	I/P: JSON
	O/P: JSON
	Desc: Get other user Profile with role tags
	-------------------------------------------------------------------------------------
	*/
	public function getOtherUserProfileLite()
	{
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$userInformation =  $this->AdminActivityLog->find('first', array('conditions' =>
		                array('AdminActivityLog.user_id' => $dataInput['loggedin_user_id']),'order'=>array('AdminActivityLog.id DESC')
		                ,'limit'=> 1));
			if($userInformation['AdminActivityLog']['message'] != "Deactivated"){
				if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1){
					// if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if( $this->validateToken() && $this->validateAccessKey() ){
							if( $this->User->findById( $dataInput['user_id'] ) ){
								$params['User.id'] = $dataInput['user_id'];
								$loggedinUserId = $dataInput['loggedin_user_id'];
								$this->User->recursive = 2;
								$userDataList = $this->User->userDetails( $params );
								$userData = $this->filterOtherUserFieldsLite($userDataList, $loggedinUserId);
									//** Get user role tags[START]
									$roleTagsArr = array(); $roleTagsval = array();
									$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
									if(!empty($roleTags['Profession']['tags'])){
										$roleTagsval = explode(",", $roleTags['Profession']['tags']);
									}
									if(!empty($roleTagsval)){
										foreach($roleTagsval as $rt){
											$values = array(); $roleTagId = 0;
											$params['user_id'] = $dataInput["user_id"];
											$params['key'] = $rt;
											$roleTagUser = $this->RoleTag->userRoleTags($params);
											if(!empty($roleTagUser)){
												$roleTagId = $roleTagUser[0]['rt']['id'];
												$values = array($roleTagUser[0]['rt']['value']);
											}
											$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
										}
									}
									$userData['role_tags'] = $roleTagsArr;
									//**  Get user role tags[END]
									$responseData = array('method_name'=> 'getOtherUserProfileLite','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> array('User'=> $userData));
					    	}
					    	else{
					    		$responseData = array('method_name'=> 'getOtherUserProfileLite','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
					    	}
					    }else{
					         $responseData = array('method_name'=> 'getOtherUserProfileLite','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
				         }
			        }else{
				    		$getUserStatusResponse = $this->getUserProfileStatus($dataInput['user_id'],$dataInput['loggedin_user_id']);
				    		$responseData = array('method_name'=> 'getOtherUserProfileLite','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
				    	}
				}else{
					$responseData = array('method_name'=> 'getOtherUserProfileLite', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
				}
	       }else{
			$responseData = array('method_name'=> 'getOtherUserProfileLite','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	echo json_encode($responseData);
    	exit();
	}

	/*
	-------------------------------------------------------------------------------------
	On: 31-10-2017
	I/P: JSON
	O/P: JSON
	Desc: Get other user Profile JSON data
	-------------------------------------------------------------------------------------
	*/
	public function filterOtherUserFieldsLite( $ud = array(), $loggedinUserId=NULL){
		$userData = array();
		if( !empty($ud) ){
			//** Get User Country[START]
			$userCountry = ''; $userCountryCode = '';
			if( !empty($ud['UserProfile']['country_id']) ){
				$countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
				$userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';
				$userCountryCode = !empty($countryData['Country']['country_code'])?$countryData['Country']['country_code']:'';
			}
			//** Get User Country[END]
			//** User Profession[START]
			$userProfession = '';
			if( !empty($ud['UserProfile']['profession_id']) ){
				$professionData = $this->Profession->findById( $ud['UserProfile']['profession_id'] );
				$userProfession  = !empty($professionData['Profession']['profession_type'])?$professionData['Profession']['profession_type']:'';
			}
			//** User Profession[END]
			//** Find User colleague of user[START]
			$colleagueStatus = 0; $requestType = '';
			// $myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'])));

			$myColleague = $this->UserColleague->find("first", array('fields' => 'UserColleague.status,UserColleague.message,UserColleague.updated', "conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'])));

			// $meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId)));

			$meColleague = $this->UserColleague->find("first", array('fields' => 'UserColleague.status,UserColleague.message,UserColleague.updated', "conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId)));

			if( !empty($myColleague) ){
				$colleagueStatus = $myColleague['UserColleague']['status'];
				$requestType = "from_me";
				$colleagueMessage = !empty($myColleague['UserColleague']['message']) ? $myColleague['UserColleague']['message'] : " ";
				$sentAt = !empty($myColleague['UserColleague']['updated']) ? strtotime($myColleague['UserColleague']['updated']) : '' ;
			}elseif( !empty($meColleague) ){
				$colleagueStatus = $meColleague['UserColleague']['status'];
				$requestType = "to_me";
				$colleagueMessage = !empty($meColleague['UserColleague']['message']) ? $meColleague['UserColleague']['message'] : " ";
				$sentAt = !empty($meColleague['UserColleague']['updated']) ? strtotime($meColleague['UserColleague']['updated']) : '' ;
			}
			//** Find User colleague of user[END]
			//** Check visibility[START]
			$emailVisibilityVal = 2; $mobileVisibilityVal = 3;
			$emailVisibilityCheck = $this->UserVisibilitySetting->find("first", array("conditions"=> array("visibility_setting_type"=> "Email", "user_id"=> $ud['User']['id'])));
			if(!empty($emailVisibilityCheck)){
				$emailVisibilityVal = (int) $emailVisibilityCheck['UserVisibilitySetting']['visibility_setting_value'];
			}

			$mobileVisibilityCheck = $this->UserVisibilitySetting->find("first", array("conditions"=> array("visibility_setting_type"=> "Mobile", "user_id"=> $ud['User']['id'])));
			if(!empty($mobileVisibilityCheck)){
				$mobileVisibilityVal = (int) $mobileVisibilityCheck['UserVisibilitySetting']['visibility_setting_value'];
			}

			if( $emailVisibilityVal != 3 ){ //** Personal Information only self user.
				$userEmail = !empty($ud['User']['email']) ? $ud['User']['email']:'';
			}
			if($mobileVisibilityVal != 3){
				$contactNum = !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'';
			}

			// if( $emailVisibilityVal == 2 ){ //** if set for colleague also check both are coleague.
			// 	if($colleagueStatus == 1){
			// 		$userEmail = !empty($ud['User']['email']) ? $ud['User']['email']:'';
			// 	}else{
			// 		$userEmail = "";
			// 	}
			// }
			// if($mobileVisibilityVal == 2){
			// 	if($colleagueStatus == 1){
			// 		$contactNum = !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'';
			// 	}else{
			// 		$contactNum = "";
			// 	}
			// }
			//** Check visibility[END]
			//** User employment history[START]
				$userEmploymentData = array(); $designationDetails = array();
				$userEmployment = $this->UserEmployment->getUserEmployment( $ud['User']['id'] );
				foreach( $userEmployment as $employment){
					$dutyStatusData = $this->UserDutyLog->userDutyStatus(array("user_id"=>$ud['User']['id'] , "company_id"=> $employment['UserEmployment']['company_id']));
					$dutyStatus = isset($dutyStatusData['UserDutyLog']['status']) ? $dutyStatusData['UserDutyLog']['status'] : 0;
					$companyIdOfuser = $employment['UserEmployment']['company_id'];
					if($companyIdOfuser == -1){
						$companyId = -1;
					}else{
						$companyId = !empty($employment['Company']['id']) ? $employment['Company']['id'] : (int) 0;
					}
					if($companyIdOfuser == -1){
						$companyName = "None";
					}else{
						$companyName = !empty($employment['Company']['company_name']) ? $employment['Company']['company_name'] : '';
					}
					$isCompanyAssignPending = 0;
					if($companyId != -1){
						if($employment['Company']['is_subscribed'] == 1 && $employment['Company']['status']==1){
							$isCompanyAssignCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $ud['User']['email'], "company_id"=> $companyId, "status"=> 0)));
						}
					}
					if($isCompanyAssignCheck > 0){ $isCompanyAssignPending = 1; }
					$userEmploymentData[] = array("id"=> !empty($employment['UserEmployment']['id']) ? (int) $employment['UserEmployment']['id'] : (int) 0, "company_id"=> $companyId ,"company_name"=> $companyName, "is_current"=> !empty($employment['UserEmployment']['is_current'])? (int) $employment['UserEmployment']['is_current']:0,"duty_status"=> $dutyStatus, "is_company_approval_pending"=> (int) $isCompanyAssignPending);
				}
			//** User employment history[END]
			//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
				$qbInfo = array("id"=> $qbId);
			//** Get QB details [END]
			//** Get role status [START]
			$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
			//** Get role status [END]

			//** User Available data[START]
				$atWorkStatus = 0;
				$dutyCheck = $this->UserDutyLog->getStatusInfo($ud['User']['id']);
				foreach ($dutyCheck AS $value) {
					$atWorkStatus = $value['UserDutyLog']['atwork_status'];
				}

				// $getUserDndStatus = $this->UserDndStatus->getUserDndStatus($ud['User']['id']);
				// $dndStatusOptions = $this->DndStatusOption->getDndStatusOptions($getUserDndStatus['UserDndStatus']['dnd_status_id']);
				// if(!empty($getUserDndStatus))
				// {
				// 	if($getUserDndStatus['UserDndStatus']['is_active'])
				// 	{

				// 		$userDndDetail = array('dnd_status_id'=>$getUserDndStatus['UserDndStatus']['dnd_status_id'], 'dnd_name'=>$dndStatusOptions['DndStatusOption']['dnd_name'], 'end_time'=>$getUserDndStatus['UserDndStatus']['end_time']);

				// 	}
				// }
				//** User Available data[END]
			if(!empty($qbId)){
			$userData = array(
								'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
								//'email'=>	stripslashes($userEmail),
								'status' => !empty($ud['User']['status']) ? (string) $ud['User']['status']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'thumbnail_img'=> !empty($ud['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['thumbnail_img']:'',
								'country_code'=> !empty($userCountryCode) ? $userCountryCode : '',
								'Profession'=> array('id'=>!empty($ud['UserProfile']['profession_id'])?$ud['UserProfile']['profession_id']:"", 'profession_type'=> !empty($userProfession) ? $userProfession:''),
								'Colleague'=> array("status"=> $colleagueStatus, "request_by"=> $requestType, "contact_custom_msg"=>$colleagueMessage, "sent_at"=>$sentAt),
								'Company'=> !empty($userEmploymentData) ? $userEmploymentData : array(),
								"qb_details"=> $qbInfo,
								"role_status"=> !empty($userRoleStatusData['UserProfile']['role_status']) ? $userRoleStatusData['UserProfile']['role_status']:'',
								"at_work"=> $atWorkStatus
							);
				if(isset($userEmail) && !empty($userEmail)){
					$userData['email'] = stripslashes($userEmail);
				}

				if(isset($contactNum) && !empty($contactNum)){
					$userData['phone'] = array('mobile_no'=> $contactNum);
				}
				$userDndParams['user_id'] = $ud['User']['id'];
				$getUserDndStatus = $this->UserDndStatus->getUserDndStatus_pre($userDndParams);
				$dndStatusOptions = $this->DndStatusOption->getDndStatusOptions($getUserDndStatus['UserDndStatus']['dnd_status_id']);
				$getUserActiveBatonRoles = $this->getOtherUserActiveBatonRoles($ud['User']['id']);
				if(!empty($getUserDndStatus))
				{
					if($getUserDndStatus['UserDndStatus']['is_active']){
						$userData['dnd_status'] = array('dnd_status_id'=>$getUserDndStatus['UserDndStatus']['dnd_status_id'], 'dnd_name'=>$dndStatusOptions['DndStatusOption']['dnd_name'], 'end_time'=>$getUserDndStatus['UserDndStatus']['end_time']);
					}
				}
				$userData['baton_roles'] = $getUserActiveBatonRoles;

			}
		}
		return $userData;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 01-11-2017
	I/P: JSON
	O/P: JSON
	Desc: Set tour guide completion data
	-------------------------------------------------------------------------------------
	*/
	public function setTourGuideCompletion(){
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$userInformation =  $this->AdminActivityLog->find('first', array('conditions' =>
		                array('AdminActivityLog.user_id' => $dataInput['user_id']),'order'=>array('AdminActivityLog.id DESC')
		                ,'limit'=> 1));
			if($userInformation['AdminActivityLog']['message'] != "Deactivated"){
				if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1){
					if( $this->validateToken() && $this->validateAccessKey() ){
						//** Set tour guide data[START]
							$headerVals = $this->getHeaderValues();
							$device_type = $headerVals['device_type'];
							$quer = array('user_id'=>$dataInput['user_id'],'device_type'=>$device_type,'status'=>1);
							$this->TourGuideCompletionVisit->saveAll($quer);
							$responseData = array('method_name'=> 'setTourGuideCompletion','status'=>'1','response_code'=>'200', 'message'=> ERROR_200);
						//** Set tour guide data[END]
					    }else{
					         $responseData = array('method_name'=> 'setTourGuideCompletion','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
				         }
			        }else{
				    		$getUserStatusResponse = $this->getUserProfileStatus($dataInput['user_id'],$dataInput['user_id']);
				    		$responseData = array('method_name'=> 'setTourGuideCompletion','status'=>'0','response_code'=>$getUserStatusResponse['response_code'], 'message'=> $getUserStatusResponse['message']);
				    	}
				}else{
					$responseData = array('method_name'=> 'setTourGuideCompletion', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
				}
	       }else{
			$responseData = array('method_name'=> 'setTourGuideCompletion','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
		exit;
	}



	/*
	------------------------------------------------------------------------------
	ON: 09-11-2017
	I/P: JSON
	O/P: JSON
	Desc:
	------------------------------------------------------------------------------
	*/

	public function getGeofenceData()
	{
		$responseData = array();
		$userGeoData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if( $this->validateToken() && $this->validateAccessKey() ){
				if(! empty($dataInput['user_id']))
				{
					$userCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'], "is_current"=> 1)));
					$companyGeoData = $this->CompanyGeofenceParameter->find("first", array("conditions"=> array("company_id"=> $userCompany['UserEmployment']['company_id'])));
					if(!empty($companyGeoData)){
						$userGeoData['GeoData'] = array("company_id"=> $companyGeoData['CompanyGeofenceParameter']['company_id'], "latitude"=> $companyGeoData['CompanyGeofenceParameter']['latitude'], "longitude"=> $companyGeoData['CompanyGeofenceParameter']['longitude'], "radius"=> $companyGeoData['CompanyGeofenceParameter']['radius']);
					}else{
						$userGeoData = (object) array();
					}
					$responseData = array('method_name'=> 'getGeofenceData', 'status'=>"1",'response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userGeoData);

				}
				else
				{
					$responseData = array('method_name'=> 'getGeofenceData', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}
			else{
				$responseData = array('method_name'=> 'getGeofenceData', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}

		}else{
			$responseData = array('method_name'=> 'getGeofenceData', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	------------------------------------------------------------------------------
	ON:
	I/P:
	O/P:
	Desc:
	------------------------------------------------------------------------------
	*/
	public function getStaticDirectoryData(){
		$dataInput = $this->request->input('json_decode', true) ;
		$encryptedData = $this->Common->decryptData( $dataInput['values'] );
		$dataInput = json_decode($encryptedData, true);
		$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
		$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId)));
		$params['loggedin_user_countryid'] = $userDetails['UserProfile']['country_id'];
		$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
		$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
		$enterpriseUserCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']),"company_id"=> $userCurrentCompany['UserEmployment']['company_id'],"status"=> 1)));
		$companySubscriptionCheck=$this->CompanyName->find("count", array("conditions"=> array("id"=>$userCurrentCompany['UserEmployment']['company_id'], "status"=>1,"is_subscribed"=>1)));
		//** Get Pending Colleagues[START]
		$pendingColleagueParams['loggedin_user_id'] = $loggedinUserId;
		$pendingColleagues = $this->UserColleague->getPendingColleague($pendingColleagueParams);
		$formattedPendingColleagues = $this->directoryFields( $pendingColleagues , $loggedinUserId );
		$userData['pending_contacts'] = $formattedPendingColleagues;
		//** Get Pending Colleagues[END]
		if($companySubscriptionCheck ==1 && $enterpriseUserCheck ==0){
			/*$responseData = array('method_name'=> 'oncallUserListLight','status'=>'0','response_code'=>'655','message'=> ERROR_655);*/
			$userData['on_duty_users'] = array();
			$userData['off_duty_users'] = array();
			$userData['diretory_access'] = 655;
			$userData['message'] = ERROR_655;
			$responseData = array('method_name'=> 'oncallUserListLight','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData);
		}else{
			$cacObj = $this->Cache->redisConn();
			if(($cacObj['connection']) && empty($cacObj['errors'])){
				if($cacObj['robj']->exists('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'])){
					$dataJson = $cacObj['robj']->get('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id']);
					$userData = json_decode($dataJson, true);

				}else{
				//** Directory Users
				$userLists = array();
				if($params['user_current_company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
					$userLists = $this->User->oncallUserListsLight( $params, $loggedinUserId );
				}
				//echo "<pre>"; print_r($userLists);die;
			}
				//** Set Directory on cache
			$key = 'institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'];
				$cacObj['robj']->set($key,json_encode($userData, true));
			}
				$responseData = array('method_name'=> 'oncallUserListLight','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------
	ON:
	I/P:
	O/P:
	Desc:
	------------------------------------------------------------------------------
	*/

	public function updateCacheForCurrentCompany( $params = array())
	{
		//Update cache for deleted user //
		if(! empty($params))
		{
			$modifiedType = "deleted_user";
			$lastModified = date('Y-m-d H:i:s');
			if($params['delete_check'] == 1)
			{
				$delete_status = 1;
			}
			else if($params['delete_check'] == 2)
			{
				$delete_status = 0;
			}
			$getDeletedTypeUser = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "company_id"=> $params['company_id'], "type"=>$modifiedType)));

	        if(count($getDeletedTypeUser) > 0)
	        {
	            $this->CacheLastModifiedUser->updateDeletedUser($params['user_id'], $params['company_id'], $modifiedType, $deletedStatus =0);
	        }
	        else
	        {
	            $saveCacheLastModifiedData = array("user_id"=> $params['user_id'], "company_id"=> $params['company_id'], "type"=>$modifiedType, "status"=>$delete_status, "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }

	        //delete cache for Current Company
	        $cacObj = $this->Cache->redisConn();
			if(($cacObj['connection']) && empty($cacObj['errors'])){
				if($cacObj['robj']->exists('institutionDirectory:'.$params['company_id']))
				{
					$key = 'institutionDirectory:'.$params['company_id'];
					$cacObj = $this->Cache->delRedisKey($key);
				}
			}

			//Update cache for Static user //
			$modifiedTypeSt = "static_directory_changed";
			$userIdVal = 0;
			$dataForStaticCache = array('company_id'=>$params['company_id'],'type'=>$modifiedTypeSt);
			$chkStaticModifiedDateExists = $this->CacheLastModifiedUser->find("count", array("conditions"=> $dataForStaticCache));
			//echo "<pre>";print_r($chkStaticModifiedDateExists);exit();
			if($chkStaticModifiedDateExists > 0){
				$this->CacheLastModifiedUser->updateLastModifiedDate($userIdVal, $params['company_id'], $modifiedTypeSt);
			}

			//Update cache for dynamic user //
			$dynamicModifiedType = "dynamic_directory_changed";
			$getDynamicCacheData = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "company_id"=> $params['company_id'], "type"=>$dynamicModifiedType)));

	        if(count($getDynamicCacheData) > 0)
	        {
	            $this->CacheLastModifiedUser->updateLastModifiedDate($params['user_id'], $params['company_id'], $dynamicModifiedType);
	        }
	        else
	        {
	            $saveDynamicCacheLastModifiedData = array("user_id"=> $params['user_id'], "company_id"=> $params['company_id'], "type"=>$dynamicModifiedType, "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveDynamicCacheLastModifiedData);
	        }
		}

	}

	/*

	*/
	// public function cacheDeleteOnInstitutionChange($userId=NULL, $currentCompanyId=NULL, $previousCompanyId){
	// 	if(!empty($userId)){
	// 		$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));

	// 		//*******
	// 		$modifiedType = "deleted_user";
	//         $lastModified = date('Y-m-d H:i:s');
	//         $getPreviousCompanyData = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $previousCompanyId, "type"=>$modifiedType)));
	//         //echo "<pre>";print_r($getCacheLastModifiedUser);exit();
	//         if(!empty($getPreviousCompanyData))
	//         {
	//             $this->CacheLastModifiedUser->updateDeletedUser($userId, $previousCompanyId, $modifiedType, 1);
	//         }
	//         else
	//         {
	//             $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $previousCompanyId, "type"=>$modifiedType, "status"=>"1", "last_modified"=>$lastModified);
	//             $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	//         }

	//         $getCurrentCompanyData = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $currentCompanyId, "type"=>$modifiedType)));

	//         if(!empty($getCurrentCompanyData))
	//         {
	//             $this->CacheLastModifiedUser->updateDeletedUser($userId, $currentCompanyId, $modifiedType, 0);
	//         }
	//         else
	//         {
	//             $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $currentCompanyId, "type"=>$modifiedType, "status"=>"0", "last_modified"=>$lastModified);
	//             $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	//         }
	//         //********

	//         //********
	//         $dataParamsPrevious['company_id'] = isset($previousCompanyId) ? $previousCompanyId : 0;
	//         $dataParamsPrevious['email'] = stripslashes($userDetails['User']['email']);
	//         $dataParamsPrevious['user_id'] = isset($userDetails['User']['id']) ? $userDetails['User']['id']: 0;
	// 		$checkPreviousCompanySubscription = $this->CompanyName->getComapanySubscriptionDetails($dataParamsPrevious);
	// 		if($checkPreviousCompanySubscription > 0)
	// 		{
	// 			$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParamsPrevious);
	// 			if(!empty($checkUserForPaid))
	// 			{
	// 				//***** Static cache update And remove cache **********//
	// 				$updateStaticCache = $this->saveStaticData($dataParamsPrevious);
	// 			}
	// 		}
	// 		else
	// 		{
	// 			$updateStaticCache = $this->saveStaticData($dataParamsPrevious);
	// 		}

	// 		$dataParamsCurrent['company_id'] = isset($currentCompanyId) ? $currentCompanyId : 0;
	// 		$dataParamsCurrent['email'] = stripslashes($userDetails['User']['email']);
	// 		$dataParamsCurrent['user_id'] = isset($userDetails['User']['id']) ? $userDetails['User']['id']: 0;
	// 		$checkCurrentCompanySubscription = $this->CompanyName->getComapanySubscriptionDetails($dataParamsCurrent);
	// 		if($checkCurrentCompanySubscription > 0)
	// 		{
	// 			$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParamsCurrent);
	// 			if(!empty($checkUserForPaid))
	// 			{
	// 				$updateStaticCache = $this->saveStaticData($dataParamsCurrent);
	// 			}
	// 		}
	// 		else
	// 		{
	// 			$updateStaticCache = $this->saveStaticData($dataParamsCurrent);
	// 		}
	//         //********
	// 	}
	// }


	// /*


	// */

	// public function saveStaticData($dataParams=array())
	// {
	// 	$companyId = $dataParams['company_id'];
	// 	$userId = $dataParams['user_id'];
	// 	$lastModified = date('Y-m-d H:i:s');
	// 	if(! empty($dataParams))
	// 	{
	// 		//****** Update Static Cache
	// 		$staticCond = array('company_id'=>$companyId,'type'=>'static_directory_changed');
	// 		$isStaticDataExist = $this->CacheLastModifiedUser->find("count", array("conditions"=> $staticCond));
	// 		if($isStaticDataExist > 0){
	// 			$this->CacheLastModifiedUser->updateLastModifiedDate(0, $companyId, 'static_directory_changed');
	// 		}
	// 		else
	// 		{
	// 			$saveStaticData = array("user_id"=> 0, "company_id"=> $companyId, "type"=>'static_directory_changed', "status"=>"1", "last_modified"=>$lastModified);
	// 			$this->CacheLastModifiedUser->saveAll($saveStaticData);
	// 		}
	// 		//** Set eTag values for static directory[START]
	// 		$paramsStaticEtagUpdate['key'] = 'staticDirectoryEtag:'.$companyId;
	// 		$this->updateStaticEtagData($paramsStaticEtagUpdate);
	// 		//** Set eTag values for static directory[END]
	// 		//****** Update Dynamic Cache
	// 		// $isDynamicDataExist = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed')));

	// 		$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

	//         if($isDynamicDataExist > 0)
	//         {
	//             // $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');

	//             $this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($userId, $companyId, 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
	//         }
	//         else
	//         {
	//             $saveDynamicData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed', "status"=>"1", "last_modified"=>$lastModified);
	//             $this->CacheLastModifiedUser->saveAll($saveDynamicData);
	//         }
	//         //** Get last modified date dynamic directory[START]
	// 		$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
	// 		if(!empty($getDynamicDirectoryLastModified)){
	// 			$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
	// 			$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
	// 			$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
	// 		}
	// 		//** Get last modified date dynamic directory[END]

	//         //****** Delete Cache
	// 		$cacheObj = $this->Cache->redisConn();
	// 		if(($cacheObj['connection']) && empty($cacheObj['errors'])){
	// 			if($cacheObj['robj']->exists('institutionDirectory:'.$companyId))
	// 			{
	// 				$key = 'institutionDirectory:'.$companyId;
	// 				$cacheObj = $this->Cache->delRedisKey($key);
	// 			}
	// 		}
	// 	}
	// }

	public function cacheDeleteOnInstitutionChange($userId=NULL, $currentCompanyId=NULL, $previousCompanyId){
		if(!empty($userId)){
			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));

			//*******
			$modifiedType = "deleted_user";
	        $lastModified = date('Y-m-d H:i:s');
	        $getPreviousCompanyData = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $previousCompanyId, "type"=>$modifiedType)));
	        //echo "<pre>";print_r($getCacheLastModifiedUser);exit();
	        if(!empty($getPreviousCompanyData))
	        {
	            $this->CacheLastModifiedUser->updateDeletedUser($userId, $previousCompanyId, $modifiedType, 1);
	        }
	        else
	        {
	            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $previousCompanyId, "type"=>$modifiedType, "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }

	        $getCurrentCompanyData = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $currentCompanyId, "type"=>$modifiedType)));

	        if(!empty($getCurrentCompanyData))
	        {
	            $this->CacheLastModifiedUser->updateDeletedUser($userId, $currentCompanyId, $modifiedType, 0);
	        }
	        else
	        {
	            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $currentCompanyId, "type"=>$modifiedType, "status"=>"0", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }
	        //********

	        //********
	        $dataParamsPrevious['company_id'] = isset($previousCompanyId) ? $previousCompanyId : 0;
	        $dataParamsPrevious['email'] = stripslashes($userDetails['User']['email']);
	        $dataParamsPrevious['user_id'] = isset($userDetails['User']['id']) ? $userDetails['User']['id']: 0;
			$checkPreviousCompanySubscription = $this->CompanyName->getComapanySubscriptionDetails($dataParamsPrevious);
			if($checkPreviousCompanySubscription > 0)
			{
				$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParamsPrevious);
				if(!empty($checkUserForPaid))
				{
					//***** Static cache update And remove cache **********//
					$updateStaticCache = $this->saveStaticData($dataParamsPrevious);
				}
			}
			else
			{
				$updateStaticCache = $this->saveStaticData($dataParamsPrevious);
			}

			$dataParamsCurrent['company_id'] = isset($currentCompanyId) ? $currentCompanyId : 0;
			$dataParamsCurrent['email'] = stripslashes($userDetails['User']['email']);
			$dataParamsCurrent['user_id'] = isset($userDetails['User']['id']) ? $userDetails['User']['id']: 0;
			$checkCurrentCompanySubscription = $this->CompanyName->getComapanySubscriptionDetails($dataParamsCurrent);
			if($checkCurrentCompanySubscription > 0)
			{
				$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParamsCurrent);
				if(!empty($checkUserForPaid))
				{
					$updateStaticCache = $this->saveStaticData($dataParamsCurrent);
				}
			}
			else
			{
				$updateStaticCache = $this->saveStaticData($dataParamsCurrent);
			}
	        //********
		}
	}


	/*


	*/

	public function saveStaticData($dataParams=array())
	{
		$companyId = $dataParams['company_id'];
		$userId = $dataParams['user_id'];
		$lastModified = date('Y-m-d H:i:s');
		if(! empty($dataParams))
		{
			//****** Update Static Cache
			$staticCond = array('company_id'=>$companyId,'type'=>'static_directory_changed');
			$isStaticDataExist = $this->CacheLastModifiedUser->find("count", array("conditions"=> $staticCond));
			if($isStaticDataExist > 0){
				$this->CacheLastModifiedUser->updateLastModifiedDate(0, $companyId, 'static_directory_changed');
			}
			else
			{
				$saveStaticData = array("user_id"=> 0, "company_id"=> $companyId, "type"=>'static_directory_changed', "status"=>"1", "last_modified"=>$lastModified);
				$this->CacheLastModifiedUser->saveAll($saveStaticData);
			}
			//** Set eTag values for static directory[START]
			$paramsStaticEtagUpdate['key'] = 'staticDirectoryEtag:'.$companyId;
			$this->updateStaticEtagData($paramsStaticEtagUpdate);
			//** Set eTag values for static directory[END]
			//****** Update Dynamic Cache
			// $isDynamicDataExist = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed')));

			$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

			if(!empty($isDynamicDataExist))
	        {
	        	$this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($userId, $companyId, 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);

	            // $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');
	        }
	        else
	        {
	            $saveDynamicData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed', "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveDynamicData);
	        }

	        // if($isDynamicDataExist > 0)
	        // {
	        //     $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');
	        // }
	        // else
	        // {
	        //     $saveDynamicData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed', "status"=>"1", "last_modified"=>$lastModified);
	        //     $this->CacheLastModifiedUser->saveAll($saveDynamicData);
	        // }
	        //** Get last modified date dynamic directory[START]
			$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
			if(!empty($getDynamicDirectoryLastModified)){
				$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
				$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
				$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
			}
			//** Get last modified date dynamic directory[END]

	        //****** Delete Cache
			$cacheObj = $this->Cache->redisConn();
			if(($cacheObj['connection']) && empty($cacheObj['errors'])){
				if($cacheObj['robj']->exists('institutionDirectory:'.$companyId))
				{
					$key = 'institutionDirectory:'.$companyId;
					$cacheObj = $this->Cache->delRedisKey($key);
				}
			}
		}
	}

	public function saveSettingStaticData($dataParams=array())
	{
		$companyId = $dataParams['company_id'];
		$userId = $dataParams['user_id'];
		$lastModified = date('Y-m-d H:i:s');
		if(! empty($dataParams))
		{
			//****** Update Deleted User[START]
			$modifiedType = "deleted_user";
	        $getDeletedUser = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType)));
	        //echo "<pre>";print_r($getCacheLastModifiedUser);exit();
	        if(!empty($getDeletedUser))
	        {
	            $this->CacheLastModifiedUser->updateDeletedUser($userId, $companyId, $modifiedType, 1);
	        }
	        else
	        {
	            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>$modifiedType, "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }
	        //****** Update Deleted User[END]

			//****** Update Static Cache
			$staticCond = array('company_id'=>$companyId,'type'=>'static_directory_changed');
			$isStaticDataExist = $this->CacheLastModifiedUser->find("count", array("conditions"=> $staticCond));
			if($isStaticDataExist > 0){
				$this->CacheLastModifiedUser->updateLastModifiedDate(0, $companyId, 'static_directory_changed');
			}
			else
			{
				$saveStaticData = array("user_id"=> 0, "company_id"=> $companyId, "type"=>'static_directory_changed', "status"=>"1", "last_modified"=>$lastModified);
				$this->CacheLastModifiedUser->saveAll($saveStaticData);
			}
			//** Set eTag values for static directory[START]
			$paramsStaticEtagUpdate['key'] = 'staticDirectoryEtag:'.$companyId;
			$this->updateStaticEtagData($paramsStaticEtagUpdate);
			//** Set eTag values for static directory[END]
			//****** Update Dynamic Cache
			// $isDynamicDataExist = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed')));

			$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

	        if($isDynamicDataExist > 0)
	        {
	            // $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');

	            $this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($userId, $companyId, 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
	        }
	        else
	        {
	            $saveDynamicData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>'dynamic_directory_changed', "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveDynamicData);
	        }
			//** Get last modified date dynamic directory[START]
			$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
			if(!empty($getDynamicDirectoryLastModified)){
				$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
				$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
				$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
			}
			//** Get last modified date dynamic directory[END]

	        //****** Delete Cache
			$cacheObj = $this->Cache->redisConn();
			if(($cacheObj['connection']) && empty($cacheObj['errors'])){
				if($cacheObj['robj']->exists('institutionDirectory:'.$companyId))
				{
					$key = 'institutionDirectory:'.$companyId;
					$cacheObj = $this->Cache->delRedisKey($key);
				}
			}
		}
	}

	/*
	------------------------------------------------------------------------------
	ON: 28-12-17
	I/P: $user_id, $company_id
	O/P: JSON
	Desc: Save Available and Oncall Transactions
	------------------------------------------------------------------------------
	*/

	public function saveAvailableAndOncallTransactions($params = array())
	{
		$response = 0;
		if( !empty($params) )
		{
			$saveTransactionData = array("user_id"=> $params['user_id'], "company_id"=> $params['company_id'], "type"=>$params['type'], "status"=>$params['status'], "device_type"=>$params['device_type'], "platform"=>$params['platform']);
	        $this->AvailableAndOncallTransaction->saveAll($saveTransactionData);
	        $response = 1;
		}
		return $response;
	}

	/*
	------------------------------------------------------------------------------
	ON: 31-03-18
	I/P:
	O/P: JSON
	Desc: Return Access key, encryption key, encryption iv.
	------------------------------------------------------------------------------
	*/

	// public function getAppKey()
	// {
	// 	if($this->request->is('post'))
	// 	{
	// 		$dataInput = $this->request->input ( 'json_decode', true) ;
	// 		echo "<pre>";print_r($dataInput);exit();
	// 		//$encryptedData = $this->Common->decryptData( $dataInput['values'] );
	// 		//$dataInput = json_decode($encryptedData, true);
	// 		$filename = APP . 'webroot/logs/'.time().rand(100,999).'.tmp';
	// 		//$filename = time().rand(100,999).'.tmp';
	// 		$bits = 256;
	// 		exec('/usr/bin/openssl genrsa -passout pass:1234 -des3 -out '.$filename.' '.$bits);
	// 		exec('/usr/bin/openssl rsa -passin pass:1234 -in '.$filename.' -out '.$filename);
	// 		$pkGeneratePrivate = file_get_contents($filename);
	// 		//unlink($filename);

	// 		$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
	// 		$pkImportDetails = openssl_pkey_get_details($pkImport);
	// 		$pkImportPublic = $pkImportDetails['key'];
	// 		openssl_pkey_free($pkImport);

	// 		echo "<pre>";print_r($pkGeneratePrivate);exit();
	// 		// echo "\n".$pkGeneratePrivate
	// 		// 	."\n".$pkImportPublic;
	// 		// exit();

	// 		$apiAccessKey = 'ocrweb70bdb66f5b3f29b306b5f34f595072cd';
	// 		$encryptKey = 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd';
	// 		$ivKey = 'XX.s9GNvnP+zRA^Y';
	// 		$userAppData['appKeys'] = array("API_ACCESS_KEY"=> $apiAccessKey, "encryptionKey"=> $encryptKey, "ivKey"=> $ivKey);
	// 		$responseData = array('method_name'=> 'getAppKey', 'status'=>"1",'response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userAppData);
	// 	}else{
	// 		$responseData = array('method_name'=> 'getAppKey', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
	// 	}
	// 	//echo json_encode($responseUserSignup);
	// 	$encryptedData = $this->Common->encryptData(json_encode($responseData));
	// 	echo json_encode(array("values"=> $encryptedData));
	// 	exit;
	// }

	public function getAppKey()
	{
		if($this->request->is('post'))
		{
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if(!empty($dataInput['device_id']) && !empty($dataInput['public_key']))
			{
				if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
				{
					$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
					$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
					$pkImportDetails = openssl_pkey_get_details($pkImport);
					$pkImportPublic = $pkImportDetails['key'];
					openssl_pkey_free($pkImport);
				}
				else
				{
					// exec('/usr/bin/openssl genrsa -passout pass:1234 -des3 -out '.$filename.' '.$bits);
					// exec('/usr/bin/openssl rsa -passin pass:1234 -in '.$filename.' -out '.$filename);
					$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
					$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
					$pkImportDetails = openssl_pkey_get_details($pkImport);
					$pkImportPublic = $pkImportDetails['key'];
					openssl_pkey_free($pkImport);
				}

				//***************//After//***************//
				$checkDataExist = $this->AppDeviceKey->find("count", array("conditions"=> array("device_id"=>$dataInput['device_id'])));
				if($checkDataExist > 0)
				{
					// echo "<pre>";print_r("HII");exit();
					// $this->AppDeviceKey->updateAll(array("client_public_key"=> $dataInput['public_key']), array('device_id'=>$dataInput['device_id'],'status'=>1));

					// $this->AppDeviceKey->update(array("client_public_key"=> $dataInput['public_key']), array('device_id'=>$dataInput['device_id']));
					$str1 = str_replace("-----BEGIN PUBLIC KEY-----","",$pkImportPublic);
	        		$str2 = str_replace("-----END PUBLIC KEY-----","",$str1);
	        		$serverPublicKey = str_replace("\n","",$str2);
					$inputPublicKey = $dataInput['public_key'];
					$inputDeviceId = $dataInput['device_id'];
					$quer = "UPDATE app_device_keys SET client_public_key='$inputPublicKey' WHERE device_id='$inputDeviceId' AND status = 1";
					$data = $this->AppDeviceKey->query($quer);
					$responseData = array('method_name'=> 'getAppKey', 'status'=>"1",'response_code'=>'200', 'message'=> ERROR_200, 'data'=> trim(preg_replace('/\s+/', ' ', trim($serverPublicKey))));
				}
				else
				{
					// echo "<pre>";print_r("HI");exit();
					$str1 = str_replace("-----BEGIN PUBLIC KEY-----","",$pkImportPublic);
	        		$str2 = str_replace("-----END PUBLIC KEY-----","",$str1);
	        		$serverPublicKey = str_replace("\n","",$str2);
					$currentDateTime = date('Y-m-d 23:59:59');
					$saveAppInfo = array("client_public_key"=> $dataInput['public_key'], "device_id"=> $dataInput['device_id'], "server_public_key"=>$serverPublicKey, "status"=>1, "created_at"=>$currentDateTime);
					$this->AppDeviceKey->saveAll($saveAppInfo);
					$responseData = array('method_name'=> 'getAppKey', 'status'=>"1",'response_code'=>'200', 'message'=> ERROR_200, 'data'=> trim(preg_replace('/\s+/', ' ', trim($serverPublicKey))));
				}

				//***************//After//***************//

				//***************//Before//***************//


				// $serverKeys = $this->AppServerKey->find('first');
				// $currentDateTime = date('Y-m-d 23:59:59');
				// $saveAppInfo = array("client_public_key"=> $dataInput['public_key'], "device_id"=> $dataInput['device_id'], "server_public_key"=>$serverKeys['AppServerKey']['public_key'], "status"=>1, "created_at"=>$currentDateTime);
    //     		$this->AppDeviceKey->saveAll($saveAppInfo);
    //     		$str1 = str_replace("-----BEGIN PUBLIC KEY-----","",$pkImportPublic);
    //     		$str2 = str_replace("-----END PUBLIC KEY-----","",$str1);
    //     		$str3 = str_replace("\n","",$str2);
    //     		$responseData = array('method_name'=> 'getAppKey', 'status'=>"1",'response_code'=>'200', 'message'=> ERROR_200, 'data'=> trim(preg_replace('/\s+/', ' ', trim($str3))));

				//***************//Before//***************//
			}
			else
			{
				$responseData = array('method_name'=> 'getAppKey', 'status'=>"1",'response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userAppData);
			}
		}else{
			$responseData = array('method_name'=> 'getAppKey', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseUserSignup);
		//$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $responseData));
		exit;
	}

// 	public function testEnc()
// 	{
// 		if($this->request->is('post'))
// 		{
// 			$header = getallheaders();
// 			$dataInput = $this->request->input ( 'json_decode', true);
// 			if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
// 			{
// 				$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
// 				$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
// 				$pkImportDetails = openssl_pkey_get_details($pkImport);
// 				$pkImportPublic = $pkImportDetails['key'];
// 				openssl_pkey_free($pkImport);

// 				// echo $pkGeneratePrivate
// 				// 	."<br><br>".$pkImportPublic;

// 				// echo "<br />";
// 				// exit();

// 			}
// 			else
// 			{
// 				// exec('/usr/bin/openssl genrsa -passout pass:1234 -des3 -out '.$filename.' '.$bits);
// 				// exec('/usr/bin/openssl rsa -passin pass:1234 -in '.$filename.' -out '.$filename);
// 				$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
// 				$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
// 				$pkImportDetails = openssl_pkey_get_details($pkImport);
// 				$pkImportPublic = $pkImportDetails['key'];
// 				openssl_pkey_free($pkImport);

// 				// echo $pkGeneratePrivate
// 				// 	."<br><br>".$pkImportPublic;

// 				// echo "<br />";
// 				// exit();
// 			}
// 			$encry = $dataInput['params'];
// 			$encryData = $dataInput['values'];
// 			// $pkImportPublic = $dataInput['public_key'];
// 			// $encrypted = openssl_public_encrypt('hello',$newsource,$pkImportPublic);
// 			// print_r(base64_decode($encry));exit();

// 			openssl_private_decrypt(base64_decode($encry), $decrpted, $pkGeneratePrivate);


// 			$dataInput = json_decode($decrpted, true);
// 			//echo "<pre>";print_r($encryData);exit();
// 			// $dataInput = $this->request->input('json_decode', true) ;
// 			$encryptedData = $this->decryptDataNew( $encryData, $dataInput['key'], $dataInput['iv']);
// 			$randKeyStr = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
// 			$randKeyIv = strtoupper('C+BWA9SEBDDCE^GY');
// 			// $randKey = str_shuffle($randKeyStr);
// 			// $randIv = str_shuffle($randKeyIv);
// 			$paramsResponceArr = array('key'=>$randKeyStr, 'iv'=>$randKeyIv);
// 			$paramsResponceJson = json_encode($paramsResponceArr);
// 			$deviceId = $header['device_id'];
// 			$getUserDeatail = $this->AppDeviceKey->find('first',
//                     array('conditions'=>
//                       array(
//                         'device_id'=> $deviceId
//                         )
//                     ));
// 			// echo "Here Check<br />";
// 			// echo "<pre>";print_r($getUserDeatail);exit();
// 			App::import('Vendor','Crypt_RSA',array('file' => 'phprsa/Crypt/RSA.php'));
// 			$rsaObj = new Crypt_RSA();
// 			// $rsaObj = $this->Rsa->rsaEncryption();
// 			// $keyVar = "-----BEGIN PUBLIC KEY----- ".$getUserDeatail['AppDeviceKey']['client_public_key']." -----END PUBLIC KEY-----";
// 			// $RSAPublicKey = $getUserDeatail['AppDeviceKey']['client_public_key'];
// 			// $keyVar = "-----BEGIN PUBLIC KEY-----\r\n" .
//    //                                   chunk_split(base64_encode($RSAPublicKey), 64) .
//    //                                   '-----END PUBLIC KEY-----';
// 			// $keyVar = "-----BEGIN PUBLIC KEY-----\r\n".$getUserDeatail['AppDeviceKey']['client_public_key']."-----END PUBLIC KEY-----";

// // 			$keyVar = "-----BEGIN PUBLIC KEY-----\r\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1eSeze/djd8IVIZFvIrsEqiA4rHMcXPq
// // Q3/71scfbpaNGxwO3/5JwuUXtzay9B4V9TZNi9plWBAVhOf+TaCxBJPtjGGn8eQqHLRfuVRyC2qH
// // jEuio59FxkEXaaU3V2NUnUXYRDktPtazlVvphVtXm30ieVH5nUy3Bnc9ngiosec/2RUBkgjMy7il
// // OKjcG2JoiQuJCBygmoTxTfUc4SyAfci3fCSfxj6X9rkGFnZVr/ooEB5gr4AS5+iMYlQnNPaIzmhS
// // 4Lgp7cwsckdYjceRfSVCEho5XOpb+wi11GeQW6flCvCNQyJcrluQdTqg6JV9s+OzsVtGFi2jYwYJ
// // vySr3wIDAQAB
// // -----END PUBLIC KEY-----";
// 			$getUserDeatail1 = $getUserDeatail['AppDeviceKey']['client_public_key'];
// // 			$keyVar = "-----BEGIN PUBLIC KEY-----\r\nMIIBCgKCAQEAmPlwL3dC8NhnNRtPbfgurY8gch6N+e3Woau4NMQrgSQnNjwznkWs36YtmRpIjyesrBbbwTVzcJj4Vj/q0NePcKHnNEfnYsCOUPFFX9tYLWiAC0/19dO8NC3B/T7NTufuBkk46Yc2fMg9+9ZyD2ZWyY2LUKZ9U5TC/55IgY/HQrxj2TRKFd1f+eIS10j6hJhHHEftk8obykRC0hiBtwDxylWG+fBBW/cCt+v7/vu4fRET9jU6RwipmLRnLRPe4nsHyxw0LO1EhDOQ/YU6URqm+tLrn92asrTUW5Pv3Y37yGU1DomJDo4ggY6G+Sro+HFjg87QLcz7lYeL+voOa0I2JQIDAQAB
// // -----END PUBLIC KEY-----";
// 			$rsaObj->loadKey('-----BEGIN PUBLIC KEY-----
// '.$getUserDeatail1.'
// -----END PUBLIC KEY-----');
// // 			$rsaObj->loadKey('-----BEGIN PUBLIC KEY-----
// // MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3ARq6dAdpN1dOMdCBmwoI2+06zldDXojV3d3nbs5exV3HhhDDDEHH9xdFlXtouyx7JhLNXEByAdbA1w5kIRWj1ky9Ph4UGCmoIitz3+ciOTs1OX7VXCJ2nkRp5O93FBBBQC4Oh0Y0hJ9F51dOZt45rKdZauxyyLqOESnXKJGqv8ZkjCtNEnxClOzpYTCrd+GTxRjWYpV6GIaQv7kztMJ+ijijHBDMiYEd9LvDR24bKELldH5txNiDOoZojZDiM4sfY7q4neqFnxPRzUB64SNQ1vBiI7RQzPRCGTXMlC7q3mHyCXLaFBKI/J1EwcTDUMYWJm08T5m/L9QL3nLw70RxwIDAQAB
// // -----END PUBLIC KEY-----');
// 			$rsaObj->setPublicKey();
// 			// $publickey = $rsaObj->getPublicKey(CRYPT_RSA_PUBLIC_FORMAT_OPENSSH);
// 			$publickey = $rsaObj->getPublicKey(CRYPT_RSA_PUBLIC_FORMAT_OPENSSH);
// 			// $plaintext ="Hello";
// 			$ciphertext = $rsaObj->encrypt($paramsResponceJson);
// 			// echo "<pre>";print_r(base64_encode($ciphertext));exit();
// 			// $encrypted12 = openssl_public_encrypt('hello',$newsource,base64_decode($getUserDeatail['client_public_key']));
// 			// echo "<pre>";print_r($newsource);exit();
// 			// echo "<pre>";print_r($encryptedData);exit();
// 			// $decData = json_decode($encryptedData, true);
// 			// echo "<pre>";print_r($decData);exit();
// 			//$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
// 			//echo "----------";
// 			// echo $pkGeneratePrivate;
// 			// print_r($decrpted);
// 			//echo "<pre>";print_r($dataInput['values']);exit();
// 			// $data['values'] = sizeof($newUsers);
// 			// $data['params'] = sizeof($newUsers);
// 			$data['values'] = "This Is Encrypted Data";
// 			$encryptedData = $this->encryptDataNew( $data['values'], $randKeyStr, $randKeyIv);
// 			// echo "<pre>";print_r(base64_encode($encryptedData));exit();

// 			$responseData = array('method_name'=> 'testEnc', 'status'=>"1", 'response_code'=> '200', 'message'=> ERROR_200, 'data'=> array('values'=> $encryptedData, 'params'=>base64_encode($ciphertext)));
// 			// echo "<pre>";print_r($responseData);exit();
// 			// $responseData = array('method_name'=> 'testEnc', 'status'=>"1",'response_code'=>'200', 'message'=> ERROR_200, 'data'=> $decrpted);
// 		}else{
// 			$responseData = array('method_name'=> 'testEnc', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
// 		}
// 		//echo json_encode($responseUserSignup);
// 		//$encryptedData = $this->Common->encryptData(json_encode($responseData));
// 		echo json_encode(array("values"=> $responseData));
// 		exit;
// 	}

	public function decryptDataNew( $encrypted = NULL, $encryptKey, $ivKey ){
		//** Decrypting Data
		$encryptKey = 'nS4Gpg9VND6FrOqS16VTWeRfMwc4jR07';
		$ivKey = 'tLYtUdNUNb2HtkHL';

		$encrypted = base64_decode($encrypted);
		$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $encrypted, MCRYPT_MODE_CBC, $ivKey );
	  	//** Remove Padding
	  	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
        return $decrypted;
	}

	public function encryptDataNew( $text = NULL, $encryptKey, $ivKey){
		// $encryptKey = 'nS4Gpg9VND6FrOqS16VTWeRfMwc4jR07';
		// $ivKey = 'tLYtUdNUNb2HtkHL';

		$encryptKey = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$ivKey = strtoupper('C+BWA9SEBDDCE^GY');
		//** Get text with padding
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$pad = $size - (strlen($text) % $size);
    	$text = $text . str_repeat(chr($pad), $pad);
    	//** Get encrypted text
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $text, MCRYPT_MODE_CBC, $ivKey ) );
		return $encrypted;
	}

	// public function decryptDataNew( $encrypted = NULL, $encryptKey, $ivKey ){
	// 	// return $encryptKey;
	// 	//** Decrypting Data
	// 	$encryptKey = 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd';
	// 	$encryptKey = 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd';
	// 	$encrypted = base64_decode($encrypted);
	// 	//echo "<pre>";print_r($encrypted );exit();
	// 	$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_256, $encryptKey, $encrypted, MCRYPT_MODE_CBC, $ivKey );
	//   	// return $decrypted;
	//   	//** Remove Padding
	//   	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
 //        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
 //        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
 //        return $decrypted;
	// }

	public function loginattempt(){
		$this->autoRender = false;
		$responseData = array();
		$number_of_attempts = 0;
		$attempt_time = 0;
		$updatePassword = 0;
		$userRemoteAddress = $_SERVER['REMOTE_ADDR'];
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			// if($this->accesskeyCheck()){
			if($this->validateAccessKey()){
				if( !empty($dataInput['email']) && !empty($dataInput['password']) ){

					$uppercase = preg_match('@[A-Z]@', $dataInput['password']);
					$lowercase = preg_match('@[a-z]@', $dataInput['password']);
					$number    = preg_match('@[0-9]@', $dataInput['password']);
					$specialChars = preg_match('@[^\w]@', $dataInput['password']);

					if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($dataInput['password']) < 8) {
					  $updatePassword = 1;
					}

					$dataInput['email'] = strtolower($dataInput['email']);
					$params = array('email'=> $dataInput['email']);
					$userDetails = $this->User->userDetails( $params );
					$userLoginTransaction = $this->LoginAttemptTransaction->find("first", array("conditions"=> array("remote_addr"=> $userRemoteAddress)));
					$number_of_attempts = !empty($userLoginTransaction['LoginAttemptTransaction']['attempt']) ? $userLoginTransaction['LoginAttemptTransaction']['attempt'] : 0;
					if(! empty($userLoginTransaction['LoginAttemptTransaction']['last_login']))
					{
						date_default_timezone_set('Asia/Kolkata');
						$last_login_time = strtotime($userLoginTransaction['LoginAttemptTransaction']['last_login']);
						$current_time = strtotime(date('Y-m-d H:i:s'));
						$difference = ($current_time - $last_login_time);
						$attempt_time = intval($difference/60);
					}
					// echo "<pre>";print_r($attempt_time);exit();
					if($attempt_time >= 10){
						if(!empty($userLoginTransaction)){
							$this->LoginAttemptTransaction->updateAll(array("attempt"=>0 , "status"=> 1, "last_login"=>'"'.date("Y-m-d H:i:s").'"'), array("remote_addr"=> $userRemoteAddress));
							}else{
								$userLoginTransactionToInsert = array("remote_addr"=>$userRemoteAddress, "attempt"=> $userLoginAttempt+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
								$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
							}
					}
					$userLoginTransaction = $this->LoginAttemptTransaction->find("first", array("conditions"=> array("remote_addr"=> $userRemoteAddress)));
					$number_of_attempts = !empty($userLoginTransaction['LoginAttemptTransaction']['attempt']) ? $userLoginTransaction['LoginAttemptTransaction']['attempt'] : 0;
					if(! empty($userLoginTransaction['LoginAttemptTransaction']['last_login']))
					{
						date_default_timezone_set('Asia/Kolkata');
						$last_login_time = strtotime($userLoginTransaction['LoginAttemptTransaction']['last_login']);
						$current_time = strtotime(date('Y-m-d H:i:s'));
						$difference = ($current_time - $last_login_time);
						$attempt_time = intval($difference/60);
					}
					if($attempt_time <=10){
						// if(!empty($userLoginTransaction)){
						// 	$this->LoginAttemptTransaction->updateAll(array("attempt"=>0 , "status"=> 1, "last_login"=>'"'.date("Y-m-d H:i:s").'"'), array("remote_addr"=> $userRemoteAddress));
						// 	}else{
						// 		$userLoginTransactionToInsert = array("remote_addr"=>$userRemoteAddress, "attempt"=> $userLoginAttempt+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
						// 		$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
						// 	}
						if($number_of_attempts  <= 10){
							if(  $userDetails['User']['email'] == $dataInput['email'] && $userDetails['User']['password'] == md5($dataInput['password']) ){
								if( $userDetails['User']['status'] != 2){
									if( $userDetails['User']['status'] == 1 ){
										if($userDetails['User']['approved'] == 1){
											$userData = $this->userFields( $userDetails );
											// Update Last Logged in Date
											$this->User->updateFields( array('User.last_loggedin_date'=> "'".date('Y-m-d H:i:s')."'"), array('User.id'=> $userDetails['User']['id']));
											// User Device data save
											$dataUserDevice['user_id'] = $userDetails['User']['id'];
											$dataUserDevice['device_id'] = isset($dataInput['device_id']) ? $dataInput['device_id']:'';
											$dataUserDevice['token'] = md5(strtotime(date('Y-m-d H:i:s')) . $userDetails['User']['id'] . rand(1000,100000));
											$dataUserDevice['device_type'] = isset($dataInput['device_type']) ? $dataInput['device_type']:'0';
											// Used to distinguish the application type Shashi 07-06-16
											$dataUserDevice['application_type'] = isset($dataInput['application_type']) ? $dataInput['application_type']:'MB';
											//** Logout from preveous devices[START]
											$this->logoutFromOtherDevices($dataUserDevice);
											//** Logout from preveous devices[END]
											//** Check User and device
											$userDeviceCheck = $this->UserDevice->find("count", array("conditions"=> array("user_id"=> $userDetails['User']['id'], "device_id"=> $dataInput['device_id'], 'application_type'=> 'MB')));
											//echo json_encode(array('value'=>$userDeviceCheck));exit();
											if($userDeviceCheck == 0){
												$this->UserDevice->save( $dataUserDevice );

												//** Inactive all other users device id
												$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataInput['device_id'], "user_id !="=> $userDetails['User']['id'], 'application_type'=> 'MB'));
											}else{
												$this->UserDevice->updateAll(array("status"=> 1), array("user_id"=> $userDetails['User']['id'], "device_id"=> $dataInput['device_id'], 'application_type'=> 'MB'));
												//** Inactive all other users device id
												$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataInput['device_id'], "user_id !="=> $userDetails['User']['id'], 'application_type'=> 'MB'));
											}

											// Get Token
											$tokenParams = array(
																'user_id'=>  $userDetails['User']['id'],
																'device_id'=> $dataInput['device_id'],
																'status'=> 1
																);
											$tokenData = $this->UserDevice->tokenDetails( $tokenParams );
											$userData['token'] = !empty($tokenData['UserDevice']['token']) ? $tokenData['UserDevice']['token'] : "";
											//** For user current company paid then add in enterprise list if not[START]
											$checkEnterprise = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']), "company_id"=> $userData['Company']['company_id'])));
											if($checkEnterprise==0){
												$this->EnterpriseUserList->save(array("email"=> stripslashes($userDetails['User']['email']), "company_id"=> $userData['Company']['company_id'], "status"=> 0));
											}

											//** For user current company paid then add in enterprise list if not[END]

											//** Get Settings Values like CIP ON/OFF, force logout values[END]
											//** Get user Enterprise data[START]
											$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userData['user_id'])));
											$dataParams['email'] = stripslashes($userDetails['User']['email']);
											$dataParams['company_id'] = $userData['Company']['company_id'];
											$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);

											$checkUserSubscribedExistingUser = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userData['user_id'])));
											//** For existing user prior to enterprise feature add enterprise data first time[START]
											if(empty($checkUserSubscribedExistingUser)){
												if(empty($checkUserForPaid)){
													$subscriptionStartDate = date('Y-m-d 23:59:59');
													$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_TRIAL_PERIOD.' days'));
												}else{
													$subscriptionStartDate = $checkUserForPaid['CompanyName']['subscription_date'];
													$subscriptionExpireDate = $checkUserForPaid['CompanyName']['subscription_expiry_date'];
												}
													$subscriptionDataVals = array("user_id"=> $userData['user_id'],"company_id"=> $userData['Company']['company_id'], "subscription_date"=> $subscriptionStartDate, "subscription_expiry_date"=> $subscriptionExpireDate);
													$this->UserSubscriptionLog->save($subscriptionDataVals);
											}

											//** For existing user prior to enterprise feature add enterprise data first time[END]
											$status = 0;$expiryDate = 0;
											$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
											//** When user comes after trust expiry date update grace period[START]
												$currentDate = date('Y-m-d 23:59:59');
												$gracePeriod = date('Y-m-d 23:59:59', strtotime($currentDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
												$trustExpiryDate = $checkUserSubscribed['UserSubscriptionLog']['subscription_expiry_date'];
												if(strtotime($currentDate) > strtotime($trustExpiryDate) && !empty($checkUserForPaid) && ($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] != 2)){
													$this->UserSubscriptionLog->updateAll(array("company_id"=> $userData['Company']['company_id'], "subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $userDetails['User']['id']));
												}elseif($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] == 1 && empty($checkUserForPaid)){
													$this->UserSubscriptionLog->updateAll(array("company_id"=> $userData['Company']['company_id'],"subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $userDetails['User']['id']));
												}

												//** When user comes after trust expiry date update grace period[END]
												//** Update user expiration[START]
												$checkUserSubscribedForexpire = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
												if(!empty($checkUserSubscribedForexpire) ){
													$paramData['subscription_expiry_date'] = $checkUserSubscribedForexpire['UserSubscriptionLog']['subscription_expiry_date'];//$checkUserForPaid['CompanyName']['subscription_expiry_date'];
													$isExpired = $this->UserSubscriptionLog->checkSusbcriptionExpired($paramData);
													if($isExpired){
														$this->UserSubscriptionLog->updateAll(array("company_id"=>$userData['Company']['company_id'], "subscribe_type"=> 3),array("user_id"=> $userDetails['User']['id']));
													}
												}

												//** Update user expiration[END]
												//** Get user SUBSCRIPTION status
												$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
												if(!empty($checkUserSubscribedFinal)){
													//if(!empty($checkUserForPaid)){ $status = 1; }
													if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
													$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
													if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
													//** Check if expired
													if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
												}
												//** In Case of grace period oncall should be off[START]
												if($status==2){
													$this->UserDutyLog->updateAll(array("status"=> 0), array("user_id"=> $userData['user_id'], "hospital_id"=> $userData['Company']['company_id']));
												}

												//** In Case of user expired update cache[START]

												if($status==3){
													$userDataParams['company_id'] = isset($userCurrentCompany['UserEmployment']['company_id']) ? $userCurrentCompany['UserEmployment']['company_id'] : 0;
													$userDataParams['user_id'] = isset($userData['user_id']) ? $userData['user_id']: 0;
													$updateStaticCache = $this->saveStaticData($userDataParams);
												}

												//** In Case of user expired update cache[END]
												//** In Case of grace period oncall should be off[END]
												$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate) > 0) ? strtotime($expiryDate):0);
												//** Get user Enterprise data[END]
												$userData['settings'] = array($subscriptionData);

												//** Get User Tour Guide Status
												$tourValue="0";
												$tourGuideData = $this->TourGuideCompletionVisit->find('count',array('conditions'=>array('user_id'=>$userData['user_id'],'device_type'=>$dataUserDevice['device_type'],'status'=>1)));
												if($tourGuideData > 0){
													$tourValue="1";
												}
												$userData['tourGuide'] = array("value"=>$tourValue);

												//** Get user role tags[START] // Discuss with Mohit, Uday
												$roleTagsArr = array(); $roleTagsVals = array();
												$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
												if(!empty($roleTags['Profession']['tags'])){
													$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
												}
												if(!empty($roleTagsVals)){
													foreach($roleTagsVals as $rt){
														$values = array(); $roleTagId = 0;
														$paramtrs['user_id'] = $userDetails['User']['id'];
														$paramtrs['key'] = $rt;
														$roleTagUser = $this->RoleTag->userRoleTags($paramtrs);
														if(!empty($roleTagUser)){
															$roleTagId = $roleTagUser[0]['rt']['id'];
															$values = array($roleTagUser[0]['rt']['value']);
														}
														$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
													}
												}
												$userData['role_tags'] = $roleTagsArr;
												$chatMessagedata = array("encryptionKey"=> 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd', "encryptionIv"=>'XX.s9GNvnP+zRA^Y');
												//** Get user role tags[END]

												$responseData = array('method_name'=> 'login', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('User'=> $userData, 'MessageKey'=>$chatMessagedata, 'updatePassword' => $updatePassword));

												//** Add custom data on QB [START]
												$atwork = "0"; $oncall = "0";
												$dutyData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userData['user_id'])));
												if(!empty($dutyData)){
													$atwork = $dutyData['UserDutyLog']['atwork_status'];
													$oncall = $dutyData['UserDutyLog']['status'];
												}
												try{
													//** One time token to validate for QB[START]
													$oneTimeToken = $this->genrateRandomToken($userDetails['User']['id']);
													$oneTimeTokenData = array("user_id"=> $userDetails['User']['id'], "token"=> $oneTimeToken);
													$this->UserOneTimeToken->save($oneTimeTokenData);
													//** One time token to validate for QB[END]
													$tokenDetails = $this->Quickblox->quickLogin($dataInput['email'], $oneTimeToken);
													$token = $tokenDetails->session->token;
													$user_id = $tokenDetails->session->user_id;
													$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
													$custom_data_from_api = json_decode($user_details->user->custom_data);
													$customData['userRoleStatus'] = $custom_data_from_api->status;
													$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
													$customData['isImport'] = "true";
													$customData['at_work'] = (string) $atwork;
													$customData['on_call'] = (string) $oncall;
													$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
												}catch(Exception $e){}

												if(!empty($userLoginTransaction)){
												$this->LoginAttemptTransaction->updateAll(array("attempt"=>0 , "status"=> 1, "last_login"=>'"'.date("Y-m-d H:i:s").'"'), array("remote_addr"=> $userRemoteAddress));
												}else{
													$userLoginTransactionToInsert = array("remote_addr"=>$userRemoteAddress, "attempt"=> $userLoginAttempt+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
													$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
												}


										}else{
										$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "633", 'message'=> ERROR_633);
										}

									}else{
									$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "605", 'message'=> ERROR_605);
									}
								}else{
									$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
								}

							}else{
								if(!empty($userLoginTransaction)){
								$this->LoginAttemptTransaction->updateAll(array("attempt"=>$userLoginTransaction['LoginAttemptTransaction']['attempt']+1 , "status"=> 1), array("remote_addr"=> $userRemoteAddress));
								}else{
									$userLoginTransactionToInsert = array("remote_addr"=>$userRemoteAddress, "attempt"=> $userLoginAttempt+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
									$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
								}
							$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "604", 'message'=> ERROR_604);
							}

						}else{
							$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "660", 'message'=> "Your Medic Bleep account has been blocked due to suspicious login. Please contact support@medicbleep.com or try again after some time");
						}

					}else{
						$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "660", 'message'=> "Your Medic Bleep account has been blocked due to suspicious login. Please contact support@medicbleep.com or try again after some time");
					}

				}else{
					$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "603", 'message'=> ERROR_603);
				}
			}else{
				$response = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	------------------------------------------------------------------------------
	ON: 28-12-17
	I/P: $user_id, $company_id
	O/P: JSON
	Desc: Save Available and Oncall Transactions
	------------------------------------------------------------------------------
	*/

	public function institutesListApprovedToUser()
	{
		$responseData = array();
		$userCompanyList = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			// if( $this->validateToken() && $this->validateAccessKey() ){
				if(! empty($dataInput['user_id']))
				{

					$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
					$dataParams['email'] = stripslashes($userDetails['User']['email']);
					$getInstitutesListApprovedToUser = $this->EnterpriseUserList->getInstitutesListApprovedToUser($dataParams);
					// echo "<pre>";print_r($getInstitutesListApprovedToUser);exit();

					// if(!empty($getInstitutesListApprovedToUser)){
					// 	$userCompanyList['CompanyDetail'] = array("company_id"=> $getInstitutesListApprovedToUser['CompanyName']['company_id'], "email"=> $getInstitutesListApprovedToUser['EnterpriseUserList']['email']);
					// }else{
					// 	$userCompanyList = (object) array();
					// }
					$responseData = array('method_name'=> 'institutesListApprovedToUser', 'status'=>"1",'response_code'=>'200', 'message'=> ERROR_200, 'data'=> $getInstitutesListApprovedToUser);

				}
				else
				{
					$responseData = array('method_name'=> 'institutesListApprovedToUser', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			// }
			// else{
			// 	$responseData = array('method_name'=> 'institutesListApprovedToUser', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			// }

		}else{
			$responseData = array('method_name'=> 'institutesListApprovedToUser', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}


	/*
	-----------------------------------------------------------------
	On: 24-08-2018
	I/P:
	O/P:
	Desc: fetches all master company names And List of user in which user is approved.
	-----------------------------------------------------------------
	*/


	public function getCompaniesMasterDataLite(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$userStatusDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			$userCountryDetail = $this->UserProfile->find("first", array("conditions"=> array("UserProfile.user_id"=> $dataInput['user_id'])));
			if($userStatusDetails['User']['status']==1 && $userStatusDetails['User']['approved']==1)
			{
				// if( $this->validateToken() && $this->validateAccessKey() ){
					$params["searchText"] = isset($dataInput['searchText']) ? $dataInput['searchText'] : '';
					$params["size"] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$params["page_number"] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
					$params["userCountry"] = isset($userCountryDetail['UserProfile']['country_id']) ? $userCountryDetail['UserProfile']['country_id'] : '';
					$companyLists = $this->CompanyName->companyListsLite( $params );
					// echo "<pre>";print_r($companyLists);exit();

					$dataParams['email'] = stripslashes($userStatusDetails['User']['email']);
					$getInstitutesListApprovedToUser = $this->EnterpriseUserList->getInstitutesListApprovedToUser($dataParams);
					// echo "<pre>";print_r($getInstitutesListApprovedToUser);exit();
					if(!empty($getInstitutesListApprovedToUser))
					{
						foreach ($getInstitutesListApprovedToUser as  $approvedInstitute) {
							if(!empty($approvedInstitute['CompanyName']['company_name']))
							{
								$isPartner = 0;
								if($approvedInstitute['CompanyName']['is_subscribed']==1 && $approvedInstitute['CompanyName']['status']==1){
									$isPartner = 1;
									$userApprovedComanyData[] = array("id"=> $approvedInstitute['CompanyName']['id'], "company_name"=> $approvedInstitute['CompanyName']['company_name'],"is_partner"=> !empty($isPartner)?1:0, "patient_number_type"=>$approvedInstitute['CompanyName']['patient_number_type']);
								}
							}
						}
					}

					if( !empty($companyLists) ){
						foreach( $companyLists as $company ){
							if(!empty($company['CompanyName']['company_name']))
							{
								//** Check if user subscribed for comapny[START]
								$isUserSubscribed = 0;
								$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
								$dataParams['email'] = $userDetails['User']['email'];
								$dataParams['company_id'] = $company['CompanyName']['id'];


								if($company['CompanyName']['is_subscribed']==1 && $company['CompanyName']['status']==1){
									$isUserSubscribed = 1;
								 	$premiumCompanyData[] = array("id"=> $company['CompanyName']['id'], "company_name"=> $company['CompanyName']['company_name'],"is_partner"=> !empty($isUserSubscribed)?1:0, "patient_number_type"=>$company['CompanyName']['patient_number_type']);
								}
								//************ Commented on 30-Nov-2018[TO Exclude Free institute List [Start] ]*****************************//

								// else{
								// 	$freeCompanyData[] = array("id"=> $company['CompanyName']['id'], "company_name"=> $company['CompanyName']['company_name'],"is_partner"=> !empty($isUserSubscribed)?1:0, "patient_number_type"=>$company['CompanyName']['patient_number_type']);
								// }

								//************ Commented on 30-Nov-2018[TO Exclude Free institute List [Start] ]*****************************//
							}
						}
						// echo "<pre>";print_r($freeCompanyData);exit();
						// $otherCompanyData = array_merge( $premiumCompanyData, $freeCompanyData);
						$otherCompanyData = $premiumCompanyData;
						$excludeSameCompanyData = $this->excludeSameCompanyData($otherCompanyData, $userApprovedComanyData);
						foreach ($excludeSameCompanyData as  $value) {
							$excludeSameCompanyData1[] = array("id"=> $value['id'], "company_name"=> $value['company_name'],"is_partner"=> $value['is_partner'],"patient_number_type"=>$value['patient_number_type']);
						}
						$finalArray[] = array('user_approved_institution' => $userApprovedComanyData, 'other_company_data' => $excludeSameCompanyData1);

						$companies['Company'] = $finalArray;
						$responseData = array('method_name'=> 'getCompaniesMasterDataLite', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $companies);
					}else{
						$responseData = array('method_name'=> 'getCompaniesMasterDataLite', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
					}
				// }else{
				// 	$responseData = array('method_name'=> 'getCompaniesMasterData', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
				// }
			}
			else
			{
				$statusResponse = $this->getUserStatus($dataInput['user_id'],$dataInput['user_id']);
				$responseData = array('method_name'=> 'getCompaniesMasterDataLite','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'getCompaniesMasterDataLite', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}



	public function excludeSameCompanyData($arraya, $arrayb) {

	    foreach ($arraya as $keya => $valuea) {
	        if (in_array($valuea, $arrayb)) {
	            unset($arraya[$keya]);
	        }
	    }
	    return $arraya;
	}


	/*
	------------------------------------------------------------------------------------
	On: 17-08-2018
	I/P: qr code detail
	O/P: success, falure
	Desc: institute selection using qr code
	------------------------------------------------------------------------------------
	*/
	public function addUserInstitutionByQrCode(){
		$this->autoRender = false;
		$responseData = array();
		$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$t1 = explode(" ",microtime());
		$milisec1 = str_replace('.', '', substr((string)$t1[0],1,4));
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$userStatusDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
			// if($userStatusDetails['User']['status']==1 && $userStatusDetails['User']['approved']==1)
			if($userStatusDetails['User']['status']==1)
			{
				//** One time token to validate for QB[START]
				$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
				$oneTimeTokenData = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
				$this->UserOneTimeToken->saveAll($oneTimeTokenData);
				//** One time token to validate for QB[END]
				// if( $this->validateToken() && $this->validateAccessKey() ){
					//** Add User Institution[START]
					if(!empty($dataInput['Company'])){
						foreach( $dataInput['Company'] as $company ){
							$profileData = array();
							if(!empty($company['company_id'])){
								//** Remove all is_current company before edit[START]
								try{
									$this->UserEmployment->updateAll( array("is_current"=> 0) , array("user_id"=> $dataInput['user_id']));
								}catch(Exception $e){}
								//** Remove all is_current company before edit[END]
								$company['designation_id'] = !empty($company['Designation']['designation_id']) ? $company['Designation']['designation_id'] : 0 ;
								if( empty($company['id']) ){
									$profileData = array("user_id"=> $dataInput['user_id'], "company_id"=> $company['company_id'], "from_date"=> isset($company['from_date']) ? date('Y-m-d', strtotime($company['from_date']))  : '0000-00-00', "to_date"=> isset($company['to_date']) ? date('Y-m-d', strtotime($company['to_date']))  : '0000-00-00', "location"=> isset($company['location']) ? $company['location']  : '', "description"=> isset($company['description']) ? $company['description']  :'', "is_current"=> 1,"designation_id"=> $company['designation_id']);
									$this->UserEmployment->saveAll( $profileData );
								}else{
									$fromDate = "0000-00-00"; $toDate = "0000-00-00"; $location = ""; $description = "";
									$profileData = array("user_id"=> $dataInput['user_id'], "company_id"=> $company['company_id'], "from_date"=> isset($company['from_date']) ? "'". date('Y-m-d', strtotime($company['from_date'])) ."'"  : "'".$fromDate."'", "to_date"=> isset($company['to_date']) ? "'". date('Y-m-d', strtotime($company['to_date'])) ."'"  : "'".$toDate."'", "location"=> isset($company['location']) ? "'". $company['location']. "'"  : "'". $location ."'", "description"=> isset($company['description']) ? "'". $company['description']. "'"  : "'". $description ."'", "is_current"=> 1,"designation_id"=> isset($company['designation_id']) ? $company['designation_id'] : 0);
									$this->UserEmployment->updateAll( $profileData , array("id"=> $company['id']));
								}
								//** Add/update ON duty[START]
								if($company['is_current'] == 1){ //** If current company add duty value (ON/OFF)
										$previousCompanyId = !empty($company['previous_company_id']) ? $company['previous_company_id'] :0;
										$currentCompanyId = !empty($company['company_id']) ? $company['company_id']: 0;
										$qbResponce = $this->updateOnCallStatusLite($userStatusDetails['User']['email'],$oneTimeToken, $company['duty_status']);
										// $encryptedData = $this->Common->encryptData(json_encode($responseData));
										// echo json_encode(array("values"=> $encryptedData));
										// exit();
										if($qbResponce['qbsuccess'] == 1)
										{
											if( $this->UserDutyLog->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id']))) == 0){
												$dutyData = array("user_id"=> $dataInput['user_id'], "hospital_id"=> $company['company_id'], "status"=> $company['duty_status']);
												$this->UserDutyLog->saveAll($dutyData);
											}else{
												$this->UserDutyLog->updateAll(array("hospital_id"=> $company['company_id'], "modified"=> "'".date('Y-m-d H:i:s')."'", "status"=> $company['duty_status']), array("user_id"=> $dataInput['user_id']));
											}
										}
										else{
											$responseData = array('method_name'=> 'addUserInstitutionLite', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $qbResponce);
										}

								}
							}
						}

						$this->QrCodeDetail->updateAll(array("is_used"=> 0), array("qr_code_number"=> $dataInput['qr_code_number']));

						$QrCodeData = $this->QrCodeDetail->find("first", array("conditions"=> array("QrCodeDetail.qr_code_number"=> $dataInput['qr_code_number'])));


						//** Update free/enterprise user date[START]
						$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
						$companySubscriptionData=$this->CompanyName->find("first", array("conditions"=> array("id"=>$currentCompanyId, "status"=>1,"is_subscribed"=>1)));
							if(!empty($companySubscriptionData)){
							//** Set email in user enterprise list
							$checkUserAdded = $this->EnterpriseUserList->find("first", array("conditions"=> array('email'=> stripslashes($userDetails['User']['email']),'company_id'=>$currentCompanyId))); //previously count changed to first in query
							// echo "<pre>";print_r($checkUserAdded);exit();
							if(empty($checkUserAdded)){ // previously $checkUserAdded == 0
								$this->EnterpriseUserList->save(array('email'=> stripslashes($userDetails['User']['email']),'company_id'=>$currentCompanyId, 'status'=> 1));
							}
							//****Update user as unassign for the institute if removed by trust admin

							else if($checkUserAdded['EnterpriseUserList']['status'] == 0)
							{
								$this->EnterpriseUserList->updateAll(array("status"=> 1), array("email"=> $userDetails['User']['email'], "company_id"=>$currentCompanyId));
							}

							else if($checkUserAdded['EnterpriseUserList']['status'] == 2)
							{
								$this->EnterpriseUserList->updateAll(array("status"=> 1), array("email"=> $userDetails['User']['email'], "company_id"=>$currentCompanyId));
							}
						}
						$checkUserAdded12 = $this->EnterpriseUserList->find("first", array("conditions"=> array('email'=> stripslashes($userDetails['User']['email']),'company_id'=>$currentCompanyId)));
						// echo "<pre>";print_r($checkUserAdded12);exit();
						//** Previous company paid check[START]
						$dataParamsPrevious['company_id'] = isset($previousCompanyId) ? $previousCompanyId : 0;
						$dataParamsPrevious['email'] = $userDetails['User']['email'];
						$checkUserForPaidPreviousCompany = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParamsPrevious);
						//** Previous company paid check[END]

						//** Current company paid check[START]
						$dataParams['company_id'] = $currentCompanyId;
						$dataParams['email'] = $userDetails['User']['email'];
						$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);
						//** Current Company paid check[END]
						$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						if(!empty($checkUserForPaid)){
							$subscriptionStartDate = date('Y-m-d 23:59:59');
							$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.$QrCodeData['QrCodeDetail']['validity'].' days'));
							// echo "<pre>";print_r($subscriptionExpireDate);exit();
							if(!empty($checkUserSubscribed)){
								$this->UserSubscriptionLog->updateAll(array("subscribe_type"=>1,"company_id"=> $currentCompanyId, "subscription_date"=> "'".$checkUserForPaid['CompanyName']['subscription_date']."'", "subscription_expiry_date"=> "'".$subscriptionExpireDate."'"), array("user_id"=> $dataInput['user_id']));
							}else{
								$subscriptionData = array("subscribe_type"=>1,"user_id"=> $dataInput['user_id'],"company_id"=> $currentCompanyId, "subscription_date"=> "'".$checkUserForPaid['CompanyName']['subscription_date']."'", "subscription_expiry_date"=> $subscriptionExpireDate);
								$this->UserSubscriptionLog->save($subscriptionData);
							}
						}else{
							//** For new user setting first time institution[START]
								$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
								if(empty($checkUserSubscribed)){
									$subscriptionStartDate = date('Y-m-d 23:59:59');
									$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.$QrCodeData['QrCodeDetail']['validity'].' days'));
									$subscriptionDataVals = array("user_id"=> $userDetails['User']['id'],"company_id"=> 0, "subscription_date"=> $subscriptionStartDate, "subscription_expiry_date"=> $subscriptionExpireDate);
									$this->UserSubscriptionLog->save($subscriptionDataVals);
								}
							//** For new user setting first time institution[END]

							//** User edit institution paid to free update grace period[START]
							if(!empty($checkUserForPaidPreviousCompany) && empty($checkUserForPaid)){
								$subscriptionStartDate = date('Y-m-d 23:59:59');
								$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.$QrCodeData['QrCodeDetail']['validity'].' days'));
								if(!empty($checkUserSubscribed)){
									$this->UserSubscriptionLog->updateAll(array("company_id"=>$currentCompanyId, "subscribe_type"=>2, "subscription_date"=> "'".$subscriptionStartDate."'", "subscription_expiry_date"=> "'".$subscriptionExpireDate."'"), array("user_id"=> $dataInput['user_id']));
								}else{
									$subscriptionData = array("user_id"=> $dataInput['user_id'],"company_id"=> $currentCompanyId,"subscribe_type"=>2, "subscription_date"=> "'".$subscriptionStartDate."'", "subscription_expiry_date"=> $subscriptionExpireDate);
									$this->UserSubscriptionLog->save($subscriptionData);
								}
							}
							//** User edit institution paid to free update grace period[END]
						}
						//** Update free/enterprise user date[END]
						//** Get user Enterprise data[START]
						$status = 0;$expiryDate = 0;
						$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						//** When user comes after trust expiry date update grace period[START]
						$currentDate = date('Y-m-d 23:59:59');
						$gracePeriod = date('Y-m-d 23:59:59', strtotime($currentDate. ' + '.$QrCodeData['QrCodeDetail']['validity'].' days'));
						$trustExpiryDate = $checkUserSubscribed['UserSubscriptionLog']['subscription_expiry_date'];
						if(strtotime($currentDate) > strtotime($trustExpiryDate) && !empty($checkUserForPaid) && ($checkUserSubscribed['UserSubscriptionLog']['subscribe_type']==2)){
							$this->UserSubscriptionLog->updateAll(array("subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $dataInput['user_id']));
						}
						//** When user comes after trust expiry date update grace period[END]
						//** Get user SUBSCRIPTION status

						$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						if(!empty($checkUserSubscribedFinal)){
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
							$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
							if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
						}
						//** In Case of grace period oncall should be off[START]
						if($status==2){
							//** One time token to validate for QB[START]
							$oneTimeToken = "";
							$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenData = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
							$this->UserOneTimeToken->saveAll($oneTimeTokenData);
							//** One time token to validate for QB[END]
							$qbResponce = $this->updateOnCallStatusLite($userStatusDetails['User']['email'],$oneTimeToken, 0);
							if($qbResponce['qbsuccess'] == 1){
								$this->UserDutyLog->updateAll(array("status"=> 0), array("user_id"=> $dataInput['user_id'], "hospital_id"=> $currentCompanyId));
							}
						}
						//** In Case of grace period oncall should be off[END]
						$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate) > 0) ? strtotime($expiryDate):0);
						//** Get user Enterprise data[END]
						//** Add company details[START]
						$companyName = "";
						$employment = $this->UserEmployment->find("first", array("conditions"=> array("company_id"=> $currentCompanyId, "is_current"=> 1)));
						$companyNameData = $this->CompanyName->find("first", array("conditions"=> array("id"=> $currentCompanyId)));
						if(!empty($companyNameData)) {
							$companyName = $companyNameData['CompanyName']['company_name'];
							$patientNumberType = $companyNameData['CompanyName']['patient_number_type'];
						}
						$isCompanyAssignPending = 0;
						$isCompanyAssignCheck = 0;
						if($currentCompanyId !=1){
							if($companyNameData['CompanyName']['is_subscribed'] == 1 && $companyNameData['CompanyName']['status']==1){
								$isCompanyAssignCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> $userDetails['User']['email'], "company_id"=> $currentCompanyId, "status"=> 0)));
							}
						}
						if($isCompanyAssignCheck > 0){ $isCompanyAssignPending = 1; }
						$userEmploymentData = array("id"=> !empty($employment['UserEmployment']['id']) ? (int) $employment['UserEmployment']['id'] : (int) 0, "company_id"=> $currentCompanyId ,"company_name"=> $companyName,"patient_number_type"=>$patientNumberType,"is_current"=> $employment['UserEmployment']['is_current'],"is_company_approval_pending"=> $isCompanyAssignPending);
						//** Add company details[END]


						//** Add User Approval if not approved[START]

						$conditions = array('User.id'=> $dataInput['user_id']);
						$data = array(
						            'User.approved'=> 1
						        );
						$this->User->updateAll( $data ,$conditions );

						//** Add User Approval if not approved[END]

						//$settingData['settings'] = array($subscriptionData);
						$data = array('settings'=> array($subscriptionData), 'Company'=> array($userEmploymentData));

						// Add transaction for QR code selection [START]

						if(!empty($dataInput['Company'])){
							$this->InstituteSelectionTransaction->updateAll(
							    array('InstituteSelectionTransaction.status'=>0),
							    array('InstituteSelectionTransaction.user_id'=>$dataInput['user_id'])
							);
							foreach( $dataInput['Company'] as $company ){
								$transactionData = array(
									'user_id' => $dataInput['user_id'],
									'in_intitute'=> $company['company_id'],
									'out_institute'=> $company['previous_company_id'],
									'mode'=> 'scan',
									'qr_code_number'=> $dataInput['qr_code_number']
								);
							}
							$this->InstituteSelectionTransaction->saveAll($transactionData);
						}


						// Add transaction for QR code selection [END]
						//Update Dnd Status
						$params['user_id'] = $dataInput['user_id'];
						$params['email'] = $userDetails['User']['email'];
						$oneTimeTokenForDnd = $this->dissmissDndOnOcrAndQb($params);
						$responseData = array('method_name'=> 'addUserInstitutionLite', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $data);

						//** For first time free user send mail to sales[START]
						$checkFinalUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
						$chkAlreadySent = $this->EmailSmsLog->find("count", array("conditions"=>array("type"=> "email", "user_id"=> $dataInput['user_id'], "notify_type"=> "Free Trial User Mail to sales")));
						if(($chkAlreadySent == 0) && ($checkFinalUserSubscribed['UserSubscriptionLog']['subscribe_type'] == 0)){
							$mailParams = array();
							$mailParams['name'] = stripslashes($userDetails['UserProfile']['first_name']).' '.stripslashes($userDetails['UserProfile']['last_name']);
							$professionData = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
							$mailParams['profession'] = !empty($professionData['Profession']['profession_type']) ? $professionData['Profession']['profession_type'] :'N/A';
							$companyData = $this->CompanyName->find("first", array("conditions"=> array("id"=> $currentCompanyId)));
							$mailParams['institution_name'] = !empty($companyData['CompanyName']['company_name']) ? $companyData['CompanyName']['company_name'] : 'N/A';
							$mailParams['user_email'] = $userDetails['User']['email'];
							$mailParams['user_phone_number'] = !empty($userDetails['UserProfile']['contact_no']) ? $userDetails['UserProfile']['contact_no'] :'N/A';
							$mailParams['registration_date'] = substr($userDetails['User']['registration_date'],0,10);
							$mailToSales = $this->MbEmail->sendMailToSalesFreeEnterpriseUser($mailParams);
							$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailToSales, "user_id"=> $dataInput['user_id'], "notify_type"=> "Free Trial User Mail to sales") );
						}
						//** For first time free user send mail to sales[END]

						//**************** Code For cache Delete[START] ****************//
						$updateCache = $this->cacheDeleteOnInstitutionChange($dataInput['user_id'], $currentCompanyId,$previousCompanyId);
						//**************** Code For cache Delete[END]   ****************//


					}
					//** Add User Institution[END]

				// }else{
				// 	$responseData = array('method_name'=> 'addUserInstitutionLite', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
				// }
			}
			else
			{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
				$responseData = array('method_name'=> 'settings','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
		}else{
			$responseData = array('method_name'=> 'addUserInstitutionLite', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		//** Track API Request, Response[START]
		$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$t2 = explode(" ",microtime());
		$milisec2 = str_replace('.', '', substr((string)$t2[0],1,4));
		$headerVals = $this->getHeaderValues();
		$trackData = array(
							"user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> ($milisec2 - $milisec1),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		//** Track API Request, Response[END]
		exit;
	}

	public function loginLite(){
		ini_set('max_execution_time', 4*300);
    	ini_set('max_input_time', 4*300);
		$this->autoRender = false;
		$responseData = array();
		$number_of_attempts = 0;
		$attempt_time = 0;
		$updatePassword = 0;
		$reasonToupdatePassword = '';
		$userRemoteAddress = $_SERVER['REMOTE_ADDR'];
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			// if($this->validateAccessKey()){
				if( !empty($dataInput['email']) && !empty($dataInput['password']) ){

					$dataInput['email'] = strtolower($dataInput['email']);
					$params = array('email'=> $dataInput['email']);
					$userDetails = $this->User->userDetails( $params );

					$userLoginTransaction = $this->LoginAttemptTransaction->find("first", array("conditions"=> array("email"=> $dataInput['email'])));
					$number_of_attempts = !empty($userLoginTransaction['LoginAttemptTransaction']['attempt']) ? $userLoginTransaction['LoginAttemptTransaction']['attempt'] : 0;
					if(! empty($userLoginTransaction['LoginAttemptTransaction']['last_login']))
					{
						date_default_timezone_set('Asia/Kolkata');
						$last_login_time = strtotime($userLoginTransaction['LoginAttemptTransaction']['last_login']);
						$current_time = strtotime(date('Y-m-d H:i:s'));
						$difference = ($current_time - $last_login_time);
						$attempt_time = intval($difference/60);
					}

					if($attempt_time >= 10){
						if(!empty($userLoginTransaction)){
							$this->LoginAttemptTransaction->updateAll(array("attempt"=>0 , "status"=> 1, "last_login"=>'"'.date("Y-m-d H:i:s").'"'), array("email"=> $dataInput['email']));
							}else{
								$userLoginTransactionToInsert = array("email"=>$dataInput['email'], "attempt"=> $userLoginAttempt+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
								$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
							}
					}
					$userLoginTransaction = $this->LoginAttemptTransaction->find("first", array("conditions"=> array("email"=> $dataInput['email'])));
					$number_of_attempts = !empty($userLoginTransaction['LoginAttemptTransaction']['attempt']) ? $userLoginTransaction['LoginAttemptTransaction']['attempt'] : 0;
					if(! empty($userLoginTransaction['LoginAttemptTransaction']['last_login']))
					{
						date_default_timezone_set('Asia/Kolkata');
						$last_login_time = strtotime($userLoginTransaction['LoginAttemptTransaction']['last_login']);
						$current_time = strtotime(date('Y-m-d H:i:s'));
						$difference = ($current_time - $last_login_time);
						$attempt_time = intval($difference/60);
					}
					if($attempt_time <=10){
						if($number_of_attempts  <= 10){
							if(  $userDetails['User']['email'] == $dataInput['email'] && $userDetails['User']['password'] == md5($dataInput['password']) ){
								if( $userDetails['User']['status'] != 2){
									if( $userDetails['User']['status'] == 1 ){
										$dataUserDevice['device_type'] = isset($dataInput['device_type']) ? $dataInput['device_type']:'0';
										$admin_approval = array('is_admin_approved' => 1, 'message'=> '' );

										if($userDetails['User']['approved'] == 0 && $dataUserDevice['device_type'] == 'MBWEB'){
											$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "633", 'message'=> ERROR_633);

										}else{
											if($userDetails['User']['approved'] == 0){
												$admin_approval = array('is_admin_approved' => 0, 'message'=> ERROR_666 );
											}
												$dataUserDevice['user_id'] = $userDetails['User']['id'];
												$dataUserDevice['device_id'] = isset($dataInput['device_id']) ? $dataInput['device_id']:'';
												$dataUserDevice['token'] = md5(strtotime(date('Y-m-d H:i:s')) . $userDetails['User']['id'] . rand(1000,100000));
												// Used to distinguish the application type Shashi 07-06-16
												$dataUserDevice['application_type'] = isset($dataInput['application_type']) ? $dataInput['application_type']:'MB';
												$userData = $this->userFieldsLiteOne( $userDetails );
												$userData['admin_approval'] = $admin_approval;
												// Update Last Logged in Date
												$this->User->updateFields( array('User.last_loggedin_date'=> "'".date('Y-m-d H:i:s')."'"), array('User.id'=> $userDetails['User']['id']));

												//** For user current company paid then add in enterprise list if not[START]
												$checkEnterprise = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']), "company_id"=> $userData['Company']['company_id'])));
												if($checkEnterprise==0){
													$this->EnterpriseUserList->save(array("email"=> stripslashes($userDetails['User']['email']), "company_id"=> $userData['Company']['company_id'], "status"=> 0));
												}

												//** For user current company paid then add in enterprise list if not[END]

												//** Get Settings Values like CIP ON/OFF, force logout values[END]
												//** Get user Enterprise data[START]
												$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userData['user_id'])));
												$dataParams['email'] = stripslashes($userDetails['User']['email']);
												$dataParams['company_id'] = $userData['Company']['company_id'];
												$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);

												$checkUserSubscribedExistingUser = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userData['user_id'])));
												//** For existing user prior to enterprise feature add enterprise data first time[START]
												if(empty($checkUserSubscribedExistingUser)){
													if(empty($checkUserForPaid)){
														$subscriptionStartDate = date('Y-m-d 23:59:59');
														$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_TRIAL_PERIOD.' days'));
													}else{
														$subscriptionStartDate = $checkUserForPaid['CompanyName']['subscription_date'];
														$subscriptionExpireDate = $checkUserForPaid['CompanyName']['subscription_expiry_date'];
													}
														$subscriptionDataVals = array("user_id"=> $userData['user_id'],"company_id"=> $userData['Company']['company_id'], "subscription_date"=> $subscriptionStartDate, "subscription_expiry_date"=> $subscriptionExpireDate);
														$this->UserSubscriptionLog->save($subscriptionDataVals);
												}

												//** For existing user prior to enterprise feature add enterprise data first time[END]
												$status = 0;$expiryDate = 0;
//------ DEEPAK ---- START (commented to restrict grace)
												// $checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
												// //** When user comes after trust expiry date update grace period[START]
												// 	$currentDate = date('Y-m-d 23:59:59');
												// 	$gracePeriod = date('Y-m-d 23:59:59', strtotime($currentDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
												// 	$trustExpiryDate = $checkUserSubscribed['UserSubscriptionLog']['subscription_expiry_date'];
												// 	if(strtotime($currentDate) > strtotime($trustExpiryDate) && !empty($checkUserForPaid) && ($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] != 2)){
												// 		$this->UserSubscriptionLog->updateAll(array("company_id"=> $userData['Company']['company_id'], "subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $userDetails['User']['id']));
												// 	}elseif($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] == 1 && empty($checkUserForPaid)){
												// 		$this->UserSubscriptionLog->updateAll(array("company_id"=> $userData['Company']['company_id'],"subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $userDetails['User']['id']));
												// 	}
//------ DEEPAK ---- END
												//** When user comes after trust expiry date update grace period[END]
												//** Update user expiration[START]
												$checkUserSubscribedForexpire = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
												if(!empty($checkUserSubscribedForexpire) ){
													$paramData['subscription_expiry_date'] = $checkUserSubscribedForexpire['UserSubscriptionLog']['subscription_expiry_date'];//$checkUserForPaid['CompanyName']['subscription_expiry_date'];
													$isExpired = $this->UserSubscriptionLog->checkSusbcriptionExpired($paramData);
													if($isExpired){
														$this->UserSubscriptionLog->updateAll(array("company_id"=>$userData['Company']['company_id'], "subscribe_type"=> 3),array("user_id"=> $userDetails['User']['id']));
													}
													// DEEPAK --- change enterprise user list status in case of non paid user
													if($checkUserSubscribedForexpire['UserSubscriptionLog']['subscribe_type'] != 1){
															$this->EnterpriseUserList->updateAll(array("status"=> 0), array('email'=>stripslashes($userDetails['User']['email']),'company_id'=>$userData['Company']['company_id']));
														}
												}
												//** Update user expiration[END]
												//** Get user SUBSCRIPTION status
												$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
												if(!empty($checkUserSubscribedFinal)){
													//if(!empty($checkUserForPaid)){ $status = 1; }
													if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
													$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
													if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
													//** Check if expired
													if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
												}
												//** In Case of grace period oncall should be off[START]
												if($status==2){
													$this->UserDutyLog->updateAll(array("status"=> 0), array("user_id"=> $userData['user_id'], "hospital_id"=> $userData['Company']['company_id']));
												}
												//** In Case of user expired update cache[START]

												if($status==3){
													$userDataParams['company_id'] = isset($userCurrentCompany['UserEmployment']['company_id']) ? $userCurrentCompany['UserEmployment']['company_id'] : 0;
													$userDataParams['user_id'] = isset($userData['user_id']) ? $userData['user_id']: 0;
													$updateStaticCache = $this->saveStaticData($userDataParams);
												}

												//** In Case of user expired update cache[END]
												//** In Case of grace period oncall should be off[END]
												$subscriptionData = array("type"=> "SUBSCRIPTION", "status"=>$status, "value"=> (!empty($expiryDate) && strtotime($expiryDate) > 0) ? strtotime($expiryDate):0);
												//** Get user Enterprise data[END]
												$userData['settings'] = array($subscriptionData);


												$chatMessagedata = array("encryptionKey"=> 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd', "encryptionIv"=>'XX.s9GNvnP+zRA^Y');

												$responseData = array('method_name'=> 'login', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('User'=> $userData, 'MessageKey'=>$chatMessagedata));

												// $responseData = array('method_name'=> 'login', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('User'=> $userData));
												//** Add custom data on QB [START]
												$atwork = "0"; $oncall = "0";
												$dutyData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userData['user_id'])));
												if(!empty($dutyData)){
													$atwork = $dutyData['UserDutyLog']['atwork_status'];
													$oncall = $dutyData['UserDutyLog']['status'];
												}
											try{
												//** One time token to validate for QB[START]
												$oneTimeToken = $this->genrateRandomToken($userDetails['User']['id']);
												$oneTimeTokenData = array("user_id"=> $userDetails['User']['id'], "token"=> $oneTimeToken);
												$this->UserOneTimeToken->save($oneTimeTokenData);
												//** One time token to validate for QB[END]
												$tokenDetails = $this->Quickblox->quickLogin($dataInput['email'], $oneTimeToken);
												$token = $tokenDetails->session->token;
												$user_id = $tokenDetails->session->user_id;
												$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
												$custom_data_from_api = json_decode($user_details->user->custom_data);
												$customData['userRoleStatus'] = $custom_data_from_api->status;
												if(empty($customData['userRoleStatus']))
												{
													$customData['userRoleStatus'] = $userData['Profession']['profession_type'];
												}
												$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
												$customData['isImport'] = "true";
												$customData['at_work'] = (string) $atwork;
												$customData['on_call'] = (string) $oncall;
												if(isset($custom_data_from_api->dnd_status) && !empty($custom_data_from_api->dnd_status))
												{
													$customData['dnd_status'] = $custom_data_from_api->dnd_status;
													$customData['is_dnd_active'] = 1;
												}
												$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);

												//**********Login transaction new[START]***********//
												$dataUserDevice['login_status'] = "1";
												// $setLoginTransaction = $this->userLoginLogoutTransaction($dataUserDevice);
												//**********Login transaction new[END]*************//
											}catch(Exception $e){}
											if(!empty($userLoginTransaction)){
											$this->LoginAttemptTransaction->updateAll(array("attempt"=>0 , "status"=> 1, "last_login"=>'"'.date("Y-m-d H:i:s").'"'), array("email"=> $dataInput['email']));
											}else{
												$userLoginTransactionToInsert = array("email"=>$dataInput['email'], "attempt"=> $number_of_attempts+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
												$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
											}
										}
									}else{
										$userInformation =  $this->AdminActivityLog->find('first', array('conditions' => array('AdminActivityLog.user_id' => $userDetails['User']['id']),'order'=>array('AdminActivityLog.id DESC'),'limit'=> 1));
										if($userInformation['AdminActivityLog']['message'] == "Deactivated"){
												$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
										}else{
												$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "605", 'message'=> ERROR_605);
										}
									}
								}else{
									$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "651", 'message'=> ERROR_651);
								}
							}else{
								if(!empty($userLoginTransaction)){
								$this->LoginAttemptTransaction->updateAll(array("attempt"=>$number_of_attempts+1 , "status"=> 1), array("email"=> $dataInput['email']));
								}else{
									$userLoginTransactionToInsert = array("email"=>$dataInput['email'], "attempt"=> $number_of_attempts+1, "status"=> 1, "last_login"=> date("Y-m-d H:i:s"));
									$this->LoginAttemptTransaction->saveAll($userLoginTransactionToInsert);
								}
								$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "604", 'message'=> ERROR_604, "noa"=>$number_of_attempts);
							}

						}else{
							$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "660", 'message'=> ERROR_660);
						}
					}else{
						$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "660", 'message'=> ERROR_660);
					}
				}else{
					$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "603", 'message'=> ERROR_603);
				}
			// }else{
			// $response = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);
			// }
		}else{
			$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	public function userFieldsLiteOne( $ud = array(), $userType = NULL ){
		$userData = array();
		if( !empty($ud) ){
				//** Get User Country[START]
			$ud['User']['email'] = stripslashes($ud['User']['email']);
				$userCountry = ''; $userCountryCode = '';
				if( !empty($ud['UserProfile']['country_id']) ){
					$countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
					$userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';

					$userCountryCode = !empty($countryData['Country']['country_code'])?$countryData['Country']['country_code']:'';
				}
				//** Get User Country[END]
				//** Get QB details [START]
				$qbInfo = array(); $qbDetails = array();
				$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
				$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
					//** If QB id not found add qb id of user[START]
				$checkUserExistsOnUserQbDetail = $this->UserQbDetail->find("count", array("conditions"=> array("user_id"=> $ud['User']['id'])));
					if(empty($qbId)){
						$responseData = $this->checkQBUser($ud['User']['email']);
						$qbDetailsData = json_decode($responseData, true);
						if(empty($qbDetailsData)){
							//** One time token to validate for QB[START]
							$oneTimeToken = $this->genrateRandomToken($dataInput['user_id']);
							$oneTimeTokenData = array("user_id"=> $dataInput['user_id'], "token"=> $oneTimeToken);
							$this->UserOneTimeToken->save($oneTimeTokenData);
							//** One time token to validate for QB[END]
							$qbDetails = $this->Quickblox->quickAdduserFromOcr($ud['User']['email'], $oneTimeToken, $ud['User']['email'], $ud['User']['id'], $ud['UserProfile']['first_name'].' '.$ud['UserProfile']['last_name']);
							$responseData = $this->checkQBUser($ud['User']['email']);
							$qbDetailsData = json_decode($responseData, true);
							//** Add QB id to our DB
							if($checkUserExistsOnUserQbDetail == 0){
								$addQbDetailsData = array("user_id"=> $ud['User']['id'], "qb_id"=> $qbDetailsData['user']['id']);
								$this->UserQbDetail->save($addQbDetailsData);
							}else{
								$this->UserQbDetail->updateAll(array("qb_id"=> $qbDetailsData['user']['id']), array("user_id"=> $ud['User']['id']));
							}
						}else{
							//** Add QB id to our DB
							if($checkUserExistsOnUserQbDetail == 0){
								$addQbDetailsData = array("user_id"=> $ud['User']['id'], "qb_id"=> $qbDetailsData['user']['id']);
								$this->UserQbDetail->save($addQbDetailsData);
							}else{
								$this->UserQbDetail->updateAll(array("qb_id"=> $qbDetailsData['user']['id']), array("user_id"=> $ud['User']['id']));
							}
						}
						$qbId = !empty($qbDetailsData['user']['id']) ? $qbDetailsData['user']['id'] : 0;
					}
					//** If QB id not found add qb id of user[END]
				$qbInfo = array("id"=> $qbId);
				//** Get QB details [END]


				//** User Profession[START]
			$userProfession = '';
			if( !empty($ud['UserProfile']['profession_id']) ){
				$professionData = $this->Profession->findById( $ud['UserProfile']['profession_id'] );
				$userProfession  = !empty($professionData['Profession']['profession_type'])?$professionData['Profession']['profession_type']:'';
			}
			//** User Profession[END]

			//** Get Current Employment[START]
				$companyId = 0; $userCurrentCompanyName = ""; $isCurrent = 1;
				$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'], "is_current"=> 1)));
				$companyId = $userCurrentCompany['UserEmployment']['company_id'];
				if($companyId == -1){
					$userCurrentCompanyName = "None";
				}else if(!empty($userCurrentCompany['UserEmployment']['company_id'])){
					$companyId = $userCurrentCompany['UserEmployment']['company_id'];
					$userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $userCurrentCompany['UserEmployment']['company_id'], "status"=> 1, "created_by"=> 0)));
					if(!empty($userCompanyDetails)){
						$userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
						$patientNumberType = $userCompanyDetails['CompanyName']['patient_number_type'];
					}
				}
			//** Get Current Employment[END]

				$userData = array(
									'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
									'first_time_loggedin'=> ($ud['User']['last_loggedin_date'] == '0000-00-00 00:00:00') ? "Y":"N",
									'first_name'=> !empty($ud['UserProfile']['first_name']) ? stripslashes($ud['UserProfile']['first_name']):'',
									'last_name'=> !empty($ud['UserProfile']['last_name']) ? stripslashes($ud['UserProfile']['last_name']):'',
									'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
									'country'=> !empty($userCountry) ? $userCountry : '',
									'country_code'=> !empty($userCountryCode) ? $userCountryCode : '',
									'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
									'role_id'=> (!empty($ud['User']['role_id']) && $ud['User']['role_id'] == 1) ? $ud['User']['role_id'] : 0,
									'phone'=> array('mobile_no'=> !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'', 'is_mobile_verified'=> !empty($ud['UserProfile']['contact_no_is_verified']) ? $ud['UserProfile']['contact_no_is_verified']: 0),
									'Profession'=> array('id'=>!empty($ud['UserProfile']['profession_id'])?$ud['UserProfile']['profession_id']:"", 'profession_type'=> !empty($userProfession) ? $userProfession:''),
									'current_employment'=> $userCurrentCompanyName,
									'qb_details'=> $qbInfo,
									"role_status"=> !empty($ud['UserProfile']['role_status']) ? $ud['UserProfile']['role_status']:'',
									"Company"=> array("company_id"=> !empty($companyId)?$companyId:0,"company_name"=>$userCurrentCompanyName, "patient_number_type"=>$patientNumberType, "is_current"=> $isCurrent)
								);
			}
		return $userData;
	}


	public function getUserLoginToken(){
		ini_set('max_execution_time', 4*300);
    	ini_set('max_input_time', 4*300);
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$dataUserDevice['device_type'] = isset($dataInput['device_type']) ? $dataInput['device_type']:'0';
			// echo "<pre>";print_r($dataUserDevice);exit();
			$dataUserDevice['user_id'] = isset($dataInput['user_id']) ? $dataInput['user_id']:'0';
			$dataUserDevice['device_id'] = isset($dataInput['device_id']) ? $dataInput['device_id']:'';
			$dataUserDevice['token'] = md5(strtotime(date('Y-m-d H:i:s')) . $dataInput['user_id'] . rand(1000,100000));
			$dataUserDevice['application_type'] = isset($dataInput['application_type']) ? $dataInput['application_type']:'MB';
			if(! empty($dataUserDevice))
			{
				if(!empty($dataUserDevice)){
					App::import('model','UserDevice');
					$UserDevice = new UserDevice();
					$userId = $dataUserDevice['user_id'];
					if(in_array(strtolower($dataUserDevice['device_type']), array("ios","android"))){
						// echo "HEllO";exit();
						$mobileTokenCount = $UserDevice->query("SELECT count(*) tokenCount FROM user_devices WHERE user_id = $userId AND (LOWER(device_type) = 'ios' OR LOWER(device_type) = 'android')");
						// echo "<pre>";print_r($mobileTokenCount);exit();
						if($mobileTokenCount[0][0]['tokenCount'] > 0){
							$mobileTokenLogout = $UserDevice->query("UPDATE user_devices SET status = 0 WHERE user_id = $userId AND LOWER(application_type) = 'mb' AND  (LOWER(device_type) = 'ios' OR LOWER(device_type) = 'android')");
							$params['user_id'] = $dataUserDevice['user_id'];
							$params['device_id'] = $dataUserDevice['device_id'];
							$sendVoipPushNotification = $this->sendLogoutPushNotification($params);
						}
					}else if(in_array(strtolower($dataUserDevice['device_type']), array("mbweb","web","0"))){
						// echo "HEllOO";exit();
						$webTokenCount = $UserDevice->query("SELECT count(*) tokenCount FROM user_devices WHERE user_id = $userId AND (LOWER(device_type) = 'mbweb' OR LOWER(device_type) = 'web' OR LOWER(device_type) = '0')");
						if($webTokenCount[0][0]['tokenCount'] > 0){
							$UserDevice->query("UPDATE user_devices SET status = 0 WHERE user_id = $userId AND LOWER(application_type) = 'mb-web' AND  (LOWER(device_type) = 'web' OR LOWER(device_type) = 'mbweb' OR LOWER(device_type) = '0')");
						}
					}
				}

				// EHTERAM -- code
				// $userDeviceCheck = $this->UserDevice->find("count", array("conditions"=> array("user_id"=> $dataUserDevice['user_id'], "device_id"=> $dataUserDevice['device_id'], 'application_type'=> 'MB')));
				// 							//echo json_encode(array('value'=>$userDeviceCheck));exit();
				// if($userDeviceCheck == 0){
				// 	$this->UserDevice->save( $dataUserDevice );
				//
				// 	//** Inactive all other users device id
				// 	$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataUserDevice['device_id'], "user_id !="=> $dataUserDevice['user_id'], 'application_type'=> 'MB'));
				// }else{
				// 	$this->UserDevice->updateAll(array("status"=> 1), array("user_id"=> $dataUserDevice['user_id'], "device_id"=> $dataUserDevice['device_id'], 'application_type'=> 'MB'));
				// 	//** Inactive all other users device id
				// 	$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataUserDevice['device_id'], "user_id !="=> $dataUserDevice['user_id'], 'application_type'=> 'MB'));
				// }
				// EHTERAM -- code

				// DEEPAK -- Code
					$this->UserDevice->save( $dataUserDevice );
					$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataUserDevice['device_id'], "user_id !="=> $dataUserDevice['user_id'], 'application_type'=> 'MB'));
				// DEEPAK -- Code
			 	$userData['token'] = !empty($dataUserDevice['token']) ?$dataUserDevice['token'] : "";
				$tourValue="0";
				$tourGuideData = $this->TourGuideCompletionVisit->find('count',array('conditions'=>array('user_id'=>$dataUserDevice['user_id'],'device_type'=>$dataUserDevice['device_type'],'status'=>1)));
				if($tourGuideData > 0){
					$tourValue="1";
				}
				$userData['tourGuide'] = array("value"=>$tourValue);
				$userData['User'] = array('token'=> $userData['token'], 'tourGuide'=>$userData['tourGuide']);
				$responseData = array('method_name'=> 'getUserLoginToken', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'token'=> $userData['token'], 'tourGuide'=> $userData['tourGuide']);
				//**********Login transaction new[START]***********//
				$dataUserDevice['login_status'] = "1";
				try {
					$setLoginTransaction = $this->userLoginLogoutTransaction($dataUserDevice);
				} catch (Exception $e) {

				}
				//**********Login transaction new[END]*************//

			}
			else{
				$responseData = array('method_name'=> 'getUserLoginToken', 'status'=>"0", 'response_code'=> "611", 'message'=> ERROR_611);
			}

		}else{
			$responseData = array('method_name'=> 'getUserLoginToken', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;

	}

	// public function logoutFromOtherDevicesLite()
	// {
	// 	ini_set('max_execution_time', 4*300);
 //    	ini_set('max_input_time', 4*300);
	// 	$this->autoRender = false;
	// 	$responseData = array();
	// 	$dataInput = $this->request->input ( 'json_decode', true);
	// 	$dataUserDevice['device_type'] = isset($dataInput['device_type']) ? $dataInput['device_type']:'0';
	// 	$dataUserDevice['user_id'] = "588";
	// 	$dataUserDevice['device_id'] = isset($dataInput['device_id']) ? $dataInput['device_id']:'';
	// 	$dataUserDevice['token'] = md5(strtotime(date('Y-m-d H:i:s')) . "588" . rand(1000,100000));
	// 	// Used to distinguish the application type Shashi 07-06-16
	// 	$dataUserDevice['application_type'] = isset($dataInput['application_type']) ? $dataInput['application_type']:'MB';

	// 	if(!empty($dataUserDevice)){
	// 		App::import('model','UserDevice');
	// 		$UserDevice = new UserDevice();
	// 		$userId = $dataUserDevice['user_id'];
	// 		if(in_array(strtolower($dataUserDevice['device_type']), array("ios","android"))){
	// 			$mobileTokenCount = $UserDevice->query("SELECT count(*) tokenCount FROM user_devices WHERE user_id = $userId AND (LOWER(device_type) = 'ios' OR LOWER(device_type) = 'android')");
	// 			if($mobileTokenCount[0][0]['tokenCount'] > 0){
	// 				$mobileTokenLogout = $UserDevice->query("UPDATE user_devices SET status = 0 WHERE user_id = $userId AND LOWER(application_type) = 'mb' AND  (LOWER(device_type) = 'ios' OR LOWER(device_type) = 'android')");
	// 			}
	// 		}else if(in_array(strtolower($dataUserDevice['device_type']), array("mbweb","web","0"))){
	// 			$webTokenCount = $UserDevice->query("SELECT count(*) tokenCount FROM user_devices WHERE user_id = $userId AND (LOWER(device_type) = 'mbweb' OR LOWER(device_type) = 'web' OR LOWER(device_type) = '0')");
	// 			if($webTokenCount[0][0]['tokenCount'] > 0){
	// 				$UserDevice->query("UPDATE user_devices SET status = 0 WHERE user_id = $userId AND LOWER(application_type) = 'mb-web' AND  (LOWER(device_type) = 'web' OR LOWER(device_type) = 'mbweb' OR LOWER(device_type) = '0')");
	// 			}
	// 		}
	// 	}
	// 	// Get Token
	// 	$tokenParams = array(
	// 						'user_id'=>  "588",
	// 						'device_id'=> $dataInput['device_id'],
	// 						'status'=> 1
	// 						);
	// 	$tokenData = $this->UserDevice->tokenDetails( $tokenParams );
	// 	$userData['token'] = !empty($tokenData['UserDevice']['token']) ? $tokenData['UserDevice']['token'] : "";
	// 	$tourValue="0";
	// 	$tourGuideData = $this->TourGuideCompletionVisit->find('count',array('conditions'=>array('user_id'=>"588",'device_type'=>$dataUserDevice['device_type'],'status'=>1)));
	// 	if($tourGuideData > 0){
	// 		$tourValue="1";
	// 	}
	// 	$userData['tourGuide'] = array("value"=>$tourValue);
	// 	$response = array('method_name'=> 'logoutFromOtherDevicesLite', 'status'=>"1", 'response_code'=> '200', 'message'=> ERROR_200, 'data'=> $userData);

	// 	echo json_encode($responseData);
	// 	exit;
	// }

	// public function updateUserSubscription()
	// {
	// 	$userDetails = $this->User->find('all', array(
	// 				    'joins' => array(
	// 				        array(
	// 				            'table' => 'user_employments',
	// 				            'alias' => 'UserEmployment',
	// 				            'type' => 'LEFT',
	// 				            'conditions' => array(
	// 				                'User.id = UserEmployment.user_id'
	// 				            )
	// 				        )
	// 				    ),
	// 				    'conditions' => array(
	// 				        'Message.to' => 4
	// 				    ),
	// 				    'fields' => array('User.id', 'User.email', 'UserEmployment.company_id'),
	// 				    'order' => 'UserEmployment.id DESC'
	// 				));
	// 	foreach ($userDetails as $userDetail) {
	// 		$dataParams['email'] = stripslashes($userDetail['User']['email']);
	// 		$dataParams['company_id'] = $userDetail['UserEmployment']['company_id'];
	// 		$checkUserForPaid = $this->EnterpriseUserList->getUserSubscriptionDetails($dataParams);

	// 		$checkUserSubscribedExistingUser = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetail['User']['id'])));
	// 		//** For existing user prior to enterprise feature add enterprise data first time[START]
	// 		if(empty($checkUserSubscribedExistingUser)){
	// 			if(empty($checkUserForPaid)){
	// 				$subscriptionStartDate = date('Y-m-d 23:59:59');
	// 				$subscriptionExpireDate = date('Y-m-d 23:59:59', strtotime($subscriptionStartDate. ' + '.ENTERPRISES_USER_TRIAL_PERIOD.' days'));
	// 			}else{
	// 				$subscriptionStartDate = $checkUserForPaid['CompanyName']['subscription_date'];
	// 				$subscriptionExpireDate = $checkUserForPaid['CompanyName']['subscription_expiry_date'];
	// 			}
	// 				$subscriptionDataVals = array("user_id"=> $userDetail['User']['id'],"company_id"=> $userDetail['UserEmployment']['company_id'], "subscription_date"=> $subscriptionStartDate, "subscription_expiry_date"=> $subscriptionExpireDate);
	// 				$this->UserSubscriptionLog->save($subscriptionDataVals);
	// 		}
	// 		//** For existing user prior to enterprise feature add enterprise data first time[END]
	// 		$status = 0;$expiryDate = 0;
	// 		$checkUserSubscribed = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetail['User']['id'])));
	// 		//** When user comes after trust expiry date update grace period[START]
	// 			$currentDate = date('Y-m-d 23:59:59');
	// 			$gracePeriod = date('Y-m-d 23:59:59', strtotime($currentDate. ' + '.ENTERPRISES_USER_GRACE_PERIOD.' days'));
	// 			$trustExpiryDate = $checkUserSubscribed['UserSubscriptionLog']['subscription_expiry_date'];
	// 			if(strtotime($currentDate) > strtotime($trustExpiryDate) && !empty($checkUserForPaid) && ($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] != 2)){
	// 				$this->UserSubscriptionLog->updateAll(array("company_id"=> $userDetail['UserEmployment']['company_id'], "subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $userDetail['User']['id']));
	// 			}elseif($checkUserSubscribed['UserSubscriptionLog']['subscribe_type'] == 1 && empty($checkUserForPaid)){
	// 				$this->UserSubscriptionLog->updateAll(array("company_id"=> $userDetail['UserEmployment']['company_id'],"subscription_expiry_date"=>"'".$gracePeriod."'", "subscribe_type"=> 2),array("user_id"=> $userDetail['User']['id']));
	// 			}
	// 		//** When user comes after trust expiry date update grace period[END]
	// 		//** Update user expiration[START]
	// 		$checkUserSubscribedForexpire = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetail['User']['id'])));
	// 		if(!empty($checkUserSubscribedForexpire) ){
	// 			$paramData['subscription_expiry_date'] = $checkUserSubscribedForexpire['UserSubscriptionLog']['subscription_expiry_date'];//$checkUserForPaid['CompanyName']['subscription_expiry_date'];
	// 			$isExpired = $this->UserSubscriptionLog->checkSusbcriptionExpired($paramData);
	// 			if($isExpired){
	// 				$this->UserSubscriptionLog->updateAll(array("company_id"=>$userDetail['UserEmployment']['company_id'], "subscribe_type"=> 3),array("user_id"=> $userDetail['User']['id']));
	// 			}
	// 		}
	// 		//** Update user expiration[END]
	// 		//** Get user SUBSCRIPTION status
	// 		$checkUserSubscribedFinal = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'])));
	// 		if(!empty($checkUserSubscribedFinal)){
	// 			//if(!empty($checkUserForPaid)){ $status = 1; }
	// 			if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 1) { $status = 1; }
	// 			$expiryDate = $checkUserSubscribedFinal['UserSubscriptionLog']['subscription_expiry_date'];
	// 			if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 2) { $status = 2; }
	// 			//** Check if expired
	// 			if($checkUserSubscribedFinal['UserSubscriptionLog']['subscribe_type'] == 3) { $status = 3; }
	// 		}
	// 		//** In Case of grace period oncall should be off[START]
	// 		if($status==2){
	// 			$this->UserDutyLog->updateAll(array("status"=> 0), array("user_id"=> $userData['user_id'], "hospital_id"=> $userData['Company']['company_id']));
	// 		}
	// 		//** In Case of user expired update cache[START]
	// 	}
	// 	echo "Done";exit();

	// }

	public function sendTestEmail()
	{
		$params = array();
		$params['activationLink'] = "www.google.com";
		$params['activationLink'] = "www.google.com";
		$params['toMail'] = "ehteram@mediccreations.com";
		$params['from'] = NO_REPLY_EMAIL;
		$params['name'] = "Ehteram Ahmad";
		$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'MedicBleep Email Verify')));
		$params['temp'] = $templateContent['EmailTemplate']['content'];
		try{
			$mailSend = $this->MbEmail->verifyEmail( $params );
		}catch(Exception $e){
			$mailSend = $e->getMessage();
		}
		echo "<pre>";print_r($mailSend);exit();
	}


	public function sendNotificationData()
	{

		$conditions = array(
				'VoipPushNotification.push_notification_status' => 0
			);
		$joins = array(
				array(
					'table' => 'user_qb_details',
					'alias' => 'UserQbDetail',
					'type' => 'left',
					'conditions'=> array('VoipPushNotification.user_id = UserQbDetail.user_id')
				)
			);
		$options = array(
				'joins' => $joins,
				'fields' => array('UserQbDetail.qb_id','VoipPushNotification.push_notification_status', 'VoipPushNotification.date_created'),
			);
		$id_data = $this->VoipPushNotification->find('all',$options);
		$all_qb_ids = array();
		// echo "<pre>";print_r($id_data);exit();
		foreach ($id_data AS  $ids) {
			date_default_timezone_set('Asia/Kolkata');
			$lastUpdated = strtotime($ids['VoipPushNotification']['date_created']);
			$current_time = strtotime(date('Y-m-d H:i:s'));
			$difference = ($current_time - $lastUpdated);
			$timeInterVal = intval($difference/60);

			$hours = floor($timeInterVal / 60);
			// echo "<pre>";print_r($hours);exit();
			// echo $current_time. " ". $lastUpdated. " ". $timeInterVal. " ". $difference;exit();
			// echo "<pre>";print_r($timeInterVal);exit();
			if($hours >= 36)
			{
				if( ($ids['VoipPushNotification']['push_notification_status'] != 2 ))
				{
					if($ids['UserQbDetail']['qb_id'] != "" && $ids['UserQbDetail']['qb_id'] != null){
						$all_qb_ids[] = $ids['UserQbDetail']['qb_id'];
						// echo "<pre>";print_r($all_qb_ids);exit();
					}
				}
			}
		}
		// exit();
		// echo "<pre>";print_r($all_qb_ids);exit();
		$userDetails['User']['id'] = 588;
		$oneTimeToken = $this->genrateRandomToken($userDetails['User']['id']);
		$oneTimeTokenData = array("user_id"=> $userDetails['User']['id'], "token"=> $oneTimeToken);
		$this->UserOneTimeToken->saveAll($oneTimeTokenData);

		$email = "ehteram@mediccreations.com";
		$tokenDetails = $this->Quickblox->quickLogin($email, $oneTimeToken);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->Quickblox->getUserSubscription($token);
		echo "<pre>";print_r($user_details);exit();
		// echo "<pre>";print_r($tokenDetails);exit();
		if(isset($tokenDetails->session)){
			$token = $tokenDetails->session->token;
			$params['qbtoken'] = $token;
		    $params['recipients'] = array( "ids"=> $all_qb_ids);
		    $params['notificationMessage'] = $message;
		    $sendNotification = $this->sendNotification($params);
		    $sendNotificationArr = json_decode(json_encode($sendNotification));
		    echo "<pre>";print_r($sendNotificationArr);exit();

		}else{
            $responseData['status'] = 0;
            $responseData['message'] = 'We are facing some technical issue.';
			echo json_encode($responseData);
		}
		echo "<pre>";print_r($all_qb_ids);exit();

	}

	public function sendNotification($params = array()){
	  $messageArr = array(
	                        "aps"=> array("alert"=> $params['notificationMessage'], "sound"=> "notification_sound.mp3"),
	                        "ios_voip"=>1,
	                        "VOIPCall"=>1,
	                        "VOIPChatDelete"=>1
	                    );
	  $messageEncoded = base64_encode(json_encode($messageArr));
	  $postData=json_encode(array("event"=> array("notification_type"=> "push", "environment"=> (ENVIRONMENT == "live" ? "production" : "development"), "user"=> $params['recipients'], "message"=> $messageEncoded)));
	  $curl = curl_init();
	   curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'events.json');
	        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
	        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	                'Content-Type: application/json',
	                'QuickBlox-REST-API-Version: 0.1.0',
	                'QB-Token: ' . $params['qbtoken']
	            ));
	    $response = curl_exec($curl);
	                //echo "<pre>"; print_r($response);die;
	                if (!empty($response)) {
	                        return $response;
	                } else {
	                        return false;
	                }
	                curl_close($curl);
	    return $response;
	}


	public function getPowerBiToken(){
		$this->autoRender=false;
		$curl = curl_init();
		$response_val = "";
		$responseData = array();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://login.microsoftonline.com/common/oauth2/token",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "grant_type=password&scope=openid&resource=https%3A%2F%2Fanalysis.windows.net%2Fpowerbi%2Fapi&client_id=5f1076b4-4d4b-435d-a8b6-9898732055fd&username=deepak%40medicchat.onmicrosoft.com&password=Medic%40123",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/x-www-form-urlencoded",
		    "Postman-Token: 45f34700-4a6a-4e8b-ac61-d3e3179a3602",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
		if ($err) {
		  $response_val = "cURL Error #:" . $err;
		} else {
		  	$response = json_decode($response,true);

			$responseData['status'] = 1;
            $responseData['message'] = 'OK.';
		  	$responseData['key'] = $response['access_token'];
		  	$response_val = json_encode($responseData);
		}
		return $response_val;
		exit();
	}

	/*
	-------------------------------------------------------------------------------------------------------
	On: 12-11-2018
	I/P: JSON
	O/P: JSOn
	Desc: Login Logout transaction
	-------------------------------------------------------------------------------------------------------
	*/

	// public function userLoginLogoutTransaction( $params = array() )
	// {
	// 	$response = 0;
	// 	if( !empty($params) )
	// 	{
	// 		$userId = $params['user_id'];
	// 		$userDeviceInfo=$this->UserDeviceInfo->find('first',array('conditions'=>array('user_id'=>$userId), "order"=> array("id DESC")));
	// 		$saveTransactionData = array("user_id"=> $params['user_id'], "device_id"=>$params['device_id'], "token"=>$params['token'], "login_status"=>$params['login_status'], "device_type"=>$params['device_type'], "device_info"=>$userDeviceInfo['UserDeviceInfo']['device_info'], "application_type"=>$params['application_type']);
	//         $this->UserLoginTransaction->saveAll($saveTransactionData);
	//         $response = 1;
	// 	}
	// 	return $response;
	// }
	public function userLoginLogoutTransaction( $params = array() )
	{
		$response = 0;
		if( !empty($params) ){
			$userId = $params['user_id'];
			if((string)$params['login_status'] == '0'){
				$Info = $this->UserLoginTransaction->find('first',array('conditions'=>array('user_id'=>$userId,'token'=>$params['token'],'login_status'=>'1'),"order"=> array("id DESC")));
				$userDeviceInfo = $Info['UserLoginTransaction']['device_info'];
			}else{
				// EHTERAM CODE
				// $Info=$this->UserDeviceInfo->find('first',array('conditions'=>array('user_id'=>$userId), "order"=> array("id DESC")));
				// $userDeviceInfo = $Info['UserLoginTransaction']['device_info'];

				// to save a default value --- DEEPAK CODE
				$userDeviceInfo = 'NULL';
			}
			$saveTransactionData = array("user_id"=> $params['user_id'], "device_id"=>$params['device_id'], "token"=>$params['token'], "login_status"=>$params['login_status'], "device_type"=>$params['device_type'], "device_info"=>$userDeviceInfo, "application_type"=>$params['application_type']);
      $this->UserLoginTransaction->saveAll($saveTransactionData);
      $response = 1;
		}
		return $response;
	}

	// public function getDeviceData()
	// {
	// 	$userDeviceInfo=$this->UserDeviceInfo->find('first',array('conditions'=>array('user_id'=>"1"), "order"=> array("id DESC")));
	// 	echo "<pre>";print_r($userDeviceInfo);exit();
	// }

	public function sendSmsLinkForgotPassword($fgLink, $userDetails) {

		$contactNumber = $userDetails['UserProfile']['contact_no'];
		$to = preg_replace('#[^w()/.%-&]#',"",$contactNumber);
		// $to = "447800633716";
		$serviceId = 'MG0490a00ed855239165604a73dda63967';

		//Your message to send, Add URL encoding here.
		$textHtml = "Hi ".$userDetails['UserProfile']['first_name']."\n";
		$textHtml .= "You have asked for your password to be reset.\n";
		$textHtml .= $fgLink."\n";
		$textHtml .= "If it was you, then click above to reset your password.\n";
		$textHtml .= "If you think it was someone else, then please ignore this email.\n";
		$textHtml .= "Cheers,\n";
		$textHtml .= "Medic Bleep Team";
		$message = $textHtml;
		$fields = array(
			'Body' => urlencode($message),
			'MessagingServiceSid' => urlencode($serviceId),
			'To' => urlencode($to),
		);
		$fields_string ='';

		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/".Configure::read("twilio_details.account_sid")."/Messages.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_USERPWD, Configure::read("twilio_details.account_sid") . ":" . Configure::read("twilio_details.auth_token"));

		$result = curl_exec($ch);

		// ----- TRANSACTION [START] ----------
		$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $userDetails['User']['id'], "is_current"=> 1)));
		$companyId = $userCurrentCompany['UserEmployment']['company_id'];

		$messageResponse = array("sender"=>$userDetails['User']['email'],"send_to"=>serialize(array($userDetails)),"trust_id"=>$companyId,"sent_messages"=>'Reset Password',"type"=>"message", "response"=>$result);
        try{
         $this->NotificationBroadcastMessage->saveAll($messageResponse);
        }catch(Exception $e){}
        // ----- TRANSACTION [END] ----------

		if($result){
			return json_encode($result);
		}
		if (curl_errno($ch)) {
		    return 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

	}


	//******************** WORKING ******************//
	// public function voipPushNotification()
	// {
	// 	$responseData = array();
	// 	$dtoken = '';
	// 	if($this->request->is('post')){
	// 			date_default_timezone_set('Asia/Kolkata');
	// 		// if( $this->tokenValidate() && $this->accesskeyCheck() ){
	// 			$dataInput = $this->request->input ( 'json_decode', true) ;
	// 			$dataUserDevice['user_id'] = isset($dataInput['user_id']) ? $dataInput['user_id']:0;
	// 			$dataUserDevice['device_id'] = isset($dataInput['device_id']) ? $dataInput['device_id']:'';
	// 			$dataUserDevice['device_type'] = isset($dataInput['device_type']) ? $dataInput['device_type']:'';
	// 			$dataUserDevice['application_type'] = isset($dataInput['application_type']) ? $dataInput['application_type']:'MB';
	// 			$dataUserDevice['voip_notification'] = isset($dataInput['voip_notification']) ? $dataInput['voip_notification']:0;

	// 			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataUserDevice['user_id'])));

	// 			$userDetails['User']['id'] = $dataUserDevice['user_id'];
	// 			$oneTimeToken = $this->genrateRandomToken($userDetails['User']['id']);
	// 			$oneTimeTokenData = array("user_id"=> $userDetails['User']['id'], "token"=> $oneTimeToken);
	// 			$this->UserOneTimeToken->saveAll($oneTimeTokenData);

	// 			$email = $userDetails['User']['email'];
	// 			$tokenDetails = $this->Quickblox->quickLogin($email, $oneTimeToken);
	// 			// echo "<pre>";print_r($tokenDetails);exit();
	// 			$token = $tokenDetails->session->token;
	// 			$user_id = $tokenDetails->session->user_id;
	// 			$user_details = $this->Quickblox->getUserSubscription($token);
	// 			foreach ($user_details as $key => $value) {
	// 				if($value->subscription->device->udid == $dataUserDevice['device_id'] )
	// 				{
	// 					if($value->subscription->notification_channel->name == 'apns_voip')
	// 					{
	// 						$dtoken = $value->subscription->device->client_identification_sequence;
	// 					}
	// 				}
	// 			}
	// 			// $user_devices_subscription = end($user_details);
	// 			// $device_token = $user_devices_subscription->subscription->device->client_identification_sequence;
	// 			// echo $token."<br />";
	// 			// echo "<pre>";print_r($user_details);exit();


	// 			if(! empty($dataUserDevice))
	// 			{
	// 				date_default_timezone_set('Asia/Kolkata');
	// 				$getDeviceDetail = $this->VoipPushNotification->find("first", array("conditions"=> array("device_id"=> $dataUserDevice['device_id'], "user_id"=> $dataUserDevice['user_id'])));
	// 				if( !empty($getDeviceDetail))
	// 				{
	// 					$currentDateFormate = date('Y-m-d H:i:s');
	// 					$userDeviceId = $dataUserDevice['device_id'];
	// 					// echo "<pre>";print_r($userDeviceId );exit();
	// 					if(!empty($dtoken))
	// 					{
	// 						$this->VoipPushNotification->updateAll(array("date_created"=> "'".$currentDateFormate."'" , "push_notification_status"=> $dataUserDevice['voip_notification'], "device_token"=> "'".$dtoken."'"), array("device_id"=> $userDeviceId, "user_id"=> $dataUserDevice['user_id']));
	// 					}
	// 					else
	// 					{
	// 						$this->VoipPushNotification->updateAll(array("date_created"=> "'".$currentDateFormate."'" , "push_notification_status"=> $dataUserDevice['voip_notification']), array("device_id"=> $userDeviceId, "user_id"=> $dataUserDevice['user_id']));
	// 					}
	// 				}
	// 				else
	// 				{
	// 					// echo "<pre>";print_r("No");exit();
	// 					$insertUserdeviceDetail = array("user_id"=>$dataUserDevice['user_id'], "device_id"=> $dataInput['device_id'], "device_type"=> $dataInput['device_type'], "device_token"=>$dtoken, "application_type"=> $dataInput['application_type'], "push_notification_status"=> $dataUserDevice['voip_notification'], "status"=>1, "date_created"=> date('Y-m-d H:i:s'));
	// 					$this->VoipPushNotification->save($insertUserdeviceDetail);
	// 				}
	// 			}
	// 			if($dataUserDevice['voip_notification'] == "2")
	// 			{
	// 				$insertChatDeleteTransaction = array("user_id"=>$dataUserDevice['user_id'], "device_id"=> $dataInput['device_id'], "device_type"=> $dataInput['device_type'], "application_type"=> $dataInput['application_type'], "status"=>1);
	// 				// echo "<pre>";print_r($insertChatDeleteTransaction);exit;
	// 				$this->ChatDeleteTransaction->saveAll($insertChatDeleteTransaction);
	// 			}
	// 			// $log = $this->VoipPushNotification->lastQuery();
	// 			$responseData = array('method_name'=> 'voipPushNotification','status'=>'1','response_code'=>'200','message'=> ERROR_200, 'data'=>$dtoken, 'detail'=>$getDeviceDetail);

	// 		// }else{
	// 		// $responseData = array('method_name'=> 'getInstitutesMasterData', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
	// 		// }
	// 	}else{
	// 	$responseData = array('method_name'=> 'voipPushNotification','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	//     }
 //    	echo json_encode($responseData);
 //    	exit();
	// }

	// public function sendPushNotificationOnIos()
	// {

	// 	$conditions = array(
	// 			'VoipPushNotification.push_notification_status' => 0
	// 		);
	// 	$joins = array(
	// 			array(
	// 				'table' => 'user_qb_details',
	// 				'alias' => 'UserQbDetail',
	// 				'type' => 'left',
	// 				'conditions'=> array('VoipPushNotification.user_id = UserQbDetail.user_id')
	// 			)
	// 		);
	// 	$options = array(
	// 			'joins' => $joins,
	// 			'fields' => array('UserQbDetail.qb_id','VoipPushNotification.push_notification_status', 'VoipPushNotification.date_created','VoipPushNotification.device_token'),
	// 		);
	// 	$id_data = $this->VoipPushNotification->find('all',$options);
	// 	// echo "<pre>";print_r($id_data);exit();


	// 	foreach ($id_data AS  $ids) {
	// 		date_default_timezone_set('Asia/Kolkata');
	// 		$lastUpdated = strtotime($ids['VoipPushNotification']['date_created']);
	// 		$current_time = strtotime(date('Y-m-d H:i:s'));
	// 		$difference = ($current_time - $lastUpdated);
	// 		$timeInterVal = intval($difference/60);

	// 		$hours = floor($timeInterVal / 60);
	// 		// echo "<pre>";print_r($hours);exit();
	// 		// echo $current_time. " ". $lastUpdated. " ". $timeInterVal. " ". $difference;exit();
	// 		// echo "<pre>";print_r($timeInterVal);exit();
	// 		// if($hours >= 36)
	// 		// {
	// 			$deviceToken = $ids['VoipPushNotification']['device_token'];


	// 			// Put your private key's passphrase here:
	// 			$passphrase = 'password';

	// 			// Put your alert message here:
	// 			$message = 'Delete Chat';

	// 			// Put the full path to your .pem file
	// 			// MBDevPem.pem
	// 			// CertificatesVOIP.pem
	// 			$pemFile = WWW_ROOT.'/voipssl/'.'CertificatesVOIP.pem';
	// 			// App::import('Vendor',array('file' => 'voipssl/CertificatesVOIP.pem'));
	// 			// if(file_exists(WWW_ROOT.'/voipssl/'.'CertificatesVOIP.pem'))
	// 			// 	echo "Yes";
	// 			// else
	// 			// 	echo "No";
	// 			// exit();
	// 			// echo $pemFile;
	// 			$ctx = stream_context_create();
	// 			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
	// 			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

	// 			// Open a connection to the APNS server
	// 			$fp = stream_socket_client(
	// 				'ssl://gateway.sandbox.push.apple.com:2195', $err,
	// 				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	// 			if (!$fp)
	// 				exit("Failed to connect: $err $errstr" . PHP_EOL);

	// 			echo 'Connected to APNS' . PHP_EOL;

	// 			$body = array('VOIPCall' => '1',
	// 				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
	//                         "ios_voip"=>1,
	//                         "VOIPCall"=>1,
	//                         "VOIPChatDelete"=>1
	// 			);

	// 			$payload = json_encode($body);

	// 			// Build the binary notification
	// 			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

	// 			// Send it to the server
	// 			$result = fwrite($fp, $msg, strlen($msg));

	// 			if (!$result)
	// 				echo 'Message not delivered' . PHP_EOL;
	// 			else
	// 				echo 'Message successfully delivered' . PHP_EOL;

	// 			// Close the connection to the server
	// 			fclose($fp);
	// 			exit();
	// 		// }
	// 	}

	// }

	public function justCheck()
	{
		$userDetails['User']['id'] = 588;
		$oneTimeToken = $this->genrateRandomToken($userDetails['User']['id']);
		$oneTimeTokenData = array("user_id"=> $userDetails['User']['id'], "token"=> $oneTimeToken);
		$this->UserOneTimeToken->saveAll($oneTimeTokenData);

		$email = 'ehteram@mediccreations.com';
		$tokenDetails = $this->Quickblox->quickLogin($email, $oneTimeToken);
		echo "<pre>";print_r($tokenDetails);
		exit();
	}

	public function sendVoipOnDevice()
	{
		$type = array(0,1);
		$message = 'Delete Chat';
		$conditions = array(
				'VoipPushNotification.push_notification_status' => $type,
				'status' =>1
			);
		$joins = array(
				array(
					'table' => 'users',
					'alias' => 'User',
					'type' => 'left',
					'conditions'=> array('VoipPushNotification.user_id = User.id')
				)
			);
		$options = array(
				'joins' => $joins,
				'fields' => array('VoipPushNotification.push_notification_status', 'VoipPushNotification.date_created','VoipPushNotification.device_token','VoipPushNotification.fcm_voip_token','VoipPushNotification.device_type','User.email','User.id'),
			);
		$id_data = $this->VoipPushNotification->find('all',$options);
		// echo "<pre>";print_r($id_data);exit();
		$counter = 0;
		foreach ($id_data AS  $ids) {
			$params['fcm_voip_token'] = $ids['VoipPushNotification']['fcm_voip_token'];
		    $params['date_created'] = $ids['VoipPushNotification']['date_created'];
		    $params['notificationMessage'] = $message;
		    $params['email'] = $ids['User']['email'];
		    $params['user_id'] = $ids['User']['id'];
			if($ids['VoipPushNotification']['device_type'] == 'ios' || $ids['VoipPushNotification']['device_type'] == 'iOS')
			{
				$sendNotificationOnIos = $this->sendPushNotificationOnIos($params);
				echo "Counter--- ".$counter."<br />";
				echo "Device Type--- ".$ids['VoipPushNotification']['device_type']."<br />";
				echo "User ID--- ".$params['user_id']."<br />";
				echo $sendNotificationOnIos;
			}
			else if($ids['VoipPushNotification']['device_type'] == 'Android' || $ids['VoipPushNotification']['device_type'] == 'android')
			{
				$sendNotificationAndriod = $this->sendPushNotificationAndroid($params);
				$counter++;
				echo "Counter--- ".$counter."<br />";
				echo "Device Type--- ".$ids['VoipPushNotification']['device_type']."<br />";
				echo "User ID--- ".$params['user_id']."<br />";
				echo $sendNotificationAndriod;
			}
		}

		echo "Done";exit();
	}


	public function sendPushNotificationOnIos($params = array())
	{
		if(!empty($params))
		{
			$lastUpdated = strtotime($params['date_created']);
			$current_time = strtotime(date('Y-m-d H:i:s'));
			$difference = ($current_time - $lastUpdated);
			$timeInterVal = intval($difference/60);
			$userId = isset($params['user_id']) ? $params['user_id'] : "";

			$hours = floor($timeInterVal / 60);
			if($hours >= 72)
			{
				// echo $timeInterVal;exit();
				$deviceToken = $params['device_token'];
				$fcmVoipToken = $params['fcm_voip_token'];
				$passphrase = 'password';
				$message = 'Delete Chat';
				$pemFile = 'app/Config/voipssl/'.VOIP_CERTIFICATE_IOS;
				$ctx = stream_context_create();
				stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
				stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

				// Open a connection to the APNS server
				$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
				if (!$fp)
					exit("Failed to connect: $err $errstr" . PHP_EOL);

				echo 'Connected to APNS' . PHP_EOL;

				$body = array('VOIPCall' => '1',
					"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
	                        "ios_voip"=>1,
	                        "VOIPCall"=>1,
	                        "VOIPChatDelete"=>1,
	                        "user_id"=> $userId
				);

				$payload = json_encode($body);

				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));

				if (!$result)
					$var = 'Message not delivered' . PHP_EOL;
					// echo 'Message not delivered' . PHP_EOL;
				else
					$var = 'Message successfully delivered' . PHP_EOL;
					// echo 'Message successfully delivered' . PHP_EOL;

				// Close the connection to the server
				fclose($fp);
				return $var;
			}
			else
			{
				// echo "GGGG";exit();
				return $var = "No need to send push notification";
			}
		}
	}

	public function sendPushNotificationAndroid($params = array()){
		if(!empty($params))
		{
			$lastUpdated = strtotime($params['date_created']);
			$current_time = strtotime(date('Y-m-d H:i:s'));
			$difference = ($current_time - $lastUpdated);
			$timeInterVal = intval($difference/60);

			$hours = floor($timeInterVal / 60);
			if($hours >= 72)
			{
				$responseData = $this->sendPushNotificationOnAndroid($params);
				return $responseData;
			}
			else
			{
				// echo "GGGG";exit();
				return $responseData = "No need to send push notification";
			}
		}
	}

	public function sendPushNotificationOnAndroid($params = array())
	{
		if(!empty($params['fcm_voip_token'])){
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$userId = isset($params['user_id']) ? $params['user_id'] : "";
			$voipPushMessage = array
					(
						'VOIPChatDelete' 	=> 1,
						'VOIPCall'		=> 1,
						'user_id' => $userId
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $result;
		}
		else
		{
			return "Token is empty";
		}
	}

	/*
	-------------------------------------------------------------------------------------
	On: 26-02-2019
	I/P: 
	O/P: 
	Desc: 
	-------------------------------------------------------------------------------------
	*/
	public function contactUsSubmit(){ 
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : ""; 
			$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : ""; 
			$message = isset($_REQUEST['message']) ? $_REQUEST['message'] : ""; 
			try{
				$params = array();
				$params['to_mail'] = 'support@medicbleep.com';
				// $params['to_mail'] = 'deepakkumar@mediccreations.com';
				$params['fromMail'] = "noreply@medicbleep.com";
				$params['fromName'] = 'No Reply';
				$params['mailSubject'] = 'Contact Us';

				$params['user_name'] = $name;
				$params['user_email'] = $email;
				$params['user_message'] = $message;

				$contactus = $this->MbEmail->contactUs($params);
				if($contactus){
					$responseData = array('method_name'=> 'contactUsSubmit', 'status'=>"1", 'response_code'=> "200");
				}
			}catch(Exception $e){
				$responseData = array('method_name'=> 'contactUsSubmit', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "system_errors"=> $e->getMessage());
			}
		}else{
			$responseData = array('method_name'=> 'contactUsSubmit', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	public function getUserActiveBatonRoles($userId)
	{
		$batonRoleNameList = array();
		if(isset($userId))
		{
			$getUserBatonRoles = $this->UserBatonRole->find("all", array("conditions"=> array("from_user"=> $userId,"is_active"=>1), "group"=>array('UserBatonRole.role_id'), 'order'=>array('UserBatonRole.updated_at')));
			foreach ($getUserBatonRoles as $value) {
				$getBatonRolesName = $this->BatonRole->find("first", array("conditions"=> array("id"=> $value['UserBatonRole']['role_id'])));
				$getBatonRolesNameOne = $this->DepartmentsBatonRole->find("first", array("conditions"=> array("role_id"=> $value['UserBatonRole']['role_id'])));

				$batonRoleNameList[] = array(
				'role_id'=> isset($getBatonRolesName['BatonRole']['id']) ? $getBatonRolesName['BatonRole']['id'] : "0",
				'role_name'=>isset($getBatonRolesName['BatonRole']['baton_roles']) ? $getBatonRolesName['BatonRole']['baton_roles'] : "",
				'on_call_value'=>isset($getBatonRolesNameOne['DepartmentsBatonRole']['on_call_value']) ? $getBatonRolesNameOne['DepartmentsBatonRole']['on_call_value'] : "0",
				'type'=>isset($value['UserBatonRole']['status']) ? $value['UserBatonRole']['status'] : "0",
				);

			}
			return $batonRoleNameList;
		}
	}

	public function getOtherUserActiveBatonRoles($userId)
	{
		$batonRoleNameList = array();
		if(isset($userId))
		{
			$userbatonRolestatus = array(1,2,5,7);
			$getUserBatonRoles = $this->UserBatonRole->find("all", array("conditions"=> array("from_user"=> $userId,
				"status" =>$userbatonRolestatus,  "is_active"=>1), "group"=>array('UserBatonRole.role_id'), 'order'=>array('UserBatonRole.updated_at')));
			foreach ($getUserBatonRoles as $value) {
				$getBatonRolesName = $this->BatonRole->find("first", array("conditions"=> array("id"=> $value['UserBatonRole']['role_id'])));
				$getBatonRolesNameOne = $this->DepartmentsBatonRole->find("first", array("conditions"=> array("role_id"=> $value['UserBatonRole']['role_id'])));

				$batonRoleNameList[] = array(
				'role_id'=> isset($getBatonRolesName['BatonRole']['id']) ? $getBatonRolesName['BatonRole']['id'] : "0",
				'role_name'=>isset($getBatonRolesName['BatonRole']['baton_roles']) ? $getBatonRolesName['BatonRole']['baton_roles'] : "",
				'on_call_value'=>isset($getBatonRolesNameOne['DepartmentsBatonRole']['on_call_value']) ? $getBatonRolesNameOne['DepartmentsBatonRole']['on_call_value'] : "0",
				'type'=>isset($value['UserBatonRole']['status']) ? $value['UserBatonRole']['status'] : "0",
				);

			}
			return $batonRoleNameList;
		}
	}

}// End Class
