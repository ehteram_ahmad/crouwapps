<?php
/*
Desc: API data get/post to Webservices.
*/

class MbapiroletagsController extends AppController {

	public $uses = array('Mbapi.Profession','Mbapi.RoleTag');
	public $components = array('Common');

	
	/*
	-------------------------------------------------------------------------------------
	On: 25-09-2017
	I/P: profession_id
	O/P: All Role tag key values
	Desc: 
	-------------------------------------------------------------------------------------
	*/
	public function getRoleTags(){
		$this->autoRender = false;
			$response = array();
			if($this->validateAccessKey()){
			$dataInput = $this->request->input ( 'json_decode', true) ;
				try{	
						$params = array();
						$professionData = $this->Profession->find("first", array("conditions"=> array("id"=> $dataInput['profession_id'], "status"=>1)));
						if(!empty($professionData)){
							$roleTagVals = array();
							$params['roleTags'] = $professionData['Profession']['tags'];
							$params['countryId'] = $professionData['Profession']['country_id'];
							$roleTagData = $this->RoleTag->roleTagVals($params);
							if(!empty($roleTagData)){
								foreach( $roleTagData as $roleTag){
									$roleTagVals[] = array("tag"=> $roleTag['RoleTag']['key'], "values"=> explode(",",$roleTag[0]['value']));
								}
							}
							$response = array('method_name'=> 'getRoleTags', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('role_tags'=> $roleTagVals));
						}else{
							$response = array('method_name'=> 'getRoleTags', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
						}
					}catch(Exception $e){	
						$response = array('method_name'=> 'getRoleTags', 'status'=>"0", 'response_code'=> '615', 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
			}else{
				$response = array('method_name'=> 'getRoleTags', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);	
			}
			echo json_encode($response);
			exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 
	I/P: Key value
	O/P: All values of any key
	Desc: 
	-------------------------------------------------------------------------------------
	*/
	public function getRoleTagValueByKey(){
		$this->autoRender = false;
			$response = array();
			if($this->validateAccessKey()){
			$dataInput = $this->request->input ( 'json_decode', true) ;
				//** Get values by key[START]
				if(file_exists(APP.'tmp/cache/models/myapp_cake_model_default_live_ocr_role_tags')){
				    unlink(APP.'tmp/cache/models/myapp_cake_model_default_live_ocr_role_tags');
				}
				$roleTagData = $this->RoleTag->roleTagValsLite($dataInput);
				$tagData['tag'] = $dataInput['tagKey'];
				if(!empty($roleTagData)){
					/*$tagData['tag'] = $roleTagData['0']['RoleTag']['key'];
					$tagData['values'] = explode(',', $roleTagData['0']['0']['value']);*/
					foreach($roleTagData as $rt){
						$tagDataVals[] = $rt['RoleTag']['value'];
					}
					$tagData['values'] = $tagDataVals;
					$response = array('method_name'=> 'getRoleTagValueByKey', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('role_tags'=> $tagData));
				}else{
					//$tagData['tag'] = $dataInput['tagKey'];
					$tagData['values'] = array();
					$response = array('method_name'=> 'getRoleTagValueByKey', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('role_tags'=> $tagData));
				}
				//** Get values by key[END]
			}else{
				$response = array('method_name'=> 'getRoleTagValueByKey', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);	
			}
			echo json_encode($response);
			exit;
	}
}// End Class
