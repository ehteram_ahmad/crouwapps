<?php


App::uses('AppController', 'Controller');

class MbappdetailsController extends AppController {
	public $uses = array('MbAppVersion', 'Mb.AppDownloadLog', "Mb.ApiRequestResponseTrack", "Mb.AppDeviceKey");
	
	/*
	-------------------------------------
	On: 
	I/P:
	O/P:
	Desc: fetches app version details for Android, ios
	-------------------------------------
	*/
	// public function appVersionDetails(){


		
	// 	$this->autoRender = false;
	// 	$responseData = array();
	// 	$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
	// 	if($this->request->is('post')) {
	// 		// $dataInput = $this->request->input ( 'json_decode', true) ;
	// 		// $encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
	// 		// $dataInput = json_decode($encryptedData, true);
	// 		//if( $this->tokenValidate() ){
	// 			//** Fetch app version details
	// 		   //echo '<pre>';print_r($dataInput);exit();
	// 			$randKeyStr = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
	// 			$randKeyIv = strtoupper('C+BWA9SEBDDCE^GY');
	// 			$header = getallheaders();
	// 			$dataInput = $this->request->input ( 'json_decode', true) ;
	// 			// echo "<pre>";print_r($dataInput);exit();
	// 			$getDecreptedResponce = $this->dataDecryptionByPrivateKey($dataInput);
	// 			// echo "<pre>";print_r($getDecreptedResponce);exit();
	// 			$encryData = $getDecreptedResponce['values'];
	// 			$paramsDataJson = $getDecreptedResponce['params'];
	// 			$paramsDataArr = json_decode($paramsDataJson);
	// 			$dataInput['key'] = $paramsDataArr->key;
	// 			$dataInput['iv'] = $paramsDataArr->iv;
	// 			$encryptedData = $this->dataDecryptionByAes( $encryData, $dataInput['key'], $dataInput['iv']);
	// 			// echo "<pre>";print_r($encryptedData);exit();
	// 			$dataInput = json_decode($encryptedData, true);
	// 			$deviceId = $header['device_id'];
	// 			$paramsEncryptedByRsa = $this->encryptionByRsa($deviceId);					
	// 			$forceUpdate = 0;
	// 			$conditionsForUser = array("version"=> $dataInput["version"],"device_type"=> $dataInput["device_type"], "status"=> 1);
	// 			//$appVersionDetails = $this->MbAppVersion->find("first", array("conditions"=> $conditions));
	// 			$userCurrentAppVersionDetails = $this->MbAppVersion->find("first", array("conditions"=> $conditionsForUser));
				
	// 			$startId = $userCurrentAppVersionDetails['MbAppVersion']['id'];
	// 			$conditionsForUpdate = array("device_type"=> $dataInput["device_type"], "status"=> 1);
	// 			$latestUpdate = $this->MbAppVersion->find("first",array("conditions"=>$conditionsForUpdate,"order"=>array("created DESC") ) );
	// 			$endId = $latestUpdate['MbAppVersion']['id'];
	// 			if($startId == $endId){
	// 				$versionDetails = array("version"=> $userCurrentAppVersionDetails['MbAppVersion']['version'], "force_update"=> $userCurrentAppVersionDetails['MbAppVersion']['force_update'], "update_frequency"=> $userCurrentAppVersionDetails['MbAppVersion']['update_frequency'] );
	// 				$versionDetails = json_encode($versionDetails);
	// 				$encryptedData = $this->encryptDataNew( $versionDetails, $randKeyStr, $randKeyIv);
	// 				$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
				
	// 			}else{
	// 				$returnVal = 0;
	// 				for($i=$startId+1;$i<=$endId;$i++){
	// 				$conditionsForUpdate = array("id"=>$i,"device_type"=> $dataInput["device_type"],"force_update"=>1, "status"=> 1);
	// 				$checkForForceUpdate = $this->MbAppVersion->find('all',array('conditions'=>$conditionsForUpdate));
	// 				if(!empty($checkForForceUpdate)){
	// 					$returnVal = $returnVal + 1;
						
	// 				}

	// 			   }
	// 			   if($returnVal > 0){
	// 			   	$versionDetails = array("version"=> $latestUpdate['MbAppVersion']['version'], "force_update"=> 1, "update_frequency"=> $latestUpdate['MbAppVersion']['update_frequency'] );
	// 			   	$versionDetails = json_encode($versionDetails);
	// 				$encryptedData = $this->encryptDataNew( $versionDetails, $randKeyStr, $randKeyIv);
	// 				$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
				
	// 			   }else{
	// 			   	$versionDetails = array("version"=> $latestUpdate['MbAppVersion']['version'], "force_update"=> 0, "update_frequency"=> $latestUpdate['MbAppVersion']['update_frequency'] );
	// 			   	$versionDetails = json_encode($versionDetails);
	// 				$encryptedData = $this->encryptDataNew( $versionDetails, $randKeyStr, $randKeyIv);
	// 				$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
				  
	// 			   }
	// 			}// End if else


				
	// 			/*
	// 			if($appVersionDetails['MbAppVersion']['force_update']){
	// 				$forceUpdate = 1;
	// 			}
				
	// 			if(!empty($appVersionDetails)){
	// 				$versionDetails = array("version"=> $appVersionDetails['MbAppVersion']['version'], "force_update"=> $forceUpdate, "update_frequency"=> $appVersionDetails['MbAppVersion']['update_frequency'] );
	// 				$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
	// 			}else{
	// 				$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
	// 			}
	// 			*/
			
	// 	}else{
	// 		$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
	// 	}
	// 	// $encryptedData = $this->Common->encryptData(json_encode($responseData));
	// 	echo json_encode($responseData);
	// 	//** Track API Request, Response[START]
	// 	$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
	// 	$headerVals = $this->getHeaderValues();
	// 	$trackData = array(
	// 						"user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : 0,
	// 						"request_at"=> $startTime,
	// 						"response_at"=> $endTime,
	// 						"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
	// 						"response_val"=> json_encode($responseData),
	// 						"api_name"=> $_SERVER['REQUEST_URI'],
	// 						"process_time"=> ($endTime - $startTime),
	// 						"device_type"=> $headerVals['device_type']
	// 					);
	// 	try{
	// 		$this->ApiRequestResponseTrack->save($trackData);
	// 	}catch(Exception $e){}
	// 	//** Track API Request, Response[END]
	// 	exit;
		
	// }


	public function appVersionDetails(){


		
		$this->autoRender = false;
		$responseData = array();
		$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		if($this->request->is('post')) {
			// $dataInput = $this->request->input ( 'json_decode', true) ; 
			// $dataInput = json_decode($dataInput, true);
			$dataInput = $this->request->input ( 'json_decode', true) ;
			// echo "<pre>";print_r($dataInput);exit();
				$forceUpdate = 0;
				$conditionsForUser = array("version"=> $dataInput["version"],"device_type"=> $dataInput["device_type"], "status"=> 1);
				$userCurrentAppVersionDetails = $this->MbAppVersion->find("first", array("conditions"=> $conditionsForUser));
				
				$startId = $userCurrentAppVersionDetails['MbAppVersion']['id'];
				$conditionsForUpdate = array("device_type"=> $dataInput["device_type"], "status"=> 1);
				$latestUpdate = $this->MbAppVersion->find("first",array("conditions"=>$conditionsForUpdate,"order"=>array("created DESC") ) );
				$endId = $latestUpdate['MbAppVersion']['id'];
				if($startId == $endId){
					$versionDetails = array("version"=> $userCurrentAppVersionDetails['MbAppVersion']['version'], "force_update"=> $userCurrentAppVersionDetails['MbAppVersion']['force_update'], "update_frequency"=> $userCurrentAppVersionDetails['MbAppVersion']['update_frequency'] );
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
				
				}else{
					$returnVal = 0;
					for($i=$startId+1;$i<=$endId;$i++){
					$conditionsForUpdate = array("id"=>$i,"device_type"=> $dataInput["device_type"],"force_update"=>1, "status"=> 1);
					$checkForForceUpdate = $this->MbAppVersion->find('all',array('conditions'=>$conditionsForUpdate));
					if(!empty($checkForForceUpdate)){
						$returnVal = $returnVal + 1;
						
					}

				   }
				   if($returnVal > 0){
				   	$versionDetails = array("version"=> $latestUpdate['MbAppVersion']['version'], "force_update"=> 1, "update_frequency"=> $latestUpdate['MbAppVersion']['update_frequency'] );
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
				
				   }else{
				   	$versionDetails = array("version"=> $latestUpdate['MbAppVersion']['version'], "force_update"=> 0, "update_frequency"=> $latestUpdate['MbAppVersion']['update_frequency'] );
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
				  
				   }
				}
			
		}else{
			$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode(array("values"=> $responseData));
		// $encryptedData = $this->Common->encryptData(json_encode($responseData));
		// echo json_encode($responseData);
		//** Track API Request, Response[START]
		$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$headerVals = $this->getHeaderValues();
		$trackData = array(
							"user_id"=> isset($dataInput['user_id']) ? $dataInput['user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> ($endTime - $startTime),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		exit;
		
	}

	/*
	-------------------------------------
	On: 19-10-2016
	I/P: JSON DATA
	O/P: JSON DATA as response
	Desc: Inserts App install by user with version
	-------------------------------------
	*/
	public function appInstallLogAdd(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$randKeyStr = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
			$randKeyIv = strtoupper('C+BWA9SEBDDCE^GY');
			$header = getallheaders();
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$getDecreptedResponce = $this->dataDecryptionByPrivateKey($dataInput);
			$encryData = $getDecreptedResponce['values'];
			$paramsDataJson = $getDecreptedResponce['params'];
			$paramsDataArr = json_decode($paramsDataJson);
			$dataInput['key'] = $paramsDataArr->key;
			$dataInput['iv'] = $paramsDataArr->iv;
			$encryptedData = $this->dataDecryptionByAes( $encryData, $dataInput['key'], $dataInput['iv']);
			$dataInput = json_decode($encryptedData, true);
			$deviceId = $header['device_id'];
			$paramsEncryptedByRsa = $this->encryptionByRsa($deviceId);	
			if( $this->tokenValidation() ){
				//** Add App install log

				$logData = array(
						"user_id"=> $dataInput['user_id'],
						"version"=> $dataInput['version'],
						"device_id"=> $dataInput['device_id'],
						"device_type"=> $dataInput['device_type'],
						"application_type"=> "MB"
					);
				try{
					$addLogData = $this->AppDownloadLog->save($logData);
					$responseData = array('method_name'=> 'appInstallLogAdd', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
				}catch(Exception $e){
					$responseData = array('method_name'=> 'appInstallLogAdd', 'status'=>"0", 'response_code'=> '615', 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
				}
			}else{
				$responseData = array('method_name'=> 'appInstallLogAdd', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'appInstallLogAdd', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		// $encryptedData = $this->Common->encryptData(json_encode($responseData));
		// echo json_encode(array("values"=> $encryptedData));
		// exit;
		// $encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode($responseData);
		exit;		
	}

	public function dataDecryptionByPrivateKey($dataInput)
	{
		$dataInputArr = $dataInput;
		// return $dataInputArr;
		if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		else
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		// return $pkGeneratePrivate;
		$paramsData = $dataInputArr['params'];
		$valueData = $dataInputArr['values'];
		openssl_private_decrypt(base64_decode($paramsData), $decrptedData, $pkGeneratePrivate);
		$newArr = array('params'=> $decrptedData, 'values'=> $valueData);
		return $newArr;
	}

	/*
	------------------------------------------------------------------------------
	ON: 24-04-18
	I/P: $values And $params in encrypted form
	O/P: JSON
	Desc: Get $deviceId And $publicKey And return server public key
	------------------------------------------------------------------------------
	*/

	public function dataDecryptionByAes( $encrypted = NULL, $encryptKey, $ivKey ){
		//** Decrypting Data
		// $encryptKey = 'd8dd1e44d4576fd72c39d587df33d81e';
		// $ivKey = 'd2b48574aa832a67';

		$encrypted = base64_decode($encrypted);
		$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $encrypted, MCRYPT_MODE_CBC, $ivKey );
	  	//** Remove Padding
	  	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
        return $decrypted;
	}

	/*
	------------------------------------------------------------------------------
	ON: 24-04-18
	I/P: $values And $params in encrypted form
	O/P: JSON
	Desc: Get $deviceId And $publicKey And return server public key
	------------------------------------------------------------------------------
	*/

	public function encryptionByRsa($deviceId)
	{
		$randKeyStr = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$randKeyIv = strtoupper('C+BWA9SEBDDCE^GY');
		$paramsResponceArr = array('key'=>$randKeyStr, 'iv'=>$randKeyIv);
		$paramsResponceJson = json_encode($paramsResponceArr);
		$getClientPublicKey = $this->AppDeviceKey->find('first', 
                array('conditions'=> 
                  array(
                    'device_id'=> $deviceId
                    )
                ));
		App::import('Vendor','Crypt_RSA',array('file' => 'phprsa/Crypt/RSA.php'));
		$rsaObj = new Crypt_RSA();
		$clientPublicKey = $getClientPublicKey['AppDeviceKey']['client_public_key'];
			$rsaObj->loadKey('-----BEGIN PUBLIC KEY-----
'.$clientPublicKey.'
-----END PUBLIC KEY-----');
			$rsaObj->setPublicKey();
			$publickey = $rsaObj->getPublicKey(CRYPT_RSA_PUBLIC_FORMAT_OPENSSH);
			$ciphertext = $rsaObj->encrypt($paramsResponceJson);
			return base64_encode($ciphertext);
	}

	public function encryptionByAes($values)
	{
		// $encryptKey = 'nS4Gpg9VND6FrOqS16VTWeRfMwc4jR07';
		// $ivKey = 'tLYtUdNUNb2HtkHL';

		$encryptKey = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$ivKey = strtoupper('C+BWA9SEBDDCE^GY');
		//** Get text with padding
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$pad = $size - (strlen($text) % $size);
    	$text = $text . str_repeat(chr($pad), $pad);
    	//** Get encrypted text
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $text, MCRYPT_MODE_CBC, $ivKey ) );
		return $encrypted;
	}

	public function decryptDataNew( $encrypted = NULL, $encryptKey, $ivKey ){
		//** Decrypting Data
		// $encryptKey = 'd8dd1e44d4576fd72c39d587df33d81e';
		// $ivKey = 'd2b48574aa832a67';

		$encrypted = base64_decode($encrypted);
		$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $encrypted, MCRYPT_MODE_CBC, $ivKey );
	  	//** Remove Padding
	  	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
        return $decrypted;
	}

	public function encryptDataNew( $text = NULL, $encryptKey, $ivKey){
		// $encryptKey = 'nS4Gpg9VND6FrOqS16VTWeRfMwc4jR07';
		// $ivKey = 'tLYtUdNUNb2HtkHL';

		$encryptKey = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$ivKey = strtoupper('C+BWA9SEBDDCE^GY');
		//** Get text with padding
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$pad = $size - (strlen($text) % $size);
    	$text = $text . str_repeat(chr($pad), $pad);
    	//** Get encrypted text
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $text, MCRYPT_MODE_CBC, $ivKey ) );
		return $encrypted;
	}
}
