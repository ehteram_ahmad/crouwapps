<?php
/*
Desc: Data Push/Pull related to GMC user.
*/

//App::uses('AppController', 'Controller');

class MbgmcuserwebservicesController extends AppController {

	public $uses = array('GmcUkUser','User', 'NpiUsaUser', 'Mb.PreRoleBasedDomain', 'Mb.AppDeviceKey');
	public $components = array('Common','Mb.MbCommon');
	/*
	On: 12-05-2016
	I/P: $params = array()
	O/P: GMC users details
	Desc: Fetch all Gmc Users details according to conditions.
	*/
	public function gmcUsersSearch(){

		$resposneGmcUser = array();
		if($this->request->is('post')) {
			$randKeyStr = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
			$randKeyIv = strtoupper('C+BWA9SEBDDCE^GY');
			$header = getallheaders();
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$getDecreptedResponce = $this->dataDecryptionByPrivateKey($dataInput);
			$encryData = $getDecreptedResponce['values'];
			$paramsDataJson = $getDecreptedResponce['params'];
			$paramsDataArr = json_decode($paramsDataJson);
			$dataInput['key'] = $paramsDataArr->key;
			$dataInput['iv'] = $paramsDataArr->iv;
			$encryptedData = $this->dataDecryptionByAes( $encryData, $dataInput['key'], $dataInput['iv']);
			$dataInput = json_decode($encryptedData, true);
			$deviceId = $header['device_id'];
			$paramsEncryptedByRsa = $this->encryptionByRsa($deviceId);
			$params['first_name'] = isset($dataInput['first_name'])?$dataInput['first_name']:'';
			$params['last_name'] = isset($dataInput['last_name'])?$dataInput['last_name']:'';
			$params['size'] = isset($dataInput['size'])?$dataInput['size']:'';
			$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
			$params['country'] = isset($dataInput['country'])?$dataInput['country']:'';

			$skipGmc = 0;
			$domainNameArr = explode("@", $dataInput['email']);//** Extract Domain name from email
			$domainName = end($domainNameArr);	//** Domain Name
			$rootDomain = $this->MbCommon->getTld($domainName);
			//$allowedDomains = array('nhs.net', 'nhs.uk', 'nhs.co.uk','wsh.nhs.uk','wmas.nhs.uk');
			$allowedDomains = $this->PreRoleBasedDomain->find("count", array("conditions"=> array("domain_name"=> $rootDomain, "status"=> 1)));
			if(in_array($dataInput['profession_id'], array(9,10,11))){
			//** Values for profession Administration,Management [START] 
				if(in_array($dataInput['profession_id'], array(9,10,11)) && $allowedDomains > 0){
						$skipGmc = 1;
						$gmcUsers = array();
						$userDataArr = array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers);
						$userDataArr = json_encode($userDataArr);
						$encryptedData = $this->encryptDataNew( $userDataArr, $randKeyStr, $randKeyIv);
						$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
				}else{
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"0", 'response_code'=> "649", 'message'=> ERROR_649);
				}
			//** Values for profession Administration,Management [END] 
			}elseif(in_array($dataInput['profession_id'], array(2,3))){
			//** Values for profession Pharmacist,Nurse [START] 
				if(in_array($dataInput['profession_id'], array(2,3)) && $allowedDomains > 0){
						$skipGmc = 1;
						$gmcUsers = array();
						$userDataArr = array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers);
						$userDataArr = json_encode($userDataArr);
						$encryptedData = $this->encryptDataNew( $userDataArr, $randKeyStr, $randKeyIv);
						$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
				}else{
					$gmcUsers = array();
					$userDataArr = array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers);
					$userDataArr = json_encode($userDataArr);
					$encryptedData = $this->encryptDataNew( $userDataArr, $randKeyStr, $randKeyIv);
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
				}
			//** Values for profession Administration,Management [END] 
			}elseif(in_array($dataInput['profession_id'], array(8))){
					$data = array();
					if(in_array(strtolower($params['country']), array('united kingdom','uk','gb'))){ //** If user is from uk check uk GMC
						if(in_array($dataInput['profession_id'], array(8))){ //** If profession is Doctor
							$data = $this->GmcUkUser->gmcUserSearch( $params );
						}
					}
					elseif(in_array(strtolower($params['country']), array('United States','us','US'))){
						if(in_array($dataInput['profession_id'], array(8))){ //** If profession is Doctor
							$this->NpiUsaUser->useDbConfig = 'gmc';
							$data = $this->NpiUsaUser->npiUserSearch( $params );
							//echo "<pre>"; print_r($data);die;
						}
					}
					if(!empty($data)){
						foreach($data as $gmcData){
							$gmcUsers[] = array(
											'id'=> $gmcData['GmcUkUser']['id'], 
											'GMCRefNo'=> $gmcData['GmcUkUser']['GMCRefNo'],
											'Surname'=> $gmcData['GmcUkUser']['Surname'],
											'GivenName'=> $gmcData['GmcUkUser']['GivenName'],
											'Gender'=> $gmcData['GmcUkUser']['Gender'],
											'YearOfQualification'=> $gmcData['GmcUkUser']['YearOfQualification'],
											'PlaceofQualificationCountry'=> $gmcData['GmcUkUser']['PlaceofQualificationCountry'],
											);
							}
							$userDataArr = array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers);
							$userDataArr = json_encode($userDataArr);
							$encryptedData = $this->encryptDataNew( $userDataArr, $randKeyStr, $randKeyIv);
							$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
						}else{
							$gmcUsers = array();
							$userDataArr = array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers);
							$userDataArr = json_encode($userDataArr);
							$encryptedData = $this->encryptDataNew( $userDataArr, $randKeyStr, $randKeyIv);
							$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
						}
			}else{
					$skipGmc = 0;
					$gmcUsers = array();
					$userDataArr = array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers);
					$userDataArr = json_encode($userDataArr);
					$encryptedData = $this->encryptDataNew( $userDataArr, $randKeyStr, $randKeyIv);
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'params'=>$paramsEncryptedByRsa, 'data'=> $encryptedData);
			}	
			
		}else{
			$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"0", 'response_code'=> "601", 'message'=>'Information not provided by app');
		}
		//echo json_encode($resposneGmcUser);
		// $encryptedData = $this->Common->encryptData(json_encode($resposneGmcUser));
		// echo json_encode(array("values"=> $encryptedData));
		// exit;
		echo json_encode($resposneGmcUser);
		exit();
	}

	public function dataDecryptionByPrivateKey($dataInput)
	{
		$dataInputArr = $dataInput;
		// return $dataInputArr;
		if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		else
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		// return $pkGeneratePrivate;
		$paramsData = $dataInputArr['params'];
		$valueData = $dataInputArr['values'];
		openssl_private_decrypt(base64_decode($paramsData), $decrptedData, $pkGeneratePrivate);
		$newArr = array('params'=> $decrptedData, 'values'=> $valueData);
		return $newArr;
	}

	/*
	------------------------------------------------------------------------------
	ON: 24-04-18
	I/P: $values And $params in encrypted form
	O/P: JSON
	Desc: Get $deviceId And $publicKey And return server public key
	------------------------------------------------------------------------------
	*/

	public function dataDecryptionByAes( $encrypted = NULL, $encryptKey, $ivKey ){
		//** Decrypting Data
		// $encryptKey = 'd8dd1e44d4576fd72c39d587df33d81e';
		// $ivKey = 'd2b48574aa832a67';

		$encrypted = base64_decode($encrypted);
		$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $encrypted, MCRYPT_MODE_CBC, $ivKey );
	  	//** Remove Padding
	  	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
        return $decrypted;
	}

	/*
	------------------------------------------------------------------------------
	ON: 24-04-18
	I/P: $values And $params in encrypted form
	O/P: JSON
	Desc: Get $deviceId And $publicKey And return server public key
	------------------------------------------------------------------------------
	*/

	public function encryptionByRsa($deviceId)
	{
		$randKeyStr = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$randKeyIv = strtoupper('C+BWA9SEBDDCE^GY');
		$paramsResponceArr = array('key'=>$randKeyStr, 'iv'=>$randKeyIv);
		$paramsResponceJson = json_encode($paramsResponceArr);
		$getClientPublicKey = $this->AppDeviceKey->find('first', 
                array('conditions'=> 
                  array(
                    'device_id'=> $deviceId
                    )
                ));
		App::import('Vendor','Crypt_RSA',array('file' => 'phprsa/Crypt/RSA.php'));
		$rsaObj = new Crypt_RSA();
		$clientPublicKey = $getClientPublicKey['AppDeviceKey']['client_public_key'];
			$rsaObj->loadKey('-----BEGIN PUBLIC KEY-----
'.$clientPublicKey.'
-----END PUBLIC KEY-----');
			$rsaObj->setPublicKey();
			$publickey = $rsaObj->getPublicKey(CRYPT_RSA_PUBLIC_FORMAT_OPENSSH);
			$ciphertext = $rsaObj->encrypt($paramsResponceJson);
			return base64_encode($ciphertext);
	}

	public function encryptionByAes($values)
	{
		// $encryptKey = 'nS4Gpg9VND6FrOqS16VTWeRfMwc4jR07';
		// $ivKey = 'tLYtUdNUNb2HtkHL';

		$encryptKey = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$ivKey = strtoupper('C+BWA9SEBDDCE^GY');
		//** Get text with padding
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$pad = $size - (strlen($text) % $size);
    	$text = $text . str_repeat(chr($pad), $pad);
    	//** Get encrypted text
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $text, MCRYPT_MODE_CBC, $ivKey ) );
		return $encrypted;
	}

	public function decryptDataNew( $encrypted = NULL, $encryptKey, $ivKey ){
		//** Decrypting Data
		// $encryptKey = 'd8dd1e44d4576fd72c39d587df33d81e';
		// $ivKey = 'd2b48574aa832a67';

		$encrypted = base64_decode($encrypted);
		$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $encrypted, MCRYPT_MODE_CBC, $ivKey );
	  	//** Remove Padding
	  	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
        return $decrypted;
	}

	public function encryptDataNew( $text = NULL, $encryptKey, $ivKey){
		// $encryptKey = 'nS4Gpg9VND6FrOqS16VTWeRfMwc4jR07';
		// $ivKey = 'tLYtUdNUNb2HtkHL';

		$encryptKey = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$ivKey = strtoupper('C+BWA9SEBDDCE^GY');
		//** Get text with padding
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$pad = $size - (strlen($text) % $size);
    	$text = $text . str_repeat(chr($pad), $pad);
    	//** Get encrypted text
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $text, MCRYPT_MODE_CBC, $ivKey ) );
		return $encrypted;
	}
}
