<?php
/*
 * Feedback controller.
 *
 * This file will render views from views/Mbfeedbackwebservices/
 *
 
 */

App::uses('AppController', 'Controller');


class MbfeedbackwebservicesController extends AppController {
	public $uses = array('Mb.User','Mb.UserFeedback', 'Mb.AppDeviceKey');
	public $components = array('Common');
	
	/*
	-------------------------------------------------------------------------
	ON: 04-07-2016
	I/P: JSON (user_id, feedback content)
	O/P: JSON (success/fail)
	Desc: User can send feedback.
	-------------------------------------------------------------------------
	*/
	public function addUserFeedback(){
		$responseData = array();
		if($this->request->is('post')) {
			$randKeyStr = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
			$randKeyIv = strtoupper('C+BWA9SEBDDCE^GY');
			$header = getallheaders();
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$getDecreptedResponce = $this->dataDecryptionByPrivateKey($dataInput);
			$encryData = $getDecreptedResponce['values'];
			$paramsDataJson = $getDecreptedResponce['params'];
			$paramsDataArr = json_decode($paramsDataJson);
			$dataInput['key'] = $paramsDataArr->key;
			$dataInput['iv'] = $paramsDataArr->iv;
			$encryptedData = $this->dataDecryptionByAes( $encryData, $dataInput['key'], $dataInput['iv']);
			$dataInput = json_decode($encryptedData, true);
			$deviceId = $header['device_id'];
			$paramsEncryptedByRsa = $this->encryptionByRsa($deviceId);	
			if( $this->tokenValidation() && $this->accesskeyCheckValidation() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0){
					//** Add feedback
					try{
						$this->UserFeedback->recursive = -1;
						$userFeedbackData = array("user_id"=> $dataInput['user_id'], "subject"=> $dataInput['subject'], "feedback"=> $dataInput['feedback'], "feedback_source"=> "MB");
						$checkFeedback = $this->UserFeedback->find("count", array("conditions"=> $userFeedbackData));
						if( $checkFeedback == 0 ){
							if($this->UserFeedback->save( $userFeedbackData )){
								$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_605);
							}else{
								$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
							}
						}else{
								$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "637", 'message'=> ERROR_637);
						}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		// $encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode($responseData);
		exit;
	}

	public function dataDecryptionByPrivateKey($dataInput)
	{
		$dataInputArr = $dataInput;
		// return $dataInputArr;
		if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		else
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		// return $pkGeneratePrivate;
		$paramsData = $dataInputArr['params'];
		$valueData = $dataInputArr['values'];
		openssl_private_decrypt(base64_decode($paramsData), $decrptedData, $pkGeneratePrivate);
		$newArr = array('params'=> $decrptedData, 'values'=> $valueData);
		return $newArr;
	}

	/*
	------------------------------------------------------------------------------
	ON: 24-04-18
	I/P: $values And $params in encrypted form
	O/P: JSON
	Desc: Get $deviceId And $publicKey And return server public key
	------------------------------------------------------------------------------
	*/

	public function dataDecryptionByAes( $encrypted = NULL, $encryptKey, $ivKey ){
		//** Decrypting Data
		// $encryptKey = 'd8dd1e44d4576fd72c39d587df33d81e';
		// $ivKey = 'd2b48574aa832a67';

		$encrypted = base64_decode($encrypted);
		$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $encrypted, MCRYPT_MODE_CBC, $ivKey );
	  	//** Remove Padding
	  	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
        return $decrypted;
	}

	/*
	------------------------------------------------------------------------------
	ON: 24-04-18
	I/P: $values And $params in encrypted form
	O/P: JSON
	Desc: Get $deviceId And $publicKey And return server public key
	------------------------------------------------------------------------------
	*/

	public function encryptionByRsa($deviceId)
	{
		$randKeyStr = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$randKeyIv = strtoupper('C+BWA9SEBDDCE^GY');
		$paramsResponceArr = array('key'=>$randKeyStr, 'iv'=>$randKeyIv);
		$paramsResponceJson = json_encode($paramsResponceArr);
		$getClientPublicKey = $this->AppDeviceKey->find('first', 
                array('conditions'=> 
                  array(
                    'device_id'=> $deviceId
                    )
                ));
		App::import('Vendor','Crypt_RSA',array('file' => 'phprsa/Crypt/RSA.php'));
		$rsaObj = new Crypt_RSA();
		$clientPublicKey = $getClientPublicKey['AppDeviceKey']['client_public_key'];
			$rsaObj->loadKey('-----BEGIN PUBLIC KEY-----
'.$clientPublicKey.'
-----END PUBLIC KEY-----');
			$rsaObj->setPublicKey();
			$publickey = $rsaObj->getPublicKey(CRYPT_RSA_PUBLIC_FORMAT_OPENSSH);
			$ciphertext = $rsaObj->encrypt($paramsResponceJson);
			return base64_encode($ciphertext);
	}

	public function encryptionByAes($values)
	{
		// $encryptKey = 'nS4Gpg9VND6FrOqS16VTWeRfMwc4jR07';
		// $ivKey = 'tLYtUdNUNb2HtkHL';

		$encryptKey = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$ivKey = strtoupper('C+BWA9SEBDDCE^GY');
		//** Get text with padding
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$pad = $size - (strlen($text) % $size);
    	$text = $text . str_repeat(chr($pad), $pad);
    	//** Get encrypted text
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $text, MCRYPT_MODE_CBC, $ivKey ) );
		return $encrypted;
	}

	public function decryptDataNew( $encrypted = NULL, $encryptKey, $ivKey ){
		//** Decrypting Data
		// $encryptKey = 'd8dd1e44d4576fd72c39d587df33d81e';
		// $ivKey = 'd2b48574aa832a67';

		$encrypted = base64_decode($encrypted);
		$decrypted = mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $encrypted, MCRYPT_MODE_CBC, $ivKey );
	  	//** Remove Padding
	  	$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = ord($decrypted[($len = strlen($decrypted)) - 1]);
        $decrypted = substr($decrypted, 0, strlen($decrypted) - $pad);
        return $decrypted;
	}

	public function encryptDataNew( $text = NULL, $encryptKey, $ivKey){
		// $encryptKey = 'nS4Gpg9VND6FrOqS16VTWeRfMwc4jR07';
		// $ivKey = 'tLYtUdNUNb2HtkHL';

		$encryptKey = strtoupper('C+BWA9VFHHDCE^GYFUGDTY$TYDTYXCYH');
		$ivKey = strtoupper('C+BWA9SEBDDCE^GY');
		//** Get text with padding
		$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    	$pad = $size - (strlen($text) % $size);
    	$text = $text . str_repeat(chr($pad), $pad);
    	//** Get encrypted text
		$encrypted = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $encryptKey, $text, MCRYPT_MODE_CBC, $ivKey ) );
		return $encrypted;
	}
}
