<?php 
/*
Add, update, Validate user posts.
*/
App::uses('AppModel', 'Model');

class UserPost extends AppModel {
	
	public $hasMany = array(
        'UserPostAttribute' => array(
            'className' => 'UserPostAttribute',
            'conditions' => array('UserPostAttribute.status' => 1),
        ),
        'UserPostSpecilities' => array(
            'className' => 'UserPostSpecilities',
        ),
        'UserPostComment' => array(
            'className' => 'UserPostComment',
            
        ),
        
    );

    /*
    --------------------------------------------------------------
	On: 13-08-2015
	I/P: NA
	O/P: array()
	Desc: Output all post lists as an array.
	--------------------------------------------------------------
    */
    public function userPostLists( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $userId = NULL, $postId = NULL, $sortBy = 'latest' ,$orderBySort = NULL){ 	
		$userPosts = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		//** If userId set show all post(s) of the particular user
		$conditions = array('User.status'=> 1, 'UserPost.status'=> 1, 'UserPost.spam_confirmed'=> 0);
		if( !empty($userId) ){
			$conditions += array('UserPost.user_id'=> $userId);
		}
		//** If postid set fetch all posts > postid
		if( !empty($postId) ){
			if( isset($sortBy) && $sortBy == 'previous'){
				$conditions += array('UserPost.id < '=> $postId);
			}else{
				$conditions += array('UserPost.id > '=> $postId);
			}
		}
		//** order by conditions
		if(!empty($orderBySort) && $orderBySort = 'top_followed'){
			$orderBy = array('totFollowing DESC');
		}else{
			$orderBy = array('UserPost.post_date DESC');
		}
		
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'user_post_attributes',
						'alias' => 'upa',
						'type' => 'left',
						'conditions'=> array('UserPost.id = upa.user_post_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserPost.user_id = UserProfile.user_id')
					),
					array(
						'table' => 'user_post_specilities',
						'alias' => 'UserPostSpecilities',
						'type' => 'left',
						'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
					),
					array(
						'table' => 'user_follows',
						'alias' => 'UserFollow',
						'type' => 'left',
						'conditions'=> array('UserPost.user_id = UserFollow.followed_to')
					),
				),
			'fields'=> array('UserPost.id, UserPost.user_id,  UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.consent_id, UserPost.is_anonymous, upa.id, upa.attribute_type, upa.content, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, count(UserFollow.followed_to) AS totFollowing'),
			'conditions'=> $conditions,
			'group'=> array('UserPost.id'),
			'order'=> $orderBy,
			'limit'=> $pageSize,
			'offset'=> $offsetVal  
		);
		$userPosts = $this->find('all', $options);
		return $userPosts;		
	}

	/*
    --------------------------------------------------------------
	On: 25-09-2015
	I/P: NA
	O/P: array()
	Desc: Output post details lists as an array.
	--------------------------------------------------------------
    */
    public function postListById( $postId = NULL){ 
    	$userPosts = array();	
		$conditions = '';
		if( !empty($postId) ){
			$conditions = array('UserPost.id'=> $postId, 'User.status'=> 1, 'UserPost.spam_confirmed'=> 0);

			$options =array(             
				'joins' =>
					array(
						array(
							'table' => 'user_post_attributes',
							'alias' => 'upa',
							'type' => 'left',
							'conditions'=> array('UserPost.id = upa.user_post_id')
						),
						array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
						),
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfile',
							'type' => 'left',
							'foreignKey' => 'user_id',
							'conditions'=> array('UserPost.user_id = UserProfile.user_id')
						),
						array(
							'table' => 'user_post_specilities',
							'alias' => 'UserPostSpecilities',
							'type' => 'left',
							'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
						),
					),
				'group'=> array('UserPost.id'),
				'fields'=> array('UserPost.id, UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.is_anonymous, upa.id, UserPost.consent_id, upa.attribute_type, upa.content, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
				'conditions'=> $conditions,
				 
			);
			$userPosts = $this->find('all', $options);
		}
		return $userPosts;		
	}

	/*
	---------------------------------------------------------------------------------------------
	On: 09-10-2015
	I/P:
	O/P:
	Desc: Returns user posts based on interest id
	---------------------------------------------------------------------------------------------
	*/
	public function userPostListsByInterest( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $interestId = NULL, $postId = NULL, $sortBy = 'latest', $orderBySort = NULL ){
		$userPosts = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		//** If userId set show all post(s) of the particular user
		$conditions = '';
		if( !empty($interestId) ){
			$conditions = array('UserPostSpecilities.specilities_id'=> $interestId, 'User.status'=> 1, 'UserPost.status'=> 1,'UserPost.spam_confirmed'=> 0);
		}
		//** If postid set fetch all posts > postid
		if( !empty($postId) ){
			if( isset($sortBy) && $sortBy == 'previous'){
				$conditions = array('UserPost.id < '=> $postId, 'UserPostSpecilities.specilities_id'=> $interestId, 'User.status'=> 1, 'UserPost.spam_confirmed'=> 0, 'UserPost.status'=> 1);
			}else{
				$conditions = array('UserPost.id > '=> $postId, 'UserPostSpecilities.specilities_id'=> $interestId, 'User.status'=> 1, 'UserPost.spam_confirmed'=> 0, 'UserPost.status'=> 1);
			}
		}
		//** order by conditions
		$orderBy = array('UserPost.post_date DESC');
		
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'user_post_attributes',
						'alias' => 'upa',
						'type' => 'left',
						'conditions'=> array('UserPost.id = upa.user_post_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
						),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserPost.user_id = UserProfile.user_id')
					),
					array(
						'table' => 'user_post_specilities',
						'alias' => 'UserPostSpecilities',
						'type' => 'left',
						'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
					),
					array(
						'table' => 'user_follows',
						'alias' => 'UserFollow',
						'type' => 'left',
						'conditions'=> array('UserPost.user_id = UserFollow.followed_to')
					),
				),
			'fields'=> array('UserPost.id, UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.consent_id, upa.id, upa.attribute_type, upa.content, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, count(UserFollow.followed_to) AS totFollowing'),
			'conditions'=> $conditions,
			'group'=> array('UserPost.id'),
			'order'=> $orderBy,
			'limit'=> $pageSize,
			'offset'=> $offsetVal  
		);
		$userPosts = $this->find('all', $options);
		return $userPosts;	
	}

	/*
    --------------------------------------------------------------
	On: 30-11-2015
	I/P: JSON ()
	O/P: array()
	Desc: Output all post lists as an array according to matching text.
	--------------------------------------------------------------
    */
    public function postSearch( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $searchText = '', $postId = NULL ){ 	
		$userPosts = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		//** All posts according to search criteria
		$conditions = " User.status = 1 AND User.approved = 1 AND UserPost.status = 1 AND UserPost.spam_confirmed = 0 AND upa.attribute_type = 'text' ";
			if(!empty($searchText)){
				if(strlen($conditions) >0){
					$conditions .= ' AND ( LOWER(UserPost.title) LIKE LOWER("%'.$searchText.'%") OR LOWER(upa.content) LIKE LOWER("%'.$searchText.'%") OR LOWER(Specilitiy.name) LIKE LOWER("%'.$searchText.'%") ) ';
				}else{
					$conditions .= ' ( LOWER(UserPost.title) LIKE LOWER("%'.$searchText.'%") OR LOWER(upa.content) LIKE LOWER("%'.$searchText.'%") OR LOWER(Specilitiy.name) LIKE LOWER("%'.$searchText.'%") ) ';
				}
			}
		//** order by conditions
		if(!empty($orderBySort) && $orderBySort = 'top_followed'){
			$orderBy = array('totFollowing DESC');
		}else{
			$orderBy = array('UserPost.post_date DESC');
		}
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'user_post_attributes',
						'alias' => 'upa',
						'type' => 'left',
						'conditions'=> array('UserPost.id = upa.user_post_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserPost.user_id = UserProfile.user_id')
					),
					array(
						'table' => 'user_post_specilities',
						'alias' => 'UserPostSpecilities',
						'type' => 'left',
						'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
					),
					array(
						'table' => 'specilities',
						'alias' => 'Specilitiy',
						'type' => 'left',
						'conditions'=> array('Specilitiy.id = UserPostSpecilities.specilities_id')
					),
					array(
						'table' => 'user_follows',
						'alias' => 'UserFollow',
						'type' => 'left',
						'conditions'=> array('UserPost.user_id = UserFollow.followed_to')
					),
				),
			'fields'=> array('UserPost.id, UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.consent_id, UserPost.is_anonymous, upa.id, upa.attribute_type, upa.content, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, count(UserFollow.followed_to) AS totFollowing, Specilitiy.name'),
			'conditions'=> $conditions,
			'group'=> array('UserPost.id'),
			'order'=> $orderBy,
			'limit'=> $pageSize,
			'offset'=> $offsetVal  
		);
		$userPosts = $this->find('all', $options);
		return $userPosts;		
	}

	/*
    --------------------------------------------------------------
	On: 22-12-2015
	I/P: 
	O/P: array()
	Desc: Output all post lists by followed users of any user.
	--------------------------------------------------------------
    */
    public function followedPostLists( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $userId = NULL, $postId = NULL, $sortBy = 'latest' ,$interestId = NULL){ 	
		$userPosts = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		//** If userId set show all post(s) of the particular user
		$conditions = array('User.status'=> 1, 'User.approved' => 1, 'UserPost.status'=> 1, 'UserPost.spam_confirmed'=> 0);
		if( !empty($interestId) ){
			$conditions += array('UserPostSpecilities.specilities_id'=> $interestId);
		}
		if( !empty($userId) ){
			$conditions += array('UserFollow.followed_by'=> $userId, 'UserFollow.follow_type'=> 1);
		}
		
		//** order by conditions
		$orderBy = array('UserPost.post_date DESC');
		
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'user_post_attributes',
						'alias' => 'upa',
						'type' => 'left',
						'conditions'=> array('UserPost.id = upa.user_post_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserPost.user_id = UserProfile.user_id')
					),
					array(
						'table' => 'user_post_specilities',
						'alias' => 'UserPostSpecilities',
						'type' => 'left',
						'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
					),
					array(
						'table' => 'user_follows',
						'alias' => 'UserFollow',
						'type' => 'left',
						'conditions'=> array('UserPost.user_id = UserFollow.followed_to')
					),
				),
			'fields'=> array('UserPost.id, UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.consent_id, UserPost.is_anonymous, upa.id, upa.attribute_type, upa.content, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
			'conditions'=> $conditions,
			'group'=> array('UserPost.id'),
			'order'=> $orderBy,
			'limit'=> $pageSize,
			'offset'=> $offsetVal  
		);
		$userPosts = $this->find('all', $options);
		return $userPosts;		
	}

	/*
    --------------------------------------------------------------
	On: 26-04-2016
	I/P: 
	O/P: 
	Desc: all post list total record count
	--------------------------------------------------------------
    */
    public function userPostListsCount( $userId = NULL, $postId = NULL, $sortBy = 'latest' ,$orderBySort = NULL){ 	
		$userPostsCount = 0;
		//** If userId set show all post(s) of the particular user
		$conditions = array('User.status'=> 1, 'UserPost.status'=> 1, 'UserPost.spam_confirmed'=> 0);
		if( !empty($userId) ){
			$conditions += array('UserPost.user_id'=> $userId);
		}
		//** If postid set fetch all posts > postid
		if( !empty($postId) ){
			if( isset($sortBy) && $sortBy == 'previous'){
				$conditions += array('UserPost.id < '=> $postId);
			}else{
				$conditions += array('UserPost.id > '=> $postId);
			}
		}
		//** order by conditions
		if(!empty($orderBySort) && $orderBySort = 'top_followed'){
			$orderBy = array('totFollowing DESC');
		}else{
			$orderBy = array('UserPost.post_date DESC');
		}
		
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'user_post_attributes',
						'alias' => 'upa',
						'type' => 'left',
						'conditions'=> array('UserPost.id = upa.user_post_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserPost.user_id = UserProfile.user_id')
					),
					array(
						'table' => 'user_post_specilities',
						'alias' => 'UserPostSpecilities',
						'type' => 'left',
						'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
					),
					array(
						'table' => 'user_follows',
						'alias' => 'UserFollow',
						'type' => 'left',
						'conditions'=> array('UserPost.user_id = UserFollow.followed_to')
					),
				),
			//'fields'=> array('UserPost.id, UserPost.user_id,  UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.consent_id, UserPost.is_anonymous, upa.id, upa.attribute_type, upa.content, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, count(UserFollow.followed_to) AS totFollowing'),
			'conditions'=> $conditions,
			'group'=> array('UserPost.id')
		);
		$userPostsCount = $this->find('count', $options);
		return $userPostsCount;		
	}

	/*
    --------------------------------------------------------------
	On: 26-04-2016
	I/P: 
	O/P: 
	Desc: Post List By id count
	--------------------------------------------------------------
    */
    public function postListByIdCount( $postId = NULL){ 
    	$userPostsCount = array();	
		$conditions = '';
		if( !empty($postId) ){
			$conditions = array('UserPost.id'=> $postId, 'User.status'=> 1, 'UserPost.spam_confirmed'=> 0);

			$options =array(             
				'joins' =>
					array(
						array(
							'table' => 'user_post_attributes',
							'alias' => 'upa',
							'type' => 'left',
							'conditions'=> array('UserPost.id = upa.user_post_id')
						),
						array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
						),
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfile',
							'type' => 'left',
							'foreignKey' => 'user_id',
							'conditions'=> array('UserPost.user_id = UserProfile.user_id')
						),
						array(
							'table' => 'user_post_specilities',
							'alias' => 'UserPostSpecilities',
							'type' => 'left',
							'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
						),
					),
				'group'=> array('UserPost.id'),
				//'fields'=> array('UserPost.id, UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.is_anonymous, upa.id, UserPost.consent_id, upa.attribute_type, upa.content, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
				'conditions'=> $conditions,
				'group'=> array('UserPost.id') 
			);
			$userPostsCount = $this->find('count', $options);
		}
		return $userPostsCount;		
	}

	/*
	---------------------------------------------------------------------------------------------
	On: 26-04-2016
	I/P:
	O/P:
	Desc: Post List by interest count
	---------------------------------------------------------------------------------------------
	*/
	public function userPostListsByInterestCount(  $interestId = NULL, $postId = NULL, $sortBy = 'latest', $orderBySort = NULL ){
		$userPostsCount = array();
		//** If userId set show all post(s) of the particular user
		$conditions = '';
		if( !empty($interestId) ){
			$conditions = array('UserPostSpecilities.specilities_id'=> $interestId, 'User.status'=> 1, 'UserPost.status'=> 1,'UserPost.spam_confirmed'=> 0);
		}
		//** If postid set fetch all posts > postid
		if( !empty($postId) ){
			if( isset($sortBy) && $sortBy == 'previous'){
				$conditions = array('UserPost.id < '=> $postId, 'UserPostSpecilities.specilities_id'=> $interestId, 'User.status'=> 1, 'UserPost.spam_confirmed'=> 0, 'UserPost.status'=> 1);
			}else{
				$conditions = array('UserPost.id > '=> $postId, 'UserPostSpecilities.specilities_id'=> $interestId, 'User.status'=> 1, 'UserPost.spam_confirmed'=> 0, 'UserPost.status'=> 1);
			}
		}
		//** order by conditions
		$orderBy = array('UserPost.post_date DESC');
		
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'user_post_attributes',
						'alias' => 'upa',
						'type' => 'left',
						'conditions'=> array('UserPost.id = upa.user_post_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
						),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserPost.user_id = UserProfile.user_id')
					),
					array(
						'table' => 'user_post_specilities',
						'alias' => 'UserPostSpecilities',
						'type' => 'left',
						'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
					),
					array(
						'table' => 'user_follows',
						'alias' => 'UserFollow',
						'type' => 'left',
						'conditions'=> array('UserPost.user_id = UserFollow.followed_to')
					),
				),
			//'fields'=> array('UserPost.id, UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.consent_id, upa.id, upa.attribute_type, upa.content, User.status, User.role_id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, count(UserFollow.followed_to) AS totFollowing'),
			'conditions'=> $conditions,
			'group'=> array('UserPost.id')
		);
		$userPostsCount = $this->find('count', $options);
		return $userPostsCount;	
	}

	/*
    --------------------------------------------------------------
	On: 26-04-2016
	I/P: 
	O/P: 
	Desc: Post search count
	--------------------------------------------------------------
    */
    public function postSearchCount( $searchText = '', $postId = NULL ){ 	
		$userPostsCount = array();
		//** All posts according to search criteria
		$conditions = " User.status = 1 AND User.approved = 1 AND UserPost.status = 1 AND UserPost.spam_confirmed = 0 AND upa.attribute_type = 'text' ";
			if(!empty($searchText)){
				if(strlen($conditions) >0){
					$conditions .= ' AND ( LOWER(UserPost.title) LIKE LOWER("%'.$searchText.'%") OR LOWER(upa.content) LIKE LOWER("%'.$searchText.'%") OR LOWER(Specilitiy.name) LIKE LOWER("%'.$searchText.'%") ) ';
				}else{
					$conditions .= ' ( LOWER(UserPost.title) LIKE LOWER("%'.$searchText.'%") OR LOWER(upa.content) LIKE LOWER("%'.$searchText.'%") OR LOWER(Specilitiy.name) LIKE LOWER("%'.$searchText.'%") ) ';
				}
			}
		//** order by conditions
		if(!empty($orderBySort) && $orderBySort = 'top_followed'){
			$orderBy = array('totFollowing DESC');
		}else{
			$orderBy = array('UserPost.post_date DESC');
		}
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'user_post_attributes',
						'alias' => 'upa',
						'type' => 'left',
						'conditions'=> array('UserPost.id = upa.user_post_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserPost.user_id = UserProfile.user_id')
					),
					array(
						'table' => 'user_post_specilities',
						'alias' => 'UserPostSpecilities',
						'type' => 'left',
						'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
					),
					array(
						'table' => 'specilities',
						'alias' => 'Specilitiy',
						'type' => 'left',
						'conditions'=> array('Specilitiy.id = UserPostSpecilities.specilities_id')
					),
					array(
						'table' => 'user_follows',
						'alias' => 'UserFollow',
						'type' => 'left',
						'conditions'=> array('UserPost.user_id = UserFollow.followed_to')
					),
				),
			//'fields'=> array('UserPost.id, UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.consent_id, UserPost.is_anonymous, upa.id, upa.attribute_type, upa.content, User.status, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, count(UserFollow.followed_to) AS totFollowing, Specilitiy.name'),
			'conditions'=> $conditions,
			'group'=> array('UserPost.id') 
		);
		$userPostsCount = $this->find('count', $options);
		return $userPostsCount;		
	}

	/*
    --------------------------------------------------------------
	On: 26-04-2016
	I/P: 
	O/P: 
	Desc: Followed Beat list count
	--------------------------------------------------------------
    */
    public function followedPostListsCount(  $userId = NULL, $postId = NULL, $sortBy = 'latest' ,$interestId = NULL){ 	
		$userPostsCount = array();
		//** If userId set show all post(s) of the particular user
		$conditions = array('User.status'=> 1, 'User.approved' => 1, 'UserPost.status'=> 1, 'UserPost.spam_confirmed'=> 0);
		if( !empty($interestId) ){
			$conditions += array('UserPostSpecilities.specilities_id'=> $interestId);
		}
		if( !empty($userId) ){
			$conditions += array('UserFollow.followed_by'=> $userId, 'UserFollow.follow_type'=> 1);
		}
		
		//** order by conditions
		$orderBy = array('UserPost.post_date DESC');
		
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'user_post_attributes',
						'alias' => 'upa',
						'type' => 'left',
						'conditions'=> array('UserPost.id = upa.user_post_id')
					),
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserPost.user_id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserPost.user_id = UserProfile.user_id')
					),
					array(
						'table' => 'user_post_specilities',
						'alias' => 'UserPostSpecilities',
						'type' => 'left',
						'conditions'=> array('UserPost.id = UserPostSpecilities.user_post_id')
					),
					array(
						'table' => 'user_follows',
						'alias' => 'UserFollow',
						'type' => 'left',
						'conditions'=> array('UserPost.user_id = UserFollow.followed_to')
					),
				),
			//'fields'=> array('UserPost.id, UserPost.title, UserPost.post_date, UserPost.parent_post_id, UserPost.original_post_id, UserPost.consent_id, UserPost.is_anonymous, upa.id, upa.attribute_type, upa.content, User.status, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
			'conditions'=> $conditions,
			'group'=> array('UserPost.id') 
		);
		$userPostsCount = $this->find('count', $options);
		return $userPostsCount;		
	}
	

}
?>