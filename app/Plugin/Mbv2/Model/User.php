<?php 
/*
User model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');

class User extends AppModel {
	var $hasOne = array(
        'UserProfile' => array(
            'className' => 'UserProfile',
            'foreignKey' => 'user_id',
        )
    );

    var $hasMany = array(
        'UserSpecilities' => array(
            'className' => 'UserSpecilities',
            'foreignKey' => 'user_id',
            'conditions' => array('UserSpecilities.status' => 1)
        )
    );

    /*public $validate = array(
		'email' => array( 
              'rule' => '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', 
              'message' => 'Please provide a valid email address.' 
         ),
		
        
        
    );*/

	/*
	On: 23-07-2015
	I/P: $modelName = string, $fieldName = string, $fieldVals = array()
	O/P: true/false
	Desc: 
	*/
	public function checkValueExists( $fieldName = '', $fieldVal = ''){
		
		$countdata = $this->find('count', array('conditions' => array( $fieldName => $fieldVal )));
		if($countdata > 0){
			return true;
		}else{
			return false;
		}
	}

	/*
	On: 04-07-2015
	I/P: $activationKey
	O/P: $data = array()
	Desc: 
	*/
	public function activationKeyStatus( $activationKey = NULL ){
		$data = array(); 
		if( !empty($activationKey) ){
			$data = $this->find('first', array('fields'=> array('User.status, User.approved'), 'conditions'=> array('activation_key'=> $activationKey)));
		}
		return $data;
	}

	/*
	On: 04-08-2015
	I/P: $activationKey
	O/P: 0 OR 1 
	Desc:
	*/
	function activateKey( $activationKey = NULL ){
		$return = 0;
		if( !empty($activationKey) ){
			//$this->updateAll( array('status'=> 1, 'approved'=> 1), array('activation_key'=> $activationKey) );
			$this->updateAll( array('status'=> 1), array('activation_key'=> $activationKey) );
			if( $this->getAffectedRows() ){
				$return = 1;
			}
		}
		return $return;
	}

	/*
	On: 04-08-2015
	I/P: $params = array()
	O/P: User Details
	Desc: 
	*/
	public function userDetails( $params = array() ){
		$userData = array();
		if( !empty($params) ){
			$userData = $this->find('first', array('conditions'=> $params));
		}
		return $userData;
	}		
	
	/*
	On: 05-08-2015
	I/P: 
	O/P:
	Desc: 
	*/
	public function updateFields( $fields = array(), $conditions = array() ){
		$return = 0;
		if( !empty($fields) && !empty($conditions) ){
			$this->updateAll( $fields, $conditions );

			if( $this->getAffectedRows() ){
				$return = 1;
			}
		}
		return $return;
	}

	/*
	On: 03-11-2015
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches user lists with pagination
	*/
	public function userSearch( $params = array() , $loggedinUser = NULL ){
		$userLists = array();
		if( !empty($params) ){ 
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			if(!empty($loggedinUser)){
				$conditions = " User.status = 1 AND User.approved = 1 AND User.id !=" . $loggedinUser . " AND UserProfile.country_id= ". $params['loggedin_user_countryid'];
			}else{
				$conditions = " User.status = 1 AND User.approved = 1 AND UserProfile.country_id= ". $params['loggedin_user_countryid'];
			}
			if(!empty($params['first_name'])){
				if(strlen($conditions) >0){
					//$conditions .= ' AND ( LOWER(UserProfile.first_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.first_name) LIKE LOWER("% '.$params['first_name'].'%") OR LOWER(UserProfile.last_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(UserProfile.role_status) LIKE LOWER("%'.$params['first_name'].'%") OR  LOWER(Profession.profession_type) LIKE LOWER("'.$params['first_name'].'%") OR ( LOWER(Specility.name) LIKE LOWER("'.$params['first_name'].'%")   AND UserSpecility.status =1 ) ) ';
					$conditions .= ' AND ( LOWER(UserProfile.first_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.last_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(UserProfile.role_status) LIKE LOWER("%'.$params['first_name'].'%") OR  LOWER(Profession.profession_type) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(User.email) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(Country.country_name) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(CompanyName.company_name) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(InstituteName.institute_name) LIKE LOWER("'.$params['first_name'].'%") ) ';
				}else{
					//$conditions .= ' ( LOWER(UserProfile.first_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.first_name) LIKE LOWER("% '.$params['first_name'].'%") OR LOWER(UserProfile.last_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.role_status) LIKE LOWER("%'.$params['first_name'].'%") OR  LOWER(Profession.profession_type) LIKE LOWER("'.$params['first_name'].'%") OR ( LOWER(Specilitiy.name) LIKE LOWER("'.$params['first_name'].'%")   AND UserSpecility.status =1 ) ) ';
					$conditions .= ' ( LOWER(UserProfile.first_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.last_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.role_status) LIKE LOWER("%'.$params['first_name'].'%") OR  LOWER(Profession.profession_type) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(User.email) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(Country.country_name) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(CompanyName.company_name) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(InstituteName.institute_name) LIKE LOWER("'.$params['first_name'].'%") ) ';
				}
			}
			if(!empty($params['last_name'])){
				if(strlen($conditions) >0){
						$conditions .= ' AND LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['last_name'].'%") ';
					}else{
						$conditions .= ' LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['last_name'].'%") ';
					}
			}
			if(!empty($params['email'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND LOWER(User.email) LIKE LOWER("%'.$params['email'].'%") ';
				}else{
					$conditions .= ' LOWER(User.email) LIKE LOWER("%'.$params['email'].'%") ';
				}
			}
			$userLists = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'countries',
										'alias' => 'Country',
										'type' => 'left',
										'conditions'=> array('UserProfile.country_id = Country.id')
									),
									array(
										'table'=> 'professions',
										'alias' => 'Profession',
										'type' => 'left',
										'conditions'=> array('UserProfile.profession_id = Profession.id')
									),
									array(
										'table'=> 'user_specilities',
										'alias' => 'UserSpecility',
										'type' => 'left',
										'conditions'=> array('User.id = UserSpecility.user_id')
									),
									array(
										'table'=> 'specilities',
										'alias' => 'Specility',
										'type' => 'left',
										'conditions'=> array('Specility.id = UserSpecility.specilities_id')
									),
									array(
										'table'=> 'user_institutions',
										'alias' => 'UserInstitution',
										'type' => 'left',
										'conditions'=> array('User.id = UserInstitution.user_id')
									),
									array(
										'table'=> 'institute_names',
										'alias' => 'InstituteName',
										'type' => 'left',
										'conditions'=> array('InstituteName.id = UserInstitution.institution_id')
									),
									array(
										'table'=> 'user_employments',
										'alias' => 'UserEmployment',
										'type' => 'left',
										'conditions'=> array('User.id = UserEmployment.user_id')
									),
									array(
										'table'=> 'company_names',
										'alias' => 'CompanyName',
										'type' => 'left',
										'conditions'=> array('CompanyName.id = UserEmployment.company_id')
									),
								),
								'conditions' => $conditions,
								'limit'=> $pageSize,
								'offset'=> $offsetVal,
								'group'=> array('User.id'), 
								'order'=> 'UserProfile.first_name'
							)
						);
		}
		return $userLists;
	}

	/*
	On: 25-04-2016
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches user details by user id
	*/
	public function userDetailsByUserId( $userId = NULL , $loggedinUser = NULL ){ 
		$userLists = array();
		if( !empty($userId) ){ 
			if(!empty($loggedinUser)){
				$conditions = " User.status = 1 AND User.approved = 1 AND User.id !=" . $loggedinUser . " AND User.id = $userId  ";
			}else{
				$conditions = " User.status = 1 AND User.approved = 1 AND User.id = $userId "; 
			}
			$userLists = $this->find('first', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'countries',
										'alias' => 'Country',
										'type' => 'left',
										'conditions'=> array('UserProfile.country_id = Country.id')
									),
									array(
										'table'=> 'professions',
										'alias' => 'Profession',
										'type' => 'left',
										'conditions'=> array('UserProfile.profession_id = Profession.id')
									),
									array(
										'table'=> 'user_specilities',
										'alias' => 'UserSpecility',
										'type' => 'left',
										'conditions'=> array('User.id = UserSpecility.user_id')
									),
									array(
										'table'=> 'specilities',
										'alias' => 'Specility',
										'type' => 'left',
										'conditions'=> array('Specility.id = UserSpecility.specilities_id')
									),
								),
								'conditions' => $conditions,
								//'group'=> array('User.id'),
								//'order'=> 'UserProfile.first_name'
							)
						);
		}
		return $userLists;
	}

	/*
	-----------------------------------------------------------
	On: 16-02-2017
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches all user of same hospital with ON/OFF duty
	-----------------------------------------------------------
	*/
	public function oncallUserLists($params = array(), $loggedinUser = NULL){
		$userLists = array();
		if( !empty($params) ){ 
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;

			$loggedinUserCompanyId = $params['user_current_company_id'];
			$loggedinUserCountryId = $params['loggedin_user_countryid'];
			if(!empty($loggedinUserCompanyId)){
			$quer = "SELECT `User`.`id`, `User`.`email`, `User`.`password`, `User`.`activation_key`,
			 `User`.`last_loggedin_date`, `User`.`login_device`, `User`.`registration_date`,
			 `User`.`status`, `User`.`approved`, `User`.`role_id`, `User`.`registration_source`,
			 `UserProfile`.`id`, `UserProfile`.`user_id`, `UserProfile`.`first_name`, 
			 `UserProfile`.`last_name`, `UserProfile`.`institution_name`, `UserProfile`.`country_id`,
			 `UserProfile`.`county`, `UserProfile`.`city`, `UserProfile`.`registration_image`, 
			 `UserProfile`.`profession_id`, `UserProfile`.`gmcnumber`, `UserProfile`.`contact_no`, 
			 `UserProfile`.`contact_no_is_verified`, `UserProfile`.`address`, `UserProfile`.`dob`, 
			 `UserProfile`.`profile_img`, `UserProfile`.`year_of_graduation`, `UserProfile`.`gender`,
			 `UserProfile`.`alias_name`, `UserProfile`.`current_employment`, `UserProfile`.`role_status`,
			 `UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`   
			  FROM user_duty_logs UserDutyLog INNER JOIN `users` AS `User` ON `User`.`id` = `UserDutyLog`.`user_id`  
			  LEFT JOIN `user_profiles` AS `UserProfile` ON (`UserProfile`.`user_id` = `User`.`id`) 
			  left JOIN `countries` AS `Country` ON (`UserProfile`.`country_id` = `Country`.`id`) 
			  left JOIN `professions` AS `Profession` ON (`UserProfile`.`profession_id` = `Profession`.`id`)
			  left JOIN `user_specilities` AS `UserSpecility` ON (`User`.`id` = `UserSpecility`.`user_id`)
			  left JOIN `specilities` AS `Specility` ON (`Specility`.`id` = `UserSpecility`.`specilities_id`)
			  left JOIN `user_institutions` AS `UserInstitution` ON (`User`.`id` = `UserInstitution`.`user_id`) 
			  left JOIN `institute_names` AS `InstituteName` ON (`InstituteName`.`id` = `UserInstitution`.`institution_id`) 
			  left JOIN `user_employments` AS `UserEmployment` ON (`User`.`id` = `UserEmployment`.`user_id`) 
			  left JOIN `company_names` AS `CompanyName` ON (`CompanyName`.`id` = `UserEmployment`.`company_id`)   
			  WHERE  `User`.`status` = 1 AND `User`.`approved` = 1  AND `User`.`id` !=  $loggedinUser AND `UserProfile`.`country_id` = $loggedinUserCountryId   
			  AND `UserDutyLog`.`hospital_id`= $loggedinUserCompanyId  GROUP BY `User`.`id`  ORDER BY `UserDutyLog`.`status` DESC, `UserDutyLog`.`atwork_status` DESC, `UserProfile`.`first_name` ASC  
			   LIMIT $offsetVal, $pageSize";
			  $userLists = $this->query($quer);
			}
		}
		return $userLists;
	}

	/*
	On: 22-04-2017
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches user either active/inactive, approve/unapprove etc.
	*/
	public function userDetailsById( $userId = NULL , $loggedinUser = NULL ){ 
		$userLists = array();
		if( !empty($userId) ){ 
			if(!empty($loggedinUser)){
				$conditions = "  User.id !=" . $loggedinUser . " AND User.id = $userId  ";
			}else{
				$conditions = "  User.id = $userId "; 
			}
			$userLists = $this->find('first', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'countries',
										'alias' => 'Country',
										'type' => 'left',
										'conditions'=> array('UserProfile.country_id = Country.id')
									),
									array(
										'table'=> 'professions',
										'alias' => 'Profession',
										'type' => 'left',
										'conditions'=> array('UserProfile.profession_id = Profession.id')
									),
									array(
										'table'=> 'user_specilities',
										'alias' => 'UserSpecility',
										'type' => 'left',
										'conditions'=> array('User.id = UserSpecility.user_id')
									),
									array(
										'table'=> 'specilities',
										'alias' => 'Specility',
										'type' => 'left',
										'conditions'=> array('Specility.id = UserSpecility.specilities_id')
									),
								),
								'conditions' => $conditions,
								//'group'=> array('User.id'),
								//'order'=> 'UserProfile.first_name'
							)
						);
		}
		return $userLists;
	}

	/*
	-----------------------------------------------------------
	On: 13-07-2017
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches all user of same hospital with ON/OFF duty (for lesser data)
	-----------------------------------------------------------
	*/
	public function oncallUserListsLight($params = array(), $loggedinUser = NULL){
		$userLists = array();
		if( !empty($params) ){ 
			//$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			//$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			//$offsetVal = ( $pageNum - 1 ) * $pageSize;

			$loggedinUserCompanyId = $params['user_current_company_id'];
			$loggedinUserCountryId = $params['loggedin_user_countryid'];
			if(!empty($loggedinUserCompanyId)){
			$quer = "SELECT `User`.`id`, `User`.`email`, `UserProfile`.`first_name`, `UserProfile`.`last_name`, 
			 `UserProfile`.`profession_id`, `UserProfile`.`profile_img`, `UserProfile`.`role_status`,
			 `UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`   
			  FROM user_duty_logs UserDutyLog INNER JOIN `users` AS `User` ON `User`.`id` = `UserDutyLog`.`user_id`  
			  LEFT JOIN `user_profiles` AS `UserProfile` ON (`UserProfile`.`user_id` = `User`.`id`) 
			  WHERE  `User`.`status` = 1 AND `User`.`approved` = 1  AND `User`.`id` !=  $loggedinUser AND `UserProfile`.`country_id` = $loggedinUserCountryId   
			  AND `UserDutyLog`.`hospital_id`= $loggedinUserCompanyId  GROUP BY `User`.`id`  ORDER BY `UserDutyLog`.`status` DESC, `UserDutyLog`.`atwork_status` DESC, `UserProfile`.`first_name` ASC";  
			   
			  $userLists = $this->query($quer);
			}
		}
		return $userLists;
	}

	/*
	-----------------------------------------------------------
	On: 13-07-2017
	I/P: $userId
	O/P: array() users lists with email and name
	Desc: fetch user list
	-----------------------------------------------------------
	*/

	public function getUserDetail($userId)
	{
		$quer = "SELECT User.email,CONCAT(UserProfile.first_name, ' ', UserProfile.last_name) AS usrName 
					FROM `users` AS User
					LEFT JOIN `user_profiles` AS UserProfile
					ON User.id = UserProfile.user_id
					WHERE User.id = $userId LIMIT 1"; 
		$userLists = $this->query($quer);
		return $userLists;
	}

	/*
	-----------------------------------------------------------
	On: 26-10-2017
	I/P: $userId
	O/P: 
	Desc: fetch user profile with role tag
	-----------------------------------------------------------
	*/
	public function getUserProfileWithRoleTag($userId)
	{
		$userProfileData = $this->query("SELECT User.email,UserProfile.user_id,UserProfile.first_name,UserProfile.last_name,UserProfile.profile_img,Country.country_code,Profession.id,Profession.profession_type,UserEmployment.id, UserEmployment.company_id,UserEmployment.is_current,UserQbDetail.qb_id,CompanyName.company_name,UserProfile.contact_no,UserProfile.contact_no_is_verified,UserProfile.role_status,UserDutyLog.atwork_status,UserDutyLog.status,UserRoleTag.role_tag_id,RoleTag.`key`,RoleTag.`value`
										FROM users AS User
										LEFT JOIN user_profiles AS UserProfile
										ON User.id = UserProfile.user_id
										LEFT JOIN user_employments AS UserEmployment
										ON User.id = UserEmployment.user_id
										LEFT JOIN company_names AS CompanyName
										ON UserEmployment.company_id = CompanyName.id
										LEFT JOIN countries AS Country
										ON UserProfile.country_id = Country.id
										LEFT JOIN professions AS Profession
										ON UserProfile.profession_id = Profession.id
										LEFT JOIN user_duty_logs AS UserDutyLog
										ON User.id = UserDutyLog.user_id
										LEFT JOIN user_role_tags AS UserRoleTag
										ON User.id = UserRoleTag.user_id
										LEFT JOIN user_qb_details AS UserQbDetail
										ON User.id = UserQbDetail.user_id
										LEFT JOIN role_tags AS RoleTag
										ON UserRoleTag.role_tag_id = RoleTag.id
										WHERE User.id = $userId AND UserEmployment.is_current = 1");
		return $userProfileData;
	}


	/*
	-----------------------------------------------------------
	On: 13-11-2017
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches all user of same hospital with ON/OFF duty (for lesser data)
	-----------------------------------------------------------
	*/
	public function getUserDirectoryData($params = array(), $loggedinUser = NULL){
		$userLists = array();
		if( !empty($params) ){ 
			//$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			//$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			//$offsetVal = ( $pageNum - 1 ) * $pageSize;

			$loggedinUserCompanyId = $params['user_current_company_id'];
			$loggedinUserCountryId = $params['loggedin_user_countryid'];
			if(!empty($loggedinUserCompanyId)){

			//************************Here Code with country check**************************//


			// $quer = "SELECT `User`.`id`, `User`.`email`, `UserProfile`.`first_name`, `UserProfile`.`last_name`, 
			//  `UserProfile`.`profession_id`, `UserProfile`.`profile_img`, `UserProfile`.`role_status`, `UserProfile`.`contact_no`, `UserProfile`.`contact_no_is_verified`, 
			//  `UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`, `Country`.`country_code`    
			//   FROM user_duty_logs UserDutyLog INNER JOIN `users` AS `User` ON `User`.`id` = `UserDutyLog`.`user_id`  
			//   LEFT JOIN `user_profiles` AS `UserProfile` ON (`UserProfile`.`user_id` = `User`.`id`) 
			//   LEFT JOIN `countries` AS Country ON (`UserProfile`.`country_id` = `Country`.`id`) 
			//   LEFT JOIN user_subscription_logs AS UserSubscriptionLog ON (`User`.`id` = `UserSubscriptionLog`.`user_id`) 
			//   LEFT JOIN user_employments UserEmployment ON (`User`.`id` = `UserEmployment`.`user_id`) 
			//   WHERE  `User`.`status` = 1 AND `User`.`approved` = 1 AND `UserProfile`.`country_id` = $loggedinUserCountryId   
			//   AND `UserDutyLog`.`hospital_id`= $loggedinUserCompanyId AND `UserEmployment`.`company_id`= $loggedinUserCompanyId AND `UserEmployment`.`is_current`=1 AND `UserSubscriptionLog`.`subscribe_type` !=3     GROUP BY `User`.`id`  ORDER BY `UserDutyLog`.`status` DESC, `UserDutyLog`.`atwork_status` DESC, `UserProfile`.`first_name` ASC";

			//************************Here Code without country check**************************// 

			  $quer = "SELECT `User`.`id`, `User`.`email`, `UserProfile`.`first_name`, `UserProfile`.`last_name`, 
			 `UserProfile`.`profession_id`, `UserProfile`.`profile_img`, `UserProfile`.`role_status`, `UserProfile`.`contact_no`, `UserProfile`.`contact_no_is_verified`, 
			 `UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`, `Country`.`country_code`    
			  FROM user_duty_logs UserDutyLog INNER JOIN `users` AS `User` ON `User`.`id` = `UserDutyLog`.`user_id`  
			  LEFT JOIN `user_profiles` AS `UserProfile` ON (`UserProfile`.`user_id` = `User`.`id`) 
			  LEFT JOIN `countries` AS Country ON (`UserProfile`.`country_id` = `Country`.`id`) 
			  LEFT JOIN user_subscription_logs AS UserSubscriptionLog ON (`User`.`id` = `UserSubscriptionLog`.`user_id`) 
			  LEFT JOIN user_employments UserEmployment ON (`User`.`id` = `UserEmployment`.`user_id`) 
			  WHERE  `User`.`status` = 1 AND `User`.`approved` = 1  
			  AND `UserDutyLog`.`hospital_id`= $loggedinUserCompanyId AND `UserEmployment`.`company_id`= $loggedinUserCompanyId AND `UserEmployment`.`is_current`=1 AND `UserSubscriptionLog`.`subscribe_type` !=3     GROUP BY `User`.`id`  ORDER BY `UserDutyLog`.`status` DESC, `UserDutyLog`.`atwork_status` DESC, `UserProfile`.`first_name` ASC";  
			   
			  $userLists = $this->query($quer);
			}
		}
		return $userLists;
	}

	/*
	-----------------------------------------------------------
	On: 14-11-2017
	I/P: $params = array()
	O/P: array() 
	Desc: Fetches user data (like oncall, available, role tags etc)
	-----------------------------------------------------------
	*/
	public function getDynamicUserDirectoryData($params = array(), $loggedinUser = NULL){
		$userLists = array();
		if( !empty($params) ){ 
			//$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			//$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			//$offsetVal = ( $pageNum - 1 ) * $pageSize;

			$loggedinUserCompanyId = $params['user_current_company_id'];
			$loggedinUserCountryId = $params['loggedin_user_countryid'];
			if(!empty($loggedinUserCompanyId)){
			/*$quer = "SELECT `User`.`id`,`User`.`email`, `UserProfile`.`role_status`,
			 `UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`   
			  FROM user_duty_logs UserDutyLog INNER JOIN `users` AS `User` ON `User`.`id` = `UserDutyLog`.`user_id`  
			  LEFT JOIN `user_profiles` AS `UserProfile` ON (`UserProfile`.`user_id` = `User`.`id`) 
			  WHERE  `User`.`status` = 1 AND `User`.`approved` = 1   AND `UserProfile`.`country_id` = $loggedinUserCountryId   
			  AND `UserDutyLog`.`hospital_id`= $loggedinUserCompanyId  GROUP BY `User`.`id`  ORDER BY `UserDutyLog`.`status` DESC, `UserDutyLog`.`atwork_status` DESC, `UserProfile`.`first_name` ASC";  */
			 


			 //***********Here directory with country check **************//


			 //  $quer = "SELECT `User`.`id`, `User`.`email`, `UserProfile`.`first_name`, `UserProfile`.`last_name`, 
			 // `UserProfile`.`profession_id`, `UserProfile`.`profile_img`, `UserProfile`.`role_status`, `UserProfile`.`contact_no`, `UserProfile`.`contact_no_is_verified`, 
			 // `UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`, `Country`.`country_code`    
			 //  FROM user_duty_logs UserDutyLog INNER JOIN `users` AS `User` ON `User`.`id` = `UserDutyLog`.`user_id`  
			 //  LEFT JOIN `user_profiles` AS `UserProfile` ON (`UserProfile`.`user_id` = `User`.`id`) 
			 //  LEFT JOIN `countries` AS Country ON (`UserProfile`.`country_id` = `Country`.`id`) 
			 //  LEFT JOIN user_subscription_logs AS UserSubscriptionLog ON (`User`.`id` = `UserSubscriptionLog`.`user_id`) 
			 //  LEFT JOIN user_employments UserEmployment ON (`User`.`id` = `UserEmployment`.`user_id`)   
			 //  WHERE  `User`.`status` = 1 AND `User`.`approved` = 1 AND `UserProfile`.`country_id` = $loggedinUserCountryId   
			 //  AND `UserDutyLog`.`hospital_id`= $loggedinUserCompanyId AND `UserEmployment`.`company_id`= $loggedinUserCompanyId AND `UserEmployment`.`is_current`=1 AND `UserSubscriptionLog`.`subscribe_type` !=3   GROUP BY `User`.`id`  ORDER BY `UserDutyLog`.`status` DESC, `UserDutyLog`.`atwork_status` DESC, `UserProfile`.`first_name` ASC"; 



			  //***********Here directory without country check **************//
			  $quer = "SELECT `User`.`id`, `User`.`email`, `UserProfile`.`first_name`, `UserProfile`.`last_name`, 
			 `UserProfile`.`profession_id`, `UserProfile`.`profile_img`, `UserProfile`.`role_status`, `UserProfile`.`contact_no`, `UserProfile`.`contact_no_is_verified`, 
			 `UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`, `Country`.`country_code`    
			  FROM user_duty_logs UserDutyLog INNER JOIN `users` AS `User` ON `User`.`id` = `UserDutyLog`.`user_id`  
			  LEFT JOIN `user_profiles` AS `UserProfile` ON (`UserProfile`.`user_id` = `User`.`id`) 
			  LEFT JOIN `countries` AS Country ON (`UserProfile`.`country_id` = `Country`.`id`) 
			  LEFT JOIN user_subscription_logs AS UserSubscriptionLog ON (`User`.`id` = `UserSubscriptionLog`.`user_id`) 
			  LEFT JOIN user_employments UserEmployment ON (`User`.`id` = `UserEmployment`.`user_id`)   
			  WHERE  `User`.`status` = 1 AND `User`.`approved` = 1
			  AND `UserDutyLog`.`hospital_id`= $loggedinUserCompanyId AND `UserEmployment`.`company_id`= $loggedinUserCompanyId AND `UserEmployment`.`is_current`=1 AND `UserSubscriptionLog`.`subscribe_type` !=3   GROUP BY `User`.`id`  ORDER BY `UserDutyLog`.`status` DESC, `UserDutyLog`.`atwork_status` DESC, `UserProfile`.`first_name` ASC"; 
			  $userLists = $this->query($quer);
			}
		}
		return $userLists;
	}

	/*
	-----------------------------------------------------------
	On: 16-11-2017
	I/P: $userId, $companyId
	O/P: array() 
	Desc: Fetches changed user data (like oncall, available, role tags etc)
	-----------------------------------------------------------
	*/

	// public function getChangedUserData($userId, $companyId, $email){
	// 	$userLists = array();
	// 	if(!empty($userId)){
	// 	$quer = "SELECT UserDutyLog.status,UserDutyLog.atwork_status,UserProfile.role_status
	// 			FROM enterprise_user_lists 
	// 			LEFT JOIN users AS User ON User.email = enterprise_user_lists.email
	// 			LEFT JOIN user_duty_logs AS UserDutyLog ON UserDutyLog.user_id = User.id
	// 			LEFT JOIN user_profiles AS UserProfile ON UserProfile.user_id = User.id
	// 			Where enterprise_user_lists.status = 1 AND enterprise_user_lists.company_id=$companyId AND  UserDutyLog.user_id = $userId AND  enterprise_user_lists.email='$email'";  
		   
	// 	  $userLists = $this->query($quer);
	// 	}
	// 	return $userLists;
	// }

	public function getChangedUserData($userId, $companyId){
		$userLists = array();
		if(!empty($userId)){
		$quer = "SELECT UserDutyLog.status,UserDutyLog.atwork_status,UserProfile.role_status
					FROM user_duty_logs AS UserDutyLog
					LEFT JOIN user_profiles AS UserProfile ON UserDutyLog.user_id = UserProfile.user_id
					WHERE UserDutyLog.user_id = $userId AND UserDutyLog.hospital_id = $companyId";  
		   
		  $userLists = $this->query($quer);
		}
		return $userLists;
	}


	/*
	-----------------------------------------------------------
	On: 17-11-2017
	I/P: $userId, $companyId
	O/P: array() 
	Desc: Fetches deleted user data.
	-----------------------------------------------------------
	*/

	public function getDeleteduser($userId, $companyId, $countryId){
		$userLists = array();
		if( !empty($userId) ){ 
			$quer = "SELECT `User`.`id`, `User`.`email`, `UserProfile`.`first_name`, `UserProfile`.`last_name`, 
			 `UserProfile`.`profession_id`, `UserProfile`.`profile_img`, `UserProfile`.`role_status`, `UserProfile`.`contact_no`, `UserProfile`.`contact_no_is_verified`, 
			 `UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`, `Country`.`country_code`    
			  FROM user_duty_logs UserDutyLog INNER JOIN `users` AS `User` ON `User`.`id` = `UserDutyLog`.`user_id`  
			  LEFT JOIN `user_profiles` AS `UserProfile` ON (`UserProfile`.`user_id` = `User`.`id`) 
			  LEFT JOIN `countries` AS Country ON (`UserProfile`.`country_id` = `Country`.`id`)
			  WHERE  `User`.`status` = 1 AND `User`.`approved` = 1 AND `UserProfile`.`country_id` = $countryId 
			  AND `UserDutyLog`.`hospital_id`= $companyId 
			  AND `User`.`id` = $userId
			  GROUP BY `User`.`id`  ORDER BY `UserDutyLog`.`status` DESC, `UserDutyLog`.`atwork_status` DESC, `UserProfile`.`first_name` ASC";  
			   
			  $userLists = $this->query($quer);
		}
		return $userLists;
	}

}
?>