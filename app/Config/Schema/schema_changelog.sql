
/*-----By Nimay: 27-07-2015[START]---*/
ALTER TABLE  `specilities` ADD  `status` TINYINT NOT NULL DEFAULT  '1' COMMENT  '1=> Active, 0=> Inactive';

/*---By Nimay: 27-07-2015[END]---*/

/*-----By Nimay: 28-07-2015[START]---*/
ALTER TABLE `professions` 
ADD COLUMN `status` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '0=>Inactive, 1=>Active' AFTER `profession_type`;
/*---By Nimay: 28-07-2015[END]---*/

/*-----By Nimay: 31-07-2015[START]---*/
ALTER TABLE `user_profiles` CHANGE `gender` `gender` ENUM('M','F') NOT NULL DEFAULT 'M' COMMENT 'user’s gender. M=> Male, F=>Female';
/*-----By Nimay: 31-07-2015[END]---*/

/*-----By Nimay: 05-08-2015[START]---*/
ALTER TABLE `user_devices` ADD `status` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0=> Inactive, 1=>Active' AFTER `token`;

ALTER TABLE `user_devices` CHANGE `token` `token` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'unique token for every logged in user.';
/*-----By Nimay: 05-08-2015[END]---*/

/*-----By Nimay: 06-08-2015[START]---*/
ALTER TABLE `user_profiles` CHANGE `registration_number` `registration_image` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'Registration number of user (like DL, passport no.) Image upload during registration';
/*-----By Nimay: 06-08-2015[END]---*/

/*-----By Nimay: 10-08-2015[START]---*/
ALTER TABLE `specilities` ADD `img_name` VARCHAR(50) NULL DEFAULT NULL COMMENT 'thumb image name for specility' AFTER `name`;

UPDATE `specilities` SET img_name= CONCAT(md5(CONCAT_ws('_',id,name)),'.png');
/*-----By Nimay: 10-08-2015[END]---*/

/*-----By Nimay: 14-08-2015[START]---*/
ALTER TABLE  `user_devices` CHANGE  `device_id`  `device_id` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT  'Device id of logged in user';
/*-----By Nimay: 14-08-2015[END]---*/

/*-----By Nimay: 17-08-2015[START]---*/
ALTER TABLE `user_devices` ADD `login_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;

ALTER TABLE `user_devices` ADD `device_type` VARCHAR(10) NOT NULL DEFAULT '0' COMMENT 'device type of loggedin user (like android, ios,web etc)' AFTER `login_date`;
/*-----By Nimay: 17-08-2015[END]---*/

/*-----By Nimay: 17-09-2015[START]---*/
ALTER TABLE `user_posts` ADD `is_anonymous` TINYINT(2) NOT NULL DEFAULT '0' COMMENT 'ifuser post as a Anonymous 0=> No, 1=> yes' AFTER `status`;
/*-----By Nimay: 17-09-2015[END]---*/

/*-----By Nimay: 23-09-2015[START]---*/
ALTER TABLE `user_posts` ADD `parent_post_id` INT NULL DEFAULT NULL COMMENT 'parent id of post if this post is shared post' AFTER `is_anonymous`;

ALTER TABLE `user_posts` ADD `original_post_id` INT NULL DEFAULT NULL COMMENT 'original post id of shared post' AFTER `parent_post_id`;

ALTER TABLE `user_post_shares` CHANGE `shared_by` `shared_from` INT(11) NOT NULL COMMENT 'id from users table whose post has been shared';
/*-----By Nimay: 23-09-2015[END]---*/

/*-----By Nimay: 30-09-2015[START]---*/
ALTER TABLE `consent_form_attributes` ADD `is_signature` TINYINT NOT NULL DEFAULT '0' COMMENT 'Storing this is signature or not, 0=> No, 1=> Yes' AFTER `attribute_type`;

ALTER TABLE `user_posts` ADD `consent_id` INT NOT NULL DEFAULT '0' COMMENT 'id from consent_forms table. if post is associated with any consent' AFTER `original_post_id`;
/*-----By Nimay: 30-09-2015[END]---*/

/*-----By Nimay: 07-10-2015[START]---*/
ALTER TABLE `user_profiles` ADD `current_employment` VARCHAR(150) NULL DEFAULT NULL AFTER `alias_name`;
/*-----By Nimay: 07-10-2015[END]---*/

/*-----By Nimay: 12-10-2015[START]---*/
ALTER TABLE `user_follows` ADD `follow_type` TINYINT(2) NOT NULL COMMENT '0=> No Follow, 1=> Follow, 2=> Neutral' AFTER `followed_to`;
ALTER TABLE `user_follows` CHANGE `follow_type` `follow_type` TINYINT(2) NOT NULL DEFAULT '0' COMMENT '0=> No Follow, 1=> Follow, 2=> Neutral';
/*-----By Nimay: 12-10-2015[END]---*/

/*-----By Nimay: 14-10-2015[START]---*/
ALTER TABLE  `user_specilities` ADD  `status` TINYINT( 2 ) NOT NULL DEFAULT  '0' COMMENT  '0=> Inactive, 1=> Active';
/*-----By Nimay: 14-10-2015[END]---*/


