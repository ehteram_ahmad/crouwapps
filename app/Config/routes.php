<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	//Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
	Router::connect('/', array('controller' => 'index', 'action' => 'index'));
	Router::connect('/about', array('controller' => 'index', 'action' => 'about'));
	Router::connect('/contact', array('controller' => 'index', 'action' => 'contact'));
	Router::connect('/network-guidelines', array('controller' => 'index', 'action' => 'networkGuidelines'));
	Router::connect('/terms-and-conditions', array('controller' => 'index', 'action' => 'tnc'));
	Router::connect('/join-the-team', array('controller' => 'index', 'action' => 'joinTheTeam'));
	Router::connect('/privacy-policy', array('controller' => 'index', 'action' => 'privacyPolicy'));
	Router::connect('/faq', array('controller' => 'index', 'action' => 'faq'));
	Router::connect('/blog', array('controller' => 'index', 'action' => 'blog'));
	Router::connect('/quick-start', array('controller' => 'index', 'action' => 'quickStart'));
	Router::connect('/product-tour', array('controller' => 'index', 'action' => 'productTour'));
	Router::connect('/other-apps', array('controller' => 'index', 'action' => 'otherApps'));
	Router::connect('/press', array('controller' => 'index', 'action' => 'press'));
	Router::connect('/student-ambassador', array('controller' => 'index', 'action' => 'studentAmbassador'));
	Router::connect('/support', array('controller' => 'index', 'action' => 'support'));
	Router::connect('/how-you-can-use-ocr', array('controller' => 'index', 'action' => 'howYouCanUseOcr'));
	Router::connect('/admin', array('plugin'=>'admin', 'controller' => 'index', 'action' => 'index'));
	Router::connect('/subadmin', array('plugin'=>'Subadmin', 'controller' => 'SubadminIndex', 'action' => 'index'));
	Router::connect('/campaign', array('plugin'=>'campaign', 'controller' => 'CampaignHome', 'action' => 'index'));
	Router::connect('/signup', array('plugin'=>'signup', 'controller' => 'Signup', 'action' => 'index'));
	Router::connect('/healthcheck', array('controller' => 'index', 'action' => 'healthcheck'));

	Router::connect('/institution', array('plugin'=>'institution', 'controller' => 'InstitutionHome', 'action' => 'index'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

