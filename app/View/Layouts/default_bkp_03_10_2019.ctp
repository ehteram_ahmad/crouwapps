<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="UTF-8" />
<title>Medical Collaboration Network: Doctors, Nurses &amp; Pharmacists<?php //echo isset($pageTitle) ? $pageTitle : '';?></title>
<?php 
//** Call css files
	echo $this->Html->css(array('bootstrap', 'style', 'responsive'));
//** Call js files	
	echo $this->Html->script(array('jquery-1.9.1', 'bootstrap', 'modernizr.custom', 'classie'));
?>


<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
<!-- Favicons -->
    <link rel="icon" href="favicon.ico">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
</head>
<body>
<header><!--header-->
<nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
      <div class="row">     
          <!--menu link-->
	  			
		<div class="navbar-header fl">		 
		 <a title="The OCR" class="navbar-brand" href="<?php echo BASE_URL;?>">
		 	<?php
		 		echo $this->Html->image('logo.png', 
		 			array('alt' => 'Logo', 'width'=> 123, 'height'=> 63)
		 			);
		 	?>
		</div>
		
          <?php /* ?><div class="social-links">
					<ul>
						<li><a target="_blank" title="LinkedIn" href="https://www.linkedin.com/company/4842839?trk=tyah&amp;trkInfo=idx%3A1-1-1%2CtarId%3A1425467003079%2Ctas%3Athe+on+call+room">
						<?php
							echo $this->Html->image('linked-icon.png', 
							array('alt' => 'LinkedIn')
							);
						?>
						</a></li>
							<li><a target="_blank" title="Facebook" href="https://www.facebook.com/TheOnCallRoom?ref=br_rs">
						<?php
							echo $this->Html->image('fb-icon.png', 
							array('alt' => 'Facebook')
							);
						?>
							</a></li>
						<li><a target="_blank" title="Twitter" href="https://twitter.com/theoncallroom">
						<?php
							echo $this->Html->image('tweets.png', 
							array('alt' => 'Twitter')
							);
						?>
						</a></li>
					</ul>
				</div><?php */ ?>
				<!--menu links-->         
            <ul class="nav navbar-nav navbar-right social-menu">
			  <li>
			    <div class="home-links">
			    	<?php $actionName = $this->action; ?>
				  <ul class="links-nav">
				   <li><a class="<?php echo ($actionName == 'productTour' ? 'active':''); ?>" title="Product Tour" href="<?php echo BASE_URL . 'product-tour'; ?>">Product Tour</a></li>
				   <li><a class="<?php echo ($actionName == 'quickStart' ? 'active':''); ?>" title="Get Started" href="<?php echo BASE_URL . 'quick-start'; ?>">Get Started</a></li>
                 </ul>				
				</div>			  
			  </li>
              <li>
               <div class="home-links">
				 <ul>
				  <li class="dropdown"> <a href="javascript:void(0);" class="user-icon" title="Dropdown">Menu</a>
					<div class="user-nav">
					   <a href="javascript:void(0);" class="close-menu" title="">Back</a>
					  <ul>					
					    <li><a class="<?php echo ($actionName == 'about' ? 'active':''); ?>" title="About" href="<?php echo BASE_URL . 'about'; ?>" data-toggle-tab=".about-link">About</a>
						  <ul class="submenu">
								<li><a href="<?php echo BASE_URL . 'about#c'; ?>" data-toggle-tab=".mission-link">Our Mission</a></li>
								<li><a href="<?php echo BASE_URL . 'about#d'; ?>" data-toggle-tab=".people-link">Our People</a></li>
								<li><a href="<?php echo BASE_URL . 'about#b'; ?>" data-toggle-tab=".commitment-link">Our Commitment</a></li>
							</ul>
					    </li>
					    <li><a class="<?php echo ($actionName == 'howYouCanUseOcr' ? 'active':''); ?>" title="How You Can Use The OCR" href="<?php echo BASE_URL . 'how-you-can-use-ocr'; ?>">How You Can Use The OCR</a></li>
					    <!--<li><a title="Who’s using The OCR?" href="usingOCR.html">Who’s using The OCR?</a></li>--><li><a class="<?php echo ($actionName == 'support' ? 'active':''); ?>" href="<?php echo BASE_URL . 'support'; ?>" title="Support">Support</a></li>
						<li><a class="<?php echo ($actionName == 'blog' ? 'active':''); ?>" href="<?php echo BASE_URL . 'blog/'; ?>" title="Blog">Blog</a></li>
						<li><a class="<?php echo ($actionName == 'contact' ? 'active':''); ?>" href="<?php echo BASE_URL . 'contact'; ?>" title="Contact">Contact</a></li>
					  </ul>
					</div>
                 </li>
			    </ul>
			  
               </div>
              </li>
 

            </ul>
         
          <!--//menu links--> 
                    
      </div>
   </div>
</nav>
  <!--//header--></header>
<?php echo $this->fetch('content'); ?>


<footer>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="footer-left">
            
				<ul>
                	<li><h4>The OCR Legal</h4></li>
					<li><a title="Terms &amp; Conditions" href="<?php echo BASE_URL . 'terms-and-conditions'; ?>">Terms &amp; Conditions</a></li>
					<li><a title="Privacy" href="<?php echo BASE_URL . 'privacy-policy'; ?>">Privacy</a></li>
					<li><a title="Network Guidelines" href="<?php echo BASE_URL . 'network-guidelines'; ?>">Network Guidelines</a></li>
				</ul>
                
				<ul>
                	<li><h4>Using The OCR</h4></li>
					<li><a title="Contact" href="<?php echo BASE_URL . 'contact'; ?>">Contact</a></li>
					<li><a title="Support" href="<?php echo BASE_URL . 'support'; ?>">Support</a></li>
					<li><a title="Join The Team" href="<?php echo BASE_URL . 'join-the-team'; ?>" >Join The Team</a></li>
					<li><a title="Student Ambassadors" href="<?php echo BASE_URL . 'student-ambassador'; ?>" >Student Ambassadors</a></li>
					
                    <!--<li><a title="Student Ambassadors" href="#">Student Ambassadors</a></li>-->
				</ul>
               
				<ul>
                	<li><h4>About The OCR</h4></li>
					<li><a title="About"  href="<?php echo BASE_URL . 'about'; ?>" >About</a></li> <li><a title="Press"  href="<?php echo BASE_URL . 'press'; ?>" >Press</a></li>         
					<li><a title="Blog" href="<?php echo BASE_URL . 'blog/'; ?>" >Blog</a></li>
                    <li><a title="Our Other Apps" href="<?php echo BASE_URL . 'other-apps'; ?>" >Our Other Apps</a></li>
				</ul>
                
			</div>
			<div class="footer-right">
				<div class="social-links">
					<ul>
						<li><a target="_blank" title="LinkedIn" href="https://www.linkedin.com/company/4842839?trk=tyah&amp;trkInfo=idx%3A1-1-1%2CtarId%3A1425467003079%2Ctas%3Athe+on+call+room">
						<?php
							echo $this->Html->image('linked-icon.png', 
							array('alt' => 'LinkedIn')
							);
						?>
						</a></li>
							<li><a target="_blank" title="Facebook" href="https://www.facebook.com/TheOnCallRoom?ref=br_rs">
						<?php
							echo $this->Html->image('fb-icon.png', 
							array('alt' => 'Facebook')
							);
						?>
							</a></li>
						<li><a target="_blank" title="Twitter" href="https://twitter.com/theoncallroom">
						<?php
							echo $this->Html->image('tweets.png', 
							array('alt' => 'Twitter')
							);
						?>
						</a></li>
					</ul>
				</div>
				<p>Copyright &copy; <a href="http://mediccreations.com/" target="_blank" style="color:#000; text-decoration:underline;">
					<?php
							echo $this->Html->image('medic-logo-small.png', 
							array('alt' => 'Logo')
							);
					?>
					Medic Creations</a> All Rights Reserved. </p>	  
				<!-- <p>Copyright &copy; All Rights Reserved. The On Call Room Limited.</p>
				<p>Designed &amp; Developed by Net Solutions</p> -->
			</div>
		</div>
	</div>
</div>
</footer>
<?php 
//** Call js files	
	echo $this->Html->script(array('functions', 'jquery.fancybox', 'jquery.validate.js', 'jquery.bxslider', 'jquery-ui'));
?>

<script>
/**
* Function that tracks a click on an outbound link in Analytics.
* This function takes a valid URL string as an argument, and uses that URL string
* as the event label. Setting the transport method to 'beacon' lets the hit be sent
* using 'navigator.sendBeacon' in browser that support it.
*/
var trackOutboundLink = function(url) {
   ga('send', 'event', 'outbound', 'click', url, {
     'transport': 'beacon',
     'hitCallback': function(){document.location = url;}
   });
}
</script>

</body>
</html>


