<section class="inner-content mobile"><!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <div class="bs-example">
	 <h3>1. Terms of Service</h3>

<p>The On Call Room Ltd., ("us," our" or "we"), provides a messaging and communications service called "The On Call Room (The OCR)" (the "Services") through our website (the "Site") and our application for mobile devices, tablet computers and similar devices (the "App"). Please read carefully the following terms and conditions ("Terms") and our Privacy Policy. These Terms govern your access to and use of the Site, Services, App and Collective Content (defined below), and constitute a binding legal agreement between you and The On Call Room Ltd.</p>

<p>YOU ACKNOWLEDGE AND AGREE THAT, BY CLICKING ON THE "AGREE" OR "I ACCEPT" BUTTON, OR ACCESSING OR USING THE SITE, SERVICES OR APP, OR BY DOWNLOADING OR POSTING ANY CONTENT FROM OR THROUGH THE SITE, SERVICES OR APP, YOU ARE INDICATING THAT YOU HAVE READ, AND UNDERSTAND AND AGREE TO BE BOUND BY, THESE TERMS, WHETHER OR NOT YOU HAVE REGISTERED VIA THE SITE OR APP. YOU HAVE NO RIGHT TO ACCESS OR USE THE SITE, SERVICES, APP OR COLLECTIVE CONTENT UNLESS YOU AGREE TO THESE TERMS. If you accept or agree to these Terms on behalf of a company or other legal entity, you represent and warrant that you have the authority to bind that company or other legal entity to these Terms and, in such event, "you" and "your" extends beyond you to also refer and apply to that company or other legal entity.</p>

<h3>2. Key Content-Related Terms</h3>

<p>"Collective Content" means, collectively, The On Call Room (The OCR) Content and Registered User Content.<br>
"Content" means text, graphics, images, software (excluding the App) information or other materials.<br>
"The On Call Room (The OCR) Content" means Content that we make available through the Site, Services or App, including any Content licensed from a third party, but excluding Registered User Content.<br>
"Registered User" and "you" means a person that completes The On Call Room (The OCR)'s account registration process, as described in the "Account Registration" section below; "your" applies as the meaning of the clause requires.<br>
"Registered User Content" means Content a Registered User posts, uploads, publishes, submits or transmits to be made available through the Site, Services or App.<br>
"Collective Content" means, collectively, The On Call Room (The OCR) Content and Registered User Content. Certain areas of the Site and App (and your access to or use of certain Collective Content) may have different terms and conditions posted or may require you to agree with and accept additional terms and conditions. If there is a conflict between these Terms and terms and conditions posted for a specific area of the Site, Services, App or Collective Content, the latter terms and conditions will take precedence with respect to your use of or access to that area of the Site, Services, App or Collective Content. Unless explicitly stated otherwise, any new features that augment or enhance the current Services in future shall be subject to these Terms; they will be posted to the Site as available as well as to the App and you are deemed to have accepted them.</p>

<h3>3. Modification</h3>

<p>The On Call Room Ltd. reserves the right, at its sole discretion, to modify, discontinue or terminate the Site, Services or App or to modify these Terms, at any time and without prior notice. If we modify these Terms we will post the modification via the Site or App or provide you with notice of the modification. By continuing to access or use the Site, Services or App after we have posted a modification via the Site or App or have provided you with notice of a modification, you agree to be bound by the modified Terms. If the modified Terms are not acceptable to you, you agree to immediately stop using the Site, Services and App.</p>

<h3>4. Account Registration; Conditions on Access and Use of Service</h3>

<p>Access to and use of the Services is limited to individuals who are at least 18 years of age and residents of Canada, the United Kingdom, Ireland, Australia, New Zealand, South Africa, France, Germany, Sweden, Norway, Finland, Iceland, Netherlands, the Spanish Kingdom, Portugal, Austria, Belgium, Greece, Malta, Cyrpus, India, Pakistan, China or the United States (the "Countries"). If you access and use the Services, but are not a resident of one of the Countries, then you are responsible to insure that your use of the Services complies with the laws of the your country of residence and assume the risks (including indemnity under Section 19) attendant to the use of the Services and the use of any materials that you provide. You must register to create an account ("Account") and become a "Registered User" to use the Services. To register, you must provide a user name, your email address, your medical specialty, and other information specified in the registration form ("Registration Data"). By registering, and in consideration of the use of the Services, you represent and warrant:<br>
(i) the Registration Data that you provide about yourself is true, accurate, current, and complete;<br>
(ii) you are at least 18 years of age; and<br>
(iii) you will maintain and promptly update the Registration Data to keep it at all times true, accurate, current and complete. You authorize The On Call Room Ltd. to confirm the truthfulness and accuracy of the Registration Data but agree that such authorization in no way binds The On Call Room Ltd. to do so. If you provide any information that is untrue, inaccurate, not current or incomplete, or if The On Call Room Ltd. has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, The On Call Room Ltd. has the right to suspend or terminate your Account and refuse any and all current or future use of the Services. You also authorize The On Call Room Ltd. to access your contact list and/or address book on your device at your direction in order to allow you to share information and images with specific contacts you choose or to allow you to invite specific contacts you choose to use the Services. If your invitee is a resident of Australia, New Zealand, South Africa, the United Kingdom, Ireland, France, Germany, Sweden, Norway, Finland, Iceland, Netherlands, the Spanish Kingdom, Portugal, Austria, Belgium, Greece, Malta, Cyprus or any other part of the European Economic Area, you must obtain his or her prior consent to using his or her e-mail address to share information or images or issue an invitation to use the Services.</p>

<p>Use of the Services is void where prohibited by law or otherwise. By using the Services, you represent and warrant that you have the right, authority, and capacity to agree to and abide by these Terms and that you are not prohibited from using the Services. You understand that your use of the Services may involve or require the transmission of significant amounts of data. You are solely responsible for all data charges that may be charged by your wireless carrier or internet service provider or that may otherwise arise from your use of the Services.</p>

<p>You understand and agree that the Services may include certain communications from The On Call Room Ltd., such as service announcements and administrative messages, and that while these communications are considered part of The On Call Room (The OCR) membership, you will be able to opt out of receiving them. However, you will not be able to opt out of any announcements and messages related to the implementation of The On Call Room (The OCR)'s Privacy Policy and your obligations thereunder with regard to your Registered User Content.</p>

<p>Please note that The On Call Room Ltd. does not currently charge fees for the use of the Services. However, you acknowledge and agree that The On Call Room Ltd. reserves the right, in its sole discretion, to charge you for andcollect fees from you for the use of the Services to send and receive communications. The On Call Room Ltd. will provide notice of any fee collection via the Services, prior to implementing such a fee, and you will have a choice at that time to continue to use the Services or not. If you choose not to pay, The On Call Room Ltd. reserves the right to immediately terminate your access to the Services.</p>

<p>Some of the Service may be supported by advertising revenue and may display advertisements and promotions, and you hereby agree that The On Call Room Ltd. may place such advertising and promotions on the Service or on, about, or in conjunction with your Content. The manner, mode and extent of such advertising and promotions are subject to change without specific notice to you.</p>

<h3>5. Security</h3>

<p>The Services are designed to require users to provide a valid, working e-mail address and password to access and use the Site, Services, App and Collective Content. Upon registering, you will select a password. Your e-mail address and the password and codes assigned to you are, collectively, your "User Information." You are solely responsible for<br>
(1) maintaining the strict confidentiality of your User Information,<br>
(2) not allowing another person to use your User Information to access the Services,<br>
(3) any and all damages or losses that may be incurred or suffered as a result of any activities that occur under your User Information. You agree to immediately notify The On Call Room Ltd. in writing by <a href="mailto:contact@theoncallroom.com">email</a> of any unauthorized use of your User Information or any other breach of security. The On Call Room Ltd. is not and shall not be liable for any harm arising from or relating to the theft of your User Information, your disclosure of your User Information, or the use of your User Information by another person or entity. Any attempt to obtain unauthorized access or to exceed authorized access to the Site, Services, App or Collective Content shall be considered a trespass and computer fraud and abuse, punishable under provincial, state and federal laws. The On Call Room Ltd. hereby notifies you that any or all communications with this Site can and will be monitored, captured, recorded, and transmitted to the authorities as deemed necessary by The On Call Room Ltd. in its sole discretion and without further notice. Notwithstanding the foregoing, as a Registered User, you remain responsible for your individual compliance with any additional applicable notification requirements related to the unauthorized use of User Information or Registered User Content.</p>

<h3>6. Disclaimers</h3>

<p>6.1. NO DIAGNOSTIC SERVICE<br>
THE ON CALL ROOM (THE OCR) IS PRIMARILY AN EDUCATIONAL TOOL AND IS NOT INTENDED TO SERVE THE FOLLOWING NEEDS: AS A DIAGNOSTIC SERVICE; AS A CONFIRMATORY SERVICE TO PROVIDE CERTAINTY IN DIAGNOSIS; TO SELECT, GUIDE, OR PROMOTE THERAPY OF MEDICAL CONDITIONS; FOR USE IN HAZARDOUS OR MISSION-CRITICAL CIRCUMSTANCES OR FOR USES REQUIRING FAIL-SAFE PERFORMANCE; OR IN SITUATIONS WHERE FAILURE COULD LEAD TO DEATH OR PERSONAL INJURY (COLLECTIVELY, "UNAUTHORIZED PURPOSES"). BECAUSE THE ON CALL ROOM (THE OCR) HAS NOT DESIGNED, INTENDED, OR AUTHORIZED FOR SUCH UNAUTHORIZED PURPOSES, YOU SHALL NOT USE THE SITE, SERVICES OR APP FOR SUCH PURPOSES OR UNDER SUCH CIRCUMSTANCES. YOU FURTHER ACKNOWLEDGE THAT THE USE OF THE ON CALL ROOM (THE OCR)'S SITE, SERVICES, AND APP FOR SUCH UNAUTHORIZED PURPOSES MAY CONSTITUTE A VIOLATION OF LAWS APPLICABLE TO THE PRACTICE OF MEDICINE OR OTHER HEALTH PROFESSION(S).</p>

<p>6.2. NO WARRANTY<br>
YOUR RELIANCE UPON THE CONTENT OBTAINED OR USED BY YOU THROUGH THE SITE, SERVICES OR APP IS SOLELY AT YOUR OWN RISK.</p>

<h3>7. Privacy</h3>

<p>See our Privacy Policy for information and notices concerning The On Call Room Ltd.'s collection and use of your personal information. By clicking on the "Agree" or "I Accept" button, or accessing or using the Site, Services or App, or by downloading or posting any content from or through the Site, Services or App, you acknowledge and agree to the provisions of the Privacy Policy and affirm that the Privacy Policy forms a part of these Terms.</p>

<p>If you are a resident of the United Kingdom, Ireland, France, Germany, Sweden, Norway, Finland, Iceland, Netherlands, the Spanish Kingdom, Portugal, Austria, Belgium, Greece, Malta or Cyprus please note that for the purposes of providing the App, The On Call Room (The OCR) may transfer your personal data to servers located in the United States or other countries outside of the European Economic Area which provide for a different level of data protection. By using, and continuing to use the App, you agree to that transfer. Similarly, if you live in South Africa, you agree to the transfer of your personal data outside the country.</p>

<p>All content generated by a Registered User must comply with local, provincial, state, and federal privacy legislation and best practices. Identifying information must be removed from Registered User Content. To assist you, The On Call Room Ltd. will make available to you its proprietary tools to help you remove direct identifiers and other common identifiers, including the tools' automatic and manual information-blocking features.</p>

<p>The Registered User agrees and warrants that they have removed all identifying information regarding the person in the Image and the relatives, employers or household members of that person. This includes, but is not limited to, the following:</p>

<ul class="aboutTab padLeft40">
	<li>Names;</li>
	<li>All geographical subdivisions smaller than a State, including street address, city, county, precinct, zip code, and their equivalent geocodes, except for the initial three digits of a zip code, if according to the current publicly available data from the Bureau of the Census: (1) The geographic unit formed by combining all zip codes with the same three initial digits contains more than 20,000 people; and (2) The initial three digits of a zip code for all such geographic units containing 20,000 or fewer people is changed to 000;</li>
	<li>United Kingdom, Irish, Belgian, Austrian, Greek or South African postal codes or parts of postal codes;</li>
	<li>All elements (except year) for dates directly related to an individual, including birth date, admission date, patient encounter date, discharge date, date of death and exact age if over 89;</li>
	<li>Phone numbers;</li>
	<li>Fax numbers;</li>
	<li>Electronic mail addresses;</li>
	<li>Social security or social insurance numbers, or equivalent;</li>
	<li>Medical record numbers;</li>
	<li>Health card numbers, health plan beneficiary numbers, or Health Insurance Account numbers;</li>
	<li>Account numbers;</li>
	<li>Certificate/license numbers;</li>
	<li>Vehicle identifiers and serial numbers, including license plate numbers;</li>
	<li>Device identifiers and serial numbers;</li>
	<li>Web Universal Resource Locators (URLs);</li>
	<li>Internet Protocol (IP) address numbers;</li>
	<li>Biometric identifiers, including finger and voice prints;</li>
	<li>Full face photographic images and any comparable images; and</li>
	<li>Photographic images of scars, tattoos, piercings etc. which would allow for an identification and</li>
	<li>Any other unique identifying number, characteristic, or code (note this does not mean the unique code assigned by the investigator to code the data).</li>
</ul>

<p>Registered User agrees and warrants that Registered User does not have actual knowledge that the Images could be used alone or in combination with other information to identify an individual who is the subject of the information.</p>

<h3>8. Ownership</h3>

<p>The On Call Room Ltd. does not claim ownership of any Content that you post on or through the Service. Instead, you hereby grant to The On Call Room Ltd., in perpetuity, a non-exclusive, fully paid and royalty-free, transferable, sub-licensable (through several tiers), worldwide license to use, reproduce, view, communicate to the public by any means, print, copy, (whether onto hard disk or other media), edit, translate, perform and display (publicly or otherwise), redistribute, modify, adapt, make, sell, offer to sell, transmit, distribute, license, transfer, stream, broadcast, create derivative works from, and otherwise use and exploit the Content that you post on or through the Service, subject to the Service's Privacy Policy.</p>

<h3>9. Licenses Granted by The On Call Room Ltd. To The On Call Room (THE OCR) Content and Registered User Content</h3>

<p>Subject to your compliance with the terms and conditions of these Terms, The On Call Room Ltd. grants you a limited, non-exclusive, non-transferable license: (i) to view, any The On Call Room (The OCR) Content solely for your personal and non-commercial purposes; and (ii) to view any Registered User Content to which you are permitted access solely for your personal and non-commercial purposes. You have no right to sublicense the license rights granted in this section.<br>
You will not use, copy, adapt, modify, prepare derivative works based upon, distribute, license, sell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the Site, Services, App or Collective Content, except as expressly permitted in these Terms or expressly permitted by applicable copyright laws. No licenses or rights are granted to you by implication or otherwise under any intellectual property rights owned or controlled by The On Call Room Ltd. or its licensors, except for the licenses and rights expressly granted in these Terms.</p>

<h3>10. License Granted by Registered User</h3>

<p>We may, in our sole discretion, permit Registered Users to post, upload, publish, submit or transmit Registered User Content. By making available any Registered User Content on or through the Site, Services or App, you hereby grant to The On Call Room Ltd. a worldwide, irrevocable, perpetual, non-exclusive, transferable, royalty-free license, with the right to sublicense (through several tiers), to use, reproduce, view, communicate to the public by any means, print, copy, (whether onto hard disk or other media), edit, translate, perform and display (publicly or otherwise), distribute, redistribute, modify, adapt, make, sell, offer to sell, transmit, license, transfer, stream, broadcast, create derivative works from, and otherwise use and exploit such Registered User Content only on, through or by means of the Site, Services or App or by sublicense to partner or affiliate publications. The On Call Room Ltd. does not claim any ownership rights in any Registered User Content and nothing in these Terms will be deemed to restrict any rights that you may have to use and exploit any Registered User Content.<br>
You acknowledge and agree that you are solely responsible for all Registered User Content that you make available through the Site, Services or App. Accordingly, you represent and warrant that: (i) you either are the sole and exclusive owner of all Registered User Content that you make available through the Site, Services or App or you have all rights, licenses, consents and releases that are necessary to grant to The On Call Room Ltd. the rights in such Registered User Content, as contemplated under these Terms; and (ii) neither the Registered User Content nor your posting, uploading, publication, submission or transmittal of the Registered User Content or The On Call Room Ltd.'s use of the Registered User Content (or any portion thereof) on, through or by means of the Site, Services or App will (a) infringe, misappropriate or violate a third party's patent, copyright, trademark, trade secret, moral rights or other intellectual property rights, or rights of publicity or privacy, or (b) result in the violation of any applicable law or regulation, including, but not limited to, the Health Insurance Portability and Accountability Act (HIPAA), the Personal Information Protection and Electronic Documents Act (PIPEDA), the Personal Health Information Protection Act (PHIPA), the Health Information Technology for Economic and Clinical Health (HITECH) Act, the Data Protection Act 1998, the Protection of Personal Information Act 2013, the Data Protection Act of 1978, the Spanish Data Protection Act 15/1999, the Belgian Act of 08 December 1992, Law 2472/1997 on the Protection of the Individual from the Processing of Personal Data and any other applicable National, Provincial, State, and Federal privacy laws (collectively, the "Privacy Laws") depending on your country of access to the App. You retain the sole responsibility of your individual compliance with applicable laws.</p>

<h3>11. App License</h3>

<p>Subject to your compliance with these Terms, The On Call Room Ltd. grants you a limited non-exclusive, non-transferable license to download and install a copy of the App on a single mobile device or computer that you own or control and to run such copy of the App solely for your own personal and professional use. The non-transferable license is also limited by any terms of service provisions required by the vendor from whom you purchased the The On Call Room (The OCR) App (e.g., Apple iTunes, Google Play, etc.) (hereinafter, "App Vendor").</p>

<h3>12. Feedback</h3>

<p>We welcome and encourage you to provide feedback, comments and suggestions for improvements to the Site, Services or App ("Feedback"). You may submit Feedback by emailing us. You acknowledge and agree that if you submit any Feedback to us, you hereby grant to us a non-exclusive, worldwide, perpetual, irrevocable, fully-paid, royalty-free, sub-licensable (through several tiers) and transferable license under any and all intellectual property rights that you own or control in relation to Feedback to use, reproduce, view, communicate to the public by any means, print, copy, (whether onto hard disk or other media), edit, translate, perform and display (publicly or otherwise), distribute, redistribute, modify, adapt, make, sell, offer to sell, transmit, license, transfer, stream, broadcast, create derivative works from, and otherwise use and exploit the Feedback for any purpose.</p>

<h3>13. Patient Data and Legal Compliance</h3>

<p>Provincial, State and Federal laws, as well as ethical and licensure requirements of your profession and health regulatory college and licensing requirements impose obligations with respect to protection of privacy and patient confidentiality that may limit the ability of physicians, health care providers, and persons acting on their behalf, to make use of certain confidential patient information ("Patient Information") and/or to transmit Patient Information to third parties without express consent. You represent and warrant that you will, at all times, comply with all laws directly or indirectly applicable to you that may now or hereafter govern the gathering, use, transmission, processing, receipt, reporting, disclosure, maintenance, and storage of the Patient Information, and require all persons or entities under your direction or control to comply with such laws, including the Privacy Laws, and the Privacy Policy. You are at all times solely responsible for obtaining and maintaining all patient consents, if applicable, and all other legally necessary consents or permissions required or advisable to disclose, process, retrieve, transmit, and view the Patient Information that you transmit, store, or receive in connection with the Site, Services, App and any third party site.<br>
We cannot and expressly do not assume any responsibility for your use or misuse of patient information or other information, whether intentional or inadvertent, that is transmitted, monitored, stored or received while using the site, services, app or collective content. We reserve the right to amend or delete any collective content (along with the right to revoke any membership or restrict access to the site, services, app or collective content) that we determine in our sole discretion violates the above. we further do not assume any responsibility to make any determinations regarding your subsequent reporting or notification obligations arising from any use or misuse of patient information or other information; these determinations and your actions in response to such determinations remain your sole responsibility.<br>
Your representations, warranties, and obligations in this section survive termination of these Terms.</p>

<h3>14. General Prohibitions</h3>

<p>You agree not to do any of the following:<br>
Post, upload, publish, submit or transmit or otherwise make available any Content that you do not have a right to make available under any law or under contractual or fiduciary relationships.</p>

<p>Post, upload, publish, submit or transmit any Content that: (i) infringes, misappropriates or violates a third party's patent, copyright, trademark, trade secret, moral rights or other intellectual property rights, or rights of publicity or privacy; (ii) violates, or encourages any conduct that would violate, any applicable law or regulation or would give rise to civil liability; (iii) is fraudulent, false, misleading or deceptive; (iv) is defamatory, obscene, pornographic, vulgar or offensive; (v) promotes discrimination, bigotry, racism, hatred, harassment or harm against any individual or group; (vi) is violent or threatening or promotes violence or actions that are threatening to any person or entity; or (vii) promotes illegal or harmful activities or substances.</p>

<p>Use, display, mirror or frame the Site or App, or any individual element within the Site, Services or App, The On Call Room Ltd. or The On Call Room (The OCR)'s names, any The On Call Room Ltd. trademark, logo or other proprietary information, or the layout and design of any page or form contained on a page, without The On Call Room Ltd.'s express written consent;</p>

<p>Access, tamper with, or use non-public areas of the Site or App, The On Call Room Ltd.'s computer systems, or the technical delivery systems of The On Call Room Ltd.'s providers;</p>

<p>Attempt to probe, scan, or test the vulnerability of any The On Call Room Ltd. system or network or breach any security or authentication measures;</p>

<p>Avoid, bypass, remove, deactivate, impair, descramble or otherwise circumvent any technological measure implemented by The On Call Room Ltd. or any of The On Call Room Ltd.'s providers or any other third party (including another user) to protect the Site, Services, App or Collective Content;</p>

<p>Attempt to access or search the Site, Services, App or Collective Content or download Collective Content from the Site, Services or App through the use of any engine, software, tool, agent, device or mechanism (including scripts, bots, spiders, scraper, crawlers, data mining tools or the like) other than the software and/or search agents provided by The On Call Room Ltd. or other generally available third party web browsers;</p>

<p>Send any unsolicited or unauthorized advertising, promotional materials, email, junk mail, spam, chain letters or other form of solicitation, from the App or otherwise;</p>

<p>Use any meta tags or other hidden text or metadata utilizing a The On Call Room Ltd. trademark, logo URL or product name without The On Call Room Ltd.'s express written consent;</p>

<p>Use the Site, Services, App or Collective Content for any commercial purpose or the benefit of any third party or in any manner not permitted by these Terms;</p>

<p>Forge any TCP/IP packet header or any part of the header information in any email or newsgroup posting, or in any way use the Site, Services, App or Collective Content to send altered, deceptive or false source-identifying information;</p>

<p>Attempt to decipher, decompile, disassemble or reverse engineer any of the software used to provide the Site, Services, App or Collective Content;</p>

<p>Interfere with, or attempt to interfere with, the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, or mail-bombing the Site, Services or App;</p>

<p>Collect or store any personally identifiable information from the Site, Services or App from other users of the Site, Services or App without their express permission;</p>

<p>Impersonate or misrepresent your affiliation with any person or entity;</p>

<p>Violate any applicable law or regulation; or</p>

<p>Encourage or enable any other individual to do any of the foregoing.</p>

<p>The On Call Room Ltd. will have the right to investigate and prosecute violations of any of the above to the fullest extent of the law. The On Call Room Ltd. may involve and cooperate with law enforcement authorities in prosecuting users who violate these Terms. You acknowledge that The On Call Room Ltd. has no obligation to monitor your access to or use of the Site, Services, App or Collective Content or to review or edit any Collective Content, but has the right to do so for the purpose of operating the Site, Services and App, to ensure your compliance with these Terms and the Privacy Policy, or to comply with applicable law or the order or requirement of a court, administrative agency or other governmental body having jurisdiction. The On Call Room Ltd. reserves the right, at any time and without prior notice, to remove or disable access to any Collective Content that The On Call Room Ltd., at its sole discretion, considers to be in violation of these Terms, the Privacy Policy, or otherwise harmful to the Site, Services or App.</p>

<h3>15. Copyright policy</h3>

<p>The On Call Room Ltd. respects copyright law and expects its users to do the same. It is The On Call Room Ltd.'s policy to terminate in appropriate circumstances Registered Users or other account holders who have infringed the rights of copyright holders or of whom The On Call Room Ltd. has reasonable grounds to believe that they have infringed the rights of copyright holders. Please see our copyright policy for further information.</p>

<h3>16. Links</h3>

<p>The Site, Services or App may contain links to third-party websites or resources which do not form part of the Site, Services or App. You acknowledge and agree that The On Call Room Ltd. is not responsible or liable for: (i) the availability or accuracy of such websites or resources; or (ii) the Content, products, or services on or available from such websites or resources for which such third party is solely responsible. Links to such websites or resources do not imply any endorsement by The On Call Room Ltd. of such websites or resources or the content, products, or services available from such websites or resources. You acknowledge sole responsibility for and assume all risk arising from your use of any such websites or resources or the Content, products, or services on or available from such websites or resources.</p>

<h3>17. Termination and Account Cancellation</h3>

<p>If you breach any of these Terms, The On Call Room Ltd. will have the right to suspend or disable your Account or terminate these Terms, at its sole discretion and without prior notice to you. The On Call Room Ltd. reserves the right to revoke your access to and use of the Site, Services, App and Collective Content at any time, with or without cause. You may cancel your Account at any time by contacting us. The change will be processed within seven (7) days.</p>

<h3>18. Disclaimers</h3>

<p>THE SITE, SERVICES, APP AND COLLECTIVE CONTENT ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED. WITHOUT LIMITING THE FOREGOING, THE ON CALL ROOM LTD. EXPLICITLY DISCLAIMS ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT, AND ANY WARRANTIES ARISING OUT OF COURSE OF DEALING OR USAGE OF TRADE. THE ON CALL ROOM LTD. MAKES NO WARRANTY THAT THE SITE, SERVICES, APP OR COLLECTIVE CONTENT WILL MEET YOUR REQUIREMENTS OR BE AVAILABLE ON AN UNINTERRUPTED, SECURE, OR ERROR-FREE BASIS. THE ON CALL ROOM LTD. MAKES NO WARRANTY REGARDING THE QUALITY OF ANY PRODUCTS, SERVICES OR COLLECTIVE CONTENT PURCHASED OR OBTAINED THROUGH THE SITE, SERVICES OR APP OR THE ACCURACY, TIMELINESS, TRUTHFULNESS, COMPLETENESS OR RELIABILITY OF ANY CONTENT OBTAINED THROUGH THE SITE, SERVICES OR APP.</p>

<p>YOU MAY NOTIFY THE APP VENDOR IN THE EVENT OF ANY FAILURE OF THE APP TO CONFORM TO THE ABOVE-STATED WARRANTY. THE APP VENDOR WILL REFUND THE PURCHASE PRICE OF THE THE ON CALL ROOM (THE OCR) APP TO YOU, BUT THE APP VENDOR WILL HAVE NO OTHER WARRANTY OBLIGATION WHATSOEVER WITH RESPECT TO THE THE ON CALL ROOM (THE OCR) APP.</p>

<p>NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED FROM THE ON CALL ROOM LTD. OR THROUGH THE SITE, SERVICES, APP OR COLLECTIVE CONTENT, WILL CREATE ANY WARRANTY NOT EXPRESSLY MADE HEREIN.</p>

<p>YOU ARE SOLELY RESPONSIBLE FOR ALL OF YOUR COMMUNICATIONS AND INTERACTIONS WITH OTHER USERS OF THE SITE, SERVICES AND APP AND WITH OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE, SERVICES OR APP. YOU UNDERSTAND THAT THE ON CALL ROOM LTD. DOES NOT TAKE RESPONSIBILITY FOR SCREENING OR INQUIRY INTO THE BACKGROUND OF ANY USERS OF THE SITE, SERVICES OR APP, NOR DOES THE ON CALL ROOM LTD. VERIFY OR TAKE RESPONSIBILITY FOR THE STATEMENTS OF USERS OF THE SITE, SERVICES OR APP. THE ON CALL ROOM LTD. MAKES NO REPRESENTATIONS OR WARRANTIES AS TO THE CONDUCT OF USERS OF THE SITE, SERVICES OR APP OR THEIR COMPATIBILITY WITH ANY CURRENT OR FUTURE USERS OF THE SITE, SERVICES OR APP. YOU AGREE TO TAKE REASONABLE PRECAUTIONS IN ALL COMMUNICATIONS AND INTERACTIONS WITH OTHER USERS OF THE SITE, SERVICES AND APP AND WITH OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE, SERVICES OR APP, PARTICULARLY IF YOU DECIDE TO MEET OFFLINE OR IN PERSON.</p>

<h3>19. Indemnity</h3>

<p>You agree to defend, indemnify, and hold The On Call Room Ltd., its officers, directors, employees and agents, harmless from and against any claims, liabilities, damages, losses, and expenses, proceedings or demands including, without limitation, reasonable legal and accounting fees, arising out of or in any way connected with your access to or use of the Site, Services, App or Collective Content, or your violation of these Terms.</p>

<h3>20. Limitation of Liability</h3>

<p>YOU ACKNOWLEDGE AND AGREE THAT, TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE ENTIRE RISK ARISING OUT OF YOUR ACCESS TO AND USE OF THE SITE, SERVICES, APP AND COLLECTIVE CONTENT REMAINS WITH YOU. NEITHER THE ON CALL ROOM LTD. NOR ANY OTHER PERSON OR ENTITY INVOLVED IN CREATING, PRODUCING, OR DELIVERING THE SITE, SERVICES, APP OR COLLECTIVE CONTENT WILL BE LIABLE FOR ANY INCIDENTAL, SPECIAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, LOSS OF DATA OR LOSS OF GOODWILL, SERVICE INTERRUPTION, COMPUTER DAMAGE OR SYSTEM FAILURE OR THE COST OF SUBSTITUTE PRODUCTS OR SERVICES, OR FOR ANY DAMAGES FOR PERSONAL OR BODILY INJURY OR EMOTIONAL DISTRESS ARISING OUT OF OR IN CONNECTION WITH THESE TERMS OR FROM THE USE OF OR INABILITY TO USE THE SITE, SERVICES, APP OR COLLECTIVE CONTENT, OR FROM ANY COMMUNICATIONS, INTERACTIONS OR MEETINGS WITH OTHER USERS OF THE SITE, SERVICES OR APP OR OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE, SERVICES OR APP WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT THE ON CALL ROOM LTD. HAS BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGE, EVEN IF A LIMITED REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED IN MEETING ITS ESSENTIAL PURPOSE.</p>

<p>IN NO EVENT WILL THE ON CALL ROOM LTD.'S AGGREGATE LIABILITY ARISING OUT OF OR IN CONNECTION WITH THESE TERMS OR FROM THE USE OF OR INABILITY TO USE THE SITE, SERVICES, APP OR COLLECTIVE CONTENT EXCEED ONE HUNDRED DOLLARS ($100). THE LIMITATIONS OF DAMAGES SET FORTH ABOVE ARE FUNDAMENTAL ELEMENTS OF THE BASIS OF THE BARGAIN BETWEEN THE ON CALL ROOM LTD. AND YOU.</p>

<h3>21. PROPRIETARY RIGHTS NOTICES</h3>

<p>All trademarks, service marks, logos, trade names and any other proprietary designations of The On Call Room Ltd. used herein are trademarks or registered trademarks of The On Call Room Ltd. Any other trademarks, service marks, logos, trade names and any other proprietary designations are the trademarks or registered trademarks of their respective parties.</p>

<h3>22. APPLICABLE LAW; JURISDICTION</h3>

<p>Except as provided in the addenda applicable to your specific jurisdiction provided with the Terms and except where mandatory law supersedes this choice of law clause, these Terms shall be governed by the laws of the Britain and the laws of Canada applicable therein. Subject to Section 27 requiring mandatory arbitration of disputes, the courts of the Britain shall have jurisdiction to entertain any action arising under these Terms or any other agreement, document or instrument contemplated herein which cannot be resolved by arbitration or where the enforcement of an arbitral award requires it, and you hereby accept and irrevocably submit to the jurisdiction of the courts of Ontario and acknowledge their competence and agree to be bound by any judgment thereof. Notwithstanding the governance of these Terms, you may have additional obligations and responsibilities to adhere to in the jurisdiction in which you practice medicine or another health profession.</p>

<h3>23. ENTIRE AGREEMENT</h3>

<p>These Terms constitute the entire and exclusive understanding and agreement between The On Call Room Ltd. and you regarding the Site, Services, App and Collective Content, and these Terms supersede and replace any and all prior oral or written understandings or agreements between The On Call Room Ltd. and you regarding the Site, Services, App and Collective Content.</p>

<h3>24. ASSIGNMENT</h3>

<p>You may not assign or transfer these Terms, by operation of law or otherwise, without The On Call Room Ltd.'s prior written consent. Any attempt by you to assign or transfer these Terms, without such consent, will be null and of no effect. The On Call Room Ltd. may assign or transfer these Terms, at its sole discretion, without restriction. Subject to the foregoing, these Terms will bind and inure to the benefit of the parties, their successors and permitted assigns.</p>

<h3>25. NOTICES</h3>

<p>Any notices or other communications permitted or required hereunder, including those regarding modifications to these Terms, will be in a written form and given: (i) by The On Call Room Ltd. via email (in each case to the address that you provide); or (ii) by posting to the Site; or (iii) via the App. For notices made by e-mail, the date of receipt will be deemed the date on which such notice is transmitted.</p>

<h3>26. DISPUTE RESOLUTION</h3>

<p>All disputes between you and The On Call Room (The OCR) arising out of or concerning these Terms, including but without limitation any dispute as to the interpretation of these Terms, any alleged breach of these Terms, the right of a party to exercise any right or remedy herein provided which have not been settled by informal negotiations or mediation, shall be resolved by binding arbitration in accordance with the Arbitrations Act, 1991 (Ontario) and the following provisions:</p>

<p>a) The arbitration process shall be commenced by one party to the dispute providing a written notice to the other parties to the effect that the notifying party wishes to have the dispute resolved by binding arbitration and appointing by such notice one disinterested arbitrator.</p>

<p>b) Within ten (10) Business Days following receipt of such written notice, the other parties to the dispute shall by written notice to the party that initiated the notice appoint a second disinterested arbitrator, and if such appointment shall not be so made, the first arbitrator appointed shall act as the sole arbitrator with respect to the dispute and all references hereinafter to the arbitrators or to the arbitration panel shall be treated as being references to that single arbitrator.</p>

<p>c) Within ten (10) Business Days following the appointment of the second arbitrator, the two (2) arbitrators shall appoint a third disinterested arbitrator. If the two (2) arbitrators for any reason fail to make such appointment, either party to the dispute shall be at liberty to apply to any judge of the Superior Court of Ontario for an order appointing the third arbitrator, provided that the other parties to the dispute are given not less than five (5) days' prior written notice of that application and is permitted to attend and speak to the Court at the hearing of that application.</p>

<p>d) The three (3) arbitrators so appointed shall comprise the arbitration panel. The arbitrator last appointed shall act as the chair of the arbitration panel and as such shall regulate the conduct of the arbitration hearing and all procedural and other analogous matters.</p>

<p>e) The arbitration panel shall authorize such discoveries or motions with respect to the arbitration hearing as the panel believes are necessary to ensure a fair hearing.</p>

<p>f) The arbitration panel shall not be bound by the rules of evidence or of civil procedure, but rather may consider such writings and oral presentations as reasonable business people would use in the conduct of their day-to-day affairs, and may require the parties to make some or all of their submissions in writing or in any other manner which the arbitrator considers to be appropriate. The Parties intend to limit live testimony and cross-examination to the extent necessary to ensure a fair hearing on material issues.</p>

<p>g) The arbitration panel shall take such steps as may be necessary to hold a private hearing within forty-five (45) days of the date of the appointment of the third arbitrator and to conclude the hearing within five (5) Business Days; and the arbitration panel's written decision shall be made not later than ten (10) Business Days after the conclusion of the hearing. The Parties have included these time limits in order to expedite the proceeding, but they are not jurisdictional and the arbitration panel may for good cause afford or permit reasonable extensions or delays, which shall not affect the validity of the award. The written decision of the panel shall contain a brief statement of each dispute determined, the decision of the panel with respect to such dispute, and the reasons for such decision. Absent fraud, financial interest, collusion or wilful misconduct of an arbitrator, the arbitration panel's decisions in the disputes shall be final.</p>

<p>h) The decision of any two (2) members of the arbitration panel in any matter pertaining to the arbitration shall, in the absence of fraud, financial interest, collusion or wilful misconduct, be and be deemed to be the decision of the arbitration panel.</p>

<p>i) The chair of the arbitration panel may retain counsel to advise the chair and the panel with respect to any issues of law involved in the determinations of the chair and the panel, and the reasonable fees and expenses of such counsel for his or her services in that regard shall form a part of the costs of the arbitration.</p>

<p>j) Each member of the arbitration panel shall be paid his or her reasonable professional fees and disbursements for acting as such. Each party to the dispute shall be solely liable to pay the fees and disbursements of the arbitrator appointed by that party. The parties to the dispute shall be jointly and severally liable to the third arbitrator to pay that arbitrator's fees and disbursements, and any award of costs that the arbitration panel may make shall not relieve the party in whose favour such award is made from its joint and several liability with the other party to the dispute to pay such fees and disbursements. Subject to any award that the arbitration panel may make, the parties to the dispute shall as between themselves each shall bear one-half of the fees and disbursements charged by the third arbitrator for his or her services in respect of the arbitration.</p>

<p>k) The arbitration panel may, but shall not be obliged, to award to the party to the dispute whom the panel decides has achieved substantial success in the arbitration proceedings all or any part of the legal fees, arbitrators' fees and costs and other costs incurred by that party with respect to the arbitration.</p>

<p>l) The Arbitration shall be conducted in strict confidence and there shall be no disclosure to any person (other than necessary to carry out the arbitration) of the existence of the dispute or any aspect of the dispute.</p>

<h3>27. GENERAL</h3>

<p>The failure of The On Call Room Ltd. to enforce any right or provision of these Terms will not constitute a waiver of future enforcement of that right or provision. The waiver of any such right or provision will be effective only if in writing and signed by a duly authorized representative of The On Call Room Ltd. Except as expressly set forth in these Terms, the exercise by either party of any of its remedies under these Terms will be without prejudice to its other remedies under these Terms or otherwise. If for any reason a court of competent jurisdiction finds any provision of these Terms invalid or unenforceable, that provision will be enforced to the maximum extent permissible and the other provisions of these Terms will remain in full force and effect.</p>

<h3>28. CONTACTING THE ON CALL ROOM LTD.</h3>

<p>These Services are operated by The On Call Room Ltd. If you have any questions about these Terms, please contact us at:<br>
The On Call Room Ltd.<br>
Oakdale, Royal Oak Hill,<br>
Newport, UK<br>
NP18 1JF</p>

<h3>29. MISCELLANEOUS</h3>

<p>You acknowledge that the Terms apply to you and The On Call Room Ltd. only, and not with any App Vendor. The App Vendor and any App Vendor subsidiaries are third party beneficiaries of the Terms. Upon your acceptance of the Terms, the App Vendor will have the right (and will be deemed to have accepted the right) to enforce the Terms against you as a third party beneficiary.<br>
You may not use or otherwise export or re-export the App except as authorized by both United States law and the laws of the jurisdiction in which you obtained The On Call Room (The OCR). In particular, but without limitation, the App may not be exported or re-exported (a) into any U.S.-embargoed countries or (b) to anyone on the U.S. Treasury Department's Specially Designated Nationals List or the U.S. Department of Commerce Denied Persons List or Entity List. By using the App, you represent and warrant that you are not located in any such country or on any such list. You also agree that you will not use these products for any purposes prohibited by United States law, including, without limitation, the development, design, manufacture, or production of nuclear, missile, or chemical or biological weapons.</p>

<h3>FOR USERS IN AUSTRALIA AND NEW ZEALAND</h3>

<p>Notwithstanding the governance of the Terms and this Addendum, you may have additional obligations and responsibilities to adhere to in the jurisdiction in which you practice medicine or another health profession.</p>

<h3>1. ADDITIONAL DISCLAIMERS</h3>

<p>YOUR RELIANCE UPON THE CONTENT OBTAINED OR USED BY YOU THROUGH THE SITE, SERVICES OR APP IS SOLELY AT YOUR OWN RISK.<br>
THE SITE, SERVICES, APP AND COLLECTIVE CONTENT ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED. WITHOUT LIMITING THE FOREGOING, THE ON CALL ROOM LTD. EXPLICITLY DISCLAIMS ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT, AND ANY WARRANTIES ARISING OUT OF COURSE OF DEALING OR USAGE OF TRADE. THE ON CALL ROOM LTD. MAKES NO WARRANTY THAT THE SITE, SERVICES, APP OR COLLECTIVE CONTENT WILL MEET YOUR REQUIREMENTS OR BE AVAILABLE ON AN UNINTERRUPTED, SECURE, OR ERROR-FREE BASIS. THE ON CALL ROOM LTD. MAKES NO WARRANTY REGARDING THE QUALITY OF ANY PRODUCTS, SERVICES OR COLLECTIVE CONTENT PURCHASED OR OBTAINED THROUGH THE SITE, SERVICES OR APP OR THE ACCURACY, TIMELINESS, TRUTHFULNESS, COMPLETENESS OR RELIABILITY OF ANY CONTENT OBTAINED THROUGH THE SITE, SERVICES OR APP.</p>

<p>YOU MAY NOTIFY THE APP VENDOR IN THE EVENT OF ANY FAILURE OF THE APP TO CONFORM TO THE ABOVE-STATED WARRANTY. THE APP VENDOR WILL REFUND THE PURCHASE PRICE OF THE THE ON CALL ROOM (THE OCR) APP TO YOU, BUT THE APP VENDOR WILL HAVE NO OTHER WARRANTY OBLIGATION WHATSOEVER WITH RESPECT TO THE THE ON CALL ROOM (THE OCR) APP.</p>

<p>NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED FROM THE ON CALL ROOM LTD. OR THROUGH THE SITE, SERVICES, APP OR COLLECTIVE CONTENT, WILL CREATE ANY WARRANTY NOT EXPRESSLY MADE HEREIN.</p>

<p>YOU ARE SOLELY RESPONSIBLE FOR ALL OF YOUR COMMUNICATIONS AND INTERACTIONS WITH OTHER USERS OF THE SITE, SERVICES AND APP AND WITH OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE, SERVICES OR APP. YOU UNDERSTAND THAT THE ON CALL ROOM LTD. DOES NOT TAKE RESPONSIBILITY FOR SCREENING OR INQUIRY INTO THE BACKGROUND OF ANY USERS OF THE SITE, SERVICES OR APP, NOR DOES THE ON CALL ROOM LTD. VERIFY OR TAKE RESPONSIBILITY FOR THE STATEMENTS OF USERS OF THE SITE, SERVICES OR APP. THE ON CALL ROOM LTD. MAKES NO REPRESENTATIONS OR WARRANTIES AS TO THE CONDUCT OF USERS OF THE SITE, SERVICES OR APP OR THEIR COMPATIBILITY WITH ANY CURRENT OR FUTURE USERS OF THE SITE, SERVICES OR APP. YOU AGREE TO TAKE REASONABLE PRECAUTIONS IN ALL COMMUNICATIONS AND INTERACTIONS WITH OTHER USERS OF THE SITE, SERVICES AND APP AND WITH OTHER PERSONS WITH WHOM YOU COMMUNICATE OR INTERACT AS A RESULT OF YOUR USE OF THE SITE, SERVICES OR APP, PARTICULARLY IF YOU DECIDE TO MEET OFFLINE OR IN PERSON.</p>

<h3>2. ADDITIONAL LIMITATIONS OF LIABILITY</h3>

<p>IN ADDITION TO THE LIMITATION OF LIABILITY TERMS IN SECTION 20 TO THE TERMS, YOU ACKNOWLEDGE AND AGREE THAT, TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE ENTIRE RISK ARISING OUT OF YOUR ACCESS TO AND USE OF THE SITE, SERVICES, APP AND COLLECTIVE CONTENT REMAINS WITH YOU AND THAT ALL EXPRESS OR IMPLIED WARRANTIES, REPRESENTATIONS, STATEMENTS, TERMS AND CONDITIONS RELATING TO THESE TERMS THAT ARE NOT INCLUDED IN THESE TERMS ARE EXCLUDED.</p>

<p>FOR SERVICES, SITE OR APPS ACCESSED IN AUSTRALIA, NOTHING IN THESE TERMS EXCLUDES, RESTRICTS OR MODIFIES ANY GUARANTEE, TERM, CONDITION, WARRANTY, RIGHT OR REMEDY IMPLIED OR IMPOSED BY ANY STATUTE OR REGULATION WHICH CANNOT LAWFULLY BE EXCLUDED OR LIMITED, WHICH MAY INCLUDE THE AUSTRALIAN CONSUMER LAW.</p>

<p>FOR SERVICES, SITE OR APPS ACCESSED IN AUSTRALIA, IF ANY GUARANTEE, TERM, CONDITION OR WARRANTY IS IMPLIED OR IMPOSED IN RELATION TO THESE TERMS (A NON-EXCLUDABLE PROVISION) AND WE ARE ABLE TO LIMIT YOUR REMEDY FOR A BREACH OF SUCH NON-EXCLUDABLE PROVISION, THEN OUR TOTAL LIABILITY FOR BREACH OF THE NON-EXCLUDABLE PROVISION IS LIMITED TO ONE OR MORE OF THE FOLLOWING AT OUR OPTION (i) IN THE CASE OF THE APP, THE REPLACEMENT OF THE APP OR THE SUPPLY OF AN EQUIVALENT APPLICATION, OR THE PAYMENT OF THE COST OF REPLACING THE APP OR OF AN EQUIVALENT APP OR THE COST OF THE REPAIR OF THE APP (ii) IN THE CASE OF THE SERVICES, THE SUPPLYING OF THE SERVICES AGAIN, OR THE COST OF HAVING THE SERVICE SUPPLIED AGAIN.</p>

<h3>3. APPLICABLE LAW</h3>

<p>These Terms shall be governed by the laws of the Britain and the laws of Canada. However, you additionally represent and warrant that you have all rights, licenses, consents and releases that are necessary to grant to The On Call Room Ltd. the rights in such Registered User Content, as contemplated under these Terms and that your Registered User Content, by means of using the Site, Services or App does not violate the Australian Privacy Act 1988 (Cth), the New Zealand Privacy Act 1993, and any other Privacy Laws as stated in Section 10 of the Terms, to the extent they apply.</p>

<p>If you accessed the Services, Site, or App in Australia, The On Call Room (The OCR) shall consent to the application of Australian law in the event of a dispute regarding the application of the Terms if and only if a court in Ontario determines that not applying Australian law (i) creates a significant imbalance in the parties' rights; (ii) is not reasonably necessary to protect The On Call Room (The OCR)'s legitimate interests; and (iii) would cause detriment to the consumer. Under such a determination by a court in Ontario, any action arising under these Terms or any other agreement, document or instrument contemplated herein, shall be adjudicated under Australian law, and you hereby accept and irrevocably submit to the jurisdiction of the courts of New South Wales and acknowledge their competence and agree to be bound by any judgment thereof.</p>

<h3>4. DISPUTE RESOLUTION</h3>

<p>In the event that the parties consent to arbitration, the Dispute Resolution provisions in Section 26 of the Terms still apply, but shall allow for the use of arbitrators based in Australia (or South Africa for South African Users) rather than those based in London, as well as a Court of Australia (or South Africa for South African Users) in place of the Superior Court of UK.</p>
</div>
 </div>
      </div>
    </div>
  </div>
</section> 



