<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
         <div class="bs-example">
            <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                        How much must one pay for the On Call Room (The OCR) facilities?</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>The OCR is available for free download from the Google Play Store and App Store. One does not need to pay any amount of money for The OCR facilities. </p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                        Where can one enjoy the benefits of The On Call Room? 
                    </a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>The app is available in all of North America, across Europe, Australia, New Zealand, South Africa, India, Sri Lanka, Canada, Singapore and Dubai. While we regret the inconvenience this geographical restriction causes, we are working hard to ensure global availability. </p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                        Who can sign up and how secure is the community?</a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>We welcome all healthcare professionals to become a part of our medical community. The OCR team realized that it may be uncomfortable for healthcare workers to discuss professional issues on regular social media platforms, open to prying and panicking eyes. Thus, we started our own platform where medical professionals could safely and securely interact with each other. We offer the very best of protection to our community, with multilayer encryptions and identification verifications. </p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                        How do we tackle false identities and impersonation? </a>
                </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Our first defence against prying infiltrators is our stringent terms and conditions of service, which we strongly advise all our users to read. While we offer anonymity to our users, we use multilayer encryption and other secure SSL technology to ensure that there is no misuse taking place. Additionally, members are not allowed to take a meaningful part in the community till their personal details have been cross-referenced and verified. The safety and security of our members and their privacy is of the utmost importance to us, and we constantly update our security protocols to ensure we meet changing security concerns.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                        What does The On Call Room do to ensure patient privacy?</a>
                </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>We know that patient privacy is a priority for healthcare professionals, and we have designed The OCR with this core to our technology.</p>

<p>Doctor-patient confidentiality is scared. While The OCR does allow members to upload files to make it easier to discuss cases with other medical professionals, there are checks and counter checks in place to ensure that the patient’s privacy and safety are not violated. Our terms of service forbid users from posting personal details about their patients. Thus, we forbid members from posting identifiable details that would uniquely mark patients, such as birthmarks or tattoos. We make compliance with our terms easier by providing tools which make compliance easier. </p>

<p>While we do not monitor every file being uploaded, we trust our members to report the mistakes that may happen during such an upload. Any files which contain a patients personal details may be easily reported by other members. Our team then reviews the post. If violations occur, the post is immediately taken down, and the user is informed of the violation. </p>
<p>Since we ensure that all details of patients are not available, the posts do not fall under regulations such as the HIPAA’s Privacy Rule in the United States, privacy legislation in Canada (such as PHIPA in Ontario) or any such legislation on other jurisdiction around the world. </p>
<p>For more information, please visit our <a href="https://theoncallroom.com/terms-and-conditions" target="_blank">Terms of Service</a> and our <a href="https://theoncallroom.com/privacy-policy" target="_blank">Privacy Policy</a> sections.</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                        What about patient consent?</a>
                </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Many medical facilities require that patients give consent before photographs are taken of them. If the facility you are associated with does not, and you feel you need your patients consent, The OCR has in-app consent forms available for your convenience. The patient can sign the form on your phone itself, or can authorise a representative to do it for him or her. The completed consent form will then be forwarded to your registered email address by The OCR team.</p>

<p>Consent forms provided by The OCR team is compliant with laws in most countries. Certain states, provinces or localities may have special laws in place which we may have missed out on. For more information on patient consent, please visit our <a href="https://theoncallroom.com/terms-and-conditions" target="_blank">Terms of Service</a> and <a href="https://theoncallroom.com/privacy-policy" target="_blank">Privacy Policy</a>. </p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                        Can The OCR be useful even if case details cannot be shared?</a>
                </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>The medical facility that you are attached to or the patient may not allow you to share the case details that you want to discuss. Thankfully, this does not mean that The OCR will be rendered entirely useless for you. You are encouraged to be an active member of our medical community in all other regards. </p>

<p>The community thrives on everything related to healthcare. Thus, you could keep abreast of the latest news articles, journal publications, new technologies, and breakthroughs. The OCR is a modern medical and professional education tool, for the modern medical professional. Case discussions are only one part of the community experience. You can easily add to our collaborative library of healthcare information and receive feedback for what you add to The OCR community. </p>
<p>We would like to take this opportunity to encourage you to explore the world of healthcare from the different perspectives of our many different users. We look forward to your participation in our community! </p>
                </div>
            </div>
        </div>
    </div>
</div>
         </div>
      </div>
    </div>
  </div>
</section>
