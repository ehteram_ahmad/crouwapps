<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
              <p>The OCR is made by Medic Creations Ltd. As a company we dream of ways to improve the healthcare system with technology and make products that will transform the lives of healthcare professionals and their patients.</p>
<p>Our other products include:</p>
              <p><strong>Medic Bleep:</strong>
Helps medical teams communicate more effectively. Our secure platform helps users instantly send updates to the rest of their team, safely share case files and collaborate on treatments. Our messaging system is fully compliant with Information Governance and ISO 27001 so you can use it safe in the knowledge it’ll work safely and securely every time.</p>
<!--<p><a href="#">Discover Medic Bleep Now</a></p><br />-->
            <p><strong>Your Health Room:</strong>
Helps medics answer questions and share their knowledge with the public to grow their client base, build their brand and position themselves as an expert in their field. We have two editions of the Your Health Room app, one that the general public can use to post questions with, and the other for professionals to answer the questions and showcase their authority with.</p>
<!--<p><a href="#">Discover Your Health Room Now</a></p>-->
    </div>
      </div>
    </div>
  </div>
</section>