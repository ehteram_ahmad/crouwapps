<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
              <p class="marTop20"><strong>How much does The On Call Room cost?</strong></p>
              <p>The On Call Room app is completely free. You can download it at no cost from the <a title="Download on the App Store" target="_blank" href="https://itunes.apple.com/us/app/the-on-call-room-ocr/id975310441?ls=1&amp;mt=8" onclick="ga('send', 'event', 'app', 'download','appstore');">iOS app store</a> or <a title="Get it on Google play" target="_blank" href="https://play.google.com/store/apps/details?id=com.ocr.theoncallroom&amp;hl=en" onclick="ga('send', 'event', 'app', 'download','googleplay');">Google play store</a></p>
              <p class="marTop20"><strong>What countries is The On Call Room app available in?</strong></p>
              <p>At the moment our app is available in the United States, Europe, Canada, Australia, New Zealand, India, South Africa, Sri Lanka, Singapore &amp; Dubai. If your country isn’t on the list, don’t worry we’re working on being with you as soon as possible.</p>
              <p class="marTop20"><strong>Who can sign up for The OCR? Is it Secure?</strong></p>
              <p>Absolutely. Our network is only available to fully vetted medical professionals and students. If you can’t prove you’re a medic, you can’t use our app. This, alongside our multi-layer encryption technology means that using the app is safe, secure and exclusive.</p>
              <p class="marTop20"><strong>How do we tackle identity fraud?</strong></p>
              <p>We know you’ll be posting sensitive data and the information you’re seeking is highly specialized, so we only want serious medical professionals to use the app. When a user signs up we map their profile against our database of registered professionals. You may also be asked to further verify your identity at any time.</p>
              <p class="marTop20"><strong>How do you ensure patient privacy?</strong></p>
              <p>We have a number of ways of making sure that patient privacy remains forefront of the On Call Room experience.</p>
              <p>Our terms of service clearly state that no identifying details about the patient should be published (e.g. name, D.O.B, social security number, patient number etc).</p>
              <p>We have included photo editing tools to make sure that any identifying marks can be quickly and easily removed from any content you upload.</p>
              <p>Whilst we can’t manually check every single post on The On Call Room ourselves, we do rely on users flagging any inappropriate material.</p>
              <p class="marTop20"><strong>How do you handle patient consent?</strong></p>
              <p>We have consent form functionality built into the app, so doctors can upload permission from patients to share (non-identifiable) case details in The On Call Room.</p>
              <p class="marTop20"><strong>What about if I can’t share case details?</strong></p>
              <p>There is still plenty you can do to be a part of The On Call Room community. You can share and comment on journal articles or offer support to fellow professionals by offering them advice or a second opinion. Being able to post your own case details really is only a small part of the experience.</p>
    </div>
      </div>
    </div>
  </div>
</section>