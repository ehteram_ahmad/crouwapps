<?php
 //echo isset($pageContent['Content']['content_text']) ? $pageContent['Content']['content_text'] : "";
?>

<!--Pulse App-->

<section class="pulse-app" id="device-section">
    <!--Pulse App-->
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="device">
            <div class="bannerLeft">
            <!--<h2 class="greenBold">The The OCR is going through an update and will be back in just a few days!</h2>
              <div class="centerAlign"> <img class="logoBig" src="images/logo-big.png" alt="logo"></div>-->
              <h2>Make a Lasting Impact on Medicine</h2>
              <div class="app-on-stores">
                <ul>
                  <li><a title="Download on the App Store" target="_blank" href="https://itunes.apple.com/us/app/the-on-call-room-ocr/id975310441?ls=1&amp;mt=8" onclick="ga('send', 'event', 'app', 'download','appstore');">
                    <?php echo $this->Html->image('app-store.png', array("alt"=> "Download on the App Store", "title" => "Download on the App Store", "height"=> "70",  "border"=> "0", "width"=> "229")); ?>
                  </a></li>
                  <li><a title="Get it on Google play" target="_blank" href="https://play.google.com/store/apps/details?id=com.ocr.theoncallroom&amp;hl=en" onclick="ga('send', 'event', 'app', 'download','googleplay');">
                    <?php echo $this->Html->image('google-play.png', array("alt"=> "Get it on Google play", "title"=> "Get it on Google play", "height"=> "70", "width"=>"229")); ?>
                  </a></li>
                </ul>
              </div>
              <p class="centerAlign">Share, Learn & Collaborate with Medical Professionals Around the World to Help Improve Patient Care.
</p>
              <h2 class="greenBold">Start Making Your Impact Today. Download The OCR Now</h2>
              
              <p>GET THE APP DOWNLOAD LINK ON YOUR EMAIL</p>
<div class="input-group">
                <div id="mc_embed_signup">
                  <form action="//mediccreations.us12.list-manage.com/subscribe/post?u=4f8f2caf54859b1e05155fb68&amp;id=37bc56d726" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    &nbsp; &nbsp;
                    <div id="mc_embed_signup_scroll">
                      <div class="clear">
                        <input value="Send" name="subscribe" id="mc-embedded-subscribe" class="button" type="submit">
                      </div>
                      <div class="mc-field-group">
                        <input aria-required="true" placeholder="Email" name="EMAIL" class="required email" id="mce-EMAIL" type="email">
                      </div>
                      <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                      </div>
                      &nbsp; &nbsp;
                      &nbsp; &nbsp;
                      <div style="position: absolute; left: -5000px;" aria-hidden="true">
                        <input name="b_4f8f2caf54859b1e05155fb68_37bc56d726" tabindex="-1" type="text">
                      </div>
                      &nbsp; &nbsp;
                      
                      &nbsp; &nbsp; </div>
                  </form>
                </div>
                <script type="text/javascript" src="js/mc-validate.js"></script> 
                <script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script> 
              </div>
            </div>
            <div class="slide-image">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!--//Pulse App-->
<!--//Value Proposition-->
<section class="product-section padTop100" id="valueProposition">
  <div class="down-arrow">
  <a href="#valueProposition" class="page-scroll" title="Scroll Down">arrow</a>
  </div>
  <div class="container">
    <div class="heading-block center">
      <!--<h2>Value Proposition</h2>-->
      <span class="divcenter">Doctors, nurses, consultants, pharmacists, dentists &amp; Students from around the world are connecting to solve problems, learn new techniques, offer advice and fast track difficult diagnoses.</span> 
    </div>
    <div class="container clearfix">
      <div class="navmenu">
        <ul class="nav  clearfix tnav" id="nav_main4">
          <li><a>Securely Share Patient Sensitive Images</a>
          <p class="centerAlign">Our unique photo sharing tools help you quickly and securely get a second opinion without breaking patient confidentiality rules.</p>
          </li>
          <li><a>Share Knowledge with Professionals</a>
          <p class="centerAlign">We pre-approve everyone before they join The OCR to make absolutely certain that only registered, trained professionals are part of our exclusive community.</p>
          </li>
          <li><a>Collaborate with Medics Everywhere</a>
          <p class="centerAlign">Communicate with other members of your medical team as well as other teams across the globe to uncover Pain points and discover new best Practises you can adopt yourself.</p>
          </li>
        </ul>
      </div>
      <div class="row bulletList">
          <div class="col-sm-6">
            <ul class="item-list-right item-list-big">
              <li>
                <p>Share and discover content and learn new insights.</p>
              </li>
              <li>
                <p>Improve patient care &amp; encourage new ways of working.</p>
              </li>
              <li>
                <p>A portfolio of cases to reference at a glance.</p>
              </li>
              <li>
                <p>A living, breathing textbook based on real life experiences.</p>
              </li>
            </ul>
          </div>
          <div class="col-sm-6">
            <ul class="item-list-left item-list-big">
              <li> 
                <p>Quickly disseminate the latest guidelines and best practices.</p>
              </li>
              <li>
                <p>Learn online &amp; attend grand rounds from anywhere in the world.</p>
              </li>
              <li>
                <p>Audit and support the research work others are doing in your field.</p>
              </li>
              <li>
                <p>Discuss new medical findings &amp; offer your own take on discoveries.</p>
              </li>
            </ul>
          </div>
          
        </div>
    </div>
    
  </div>
</section>
<!--//Video-->
<section class="product-section pulse-app2 padTop100" id="videoMain">
  <div class="down-arrow"><a href="#videoMain" class="page-scroll" title="Scroll Down">arrow</a></div>
  <div class="section nomargin">
    <div class="heading-block center">
      <h2>The Future of Medicine</h2>
      <span>Discover how The OCR is empowering medical professionals worldwide</span>
    </div>
    <div class="container clearfix">
      <div class="navmenu">
        <ul class="nav  clearfix tnav" id="nav_main5">
          <li>
            <video poster="img/poster.png" controls>
              <source src="videos/TheOnCallRoom.mp4" type="video/mp4" />
              <source src="videos/TheOnCallRoom.webm" type="video/mp4" />
              <source src="videos/TheOnCallRoom.ogg" type="video/ogg" />
            Your browser does not support the video tag.
            </video> 
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--//The OCR App-->
<section class="product-section pulse-app2" id="app">
<!--Product Details-->
  <div class="down-arrow"><a title="Scroll Down" class="page-scroll" href="#app">arrow</a></div>
  <div class="container">
    <div class="row tab-content" id="medicBleep2">
      <div class="container">
        <div class="heading-block center">
          <h2>Why Use The OCR?</h2>
          </div>
        <div class="row">
          <div class="col-sm-4 marTop40">
            <ul class="item-list-right item-list-big">
              <li class="wow fadeInLeft">
                <h3>Learn From Experience</h3>
                <p>Tap into a ready made network of highly experienced medical practitioners. Learn from thousands of years of cumulative knowledge and contribute your own expertise too.</p>
              </li>
              <li class="wow fadeInLeft">
                <h3>Get Practical Advice</h3>
                <p>Need a second opinion? Want to run something past a specialist? With The OCR the world’s smartest medical minds are at your fingertips.</p>
              </li>
              <li class="wow fadeInLeft">
                <h3>Global Support Network</h3>
                <p>The OCR is a ready made support network of professionals who’ve had nightmare shifts just like yours  and can help you decompress after a difficult day.</p>
              </li>
            </ul>
          </div>
          <div class="col-sm-4 col-sm-push-4 marTop40">
            <ul class="item-list-left item-list-big">
              <li class="wow fadeInRight">
                <h3>Securely Share Information</h3>
                <p>Our photo editing tools help you easily share images and quickly remove sensitive data, so you can seek a second opinion without breaching patient confidentiality</p>
              </li>
              <li class="wow fadeInRight">
                <h3>Fast Track Medical Breakthroughs</h3>
                <p>New discoveries and insights Can quickly gain traction thanks to our 'upbeat' voting feature. So you’ll always be at the bleeding edge of medical advancements</p>
              </li>
              <li class="wow fadeInRight">
                <h3>Exclusive Journal Content</h3>
                <p>We’ve teamed up with some of the world’s leading medical journals to provide you exclusive sneak peeks at journal content before anyone else.</p>
              </li>
            </ul>
          </div>
          <div class="col-sm-4 col-sm-pull-4">
            <div class="animation-box wow bounceIn"> 
              <?php echo $this->Html->image("light.png", array("class"=> "highlight-left wow", "alt"=> "", "height"=> "192", "width"=>"48")); ?>
              <?php echo $this->Html->image("light.png", array("class"=>"highlight-right wow", "src"=> "images/light.png", "alt"=> "", "height"=> "192", "width"=> "48")); ?>
              <a href="http://theoncallroom.com/" target="_blank">
              <?php echo $this->Html->image("feature-ocr1.png", array("class"=>"screen", "alt"=>"", "height"=>"581", "width"=>"300")); ?>
            </a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--//How it Works-->
  </section>
<section class="product-section padTop100 pulse-app2" id="howWorks">
  <div class="down-arrow">
  <a href="#howWorks" class="page-scroll" title="Scroll Down">arrow</a>
  </div>
  <div class="container">
    <div class="heading-block center">
      <h2>How it Works</h2>
      <span class="divcenter">Joining The OCR is quick, easy and painless. Our simple 3 step process takes less than 3 minutes.</span> 
    </div>
    <div class="container clearfix">
      <div class="navmenu">
        <ul class="nav  clearfix tnav" id="nav_main6">
          <li><a>Download The OCR App</a>
          <p class="centerAlign">Simply visit the app store for iPhone or the Google play store for android and download The OCR app onto your phone for free.</p>
          </li>
          <li><a>We'll Verify Your Identity</a>
          <p class="centerAlign">To make sure only registered medical professionals use The OCR, we’ll check your name against medical practitioner databases.</p>
          </li>
          <li><a>Medical Professionals</a>
          <p class="centerAlign">Once you’re signed up, you can use The OCR straight away and start making your impact on medicine.</p>
          </li>
        </ul>
      </div>
      
    </div>
    
  </div>
</section>

<!--//It’s not all clinical…-->
<section class="product-section pulse-app2 padTop100" id="clinical">
  <div class="down-arrow"><a href="#clinical" class="page-scroll" title="Scroll Down">arrow</a></div>
  <div class="section why why-desktop">
<div class="container row-pad">
<div class="heading-block center">
      <h2>It's Not All Clinical...</h2>
      <span class="divcenter">Working in healthcare can be tough. Sometimes you just need a laugh after a hard day, or someone to talk to and get advice from. The On Call Room has got your back.</span> 
    </div>
<div class="row screenDesktop">
<div class="col-md-6 col-sm-6 info-box animated fadeInLeft">


<div class="clearfix">
 <div class="mobileOverlay">
<div class="mobiledata">
<div class="slide1">
  <?php echo $this->Html->image('clinical01.png', array("alt"=> "clinical01")); ?>
</div> 
<div class="slide2">
  <?php echo $this->Html->image('clinical02.png', array("alt"=> "clinical02")); ?>
</div> 
<div class="slide3">
  <?php echo $this->Html->image('clinical03.png', array("alt"=> "clinical03")); ?>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-6 col-sm-6 info-box wp12 delay-04s animated fadeInRight marTop120">
    <div class="small-section sec1 Scolor" data="0">
<h3>Laughter is the Best Medicine</h3>
<p>Watch funny videos, share medical memes, have a laugh and unwind.</p>
        </div>
    <div class="small-section sec2" data="1">
<h3>A Problem Shared…</h3>
<p>Got a problem? Need some advice? Anonymously ask your fellow medics for their input.</p>
        </div>
    <div class="small-section  sec3" data="2">
<h3>Healthcare News</h3>
<p>Keep up to date with the latest and join in the discussion with your fellow professionals.</p>
        </div>
    
</div>
</div>
<div class="clear"></div>
<div id="carousel-example-generic" class="carousel slide screenMobile" data-ride="carousel">

   <ol class="carousel-indicators">
       <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
       <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
       <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
   </ol>


   <div class="carousel-inner" role="listbox">
       <div class="item active">
         <?php echo $this->Html->image('clinical01.png', array("alt"=>"...")); ?>
         <div class="carousel-caption">
            <div class="small-section-mobile sec1 Scolor" data="0">
                <h3>Laughter is the Best Medicine</h3>
                <p>Watch funny videos, share medical memes, have a laugh and unwind.</p>
            </div>
        </div>
    </div>
    <div class="item">
     <?php echo $this->Html->image('clinical02.png', array("alt"=>"...")); ?>
     <div class="carousel-caption">
      <div class="small-section-mobile sec1 Scolor" data="0">
        <h3>A Problem Shared…</h3>
        <p>Got a problem? Need some advice? Anonymously ask your fellow medics for their input.</p>
    </div>
</div>
</div>
<div class="item">
 <?php echo $this->Html->image('clinical03.png', array("alt"=>"...")); ?>
 <div class="carousel-caption">
  <div class="small-section-mobile sec1 Scolor" data="0">
    <h3>Healthcare News</h3>
    <p>Keep up to date with the latest and join in the discussion with your fellow professionals.</p>
</div>
</div>
</div>
</div>
</div>

<!-- Controls -->

</div>
</div>
<!-- Fun Side -->
<!--<div class="clear"></div>
<div class="marAuto width30">
        <ul class="nav clearfix tnav" id="nav_main7">
          <li><a></a></li>
          <li><a></a></li>
        </ul>
        </div>
<div class="marAuto width51">
        <ul class="nav clearfix tnav" id="nav_main8">
          <li><a></a></li>
          <li><a></a></li>
        </ul>
        </div>-->
</section>

<!--//Everyone Loves The OCR-->
<section class="product-section pulse-app2 padTop100" id="ourPartners">
  <div class="down-arrow"><a href="#ourPartners" class="page-scroll" title="Scroll Down">arrow</a></div>
  <div class="section nomargin">
    <div class="heading-block center marBottom10">
      <h2>Everyone Loves The OCR</h2>
      <!--<span class="divcenter">We've already had 100,000 downloads across 6 countries</span> -->
    </div>
    <div class="clear"></div>
      <div class="navmenu marBottom40 marTop20" id="neWs2">
      <div class="marAuto width80">
        
        <div id="carousel-2" class="carousel slide">
          <!-- Carousel items -->
          <div class="carousel-inner">
            <div class="active item">
              <div class="docProfile">
                    <?php echo $this->Html->image('doctor.png', array("alt"=>"")); ?>
                    <p>"Me and my colleague have used The OCR to better treat our patients and learn from other medical professionals" 
                    <em>Dr Jones, Head of Thoracic Surgery All Saints Hospital, London</em></p>
                </div>
            </div>
            <div class="item">
              <div class="docProfile">
                    <?php echo $this->Html->image('doctor.jpg', array("alt"=>"")); ?>
                    <p>"Me and my colleague have used The OCR to better treat our patients and learn from other medical professionals" 
                    <em>Dr Jones, Head of Thoracic Surgery All Saints Hospital, London</em></p>
                </div>
            </div>
            <div class="item">
              <div class="docProfile">
                    <?php echo $this->Html->image('doctor.jpg', array("alt"=>"")); ?>
                    <p>"Me and my colleague have used The OCR to better treat our patients and learn from other medical professionals" 
                    <em>Dr Jones, Head of Thoracic Surgery All Saints Hospital, London</em></p>
                </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    <div class="container clearfix">
      <div class="clear"></div>
      <div class="navmenu marBottom20" id="partNer2">
      <div class="leftFloat width100">
      <div class="heading-block center marBottom0">
      <span class="divcenter">We've Been Selected in the Top 100 Digital Health List</span> 
    </div>
        <ul class="nav  clearfix tnav" id="nav_main3">
          <li><a href="http://www.joomag.com/magazine/the-journal-of-mhealth-vol-2-issue-6-dec-2015/0902490001450798164/p10?short" target="_blank"></a></li>
        </ul>
        </div>
      </div>

      <!-- <div class="clear"></div>
      <div class="navmenu">
      <div class="navmenu marBottom20">
       <div class="marAuto width100">
       <div class="heading-block center marBottom10">
          <span class="divcenter">We're Working With Great Partner Hospitals</span> 
        </div>
        <ul class="nav clearfix tnav" id="nav_main2">
          <li><a href="http://www.mkhospital.nhs.uk/" target="_blank"></a></li>
          <li><a href="http://www.buckingham.ac.uk/medicine" target="_blank"></a></li>
          <li><a href="http://www.lsbu.ac.uk/courses/undergraduate/qualifications" target="_blank"></a></li>
        </ul>
       </div>
      </div>
      </div> -->
    </div>
  </div>
</section>
<section class="product-section pulse-app2 padTopBottom100" id="ourNews">
  <div class="down-arrow">
    <a href="#ourNews" class="page-scroll" title="Scroll Down">arrow</a>
  </div>
  <div class="section nomargin">
    <div class="heading-block center">
      <!--<h2>Call To Action</h2>-->
    </div>
    <div class="container clearfix">
      <div class="navmenu centerAlign">
        <h3>Connect With Other Medical Professionals & Make Your Own Lasting Impact on Medicine.</h3>
        <h4>Download The OCR Now</h4>
        <div class="app-on-stores2">
                <ul>
                  <li><a title="Download on the App Store" target="_blank" href="https://itunes.apple.com/us/app/the-on-call-room-ocr/id975310441?ls=1&amp;mt=8" onclick="ga('send', 'event', 'app', 'download','appstore');">
                    <?php echo $this->Html->image('app-store.png', array("alt"=>"Download on the App Store", "title"=> "Download on the App Store", "height"=>"70", "border"=>"0", "width"=>"229")); ?>
                  </a></li>
                  <li><a title="Get it on Google play" target="_blank" href="https://play.google.com/store/apps/details?id=com.ocr.theoncallroom&amp;hl=en" onclick="ga('send', 'event', 'app', 'download','googleplay');">
                    <?php echo $this->Html->image('google-play.png', array("alt"=>"Get it on Google play", "title"=>"Get it on Google play", "height"=>"70", "width"=>"229")); ?>
                  </a></li>
                </ul>
              </div>
      </div>
    </div>
  </div>
</section>

<button style="display:none;" type="button" class="btn2 btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="logo-expo">
        <?php echo $this->Html->image('logo-expo.png',array('alt'=>'logo-expo'));?>

        </div>
      <button aria-label="Close" data-dismiss="modal" class="close" type="button">
        <span aria-hidden="true">×</span>
      </button>
      <div class="logo-websummit" onclick="newDoc()">
        <h2>Health and Care<br />Innovation Expo 2016</h2>
        <div class="text-center padding15">
        <?php echo $this->Html->image('banner-phone.png',array('alt'=>'logo'));?>
           
        </div>
        <p>On<br />7th and 8th September 2016</p>
      </div>
      <?php echo $this->Html->image('card.png',array('class'=>'websubmmit'));?>
      
      <div class="logo-medic">
        <?php echo $this->Html->image('logo-medic.png',array('alt'=>'logo-medic'));?>
        
      </div>
      <div class="logo-nhs">
        <?php echo $this->Html->image('logo-nhs.png',array('alt'=>'logo-nhs'));?>
      </div>
    </div>
  </div>
</div>

<?php
  echo $this->Html->script(array('mc-validate'));
?>
<script type="text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);
</script>

<script type="text/javascript">

$( document ).ready(function() {



$(".small-section").click(function(){
//alert( "ready!" );
    $(".slide1").animate({ 'margin-top': -($(this).attr('data') * 377) });
    $('.small-section').removeClass('Scolor');
    if ($(this).attr('data') == '1') {

        $(".sec2").addClass('Scolor');
    }
    if ($(this).attr('data') == '2') {

        $(".sec3").addClass('Scolor');
    }
    if ($(this).attr('data') == '3') {

        $(".sec4").addClass('Scolor');
    }
    if ($(this).attr('data') == '0') {

        $(".sec1").addClass('Scolor');
    }
});


});

$('.carousel').carousel({
  interval: 4000
});

function newDoc() {
  window.open('https://www.facebook.com/events/1760311317586587/','_blank');
    done = true;
}
</script>

