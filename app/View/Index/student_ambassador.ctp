<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
              <p class="marTop20">We want The OCR to spread around your campus quicker than Chlamydia…</p>
              <p>The OCR is a great tool to help both fully qualified medical professionals and students share, learn and collaborate.</p>
              <p>To help make sure that medical, nursing, pharmaceutical and dental students get to use The OCR and benefit from the knowledge of medical professionals, we are looking for key representatives at medical colleges and universities to help us spread the word.</p>
              <p>We’ve found that the most successful ambassadors (and medical professionals for that matter) are:</p>
              <ul class="aboutTab marBottom10">
                  <li>Outgoing</li>
                  <li>Creative</li>
                  <li>Well connected on Campus</li>
                  <li>Independent</li>
              </ul>
              <p>And want to help improve medical standards and patient care.</p>
              <p>Does this sound like you? If so, we want to hear from you.</p>
              <p>We’ll give you your very own referral link that you can give out to students on campus to help keep track of your hustling. For every active user you sign up who is a confirmed medical student or professional, we’ll give you £10/$15.</p>
              <p>If you’ve got 100 people in your medical class, that’s an easy £1000/$1500 for a few emails, text messages and conversations.</p>
              <p>To be in with a shot at being an On Call Room student ambassador, email us at <a href="mailto:students@theoncallroom.com">students@theoncallroom.com</a> and tell us a little about yourself and how would you promote The OCR at your college.</p>
              <p>We look forward to welcoming you to the team.</p>
    </div>
      </div>
    </div>
  </div>
</section>