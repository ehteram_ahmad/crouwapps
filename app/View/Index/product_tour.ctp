<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <div class="bs-example">
        <h1>Product Tour</h1>
               <h3>Registration</h3>
               <ul class="quickStart width100">
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Guest Access –</strong> Take a sneak peek at how OCR works before you sign up. You’ll get to look at an anonymous ‘Pulse’ newsfeed to understand a little more how it works, You won’t be able to post, comment or share though.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start20.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Manual Registration –</strong> As standard we verify your identity against our extensive database of medical professionals from across the world. If for some reason you don’t appear on these databases you can still register and we’ll manually verify your identity and make sure you’re definitely a medical professional.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start21.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Automatic Registration -</strong> 99% of the time all it takes to register is to simply enter your name and job title (e.g. Doctor, Dentist, Nurse) and we’ll verify your professional standing against our database. Automatic registration means you’ll be able to claim your profile and start using OCR in no time.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start22.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                </ul>
              <h3>Search</h3>
              <ul class="quickStart width100">
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Search Beats –</strong> You can search the ‘beats’ content on The OCR to discover posts based on keywords you’ve been looking for. For example searching for ‘cardiac’ would bring up all posts related to that topic.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start23.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Search Professionals –</strong> Are you looking to connect with a key person within your field? Perhaps a thought leader or chief researcher, or possible a former colleague? Whoever you’re looking to reconnect with, as long as they’re on OCR, our search professionals feature will help you find them.</p>
                    </div>
                    <div class="text-center">
                     <?php echo $this->Html->image('start24.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                </ul>
              
              
              <h3 class="marTop20">Content</h3>
              <ul class="quickStart width100">
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Beats –</strong> A ‘Beat’ is content you share on The OCR. This could be a document summarizing a new discovery, a photo of an X-Ray or symptom, a question or a link to content you’ve discovered online.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start25.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Upbeat –</strong> This is a way to personally approve content posted to OCR and is the equivalent of a Facebook ‘like’. Content that is peer approved in this way is more likely to appear in your ‘Pulse’ newsfeed as it gains traction.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start26.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Downbeat –</strong> This is a way to disregard or ‘dislike’ content posted to OCR. It’s a negative action and should only be used if you strongly disagree with a post.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start27.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Pulse –</strong> This is the newsfeed where all the content is posted to. The content you see here will be based upon the interests you chose when you signed up, as well as content that has gained popularity within the community by attracting lots of Upbeat votes. You can filter this view based on ‘All Beats’, ‘Top Beats’ and ‘People I follow’.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start28.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>My Beats –</strong> This is a summary of all the posts you’ve made and all the conversations you’re currently active in.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start29.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Multimedia Uploads –</strong> To help support the content you post on OCR you can upload photos, Youtube videos, documents, or links to other websites. Remember to get consent forms for any sensitive information you want to share.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start30.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Tag User –</strong> If you’ve made a discovery as part of a team and want to include them in a post, or if you want to alert a colleague to a post you’re making, then use the tag feature to make sure the other person becomes aware of it.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start31.png', array("alt"=> "")); ?></div>
                    <div class="clear"></div>
                </li>
                </ul>
              
              
              
              
              
              
              
              <h3 class="marTop20">Social</h3>
              <ul class="quickStart width100">
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Build Your Network –</strong> As well as sharing your knowledge with the community and gaining insight from other medics, you can use The OCR to build your professional network and enhance your career.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start32.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Invite Colleagues -</strong>  Encourage others to follow your posts on The OCR</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start33.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Colleagues –</strong> You can ‘follow’ your colleagues on OCR or be ‘followed’ by others. This means you can keep track of posts from people within the same hospital as you or in the same field.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start34.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Comment –</strong> Want to join the conversation on someone else’s post? Simply hit the comment button and add your words of wisdom.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start35.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Share –</strong> Have you seen something you like on OCR that you think is particularly relevant to you and your followers? Hit the share button and you can repost it to make sure all your followers get to see it too.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start36.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Notifications –</strong> These are optional and allow you to be notified when someone responds to your ‘beat’ post or the conversation you’re in.</p>
                    </div>
                    <div class="text-center">
                     <?php echo $this->Html->image('start37.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                </ul>
              
              
              
              
              
              
              <h3 class="marTop20">Security</h3>
              <ul class="quickStart width100">
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Consent Form –</strong> You can use this feature to collect consent from a patient for you to share their X-Ray, Photograph or case files on The OCR in an effort to aid diagnosis or improve treatment. Consent forms can then be associated with specific content you post.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start38.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Anonymous Posting –</strong> Is there something you want to share anonymously? Perhaps you’ve seen an example of malpractice, or want to blow the whistle on poor working conditions. Whatever the situation, we have an anonymous feature to help you raise awareness without fear of reprisals.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start39.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Photo editing –</strong> To help protect patient’s identity or annotate a point, we’ve included photo editing tools within OCR that help you draw, crop or add text to a picture you’ve uploaded.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start40.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                </ul>
    </div>
    </div>
      </div>
    </div>
  </div>
</section>