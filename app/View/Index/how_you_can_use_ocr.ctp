<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
              <h3>For Consultants</h3>
              <p>We want to help you share your knowledge with the healthcare community. We run regular live Q&A sessions where you can showcase your expertise to other doctors across the world.</p>
             <ul class="aboutTab">
                <li>  Share your knowledge with Q&amp;A sessions</li>
                <li>  Help us build a living, breathing real life text book</li>
                <li>  Understand &amp; improve best practice around the world</li>
                <li>  Build your professional network</li>
            </ul>
            <h3>For Doctors</h3>
            <p>We know you need somewhere to quickly discover, discuss and collaborate on new medical findings as well as seek second opinions and gain the support of your peers when things get tough.</p>
      <ul class="aboutTab">
                <li>  Quickly seek and share knowledge</li>
                <li>  Get peer approved second opinions</li>
                <li>Collaborate on diagnoses/research</li>
                <li>Discover new ways of doing things</li>
                <li>Help improve patient care around the world</li>
                <li>Discuss medical advances with like minded people</li>
                <li>Have an outlet for workplace stress and anxiety</li>
            </ul>
            <h3>For Nurses</h3>
            <p>It’s critical that nurses use OCR to share their insight into patient management, collaborate on new treatment methods and share their own best practice.</p>
      <ul class="aboutTab">
            <li>  Understand doctor’s insight into patient management</li>
            <li>  Share your insight and highlight areas to improve</li>
            <li>  Learn</li>
            <li>  Discuss</li>
            </ul>
            <h3>For Pharmacists</h3>
            <ul class="aboutTab">
                <li>  Quickly discover new advances and opinions in pharmaceutical medicine</li>
                <li>  Connect and follow key influencers in your field</li>
                <li>  Discuss advancements in pharmaceutical management</li>
                <li>  Collaborate and communicate with doctors and nurses to share best practice</li>
            </ul>
            <h3>For Dentists</h3>
            <ul class="aboutTab">
                <li>  Connect and follow key influencers in your field</li>
                <li>  Discover and collaborate on new research, findings and techniques</li>
            </ul>
            <h3>For Students</h3>
            <ul class="aboutTab">
                <li>Further develop your learning by getting first-hand knowledge from professionals</li>
                <li>Discover new findings first</li>
                <li>Understand the inner workings of the healthcare industry</li>
                <li>Become an OCR brand ambassador in your college</li>
                <li>Start building your professional network</li>
            </ul>
    </div>
      </div>
    </div>
  </div>
</section>