<?php 
	/*if(!empty($networkGuidelines)){
		echo !empty($networkGuidelines['Content']['content_text']) ? $networkGuidelines['Content']['content_text'] : '';
	}*/
?>
<section class="inner-content"><!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <div class="bs-example">
 <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
<p>The On Call Room is a library of medical journals (including both images and text) that is continuously growing due to the advancement of research in the medical sector and the insightiful content shared by you, our user community.</p>

<p>The following Community Guidelines are here to help you understand what it means to be a member of the 'The On Call Room'. Please be aware that all of your activity on The On Call Room is subject to these Guidelines and our Terms of Service. This includes all content &ndash; journals, images, comments and profile information.</p>

<h3>Respect your colleague's patients.</h3>

<p>Information and discussion featured on the On Call Room may relate to real life cases and patients. Please apply the same ethical principles you use in your practice to what you post. Please don't write anything on 'The On Call Room' that you wouldn't say in front of a colleague or fellow healthcare professional or patient.</p>

<h3>Never include identifying patient information.</h3>

<p>Whether you are uploading an article, uploading an image or posting a comment you must never include any personal identifiable information. Any article, comment or image that violates this policy will be removed. As outlined in our Privacy Policy and Terms &amp; Conditions, it is your responsibility to maintain the privacy of confidential information at all times.</p>

<h3>Post educational captions and comments.</h3>

<p>The On Call Room is a community based learning and research environment and we encourage users to leave relevant content and comments about the medical research and articles presented. This includes anything that builds a useful clinical dialogue. Do not post any content that is not conducive to maintaining a respectful learning environment. Content without educational value will be removed by the admin team.</p>

<h3>Share professional articles and research only.</h3>

<p>The On Call Room is intended for healthcare professionals to share and access articles and research, which may include cases relating to patients under direct clinical care. It is not intended for sharing your own medical conditions, or those of your family or friends.</p>

<h3>Use your professional judgment.</h3>

<p>As an On Call Room user, you can help maintain the integrity of our community by flagging content that is not in line with these guidelines. These consist of, but are not limited to:</p>
<ul class="aboutTab padLeft40">
    <li>Any post or comment that violates The On Call Rooms Terms and Conditions.</li>
    <li>A joke that is made at a patient or colleagues expense or is offensive to a patient or colleague.</li>
    <li>Comments in which a user insults, bullies or instigates conflict with another user.</li>
    <li>Any comment that contains inappropriate or malicious language.</li>
    <li>Be an Authentic Healthcare Professional.</li>
</ul>
<p>If you are not a healthcare professional, don't misrepresent yourself as one. The On Call Room is intended for healthcare professionals to learn, share and discuss medical research and articles. It is not a tool for patients to post content relating to their own conditions. If you have a health concern, please see your physician.</p>

<p>If you have any questions or comments, we'd love to hear from you.<br>
Please feel free to email us at contact@theoncallroom.com</p>

<p>The On Call Room Team</p>
 </div>
 </div>
      </div>
    </div>
  </div>
</section>
