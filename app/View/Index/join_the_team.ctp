<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="faq">
        <div class="col-sm-12">
        <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
              <p>Are you at the top of your game and looking for your next challenge? Whether you’re a developer, a marketer or a medical professional, you could be just what we’re looking for.</p>
              <p>Medic Creations Ltd is a dynamic, fast growing Healthtech company with big plans for the future and a whole host of products in the pipeline. We make apps that empower medical professionals to help their patients live healthier, happier lives.</p>
              <p>We're looking for enthusiastic, talented individuals who know that 'humorous' isn't just a bone in your arm. Our ideal fit knows how to have fun and wants to work on hugely rewarding projects that make a lasting impact on the world around them.</p>
              <p>If you can help us develop new products, break into markets or encourage medical professionals to use our apps, then let's talk.</p>
              <!--team form -->

          <div class="account-section">
            <div class="col-xs-12 col-sm-12 col-md-7 team-form">
            <h3 class="text-center marBottom10">Want to Join Our Team? Send us Your CV Now</h3>
                  <?php if ($this->Session->check('Message.flash')): ?>
                  <div class="formResponseMessage">
                    <h3><?php 
                      echo $this->Session->flash(); 
                    ?></h3>
                  </div> <?php endif; ?>
                <form novalidate action="<?php echo BASE_URL . 'Index/joinTeam'; ?>" class="" id="contact-form" method="post" accept-charset="utf-8" enctype="multipart/form-data"><div style="display:none;"><input type="hidden" name="_method" value="POST"></div>  
         <fieldset class="form-group">
                 <label for="name">Your Name</label>
                 <input type="text" name="data[contact][name]" id="name" class="form-control" maxlength="50" value="">
                </fieldset>
                <fieldset class="form-group">
                  <label for="email">Your Email</label>
                   <input type="text" name="data[contact][email]" id="email" class="form-control" maxlength="250" value="">              
                </fieldset>
                <fieldset class="form-group">
                  <label for="subject">Your CV</label>
                    <input type="file" name="data[contact][file]" id="CV" class="form-control">
                </fieldset>
                <fieldset class="form-group">
                  <label for="message" class="lineHeight22">Your Covering Letter</label>
                  <textarea name="data[contact][message]" id="message" class="form-control" rows="4" cols="50" maxlength="500"></textarea>
                </fieldset>
                <fieldset class="form-group">
                  <input type="submit" class="btn btn-signup" id="" name="" value="Send Your CV">
                </fieldset>
              </form>
            </div>
          </div>    
          <!--//team form --> 
    </div>
      </div>
    </div>
  </div>
</section>

<script>
  setTimeout(function() {
            $('.formResponseMessage').fadeOut('fast');
            }, 5000); // <-- time in milliseconds
</script>