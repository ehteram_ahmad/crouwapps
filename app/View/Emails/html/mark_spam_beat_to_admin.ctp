<?php
	// Set email template header
	echo $this->element('email_template_header');
	// Email template body part
 	echo isset($msg) ? $msg : '';
 	// Set email template footer
 	echo $this->element('email_template_footer');