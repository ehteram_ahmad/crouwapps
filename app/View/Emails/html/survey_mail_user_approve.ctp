<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>The Junior Doctor Sun Survey - For Real</title>
</head>
<body>
<table width="535" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Segoe UI', Arial;font-size:14px;line-height:18px;color:#3a3a3a;text-decoration:none;" >
  <tbody >
    <tr>
      <td bgcolor="#FFFFFF" style="border:1px solid #d2d2d2;" >
         <table width="535" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 10px;">
          <tbody>
            <tr>
              <td align="left" valign="top" style="text-align: center; background-color:#ffffff; padding: 5px;"><img src="http://medicbleep.com/images/logo-newsletter.png" alt="" width="175.177" height="63" style="border:0px;"></td>
            </tr>
<!-- Body [START]-->
<td class="bodyContainer" style="padding-top:9px; padding-bottom:9px;" valign="top"><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="598" style="width:598px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                        
                            <div style="text-align: center;">Thanks for verifying via signup, Here's the Survey:</div>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #007FFF;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Arial; font-size: 18px; padding: 12px;" valign="middle" align="center">
                                <a class="mcnButton " title="LINK TO THE SURVEY" href="https://www.surveymonkey.co.uk/r/X27M5LV" target="_self" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">LINK TO THE SURVEY</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="598" style="width:598px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                        
                            <span style="font-size:16px"><span style="color:#000000">The Sun claimed that "93% of junior doctors and medical students would support full </span></span><span style="color:#000000"><span style="font-size:16px">privatisation</span></span><span style="font-size:16px"><span style="color:#000000"> if they got a significant pay increase."</span></span>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnImageCardBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageCardBlockOuter">
        <tr>
            <td class="mcnImageCardBlockInner" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                
<table class="mcnImageCardBottomContent" style="background-color: #404040;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
    <tbody><tr>
        <td class="mcnImageCardBottomImageContent" style="padding-top:18px; padding-right:18px; padding-bottom:0; padding-left:18px;" valign="top" align="center">
        
            

            <img alt="" src="https://gallery.mailchimp.com/4f8f2caf54859b1e05155fb68/images/587d483c-ae52-49ed-9e07-f60380efe296.png" style="max-width:663px;" class="mcnImage" width="526">
            
        
        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" style="padding: 9px 18px;color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;" width="526" valign="top">
            <a href="https://www.thesun.co.uk/news/2323924/survey-shows-93-of-junior-doctors-would-back-a-fully-privatised-health-service-if-it-meant-a-pay-increase/" target="_blank"><span style="color:#FFFFFF">Link to The Sun's Article</span></a>
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="598" style="width:598px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                        
                            <ol>
	<li><span style="font-size:13px"><span style="color:#000000">The supposed junior doctors at Hull who did this survey have not shown their methodology. This has brought about a huge debate in many forums.</span></span></li>
	<li><span style="font-size:13px"><span style="color:#000000">Our survey is a genuine survey which seeks to obtain a fully representative picture by collating data from doctors with GMC numbers.</span></span></li>
	<li><span style="font-size:13px"><span style="color:#000000">It includes a process to ensure that only genuine doctors with GMC numbers are permitted to input data. </span></span></li>
	<li><span style="font-size:13px"><span style="color:#000000">In addition, our survey asks you to confirm you are a junior doctor. It The final collated data will be anonymised so your personal details won’t be shown to the public. </span></span></li>
	<li><span style="font-size:13px"><span style="color:#000000">However, the overall stats will be emailed to the JDC/BMA, communicated to the Sun newspaper and other media, but also to every person who responds to the survey, in order to ensure transparency.</span></span></li>
	<li><span style="font-size:13px"><span style="color:#000000">We have tried to keep the questions as close to the original survey as possible to see whether a validated cohort of junior doctors with authenticated GMC numbers produces responses similar to the Sun newspaper’s non-validated respondents.</span></span></li>
</ol>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #007FFF;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Arial; font-size: 18px; padding: 12px;" valign="middle" align="center">
                                <a class="mcnButton " title="LINK TO THE SURVEY" href="https://www.surveymonkey.co.uk/r/X27M5LV" target="_self" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">LINK TO THE SURVEY</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnImageCardBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageCardBlockOuter">
        <tr>
            <td class="mcnImageCardBlockInner" style="padding-top:9px; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                
<table class="mcnImageCardBottomContent" style="background-color: #404040;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
    <tbody><tr>
        <td class="mcnImageCardBottomImageContent" style="padding-top:0px; padding-right:0px; padding-bottom:0; padding-left:0px;" valign="top" align="left">
        
            

            <img alt="" src="https://gallery.mailchimp.com/4f8f2caf54859b1e05155fb68/images/533805c2-a7c1-4a57-a201-80305808e8b4.png" style="max-width:1200px;" class="mcnImage" width="562">
            
        
        </td>
    </tr>
    <tr>
        <td class="mcnTextContent" style="padding: 9px 18px;color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;" width="544" valign="top">
            
        </td>
    </tr>
</tbody></table>




            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 36px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 1px solid #E3E5E6;" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table></td>

<!-- Body [END]-->
</td> 
            </tr>
          </tbody>
        </table></td>
    </tr>
   <tr>
      <td height="1" align="left" valign="middle" bgcolor="#d5d5d5" style="font-family:'Segoe UI', Arial;font-size:1px;color:#ffffff;text-decoration:none;line-height:0px;">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" valign="top" bgcolor="#ffffff" style="font-family:'Segoe UI', Arial;font-size:9px;color:#000;text-decoration:none; border: 1px solid #d2d2d2; padding-top: 20px;">
        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" class="m_6145364116186641551mcnTextContentContainer" style="border-collapse:collapse;max-width:100%;min-width:100%">
                                            <tbody>
                                              <tr>
                                                <td valign="top" class="m_6145364116186641551mcnTextContent" style="word-break:break-word;color:rgb(128,128,128);font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:10px;line-height:12.5px;text-align:left;padding:0px 18px 9px"><em>Copyright © 2016 Medic Creations, All rights reserved.</em><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><br>
                                                  You signed up on The On Call Room via the App or Joined the waiting list of MedicBleep or Entered King's Fund Contest<span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><br>
                                                  <br>
                                                  <strong>Our mailing address is:</strong><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><br>
                                                  <div class="m_6145364116186641551vcard"><span class="m_6145364116186641551org m_6145364116186641551fn">Medic Creations</span>
                                                    <div class="m_6145364116186641551adr">
                                                      <div class="m_6145364116186641551street-address">Oakdale</div>
                                                      <div class="m_6145364116186641551extended-address">Royal Oak Hill</div>
                                                      <span class="m_6145364116186641551locality">Newport</span>,<span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><span class="m_6145364116186641551region">Gwent</span><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span> <span class="m_6145364116186641551postal-code">NP18 1JF</span>
                                                      <div class="m_6145364116186641551country-name">United Kingdom</div>
                                                    </div>
                                                    <br>
                                                    <a href="http://mediccreations.us12.list-manage.com/vcard?u=4f8f2caf54859b1e05155fb68&amp;id=d83d76884e" class="m_6145364116186641551hcard-download" style="color:rgb(197,46,38);font-weight:bold;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://mediccreations.us12.list-manage.com/vcard?u%3D4f8f2caf54859b1e05155fb68%26id%3Dd83d76884e&amp;source=gmail&amp;ust=1483151065894000&amp;usg=AFQjCNEOtoHdvxraHJzY2rtB0L538I4P9g">Add us to your address book</a></div>
                                                  <br>
                                                  <br>
                                                  Want to change how you receive these emails?<br>
                                                  You can<span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><a href="http://mediccreations.us12.list-manage1.com/profile?u=4f8f2caf54859b1e05155fb68&amp;id=d83d76884e&amp;e=25168b7811" style="color:rgb(197,46,38);font-weight:bold;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://mediccreations.us12.list-manage1.com/profile?u%3D4f8f2caf54859b1e05155fb68%26id%3Dd83d76884e%26e%3D25168b7811&amp;source=gmail&amp;ust=1483151065894000&amp;usg=AFQjCNHSN1wHCpMkHvdRIKvmOBq-Loiluw">update your preferences</a><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span>or<span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><a href="http://mediccreations.us12.list-manage.com/unsubscribe?u=4f8f2caf54859b1e05155fb68&amp;id=d83d76884e&amp;e=25168b7811&amp;c=5014839cdb" style="color:rgb(197,46,38);font-weight:bold;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://mediccreations.us12.list-manage.com/unsubscribe?u%3D4f8f2caf54859b1e05155fb68%26id%3Dd83d76884e%26e%3D25168b7811%26c%3D5014839cdb&amp;source=gmail&amp;ust=1483151065894000&amp;usg=AFQjCNHN5p2w0nmKdSVdMlCstrMZcTjBBg">unsubscribe from this list</a><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><br>
                                                  <br></td>
                                              </tr>
                                            </tbody>
                                          </table>
      </td>
    </tr>
  </tbody>
</table>
</body>
</html>