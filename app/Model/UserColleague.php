<?php 
/*
UserColleague model to represent colleague users.
*/
App::uses('AppModel', 'Model');

class UserColleague extends AppModel {

	/*
	---------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	---------------------------------------------------------------------------------------
	*/
	public function colleagueLists( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $userId = NULL){
		$userColleagues = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		//** If userId set show all colleagues of the particular user
		$conditions = '';
		if( !empty($userId) ){
			$conditions = array('UserColleague.user_id'=> $userId, 'User.status'=> 1);
		}
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserColleague.colleague_user_id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserColleague.colleague_user_id = UserProfile.user_id')
					),
				),
			'fields'=> array('UserColleague.id, UserColleague.user_id, UserColleague.colleague_user_id, UserColleague.status, UserColleague.created, User.id, User.status, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
			'conditions'=> $conditions,
			//'group'=> array('UserColleague.id'),
			'order'=> array('UserColleague.created DESC'),
			'limit'=> $pageSize,
			'offset'=> $offsetVal  
		);
		$userColleagues = $this->find('all', $options);
		return $userColleagues;
	}

	
}

?>