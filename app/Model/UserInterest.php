<?php 
/*
Add, update, List user interests.
*/
App::uses('AppModel', 'Model');

class UserInterest extends AppModel {
	
	/*
    -----------------------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P:
	Desc: Fetch all consent by any user
	-----------------------------------------------------------------------------------------------
    */
    public function interestList( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1, $userId = NULL , $interestId = NULL , $sortBy = 'latest' ){
    	$interestData = array();
    	$offsetVal = ( $pageNum - 1 ) * $pageSize;
    	
    	if( !empty( $userId ) ){
    		$conditions = array("user_id"=> $userId, "UserInterest.status"=> 1);
	    	//** If interest id set fetch all consents > $interestId
			if( !empty($interestId) ){
                if( isset($sortBy) && $sortBy == 'previous' ){
				    $conditions = array("user_id"=> $userId, "UserInterest.status"=> 1, "UserInterest.interest_id < "=> $interestId);
			     }else{
                    $conditions = array("user_id"=> $userId, "UserInterest.status"=> 1, "UserInterest.interest_id > "=> $interestId);
                 }
            }
    		$options =  array(
    						'joins'=> array(
								array(
								'table' => 'specilities',
								'alias' => 'spec',
								'type' => 'inner',
								'conditions'=> array('spec.id = UserInterest.interest_id')
								),
    						),
    						'conditions'=> $conditions,
    						'fields'=> '`UserInterest`.`id`, `UserInterest`.`user_id`, `UserInterest`.`interest_id`, `UserInterest`.`status`, `UserInterest`.`created`, spec.id, spec.name, spec.img_name',
    						'order'=> array('spec.name'),
							'limit'=> $pageSize,
							'offset'=> $offsetVal
    					);
    							
    		$interestData = $this->find("all", $options);
    	}
    	return $interestData;
    }
}
?>