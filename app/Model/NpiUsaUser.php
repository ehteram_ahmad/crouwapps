<?php 
/*
NpiUsaUser model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');

class NpiUsaUser extends AppModel {
	//public $useDbConfig = 'country_registered_professional';


	/*
	On: 07-12-2016
	I/P: $params = array()
	O/P: NPI users info
	Desc: Fetch all NPI users info.
	*/
	public function npiUserSearch( $params = array()){
		
		$npiUsers = array();
		if( !empty($params) ){
			$pageSize = empty($params['size']) ? DEFAULT_PAGE_SIZE : $params['size'];
			$pageNum = empty($params['page_number']) ? 1 : $params['page_number'];
			$offsetVal = ( $pageNum - 1 ) * $pageSize; 
			
			$conditions = " is_registered = 0 ";
			if(!empty($params['first_name'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND  LOWER(provider_first_name) LIKE LOWER("'.$params['first_name'].'%") ';
				}else{
					$conditions .= ' LOWER(provider_first_name) LIKE LOWER("'.$params['first_name'].'%") ';
				}
			}
			if(!empty($params['last_name'])){
				if(strlen($conditions) >0){
						$conditions .= ' AND LOWER(provider_last_name) LIKE LOWER("'.$params['last_name'].'%") ';
					}else{
						$conditions .= ' LOWER(provider_last_name) LIKE LOWER("'.$params['last_name'].'%") ';
					}
			}
			$npiUsersData = $this->find('all', 
							array(
								'conditions' => $conditions,
								'limit'=> $pageSize,
								'offset'=> $offsetVal,
								'order'=> array('provider_first_name'=> 'ASC', 'provider_last_name'=> 'ASC')
							)
						);

			foreach($npiUsersData as $npi){
				$npiUsersArr[]['GmcUkUser'] = array(
						'id'=> $npi['NpiUsaUser']['id'],
						'GMCRefNo'=> $npi['NpiUsaUser']['npi'],
						'Surname'=> $npi['NpiUsaUser']['provider_last_name'],
						'GivenName'=> $npi['NpiUsaUser']['provider_first_name'],
						'Gender'=> $npi['NpiUsaUser']['provider_gender_code'],
						'YearOfQualification'=> '',
						'PlaceofQualificationCountry'=> ''
					);
			}
			$npiUsers = $npiUsersArr;
		}
		return $npiUsers; 
	}

	/*
	On: 09-12-2016
	I/P: 
	O/P:
	Desc: 
	*/
	public function updateFields( $fields = array(), $conditions = array() ){
		$return = 0;
		if( !empty($fields) && !empty($conditions) ){
			$this->updateAll( $fields, $conditions );

			if( $this->getAffectedRows() ){
				$return = 1;
			}
		}
		return $return;
	}

}
?>