<?php 
App::uses('AppModel', 'Model');

class Profession extends AppModel {
	
	/*
	On: 28-07-2015
	I/P: $params = array()
	O/P: array()
	Desc: Fetches all active Professions.
	*/	
	public function allProfessionss($params = array()){
		$pageSize = isset($params['size'])?$params['size']:PAGE_SIZE;
		$data = array();
		$data = $this->find('all', array('conditions'=> array('status'=> 1), 'order'=>array('profession_type ASC'), 'limit'=> $pageSize));
		return $data;
	}
}
?>