<?php 
/*
Add, update, Validate user consent form.
*/
App::uses('AppModel', 'Model');

class ConsentForm extends AppModel {

	public $hasMany = array(
        'ConsentFormAttribute' => array(
            'className' => 'ConsentFormAttribute',
            'conditions' => array('ConsentFormAttribute.status' => 1), 
            'order'=> array(" field(ConsentFormAttribute.attribute_type, 'text','video','image'),ConsentFormAttribute.is_signature ASC ")
        ),
    );

    /*
    -----------------------------------------------------------------------------------------------
	On: 29-09-2015
	I/P: 
	O/P:
	Desc: Fetch all consent by any user
	-----------------------------------------------------------------------------------------------
    */
    public function consentList( $userId = NULL , $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1, $consentId = NULL , $sortBy = 'latest' ){
    	$consentData = array();
    	$offsetVal = ( $pageNum - 1 ) * $pageSize;
    	
    	if( !empty( $userId ) ){
    		$conditions = array("user_id"=> $userId, "ConsentForm.status"=> 1);
	    	//** If consent id set fetch all consents > $consentId
			if( !empty($consentId) ){
                if( isset($sortBy) && $sortBy == 'previous' ){
				    $conditions = array("user_id"=> $userId, "ConsentForm.status"=> 1, "ConsentForm.id < "=> $consentId);
			     }else{
                    $conditions = array("user_id"=> $userId, "ConsentForm.status"=> 1, "ConsentForm.id > "=> $consentId);
                 }
            }
    		$options =  array(
    						'conditions'=> $conditions,
    						'order'=> array('ConsentForm.created DESC'),
							'limit'=> $pageSize,
							'offset'=> $offsetVal
    					);
    							
    		$consentData = $this->find("all", $options);
    	}
    	return $consentData;
    }

    /*
    -----------------------------------------------------------------------------------------------
	On: 29-09-2015
	I/P: 
	O/P:
	Desc: Fetch all consent by any user
	-----------------------------------------------------------------------------------------------
    */
    public function consentListById( $consentId = NULL, $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 ){
    	$consentData = array();
    	$offsetVal = ( $pageNum - 1 ) * $pageSize;
    	
    	if( !empty( $consentId ) ){
    		$conditions = array( "ConsentForm.id > "=> $consentId, "ConsentForm.status"=> 1 );
    		$options =  array(
    						'conditions'=> $conditions,
    						'order'=> array('ConsentForm.created DESC'),
							'limit'=> $pageSize,
							'offset'=> $offsetVal
    					);
    							
    		$consentData = $this->find("all", $options);
    	}
    	return $consentData;
    }

    /*
    -----------------------------------------------------------------------------------------------
    On: 21-12-2015
    I/P: 
    O/P:
    Desc: Unsent consent details.
    -----------------------------------------------------------------------------------------------
    */
    public function unsentConsent( ){
        //$this->unBindModel(array('hasMany' => array('ConsentFormAttribute')));
        //$this->recursive = -1;
        $consentData = array();
            $conditions = array( "ConsentForm.status"=> 1, "ConsentForm.consent_send_confirmed"=> 0 );
            $options =  array(
                            'joins' =>
                            array(
                                /*array(
                                'table' => 'consent_form_attributes',
                                'alias' => 'ConsentFormAttribute',
                                'type' => 'left',
                                'conditions'=> array('ConsentFormAttribute.consent_form_id = ConsentForm.id')
                                ),*/
                                array(
                                'table' => 'users',
                                'alias' => 'u',
                                'type' => 'left',
                                'conditions'=> array('ConsentForm.user_id = u.id')
                                ),
                                array(
                                'table' => 'user_profiles',
                                'alias' => 'up',
                                'type' => 'left',
                                'conditions'=> array('ConsentForm.user_id = up.user_id')
                                ),
                            ),
                            'conditions'=> $conditions,
                            'order'=> array('ConsentForm.id DESC'),
                            'fields'=> array("ConsentForm.id", "ConsentForm.user_id", "ConsentForm.patient_name", "ConsentForm.patient_email", "u.email", "up.first_name", "up.last_name"),
                            //'order'=> array(" field(ConsentFormAttribute.attribute_type, 'text','video','image'),ConsentFormAttribute.is_signature ASC ")
                            );
                                
            $consentData = $this->find("all", $options);
        return $consentData;
    }
}
?>