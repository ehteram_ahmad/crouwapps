<?php 
/*
Represent model for designations name.
*/
App::uses('AppModel', 'Model');

class Designation extends AppModel {
	
	/*
	--------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: Displays all institute names.
	--------------------------------------------------------------------------
	*/
	public function designationLists( $params = array() ){
		$designationLists = array();
		if( !empty($params) ){
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			$conditions = " status = 1  AND created_by = 0 ";
			if(!empty($params['searchText'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND LOWER(designation_name) LIKE LOWER("'.$params['searchText'].'%") ';
				}
			}
			$designationLists = $this->find("all", array(
										"conditions"=> $conditions,
										"order"=> array("designation_name"),
										'limit'=> $pageSize,
										'offset'=> $offsetVal
										)
									);
		}
		return $designationLists;
	}

}
?>