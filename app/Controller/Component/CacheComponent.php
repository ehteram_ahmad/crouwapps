<?php

/*
 * Cache Component
 * 
 * 
 *
 */
class CacheComponent extends Component{ 

	/*
	----------------------------------------------------------------------------------------------
	On: 10-11-2017
	I/P: 
	O/P: 
	Desc: Redis connection
	----------------------------------------------------------------------------------------------
	*/
	public function redisConn(){
		$returnVal = array();
		App::import('Vendor','PREDIS',array('file' => 'predis/autoload.php'));
		$redisVars = array(
		"scheme" => "tcp",
		'host'     => '3.250.106.38',
		'port'     => 6379,
		'timeout' => 0.8, // (In seconds) used to connect to a Redis server after which an exception is thrown.
		"password" => 'bd88ffcb3c87c99d1fd50ece02f125fa',
		);
		$redis = new Predis\Client($redisVars);
		try {
			$msg =  $redis->ping();
			if($msg = 'PONG'){
				$returnVal = array("connection"=> true, "errors"=> "", "robj"=> $redis);
				return $returnVal;
			}
		} catch (Exception $e) {
			$msg = 'Redis Error : '.$e->getMessage();
			$returnVal = array("connection"=> false, "errors"=> $msg, "robj"=> "");
		}
		return $returnVal;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 13-11-2017
	I/P: 
	O/P: 
	Desc: Delete cache data of specified key
	----------------------------------------------------------------------------------------------
	*/
	public function delRedisKey($keyName=NULL){
		$keyDel = false;
		if(!empty($keyName)){
			$redisConn = $this->redisConn();
			if(($redisConn['connection']) && empty($redisConn['errors'])){
				if($redisConn['robj']->exists($keyName)){
					$keyDel = $redisConn['robj']->del($keyName);
				}
			}
		}
		return $keyDel;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 13-11-2017
	I/P:
	O/P:
	Desc: Delete cache data of specified key
	----------------------------------------------------------------------------------------------
	*/
	public function delHRedisKey($params=NULL){
		$keyDel = false;
		if(!empty($params)){
			$redisConn = $this->redisConn();
			if(($redisConn['connection']) && empty($redisConn['errors'])){
					$keyDel = $redisConn['robj']->hdel('newInstitutionDirectory:86',$params['user_id']);
			}
		}
		return $keyDel;
	}


	/**
	 * resetSessionTTL
	 * @params $authToken string
	 * @returns void
	 *
	 * Sets a key for the session with an expiry.  This is used by the app
	 * via an endpoint to determine whether the user should be logged in or
	 * not. This is almost certainly at least a partial duplication of work,
	 * however at this point in the product lifecycle it doesnt' make sense
	 * to disambiguate and refactor: this solution should be fast enough, and
	 * the session info shoul only be used for information governance.
     */
	public function resetSessionTTL($authToken) {
		$redisConn = $this->redisConn();
		if(!($redisConn['connection'] && empty($redisConn['errors']))){
			throw new Exception("Could not connect to redis. Please check configuration.");
		}

		// Prepend "session_" to the key so that it is identifiable as such.
		$key = "session::" . $authToken;
		$expiry_secs = 60 * 60 * 72;

		// Add a key with the current timestamp in it,
		// Then add an expiry to the token (72 Hours)
		$now = (new DateTime())->format(DateTime::ATOM);
		// Format the time logged as iso8601
		$redisConn['robj']->set($key, $now);
		// Apply a TTL of now + 72 hours.
		$redisConn['robj']->expire($key, $expiry_secs);
	}

	/**
	 * hasSession
	 * @params $authToken string
	 * @returns bool
	 *
	 * This function looks for a key matching the auth token. If one exists,
	 * that means the token hasn't expired in the last 72 hours, and therefore
	 * the session is still valid.
	 *
	 * YES: The authToken is a JWT.
	 * NO:  I don't trust this code to handle timezones and other edge cases
	 * NO:  The app should not decide if the session is valid in the first
	 *      instance.
     */
	public function hasSession($authToken) {
		$redisConn = $this->redisConn();
		if(!($redisConn['connection'] && empty($redisConn['errors']))){
			throw new Exception("Could not connect to redis. Please check configuration.");
		}

		// Prepend "session_" to the key so that it is identifiable as such.
		$key = "session::" . $authToken;

		// If a key exists, that means it hasn't expired in the last
		// 72 hours, and the session is therefore valid.
		return $redisConn['robj']->exists($key);
	}
}
?>
