<?php

/*
 * Quickblox Component
 * 
 * Contains all related function for email sending
 *
 */
class QuickbloxComponent extends Component{ 
	public function quickAuth() {
		        $nonce = rand();
		        $timestamp = time();
		        $signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp;
		        $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);
		        // Build post body
		        $post_body = http_build_query(array(
		            'application_id' => APPLICATION_ID,
		            'auth_key' => AUTH_KEY,
		            'timestamp' => $timestamp,
		            'nonce' => $nonce,
		            'signature' => $signature,
		        ));
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . 'session.json' );
		        curl_setopt($curl, CURLOPT_POST, true); // Use POST
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Receive server response
		        $response = curl_exec($curl);
		        $responseVal = json_decode($response, true);
		        if (isset($responseVal['session']['token']) && !empty($responseVal['session']['token'])) {
		            //return json_decode($response);
		            return $responseVal['session']['token'];
		        } else {
		            //$error = curl_error($curl). '(' .curl_errno($curl). ')';
		        	//return $error;
		        	$responseVal['session']['token'] = "";
		        	return $responseVal['session']['token'];
		        }
		        curl_close($curl);
		}
		
	public function quickAddUsers($token,$username,$password,$email,$externalid,$fullname,$profile_image) {
				$user_data = array(
		            'user[login]' => $username,
		            'user[password]' => $password,
		            'user[email]' => $email,
		            'user[full_name]' => $fullname,
		            'user[external_user_id]' => $externalid,
		        );
		        if($profile_image != ""){
		        		$update_array = array(
		        					'avatar_url' => $profile_image ,
  								'status' => "",
  								'is_import' => "true",
  							);
  					$user_data["user"]["custom_data"] = json_encode($update_array);
		        }
		        $post_body = http_build_query($user_data);
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users.json');
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'Accept: application/json',
		            'Content-Type: application/x-www-form-urlencoded',
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		            return $response . "\n";
		        } else {
		            $error = curl_error($curl). '(' .curl_errno($curl). ')';
		            return $error . "\n";
		        }
		        curl_close($curl);
		}
		
	public function quickGetUsers($token) {
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users.json?per_page=20');
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                echo false;
		        }
		        curl_close($curl);
		}
		
	public function quickGetDialog($token) {
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json');
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return json_decode($error) ;
		        }
		        curl_close($curl);
		}
		
	public function quickAddDialog($token,$from,$to,$name) {
		        $post_body = http_build_query(array(
		            'type' => 3,
		            'name' => $name,
		            'occupants_ids' => $to,
		        ));
		        $post_body = urldecode($post_body);
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json');
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return $response;
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error . "\n";
		        }
		        curl_close($curl);
		}
		
	public function quickGetMessage($token, $dialogId) {
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json?chat_dialog_id=' . $dialogId);
		
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		
		        if ($response) {
		                return json_decode($response);
		        } else {
		                echo false;
		        }
		        curl_close($curl);
		}
	public function quickLogin($username,$password){
		$nonce = rand();
        $timestamp = time();
        $signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp . "&user[email]=" . $username . "&user[password]=" . $password;
        $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);
        $post_array = array(
            'application_id' => APPLICATION_ID,
            'auth_key' => AUTH_KEY,
            'timestamp' => $timestamp,
            'nonce' => $nonce,
            'signature' => $signature,
            'user[email]' => $username,
            'user[password]' => $password
        );
        $post_body = http_build_query($post_array);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . 'session.json' );
        curl_setopt($curl, CURLOPT_POST, true); // Use POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Receive server response
        // Execute request and read response
        $response = curl_exec($curl);
        //echo "<pre>"; print_r($response);die;
        // Close connection
        curl_close($curl);
		if ($response) {
                return json_decode($response);
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		        return $error;
        }   
	}
		
	public function quickLogout($token){
		        //echo $token;exit();
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'session.json');
		        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		            return $response;
		        } else {
		            $error = curl_error($curl). '(' .curl_errno($curl). ')';
		            return $error;
		        }
		        curl_close($curl);
		}
	public function quickSessionDetails($token){
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'session.json');
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		
		        if ($response) {
		                return $response;
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error;
		        }
		        curl_close($curl);
		}
	public function getuserDetails($token,$externalid){
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/external/'.$externalid.'.json');
		
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error;
		        }
		        curl_close($curl);
		}
	public function sendMessage($token,$dialogId,$message){
		$data = array(
		  'recipient_id' => $dialogId,
		  'message' => $message,
		  'send_to_chat' => 1,
		);
		$request = json_encode($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json');  
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  'Content-Type: application/json',
		  'QuickBlox-REST-API-Version: 0.1.0',
		  'QB-Token: ' . $token
		));
		$resultJSON = curl_exec($ch);
		$pretty = json_encode(json_decode($resultJSON), JSON_PRETTY_PRINT);
		return $pretty;
	}
	public function quickAdduserFromOcr($username,$password,$email,$externalid,$fullname){
		$tokenDetails = $this->quickAuth();
		/*$token_array = json_decode($tokenDetails);
		$token = "";
		if(isset($token_array->session->token) && $token_array->session->token != ""){
			$token = $token_array->session->token;
			return $this->quickAddUsers($token,$username,$password,$email,$externalid,$fullname,'');
		}
		*/
		if(!empty($tokenDetails)){
			$token = $tokenDetails;
			return $this->quickAddUsers($token,$username,$password,$email,$externalid,$fullname,'');
		}else{
			return 'no token created';
		}
		
	}
	public function quickAddRemoveDialog($sender,$receiver,$status,$mbToken){
		/*$status = 0 (for unfriend case)
		$status = 1 (for Accepting friend request case)*/
		$sender_email = $sender['User']['email'];
		$tokenDetails = $this->quickLogin($sender_email,$mbToken);
		$token = $tokenDetails->session->token;
		$sender_id = $tokenDetails->session->user_id;
		if($status == 1 ){
			$colleagueDetails = $this->getuserDetails($token,$receiver['User']['id']);
			$receiver_name = $colleagueDetails->user->full_name;
			$receiver_chat_id = $colleagueDetails->user->id;
			$message = $this->quickAddDialog($token,$sender_id,$receiver_chat_id,$receiver_name);
		}else if($status == 0){
			$colleagueDetails = $this->getuserDetails($token,$receiver['User']['id']);
			$receiver_name = $colleagueDetails->user->full_name;
			$receiver_chat_id = $colleagueDetails->user->id;
			$dialogueList = $this->quickGetDialog($token);
			$dialogue_ids = array();
			foreach($dialogueList->items as $dialogue){
				if (in_array($receiver_chat_id, $dialogue->occupants_ids)) {
	    				if($dialogue->type == 3){
	    					$dialogue_ids[] = $dialogue->_id;
	    					$delete_status = $this->delete_dialogue($token,$dialogue_ids);
	    				}else if($dialogue->type == 2){
	    					if($dialogue->user_id == $sender_id){
	    						$update_group_status = $this->update_dialogue($dialogue->_id,$dialogue->name,$receiver_chat_id);
	    					}elseif($dialogue->user_id == $receiver_chat_id){
	    						$update_group_status = $this->update_dialogue($dialogue->_id,$dialogue->name,$sender_id);
	    					}
	    				}
				}
			}
		}
		return $message;
	}
	public function delete_dialogue($token,$dialogue_ids){
		if(count($dialogue_ids) > 0){
			$dialogue_strings = implode(",",$dialogue_ids);
			$dialogue_url = $dialogue_strings.'.json';
			$curl = curl_init();
	        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog/'.$dialogue_url);
	        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
	        $response = curl_exec($curl);
	        if ($response) {
                return json_decode($response);
	        } else {
	                $error = curl_error($curl). '(' .curl_errno($curl). ')';
			        return $error;
	        }   
	        curl_close($curl);
		}
	}
	public function update_dialogue($token,$group_name,$userid){
		$data = array(
  			'name' => $group_name,
			'pull_all'=> array('occupants_ids' =>  array($userid)),
  		);
  		/////////
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog/'.$token);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        /////////
        $response = curl_exec($curl);
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error . "\n";
        }
        curl_close($curl);
	}
	public function update_userprofile($useremail,$profile_image,$mbToken){
		$tokenDetails = $this->quickLogin($useremail,$mbToken);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->getuserDetailsbyId($token,$user_id);
		$custom_data = json_decode($user_details->user->custom_data);
		$status = "";
		$is_import = "true";
		if(isset($custom_data->status) && $custom_data->status != ""){
			$status = $custom_data->status;
		}
		$profile_image_details = $this->update_profile_image($token,$user_id,$profile_image,$status,$is_import);
		return $profile_image_details;
	}
	public function update_profile_image($token,$user_id,$profile_image,$status,$is_import){
  		$update_array = array('avatar_url' => $profile_image ,
  								'status' => $status,
  								'is_import' => $is_import,
  							);
  		$data["user"]["custom_data"] = json_encode($update_array);
  		/////////
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$user_id.'.json');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
        /////////
        $response = curl_exec($curl);
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error . "\n";
        }
        curl_close($curl);
	}
	
	public function showAllMessages($token,$chatid){
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json?chat_dialog_id='.$chatid);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'QuickBlox-REST-API-Version: 0.1.0',
            'QB-Token: ' . $token
        ));
        $response = curl_exec($curl);
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error;
        }
        curl_close($curl);
	}
	
	public function getuserDetailsbyId($token,$userId){
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$userId.'.json');
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error;
		        }
		        curl_close($curl);
		}

	/*
	-----------------------------------------------------------------------------------------
	On: 24-08-2016
	I/P: $userData = array()
	O/p: 
	Desc: Update user details on QB like profile image, status etc
	-----------------------------------------------------------------------------------------
	*/
	public function updateCustomData($userData = array()){
		$tokenDetails = $this->quickLogin($userData['userEmail'], $userData['userPassword']);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->getuserDetailsbyId($token,$user_id);
		$custom_data = json_decode($user_details->user->custom_data);
		$status = "";
		$is_import = "true";
		//** Set status of QB custom data[START]
		if(!empty($userData['userRoleStatus'])){
			$userData['userRoleStatus'] = $userData['userRoleStatus'];
		}
		else if(isset($custom_data->status) && $custom_data->status != ""){
			$userData['userRoleStatus'] = $custom_data->status;
		}else{
			$userData['userRoleStatus'] = "";
		}
		//** Set status of QB custom data[END]

		//** Set avatar of QB custom data[START]
		if(!empty($userData['userProfileImgPath'])){
			$userData['userProfileImgPath'] = $userData['userProfileImgPath'];
		}
		else if(isset($custom_data->avatar_url) && $custom_data->avatar_url != ""){
			$userData['userProfileImgPath'] = $custom_data->avatar_url;
		}else{
			$userData['userProfileImgPath'] = "";
		}
		//** Set avatar of QB custom data[END]
		//** Update Available custom data[START]
		if(isset($custom_data->at_work) && !empty($custom_data->at_work)){
			$userData['at_work'] = $custom_data->at_work;
		}else{
			$userData['at_work'] = "0";
		}
		//** Update Available custom data[END]
		//** Update oncall custom data[START]
		if(isset($custom_data->on_call) && !empty($custom_data->on_call)){
			$userData['on_call'] = $custom_data->on_call;
		}else{
			$userData['on_call'] = "0";
		}
		//** Update oncall custom data[END]
		$customData['profileImage'] = $userData['userProfileImgPath'];
		$customData['roleStatus'] = $userData['userRoleStatus'];
		$customData['isImport'] = $is_import;
		$customData['at_work'] = $userData['at_work'];
		$customData['on_call'] = $userData['on_call'];
		$profile_image_details = $this->updateCustomDataOnQb($token,$user_id, $customData);
		return $profile_image_details;
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 24-08-2016
	I/P: $token , $user_id , $customData = array()
	O/p: 
	Desc: Update users custom data on QB
	-----------------------------------------------------------------------------------------
	*/
	public function updateCustomDataOnQb($token = NULL, $user_id = NULL, $customData = array()){
		$profile_image = $customData['profileImage'];
		$status = $customData['roleStatus'];
		$is_import = $customData['isImport'];
		$at_work = $customData['at_work'];
		$on_call = $customData['on_call'];
  		$update_array = array('avatar_url' => $profile_image ,
  								'status' => $status,
  								'is_import' => $is_import,
  								'at_work'=> $at_work,
  								'on_call'=> $on_call
  							);
  		$data["user"]["custom_data"] = json_encode($update_array);
  		/////////
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$user_id.'.json');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
        /////////
        $response = curl_exec($curl);
        //echo "<pre>"; print_r($response);exit;
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error . "\n";
        }
        curl_close($curl);
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 24-08-2017
	I/P: $token , $user_id , $customData = array()
	O/p: 
	Desc: Update users custom data on QB
	-----------------------------------------------------------------------------------------
	*/
	public function updateCustomDataOnQbLite($token = NULL, $user_id = NULL, $customData = array()){
		$profile_image = $customData['userProfileImgPath'];
		$status = $customData['userRoleStatus'];
		$is_import = $customData['isImport'];
		$at_work = $customData['at_work'];
		$on_call = $customData['on_call'];
		$dnd_data = isset($customData['dnd_status']) ? $customData['dnd_status'] : "";
		if( isset($customData['is_dnd_active']) && ($customData['is_dnd_active'] == 1))
		{
			$update_array = array('avatar_url' => $profile_image,
  								'status' => $status,
  								'is_import' => $is_import,
  								'at_work' => $at_work,
  								'on_call' => $on_call,
  								'dnd_status' => $dnd_data
  							);
		}
		else
		{
			$update_array = array('avatar_url' => $profile_image ,
  								'status' => $status,
  								'is_import' => $is_import,
  								'at_work' => $at_work,
  								'on_call' => $on_call
  							);
		}
  		$data["user"]["custom_data"] = json_encode($update_array);
  		/////////
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$user_id.'.json');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
        /////////
        $response = curl_exec($curl);
        //echo "<pre>"; print_r($response);exit;
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error . "\n";
        }
        curl_close($curl);
	}

	/*
	-------------------------------------------------------------------------------------
	On: 19-09-2017
	I/P: JSON
	O/P: JSON
	Desc: update phone number on qb
	-------------------------------------------------------------------------------------
	*/

	public function updatePhoneOnQuickBlox($token,$user_id,$contact_no){
  		$data["user"]['phone'] = $contact_no;
  		/////////
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$user_id.'.json');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
        /////////
        $response = curl_exec($curl);
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error . "\n";
        }
        curl_close($curl);
	}

	/*
	-------------------------------------------------------------------------------------
	On: 10-09-18
	I/P: JSON
	O/P: JSON
	Desc: User dialogue
	-------------------------------------------------------------------------------------
	*/
	// public function quickGetDialogLite($token, $fromDate, $toDate) {
	//         $curl = curl_init();
	//         curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json?last_message_date_sent[gte]='.$fromDate.'&last_message_date_sent[lte]='.$toDate.'&sort_asc=date_sent&limit=500');
	//         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	//         curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	//         curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	//             'QuickBlox-REST-API-Version: 0.1.0',
	//             'QB-Token: ' . $token
	//         ));
	//         $response = curl_exec($curl);
	//         if ($response) {
	//                 return json_decode($response);
	//         } else {
	//                 $error = curl_error($curl). '(' .curl_errno($curl). ')';
	//                 return json_decode($error) ;
	//         }
	//         curl_close($curl);
	// }

	public function quickGetDialogLite($token, $fromDate, $toDate) {
	        $curl = curl_init();
	        $condition = QB_API_ENDPOINT.'chat/Dialog.json?sort_desc=last_message_date_sent';
	        if($fromDate != '' && $toDate != ''){
	        	$condition .= $condition.'&last_message_date_sent[gte]='.$fromDate.'&last_message_date_sent[lte]='.$toDate;
	        }
	        curl_setopt($curl, CURLOPT_URL, $condition);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
	        $response = curl_exec($curl);
	        if ($response) {
                $response =  json_decode($response);
                usort($response->items, function($a, $b)
                {

                    if ($a->last_message_date_sent == $b->last_message_date_sent) return 0;
                    return ($a->last_message_date_sent < $b->last_message_date_sent) ? 1 : -1;
                });

                return $response;

	        } else {
	                $error = curl_error($curl). '(' .curl_errno($curl). ')';
	                return json_decode($error) ;
	        }
	        curl_close($curl);
	}

	public function quickGetMessageLite($token, $dialogId, $fromDate, $toDate) {
	        $curl = curl_init();
	        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json?chat_dialog_id=' . $dialogId.'&date_sent[gte]='.$fromDate.'&date_sent[lte]='.$toDate.'&sort_asc=date_sent&limit=500');
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
	        $response = curl_exec($curl);
	
	        if ($response) {
	                return json_decode($response);
	        } else {
	                echo false;
	        }
	        curl_close($curl);
	}

	public function quickGetDialogData($token,$dialog_id) {
	        $curl = curl_init();
	        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json?_id='.$dialog_id);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
	        $response = curl_exec($curl);
	        if ($response) {
	                return json_decode($response);
	        } else {
	                $error = curl_error($curl). '(' .curl_errno($curl). ')';
	                return json_decode($error) ;
	        }
	        curl_close($curl);
	}

	public function getUserSubscription($token){
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'subscriptions.json');  
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error;
		        }
		        curl_close($curl);
		}

	/*
	-------------------------------------------------------------------------------------
	On: 19-07-2019
	I/P: JSON
	O/P: JSON
	Desc: update different data on QB (name, email, phone)
	-------------------------------------------------------------------------------------
	*/

	public function updateParamOnQuickBlox($token, $user_id, $data){
		if($data['type'] == 'email'){
			$update_data["user"]['email'] = $data['value'];
			$update_data["user"]['Login'] = $data['value'];
		}else if($data['type'] == 'phone'){
			$update_data["user"]['phone'] = $data['value'];
		}else if($data['type'] == 'name'){
			$update_data["user"]['full_name'] = $data['value'];
		}

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$user_id.'.json');
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($update_data));
		//curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
					'QuickBlox-REST-API-Version: 0.1.0',
					'QB-Token: ' . $token
			));

		$response = curl_exec($curl);
		if ($response) {
						return $response;
		} else {
						$error = curl_error($curl). '(' .curl_errno($curl). ')';
						return $error . "\n";
		}
		curl_close($curl);
	}

}
?>
