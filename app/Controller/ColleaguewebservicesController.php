<?php
/*
 * Colleague controller.
 *
 * This file will render views from views/Colleaguewebservices/
 *
 
 */

App::uses('AppController', 'Controller');


class ColleaguewebservicesController extends AppController {
	public $uses = array('User', 'UserColleague', 'NotificationUser', 'UserNotificationSetting', 'NotificationLog', 'UserFollow', 'NotificationLastVisit');
	public $components = array('Common','Quickblox');
	
	/*
	------------------------------------------------------------------------------------------------
	On: 04-11-2015
	I/P: JSON (logggedin user id and userid of the user invited as a colleague)
	O/P: JSON (message of success of Fail)
	Desc: Invite as colleague by any user.
	------------------------------------------------------------------------------------------------
	*/
	public function inviteColleague(){
		$responseData = array();
		//** As disscussed on MB sprint5 (12-08-2016), remove colleague invite from OCR[START]
				$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'641', 'message'=> ERROR_641);
				$encryptedData = $this->Common->encryptData(json_encode($responseData));
				echo json_encode(array("values"=> $encryptedData));
				exit;
		//** As disscussed on MB sprint5 (12-08-2016), remove colleague invite from OCR[END]
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true); 
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if( $this->User->findById( $dataInput['user_id'] ) ){ //** Check logged in user
						if( $this->User->findById( $dataInput['colleague_user_id'] ) ){ //** Check colleague user
							//** Send colleague request
							$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'])));
							$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id'])));
								$colleagueData = array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id']);
								try{
									if( $myColleague == 0 && $meColleague == 0 ){ //** Check if already colleague
										$inviteColleague = $this->UserColleague->save( $colleagueData );
										$sendCollegueNotifyUserId = $dataInput['colleague_user_id'];
									}else{
										$inviteColleague = 0;
										if( $meColleague > 0){
											//$colleagueData = array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id'] );
											$checkColleagueStatus = $this->UserColleague->find("first", array("conditions"=> $colleagueData)); 
											if( $checkColleagueStatus['UserColleague']['status'] == 1){
												$responseData = array('method_name'=> 'inviteColleague', 'status'=>"0", 'response_code'=> "ERROR_626", 'message'=> ERROR_626);
											}elseif( $checkColleagueStatus['UserColleague']['status'] == 2 ){
												$responseData = array('method_name'=> 'inviteColleague', 'status'=>"0", 'response_code'=> "ERROR_630", 'message'=> ERROR_630);
											}elseif( ($checkColleagueStatus['UserColleague']['status'] == 3) || ($checkColleagueStatus['UserColleague']['status'] == 0) ){
												//$inviteColleague = $this->UserColleague->updateAll( array("status"=> 2), $colleagueData );
												$inviteColleague = $this->UserColleague->updateAll(array("status"=> 2), $colleagueData );
											}
											$sendCollegueNotifyUserId = $dataInput['colleague_user_id'];
										}else{
											$colleagueData = array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id'] );
											$checkColleagueStatus = $this->UserColleague->find("first", array("conditions"=> $colleagueData));
											if( $checkColleagueStatus['UserColleague']['status'] == 1){
												$responseData = array('method_name'=> 'inviteColleague', 'status'=>"0", 'response_code'=> "ERROR_626", 'message'=> ERROR_626);
											}elseif( $checkColleagueStatus['UserColleague']['status'] == 2 ){
												$responseData = array('method_name'=> 'inviteColleague', 'status'=>"0", 'response_code'=> "ERROR_630", 'message'=> ERROR_630);
											}elseif( ($checkColleagueStatus['UserColleague']['status'] == 3) || ($checkColleagueStatus['UserColleague']['status'] == 0) ){
												//$inviteColleague = $this->UserColleague->updateAll( array("status"=> 2), $colleagueData );
												$inviteColleague = $this->UserColleague->updateAll( array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'],"status"=> 2), array("id"=> $checkColleagueStatus['UserColleague']['id']) );
											}
											$sendCollegueNotifyUserId = $dataInput['colleague_user_id'];
										}
									}
									if( $inviteColleague ){
										$responseData = array('method_name'=> 'inviteColleague', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
										//** Send Notification [START]
										$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $sendCollegueNotifyUserId, "status"=> 1 )));
										
											$userDetails = $this->User->findById( $dataInput['user_id'] );
											$userDevices = $this->NotificationUser->find("all", array("conditions"=> array("user_id"=> $sendCollegueNotifyUserId, "status"=> 1)));
											foreach( $userDevices as $userDevice ){
												$userRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];
											}
												$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " wants to add you as a colleague.";
											try{
													$notifyResponse = ""; $notificationId = 0; $totUnreadNotificationCount = 0;
													if($dataInput['colleague_user_id'] !=$dataInput['user_id']){ //** Don't save self notification
														$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $dataInput['colleague_user_id'], "item_id"=> '' ,"notify_type"=> "colleagueInvitation" ,"content"=> $message ,"api_response"=> $notifyResponse );
														$this->NotificationLog->save( $notifyLogData );
														$notificationId = $this->NotificationLog->getLastInsertId();
													}
													if( ($notificationSetting == (int) 0) && ($dataInput['colleague_user_id'] !=$dataInput['user_id'])){ //** Check user notification setting ON/OFF and not to self user
														//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $dataInput['colleague_user_id'], "read_status"=> 0)));
														$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
														$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
														$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "item_id"=> 0 ,"notify_type"=> "colleagueInvitation", "tot_unread_notification"=>$totUnreadNotificationCount) );
													}
													$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
													
												}catch( Exception $e ){
													//$notifyLogData = array("by_user_id"=> $dataInput['colleague_user_id'], "to_user_id"=> $dataInput['user_id'], "item_id"=> '' ,"notify_type"=> "colleagueInvitation" ,"content"=> $message ,"api_response"=> $e->getMessage() );
													//$this->NotificationLog->save( $notifyLogData );
													//exit;
												}
									
										//** Send Notification [END]
									}
									/*else{
										$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
									}*/
								}catch( Exception $e ){
									$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
								}
								//** If any user send colleague request also add user follow
								try{
									$conditions = array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id']);
									$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
									if($userFollowCheck == 0 ){
										$this->UserFollow->save( array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id'], "follow_type"=> 1, "status"=> 1) );
									}else{
										$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
									}
								}catch( Exception $e ){}
								
							/*}else{
								$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=> "626", 'message'=> ERROR_626);
							}*/
			    		}else{
			    			$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'625', 'message'=> ERROR_625);
			    		}

			    	}else{
			    		$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'inviteColleague','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 04-11-2015
	I/P: JSON Data ( user id, colleague id with status )
	O/P: JSON data ( Success or Fail message )
	Desc: Change status of colleague like accept, deny etc.
	------------------------------------------------------------------------------------------------
	*/
	public function colleagueStatusChange(){
		$responseData = array();
		//** As disscussed on MB sprint5 (12-08-2016), remove colleague invite from OCR[START]
				$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'641', 'message'=> ERROR_641);
				$encryptedData = $this->Common->encryptData(json_encode($responseData));
				echo json_encode(array("values"=> $encryptedData));
				exit;
		//** As disscussed on MB sprint5 (12-08-2016), remove colleague invite from OCR[END]
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if( $this->User->findById( $dataInput['user_id'] ) ){ //** Check logged in user
						if( $this->User->findById( $dataInput['colleague_user_id'] ) ){ //** Check colleague user
							//$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'])));
							$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id'])));
							//if( $myColleague > 0  ){ //** Check if already colleague
								if( $meColleague > 0){
									$colleagueCondition = array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['colleague_user_id']);
								}else{
									$colleagueCondition = array("user_id"=> $dataInput['colleague_user_id'], "colleague_user_id"=> $dataInput['user_id']);
								}
								try{
									$updateColleague = $this->UserColleague->updateAll( array("status"=> (int) $dataInput['colleagueStatus']), $colleagueCondition );
									if( $updateColleague ){
										$responseData = array('method_name'=> 'colleagueStatusChange', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
										if( $dataInput['colleagueStatus'] == 1 ){
											$acceptType = "accepted";
										//** Send Notification [START]
										$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'], "status"=> 1 )));
											$userDetails = $this->User->findById( $dataInput['user_id'] );
											$userDevices = $this->NotificationUser->find("all", array("conditions"=> array("user_id"=> $dataInput['colleague_user_id'], "status"=> 1)));
											foreach( $userDevices as $userDevice ){
												$userRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];
											}
												
											$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " accepted your colleague request.";
											try{
													$notifyResponse = ""; $notificationId = 0; $totUnreadNotificationCount = 0;
													if($dataInput['user_id'] !=$dataInput['colleague_user_id']){ //** Don't save self notification
														$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $dataInput['colleague_user_id'], "item_id"=> '' ,"notify_type"=> "colleagueinvitationaccepted" ,"content"=> $message ,"api_response"=> $notifyResponse );
														$this->NotificationLog->save( $notifyLogData );
														$notificationId = $this->NotificationLog->getLastInsertId();
													}
													if( ($notificationSetting == (int) 0) && ($dataInput['user_id'] !=$dataInput['colleague_user_id'])){ //** Check user notification setting ON/OFF and not to self user
														//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $dataInput['colleague_user_id'], "read_status"=> 0)));
														$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
														$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
														$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "item_id"=> 0 ,"notify_type"=> "colleagueinvitationaccepted", "tot_unread_notification"=>$totUnreadNotificationCount) );
													}
													$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
												}catch( Exception $e ){
													if($dataInput['user_id'] != $dataInput['colleague_user_id']){
														$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $dataInput['colleague_user_id'], "item_id"=> '' ,"notify_type"=> "colleagueinvitationaccepted" ,"content"=> $message ,"api_response"=> $e->getMessage() );
														$this->NotificationLog->save( $notifyLogData );
													}
												}
											//** If any user accept colleague request auto follow
											try{
											$conditions = array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id']);
											$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
												if($userFollowCheck == 0 ){
												$this->UserFollow->save( array("followed_by"=> $dataInput['user_id'], "followed_to"=> $dataInput['colleague_user_id'], "follow_type"=> 1, "status"=> 1) );
												}else{
													$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
												}
											}catch( Exception $e ){}
										}		
										//** Send Notification [END]
										//** If any user send colleague request also add user follow
											try{
												$conditions = array("followed_by"=> $dataInput['colleague_user_id'], "followed_to"=> $dataInput['user_id']);
												$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
												if($userFollowCheck == 0 ){
													$this->UserFollow->save( array("followed_by"=> $dataInput['colleague_user_id'], "followed_to"=> $dataInput['user_id'], "follow_type"=> 1, "status"=> 1) );
												}else{
													$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
												}
											}catch( Exception $e ){}

											/* Accept/Remove friend  request Quickblox start */
											$senderDetails = $this->User->findById($dataInput['user_id'] );
											$receiverDetails = $this->User->findById($dataInput['colleague_user_id']);
											try{ 
												$this->Quickblox->quickAddRemoveDialog($senderDetails,$receiverDetails,$dataInput['colleagueStatus']);
											}catch( Exception $e ){}
											/* Accept/Remove friend request Quickblox end */

									}else{
										$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=> "615", 'message'=> ERROR_615);
									}
								}catch( Exception $e ){
									$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
								}
							/*}else{
								$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=> "629", 'message'=> ERROR_629);
							}*/
			    		}else{
			    			$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'625', 'message'=> ERROR_625);
			    		}

			    	}else{
			    		$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'colleagueStatusChange','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    } 
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	------------------------------------------------------------------------------------------------
	On: 03-11-2015
	I/P: JSON Data
	O/P: JSON data as colleague List
	Desc: Display all colleague list for any particular user.
	------------------------------------------------------------------------------------------------
	*/
	public function colleagueLists(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if( $this->User->findById( $dataInput['user_id'] ) ){ //** Check logged in user
						//** Code goes here
						$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
						$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
						$params['user_id'] = isset($dataInput['user_id']) ? $dataInput['user_id'] : DEFAULT_PAGE_SIZE;
						$params['colleagueStatus'] = isset($dataInput['colleagueStatus']) ? implode(",", $dataInput['colleagueStatus']) : 1;
						$colleagueRequestBy = isset($dataInput['request_by']) ? $dataInput['request_by'] : "all";
						$colleagueList = $this->colleagueData( $params, $colleagueRequestBy );
						if( !empty($colleagueList) ){
							$colleagueListData = $this->colleagueFields( $colleagueList );
							//** Total records and per page size
							$totColleagueCount = $this->colleagueDataCount($params, $colleagueRequestBy);
							$colleagueList = array('Colleagues'=> $colleagueListData, 'total_records'=> $totColleagueCount, 'size'=> $params['size']);
							$responseData = array('method_name'=> 'colleagueLists','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $colleagueList);
			    		}else{
			    			$responseData = array('method_name'=> 'colleagueLists','status'=>'0','response_code'=>'613', 'message'=> ERROR_613);
			    		}
			    	}else{
			    		$responseData = array('method_name'=> 'colleagueLists','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'colleagueLists','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'colleagueLists','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
	    //echo json_encode($responseData);
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 03-11-2015
	I/P: array()
	O/P: array() of colleague list
	Desc: Formatting colleague list Data.
	---------------------------------------------------------------------------------------
	*/
	public function colleagueFields( $colleagueList = array() ){
		$colleagueListData = array();
		foreach( $colleagueList as $colD ){ 
			$colleagueData = array(); $colleagueProfile = array();
			//** Colleague info
			$colleagueData['colleague_user_id'] = !empty($colD['UserColleague']['colleague_user_id']) ? $colD['UserColleague']['colleague_user_id'] : '';
			$colleagueData['status'] = !empty($colD['UserColleague']['status']) ? $colD['UserColleague']['status'] : '';
			//** Colleague profile Info
			$colleagueProfile['user_id'] = !empty($colD['UserProfile']['user_id']) ? $colD['UserProfile']['user_id'] : '';
			$colleagueProfile['first_name'] = !empty($colD['UserProfile']['first_name']) ? $colD['UserProfile']['first_name'] : '';
			$colleagueProfile['last_name'] = !empty($colD['UserProfile']['last_name']) ? $colD['UserProfile']['last_name'] : '';
			$colleagueProfile['profile_img'] = !empty($colD['UserProfile']['profile_img']) ? AMAZON_PATH . $colD['UserProfile']['user_id']. '/profile/' . $colD['UserProfile']['profile_img'] : '';
			$colleagueProfile['country'] = !empty($colD['Country']['country_name']) ? $colD['Country']['country_name'] : '';
			$colleagueProfile['profession'] = !empty($colD['Profession']['profession_type']) ? $colD['Profession']['profession_type'] : '';
			//** Colleague info array
			$colleagueListData[] = array('Colleague'=> $colleagueData, 'UserProfile'=> $colleagueProfile);
		}
		return $colleagueListData;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 03-11-2015
	I/P: array() as parameter
	O/P: array() of colleague data fetched from query
	Desc: Fetching all colleagues data from DB 
	---------------------------------------------------------------------------------------	
	*/
		public function colleagueData( $params = array(), $colleagueRequestBy = 'all' ){ 
			$colleagueList = array();
			if( !empty($params) ){
				App::import('model','UserColleague');
				$UserColleague = new UserColleague();
				$offsetVal = ( $params['page_number'] - 1 ) * $params['size'];
				if( !empty($params['name']) ){
						$conditions = ' AND User.status =  1 AND  User.approved = 1 AND ( LOWER(UserProfile.first_name) LIKE LOWER("%'.$params['name'].'%") ';
						//$conditions .= ' OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['name'].'%") ) LIMIT ' . $offsetVal . ',' . $params['size'];
						$conditions .= ' AND User.status =  1 AND  User.approved = 1 OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['name'].'%") ) ORDER BY UserProfile.first_name ASC ';	
				}else{
						//$conditions = " LIMIT " . $offsetVal . "," . $params['size'];
					$conditions = ' AND User.status =  1 AND  User.approved = 1 ORDER BY UserProfile.first_name ASC ';
				}
				if( $colleagueRequestBy == "from_me" ){
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT User.id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name , 
					UserProfile.profile_img , Country.country_name, Profession.profession_type,  UserColleague.colleague_user_id  , UserColleague.status 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					$colleagueList = $myColleague;
				}elseif( $colleagueRequestBy == "to_me" ){
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT User.id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name , 
					UserProfile.profile_img , Country.country_name, Profession.profession_type, UserColleague.user_id colleague_user_id , UserColleague.status  
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueList = $meColleague;
				}else{
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT User.id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name , 
					UserProfile.profile_img , Country.country_name, Profession.profession_type,  UserColleague.colleague_user_id  , UserColleague.status 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT User.id, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name , 
					UserProfile.profile_img , Country.country_name, Profession.profession_type, UserColleague.user_id colleague_user_id , UserColleague.status  
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueList = array_merge( $myColleague , $meColleague); 
				}
			}
			$slicedArr = array_slice($colleagueList, $offsetVal, $params['size']); //** Fetch array size
			$slicedArr = $this->arraySort($slicedArr, 'first_name', SORT_ASC); //** Sort By First name
			//echo "<pre>";print_r($slicedArr);die;
			return $slicedArr;
		}

	/*
	------------------------------------------------------------------------------------------------
	On: 19-11-2015
	I/P: JSON Data
	O/P: JSON data as colleague List
	Desc: Display all colleague list search by first_name, last_name of any particular user.
	------------------------------------------------------------------------------------------------
	*/
	public function colleagueSearch(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if( $this->User->findById( $dataInput['user_id'] ) ){ //** Check logged in user
						//** Search colleague list
						$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
						$params['size'] = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
						$params['user_id'] = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
						$params['colleagueStatus'] = 1;
						$params['name'] = isset($dataInput['name']) ? $dataInput['name'] : '';
						$colleagueRequestBy = isset($dataInput['request_by']) ? $dataInput['request_by'] : 'all';
						$colleagueList = $this->colleagueData( $params, $colleagueRequestBy );
						$colleagueListData = $this->colleagueFields( $colleagueList );
						//** Total records and per page size
						$totColleagueCount = $this->colleagueDataCount($params, $colleagueRequestBy);
						$colleagueList = array('Colleagues'=> $colleagueListData, 'total_records'=> $totColleagueCount, 'size'=> $params['size']);
						$responseData = array('method_name'=> 'colleagueSearch','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $colleagueList);
			    	}else{
			    		$responseData = array('method_name'=> 'colleagueSearch','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'colleagueSearch','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'colleagueSearch','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
	    $encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
    	exit;
	}
	/*
	----------------------------------------------
	On: 
	I/P: 
	O/P:
	Desc: 
	----------------------------------------------
	*/
	public function arraySort($array, $on, $order=SORT_ASC)
	{
		$new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
            	foreach ($v as $k2 => $v2) {
            		if (is_array($v2)) {
                foreach ($v2 as $k3 => $v3) {
                    if ($k3 == $on) {
                        $sortable_array[$k] = $v3;
                    }
                }
              } else {
                $sortable_array[$k2] = $v2;
            	} 
            } 
            } else {
                $sortable_array[$k] = $v;
            }
        	
        }
        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    	
    }
    return $new_array;
	}

	/*
	----------------------------------------------------------------------
	On: 22-04-2016
	I/P: 
	O/P: 
	Desc: Fetch total colleague count
	----------------------------------------------------------------------
	*/
	public function colleagueDataCount( $params = array(), $colleagueRequestBy = 'all' ){ 
			$colleagueListCount = 0;
			if( !empty($params) ){
				App::import('model','UserColleague');
				$UserColleague = new UserColleague();
				
				if( !empty($params['name']) ){
						$conditions = ' AND User.status =  1 AND  User.approved = 1 AND ( LOWER(UserProfile.first_name) LIKE LOWER("%'.$params['name'].'%") ';
						//$conditions .= ' OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['name'].'%") ) LIMIT ' . $offsetVal . ',' . $params['size'];
						$conditions .= ' AND  OR LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['name'].'%") ) ORDER BY UserProfile.first_name ASC ';	
				}else{
						//$conditions = " LIMIT " . $offsetVal . "," . $params['size'];
					$conditions = ' AND User.status =  1 AND  User.approved = 1 ORDER BY UserProfile.first_name ASC ';
				}
				if( $colleagueRequestBy == "from_me" ){
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT count(*) AS totCnt 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					$colleagueListCount = $myColleague[0][0]['totCnt'];
				}elseif( $colleagueRequestBy == "to_me" ){
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT count(*)  AS totCnt  
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueListCount = $meColleague[0][0]['totCnt'];
				}else{
					//** Colleague list invited by me
					$myColleagueQuer = "SELECT count(*)  AS totCnt 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.colleague_user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id )
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id ) 
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.user_id = " . $params['user_id'] . $conditions;
					$myColleague = $UserColleague->query( $myColleagueQuer );
					//** Colleague List Invited to me
					$meColleagueQuer = "SELECT count(*)  AS totCnt 
					FROM user_colleagues UserColleague 
					INNER JOIN users User ON ( User.id = UserColleague.user_id ) 
					INNER JOIN user_profiles UserProfile ON ( UserProfile.user_id = User.id ) 
					LEFT JOIN countries Country ON ( UserProfile.country_id = Country.id ) 
					LEFT JOIN professions Profession ON ( UserProfile.profession_id = Profession.id )   
					WHERE 
					User.status = 1 AND UserColleague.status IN (" . $params['colleagueStatus']. ") AND UserColleague.colleague_user_id = " . $params['user_id'] . $conditions;
					$meColleague = $UserColleague->query( $meColleagueQuer );
					$colleagueListCount = $myColleague[0][0]['totCnt'] + $meColleague[0][0]['totCnt']; 
				}
			}
			return $colleagueListCount;
		}

}
