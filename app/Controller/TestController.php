<?php


App::uses('AppController', 'Controller');

class TestController extends AppController {
	public $uses = array('Content','UserPost', 'Mbapi.User', 'Mbapi.UserProfile','Mbapi.UserDutyLog', 'Mbapi.UserEmployment', 'Mbapi.EnterpriseUserList', 'Mbapi.UserSubscriptionLog', 'Mbapi.UserOneTimeToken', 'UserDevice', 'Country', 'PostUserTag', 'TestTable', 'UserProfileCheck', 'NotificationLastVisit', 'NotificationLog', 'InstitutionInformation', 'UserQbDetail', 'UserColleague', 'Profession', 'NpiUsaUser', 'GmcUkUser', 'GmcUkExtraUser','Mbapi.CompanyGeofenceParameter','Mbapi.UserProfile','Mbapi.Profession','Mbapi.RoleTag','Mbapi.BatonRole','Mbapi.UserBatonRole','Mbapi.DepartmentsBatonRole','Mbapi.PreQualifiedDomain','Mbapiv1.CompanyName');
	public $components = array('Common', 'Quickblox','Email','Mb.MbEmail','Cache');
	public static	$beatIds = array();

	public function beforeFilter(){
		header("Access-Control-Allow-Origin: *");
    	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, If-Modified-Since, Cache-Control, Pragma");
	}
	/*
	Test Encrypt method
	*/
	public function testEncrypt(){
		$encryptedData = $this->Common->encryptData( $_REQUEST['values'] );
		echo $encryptedData;
		exit;
	}

	/*
	Test Decrypt method
	*/
	public function testDecrypt(){
		$decryptedData = $this->Common->decryptData( $_REQUEST['values'] );
		echo $decryptedData;
		exit;
	}

	/*
	Test GCM message send
	*/
	public function testGcmSend(  ){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$ids = $dataInput['registration_ids'];
			$msg = $dataInput['message'];
			$data = array( 'message' => $msg );
			$gcmResponse = $this->Common->sendGcmMessage( $ids, $data );
			$responseData = array('method_name'=> 'testGcmSend', 'status'=>"1", 'response_code'=> "200", 'message'=> $gcmResponse);
		}else{
			$responseData = array('method_name'=> 'testGcmSend', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	Test get tagged user for post
	*/
	public function testTaggedUser(){
		$postId = 1;
		$taggedUsers = $this->PostUserTag->postTaggedUsers( $postId );
		echo "<pre>"; print_r( $taggedUsers );
		exit;
	}

	/*
	Hash tag extract check
	*/
	public function testHashTag(  ){
		$contents = "Here we get info of #Taj , #Tajmahal. Also More about #Agra.";
		$hashTags = $this->Common->getHashTags( $contents );
		echo "<pre>";print_r($hashTags);
		exit;
	}

	/*
	Check encrypted data insert into fields.
	*/
	public function addEncryptedData(){
		$text = "Nimay";
		$key = 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd';
		$textVal = "AES_ENCRYPT($text, $key)";
		App::uses('ConnectionManager', 'Model');
		$db = ConnectionManager::getDataSource('default');
		$data = array( 'content' => $db->expression("AES_ENCRYPT('$text', '$key')") );
		//$data = "INSERT INTO test_tables VALUES('', AES_ENCRYPT('$text','$key'))";
		try{
			if($this->TestTable->save( $data )){
				echo "data added";
			}else{
				echo "data not added";
			}
		}catch( Exception $e ){
			echo $e->getMessage();
		}
		exit;
	}

	/*
	update  data as encrypted into fields.
	*/
	public function updateEncryptData(){
		$key = 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd';
		$userProfile = $this->UserProfileCheck->findById( 1 );
		$lastName = $userProfile['UserProfileCheck']['last_name'];
		App::uses('ConnectionManager', 'Model');
		$db = ConnectionManager::getDataSource('default');
		App::uses('Security', 'Utility');
		$data = array( 'last_name' => Security::rijndael(
                $lastName,
                $key,
                'encrypt'
                ) );
		try{
			if($this->UserProfileCheck->updateAll( $data , array("id"=> 1))){
			echo "data updated";
			}else{
				echo "data not updated";
			}
		}catch( Exception $e ){
			echo $e->getMessage();
		}
		exit;
	}

	/*
	Check decrypted data from table.
	*/
	public function checkDecryptDb(){
		$key = 'T+!Ks4hvQ+4zLGS^R=Nj_Xj9WR$s%WCd';
		$decryptedData = $this->TestTable->find("all", array("fields"=> array("id, CAST(AES_DECRYPT(content,'$key') AS CHAR(255)) AS encryptCheck")));
		echo "<pre>"; print_r($decryptedData);
		exit;
	}

	/*
	create pdf
	*/
	public function createTestPdf(){
		$this->Common->createConsentPdf();
		exit;
	}
	/*
		check post lists
	*/
	public function postListCheck(){
		$postSearchLists = $this->UserPost->postSearch(100, 1 , '');
		echo "<pre>"; print_r($postSearchLists);
		exit;
	}

	/*
		check user search check lists
	*/
	public function userSearchCheck(){
		$userSearchCheck = $this->User->userSearch(array("first_name"=> 'Al'));
		echo "<pre>"; print_r($userSearchCheck);
		exit;
	}

	/*

	*/
	public function checkSameSpecilityUser(){
		App::import('Controller', 'Postwebservices');
		$Postwebservices = new Postwebservices();
		$users = $Postwebservices->getUsersSameSpecility(7);
		exit;
	}

	public function checkArrayMerge(){
		$arr1 = array(1,2,3);
		$arr2 = array(3,4,5);
		$arr3 = array(5,6,7);
		$arr = array_unique(array_merge($arr1, $arr2, $arr3));
		echo "<pre>";print_r($arr);
		exit;
	}

	public function test(){
		$header = getallheaders();
		echo $header['access_key'];
		die;
	}



	public function getTld($url = "") {
		$rootDomain = "";
		$url = "abc.nhs.uk";
		if(!empty($url)){
		$fullUrl = "http://www." . $url;
		$pieces = parse_url($fullUrl);
		if (strpos($pieces['host'], "nhs.uk") !== false) {
			$rootDomain = "nhs.uk";
		}else{
			$domain = isset($pieces['host']) ? $pieces['host'] : '';
				if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $m)) {
					$rootDomain = $m['domain'];
				}
			}
		}
		echo $rootDomain;
		die;
	}

	/*
	Login test withour encryption
	*/
  public function loginCheck(){
  		header('Access-Control-Allow-Origin: *');
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			//$encryptedData = $this->Common->decryptData( $dataInput['values'] );
			//$dataInput = json_decode($encryptedData, true);
			//if($this->accesskeyCheck()){
				if( !empty($dataInput['email']) && !empty($dataInput['password']) ){
					$dataInput['email'] = strtolower($dataInput['email']);
					$params = array('email'=> $dataInput['email']);
					$userDetails = $this->User->userDetails( $params );
					if(  $userDetails['User']['email'] == $dataInput['email'] && $userDetails['User']['password'] == md5($dataInput['password']) ){
						if( $userDetails['User']['status'] == 1 ){
							if($userDetails['User']['approved'] == 1){
								$userData = $this->userFields( $userDetails );
								// Update Last Logged in Date
								$this->User->updateFields( array('User.last_loggedin_date'=> "'".date('Y-m-d H:i:s')."'"), array('User.id'=> $userDetails['User']['id']));
								// User Device data save
								$dataUserDevice['user_id'] = $userDetails['User']['id'];
								$dataUserDevice['device_id'] = isset($dataInput['device_id']) ? $dataInput['device_id']:'';
								$dataUserDevice['token'] = md5(strtotime(date('Y-m-d H:i:s')) . $userDetails['User']['id'] . rand(1000,100000));
								$dataUserDevice['device_type'] = isset($dataInput['device_type']) ? $dataInput['device_type']:'0';
								$userDeviceCheck = $this->UserDevice->find("count", array("conditions"=> array("user_id"=> $userDetails['User']['id'], "device_id"=> $dataInput['device_id'])));
								if($userDeviceCheck == 0){
									$this->UserDevice->save( $dataUserDevice );
									//** Inactive all other users device id
									$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataInput['device_id'], "user_id !="=> $userDetails['User']['id']));
								}else{
									$this->UserDevice->updateAll(array("status"=> 1), array("user_id"=> $userDetails['User']['id'], "device_id"=> $dataInput['device_id']));
									//** Inactive all other users device id
									$this->UserDevice->updateAll(array("status"=> 0), array("device_id"=> $dataInput['device_id'], "user_id !="=> $userDetails['User']['id']));
								}
								// Get Token
								$tokenParams = array(
													'user_id'=>  $userDetails['User']['id'],
													'device_id'=> $dataInput['device_id'],
													'status'=> 1
													);
								$tokenData = $this->UserDevice->tokenDetails( $tokenParams );
								$userData['token'] = $tokenData['UserDevice']['token'];
								$responseData = array('method_name'=> 'login', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('User'=> $userData));
							}else{
								$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "633", 'message'=> ERROR_633);
							}
						}else{
							$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "605", 'message'=> ERROR_605);
						}
					}else{
						$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "604", 'message'=> ERROR_604);
					}
				}else{
					$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "603", 'message'=> ERROR_603);
				}
			/*}else{
				$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}*/
		}else{
			$responseData = array('method_name'=> 'login', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		//$encryptedData = $this->Common->encryptData(json_encode($responseData));
		//echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
		Test Method
	*/
	public function userFields( $ud = array(), $userType = NULL ){
		$userData = array();
		if( !empty($ud) ){
				//** Get User Country
				$userCountry = '';
				if( !empty($ud['UserProfile']['country_id']) ){
					$countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
					$userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';
				}
				$userData = array(
									'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
									'first_time_loggedin'=> ($ud['User']['last_loggedin_date'] == '0000-00-00 00:00:00') ? "Y":"N",
									'first_name'=> !empty($ud['UserProfile']['first_name']) ? $ud['UserProfile']['first_name']:'',
									'last_name'=> !empty($ud['UserProfile']['last_name']) ? $ud['UserProfile']['last_name']:'',
									'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
									'country'=> !empty($userCountry) ? $userCountry:'',
									'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
								);
			}
		return $userData;
	}

	/*
		Test method
	*/
	public function testRecursive(){
		$beatId = 1;
		$ids = array();
		$this->checkRecursive($ids, $beatId);
		echo "<pre>"; print_r($ids);
		exit;
	}

	/*
		Test method
	*/
	public function checkRecursive( &$ids, $parentBeatId = NULL ){
		if(!empty($parentBeatId)){
			$this->UserPost->recursive = -1;
			$childBeat = $this->UserPost->find("all", array("conditions"=> array("UserPost.parent_post_id"=> $parentBeatId)));
			foreach( $childBeat as $cb ){
				if( empty($cb['UserPost']['id']) ){
					return 1;
				}else{
					$ids[] = $cb['UserPost']['id'];
					$this->checkRecursive($ids, $cb['UserPost']['id']);//** Call recursive
				}

			}
		}
		return $ids;
	}

	/*
		Test Shell
	*/
	public function checkShell(){
		//$this->autoRender = false;
		App::import('Console/Command', 'AppShell');
		App::import('Console/Command', 'MessageShell');
		$job = new MessageShell();
		$job->dispatchMethod('addCheck');

		echo "Running shell";
		exit;
	}

	/*
		Test
	*/
		public function testInfo(){
			echo $this->Common->getTld("wales.doctors.net");
			exit;
		}

	/*
		Test method
	*/
	public function userDetailsById($userId = NULL, $loggedinUserId = NULL){
		$userLists = array();
		if(!empty($userId)){
			$userData = $this->User->userDetailsByUserId($userId, $loggedinUserId);
			$userLists = $userData; //$this->userFieldData($userData, $loggedinUserId);
		}
		echo "<pre>"; print_r($userLists);
		exit;
	}

	/*
		Test method
	*/
		public function instititionData(){
			$instititionData = $this->InstitutionInformation->institutionInfo(300);
			echo "<pre>"; print_r($instititionData);
			exit;
		}

	/*
		Check signup

	*/
	public function checkSignup(){
		$this->request->data['User']['email'] = "test123@test.com";
		$userId = 501;
		$this->request->data['UserProfile']['first_name'] = "testfname";
		$this->request->data['UserProfile']['last_name'] = "testlname";
		$details = $this->Quickblox->quickAdduserFromOcr($this->request->data['User']['email'],'12345678',$this->request->data['User']['email'],$userId,$this->request->data['UserProfile']['first_name'].' '.$this->request->data['UserProfile']['last_name']);
		echo "<pre>"; print_r($details);
		exit;
	}

	/*

		check remote address
	*/
	public function checkRemoteAddress(){
		echo json_encode($_SERVER);
		exit;
	}

	/*
		Check
	*/
	public function checkHead(){
		$tinyUrl = $this->get_tiny_url("https://theoncallroom.com/userwebservices/allSpecilities");
		echo $tinyUrl;
		exit;
	}

	/*
		Test function to create tiny utl
	*/
	public function get_tiny_url($url)  {
		$ch = curl_init();
		$timeout = 5;
		//curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
		$postData = http_build_query(array(
		            'access_key' => "",
		            ));
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('access_key: KgGcxuClGLh6wSLlXi82KSotz6viIgohR/eyRnMn/mfKCcgK04obx9JU67+wH+to'));
		curl_setopt($ch, CURLOPT_POST, true); // Use POST
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $postData); // Setup post body
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$data = curl_exec($ch);
		return $data;
		curl_close($ch);
		exit;
	}

	/*

	*/
	public function checkQBUser($userEmail = NULL){
		$token = $this->Quickblox->quickAuth();
		$curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/by_email.json?email=' . $userEmail);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);

		        if (!empty($response)) {
		                return $response;
		        } else {
		                return false;
		        }
		        curl_close($curl);

		 die;
	}

	/*
		add/update qb id for users
	*/
	public function checkQBuserByEmail(){
		$totAdded =0; $totUpdated = 0;
		$userDetails = $this->User->find("all", array("order"=> array("User.id")));
		foreach($userDetails as $ud){
			$userId = $ud['User']['id'];
			$userEmail = $ud['User']['email'];
			//** Initialize variables
			$responseData = ''; $qbDetailsData = array(); $addQbDetailsData = array(); $checkUserCount=0; $updateQbDetailsData =array();
			$checkUserCount = $this->UserQbDetail->find("count", array("conditions"=> array("user_id"=> $userId)));
			$responseData = $this->checkQBUser($userEmail);
			$qbDetailsData = json_decode($responseData, true);
			//** if user exists in our DB and not in QB add user to QB[START]
			if(empty($qbDetailsData)){
				$qbDetails = $this->Quickblox->quickAdduserFromOcr($userEmail, '12345678', $userEmail, $userId, $ud['UserProfile']['first_name'].' '.$ud['UserProfile']['last_name']);
				$responseData = $this->checkQBUser($userEmail);
				$qbDetailsData = json_decode($responseData, true);
			}
			//** if user exists in our DB and not in QB add user to QB[END]
			if($checkUserCount==0){
				if(!empty($qbDetailsData)){
					$addQbDetailsData = array("user_id"=> $userId, "qb_id"=> $qbDetailsData['user']['id']);
					$this->UserQbDetail->saveAll($addQbDetailsData);
					$totAdded++;
				}
			}else{
				if(!empty($qbDetailsData)){
					$updateQbDetailsData = array("qb_id"=> $qbDetailsData['user']['id']);
					$updateConditions = array("user_id"=> $userId);
					$this->UserQbDetail->updateAll($updateQbDetailsData, $updateConditions);
					$totUpdated++;
				}
			}

		}
		echo "Total Added: " . $totAdded . " ". " Total Updated: ". $totUpdated ;
		exit;
	}


	/*
		Test method to update QB details
	*/
	public function updateQbDetails(){
		//$token = $this->Quickblox->quickAuth();
		$tokenFieldsObj = $this->Quickblox->quickLogin('hellodear@yopmail.com','12345678');
		echo "<pre>"; print_r($tokenFieldsObj);die;
		$tokenFieldsArr = json_decode(json_encode($tokenFieldsObj), true);
		$token = $tokenFieldsArr['session']['token'];
		$updateData = array(
		            'user[full_name]' => "Nimay",
		        );
		$userProfileImage = 'https://ocr1dev.s3-us-west-2.amazonaws.com/1/profile/231fcdc2c9852252b4f93efb547adcf3.jpg';
		$customData = array(
							//'avatar_url' => $userProfileImage,
							'status'=> 'Hi I am Fine',
							'Profession'=> 'Doctor'
						);
		$updateData["user"]["custom_data"] = json_encode($customData);
		$postData = http_build_query($updateData);
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/802.json');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
        $response = curl_exec($curl);
		        echo "<pre>"; print_r($response);die;
		        if (!empty($response)) {
		                return $response;
		        } else {
		                return false;
		        }
		        curl_close($curl);
		exit;
	}


	/*
		Test email send
	*/
	public function testEmailSend(){
		$Email = new CakeEmail();
		//$Email->config('gmail');
		$Email->config('mailchimp');
		$params['subject'] = 'Check Mail Send';
		$Email->template('verify_email', '');
		$Email->emailFormat('html');
		$Email->from(array('test@medicbleep.com' => "Medic Bleep"));
		$Email->to( array('nimay@mediccreations.com') );
		$Email->subject( $params['subject'] );
		$message = '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:493px">
	<tbody>
		<tr>
			<td><strong>Dear {%NAME%}, </strong>
			<p>Your password has been changed successfully.<br />
			New password is: {%PASS%}</p>
			</td>
		</tr>
		<tr>
			<td>
			<table border="0" cellpadding="0" cellspacing="0" style="width:493px">
				<tbody>
					<tr>
						<td>Thanks,<br />
						<br />
						{%SITE_NAME%}</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>
';
$Email->viewVars(array('msg' => $message));
		try{
				if($Email->send()){
					$msg = 'mail sent';
				}else{
					$msg = 'mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			echo $msg;
		exit;
	}

	/*
		Update user profile image for all users(its one time task)
	*/
	public function updateProfileImageOnQB(){
		$conditions = array("User.status"=> 1, "User.approved"=> 1);
		$userData = $this->User->find("all", array("conditions"=> $conditions));
		//echo "<pre>"; print_r($userData);die;
		$totImgFound = 0;
		foreach($userData as $ud){
			$userId = $ud['User']['id'];
			$userEmail = $ud['User']['email'];
			$userProfileImage = !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $userId . '/profile/' . $ud['UserProfile']['profile_img'] : '';
			$userProfileImagePath = "";
				if(@getimagesize($userProfileImage)){
					$userProfileImagePath = $userProfileImage;
					$totImgFound++;
				}
			$updateProfileImg = $this->Quickblox->update_userprofile($userEmail, $userProfileImagePath);
		}
		//echo "<pre>"; print_r($updateProfileImg);
		echo $totImgFound;
		exit;
	}

	/*
		Update user role_status as profession who have not set role_status(its one time task)
	*/
	public function updateRoleStatusOnQB(){
		//$conditions = array("User.status"=> 1, "User.approved"=> 1);
		$userData = $this->User->find("all");
		//echo "<pre>"; print_r($userData);die;
		$totUserProfessionUpdated = 0;
		foreach($userData as $ud){
			$userId = $ud['User']['id'];
			$userEmail = $ud['User']['email'];
			$qbPass = "12345678";
			//** Set profession as status on QB[START]
				$userDetails = $this->User->findById($userId);
				$professionData = $this->Profession->findById($userDetails['UserProfile']['profession_id']);
				if(empty($userDetails['UserProfile']['role_status'])){
					$profession = !empty($professionData['Profession']['profession_type']) ? $professionData['Profession']['profession_type'] : "";
					$userCustomQbData = array("userEmail"=> $userEmail, "userPassword"=> $qbPass, "userRoleStatus"=> $profession);
					$customDataQb = $this->Quickblox->updateCustomData($userCustomQbData);
					$totUserProfessionUpdated++;
				}
			//** Set profession as status on QB[END]
		}
		echo $totUserProfessionUpdated;
		exit;
	}

	/*
		Test
	*/
	public function testEmpty(){
		$chk = 0;
		if(!empty($chk)){
			echo "if";
		}else{
			echo "else";
		}
		exit;
	}

	/*
		Check Mandrill APIs
	*/
	public function mandrillMailSend(){
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));

			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');

			$template_name = 'Welcome Template';
    $template_content = array(
        array(
            'name' => 'Welcome Template',
            'content' => 'example content'
        )
    );
    $message = array(
	        'html' => '<p>Example HTML content</p>',
	        'text' => 'Example text content',
	        'subject' => 'example subject',
	        'from_email' => 'test@medicbleep.com',
	        'from_name' => 'Test',
	        'to' => array(
	            array(
	                'email' => 'nimay@mediccreations.com',
	                'name' => 'Recipient Name',
	                'type' => 'to'
	            )
	        ),
	        "global_merge_vars"=> array(
	        		array(
                	'name' => 'name',
                	'content' => 'Nimay'
            		),
            		array(
                	'name' => 'regards',
                	'content' => 'MB'
            		)
	        	)
        );

    $async = false;
    $ip_pool = 'Main Pool';
    $send_at = '07-09-2016';

        $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
    print_r($result);
		exit;
	}

	/*

	*/
	public function getFiles(){
		//** Get All csv files[START]
		$folderName = APP . 'webroot/NPI/';
		$files = $this->getDirContents($folderName);
		foreach($files as $f){
			$filePath = explode('/', $f);
			$fileName = end($filePath);
			$getExt = explode('.', $fileName);
			$fileExt = end($getExt);
			if($fileExt == 'csv'){
				$csvFiles[] = $f;
			}
		}
		//** Get All csv files[END]

		//** Extrace file one by one[START]
		foreach($csvFiles as $csvF){
			if(!empty($csvF)){
				$this->readCsv($csvF);
			}
			sleep(1);
		}
		//** Extrace file one by one[END]
		exit;
	}

	/*
		Test method to read csv and save in DB
	*/
	public function readCsv( $fileName = NULL ){
		$fileName = 'xbx.csv';
		$fileName = APP . 'webroot/NPI/'. $fileName;
		if(!empty($fileName)){
			$this->NpiUsaUser->setDataSource('testdb');
			//echo "Extracting...Please wait.";
			$startTime = strtotime(date('Y-m-d H:i:s'));
			$row = 1;
			//$fileName = APP . 'webroot/NPI/'. $fileName;
			if (($handle = fopen($fileName , "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
					$num = count($data);
					$row++;
					if($row == 2) continue; //** Skip Header Row
					//echo "<pre>"; print_r($data);exit;
					//** Set variable Values[START]
					$npi = $data[0];
					$providerOrganizationName = $data[4];
					$providerLastName = $data[5];
					$providerMiddleName = $data[7];
					$providerFirstName = $data[6];
					$providerCredentialText = $data[10];
					$providerOtherOrganizationName = $data[11];
					$providerFirstLineBusinessMailingAddress = $data[20];
					$providerSecondLineBusinessMailingAddress = $data[21];
					$providerBusinessMailingAddressCityName = $data[22];
					$providerBusinessMailingAddressStateName = $data[23];
					$providerBusinessMailingAddressPostalCode = $data[24];
					$providerBusinessMailingAddressTelephoneNumber = $data[26];
					$providerBusinessMailingAaddressFaxNumber = $data[27];
					$providerEnumerationDate = date('Y-m-d', strtotime($data[36]));;
					$providerGenderCode = $data[41];
					$authorizedOfficialTelephoneNumber = $data[46];
					$providerLicenseNumber_1 = $data[48];
					$providerLicenseNumberStateCode_1 = $data[49];
					//** Set variable Values[END]

					//** Set Array Values[START]
					$npiData = array(
							"npi"=> $npi,
							"provider_organization_name"=> $providerOrganizationName,
							"provider_last_name"=> $providerLastName,
							"provider_middle_name"=> $providerMiddleName,
							"provider_first_name"=> $providerFirstName,
							"provider_credential_text"=> $providerCredentialText,
							"provider_other_organization_name"=> $providerOtherOrganizationName,
							"provider_first_line_business_mailing_address"=> $providerFirstLineBusinessMailingAddress,
							"provider_second_line_business_mailing_address"=> $providerSecondLineBusinessMailingAddress,
							"provider_business_mailing_address_city_name"=> $providerBusinessMailingAddressCityName,
							"provider_business_mailing_address_state_name"=> $providerBusinessMailingAddressStateName,
							"provider_business_mailing_address_postal_code"=> $providerBusinessMailingAddressPostalCode,
							"provider_business_mailing_address_telephone_number"=> $providerBusinessMailingAddressTelephoneNumber,
							"provider_business_mailing_address_fax_number"=> $providerBusinessMailingAaddressFaxNumber,
							"provider_enumeration_date"=> $providerEnumerationDate,
							"provider_gender_code"=> $providerGenderCode,
							"authorized_official_telephone_number"=> $authorizedOfficialTelephoneNumber,
							"provider_license_number_1"=> $providerLicenseNumber_1,
							"provider_license_number_state_code_1"=> $providerLicenseNumberStateCode_1,
						);
					//** Set Array Values[END]
					$npiCheck = $this->NpiUsaUser->find("count", array("conditions"=> array("npi"=> $npi)));
					if( $npiCheck==0 ){
						$this->NpiUsaUser->saveAll($npiData);
					}
					if($row % 100 == 0) { sleep(1); }
				}
				fclose($handle);
				$endTime = startotime(date('Y-m-d H:i:s'));
				$timeTakenDiff = $endTime - $startTime;
				$timeTake = date('i:s', $timeTakenDiff);
				echo $timeTake;
			}
		}
		exit;
	}

	/*
		Get recursive files of any directory
	*/
	function getDirContents($dir, &$results = array()){
	    $files = scandir($dir);

	    foreach($files as $key => $value){
	        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
	        if(!is_dir($path)) {
	            $results[] = $path;
	        } else if($value != "." && $value != "..") {
	            getDirContents($path, $results);
	            //$results[] = $path;
	        }
	    }

	    return $results;
	}

	/*

	*/
	public function phpVersion(){
		phpinfo();
		exit;
	}

	/*
		check qb details
	*/
	public function checkQbDetails(){
		echo "APP id :" . APPLICATION_ID ." Key :". AUTH_KEY ."Auth :". AUTH_SECRET ." url :". QB_API_ENDPOINT;
		exit;
	}

	/*
		Check QB login
	*/
	public function checkQbLogin(){
		$checkLogin = $this->Quickblox->quickLogin("som's_1@yopmail.com",'Medic123@');
		echo "<pre>"; print_r($checkLogin);
		exit;
	}

	/*
		Test method to add new gmc data
	*/
	public function addNewGmc(){
		//$newGmcDataCount = $this->GmcUkExtraUser->find("count");
		$sizeLimit = 10001;
		//$totSize = ceil( ($newGmcDataCount/$sizeLimit));
		$offsetVal = 375000;
		$newGmc = 0;
		$oldGmc = 0;
		//for($cnt=0; $cnt < $totSize; $cnt++){
			$newGmcData = $this->GmcUkExtraUser->find("all",
														array('limit'=> $sizeLimit,
																'offset'=> $offsetVal,
																'order'=> array('id DESC')
															)
													);
			//echo "<pre>"; print_r($newGmcData);die;
			foreach($newGmcData as $gmc){
				if( $this->GmcUkUser->find("count", array("conditions"=> array("GMCRefNo"=> $gmc['GmcUkExtraUser']['GMCRefNo']))) == 0 ){
					$this->GmcUkExtraUser->updateAll(array("new_gmc"=> 1), array("GMCRefNo"=> $gmc['GmcUkExtraUser']['GMCRefNo']));
					$newGmc++;
				}else{
					$oldGmc++;
				}
			}
			//$offsetVal++;
			//sleep(1);
		//}
		echo "New: ". $newGmc ." Old: ". $oldGmc;
		exit;
	}

	/*
		Test method to add new gmc data to gmc table
	*/
	public function addGmcDataToDb(){
		$params = $this->params['pass'];
		$size = $params[0];
		$offset = $params[1];
		$recAdded = 0;
		App::import('model','GmcUkUser');
        $GmcUkUser = new GmcUkUser();
        $quer = "SELECT * FROM gmc_uk_final_users LIMIT ". $offset. " , ". $size ;
        $gmcNewdata = $GmcUkUser->query($quer);
        foreach($gmcNewdata as $newGmc){
        	//echo "<pre>" ; print_r($newGmc);
        	if( $this->GmcUkUser->find("count", array("conditions"=> array("GMCRefNo"=> $newGmc['gmc_uk_final_users']['GMCRefNo']))) == 0 ){
					$newGmcData = array(
								"GMCRefNo" => $newGmc['gmc_uk_final_users']['GMCRefNo'],
            					"Surname" => $newGmc['gmc_uk_final_users']['Surname'],
					            "GivenName" => $newGmc['gmc_uk_final_users']['GivenName'],
					            "Gender" => $newGmc['gmc_uk_final_users']['Gender'],
					            "Qualification" => $newGmc['gmc_uk_final_users']['Qualification'],
					            "YearOfQualification" => $newGmc['gmc_uk_final_users']['YearOfQualification'],
					            "PlaceofQualification" => $newGmc['gmc_uk_final_users']['PlaceofQualification'],
					            "PRDate" => $newGmc['gmc_uk_final_users']['PRDate'],
					            "FRDate" => $newGmc['gmc_uk_final_users']['FRDate'],
					            "SpecialistRegisterDate" => $newGmc['gmc_uk_final_users']['SpecialistRegisterDate'],
					            "GPRegisterDate" => $newGmc['gmc_uk_final_users']['GPRegisterDate'],
					            "RegistrationStatus" => $newGmc['gmc_uk_final_users']['RegistrationStatus'],
					            "ARFDueDate" => $newGmc['gmc_uk_final_users']['ARFDueDate'],
					            "Specialty1" => $newGmc['gmc_uk_final_users']['Specialty1'],
					            "Sub_Specialty1" => $newGmc['gmc_uk_final_users']['Sub_Specialty1'],
					            "Specialty2" => $newGmc['gmc_uk_final_users']['Specialty2'],
					            "Sub_Specialty2" => $newGmc['gmc_uk_final_users']['Sub_Specialty2'],
					            "Specialty3" => $newGmc['gmc_uk_final_users']['Specialty3'],
					            "Sub_Specialty3" => $newGmc['gmc_uk_final_users']['Sub_Specialty3'],
					            "Specialty4" => $newGmc['gmc_uk_final_users']['Specialty4'],
					            "Sub_Specialty4" => $newGmc['gmc_uk_final_users']['Sub_Specialty4'],
					            "Specialty5" => $newGmc['gmc_uk_final_users']['Specialty5'],
					            "Sub_Specialty5" => $newGmc['gmc_uk_final_users']['Sub_Specialty5'],
					            "Specialty6" => $newGmc['gmc_uk_final_users']['Specialty6'],
					            "Sub_Specialty6" => $newGmc['gmc_uk_final_users']['Sub_Specialty6'],
					            "Specialty7" => $newGmc['gmc_uk_final_users']['Specialty7'],
					            "Sub_Specialty7" => $newGmc['gmc_uk_final_users']['Sub_Specialty7'],
					            "FTPConditionsExist" => $newGmc['gmc_uk_final_users']['FTPConditionsExist'],
					            "FTPUndertakingsExist" => $newGmc['gmc_uk_final_users']['FTPUndertakingsExist'],
					            "OtherNames" => $newGmc['gmc_uk_final_users']['OtherNames'],
					            "FTPWarningsExist" => $newGmc['gmc_uk_final_users']['FTPWarningsExist'],
					            "PlaceofQualificationCountry" => $newGmc['gmc_uk_final_users']['PlaceofQualificationCountry'],
								);
					$this->GmcUkUser->saveAll($newGmcData);
					$recAdded++;
			}

		}
		echo $recAdded;
		exit;
	}

	/*
		Test for value encrypt
	*/
	public function encryptValue(){
		if(!empty($_POST['values'])){
			$encryptedData = $this->Common->encryptData( $_POST['values'] );
			$jsonData = array("values"=> $encryptedData);
			echo json_encode($jsonData);
		}else{
			$jsonData = array("values"=> "");
			echo json_encode($jsonData);
		}
		exit;
	}

	/*
		Test for value decrypt
	*/
	public function decryptValue(){

		if(!empty($_REQUEST['values'])){
			$decryptedData = $this->Common->decryptData( $_REQUEST['values'] );
			$jsonData = array("values"=> $decryptedData);
			echo json_encode($jsonData);
		}else{
			$jsonData = array("values"=> "No value");
			echo json_encode($jsonData);
		}
		exit;
	}

	/*
		Test deeplink redirect url for ios
	*/
	public function redirectIosAppDeeplink(){
		//header('location:1109659209invitecolleagues://');
		echo "<script type='text/javascript'> document.location = 'www.109659209invitecolleagues://'; </script>";
		exit;
	}

	/*
		Test QB Token
	*/
	public function testQbToken(){
		//** Dev environment[START]
			/*
			DEFINE('APPLICATION_ID', '13'); // Enter your application ID
			DEFINE('AUTH_KEY', "3UXy7s7LXaXK2ky"); // Enter yur auth key
			DEFINE('AUTH_SECRET', "EP2HfA9yaKu77sU"); // Enter your secret key
			DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL
			*/
		//** Dev environment[END]

		//** QA environment[START]
			DEFINE('APPLICATION_ID', 15); // Enter your application ID
	        DEFINE('AUTH_KEY', "YxsAd85qbObAjMZ"); // Enter yur auth key
	        DEFINE('AUTH_SECRET', "e-Nc9q3SCHbR4gZ"); // Enter your secret key
	        DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL
        //** QA environment[END]

		$nonce = rand();
		        $timestamp = time();
		        $signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp;
		        $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);
		        // Build post body
		        $post_body = http_build_query(array(
		            'application_id' => APPLICATION_ID,
		            'auth_key' => AUTH_KEY,
		            'timestamp' => $timestamp,
		            'nonce' => $nonce,
		            'signature' => $signature,
		        ));
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . 'session.json' );
		        curl_setopt($curl, CURLOPT_POST, true); // Use POST
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Receive server response
		        $response = curl_exec($curl); //echo "<pre>"; print_r($response);exit;
		        $responseVal = json_decode($response, true);
		        if (isset($responseVal['session']['token']) && !empty($responseVal['session']['token'])) {
		            //return json_decode($response);
		            return $responseVal['session']['token'];
		        } else {
		            //$error = curl_error($curl). '(' .curl_errno($curl). ')';
		        	//return $error;
		        	$responseVal['session']['token'] = "";
		        	return $responseVal['session']['token'];
		        }
		        curl_close($curl);
		exit;
	}

	/*
		Test Method
	*/
	public function qbSessionCheck(){
		// Application credentials - change to yours (found in QB Dashboard)
		DEFINE('APPLICATION_ID', 13);
		DEFINE('AUTH_KEY', "3UXy7s7LXaXK2ky");
		DEFINE('AUTH_SECRET', "EP2HfA9yaKu77sU");

		// User credentials
		DEFINE('USER_LOGIN', "mbone@yopmail.com");
		DEFINE('USER_PASSWORD', "Medic123@");

		// Quickblox endpoints
		DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com");
		DEFINE('QB_PATH_SESSION', "session.json");

		// Generate signature
		$nonce = rand();
		$timestamp = time(); // time() method must return current timestamp in UTC but seems like hi is return timestamp in current time zone
		$signature_string = "application_id=".APPLICATION_ID."&auth_key=".AUTH_KEY."&nonce=".$nonce."&timestamp=".$timestamp."&user[email]=".USER_LOGIN."&user[password]=".USER_PASSWORD;

		echo "stringForSignature: " . $signature_string . "<br><br>";
		$signature = hash_hmac('sha1', $signature_string , AUTH_SECRET);

		// Build post body
		$post_body = http_build_query(array(
		     'application_id' => APPLICATION_ID,
		     'auth_key' => AUTH_KEY,
		     'timestamp' => $timestamp,
		     'nonce' => $nonce,
		     'signature' => $signature,
		     'user[email]' => USER_LOGIN,
		     'user[password]' => USER_PASSWORD
		     ));

		// $post_body = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&timestamp=" . $timestamp . "&nonce=" . $nonce . "&signature=" . $signature . "&user[login]=" . USER_LOGIN . "&user[password]=" . USER_PASSWORD;

		echo "postBody: " . $post_body . "<br><br>";
		// Configure cURL
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . '/' . QB_PATH_SESSION); // Full path is - https://api.quickblox.com/session.json
		curl_setopt($curl, CURLOPT_POST, true); // Use POST
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response

		// Execute request and read response
		$response = curl_exec($curl);
		echo "<pre>"; print_r($response);die;
		// Check errors
		if ($response) {
		echo $response . "\n";
		} else {
		$error = curl_error($curl). '(' .curl_errno($curl). ')';
		echo $error . "\n";
		}

		// Close connection
		curl_close($curl);
	}

	/*
		Test method QB user edit
	*/
	public function qbUserEdit($token = NULL, $user_id = NULL, $customData = array()){
			$token = "55ea1fe458270543e329089d89a3698dab00000d";
			$user_id = 60529;

			$profile_image = "http://www.check.com/profile.jpg";//$customData['profileImage'];
			$status = "check edit";//$customData['roleStatus'];
			$is_import = true;//$customData['isImport'];
			$update_array = array('avatar_url' => $profile_image ,
						'status' => $status,
						'is_import' => $is_import,
					);
			$data["user"]["custom_data"] = json_encode($update_array);
			/////////
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$user_id.'.json');
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
			//curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'QuickBlox-REST-API-Version: 0.1.0',
			'QB-Token: ' . $token
			));
			/////////
			$response = curl_exec($curl);
			echo "<pre>"; print_r($response);exit;
			if ($response) {
			return $response;
			} else {
			$error = curl_error($curl). '(' .curl_errno($curl). ')';
			return $error . "\n";
			}
			curl_close($curl);

	}

	/*
		Check login of third party api
	*/
	public function checkTpapiLogin(){

			$valuesArr = array("values" => 's8uhy58LPLzkACW6JqAMPfW8Rfj4AHtMu7FGfIWAPw+XoBXB6yF+w+OZlkc0sawop5YrSoOfzs6FrnJbotjdXQ=='
					);
			//$data["values"] = json_encode($valuesArr);

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'http://52.24.50.79/tpapi/thirdpartyapi/tpapi/tpapiuserwebservices/login');
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(json_encode($valuesArr)));
			//curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'access_key: ' . 'c1dfa623d78050bdeb773d6cab43ea86'
			));
			/////////
			$response = curl_exec($curl);
			echo "<pre>"; print_r($response);exit;
			if ($response) {
			return $response;
			} else {
			$error = curl_error($curl). '(' .curl_errno($curl). ')';
			return $error . "\n";
			}
			curl_close($curl);
			exit;
	}

	/*
		Test quick token create
	*/
		public function quickLogin($username,$password){
			//** Dev environment[START]
			/*
			DEFINE('APPLICATION_ID', '13'); // Enter your application ID
			DEFINE('AUTH_KEY', "3UXy7s7LXaXK2ky"); // Enter yur auth key
			DEFINE('AUTH_SECRET', "EP2HfA9yaKu77sU"); // Enter your secret key
			DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL
			*/
		//** Dev environment[END]

		//** QA environment[START]
			DEFINE('APPLICATION_ID', 15); // Enter your application ID
	        DEFINE('AUTH_KEY', "YxsAd85qbObAjMZ"); // Enter yur auth key
	        DEFINE('AUTH_SECRET', "e-Nc9q3SCHbR4gZ"); // Enter your secret key
	        DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL
        //** QA environment[END]
			$nonce = rand();
	        $timestamp = time();
	        $signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp . "&user[email]=" . $username . "&user[password]=" . $password;
	        $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);
	        $post_array = array(
	            'application_id' => APPLICATION_ID,
	            'auth_key' => AUTH_KEY,
	            'timestamp' => $timestamp,
	            'nonce' => $nonce,
	            'signature' => $signature,
	            'user[email]' => $username,
	            'user[password]' => $password
	        );
	        $post_body = http_build_query($post_array);
	        $curl = curl_init();
	        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . 'session.json' );
	        curl_setopt($curl, CURLOPT_POST, true); // Use POST
	        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
	        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Receive server response
	        // Execute request and read response
	        $response = curl_exec($curl);
	        //echo "<pre>"; print_r($response);die;
	        // Close connection
	        curl_close($curl);
			if ($response) {
	                return json_decode($response);
	        } else {
	                $error = curl_error($curl). '(' .curl_errno($curl). ')';
			        return $error;
	        }
	}

	/*
		Test method to create dialog
	*/
	public function createDialog(){
		//** Dev environment[START]

			DEFINE('APPLICATION_ID', '13'); // Enter your application ID
			DEFINE('AUTH_KEY', "3UXy7s7LXaXK2ky"); // Enter yur auth key
			DEFINE('AUTH_SECRET', "EP2HfA9yaKu77sU"); // Enter your secret key
			DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL

		//** Dev environment[END]

		//** QA environment[START]
			/*
			DEFINE('APPLICATION_ID', 15); // Enter your application ID
	        DEFINE('AUTH_KEY', "YxsAd85qbObAjMZ"); // Enter yur auth key
	        DEFINE('AUTH_SECRET', "e-Nc9q3SCHbR4gZ"); // Enter your secret key
	        DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL
        	*/
        //** QA environment[END]


		$tokenDetails = $this->quickLogin("nimay@mediccreations.com",'12345678');
		$token = $tokenDetails->session->token;
		$post_body = http_build_query(array(
		            'type' => 3,
		            'name' => "Dialog Check",
		            'occupants_ids' => "59934",
		            //'occupants_ids' => "110,17",
		        ));
		        $post_body = urldecode($post_body);
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json');
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl); echo "<pre>"; print_r($response);exit;
		        if ($response) {
		                return $response;
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error . "\n";
		        }
		        curl_close($curl);
		        exit;
	}

	/*
		Check message
	*/
		public function checkMessage(){
			$tokenDetails = $this->quickLogin("nimay@mediccreations.com",'12345678');
			$token = $tokenDetails->session->token;

			$post_body = http_build_query(array(
		            //'chat_dialog_id' => "58a1c82a6ed55556ea000001",
		            'message' => "Hello",
		            'recipient_id' => 59934,
		            //'type'=> 3
		            //'occupants_ids' => "110,17",
		        ));
		        $post_body = urldecode($post_body);
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json');
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl); echo "<pre>"; print_r($response);exit;
		        if ($response) {
		                return $response;
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error . "\n";
		        }
		        curl_close($curl);
		        exit;
		}

	/*
		Test FCM Message
	*/
		public function sendFcmMessage( $ids = array(), $data = array(), $params = array() ){
	    if(!empty($_REQUEST['fcmTokens'])){
		    $ids = explode(",", $_REQUEST['fcmTokens']);
		    $data = array("notification_id"=> '1', "message"=> 'Test FCM', "msg_type"=> "Fast Bleep");
		    $apiKey = 'AIzaSyBB24pkKtKWSUI9qSBC2RqGvE8bdVvs_54';//'AIzaSyClyrvyeQd-BcR7sfEXVPpgb12ABS8X60g';
		    $url = 'https://fcm.googleapis.com/fcm/send';
			$post = array(
		                    'registration_ids'  => $ids,
		                    'data'              => $data,
		                    'notification'=> array('title' => 'Test FCM','body'  => 'This is FCM Test', 'sound'=>'Emergency.mp3', 'badge'=> 0),
		                    //'content_available' => true,
		                    //'priority'=> 'high'
		                    'content_available' => true,
		                    'priority'=> 'high',
		                );
		    $headers = array(
		                    'Authorization: key=' . $apiKey,
		                    'Content-Type: application/json'
		                    );
		    $ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );
			$result = curl_exec( $ch );
			$curlError = '';
			if ( curl_errno( $ch ) )
		    {
		        $curlError = curl_error( $ch );
		    }
			curl_close( $ch );
			$response = !empty($curlError) ? $curlError : $result;
			echo "<pre>"; print_r($response);
			//return $response;
		}else{
			echo "Provide FCM Tokens.";
		}
	}

	/*
		Test FCM Message using default sound
	*/
		public function sendFcmMessageDefaultSound( $ids = array(), $data = array(), $params = array() ){
	    //$ids = array('dwqCMHCT1nA:APA91bG6jGpz8b70EZo2-xq2zojWEH7UF8rrbJ1fuspccVpM2TZ1L6qNB9_6Vi3DkX_j8IqAbHFh-zFui_ZrSMclZQb9kIIT1PApOaRTHZRSLkJhZov7AzYZEVX76FNJE17CODISPYWU');
	    if(!empty($_REQUEST['fcmTokens'])){
		    $ids = explode(",", $_REQUEST['fcmTokens']);
		    $data = array("notification_id"=> '1', "message"=> 'Test FCM', "msg_type"=> "Fast Bleep");
		    $apiKey = 'AIzaSyBB24pkKtKWSUI9qSBC2RqGvE8bdVvs_54';
		    $url = 'https://fcm.googleapis.com/fcm/send';
			$post = array(
		                    'registration_ids'  => $ids,
		                    'data'              => $data,
		                    'notification'=> array('title' => 'Test FCM','body'  => 'This is FCM Test', 'sound'=>'default', 'badge'=> 0),
		                    //'content_available' => true,
		                    //'priority'=> 'high'
		                    'content_available' => true,
		                    'priority'=> 'high',
		                );
		    $headers = array(
		                    'Authorization: key=' . $apiKey,
		                    'Content-Type: application/json'
		                    );
		    $ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );
			$result = curl_exec( $ch );
			$curlError = '';
			if ( curl_errno( $ch ) )
		    {
		        $curlError = curl_error( $ch );
		    }
			curl_close( $ch );
			$response = !empty($curlError) ? $curlError : $result;
			echo "<pre>"; print_r($response);
			//return $response;
		}else{
			echo "Provide FCM Tokens.";
		}
		exit;
	}

	/*
		Check quickblox password update
	*/
	public function checkQBPassUpdate(){
		DEFINE('APPLICATION_ID', '13'); // Enter your application ID
			DEFINE('AUTH_KEY', "3UXy7s7LXaXK2ky"); // Enter yur auth key
			DEFINE('AUTH_SECRET', "EP2HfA9yaKu77sU"); // Enter your secret key
			DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL

		//** Dev environment[END]

		//** QA environment[START]
			/*
			DEFINE('APPLICATION_ID', 15); // Enter your application ID
	        DEFINE('AUTH_KEY', "YxsAd85qbObAjMZ"); // Enter yur auth key
	        DEFINE('AUTH_SECRET', "e-Nc9q3SCHbR4gZ"); // Enter your secret key
	        DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL
        	*/
        //** QA environment[END]


		$tokenFieldsObj = $this->Quickblox->quickLogin('nimay@mediccreations.com','12345678');
		$tokenFieldsArr = json_decode(json_encode($tokenFieldsObj), true);
		$token = $tokenFieldsArr['session']['token'];
		$updateData = array(
		            'user[password]' => "Changeit@14#",
		            'user[old_password]' => "Medic1234@"
		        );

		$postData = http_build_query($updateData);
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/60602.json');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
        $response = curl_exec($curl);
		        echo "<pre>"; print_r($response);die;
		        if (!empty($response)) {
		                return $response;
		        } else {
		                return false;
		        }
		        curl_close($curl);
		exit;
	}

	/*
		Check quickblox forgot password
	*/
	public function checkQBForgotPassword(){
		DEFINE('APPLICATION_ID', '13'); // Enter your application ID
			DEFINE('AUTH_KEY', "3UXy7s7LXaXK2ky"); // Enter yur auth key
			DEFINE('AUTH_SECRET', "EP2HfA9yaKu77sU"); // Enter your secret key
			DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL

		//** Dev environment[END]

		//** QA environment[START]
			/*
			DEFINE('APPLICATION_ID', 15); // Enter your application ID
	        DEFINE('AUTH_KEY', "YxsAd85qbObAjMZ"); // Enter yur auth key
	        DEFINE('AUTH_SECRET', "e-Nc9q3SCHbR4gZ"); // Enter your secret key
	        DEFINE('QB_API_ENDPOINT', "https://apimedicbleeptest.quickblox.com/"); // API Endpoints URL
        	*/
        //** QA environment[END]


		$tokenFieldsObj = $this->Quickblox->quickLogin('nimay@mediccreations.com','12345678');
		$tokenFieldsArr = json_decode(json_encode($tokenFieldsObj), true);
		$token = $tokenFieldsArr['session']['token'];
		/*$updateData = array(
		            'user[password]' => "Chekit123#",
		            'user[old_password]' => "Medic123@"
		        );

		$postData = http_build_query($updateData);*/
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/password/reset.json?email=nimay@mediccreations.com');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
        $response = curl_exec($curl);
		        echo "<pre>"; print_r($response);die;
		        if (!empty($response)) {
		                return $response;
		        } else {
		                return false;
		        }
		        curl_close($curl);
		exit;
	}

	/*
		Test table data fetch
	*/
	public function checkTableData(){
		App::import('Model', 'User');
		$User = new User();
		$data = $User->query("SELECT * FROM `user_role_tags`");
		echo "<pre>"; print_r($data);
		exit;
	}

	/*
		Test method for custom data updation
	*/
	public function updateQbCustomDetails(){
		$email = 'yeti@yopmail.com';
		$securePass = '12345678';
		$userRoleStatus = "Hello its me";
		$userCustomQbData = array("userEmail"=> $email, "userPassword"=> $securePass, "userRoleStatus"=> $userRoleStatus);
		$customDataQb = $this->Quickblox->updateCustomData($userCustomQbData);
		echo "<pre>"; print_r($customDataQb);
		exit;
	}

	/*
		Test method for mailchimp schedule mail
	*/
	public function mailchimpScheduleCheck($params = array()){
		//$params['toMail'] = 'nimay@mediccreations.com';
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
		$template = $params['templateName'];//"MB_Add_Colleagues";
		//$template_name = $template;
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'Test Mail'
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
				array(
					'name' => 'FNAME',
					'content' => 'Nimay Test'
				),
			)
		);

		$message = array(
		'html' => 'Test Mail',
		'text' => 'Test Mail',
		'subject' => "Test Mail",
		'from_email' => 'support@medicbleep.com',//'medicbleep@theoncallroom.com',
		'from_name' => 'Medic Bleep',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => 'MB_QA',
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = $params['sendAt'];
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	 	//echo print_r(json_encode($result));
		//exit;
	}

	/*
		Test json data
	*/
	public function sampleJsonData(){
		$arrData[] = array("users"=> array("id"=>1 ,"name"=> "test"));
		echo json_encode($arrData);
		exit;
	}

	/*
		Test time/days adding
	*/
	public function testDayAdd(){
		$day1 = date('Y-m-d h:i:00');
		$day2 = date('Y-m-d H:i:s', strtotime($day1 . ' +5 minutes'));
		$day3 = date('Y-m-d H:i:s', strtotime($day2 . ' +5 minutes'));
		$day4 = date('Y-m-d H:i:s', strtotime($day3 . ' +5 minutes'));
		$day5 = date('Y-m-d H:i:s', strtotime($day4 . ' +5 minutes'));
		$day6 = date('Y-m-d H:i:s', strtotime($day5 . ' +2 day'));
		$day7 = date('Y-m-d H:i:s', strtotime($day6 . ' +2 day'));
		$day8 = date('Y-m-d H:i:s', strtotime($day7 . ' +2 day'));
		$day9 = date('Y-m-d H:i:s', strtotime($day8 . ' +2 day'));
		$day10 = date('Y-m-d H:i:s', strtotime($day9 . ' +2 day'));

		$params['toMail'] = "nimay@mediccreations.com";

		//** First Mail [START]
		$params['templateName'] = "MB_Welcome_to_Medic_Bleep";
		$params['subject'] = "Welcome to Medic Bleep";
		$params['sendAt'] = $day1;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_Add_Colleagues";
		$params['subject'] = "It’s good to talk";
		$params['sendAt'] = $day2;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_Invite_Colleagues";
		$params['subject'] = "Find the people you want to talk to";
		$params['sendAt'] = $day3;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_Starting_Group_Chat";
		$params['subject'] = "Keep Everyone in the Loop";
		$params['sendAt'] = $day4;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_Send_Images_and_Documents";
		$params['subject'] = "Never Lose a Handover Sheet Again!";
		$params['sendAt'] = $day5;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_Export_Chat";
		$params['subject'] = "How to Export Your Chats";
		$params['sendAt'] = $day6;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_Your_Profile_&_Settings";
		$params['subject'] = "Your Profile. Your Settings.";
		$params['sendAt'] = $day7;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_See_&_Hear_Your_Colleagues";
		$params['subject'] = "See & Hear Your Colleagues";
		$params['sendAt'] = $day8;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_Web_App";
		$params['subject'] = "Which version is right for you?";
		$params['sendAt'] = $day9;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		//** First Mail [START]
		$params['templateName'] = "MB_Secure_Messaging";
		$params['subject'] = "How Secure is Medic Bleep?";
		$params['sendAt'] = $day10;
		echo $this->mailchimpScheduleCheck($params);
		//** First Mail [END]

		exit;
	}

	/*

	*/
	public function userActiveDeactiveCheck(){
		$email = $_REQUEST['email'];
		$statusVal = !empty($_REQUEST['status'])?1:0;
		App::import('Model', 'User');
		$User = new User();
		$updateCheck = $User->query("UPDATE users SET status=$statusVal WHERE email='$email'");
		echo "<pre>"; print_r($updateCheck);
		exit;
	}

	//Check redis connection

	public function checkRedisConn()
	{
		$cacObj = $this->Cache->redisConn();
		if(($cacObj['connection']) && empty($cacObj['errors'])){
					echo "connected";
		}
		else
		{
			echo "Not connected".$cacObj['errors'];
		}
	}

	public function chechRedisConn()
       {
                $returnVal = array();
                App::import('Vendor','PREDIS',array('file' => 'predis/autoload.php'));
                $redisVars = array(
                "scheme" => "tcp",
                'host'     => '52.56.203.69',
                'port'     => 6379,
                'timeout' => 0.8, // (In seconds) used to connect to a Redis server after which an exception is thrown.
                "password" => 'bd88ffcb3c87c99d1fd50ece02f125fa',
                );
                $redis = new Predis\Client($redisVars);
                try {
                        $msg =  $redis->ping();
                        if($msg = 'PONG'){
                                $returnVal = array("connection"=> true, "errors"=> "", "robj"=> $redis);
                                //return $returnVal;
                           // echo "<pre>";print_r($returnVal);exit();
                        }
                } catch (Exception $e) {
                        $msg = 'Redis Error : '.$e->getMessage();
                        $returnVal = array("connection"=> false, "errors"=> $msg, "robj"=> "");
                }

    //             if($returnVal['robj']->exists('institutionDirectorySSSSSS:85')){
				// //echo "<pre>";print_r("TEST12");exit();
				// 	$dataJson = $returnVal['robj']->get('institutionDirectoryAAAAA:85');
				// 	$userData = json_decode($dataJson, true);
				// }
				// else
				// {
				// 	$userListsArr = array("user_id"=> "588");
				// 	$key = 'institutionDirectoryAAAAA:85';
				// 	$returnVal['robj']->set($key,json_encode($userListsArr, true));
				// }
				echo "<pre>";print_r($returnVal);exit();
                if($returnVal['connection'] == 1)
                {
                	$connectionArr = array("status"=>1,"response_code"=>200,"message"=>"Connected successfully.");
                	$connectionJson = json_encode($connectionArr);
                	echo $connectionJson;exit();
                }
                else{
                	$connectionArr = array("status"=>0,"response_code"=>615,"message"=>"Some technical error occurred. Please try again.");
                	$connectionJson = json_encode($connectionArr);
                	echo $connectionJson;exit();
                }
       }

       public function callbackFunction()
       {
       	 	$var = array("a"=>"b", "c"=>"d");
       	 	echo json_encode($var);
       	 	exit();
       }

       public function getLatituteLongitute(){
		$this->autoRender = false;
		$response = array();
		$response2 = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$value = isset($dataInput['value']) ? $dataInput['value']:'0';

			if(! empty($value))
			{
				$getLatituteLongitute = $this->CompanyGeofenceParameter->find("first", array("conditions"=> array("id"=> $value)));
				$response2['latitude'] = $getLatituteLongitute['CompanyGeofenceParameter']['latitude'];
				$response2['longitude'] = $getLatituteLongitute['CompanyGeofenceParameter']['longitude'];
				$response = array('Name'=>$getLatituteLongitute['CompanyGeofenceParameter']['company_address'], 'value'=>$response2);

				if(!empty($response))
				{
					$responseData = array('method_name'=> 'getLatituteLongitute', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=>$response);
				}
				else{
					$responseData = array('method_name'=> 'getLatituteLongitute', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}

			}
			else{
				$responseData = array('method_name'=> 'getLatituteLongitute', 'status'=>"0", 'response_code'=> "611", 'message'=> ERROR_611);
			}

		}else{
			$responseData = array('method_name'=> 'getLatituteLongitute', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;

	}

	/*
		Test method to read csv and save in DB
	*/
	public function readCsvOne( $fileName = NULL ){
		$fileName = 'Critical_bleep_holder_roles_draft_1.csv';
		$fileName = APP . 'webroot/videos/'. $fileName;
		if(!empty($fileName)){
			$startTime = date('Y-m-d H:i:s');
			$row = 1;
			$dataArr = array();
			if (($handle = fopen($fileName , "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
					$num = count($data);
					$row++;
					if($row == 2) continue;
					$dataArr[] = $data[1];
					$oneTimeTokenData = array("baton_roles"=>ucwords($data[1]), "created_at"=>$startTime, "updated_at"=>$startTime);
					$this->BatonRole->saveAll($oneTimeTokenData);
					$batonLastInserted = $this->BatonRole->getInsertID();
					$oneTimeTokenData = array("department_id"=>1, "role_id"=>$batonLastInserted, "is_occupied"=>0,"is_active"=>1,"timeout"=>60,"institute_id"=>336,"on_call_value"=>1,"created_at"=>$startTime,"updated_at"=>$startTime);
					$this->DepartmentsBatonRole->saveAll($oneTimeTokenData);
					//** Set variable Values[END]


					if( $npiCheck==0 ){

					}
					if($row % 100 == 0) { sleep(1); }
				}
				fclose($handle);
				echo "Done";exit();
			}
		}
		exit;
	}

	public function checkPushNotification()
	{
		$responseData = array();
		$userVoipDataForAndriod = $this->VoipPushNotification->find('first', array('conditions' => array('VoipPushNotification.device_type' => "Android")));
		$params['fcm_voip_token'] = $userVoipDataForAndriod['VoipPushNotification']['fcm_voip_token'];
		$checkPushNotificationOnAndroid = $this->checkPushNotificationOnAndroid($params);
		$checkPushNotificationOnAndroid = json_decode($checkPushNotificationOnAndroid, true);
		if($checkPushNotificationOnAndroid['success'] == 0)
		{
			$responseForAndroid = array("status"=>0,"response_code"=>615,"message"=>"Some technical error occurred. Please try again.", "device_type"=>"Android", "value"=>json_encode($checkPushNotificationOnAndroid));
		}
		else
		{
			$responseForAndroid = array("status"=>1,"response_code"=>200,"message"=>"Voip Sent successfully","device_type"=>"Android", "value"=>json_encode($checkPushNotificationOnAndroid));
		}
		$responseData['Andriod'] = $responseForAndroid;

		$userVoipDataForIos = $this->VoipPushNotification->find('first', array('conditions' => array('VoipPushNotification.device_type' => "iOS")));
		$params['fcm_voip_token'] = $userVoipDataForIos['VoipPushNotification']['fcm_voip_token'];
		$checkPushNotificationOnIos = $this->checkPushNotificationOnIos($params);
		$responseForIos = array("status"=>1,"response_code"=>200,"message"=>"Voip Sent successfully","device_type"=>"Ios", "value"=>1);
		$responseData['Ios'] = $responseForIos;
		$finalArr = array("Andriod"=>$responseForAndroid,"Ios"=>$responseForIos);
		echo json_encode($finalArr);
		exit();
	}


	public function checkPushNotificationOnIos($params = array())
	{
		if(!empty($params))
		{
			$fcmVoipToken = $params['fcm_voip_token'];
			$passphrase = 'password';
			$message = 'Dissmiss Dnd';
			$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "ios_voip"=>1,
                        "VOIPCall"=>1
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				$var = 0;
			else
				$var = 1;
			fclose($fp);
			return $var;
		}
	}

	public function checkPushNotificationOnAndroid($params = array())
	{
		if(!empty($params['fcm_voip_token'])){
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$voipPushMessage = array
					(
						'VOIPCall'=> 1
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $result;
		}
		else
		{
			return "Token Is Empty";
		}

	}

	public function checkMainDrillEmail()
	{
		$name = "Ehtera Ahmad";
		$email = "ehteram@mediccreations.com";
		$message = "This is a test email to check maindrill functionality";
		$params = array();
		$params['to_mail'] = 'ehteram@mediccreations.com';
		// $params['to_mail'] = 'deepakkumar@mediccreations.com';
		$params['fromMail'] = "noreply@medicbleep.com";
		$params['fromName'] = 'No Reply';
		$params['mailSubject'] = 'Contact Us';

		$params['user_name'] = $name;
		$params['user_email'] = $email;
		$params['user_message'] = $message;

		$contactus = $this->MbEmail->checkMainDrillEmails($params);
		$data = json_decode($contactus);
		if($data[0]->status == "sent")
		{
			$responseArr = array("status"=>1,"response_code"=>200,"message"=>"Mail Delivered", "value"=>$contactus);
		}
		else{
			$responseArr = array("status"=>0,"response_code"=>615,"message"=>"Mail Not Delivered", "value"=>$contactus);
		}
		$finalJson = json_encode($responseArr);
		echo "<pre>";print_r($finalJson);
		exit;
	}

	public function checkMysqlConnection()
	{
		$dbhost = 'mbstaging-cluster.cluster-ro-cfj4158m8zaj.us-west-1.rds.amazonaws.com';
		$dbuser = 'root';
		$dbpass = 'TeamMedic';
		$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
		if(! $conn )
		{
			$responseArr = array("status"=>0,"response_code"=>615,"message"=>"Not Connected to mysql");
		}
		else
		{
			$responseArr = array("status"=>1,"response_code"=>200,"message"=>"Connected to MySQL Successfully!");
		}
		mysqli_close($conn);
		$finalJson = json_encode($responseArr);
		echo "<pre>";print_r($finalJson);
		exit;
	}

	public function checkDbChanges()
	{
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$table_name = $dataInput['table_name'];
			$field_name = $dataInput['field_name'];
			$dbhost = 'mbstaging-cluster.cluster-ro-cfj4158m8zaj.us-west-1.rds.amazonaws.com';
			$dbuser = 'root';
			$dbpass = 'TeamMedic';
			$database = 'mbstaging';
			$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $database);
			if(! $conn )
			{
				$responseData = array("status"=>0,"response_code"=>615,"message"=>"Not Connected to mysql");
			}
			else
			{
				$responseData = array("status"=>1,"response_code"=>200,"message"=>"Connected to MySQL Successfully!");
			}
			$sql = "SELECT $field_name FROM $table_name LIMIT 20";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
			    while($row = $result->fetch_assoc()) {
			    	$dataArr[] = array("id"=>$row[$field_name]);
			    }
			    $responseData = array('method_name'=> 'checkDbChanges', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=>$dataArr);
			} else {
			    $dataArr = array();
			    $responseData = array('method_name'=> 'checkDbChanges', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
			}
			$conn->close();


		}else{
			$responseData = array('method_name'=> 'checkDbChanges', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit();
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 20-12-2016
	I/P:
	O/P:
	Desc: Send Emails to Partial Signup Users.
	-----------------------------------------------------------------------------------------
	*/
	public function getPartilaSalesEmail(){
		App::import('model','TempRegistration');
		$emailArray = array();
		$TempRegistration = new TempRegistration();
		$partialSignupQuer = "SELECT ut.id, ut.email, ut.version, ut.device_type, ut.created, u.status,u.id,upro.first_name,upro.last_name,prof.profession_type
							FROM temp_registrations ut
							LEFT JOIN users u ON  u.email=ut.email
							LEFT JOIN user_profiles AS upro ON u.id=upro.user_id
							LEFT JOIN professions AS prof ON upro.profession_id=prof.id
							WHERE created >= (DATE(NOW()) - INTERVAL 7 DAY) GROUP BY ut.email ORDER BY ut.created DESC";
		$partialRegisterUsers = $TempRegistration->query( $partialSignupQuer );
		if(!empty($partialRegisterUsers)){
			$filename = 'partilaSignup.csv';
            $csv_file = fopen(APP . '/webroot/googlesheet/credentialData/' . $filename, 'w+');

            $header_row = array("First Name", "Last Name","Email","Joining Date", "Proffession");
            fputcsv($csv_file, $header_row);
			foreach($partialRegisterUsers as $partialData){
				$userEmail = trim($partialData['ut']['email']);
				$firstName = isset($partialData['upro']['first_name'])? $partialData['upro']['first_name'] : "N/A";
				$lastName = isset($partialData['upro']['last_name']) ? $partialData['upro']['last_name'] : "N/A";
				$Joiningdate = $partialData['ut']['created'];
				$profession = isset($partialData['prof']['profession_type']) ? $partialData['prof']['profession_type'] : "N/A";
				$domainNameArr = explode("@", $userEmail);//** Extract Domain name from email
				$domainName = end($domainNameArr);	//** Domain Name
				$rootDomain = $this->Common->getTld($domainName);
				// $preQualifiedEmail = $this->PreQualifiedDomain->find("count", array("conditions"=> array("domain_name"=> trim($rootDomain), "status"=>1)));
				$preapproved = 0;
				if($domainName == 'wsh.nhs.uk' || $domainName == 'mediccreations.com' || $domainName == 'wwl.nhs.uk')
				{
					$preapproved = 1;
				}
				if($preapproved != 1)
				{
					$instituteSelectionDetail = $this->UserEmployment->find('first', array("conditions"=>array("user_id"=>$partialData['u']['id'],"is_current"=>1)));
					if( ($instituteSelectionDetail['UserEmployment']['company_id'] !=86)  && ($instituteSelectionDetail['UserEmployment']['company_id'] !=336) && ($instituteSelectionDetail['UserEmployment']['company_id'] !=315) && ($instituteSelectionDetail['UserEmployment']['company_id'] !=126) )
					{
						$emailArray[] = $userEmail;
						$rows = array(
			                $firstName,
			                $lastName,
			                $userEmail,
			                $Joiningdate,
			                $profession
		                );
		                fputcsv($csv_file,array($firstName,$lastName,$userEmail,$Joiningdate,$profession),',','"');
					}
				}
			}
			$partialSignupParams['toMail'] = "anil@mediccreations.com";
			$partialSignupParams['name'] = "Ehteram";
			$partialSignupParams['partialSignupUrl'] = "abc.com";
			$partialSignupParams['file_name'] = $filename;
			$partialSignupParams['file_path'] = base64_encode(file_get_contents(APP . 'webroot/googlesheet/credentialData/'.$filename));
			$partialSignupParams['file_type'] = "application/vnd.ms-excel";
			$partialSignupMailSend = $this->MbEmail->sendMailPartialSignupUserTest($partialSignupParams);
			fclose($csv_file);
		}
		echo "<pre>";print_r($partialSignupMailSend);
		exit;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 17-04-2018
	I/P: Data Upload Using Sheet
	O/P:
	DESC: Save Multiple SignUp Data In DB
	-------------------------------------------------------------------------------------
	*/

	public function uploadFile( $fileName = NULL ){
		// echo "<pre>";print_r("Hi");exit();
		$userProfile = array();
		$userDetail = array();
		$existingEmail = array();
		$fileName = 'email-updated.csv';
		$fileName = APP . 'webroot/files/'. $fileName;
		$csv = array();
		$lines = file($fileName, FILE_IGNORE_NEW_LINES);

		foreach ($lines as $key => $value)
		{
		    $csv[$key] = str_getcsv($value);
		}
		$rowNos = 1;
		$columnNames = array();
		$sheetArray = array();
		//Use first row for names
		if(count($csv) && is_array($csv[0])){
			$columnNames = $csv[0];
		}
		array_shift($csv);
		$rowNos++;

		foreach($csv as $rec)
		{
				$d = array_combine($columnNames, $rec);
				$sheetArray[] = $d;
				$rowNos++;
		}
		// array_shift($sheetArray);
		// echo "<pre>";print_r($sheetArray);exit();
		$counter = 0;
		$existingEmailCounter = 0;
		$custom_role = array();
		$custom_role_json = json_encode($custom_role);
		$custom_baton_roles = array();
		$custom_baton_roles_json = json_encode($custom_baton_roles);
		foreach ($sheetArray as  $key => $sheetVal) {
			$userSheetData = array(
							"email"=> $sheetVal['email'],
							"password"=> md5("Medic@123"),//$sheetVal['password'],
							"activation_key"=> "150106879390100",//$sheetVal['activation_key'],
							"last_loggedin_date"=> date("Y-m-d H:i:s"),//$sheetVal['last_loggedin_date'],
							"login_device"=> "web",//$sheetVal['login_device'],
							"registration_date"=> date("Y-m-d H:i:s"),//$sheetVal['registration_date'],
							"status"=> 1, // $sheetVal['status']
							"approved"=> 1, //$userDetail['User']['approved']
							"company_approved"=> 1, //$sheetVal['company_approved']
							"role_id"=> 0, // $sheetVal['role_id']
							"registration_source"=> "MB", // $sheetVal['registration_source']
						);
			$emailCheck = $this->User->find("first", array("conditions"=> array("User.email"=> $sheetVal['email'])));
			if( empty($emailCheck) ){
				$this->User->saveAll($userSheetData);
				$userId = $this->User->getLastInsertID();
				if(! empty($userId))
				{
					$userProfileData = array(
						"user_id"=> $userId,
						"first_name"=> $sheetVal['first_name'],
						"last_name"=> $sheetVal['last_name'],
						"country_id"=> 99, //$userProfile['UserProfile']['country_id']
						"profession_id"=> 19,
						"gmcnumber"=> "12345",//$sheetVal['gmcnumber'],
						"contact_no"=> "7838017787",//$sheetVal['contact_no'],
						"contact_no_is_verified"=> 1,//$sheetVal['contact_no_is_verified'],
						"dob"=> date("Y-m-d H:i:s"),
						"role_status"=> "Doctor", //$sheetVal['role_status']
						"custom_role" => $custom_role_json,
						"custom_baton_roles" => $custom_baton_roles_json,
					);
					$this->UserProfile->saveAll($userProfileData);

					//** add default on duty/at work[START]
					$dutyLogData = $this->UserDutyLog->find("first", array("conditions"=> array("user_id"=> $userId)));
					if(!empty($dutyLogData)){
							// $this->UserDutyLog->updateAll(array("hospital_id"=>355, "atwork_status"=> 1, "status"=> 1), array("user_id"=> $userId));
					}else{
						$dutyData = array("hospital_id"=>434, "atwork_status"=> 1, "status"=> 1, "user_id"=> $userId);
						$this->UserDutyLog->saveAll($dutyData);
					}
					//** add default on duty/at work[END]

					//** Add entry to user employment[START]
					$userEmploymentData = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $userId)));
					if(!empty($userEmploymentData)){
							// $this->UserEmployment->updateAll(array("company_id"=>86, "from_date"=> date("Y-m-d"), "to_date"=> date("Y-m-d"), "location"=>'In', "description"=>'Test', "is_current"=>1,"designation_id"=>0,"status"=>1,"created"=>date("Y-m-d H:i:s")), array("user_id"=> $userId));
					}else{
						$dutyData = array("user_id"=> $userId, "company_id"=>434, "from_date"=> date("Y-m-d"), "to_date"=> date("Y-m-d"), "location"=>'In', "description"=>'Test',"is_current"=>1,"designation_id"=>0,"status"=>1,"created"=>date("Y-m-d H:i:s"));
						$this->UserEmployment->saveAll($dutyData);
					}
					//** Add entry to user employment[END]

					//** Add entry to Enter Prise User List[START]
					$enterPriseUserListData = $this->EnterpriseUserList->find("first", array("conditions"=> array("email"=> $sheetVal['email'])));
					if(!empty($enterPriseUserListData)){
							// $this->EnterpriseUserList->updateAll(array("company_id"=> 355,"status"=>1, "created"=> date("Y-m-d H:i:s")), array("email"=> $sheetVal['email']));
					}else{
						$enterPriseData = array("email"=> $sheetVal['email'], "company_id"=>434, "status"=> 1, "created"=> date("Y-m-d H:i:s"));
						$this->EnterpriseUserList->saveAll($enterPriseData);
					}
					//** Add entry to Enter Prise User List[END]

					//** Add entry to User Subscribtion Log[START]
					$userSubscriptionLogData = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $userId)));
					if(! empty($userSubscriptionLogData))
					{
				 		// $this->UserSubscriptionLog->updateAll(array('company_id'=>86,'subscription_date'=>date("Y-m-d H:i:s"),'subscription_expiry_date'=>date("Y-m-d H:i:s"),'subscribe_type'=>0), array("user_id"=> $userId));
					}
					else
					{
						$this->UserSubscriptionLog->saveAll(array('user_id'=>$userId,'company_id'=>434,'subscription_date'=>date("Y-m-d H:i:s"),'subscription_expiry_date'=>date("Y-m-d H:i:s"),'subscribe_type'=>0));
					}
				 	//** Add entry to User Subscribtion Log[END]


					//// add data in Quickblox database [start]
				 	$userEmail = stripslashes($sheetVal['email']);
					$oneTimeToken = $this->genrateRandomToken($userId);
           			$oneTimeTokenData = array("user_id"=> $userId, "token"=> $oneTimeToken);
           			$this->UserOneTimeToken->saveAll($oneTimeTokenData);

					//$securePass = '12345678';
					$details = $this->Quickblox->quickAdduserFromOcr(stripslashes($userEmail), $oneTimeToken, stripslashes($userEmail),$userId,stripslashes($sheetVal['first_name']).' '.stripslashes($sheetVal['last_name']));
					$detailsArr = json_decode($details, true);
					//echo "<pre>";print_r($detailsArr['user']['id']);exit();
					// if(!empty($detailsArr) && is_array($detailsArr)){
					// 	//$addDataToQb = $this->addUserQBid($detailsArr, $userId);
					// 	$this->addUserQBid($detailsArr, $userId);
					// }
					//echo "<pre>";print_r($detailsArr);exit();
					//$tokenDetails = $this->Quickblox->quickLogin($userEmail, $oneTimeToken);
					//echo "<pre>";print_r($tokenDetails);exit();
					//$token = $tokenDetails->session->token;
					//$userQbId = $tokenDetails->session->user_id;
					$userQbData = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $userId)));
					if(! empty($userQbData))
					{
						// $this->UserQbDetail->updateAll(array('qb_id'=>$detailsArr['user']['id'],'status'=>1,'created'=>date("Y-m-d H:i:s")), array("user_id"=> $userId));
					}
					else
					{
						$this->UserQbDetail->saveAll(array('user_id'=>$userId,'qb_id'=>$detailsArr['user']['id'],'status'=>1,'created'=>date("Y-m-d H:i:s")));
					}
					//** Set profession as status on QB[START]
					// $professionData = $this->Profession->findById($sheetVal['profession_id']);
					// $profession = !empty($professionData['Profession']['profession_type']) ? $professionData['Profession']['profession_type'] : "";
					// $userCustomQbData = array("userEmail"=> $userEmail, "userPassword"=> $securePass, "userRoleStatus"=> $profession);
					// $customDataQb = $this->Quickblox->updateCustomData($userCustomQbData);
					//// add data in Quickblox database [END]
				}
				else
				{

				}
			}
			else
			{
				$existingEmailCounter++;
			}
			$counter++;

		}
		echo $counter;exit();
	}

	public function updateBatonRoleTags($pageNo) {
		$this->autoRender = false;
		$this->layout = false;

		$pageNum = $pageNo;
		$offsetVal = ( $pageNum - 1 ) * 40;
		$userData_all = $this->User->find('all',array(
			'limit'=> 40,
			'offset' => $offsetVal
		));
		foreach ($userData_all as $userDetails) {
			$roleTagsArr = array(); $roleTagsVals = array(); $batonRoleCount = array();
			$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
			if(!empty($roleTags['Profession']['tags'])){
				$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
			}
			if(!empty($roleTagsVals)){
				foreach($roleTagsVals as $rt){
					$values = array();
					$roleTagId = 0;
					$params['user_id'] = $userDetails['UserProfile']["user_id"];
					$params['key'] = $rt;
					$roleTagUser = $this->RoleTag->userRoleTags($params);
					if(!empty($roleTagUser)){
						$roleTagId = $roleTagUser[0]['rt']['id'];
						$values = array($roleTagUser[0]['rt']['value']);
						$roleTagsArr[] = array("id"=> $roleTagId, "key"=> $rt, "value"=> $values);
					}
				}
			}
			$userData1 = array();
			// $userData1['user_id'] = $userDetails['UserProfile']['user_id'];
			// $userData1['role_tags'] = $roleTagsArr;
			$userbatonRolestatus = array(1,2,5,7);
			// $userBatonRole = $this->UserBatonRole->find("count", array("conditions"=> array("from_user"=> $userDetails['UserProfile']['user_id'], "status"=>$userbatonRolestatus , "is_active"=>1), "group"=>array('UserBatonRole.role_id')));
			// $userData1['user_baton_role_count'] = $userBatonRole;
			$getUserActiveBatonRoles = $this->getUserActiveBatonRoles($userDetails['UserProfile']['user_id']);
			// $userData1['baton_roles'] = $getUserActiveBatonRoles;
			// $userData[] = $userData1;

			$customRoleTagData = array("UserProfile.custom_role"=> "'". json_encode($roleTagsArr) ."'");
			$updateCustomRoleTag = $this->UserProfile->updateAll($customRoleTagData, array("UserProfile.user_id"=> $userDetails['UserProfile']['user_id']));

			$customBatonData = array("UserProfile.custom_baton_roles"=> "'". json_encode($getUserActiveBatonRoles) ."'");
			$updateCustomRoleTag = $this->UserProfile->updateAll($customBatonData, array("UserProfile.user_id"=> $userDetails['UserProfile']['user_id']));
		}
    echo "done";
		// print_r($userData);
		// exit();
	}

	public function updateBulk(){
		// $this->updateBatonRoleTags(1);
		for ($i=1; $i < 155; $i++) {
			$this->updateBatonRoleTags($i);
		}
	}

	public function getUserActiveBatonRoles($userId)
	{
		$batonRoleNameList = array();
		if(isset($userId))
		{
			$getUserBatonRoles = $this->UserBatonRole->find("all", array("conditions"=> array("from_user"=> $userId,"is_active"=>1), "group"=>array('UserBatonRole.role_id'), 'order'=>array('UserBatonRole.updated_at')));
			foreach ($getUserBatonRoles as $value) {
				$getBatonRolesName = $this->BatonRole->find("first", array("conditions"=> array("id"=> $value['UserBatonRole']['role_id'])));
				$getBatonRolesNameOne = $this->DepartmentsBatonRole->find("first", array("conditions"=> array("role_id"=> $value['UserBatonRole']['role_id'])));

				$batonRoleNameList[] = array(
				'role_id'=> isset($getBatonRolesName['BatonRole']['id']) ? $getBatonRolesName['BatonRole']['id'] : "0",
				'is_active'=> '1',
				'role_name'=>isset($getBatonRolesName['BatonRole']['baton_roles']) ? $getBatonRolesName['BatonRole']['baton_roles'] : "",
				// 'on_call_value'=>isset($getBatonRolesNameOne['DepartmentsBatonRole']['on_call_value']) ? $getBatonRolesNameOne['DepartmentsBatonRole']['on_call_value'] : "0",
				'type'=>isset($value['UserBatonRole']['status']) ? $value['UserBatonRole']['status'] : "0",
				);

			}
			return $batonRoleNameList;
		}
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 26-06-2019
	I/P:
	O/P:
	Desc: Anil Sir Requirment
	-----------------------------------------------------------------------------------------
	*/
	public function getUserRegistrationDetail(){
		App::import('Model', 'User');
		$User = new User();
		$data = $User->query("SELECT * FROM `users` AS usr
								LEFT JOIN `user_profiles` AS upro
								ON usr.id = upro.user_id
								LEFT JOIN `user_employments` AS uemp
								ON usr.id = uemp.user_id
								LEFT JOIN company_names AS compname
								ON uemp.company_id = compname.id
							WHERE  registration_date >= (DATE_FORMAT(NOW(), '%Y-%m-%d') - interval 2 DAY) AND registration_date < (DATE_FORMAT(NOW(), '%Y-%m-%d') - interval 1 DAY) ");
		if(!empty($data))
		{
			$filename = 'userRegistrationDetail.csv';
            $csv_file = fopen(APP . '/webroot/googlesheet/credentialData/' . $filename, 'w+');

            $header_row = array("Email", "First Name","Last Name", "Proffession", "Contact Number", "Company Name");
            fputcsv($csv_file, $header_row);
			foreach ($data as  $value) {
				$userEmail = trim($value['usr']['email']);
				$firstName = isset($value['upro']['first_name'])? $value['upro']['first_name'] : "N/A";
				$lastName = isset($value['upro']['last_name']) ? $value['upro']['last_name'] : "N/A";
				$profession = isset($value['prof']['profession_type']) ? $value['prof']['profession_type'] : "N/A";
				$isCurrent = isset($value['uemp']['is_current']) ? $value['uemp']['is_current'] : 0;
				$companyId = isset($value['uemp']['company_id']) ? $value['uemp']['company_id'] : 0;
				$companyName = isset($value['compname']['company_name']) ? $value['compname']['company_name'] : "N/A";
				$contactNum = isset($value['user_profiles']['contact_no']) ? $value['user_profiles']['contact_no'] : "N/A";
				$domainNameArr = explode("@", $userEmail);//** Extract Domain name from email
				$domainName = end($domainNameArr);	//** Domain Name
				$rootDomain = $this->Common->getTld($domainName);
				// $preQualifiedEmail = $this->PreQualifiedDomain->find("count", array("conditions"=> array("domain_name"=> trim($rootDomain), "status"=>1)));
				$preapproved = 0;
				$instituteSelected = 0;
				if($domainName == 'wsh.nhs.uk' || $domainName == 'mediccreations.com' || $domainName == 'wwl.nhs.uk')
				{
					$preapproved = 1;
				}
				if($companyId == 336 || $companyId == 86 || $companyId == 315)
				{
					$instituteSelected = 1;
				}
				if( (!$preapproved == 1 && !$instituteSelected == 1) )
				{
					$rows = array(
			                $userEmail,
			                $firstName,
			                $lastName,
			                $profession,
			                $contactNum,
			                $companyName
		                );
		        	fputcsv($csv_file,array($userEmail,$firstName,$lastName,$profession,$contactNum,$companyName));
				}

			}
			$partialSignupParams['toMail'] = "ehteram@mediccreations.com";
			$partialSignupParams['name'] = "Ehteram";
			$partialSignupParams['partialSignupUrl'] = "abc.com";
			$partialSignupParams['file_name'] = $filename;
			$partialSignupParams['file_path'] = base64_encode(file_get_contents(APP . 'webroot/googlesheet/credentialData/'.$filename));
			$partialSignupParams['file_type'] = "application/vnd.ms-excel";
			$partialSignupMailSend = $this->MbEmail->sendMailPartialSignupUserTest($partialSignupParams);
			fclose($csv_file);
			echo "<pre>"; print_r($dataArr);
			exit;
		}
		else{
			echo "No Records Found";
			exit();
		}
	}


	public function updatePermanentRoleTags() {
		$this->autoRender = false;
		$this->layout = false;
		$counter = 0;
		$conditions = " UserProfile.profession_id != 4 AND UserProfile.profession_id != 5 AND UserProfile.profession_id != 6 AND UserProfile.profession_id != 7 AND UserProfile.profession_id != 15 AND UserProfile.profession_id != 16 AND UserProfile.profession_id != 17 AND UserProfile.profession_id != 18 AND UserEmployment.is_current = 1";

		// $userData_all = $this->UserProfile->find('all', array("conditions"=> $conditions));


		$userData_all = $this->UserProfile->find('all', array(
		    'joins' => array(
		        array(
		            'table' => 'user_employments',
		            'alias' => 'UserEmployment',
		            'type' => 'LEFT',
		            'conditions' => array(
		                'UserProfile.user_id = UserEmployment.user_id'
		            )
		        )
		    ),
		    'conditions' => $conditions,
		    'fields' => array('UserProfile.*', 'UserEmployment.*'),
		    'order' => 'UserProfile.id DESC'
		));
		// $userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
		foreach ($userData_all as $userDetails) {
			$counter++;
			$roleTagsArr = array(); $roleTagsVals = array(); $batonRoleCount = array();
			$roleTags = $this->Profession->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['profession_id'])));
			if(!empty($roleTags['Profession']['tags'])){
				$roleTagsVals = explode(",", $roleTags['Profession']['tags']);
			}
			if(!empty($roleTagsVals)){
				foreach($roleTagsVals as $rt){
					$values = array();
					$params['user_id'] = $userDetails['UserProfile']["user_id"];
					$params['key'] = $rt;
					$roleTagUser = $this->RoleTag->userRoleTags($params);
					if(!empty($roleTagUser)){
						$values = array($roleTagUser[0]['rt']['value']);
						if($rt == 'Base Location')
						{

						}
						else
						{
							$roleTagsArr[] = array("tag"=> $rt, "values"=> preg_replace('/[^a-zA-Z0-9_ -$]/s','',$values));
						}
					}
				}
			}

			if($userDetails['UserProfile']["role_status"]  != NULL || $userDetails['UserProfile']["role_status"] != '' ){
				$roleStatusVal = preg_replace('/[^a-zA-Z0-9_ -$]/s','',$userDetails['UserProfile']["role_status"]);

			}
			else
			{
				$roleStatusVal = '';
			}
			$roleArra = array('text'=>$roleStatusVal, 'role'=>$roleTagsArr);
			if( (empty($roleStatusVal) || $roleStatusVal == '') && empty($roleTagsArr))
			{

			}
			else
			{
				$getUserActiveBatonRoles = $this->getUserRoleStatusArr($roleStatusVal, $roleTagsArr);

				$customBatonData = array("UserProfile.custom_permanent_role"=> "'". json_encode($getUserActiveBatonRoles) ."'");
				$updateCustomRoleTag = $this->UserProfile->updateAll($customBatonData, array("UserProfile.user_id"=> $userDetails['UserProfile']['user_id']));

				$userPermanentData = $this->UserPermanentRole->find("first", array("conditions"=> array("user_id"=> $userDetails['UserProfile']["user_id"])));
				if(empty($userPermanentData))
				{
					$saveUserPermanentRole = array("user_id"=>$userDetails['UserProfile']["user_id"], "other_user_id"=>"0", "institute_id"=> $userDetails['UserEmployment']['company_id'], "user_role_id"=>"0","role"=>json_encode($roleArra), "status"=>"1", "active"=> "1",  "created_at"=> date("Y-m-d H:i:s"),  "updated_at"=> date("Y-m-d H:i:s"));
					$this->UserPermanentRole->saveAll($saveUserPermanentRole);
					$role_id = $this->UserPermanentRole->getLastInsertId();
					$this->UserPermanentRole->updateAll(array("user_role_id"=> $role_id), array("id"=>$role_id));
				}
			}
		}
		//******* Delete Cache in case of Delete, Activate, Deactivate[START] ********//
		echo "Done Total Count = ".$counter;exit();
	}

	public function getUserBulkData(){
		for ($i=1; $i < 50; $i++) {
			$this->updatePermanentRoleTags($i);
		}
		echo "Done";exit();
	}

	public function getUserRoleStatusArr($roleStatusVal, $roleTagsArr)
	{
		$permanemtRoleNameList = array();
		$permanemtRoleNameList[] = array(
		'text'=> $roleStatusVal,
		'role'=> $roleTagsArr,
		);
		return $permanemtRoleNameList;
	}

	public function updateUserSubscription()
	{
		$counter = 0;
		$companySubscriptionLogData = $this->CompanyName->find("first", array("conditions"=> array("id"=> "336")));
		$company_id = $companySubscriptionLogData['CompanyName']['id'];
		$subscriptionExpireDate = $companySubscriptionLogData['CompanyName']['subscription_expiry_date'];
		echo "id: ".$company_id." "."Subscription: ".$subscriptionExpireDate."<br />";
		$userSubscriptionLogData = $this->UserSubscriptionLog->find("all", array("conditions"=> array("company_id"=>$company_id)));

		foreach ($userSubscriptionLogData as  $value) {
			$counter++;
			$this->UserSubscriptionLog->updateAll(array("subscription_expiry_date"=>"'".$subscriptionExpireDate."'"),array("company_id"=> $company_id,"user_id"=> $value['UserSubscriptionLog']['user_id']));
		}
		echo "Done Total Count :".$counter;exit();
		// echo "<pre>";print_r($userSubscriptionLogData);exit();
	}


	public function checkpushOnIos()
	{
		$params['fcm_voip_token'] = "1901670593e719e1c9d404d683cfc06a908f18f94d59d3d5c4ad875f56ca33ee";
		if(!empty($params['fcm_voip_token'])) {
			$fcmVoipToken = $params['fcm_voip_token'];
			$passphrase = 'password';
			$message = 'User Role Exchange';
			$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "ios_voip"=>1,
                        "VOIPCall"=>1
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}

	}

	// public function checkUserVersion()
	// {
	// 	$val = "13.";
	// 	$institutionId = "336";
	// 	$conditions = "";
	// 	$counter = 0;
	// 	$conditions .= ' ult.`device_info` LIKE "%'.$val.'%" AND udl.`hospital_id`=336';
	// 	$myColleagueQuer = "SELECT ult.`user_id`, usr.`email`,  upro.`first_name`, upro.`last_name`, ult.`login_status` AS UserLoginStatus, udl.`atwork_status` AS AvailabiltyStatus
	// 				FROM `user_login_transactions` AS ult
	// 				LEFT JOIN `user_duty_logs` udl
	// 				ON ult.user_id = udl.user_id
	// 				LEFT JOIN `users` usr
	// 				ON ult.user_id = usr.id
	// 				LEFT JOIN `user_profiles` upro
	// 				ON usr.id = upro.user_id
	// 				WHERE ult.id IN (
	// 				    SELECT MAX(id)
	// 				    FROM user_login_transactions
	// 				    GROUP BY `user_id`
	// 				)														
	// 				 AND  " . $conditions;
	// 	App::import('model','User');
	// 	$users = new User();
 //        $iOSUserVersionData = $users->query($myColleagueQuer);

 //        if(!empty($iOSUserVersionData))
 //        {
 //        	// echo "<pre>";print_r($iOSUserVersionData);exit();
 //        	echo "<b> iOS User version: -- 13</b></br>";
	//         echo $var .= "<table cellpadding=\"5\">
	// 		<thead>
	// 		<tr>
	// 		  <th style='width:50px;text-align:left; margin-right: 20px;'>Email</th>
	// 		  <th style='width:50px;text-align:left; margin-right: 20px;'>Name</th>
	// 		  </tr>
	// 		</thead>
	// 		<tbody>";
	// 	    foreach ($iOSUserVersionData as $key => $value) {
	// 	    	$counter++;
	// 	    	echo '<tr>
	// 	    	<td>' . $value['usr']['email'] . '</td>
	// 	    	<td>' . $value['upro']['first_name'] ." ".$value['upro']['last_name'] . '</td>
	// 	    	</tr>';
	// 	    	// echo "Email: ".$value['usr']['email']."<br />';
	// 	    	// echo "Name: Ehteram Ahmad";exit();

	// 	    }
	// 	    "</tbody>
	// 		</table>";
 //        }
 //        else
 //        {
 //        	echo "No result found";
 //        }
 //        echo "<i> Total Count: </i>".$counter;
        
	// exit();
	// }


}
