<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $uses = array('UserDevice', 'User', 'Mbapi.VoipPushNotification', 'Mbapi.UserDndStatus','UserOneTimeToken','UserDutyLog','Mbapi.UserBatonRole','Mbapi.BatonRole','Mbapi.UserProfile','Mbapiv2.UserPermanentRole','Mbapiv1.UserLoginTransaction','Mbapiv1.UserDeviceInfo','Mbapiv3.UserBatonRoleTransaction','Mbapiv3.AvailableAndOncallTransaction','Mbapiv3.DndTransaction');
	public $components = array('Common','Session', 'Quickblox', 'Cache');

	function endc( $array ) { return end( $array ); }

	public function beforeFilter() {
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, If-Modified-Since, Cache-Control, Pragma, access_key, token");

		// Sets a key for the session with an expiry.  This is used by the app
		// via an endpoint to determine whether the user should be logged in or
		// not. This is almost certainly at least a partial duplication of work,
		// however at this point in the product lifecycle it doesn't make sense
		// to disambiguate and refactor: this solution should be fast enough, and
		// the session info should only be used for information governance.

		$blocklist = array(
			"mbapiv4sessionexpiry",
			"mbapiv3sessionexpiry",
		);

		$path   = $this->request->here();
		$action = $this->endc(explode("/", $path));

		if (!in_array($action, $blocklist)) {
			$header = getallheaders();

			$token = null;
			if (array_key_exists('token', $header)) {
				$token = $header['token'];
			} else if (array_key_exists('Token', $header)) {
				$token = $header['Token'];
			}

			if (!empty($token)) {
				$this->Cache->resetSessionTTL($token);
			}
		} 
	}

	/*
	On: 05-08-2015
	I/P: NA
	O/P: true/false, if access key valid/invalid
	Desc: Checks valid access key provided by API requester.
	*/
	public function accesskeyCheck() {
		$header = getallheaders();
		$accessKey = $this->Common->decryptData($header['access_key']);
		if(in_array($accessKey, Configure::read('API_ACCESS_KEY'))) {
			return true;
		}else{
			return false;
		}

	}

/*
________________________________________________
Method Name:=>getAppType
Purpose:Used to get the application name which is accesing the services ie:MB,TheOCR,Web
Detail:
created by :Developer Shashi
Created date:07-06-16
Params:NULL
_________________________________________________
*/
	public function getAppType() {
		$header = getallheaders();
		$applicationType = $header['application_type'];
		if($applicationType!='') {
			return $applicationType;
		}else{
			return '';
		}

	}

	/*
	On: 10-08-2015
	I/P: token (passed by headers)
	O/P: true/false
	Desc: It checks user logged in token and if validate it will return true else false
	*/
	public function tokenValidate() {
		$header = getallheaders();
		$token = $this->Common->decryptData($header['token']);
		$params['status'] = 1;
		$params['token'] = $token;

		if( !empty($params) ) {
			$cnt = $this->UserDevice->find('count', array('conditions'=> $params));
			if( $cnt > 0 ) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}

	/*
	On:
	I/P: NA
	O/P: returns array()
	Desc: returns all header values.
	*/
	public function getHeaderValues() {
		return getallheaders();
	}
/*
________________________________________________
Method Name:checkAdminSession
Purpose:Check the admin session
Detail: Get the Admin id and return true/false
created by :Developer
Created date:31-08-15
Params:None

_________________________________________________
*/
    public function checkBeatUserSession() {
		$value = $this->Session->read('beatPostUserName.email');
		if($value!='') {
			return $value;

		}else{
			return false;
		}
    }
/*
________________________________________________
Method Name:adminAuthorisation
Purpose:Provide admin access to authoried page
Detail:
created by :Developer
Created date:31-08-15
Params:None
_________________________________________________
*/
  public function beatUserAuthorisation() {
		if($this->checkBeatUserSession()) {
			return true;
		}else{
			$this->Session->setFlash(Configure::read('ERROR_617'));
			$this->redirect(array('plugin'=>'institution','controller'=>'institutionindex','action'=>'index'));

		}
    }
/*
________________________________________________
Method Name:=>getDeviceType
Purpose:Used to get the Device Type(Ex:Web,App etc)
Detail:
created by :
Created date:04-05-17
Params:NULL
_________________________________________________
*/
	public function getDeviceType() {
		$header = getallheaders();
		$deviceType = $header['device_type'];
		if($deviceType!='') {
			return $deviceType;
		}else{
			return '';
		}

	}

	/*
	________________________________________________
	Method Name:=>getUserProfileStatus
	Purpose:Used to get the User Status(inactive, deleted etc.)
	Detail:
	created by :
	Created date:05-07-17
	Params:NULL
	_________________________________________________
	*/

	public function getUserProfileStatus($userId, $loggedInUserId) {
		$responceMassage = array();
		//$getstatus = $this->User->find("first", array("conditions"=> array("id"=> $userId)));
		$getstatus = $this->User->find('first', array('conditions'=> array("User.id"=> $userId)));
		if(($userId == $loggedInUserId) && ($getstatus['User']['status'] == 0 || $getstatus['User']['status'] == 2))
		{
			$responceMassage['message'] =  ERROR_651;
			$responceMassage['response_code'] = 651;
		}
		else
		{
			if($getstatus['User']['status'] == 0)
			{
				$responceMassage['message'] =  ERROR_652;
				$responceMassage['response_code'] = 652;

			}
			else if($getstatus['User']['status'] == 2)
			{
				$responceMassage['message'] =  ERROR_653;
				$responceMassage['response_code'] = 653;
			}
			else
			{
				$responceMassage['message'] =  ERROR_654;
				$responceMassage['response_code'] = 654;
			}
		}
		return $responceMassage;
	}

	/*
	________________________________________________
	Method Name:=>getUserStatus
	Purpose:Used to get the User Status(inactive, deleted etc.)
	Detail:
	created by :
	Created date:05-07-17
	Params:NULL
	_________________________________________________
	*/

	public function getUserStatus($userId, $loggedInUserId) {
		$responceMassage = array();
		$getstatus = $this->User->find('first', array('conditions'=> array("User.id"=> $userId)));
		if(($userId == $loggedInUserId) && ($getstatus['User']['status'] == 0 || $getstatus['User']['status'] == 2))
		{
			$responceMassage['message'] =  ERROR_651;
			$responceMassage['response_code'] = 651;
		}
		else
		{
			if($getstatus['User']['status'] == 0)
			{
				$responceMassage['message'] =  ERROR_652;
				$responceMassage['response_code'] = 652;

			}
			else if($getstatus['User']['status'] == 2)
			{
				$responceMassage['message'] =  ERROR_653;
				$responceMassage['response_code'] = 653;
			}
			else
			{
				$responceMassage['message'] =  ERROR_653;
				$responceMassage['response_code'] = 653;
			}
		}
		return $responceMassage;
	}

	public function getUserProfileStatuscheck($userId, $iscurrntUser)
	{
		$responceMassage = array();
		$getstatus = $this->User->find('first', array('conditions'=> array("User.id"=> $userId)));
		if($iscurrntUser == 1)
		{
			$responceMassage['message'] =  ERROR_651;
			$responceMassage['response_code'] = 651;
		}
		else
		{
			if($getstatus['User']['approved'] == 0)
			{
				$responceMassage['message'] =  ERROR_652;
				$responceMassage['response_code'] = 652;
			}
			if($getstatus['User']['status'] == 2)
			{
				$responceMassage['message'] =  ERROR_653;
				$responceMassage['response_code'] = 653;
			}
		}
		return $responceMassage;
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function checkEtagNotModified($params = array()){
		$header = getallheaders();
		// $eTag = (isset($header['Etag']) && !empty($header['Etag'])) ? $header['Etag'] : 0;
		$eTag = (isset($header['Etag']) && !empty($header['Etag'])) ? $header['Etag'] : 0;

		if($eTag == 0){
			$eTag = (isset($header['etag']) && !empty($header['etag'])) ? $header['etag'] : 0;
		}
		$etagVals = array();
		$cacObj = $this->Cache->redisConn();
		if(($cacObj['connection']) && empty($cacObj['errors'])){
						if($cacObj['robj']->exists($params['key'])){
							$etagVals = $cacObj['robj']->hgetall($params['key']);
						}
		}

		if($etagVals['eTag'] == (string) $eTag){
			$etagVals['etagHeader'] = $eTag;
			$etagVals['code'] = 304;
			return $etagVals;
		}elseif(empty($eTag)){
			$etagVals['etagHeader'] = 0;
			return $etagVals;
		}else{
				if($cacObj['robj']->exists('institutionDirectory:'.$params['institution_key'])){
					$etagVals['etagHeader'] = $etagVals['eTag'];
					$etagVals['code'] = 200;
					return $etagVals;
				}
				else{
					$etagVals['etagHeader'] = $eTag;
					return $etagVals;
				}
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function checkEtagNotModifiedDynamicDirectory($params = array()) {
		$header = getallheaders();
		// $eTag = (isset($header['Etag']) && !empty($header['Etag'])) ? $header['Etag'] : 0;
		$eTag = (isset($header['Etag']) && !empty($header['Etag'])) ? $header['Etag'] : 0;

		if($eTag === 0) {
			$eTag = (isset($header['etag']) && !empty($header['etag'])) ? $header['etag'] : 0;
		}
		$etagVals = array();
		$cacObj = $this->Cache->redisConn();
		if(($cacObj['connection']) && empty($cacObj['errors'])) {
			if($cacObj['robj']->exists($params['key'])) {
				$etagVals = $cacObj['robj']->hgetall($params['key']);
			}
		}
		$previousDateModified = explode("~",$eTag);

		if($etagVals['eTag'] == $previousDateModified[0]) {
			$etagVals['etagHeader'] = $eTag;
			$etagVals['code'] = 304;
			return $etagVals;
		}elseif(empty($eTag)) {
			$etagVals['etagHeader'] = 0;
			return $etagVals;
		}else{
			$etagVals['etagHeader'] = $eTag;
			return $etagVals;
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function updateStaticEtagData($params = array()) {
		$cacObjEtag = $this->Cache->redisConn();
		if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])) {
				$etagLastModified = strtotime(date('Y-m-d H:i:s'));
				$eTagNewVal = md5($params['key'] . rand(1,10000) . $etagLastModified);
				$cacObjEtag['robj']->hset($params['key'],'eTag' , $eTagNewVal);
				$cacObjEtag['robj']->hset($params['key'],'lastmodified' , $etagLastModified);
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function updateDynamicEtagData($params = array()) {
		$cacObjEtag = $this->Cache->redisConn();
		if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])) {
				//$dynamicPreviousModifiedDate = $cacObjEtag['robj']->hget($params['key'],'lastmodified');
				//$cacObjEtag['robj']->hset($params['key'],'previousLastmodified' , $dynamicPreviousModifiedDate);
				$etagLastModified = strtotime($params['lastmodified']);
				$eTagNewVal = md5($params['key'] . rand(1,10000) . $etagLastModified);
				$cacObjEtag['robj']->hset($params['key'],'eTag' , $eTagNewVal);
				$cacObjEtag['robj']->hset($params['key'],'lastmodified' , $etagLastModified);
		}
	}

	/*
	--------------------------------------------------------------------
	On: 09-05-2018
	I/P: Rsa encrypted token
	O/P: Return true or false if token exist or not
	Desc:
	--------------------------------------------------------------------
	*/

	public function tokenValidation() {
		$header = getallheaders();
		if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		else
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		$paramsData = $header['token'];
		openssl_private_decrypt(base64_decode($paramsData), $decrptedData, $pkGeneratePrivate);
		$params['status'] = 1;
		$params['token'] = $decrptedData;
		if( !empty($params) ) {
			$cnt = $this->UserDevice->find('count', array('conditions'=> $params));
			if( $cnt > 0 ) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 09-05-2018
	I/P: Rsa encrypted access key
	O/P: Return true or false if token exist or not
	Desc:
	--------------------------------------------------------------------
	*/

	public function accesskeyCheckValidation() {
		$header = getallheaders();
		if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		else
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		$paramsData = $header['access_key'];
		openssl_private_decrypt(base64_decode($paramsData), $decrptedData, $pkGeneratePrivate);
		// $accessKey = $this->Common->decryptData($header['access_key']);
		if(in_array($decrptedData, Configure::read('API_ACCESS_KEY'))) {
			return true;
		}else{
			return false;
		}

	}


	/*
	--------------------------------------------------------------------
	On: 12-06-18
	I/P: token in header
	O/P: true/false if token exist or not
	Desc: This function is use to check the user access for the plane api Mbapi
	--------------------------------------------------------------------
	*/

	public function validateToken() {
		$header = getallheaders();
		$token = $header['token'];
		$params['status'] = 1;
		$params['token'] = $token;
		if( !empty($params) ) {
			$cnt = $this->UserDevice->find('count', array('conditions'=> $params));
			if( $cnt > 0 ) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 12-06-18
	I/P: access_key in header
	O/P: true/false if access_key exist or not
	Desc: This function is use to check the user access for the plane api Mbapi
	--------------------------------------------------------------------
	*/

	public function validateAccessKey() {
		$header = getallheaders();
		$accessKey = $header['access_key'];
		if(in_array($accessKey, Configure::read('API_ACCESS_KEY'))) {
			return true;
		}else{
			return false;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 22-10-18
	I/P: user_id in header
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function genrateRandomToken($userId=NULL)
	{
		if(!empty($userId))
		{
			$userInfo = $userId.rand(1000,100000);
			$options = [
			    'cost' => 4,
			];
			$token = md5(password_hash($userInfo, PASSWORD_BCRYPT, $options));
			// $token = md5(crypt($userInfo));
		}
		return $token;
	}


	public function sendCustomVoipPush($params =array()) {
		if(!empty($params)) {
			if($params['user_id']) {
				$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
			}else if($params['voip_tokens']) {
				$userVoipData[] = $params['voip_tokens'];
			}
			if(!empty($userVoipData)) {
				foreach ($userVoipData as $value) {
					$params['device_token'] = $value['VoipPushNotification']['device_token'];
					$params['fcm_voip_token'] = $value['VoipPushNotification']['fcm_voip_token'];
					$sendNotification = $this->sendPushOnIos($params);
					$sendNotification = $this->sendPushOnAndroid($params);
				}
			}else{
				return "Data is empty";
			}
		}else{
			return "Data is empty";
		}
	}

	public function sendPushOnIos($params = array()) {
		if(!empty($params)) {

			$deviceToken = $params['device_token'];
			$fcmVoipToken = $params['fcm_voip_token'];
			$passphrase = 'password';
			$message = $params['notificationMessage'];
			$pemFile = 'app/Config/voipssl/'.VOIP_CERTIFICATE_IOS;
			// $pemFile = WWW_ROOT.'/voipssl/'.'apns-dist-cert.pem';
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx
			);

			if (!$fp) {
				exit("Failed to connect: $err $errstr" . PHP_EOL);
			}

			$body = array(
				"aps"=> array(
					"alert"=> $message,
					"sound"=> "notification_sound.mp3"
				),
        "ios_voip"=> 1,
				'VOIPCall'=> 1,
				"is_broadcast"=> 1,
				"show_alert"=>1,
				"tap_to_reply" => 0
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			// print_r($result);
			if(!$result) {
				$var = 0;
			}else{
				$var = 1;
			}
			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}

	public function sendPushOnAndroid($params = array()) {
		if(!empty($params['fcm_voip_token'])) {
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$message = $params['notificationMessage'];

			$voipPushMessage = array(
				"message" => $message,
				'VOIPCall'=> 1,
				"is_broadcast"=> 1,
				"show_alert"=>1,
				"tap_to_reply" => 0
			);

			$postFeilds = array(
				'registration_ids' => $tokenRegistrationIds,
				'data' => $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);

			$ch = curl_init();
			// curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			// print_r($result);
			curl_close( $ch );
			return $var = 1;
		}else{
			return $var = 0;
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendVoipPushNotification($params =array())
	{
		if(!empty($params)) {
			$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
			if(!empty($userVoipData))
			{
				foreach ($userVoipData as $key => $value) {
					$params['device_token'] = $value['VoipPushNotification']['device_token'];
					$params['fcm_voip_token'] = $value['VoipPushNotification']['fcm_voip_token'];
					$params['is_dnd_active'] = $params['is_dnd_active'];
					$params['push_on'] = $params['push_on'];
					$params['dnd_data'] = $params['dnd_data'];
					$sendNotification = $this->sendPushNotificationOnIosOne($params);
					$sendNotification = $this->sendPushNotificationAndroidOne($params);
				}
			}

		}else{
			return "Data is empty";
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendPushNotificationOnIosOne($params = array())
	{
		if(!empty($params))
		{

			$deviceToken = $params['device_token'];
			$fcmVoipToken = $params['fcm_voip_token'];
			$isDndActive = $params['is_dnd_active'];
			$pushOn = $params['push_on'];
			$dndData = $params['dnd_data'];
			$passphrase = 'password';
			$message = 'Dissmiss Dnd';
			$pemFile = 'app/Config/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "ios_voip"=>1,
                        "VOIPCall"=>1,
                        "VOIPDndStatus"=>$isDndActive,
                        "push_on"=>$pushOn,
                        "dnd_data"=>$dndData
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}
	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendPushNotificationAndroidOne($params = array()) {
		if(!empty($params))
		{
			$responseData = $this->sendDndPushNotificationOnAndroid($params);
			return $responseData;
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function sendDndPushNotificationOnAndroid($params = array())
	{
		if(!empty($params['fcm_voip_token'])) {
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$isDndActive = $params['is_dnd_active'];
			$pushOn = $params['push_on'];
			$voipPushMessage = array
					(
						'VOIPDndStatus'=> $isDndActive,
						"push_on"=>$pushOn,
						'VOIPCall'=> 1
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $var = 1;
		}
		else
		{
			return $var = 0;
		}

	}


	/*
	--------------------------------------------------------------------------
	ON: 12-02-2019
	I/P:
	O/P: True or False
	Desc: This function will send a voip/fcm push notification to dissmiss
		  the Dnd status options.
	--------------------------------------------------------------------------
	*/

	public function dissmissDndOnOcrAndQb($params = array(),$message)
	{
		if(!empty($params))
		{
			$userId = $params['user_id'];
			// DK: Save the DND transaction------->>>>>>>>
			$dnd_conditions = array(
				'UserDndStatus.is_active' => 1,
				'UserDndStatus.user_id' => $userId
			);
			$options = array(
					'conditions'=>$dnd_conditions
				);
			$userDndStatus = $this->UserDndStatus->find('first',$options);
			// DK: Save the DND transaction------->>>>>>>>

			if(!empty($params['email'])) {
				$params['user_email'] = $params['email'];
			}else{
				$getUserDetail = $this->User->find('first', array('conditions'=> array("User.id"=> $userId)));
				$params['user_email'] = $getUserDetail['User']['email'];
			}
			//***********Update Dnd Status On Ocr
			$this->UserDndStatus->updateAll(array("is_active"=> 0, "duration"=> 0), array('user_id'=>$userId));
			//***********Update Dnd Status On Qb
			$oneTimeTokenForDnd = $this->genrateRandomToken($userId);
			$oneTimeTokenDataDnd = array("user_id"=> $userId, "token"=> $oneTimeTokenForDnd);
			$this->UserOneTimeToken->saveAll($oneTimeTokenDataDnd);
			$params['one_time_token'] = $oneTimeTokenForDnd;
			$tokenDetails = $this->Quickblox->quickLogin($params['user_email'], $params['one_time_token']);
			$token = $tokenDetails->session->token;
			$user_id = $tokenDetails->session->user_id;
			$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
			$custom_data_from_api = json_decode($user_details->user->custom_data);
			$customData['userRoleStatus'] = $custom_data_from_api->status;
			$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
			$customData['isImport'] = isset($is_import) ? $is_import : "true";
			$customData['at_work'] = $custom_data_from_api->at_work;
			$customData['on_call'] = $custom_data_from_api->on_call;
			$customData['is_dnd_active'] = 0;
			if(!empty($token)) {
				$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
				$res = json_decode(json_encode($qbResponce));
				if(!empty($res['errors'])) {
					$response['qberror'] = $qbResponce;
				}else{
					$response['qbsuccess'] = 1;
					if(isset($custom_data_from_api->dnd_status)){
						$this->DndTransaction->saveAll(array(
							'user_id'=>$userDndStatus['UserDndStatus']['user_id'],
							'institute_id'=>$userDndStatus['UserDndStatus']['institute_id'],
							"dnd_status_id"=>$userDndStatus['UserDndStatus']['dnd_status_id'],
							"start_time"=>$userDndStatus['UserDndStatus']['start_time'],
							"end_time"=>$userDndStatus['UserDndStatus']['end_time'],
							"duration"=>$userDndStatus['UserDndStatus']['duration'],
							"is_active"=>"0",
							"other_user_id"=>$userDndStatus['UserDndStatus']['other_user_id'],
							"note"=>$userDndStatus['UserDndStatus']['note'],
							"custom_data"=>$message ? $message : 'DND Dissmiss',
							'created'=>date("Y-m-d H:i:s")
						));
					}
				}
			}else{
				$response['qberror'] = $tokenDetails;
			}
			return $response;
		}
	}

	/*
	--------------------------------------------------------------------------
	ON: 22-02-2019
	I/P:
	O/P: True or False
	Desc: This function will send a voip/fcm push notification to dissmiss
		  the Dnd status options.
	--------------------------------------------------------------------------
	*/

	public function sendLogoutPushNotification($params =array())
	{
		if(!empty($params)) {
			$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
			if(!empty($userVoipData))
			{
				$counter = 0;
				foreach ($userVoipData as $key => $value) {
					$params['device_token'] = $value['VoipPushNotification']['device_token'];
					$params['fcm_voip_token'] = $value['VoipPushNotification']['fcm_voip_token'];
					$params['is_dnd_active'] = $params['is_dnd_active'];
					$params['user_id'] = $params['user_id'];
					if($params['device_id'] != $value['VoipPushNotification']['device_id'])
					{

						$sendNotification = $this->sendLogoutPushNotificationOnIosOne($params);
						if($sendNotification == 1)
						{
							$deleteRecords = $this->VoipPushNotification->deleteAll(array("user_id"=>$params['user_id'], "device_id !="=>$params['device_id']));
						}
						$sendNotificationAndroid = $this->sendLogoutPushNotificationAndroidOne($params);
						if($sendNotificationAndroid == 1)
						{
							$deleteRecords = $this->VoipPushNotification->deleteAll(array("user_id"=>$params['user_id'], "device_id !="=>$params['device_id']));
						}
						$counter++;
					}
				}
				return $counter;
			}

		}else{
			return "Data is empty";
		}
	}


	public function sendLogoutPushNotificationOnIosOne($params = array())
	{
		if(!empty($params))
		{

			$deviceToken = $params['device_token'];
			$fcmVoipToken = $params['fcm_voip_token'];
			$userId = isset($params['user_id']) ? $params['user_id'] : "";
			$passphrase = 'password';
			$message = 'Dissmiss Dnd';
			$pemFile = 'app/Config/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "ios_voip"=>1,
                        "VOIPCall"=>1,
                        "VOIPLogoutStatus"=>1,
                        "message"=>ERROR_602,
                        "user_id"=>$userId
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}

	public function sendLogoutPushNotificationAndroidOne($params = array()) {
		if(!empty($params))
		{
			$responseData = $this->sendLogoutPushNotificationOnAndroid($params);
			return $responseData;
		}
	}

	public function sendLogoutPushNotificationOnAndroid($params = array())
	{
		if(!empty($params['fcm_voip_token'])) {
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$isDndActive = $params['is_dnd_active'];
			$userId = isset($params['user_id']) ? $params['user_id'] : "";
			$voipPushMessage = array
					(
						'VOIPLogoutStatus'=> 1,
						'VOIPCall'=> 1,
						'message'=>ERROR_602,
						'user_id'=>$userId
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $var = 1;
		}
		else
		{
			return $var = 0;
		}

	}


	public function sendRoleExchangePushNotification($params)
	{
		if(!empty($params))
		{
			$sendRoleExchangePushNotification = $this->VoipPushNotification->find('first', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
			if($params['from_user_id'] == 0)
			{
				$userData['UserProfile']['first_name'] = "Switch";
				$userData['UserProfile']['last_name'] = "Borad";
			}
			else
			{
				$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $params['from_user_id'])));
			}
			if($params['user_id'] == 0)
			{
				$userDataTo['UserProfile']['first_name'] = "Switch";
				$userDataTo['UserProfile']['last_name'] = "Borad";
			}
			else
			{
				$userDataTo = $this->User->find('first', array('conditions'=> array("User.id"=> $params['user_id'])));
			}
			if(!empty($sendRoleExchangePushNotification))
			{
				$param['fcm_voip_token'] = $sendRoleExchangePushNotification['VoipPushNotification']['fcm_voip_token'];
				$param['device_type'] = $sendRoleExchangePushNotification['VoipPushNotification']['device_type'];
				$param['request_type'] = $params['request_type'];
				$param['first_name'] = $userData['UserProfile']['first_name'];
				$param['last_name'] = $userData['UserProfile']['last_name'];
				$param['push_on'] = $params['push_on'];
				$param['role_id'] = $params['role_id'];
				$param['is_qr_request'] = isset($params['is_qr_request']) ? $params['is_qr_request'] : 0;
				$param['from_user_id'] = $params['from_user_id'];
				$param['to_user'] = $params['user_id'];

				$getBatonRoleName = $this->BatonRole->find('first', array('conditions'=> array("BatonRole.id"=> $param['role_id'])));
				$param['role_name'] = $getBatonRoleName['BatonRole']['baton_roles'];
				$param['to_user_first_name'] = $userDataTo['UserProfile']['first_name'];
				$param['to_user_last_name'] = $userDataTo['UserProfile']['last_name'];


				// if($param['device_type'] == 'ios' || $param['device_type'] == 'iOS')
				// {
				// 	$sendNotification = $this->roleExchangePushNotificationOnIos($param);
				// }
				// else if($param['device_type'] == 'android' || $param['device_type'] == 'Android')
				// {
				// 	$sendNotification = $this->roleExchangePushNotificationOnAndroid($param);
				// }
				$sendNotification = $this->roleExchangePushNotificationOnIos($param);
				$sendNotification = $this->roleExchangePushNotificationOnAndroid($param);
			}
		}
	}

	public function roleExchangePushNotificationOnIos($params = array())
	{
		if(!empty($params))
		{
			$var = "";
			$requestTypeVal = 0;
			if($params['request_type'] == 'transfer')
			{
				// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
				$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'].".";
				if($params['to_user'] != 0){
					$custMessage = $params['first_name']." ".$params['last_name']." Transfer " .$params['role_name']. " role to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = $params['first_name']." ".$params['last_name']." Transfer ".$params['role_name']." to Switch Borad ";
				}
			}
			elseif ($params['request_type'] == 'transfer_accept') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_004;
				$var = "Accepted";
				if($params['to_user'] != 0){
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
			}
			elseif ($params['request_type'] == 'transfer_rejected') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_005;
				$var = "Rejected";
				$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'request') {
				$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'];
				$requestTypeVal = $params['push_on'];
				if($params['to_user'] != 0){
					$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
			}
			elseif ($params['request_type'] == 'request_accept') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_002;
				$var = "Accepted";
				$requestTypeVal = $params['push_on'];
				if($params['to_user'] != 0){
					$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
			}
			elseif ($params['request_type'] == 'request_reject') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_003;
				$var = "Rejected";
				if($params['to_user'] != 0){
					$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
			}
			elseif ($params['request_type'] == 'revoke_request') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
				$var = "Revoked";
				$custMessage = "Request revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'revoke_transfer') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
				$var = "Revoked";
				$custMessage = "Transfer revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'timeout') {
				$var = "Timeout";
				$messageVal = ROLEEX_007;
			}
			elseif ($params['request_type'] == 'swichborad_accept') {
				$var = "Accepted";
				$messageVal = ROLEEX_008;
				$requestTypeVal = $params['push_on'];
				$custMessage = "Request accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'swichborad_rejected') {
				$var = "Rejected";
				$messageVal = ROLEEX_009;
				$custMessage = "Request rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			//TO do this will be removed when auto accept functionality goes live
			elseif ($params['request_type'] == 'swichborad_transfered') {
				$var = "Requested";
				$messageVal = ROLEEX_019;
				$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			// elseif ($params['request_type'] == 'swichborad_added') {
			elseif ($params['request_type'] == 'swichborad_assigned') {
				$var = "Assigned";
				$messageVal = ROLEEX_011;
				$requestTypeVal = $params['push_on'];
				$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'swichborad_removed') {
				$var = "Rejected";
				$messageVal = ROLEEX_012;
			}
			elseif ($params['request_type'] == 'swichborad_transfer_accept') {
				$var = "Accepted";
				$messageVal = ROLEEX_013;
				$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'swichborad_transfer_rejected') {
				$var = "Rejected";
				$messageVal = ROLEEX_014;
				$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];

			}
			elseif ($params['request_type'] == 'swichborad_unassign') {
				$var = "Unassigned";
				$messageVal = ROLEEX_017;
			}
			elseif ($params['request_type'] == 'swichborad_deleted') {
				$var = "Deleted";
				$messageVal = ROLEEX_016;
			}
			elseif ($params['request_type'] == 'qr_transfer') {
				$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered  through qr code by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}

			// $transactionParams['request_type'] = $params['request_type'];
			// $transactionParams['from_user'] = $params['from_user_id'];
			// $transactionParams['to_user'] = $params['to_user'];
			// $transactionParams['role_id'] = $params['role_id'];
			// $transactionParams['custom_message'] = $custMessage;

			$saveRoleExchangeTransactions = $this->UserBatonRoleTransaction->saveRoleExchangeTransactions($transactionParams);

			$fcmVoipToken = $params['fcm_voip_token'];
			$passphrase = 'password';
			$message = 'User Role Exchange';
			$pemFile = 'app/Config/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "ios_voip"=>1,
                        "VOIPCall"=>1,
                        "VOIPRoleExchange"=>1,
                        "role_type"=>2,
                        "message"=>$messageVal,
                        'title'=>"Baton Role ".$var,
                        'push_on'=>$requestTypeVal,
                        'role_id'=>$params['role_id'],
                        'is_qr_request'=>$params['is_qr_request']
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}

	public function roleExchangePushNotificationOnAndroid($params = array())
	{
		if(!empty($params['fcm_voip_token'])){
			$var = "";
			$requestTypeVal = 0;
			if($params['request_type'] == 'transfer')
			{
				// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
				$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'].".";
				if($params['to_user'] != 0){
					$custMessage = $params['first_name']." ".$params['last_name']." Transfer " .$params['role_name']. " role to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = $params['first_name']." ".$params['last_name']." Transfer ".$params['role_name']." to Switch Borad ";
				}
			}
			elseif ($params['request_type'] == 'transfer_accept') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_004;
				$var = "Accepted";
				if($params['to_user'] != 0){
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
			}
			elseif ($params['request_type'] == 'transfer_rejected') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_005;
				$var = "Rejected";
				$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'request') {
				$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'];
				$requestTypeVal = $params['push_on'];
				if($params['to_user'] != 0){
					$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
			}
			elseif ($params['request_type'] == 'request_accept') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_002;
				$var = "Accepted";
				$requestTypeVal = $params['push_on'];
				if($params['to_user'] != 0){
					$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
			}
			elseif ($params['request_type'] == 'request_reject') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_003;
				$var = "Rejected";
				if($params['to_user'] != 0){
					$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				else{
					$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
			}

			elseif ($params['request_type'] == 'revoke_request') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
				$var = "Revoked";
				$custMessage = "Request revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'revoke_transfer') {
				$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
				$var = "Revoked";
				$custMessage = "Transfer revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'timeout') {
				$var = "Timeout";
				$messageVal = ROLEEX_007;
			}
			elseif ($params['request_type'] == 'swichborad_accept') {
				$var = "Accepted";
				$messageVal = ROLEEX_008;
				$requestTypeVal = $params['push_on'];
				$custMessage = "Request accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'swichborad_rejected') {
				$var = "Rejected";
				$messageVal = ROLEEX_009;
				$custMessage = "Request rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			//TO do this will be removed when auto accept functionality goes live
			elseif ($params['request_type'] == 'swichborad_transfered') {
				$var = "Requested";
				$messageVal = ROLEEX_019;
				$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			// elseif ($params['request_type'] == 'swichborad_added') {
			elseif ($params['request_type'] == 'swichborad_assigned') {
				$var = "Assigned";
				$messageVal = ROLEEX_011;
				$requestTypeVal = $params['push_on'];
				$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'swichborad_removed') {
				$var = "Rejected";
				$messageVal = ROLEEX_012;
			}elseif ($params['request_type'] == 'swichborad_transfer_accept') {
				$var = "Accepted";
				$messageVal = ROLEEX_013;
				$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'swichborad_transfer_rejected') {
				$var = "Rejected";
				$messageVal = ROLEEX_014;
				$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			elseif ($params['request_type'] == 'swichborad_unassign') {
				$var = "Unassigned";
				$messageVal = ROLEEX_017;
			}
			elseif ($params['request_type'] == 'swichborad_deleted') {
				$var = "Deleted";
				$messageVal = ROLEEX_016;
			}
			elseif ($params['request_type'] == 'qr_transfer') {
				$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered  through qr code by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
			}
			// $transactionParams['request_type'] = $params['request_type'];
			// $transactionParams['from_user'] = $params['from_user_id'];
			// $transactionParams['to_user'] = $params['to_user'];
			// $transactionParams['role_id'] = $params['role_id'];
			// $transactionParams['is_qr_request'] = $params['is_qr_request'];
			// $transactionParams['custom_message'] = $custMessage;

			$saveRoleExchangeTransactions = $this->UserBatonRoleTransaction->saveRoleExchangeTransactions($transactionParams);
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$voipPushMessage = array
					(
						'VOIPRoleExchange'=> 1,
						'VOIPCall'=> 1,
						'role_type'=>2,
						'message'=>$messageVal,
						'title'=> "Baton Role".$var,
						'push_on'=>$requestTypeVal,
						'role_id'=>$params['role_id'],
                        'is_qr_request'=>$params['is_qr_request']
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $var = 1;
		}
		else
		{
			return $var = 0;
		}

	}



	//Test Function To send push notification On andriod
	public function sendRoleExchangePushNotificationTwo()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			try{
				$params['fcm_voip_token'] = "c15FQcdhLCI:APA91bGldAF8ByhwwmE8Qgsdg1bKmeUoAcSpIWlUKki0ZHMBsfXdhPU7pOPVWnnHJMq14H_TqBEN7yEFQjVP2LM080K0aqCEj5iHb29MzVqi9CSzKkQia3OgTU4HmEYgrDrUrhgR6QOT";
				$userData = $this->sendRoleExchangePushNotificationAndroidTwo($params);
				$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $userData);
			}catch( Exception $e ) {
				$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
			}
		}else{
			$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	public function sendRoleExchangePushNotificationAndroidTwo($params = array()) {
		if(!empty($params))
		{
			$responseData = $this->sendRoleExchangePushNotificationOnAndroid($params);
			return $responseData;
		}
	}

	public function sendRoleExchangePushNotificationOnAndroid($params = array())
	{
		$params['fcm_voip_token'] ="c1IrzNa9Bhk:APA91bFazdP2jPtp7XRe9Er9-8FNwzFIW3zBKLcsujF8JR_ixYaOsOleEeATxHuILoJ41sEpBESI8wWnsBlRNSbaY0duljKDg6i7lDq6YV5CTb9hvuqFJpqC5iQSRGTYFNd1NLIHWD70";
		if(!empty($params['fcm_voip_token'])) {
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$isDndActive = $params['is_dnd_active'];
			$voipPushMessage = array
					(
						'VOIPLogoutStatus'=> 1,
						'VOIPCall'=> 1,
						'message'=>ERROR_602
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $result;
		}
		else
		{
			return $result;
		}

	}
	/*
	----------------------------------------------------------------------------------------------
	On: 12-03-2019
	I/P: JSON
	O/P: JSON
	Desc: Update at work status for user on qb
	----------------------------------------------------------------------------------------------
	*/

	public function updateOnCallStatusRoleExchange($userId)
	{
		$response = array();
		$customData = array();
		$companyId = $userId['company_id'];
		$is_import = "true";
		$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $userId['user_id'])));
		$userEmail = $userData['User']['email'];

		//** One time token to validate for QB[START]
		$oneTimeTokenForOnCall = $this->genrateRandomToken($userId['user_id']);
		$oneTimeTokenDataOnCall = array("user_id"=> $userId['user_id'], "token"=> $oneTimeTokenForOnCall);
		$this->UserOneTimeToken->saveAll($oneTimeTokenDataOnCall);
		$password = $oneTimeTokenForOnCall;
		//** One time token to validate for QB[END]

		$tokenDetails = $this->Quickblox->quickLogin($userEmail, $password);
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
		$custom_data_from_api = json_decode($user_details->user->custom_data);

		if($userId['on_call_value'] == 0) {
			$duty = (isset($custom_data_from_api->on_call) && (string)$custom_data_from_api->on_call == '1')?'1':"0";
			$at_work = "1";
		}else{
			$duty = "1";
			$at_work = "1";
		}

		$customData['userRoleStatus'] = $custom_data_from_api->status;
		$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
		$customData['isImport'] = isset($is_import) ? $is_import : "true";
		$customData['at_work'] = $at_work;
		$customData['on_call'] = $duty;
		$customData['dnd_status'] = isset($custom_data_from_api->dnd_status)?$custom_data_from_api->dnd_status:[];
		if(!empty($customData['dnd_status']))
		{
			$customData['is_dnd_active'] = 1;
		}
		if(!empty($token)) {
			$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
			$res = json_decode(json_encode($qbResponce));
			if(!empty($res['errors'])) {
				$response['qberror'] = $qbResponce;
			}
			else
			{
				$response['qbsuccess'] = 1;
				// ----------
					$this->UserDutyLog->updateAll(array("status"=> $duty, "atwork_status"=>$at_work), array("user_id"=>$userId['user_id'],"hospital_id"=>$userId['company_id']));
					// DK: Save the On call transaction------->>>>>>>>
					$transactionData = array();
					if(isset($custom_data_from_api->on_call) && (string)$custom_data_from_api->on_call == '0' && $duty == '1'){
						$transactionData[] = array(
							'user_id'=>$userId['user_id'],
							'company_id'=>$companyId,
							'type'=>'OnCall',
							'status'=>$duty,
							'device_type'=>'Institution_pannel',
							'platform'=>'web'
						);
					}

					// DK: Save the Available transaction------->>>>>>>>
					if(isset($custom_data_from_api->at_work) && (string)$custom_data_from_api->at_work == '0' && $at_work == '1'){
						$transactionData[] = array(
							'user_id'=>$userId['user_id'],
							'company_id'=>$companyId,
							'type'=>'Available',
							'status'=>$at_work,
							'device_type'=>'Institution_pannel',
							'platform'=>'web'
						);
					}
					$this->AvailableAndOncallTransaction->saveAll($transactionData);
				// ----------
			}
		}
		else{
			$response['qberror'] = $tokenDetails;
		}
		return $response;
	}

	public function getUserBatonRolesData($params = array())
	{
		if($params['to_user'] != 0)
		{
			$touserData = $this->getUserBatonRoles($params['to_user']);
			$touser = array("UserProfile.custom_baton_roles"=> "'". json_encode($touserData) ."'");
			$toroleStatusResult = $this->UserProfile->updateAll($touser, array("UserProfile.user_id"=> $params['to_user']));
		}
		if($params['from_user'] != 0)
		{
			$fromUserData = $this->getUserBatonRoles($params['from_user']);
			$fromuser = array("UserProfile.custom_baton_roles"=> "'". json_encode($fromUserData) ."'");
			$fromroleStatusResult = $this->UserProfile->updateAll($fromuser, array("UserProfile.user_id"=> $params['from_user']));
		}
		return true;
	}

	public function getUserBatonRoles($userId)
	{
		$batonRoleDataList = array();
		if(!empty($userId))
		{
			$userbatonRolestatus = array(1,2,5,7);
			$batonRoleDetail = $this->UserBatonRole->find("all", array("conditions"=> array("from_user"=> $userId,"status"=>$userbatonRolestatus,"is_active"=>1),"group"=>array('UserBatonRole.role_id'),'order'=>array('UserBatonRole.updated_at')));
			foreach ($batonRoleDetail as  $value) {

				$batonRoleName = $this->BatonRole->find("first", array("conditions"=> array("id"=> $value['UserBatonRole']['role_id'])));
				$batonRoleDataList[] = array(
							'role_id'=> isset($value['UserBatonRole']['role_id']) ? $value['UserBatonRole']['role_id'] : "",
							'is_active'=>	isset($value['UserBatonRole']['is_active']) ? $value['UserBatonRole']['is_active'] : 0,
							'role_name'=>	isset($batonRoleName['BatonRole']['baton_roles']) ? $batonRoleName['BatonRole']['baton_roles'] : "",
							'type'=>isset($value['UserBatonRole']['status']) ? $value['UserBatonRole']['status'] : "0",
							);
			}

			return $batonRoleDataList;
		}
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 28-05-2019
	I/P: $to, $message, $serviceId
	O/P: Json Data
	Desc: Request Api to send sms from twillo
	----------------------------------------------------------------------------------------------
	*/

	public function sendSms($to, $message, $serviceId) {

			$fields = array(
					'Body' => urlencode($message),
					'MessagingServiceSid' => urlencode($serviceId),
					'To' => urlencode($to),
			);
			$fields_string ='';

			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/".Configure::read("twilio_details.account_sid")."/Messages.json");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_USERPWD, Configure::read("twilio_details.account_sid") . ":" . Configure::read("twilio_details.auth_token"));

			$result = curl_exec($ch);
			if($result){
					return json_encode($result);
			}
			if (curl_errno($ch)) {
					return 'Error:' . curl_error($ch);
			}
			curl_close ($ch);

	}

	/*
	----------------------------------------------------------------------------------------------
	On: 28-05-2019
	I/P: $sid(Request Id)
	O/P: Success, Failure
	Desc: Get Response from twillo
	----------------------------------------------------------------------------------------------
	*/

	public function getSmsResponse($sid) {

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/".Configure::read("twilio_details.account_sid")."/Messages/".$sid.".json");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 0);
			curl_setopt($ch, CURLOPT_USERPWD, Configure::read("twilio_details.account_sid") . ":" . Configure::read("twilio_details.auth_token"));

			$result = curl_exec($ch);
			if($result){
					return json_encode($result);
			}
			if (curl_errno($ch)) {
					return 'Error:' . curl_error($ch);
			}
			curl_close ($ch);

	}

	/*
	----------------------------------------------------------------------------------------------
	On: 12-06-2019
	I/P: user id's(from and to)
	O/P: Push Success or Failure
	Desc: This function will send push on Android and ios devices
	----------------------------------------------------------------------------------------------
	*/

	public function sendPermanentRoleExchangePushNotification($params)
	{
		if(!empty($params))
		{
			$sendRoleExchangePushNotification = $this->VoipPushNotification->find('first', array('conditions' => array('VoipPushNotification.user_id' => $params['other_user_id'])));
			$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $params['user_id'])));

			if(!empty($sendRoleExchangePushNotification))
			{
				$param['fcm_voip_token'] = $sendRoleExchangePushNotification['VoipPushNotification']['fcm_voip_token'];
				$param['request_type'] = $params['request_type'];
				$param['first_name'] = $userData['UserProfile']['first_name'];
				$param['last_name'] = $userData['UserProfile']['last_name'];
				$sendNotification = $this->permanentRoleExchangePushNotificationOnIos($param);
				$sendNotification = $this->permanentroleExchangePushNotificationOnAndroid($param);
			}
		}
	}

	public function permanentRoleExchangePushNotificationOnIos($params = array())
	{
		if(!empty($params))
		{
			$var = "";
			if($params['request_type'] == 'transfer')
			{
				// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
				$messageVal = PROLEEX_001.$params['first_name']." ".$params['last_name'].".";
			}
			elseif ($params['request_type'] == 'transfer_accept') {
				$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_002;
				$var = "Accepted";
			}
			elseif ($params['request_type'] == 'transfer_rejected') {
				$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_003;
				$var = "Rejected";
			}
			elseif ($params['request_type'] == 'revoke_transfer') {
				$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_004;
				$var = "Revoked";
			}
			elseif ($params['request_type'] == 'timeout') {
				$var = "Timeout";
				$messageVal = PROLEEX_007;
			}
			$fcmVoipToken = $params['fcm_voip_token'];
			$passphrase = 'password';
			$message = 'User Role Exchange';
			$pemFile = 'app/Config/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "ios_voip"=>1,
                        "VOIPCall"=>1,
                        "VOIPRoleExchange"=>1,
                        "role_type"=>1,
                        "message"=>$messageVal,
                        'title'=>"Permanent Role ".$var
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}

	public function permanentroleExchangePushNotificationOnAndroid($params = array())
	{
		if(!empty($params['fcm_voip_token'])) {
			$var = "";
			if($params['request_type'] == 'transfer')
			{
				// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
				$messageVal = PROLEEX_001.$params['first_name']." ".$params['last_name'].".";
			}
			elseif ($params['request_type'] == 'transfer_accept') {
				$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_002;
				$var = "Accepted";
			}
			elseif ($params['request_type'] == 'transfer_rejected') {
				$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_003;
				$var = "Rejected";
			}
			elseif ($params['request_type'] == 'revoke_transfer') {
				$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_004;
				$var = "Revoked";
			}
			elseif ($params['request_type'] == 'timeout') {
				$var = "Timeout";
				$messageVal = PROLEEX_007;
			}
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$voipPushMessage = array
					(
						'VOIPRoleExchange'=> 1,
						'VOIPCall'=> 1,
						'role_type'=>1,
						'message'=>$messageVal,
						'title'=> "Permanent Role".$var
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $var = 1;
		}
		else
		{
			return $var = 0;
		}

	}

	/*
	----------------------------------------------------------------------------------------------
	On: 13-06-2019
	I/P: user id's(from and to)
	O/P: true or false
	Desc: This function will update user permanent role
	----------------------------------------------------------------------------------------------
	*/

	public function getUserPermanentRolesData($params = array())
	{
		if(!empty($params))
		{
			if($params['user_id'] != 0){
				$fromUserPermanentRole = $this->getUserPermanentRoles($params['user_id'],$params['institute_id']);
				$fromUserData = array("UserProfile.custom_permanent_role"=> "'". json_encode($fromUserPermanentRole) ."'");
				$toroleStatusResult = $this->UserProfile->updateAll($fromUserData, array("UserProfile.user_id"=> $params['user_id']));
			}

			if($params['other_user_id'] != 0){
				$toUserPermanentRole = $this->getUserPermanentRoles($params['other_user_id'],$params['institute_id']);
				$toUserData = array("UserProfile.custom_permanent_role"=> "'". json_encode($toUserPermanentRole) ."'");
				$fromroleStatusResult = $this->UserProfile->updateAll($toUserData, array("UserProfile.user_id"=> $params['other_user_id']));
			}
			return true;
		}
	}

	public function getUserPermanentRoles($userId, $instituteId)
	{
		if(!empty($userId))
		{
			$userpermanentRolestatus = array(1,2);
			$permanentRoleDetail = $this->UserPermanentRole->find("all", array("conditions"=> array("user_id"=> $userId, "status"=>$userpermanentRolestatus,"active"=>1, 'institute_id'=>$instituteId)));

			if(!empty($permanentRoleDetail))
			{
				foreach ($permanentRoleDetail as  $value) {
					$permanentRoleDataList[] =  json_decode($value['UserPermanentRole']['role']);
				}
			}
			else
			{
				$permanentRoleDataList = array();
			}

			return $permanentRoleDataList;
		}
	}



	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendAtWorkPushNotification($params =array())
	{
		if(!empty($params)) {
			$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));

			$userVersionInfo =  $this->UserLoginTransaction->find('first', array('conditions' =>
		                array('UserLoginTransaction.user_id' => $params['user_id'], 'UserLoginTransaction.device_type !=' => 'MBWEB'),'order'=>array('UserLoginTransaction.id DESC')
		                ,'limit'=> 1));

			$dataArr =  json_decode($userVersionInfo['UserLoginTransaction']['device_info']);
			$version = $dataArr->version;
			$deviceType = $dataArr->device_type;

			if($deviceType == "iOS"){
				if($version > "1.8.7"){
					if(!empty($userVoipData))
					{
						foreach ($userVoipData as $key => $value) {
							$params['device_token'] = $value['VoipPushNotification']['device_token'];
							$params['fcm_voip_token'] = $value['VoipPushNotification']['fcm_voip_token'];
							$params['at_work_status'] = $params['at_work_status'];
							$params['push_on'] = $params['push_on'];
							$sendNotification = $this->sendAtWorkPushNotificationOnIosOne($params);
						}
					}
				}
			}

			if($deviceType == "android" || $deviceType == "Android"){
				// if($version > "1.9.8"){
					if(!empty($userVoipData))
					{

						foreach ($userVoipData as $key => $value) {
							$params['device_token'] = $value['VoipPushNotification']['device_token'];
							$params['fcm_voip_token'] = $value['VoipPushNotification']['fcm_voip_token'];
							$params['at_work_status'] = $params['at_work_status'];
							$params['push_on'] = $params['push_on'];
							$sendNotification = $this->sendAtWorkPushNotificationAndroidOne($params);
						}
					}
				// }
			}

		}else{
			return "Data is empty";
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendAtWorkPushNotificationOnIosOne($params = array())
	{
		if(!empty($params))
		{

			$deviceToken = $params['device_token'];
			$fcmVoipToken = $params['fcm_voip_token'];
			$atworkStatus = $params['at_work_status'];
			$pushOn = $params['push_on'];
			$passphrase = 'password';
			$message = 'Update Available Status';
			$pemFile = 'app/Config/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "ios_voip"=>1,
                        "VOIPCall"=>1,
                        "at_work_status"=>$atworkStatus,
                        "push_on"=>$pushOn
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}
	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendAtWorkPushNotificationAndroidOne($params = array()) {
		if(!empty($params))
		{
			$responseData = $this->sendAtWorkPushNotificationOnAndroid($params);
			return $responseData;
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function sendAtWorkPushNotificationOnAndroid($params = array())
	{
		if(!empty($params['fcm_voip_token'])) {
			$dataUserDevice = isset($params['fcm_voip_token']) ? $params['fcm_voip_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$atworkStatus = $params['at_work_status'];
			$pushOn = $params['push_on'];
			$voipPushMessage = array
					(
						'at_work_status'=> $atworkStatus,
						'push_on'=>$pushOn,
						'VOIPCall'=> 1
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $var = 1;
		}
		else
		{
			return $var = 0;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 23-07-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function getRoleTagForCustomDimension($userId,$companyId){
		if(isset($userId)){
			$return_array = array();
			$perm_data = $this->UserPermanentRole->find('all',array(
				'conditions'=>array(
					'UserPermanentRole.user_id' => $userId,
					'UserPermanentRole.institute_id' => $companyId
				),
				'fields' => array('
					UserPermanentRole.role
				'),
				'order' => 'UserPermanentRole.created_at ASC'
			));
			if(!empty($perm_data)){
				foreach ($perm_data as $perm_val) {
					$permanet_data = json_decode($perm_val['UserPermanentRole']['role'],true);
					if(isset($permanet_data['role'])){
						foreach ($permanet_data['role'] as $value) {
							if(count($value['values']) > 0){
								if(!isset($return_array[$value['tag']]) || $return_array[$value['tag']] == ''){
									$return_array = array_merge($return_array,array($value['tag'] => $value['values'][0]));
								}
							}
						}
					}
				}
			}
			if(!empty($return_array)){
				return json_encode(array($return_array));
			}else{
				return json_encode($return_array);
			}
		}
	}

	/*
	--------------------------------------------------------------------
	On: 23-07-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function saveTransaction($params = array() ){
		$response = 0;
		if(!empty($params))
		{	$params['note'] = "";
			$duration = (isset($duration)) ? $duration : 0;
			if($params['type'] == "Available")
			{
				$transactionParams['user_id'] = $params['user_id'];
				$transactionParams['company_id'] = $params['company_id'];
				$transactionParams['type'] = "Available";
				$transactionParams['status'] = $params['at_work'];
				$transactionParams['device_type'] = "Institution_pannel";
				$transactionParams['platform'] = "web";
      	$this->AvailableAndOncallTransaction->saveAll($saveTransactionData);
      	// if($params['start_time'] && $params['at_work'] == "0")
      	// {
      	// 	$this->DndTransaction->saveAll(array(
				// 		'user_id'=>$params['user_id'],
				// 		'institute_id'=>$params['company_id'],
				// 		"dnd_status_id"=>$params['dnd_status_id'],
				// 		"start_time"=>$params['start_time'],
				// 		"end_time"=>$params['end_time'],
				// 		"duration"=>"0","is_active"=>"0",
				// 		"other_user_id"=>"9", "note"=>$params['note'],
				// 		'custom_data'=>'web',
				// 		'created'=>date("Y-m-d H:i:s")
				// 	));
      	// }
			}
			if($params['type'] == "OnCall")
			{
				$transactionParams['user_id'] = $params['user_id'];
				$transactionParams['company_id'] = $params['company_id'];
				$transactionParams['type'] = "OnCall";
				$transactionParams['status'] = $params['oncall'];
				$transactionParams['device_type'] = "Institution_pannel";
				$transactionParams['platform'] = "web";
	      $this->AvailableAndOncallTransaction->saveAll($saveTransactionData);
			}
			$response = 1;
		}
		return $response;
	}

}// End class
