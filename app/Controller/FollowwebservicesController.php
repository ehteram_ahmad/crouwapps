
<?php
/*
Desc: Manage User Follow/Following webservices.
*/

//App::uses('AppController', 'Controller');

class FollowwebservicesController extends AppController {

	public $uses = array('User', 'UserFollow', 'Profession', 'Country', 'UserInstitution', 'InstituteName');
	public $components = array('Common');

	/*
	----------------------------------------------------------------------------------------------
	On: 08-10-2015
	I/P: 
	O/P: 
	Desc: Add follow user
	----------------------------------------------------------------------------------------------
	*/
	public function followUnfollowUser(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ; 
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->findById($dataInput['followed_by']) ){
					if( $this->User->findById($dataInput['followed_to']) ){
						//** Add follow/unfollow user
						$followData = array("followed_by"=> (int) $dataInput['followed_by'], "followed_to"=> (int) $dataInput['followed_to'], "follow_type"=> (int) $dataInput['follow_type'], "status"=> 1);
						try{
							if( $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> (int) $dataInput['followed_by'], "followed_to"=> (int) $dataInput['followed_to']))) > 0 ){
								$this->UserFollow->updateAll( array("follow_type"=> (int) $dataInput['follow_type']), array("followed_by"=> (int) $dataInput['followed_by'], "followed_to"=> (int) $dataInput['followed_to']) );
							}else{
								$this->UserFollow->save( $followData );
							}
							$responseData = array('method_name'=> 'followUser', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
						}catch(Exception $e){
							$responseData = array('method_name'=> 'followUser', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
						}
					}else{
						$responseData = array('method_name'=> 'followUser', 'status'=>"0", 'response_code'=> "624", 'message'=> ERROR_624);
					}
				}else{
					$responseData = array('method_name'=> 'followUser', 'status'=>"0", 'response_code'=> "623", 'message'=> ERROR_623);
				}
			}else{
				$responseData = array('method_name'=> 'followUser', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'followUser', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 12-10-2015
	I/P: JSON
	O/P: JSON
	Desc: List Follow user details
	----------------------------------------------------------------------------------------------
	*/
	public function listFollow(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ; 
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->findById($dataInput['user_id']) ){
					//** List of follow/following users
					$pageSize = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$pageNum = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
					$followId = isset($dataInput['follow_id']) ? $dataInput['follow_id'] : '';
					$followUserList = $this->UserFollow->followLists( $pageSize, $pageNum, $dataInput['user_id'], $followId ); 
					if(!empty($followUserList)){
						$loggedinUserId = $dataInput['loggedin_user_id'];
						$followUserData = $this->formatFollowList($followUserList, $loggedinUserId);
						$responseData = array('method_name'=> 'listFollow', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Follow"=> $followUserData));
					}else{
						$responseData = array('method_name'=> 'listFollow', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
					}
				}else{
					$responseData = array('method_name'=> 'listFollow', 'status'=>"0", 'response_code'=> "623", 'message'=> ERROR_623);
				}
			}else{
				$responseData = array('method_name'=> 'listFollow', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'listFollow', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	--------------------------------------------------------------------------------------------------
	On: 12-10-2015
	I/P: array()
	O/P: array()
	Desc: Format follow details fields
	-------------------------------------------------------------------------------------------------
	*/
	public function formatFollowList( $followData = array(), $loggedinUserId = NULL ){
		$followDataListArr = array();
		if(!empty($followData)){
			foreach( $followData as $follow ){
				//** Get Country name
				$countryDetails = $this->Country->find("first", array("conditions"=> array("id"=> $follow['UserProfile']['country_id'])));
				//** Get Profession
				$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $follow['UserProfile']['profession_id'])));
				//** get Institution
				$institutionDetails = $this->InstituteName->find("first", array("conditions"=> array("id"=> $follow['UserInstitution']['institution_id'])));

				$followDataList['user_id'] = !empty($follow['UserProfile']['user_id']) ? $follow['UserProfile']['user_id'] : '';
				$followDataList['first_name'] = !empty($follow['UserProfile']['first_name']) ? $follow['UserProfile']['first_name'] : '';
				$followDataList['last_name'] = !empty($follow['UserProfile']['last_name']) ? $follow['UserProfile']['last_name'] : '';
				$followDataList['institution_name'] = !empty($institutionDetails['InstituteName']['institute_name']) ? $institutionDetails['InstituteName']['institute_name'] : '';
				$followDataList['country'] = !empty($countryDetails['Country']['country_name']) ? $countryDetails['Country']['country_name'] : '';
				$followDataList['county'] = !empty($follow['UserProfile']['county']) ? $follow['UserProfile']['county'] : '';
				$followDataList['profession'] = !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type'] : '';
				$followDataList['profile_img'] = !empty($follow['UserProfile']['profile_img']) ? AMAZON_PATH . $follow['UserProfile']['user_id']. '/profile/' . $follow['UserProfile']['profile_img'] : '';
				$isFollow = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $loggedinUserId, "followed_to"=> $follow['UserProfile']['user_id'], "follow_type"=> 1, "status"=> 1)));
				$followDataList['is_following'] = ( $isFollow > 0 ) ? 1 : 0 ;
				$followDataListArr[] = $followDataList;
			}
		}
		return $followDataListArr;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 12-10-2015
	I/P: JSON
	O/P: JSON
	Desc: List Following user details
	----------------------------------------------------------------------------------------------
	*/
	public function listFollowing(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true); 
				if( $this->User->findById($dataInput['user_id']) ){
					//** List of follow/following users
					$pageSize = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$pageNum = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
					$followId = isset($dataInput['follow_id']) ? $dataInput['follow_id'] : '';
					$followingUserList = $this->UserFollow->followingLists( $pageSize, $pageNum, $dataInput['user_id'], $followId );
					if(!empty($followingUserList)){
						$logedinUserId = $dataInput['loggedin_user_id'];
						$followingUserData = $this->formatFollowingList( $followingUserList, $logedinUserId );
						$responseData = array('method_name'=> 'listFollowing', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Following"=> $followingUserData));
					}else{
						$responseData = array('method_name'=> 'listFollowing', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
					}
				}else{
					$responseData = array('method_name'=> 'listFollowing', 'status'=>"0", 'response_code'=> "623", 'message'=> ERROR_623);
				}
			}else{
				$responseData = array('method_name'=> 'listFollowing', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'listFollowing', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	--------------------------------------------------------------------------------------------------
	On: 12-10-2015
	I/P: array()
	O/P: array()
	Desc: Format following details fields
	-------------------------------------------------------------------------------------------------
	*/
	public function formatFollowingList( $followingData = array(), $loggedinUserId = NULL ){
		$followingDataArr = array();
		if(!empty($followingData)){
			foreach( $followingData as $following ){
				//** Get Country name
				$countryDetails = $this->Country->find("first", array("conditions"=> array("id"=> $following['UserProfile']['country_id'])));
				//** Get Profession
				$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $following['UserProfile']['profession_id'])));
				//** get Institution
				$institutionDetails = $this->InstituteName->find("first", array("conditions"=> array("id"=> $following['UserInstitution']['institution_id'])));
				
				$followingDataList['user_id'] = !empty($following['UserProfile']['user_id']) ? $following['UserProfile']['user_id'] : '';
				$followingDataList['first_name'] = !empty($following['UserProfile']['first_name']) ? $following['UserProfile']['first_name'] : '';
				$followingDataList['last_name'] = !empty($following['UserProfile']['last_name']) ? $following['UserProfile']['last_name'] : '';
				$followingDataList['institution_name'] = !empty($institutionDetails['InstituteName']['institute_name']) ? $institutionDetails['InstituteName']['institute_name'] : '';
				$followingDataList['country'] = !empty($countryDetails['Country']['country_name']) ? $countryDetails['Country']['country_name'] : '';
				$followingDataList['county'] = !empty($following['UserProfile']['county']) ? $following['UserProfile']['county'] : '';
				$followingDataList['profession'] = !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type'] : '';
				$followingDataList['profile_img'] = !empty($following['UserProfile']['profile_img']) ? AMAZON_PATH . $following['UserProfile']['user_id']. '/profile/' . $following['UserProfile']['profile_img'] : '';
				//** Check if loggedin user also following the user who following him
				$isFollow = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $loggedinUserId, "followed_to"=> $following['UserProfile']['user_id'], "follow_type"=> 1, "status"=> 1)));
				$followingDataList['is_following'] = ( $isFollow > 0 ) ? 1 : 0;
				$followingDataArr[] = $followingDataList;
			}
		}
		return $followingDataArr;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 19-10-2015
	I/P: JSON
	O/P: JSON
	Desc: count for follow/following for any particular user
	----------------------------------------------------------------------------------------------
	*/
	public function followFollowingCount(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true); 
				if( $this->User->findById($dataInput['user_id']) ){
					//** count for follow/following
					try{
						//$followCount = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $dataInput['user_id'], "follow_type"=> 1, "status"=> 1)));
						$followCount = $this->UserFollow->find("count", 
							array(
							'joins' =>
							array(
								array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'left',
								'conditions'=> array('UserFollow.followed_to = User.id')
								),
							),
							"conditions"=> 
								array("UserFollow.followed_by"=> $dataInput['user_id'], "UserFollow.follow_type"=> 1, "UserFollow.status"=> 1, 'User.status'=> 1, 'User.approved'=> 1)
							)
							);
						//$followingCount = $this->UserFollow->find("count", array("conditions"=> array("followed_to"=> $dataInput['user_id'], "follow_type"=> 1, "status"=> 1)));
						$followingCount = $this->UserFollow->find("count",
							array(
							'joins' => 
								array(
									array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'left',
									'conditions'=> array('UserFollow.followed_by = User.id')
									),
								),
							"conditions"=> 
								array("UserFollow.followed_to"=> $dataInput['user_id'], "UserFollow.follow_type"=> 1, "UserFollow.status"=> 1, 'User.status'=> 1, 'User.approved'=> 1)
							 )
							);
						$followFollowingCount = array("follow"=> $followCount, "following"=> $followingCount);
						$responseData = array('method_name'=> 'followFollowingCount', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $followFollowingCount);
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'followFollowingCount', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'followFollowingCount', 'status'=>"0", 'response_code'=> "623", 'message'=> ERROR_623);
				}
			}else{
				$responseData = array('method_name'=> 'followFollowingCount', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'followFollowingCount', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	
}

