

<?php
/*
Desc: Data Push/Pull related to GMC user.
*/

//App::uses('AppController', 'Controller');

class GmcuserwebservicesController extends AppController {

	public $uses = array('GmcUkUser','User', 'NpiUsaUser');
	public $components = array('Common');

	public function beforeFilter(){
		header('Access-Control-Allow-Origin: *'); 
	}
	
	/*
	On: 23-07-2015
	I/P: $params = array()
	O/P: GMC users details
	Desc: Fetch all Gmc Users details according to conditions.
	*/
	public function gmcUsersSearch(){
		
		$resposneGmcUser = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			$params['first_name'] = isset($dataInput['first_name'])?$dataInput['first_name']:'';
			$params['last_name'] = isset($dataInput['last_name'])?$dataInput['last_name']:'';
			$params['size'] = isset($dataInput['size'])?$dataInput['size']:'';
			$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
			$params['country'] = isset($dataInput['country'])?$dataInput['country']:'';
			//if(!empty($dataInput['profession_id']) ){
			$data = array();
			if(in_array(strtolower($params['country']), array('united kingdom','uk','gb'))){ //** If user is from uk check uk GMC
				if(in_array($dataInput['profession_id'], array(8))){ //** If profession is Doctor
					$data = $this->GmcUkUser->gmcUserSearch( $params );
				}
			}elseif(in_array(strtolower($params['country']), array('united states','us','usa'))){
				if(in_array($dataInput['profession_id'], array(8))){ //** If profession is Doctor
					$this->NpiUsaUser->useDbConfig = 'gmc';
					$data = $this->NpiUsaUser->npiUserSearch( $params );
					//echo "<pre>"; print_r($data);die;
				}
			}
			if(!empty($data)){
				foreach($data as $gmcData){
					$gmcUsers[] = array(
									'id'=> $gmcData['GmcUkUser']['id'], 
									'GMCRefNo'=> $gmcData['GmcUkUser']['GMCRefNo'],
									'Surname'=> $gmcData['GmcUkUser']['Surname'],
									'GivenName'=> $gmcData['GmcUkUser']['GivenName'],
									'Gender'=> $gmcData['GmcUkUser']['Gender'],
									'YearOfQualification'=> $gmcData['GmcUkUser']['YearOfQualification'],
									'PlaceofQualificationCountry'=> $gmcData['GmcUkUser']['PlaceofQualificationCountry'],
									);
					}
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'data'=> array('GmcUser'=> $gmcUsers));
				}else{
					$gmcUsers = array();
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'data'=> array('GmcUser'=> $gmcUsers));
				}
			/*}else{
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"0", 'response_code'=> "634", 'message'=> ERROR_634);
			}*/
		}else{
			$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"0", 'response_code'=> "601", 'message'=>'Information not provided by app');
		}
		//echo json_encode($resposneGmcUser);
		$encryptedData = $this->Common->encryptData(json_encode($resposneGmcUser));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}
}
