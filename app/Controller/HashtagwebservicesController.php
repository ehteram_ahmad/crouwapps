
<?php
/*
Desc: API data get/post to Webservices realated to Hash Tag.
*/

class HashtagwebservicesController extends AppController {

	public $uses = array('HashTag');
	public $components = array('Common');

	/*
	On: 18-11-2015
	I/P: 
	O/P: 
	Desc: List out all hash tags OR get according to search criteria.
	*/
	public function hashTagLists(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** Get hash tags
				$pageSize = isset( $dataInput['size'] ) ? $dataInput['size'] : DEFAULT_PAGE_SIZE; 
				$pageNum = isset( $dataInput['page_number'] ) ? $dataInput['page_number'] : 1;
				$params['searchText'] = isset( $dataInput['searchText'] ) ? $dataInput['searchText'] : '';
				$hashTags = $this->HashTag->hashLists( $pageSize, $pageNum, $params );
				foreach( $hashTags as $ht ){
					$hashData[] = array("id"=> $ht['HashTag']['id'], "name"=> $ht['HashTag']['name']);
				}
				$hashTagData['HashTags'] = !empty($hashData) ? $hashData : array();
				$responseData = array('method_name'=> 'hashTagLists', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $hashTagData);
			}else{
				$responseData = array('method_name'=> 'hashTagLists', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'hashTagLists', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

}
