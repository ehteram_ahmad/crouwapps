<?php


App::uses('AppController', 'Controller');

class AppdetailsController extends AppController {
	public $uses = array('AppVersion');
	
	/*
	-------------------------------------
	On: 
	I/P:
	O/P:
	Desc: fetches app version details for Android, ios
	-------------------------------------
	*/
	public function appVersionDetails(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() ){
				//** Fetch app version details
				$conditions = array("device_type"=> $dataInput["device_type"], "status"=> 1);
				$appVersionDetails = $this->AppVersion->find("first", array("conditions"=> $conditions));
				if(!empty($appVersionDetails)){
					$versionDetails = array("version"=> $appVersionDetails['AppVersion']['version']);
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $versionDetails);
				}else{
					$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'appVersionDetails', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;	
	}
}
