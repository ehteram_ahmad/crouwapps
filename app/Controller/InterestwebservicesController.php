
<?php
/*
Desc: API data get/post to Webservices.
*/

//App::uses('AppController', 'Controller');

class InterestwebservicesController extends AppController {

	public $uses = array('User', 'UserInterest');
	public $components = array('Common');

	/*
	----------------------------------------------------------------------------------------------
	On: 06-10-2015
	I/P: 
	O/P: 
	Desc: Add interest for user
	----------------------------------------------------------------------------------------------
	*/
	public function addInterest(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true); 
				if( $this->User->findById( $dataInput['user_id'] )){
					//** update all Interests
					$this->UserInterest->updateAll(array("status"=>0), array('user_id'=> $dataInput['user_id']));
					//** add or update interest one by one according to request
					foreach( $dataInput['interests'] as $interestId ){	//echo "<pre>";print_r($interestId);die;
						$interestData = array('user_id'=> $dataInput['user_id'],'interest_id'=> $interestId, 'status'=> 1 );
						if($this->UserInterest->find("count", array("conditions"=>array('user_id'=> $dataInput['user_id'],'interest_id'=> $interestId))) ==0 ){
							$this->UserInterest->saveAll( $interestData );
						}else{
							$this->UserInterest->updateAll(array("status"=> 1), array('user_id'=> $dataInput['user_id'],'interest_id'=> $interestId));
						}
					}
					$responseData = array('method_name'=> 'addInterest', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
				}else{
					$responseData = array('method_name'=> 'addInterest', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'addInterest', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addInterest', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 06-10-2015
	I/P: 
	O/P: 
	Desc: List interest for user
	----------------------------------------------------------------------------------------------
	*/
	public function listUserInterest(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find( "count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0 ){
					//** List User Interest
					$pageSize = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$pageNumber = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
					$interestId = isset($dataInput['interest_id']) ? $dataInput['interest_id'] : '';
					$interestList = $this->UserInterest->interestList( $pageSize, $pageNumber, $dataInput['user_id'], $interestId );
					$interestData['interests'] = $this->getInterestFields($interestList);
					$responseData = array('method_name'=> 'listUserInterest', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $interestData);
				}else{
					$responseData = array('method_name'=> 'listUserInterest', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'listUserInterest', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'listUserInterest', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------
	On: 06-10-2015
	I/P:
	O/P:
	Desc: Formatting fields for interests
	------------------------------------------------------------------------------
	*/
	public function getInterestFields( $data = array() ){
		$interestDataArr = array();
		foreach( $data as $interest ){
			$interestData['id'] = $interest['UserInterest']['interest_id'];
			$interestData['name'] = $interest['spec']['name'];
			$interestData['img_path'] = SPECILITY_ICON_PATH . $interest['spec']['img_name'];
			$interestDataArr[] = $interestData;
		}
		return $interestDataArr;
	}
}

